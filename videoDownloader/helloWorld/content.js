// listen for request
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.action === "getTable") {
          var table = document.getElementsByClassName("directory-listing-table");
          var tbody = table[0].getElementsByTagName("tbody");
          var trows = tbody[0].getElementsByTagName("tr");
          var myrows = {};
          for(var i=0;i<trows.length; i++) {
            //console.log(i+" ",trows[i]);
            
            var td = trows[i].getElementsByTagName("td");
            var ja = [];
            for(j=0; j<td.length; j++) {
              //console.log("td:"+td[i].innerHtml);
              ja.push(td[j].innerHTML);
            }
            myrows[i] = ja;
          }
          //console.log("myrows:",myrows);
          var s = JSON.stringify(myrows);
          //console.log("s:"+s);
          return sendResponse(s);
        }
    }
);
