var url;
//var mydetvUrl = "https://dev.swarm.mydetv.com:8991";  //dev
var mydetvUrl = "https://swarm.mydetv.com:8992";  //prod

function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, (tabs) => {

    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

}

document.addEventListener('DOMContentLoaded', () => {
  
  getCurrentTabUrl((url) => {
    
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { action: "getTable" }, function (response) {
            //console.log("response from "+url,response);
            if(response===undefined) {
              console.log("No response?");
              return;
            }
            
            var jo = JSON.parse(response);
            //console.log("jo:",jo);
            processTable(url, jo);
        });
    });
  });
});

function processTable(url, jo) {
  $('#url').text(url);
  var table = "<table>";
  for (var key in jo) {
    if (jo.hasOwnProperty(key)) {
      table+="<tr>";
      //console.log(key + " -> " ,jo[key]);
      var json=jo[key];
      
      // <a href=\"1931_Cartoon_Within_a_Cartoon_1.thumbs/\">1931_Cartoon_Within_a_Cartoon_1.thumbs/</a>
      // "<a href="S01E04%20-%20Don%27t%20Come%20Back%20Alive.avi">S01E04 - Don't Come Back Alive.avi</a>"
      for(var i = 0; i < json.length; i++) {
        var obj = json[i];
        if(obj.indexOf("<a href=")!==-1) {
          //console.log("obj |"+obj+"|");
          var newobj = encodeURIComponent(obj);
          
          //console.log("newobj |"+newobj+"|");
          //newobj.replace("\">.*","");
          //console.log("newobj |"+newobj+"|");
          table+="<td><button class='grab' value=\""+newobj+"\">Grab</button></td>";

        }
        table+="<td>"+obj+"</td>";
      }

     table+="</tr>";       
    }
  }
  table+="<table>";
  $('#videos').replaceWith(table);
  var classname = document.getElementsByClassName("grab");
  for (var k = 0; k < classname.length; k++) {
    classname[k].addEventListener('click', fnClick, false);
  }
}

var fnClick = function(event){

  //alert("val:"+$(this).val());
  
  sendVideo($(this).val());
};

function sendVideo(video) {
  var pretitle = $('#pretitle').val();
  var data = {};
  data.url=url;
  data.url2=video;
  data.pretitle = pretitle;
  
  $.ajax({
        url: mydetvUrl+"/api/archive/insert",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("video inserted");
                    break;
                default:
                    
            }
            // success finally
            alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}











