#!/usr/bin/perl

# remember, workbench models tab has a nice compare widget
# this is a dangerous script and most likely needs to be edited each time you run it 
use strict;

print "updateStagingDatabase\n";

my @dropUpdateTables = (

	#'webUserLogin'
	#,'mydetvLog'
	#,'mydetvLogLevels'
	#,'logging_event'
	#,'logging_event_property'
	#,'logging_event_exception'
	#,'chatMessages'
	#'chatBlock'
	#'reportPerson'
);

my @dropUpdateTablesWithData = (
	#"mbp"
	#'securityQuestion'
	#'videoCategorySub'
	#,'videoCategoryMain'
	#,'videoCategorySubImage'

	#'webUser'
	#,'webUserEmail'
	#,'webUserFailed'
	#,'webUserImage'
	#,'webUserRole'
	#,'webUserUsername'
	#,'webUserPassword'
);


print "updating tables\n";
foreach my $table (@dropUpdateTables) {
    print " table: $table\n";
    dropTable($table);
    updateTable($table, 0);
}
foreach my $table (@dropUpdateTablesWithData) {
    print " table: $table\n";
    dropTable($table);
    updateTable($table, 1);
}

print "updating sprocs\n";
my $sc = "mysqldump mydetvDev --routines -n -d -t  > /tmp/routines";
print "sc: $sc\n";
system($sc);

$sc = "mysql mydetvStaging</tmp/routines";
print "sc: $sc\n";
system($sc);

print "done\n";
exit;

sub dropTable() {
my ($table) = @_;
    print " drop $table\n";
    my $sc = "mysql -e \"drop table mydetvStaging.$table\"";
    print "sc: $sc\n";
    system($sc);
}
sub updateTable() {
my ($table, $withdata) = @_;
    print " update $table\n";
    my $sc;
    $sc = "mysqldump ";
    if($withdata==0) {
	$sc .= " --no-data";
	}
    $sc .= " mydetvDev $table > /tmp/$table";
    print "sc: $sc\n";
    system($sc);
    $sc = "mysql mydetvStaging < /tmp/$table";
    print "sc: $sc\n";
    system($sc);
}





