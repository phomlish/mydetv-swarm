#!/usr/bin/perl

use strict;

# /private/nfs/a6/usr5/mydetv/recordings/videoroom-111-user-111-1545122269938373
my $inFpn= $ARGV[0];
if($inFpn eq '') {
    print "Error, you must provide an fpn\n";
    exit;
}
my @pieces = split "/", $inFpn;
#print @pieces."\n";
my $fp;
my $fn;
for(my $i=0; $i<@pieces; $i++) {
    print "piece ".$pieces[$i]."\n";
    if($i eq @pieces-1) {
        $fn=$pieces[$i];
    }
    else {
        $fp.=$pieces[$i]."/";
    }
}

print "fp:".$fp."\n";
print "fn:".$fn."\n";

my $tvideo = $fn."-video.mjr";
my $taudio = $fn."-audio.mjr";
my $sc;

$sc = "/usr2/dev/janus/janus-gateway/janus-pp-rec $fp$taudio $fn.opus";
print "sc:$sc\n";
system $sc;
$sc = "/usr2/dev/janus/janus-gateway/janus-pp-rec $fp$tvideo $fn.webm";
print "sc:$sc\n";
system $sc;

$sc = "ffmpeg -i $fn.opus -i $fn.webm -c:v copy -c:a opus -strict experimental mergedoutput.webm";
system $sc;
