# copy to a6 prod
rsync -ruvzL -e 'ssh -p 2222' /etc/letsencrypt/live/mydetv.com/* swarm@a6:/usr2/mydetv/certs/letsencryp
t/mydetv.com/

# copy to a6 staging
rsync -ruvzL -e 'ssh -p 2222' /etc/letsencrypt/live/mydetv.com/* swarm@a6.staging.homlish.net:/usr2/myd
etv/certs/letsencrypt/mydetv.com/

# copy to /hme/phomlish so macdev can get them
cp -aL /etc/letsencrypt/live/mydetv.com/ /home/phomlish/letsencrypt/
chown -R phomlish.homeuser /home/phomlish/letsencrypt/
chmod -R og-rwx /home/phomlish/letsencrypt/
