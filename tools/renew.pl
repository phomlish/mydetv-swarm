#!/usr/bin/perl

# this will need to be run as root in production (a6)
# this needs to be copied manualy to production! 
# cp renew.pl /private/nfs/a6/usr2/mydetv/tools/renew.pl 

use strict;
use JSON;
use File::stat;

my $instance = $ARGV[0];
if($instance eq '') {
    print "Error, you must provide an instance\n";
    exit;
}

if($instance ne 'macdev' and $instance ne 'a6' and $instance ne 'a6.staging') {
    print "didn't recognize instance $instance\n";
    exit;
}

print "running for $instance\n";

my $leDir;
my $swarmConfigDir;
my $swarmCertDirectory;
my $swarmUserGroup;
my $keytool;
my $keypass;
my $alias;
my $sc;
my $configFile;
my $out;

if($instance eq "macdev") {
    $swarmConfigDir = "/Users/phomlish/mydetv/conf";
    $leDir = "/Users/phomlish/mydetv/certs/letsencrypt/swarmdev";
    $swarmCertDirectory = "/Users/phomlish/mydetv";
    $swarmUserGroup = "phomlish:staff";
    $keytool = "/usr/bin/keytool";
    $alias = "swarmdev";
    # we need to get our certs from a0
    $sc = "cp -a /private/nfs/a0/home/phomlish/letsencrypt/mydetv.com/ $leDir";
    print "sc:$sc\n";
    $out = `$sc`;
    if($out ne '') {
        print "out: $out\n";
    }
}
elsif($instance eq 'a6.staging') {
    $swarmConfigDir = "/usr2/mydetv/conf";
    $leDir = "/usr2/mydetv/certs/letsencrypt/mydetv.com";
    $swarmCertDirectory = "/usr2/mydetv/certs";
    $swarmUserGroup = "swarm.swarm";
    $keytool = "/usr/bin/keytool";
    $alias = "swarmstaging";
}
else {
    $swarmConfigDir = "/usr2/mydetv/conf";
    $leDir = "/usr2/mydetv/certs/letsencrypt/mydetv.com";
    $swarmCertDirectory = "/usr2/mydetv/certs";
    $swarmUserGroup = "swarm.swarm";
    $keytool = "/usr/bin/keytool";
    $alias = "swarm";
}
$configFile = "$swarmConfigDir/swarm.json";


validateInitialization();
backup();
doINeedToRun();
getKeypass();
#combineCertAndKey();
fixCertDirectoryPermissions();
combineCertAndKeyToP12();
replaceKey();

$sc = "touch $leDir/.last";
system $sc;

if($instance eq "a6") {
    print "restarting our services\n";
    $sc = "systemctl restart broadcast.service";
    system $sc;
    $sc = "systemctl restart janus.service";
    system $sc;
}

sub backup {
   $sc = "cp $swarmConfigDir/letsencrypt.jks $swarmConfigDir/letsencrypt.jks.jic";
   system $sc;
}
sub validateInitialization {
   my $errors = 0;
   if (not -e $swarmConfigDir or not -d $swarmConfigDir) {
    print "$swarmConfigDir does not exist!\n";
    $errors++;
   }
   if (not -e $leDir or not -d $leDir) {
    print "$leDir does not exist!\n";
    $errors++;
   }
   if (not -e $swarmCertDirectory or not -d $swarmCertDirectory) {
    print "$swarmCertDirectory does not exist!\n";
    $errors++;
   }
   if (not -e $keytool or not -f $keytool) {
    print "$keytool does not exist!\n";
    $errors++;
   }

   if (not -e "$swarmConfigDir/letsencrypt.jks" or not -f "$swarmConfigDir/letsencrypt.jks") {
    print "$swarmConfigDir/letsencrypt.jks does not exist!\n";
    $errors++;
   }
   
   if($errors>0) {
      print "I will die now\n";
      exit;
   }
   
   print "using swarmCertDirectory $swarmCertDirectory\n";
   print "using swarmConfigDir $swarmConfigDir\n";
   print "using leDir $leDir\n";
   print "using keytool $keytool\n";
   print "using swarmUserGroup $swarmUserGroup\n";
   print "editing $swarmCertDirectory/letsencrypt.jks\n";
   print "\n";
   
}

sub doINeedToRun {
   my $lastMtime;
   my $certMtime;
   
   my $stLast = stat("$leDir/.last");
   print "checking $leDir/.last\n";
   if(! defined $stLast || $stLast eq "") {
       $lastMtime = time()-(365*24*60*60);
       print "no $leDir/.last, using $lastMtime\n";
   }
   else {
       $lastMtime = $stLast->mtime;
   }
   
   my $stCert = stat("$leDir/cert.pem") or die "No $leDir/cert.pem : $!";
   $certMtime = $stCert->mtime;
   print "last: $lastMtime, cert: $certMtime\n";
   
   if($certMtime<$lastMtime) {
       print "No need to run, the certs have not changed\n";
       #exit;
   }
}

sub getKeypass {
   my $json_text = do {
      open(my $json_fh, "<:encoding(UTF-8)", $configFile)
         or die("Can't open \$configFile\": $!\n");
      local $/;
      <$json_fh>  
   };

   my $json = JSON->new;
   my $data = $json->decode($json_text);
   
   my $m = $data->{mydetv};
   $keypass = $data->{'mydetv'}->{'keystorePassword'};
   #print "keypass:$keypass\n";
   
   if($keypass eq "") {
      print "bad keypass: '$keypass'\n";
      exit;
   }
}


sub combineCertAndKey {
   # a monthly cronjob on a0 will copy the updated certs to leDir
   # combine the updated cert and privkey and copy to the $swarmCertDirectory
   $sc = "cat $leDir/cert.pem > $swarmCertDirectory/letsencrypt.pem";
   print "sc:$sc\n";
   system $sc;
   $sc = "cat $leDir/privkey.pem >> $swarmCertDirectory/letsencrypt.pem";
   print "sc:$sc\n";
   system $sc;
}

sub fixCertDirectoryPermissions {
   # change owner and permissions
   $sc = "chown -R $swarmUserGroup $swarmCertDirectory";
   print "sc:$sc\n";
   system $sc;
   $sc = "chmod -R og-rwx $swarmCertDirectory";
   print "sc:$sc\n";
   system $sc;
}

sub combineCertAndKeyToP12 {
   # now we focus on the swarm website key
   # combine our cert and priv key into one p12 file
   $sc = "openssl pkcs12 -export -in $leDir/cert.pem -inkey $leDir/privkey.pem -out $leDir/swarm.p12 -name $alias -password pass:password";
   print "sc:$sc\n";
   system $sc;
}

sub replaceKey {
   # first remove the old key
   $sc = "keytool -delete -alias $alias -keystore $swarmConfigDir/letsencrypt.jks -storepass $keypass";
   print "sc:$sc\n";
   system $sc;
   
   # add to our keystore
   $sc = "keytool -importkeystore -deststorepass $keypass -destkeypass $keypass ";
   $sc .= "-destkeystore $swarmConfigDir/letsencrypt.jks -srckeystore $leDir/swarm.p12 -srcstoretype PKCS12 -srcstorepass password -alias $alias";
   print "sc:$sc\n";
   system $sc;
}

