package shared.setups;

public class SwarmException extends Exception {

	public SwarmException(String errorMessage, Throwable ex) {
		super(errorMessage, ex);
	}
	public SwarmException(String errorMessage) {
		super(errorMessage);
	}
}
