package shared.setups;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.BasicConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.io.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.OneDbConfig;
import shared.data.Config;

public class ReadSharedDatabase {
	
	private static final Logger log = LoggerFactory.getLogger(ReadSharedDatabase.class);
	
	public static void ReadConfigDb(Config sharedConfig) {
		try {
				
			XMLConfiguration xmlConfig = new BasicConfigurationBuilder<>(XMLConfiguration.class).configure(new Parameters().xml()).getConfiguration();
			FileHandler fh = new FileHandler(xmlConfig);
			fh.load(sharedConfig.getConfigDir()+"/db.xml");

			OneDbConfig dbConfig = sharedConfig.getDbConfig();
			dbConfig.setDbUrl(xmlConfig.getString("database(0).url"));
			dbConfig.setDbUsername(xmlConfig.getString("database(0).username"));
			dbConfig.setDbPassword(xmlConfig.getString("database(0).password"));
			dbConfig.setDbDriver(xmlConfig.getString("database(0).driver", "unknown"));
			dbConfig.setUseSsl(xmlConfig.getBoolean("database(0).useSsl", true));
			
		} catch (ConfigurationException cex) {
			log.error("Error opening configuration file: " + cex.toString());
			System.exit(1);
		}
	}
}
