package shared.setups;

public class SwarmConfigException extends Exception {

	public SwarmConfigException(String errorMessage, Throwable ex) {
		super(errorMessage, ex);
	}
	public SwarmConfigException(String errorMessage) {
		super(errorMessage);
	}
	
}
