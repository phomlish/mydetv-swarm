package shared.setups;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.Config;
import shared.data.MaintConfig;
import shared.data.OneDbConfig;
import shared.data.broadcast.StunServer;

public class ReadConfigJson {

	private static final Logger log = LoggerFactory.getLogger(ReadConfigJson.class);
	static JSONObject json;

	public static Config GetConfig(String configDir) throws SwarmConfigException {

		Config config = new Config();
		config.setConfigDir(configDir);
		String version = "0.0.0";
		try {
			Path vPath = Paths.get("version.txt");
			if(Files.exists(vPath)) {
				version = new String(Files.readAllBytes(vPath));
				version = version.replace("\n", " ");
			}
		} catch (Exception e) {
			throw new SwarmConfigException("could not read version.txt");
		}	
		config.setVersion(version);

		// target 36 chars
		int startPad = ((74-version.length())/2);
		int endPad = 74-version.length()-startPad;
		String paddedVersion = String.format("%1$"+startPad+"s", "")+version+String.format("%1$"+endPad+"s", "");
		log.info("  ___________________________________________________________________________ ");
		log.info(" /                                                                          /");
		log.info("|                                                                          |");
		log.info("|     __  __       _____       _                           _______         |");		
		log.info("|    |  \\/  |     |  __ \\     | |                         |__   __|        |");
		log.info("|    | \\  / |_   _| |  | | ___| | __ ___      ____ _ _ __ ___| |_   __     |");
		log.info("|    | |\\/| | | | | |  | |/ _ \\ |/ _` \\ \\ /\\ / / _` | '__/ _ \\ \\ \\ / /     |");
		log.info("|    | |  | | |_| | |__| |  __/ | (_| |\\ V  V / (_| | | |  __/ |\\ V /      |");
		log.info("|    |_|  |_|\\__, |_____/ \\___|_|\\__,_| \\_/\\_/ \\__,_|_|  \\___|_| \\_/       |");
		log.info("|             __/ |                                                        |");                                               
		log.info("|            |___/                                                         |");       
		log.info("|                                                                          |");
		log.info("|"+paddedVersion+"|");
		log.info("|                                                                          |");
		log.info(" \\                                                                        /");
		log.info("  \\______________________________________________________________________/");
		log.info("");




		try {
			String fc = new String(Files.readAllBytes(Paths.get(config.getConfigDir()+"/swarm.json")));
			JSONObject f = new JSONObject(fc);
			json = f.getJSONObject("mydetv");
		} catch (Exception e) {
			throw new SwarmConfigException("could not read swarm.json "+e);
		}

		config.setInstance(GetStringOrDefault("instance", "development").toLowerCase());
		if(config.getInstance().equals("production"))
			config.setInstance("prod");
		config.setTmpDir(GetStringOrDefault("tmpDir", "/tmp/swarm"));
		config.setApiKey(GetStringOrDefault("apiKey", "ABCD-EFGH"));

		String machineName = "unknown";
		try {
			InetAddress addr;
			addr = InetAddress.getLocalHost();
			machineName = addr.getHostName();
		}catch (UnknownHostException e) { 
			throw new SwarmConfigException("machineName can not be resolved "+e);
		}
		config.setMachineName(machineName);

		config.setHostname(GetStringOrDefault("hostname", "localhost").toLowerCase());
		config.setWebPort(GetIntOrDefault("webPort", 443));

		// setup our rootDir
		String rootDir = System.getProperty("user.dir");		
		config.setRootDir(rootDir);
		log.info("rootDir:"+rootDir);

		config.setKeystoreFile(GetStringOrDefault("keystoreFile", "cert.jks"));
		config.setKeystorePassword(GetStringOrDefault("keystorePassword", "insecure"));
		config.setTruststoreFile(GetStringOrDefault("truststoreFile", "trust.jks"));
		config.setTruststorePassword(GetStringOrDefault("truststorePassword", "insecure"));

		// smtp stuff
		config.setSmtpHost(GetStringOrDefault("smtpHost", "localhost"));
		config.setSmtpPort(GetIntOrDefault("smtpPort", 587));
		config.setSmtpFrom(GetStringOrDefault("smtpFrom", "no-reply@example.com"));

		config.setJanusHost(GetStringOrDefault("janusHost", "127.0.0.1"));
		config.setJanusUrl(GetStringOrDefault("janusUrl", "wss://127.0.0.1:8989"));
		config.setJanusApiSecret(GetStringOrDefault("janusApiSecret", null));
		config.setJanusAdminUrl(GetStringOrDefault("janusAdminUrl", null));
		config.setJanusAdminSecret(GetStringOrDefault("janusAdminSecret", null));

		config.setLockSystemFailedAttempts(GetIntOrDefault("lockSystemFailedAttempts", 20));

		config.setFfmpeg(GetStringOrDefault("ffmpeg", ""));
		config.setFfprobe(GetStringOrDefault("ffprobe", ""));
		config.setGstLaunch(GetStringOrDefault("gst-launch", ""));

		JSONObject html = json.getJSONObject("html");
		config.setCompany(html.getString("company"));

		config.setSecretKey(GetStringOrDefault("secretKey", "g'way"));

		config.setMaxConnections(GetIntOrDefault("maxConnections", 2));
		config.setMaxConnectionsTier1(GetIntOrDefault("maxConnectionsTier1", 2));

		config.setMasterServerPort(GetIntOrDefault("masterServerPort", 0));
		config.setJukeboxUrl(GetStringOrDefault("jukeboxUrl", "https://localhost"));
		config.setMatomoSite(GetIntOrDefault("matomoSite", 1));
		config.setMatomoUrl(GetStringOrDefault("matomoUrl", null));
		config.setMatomoKey(GetStringOrDefault("matomoKey", null));
		config.setVideosDirectory(GetStringOrDefault("videosDirectory", null));
		config.setVideosConvertedDirectory(GetStringOrDefault("videosConvertedDirectory", null));
		String ewd = GetStringOrDefault("externalWebDirectory", null);
		if(config.getInstance().contains("dev")) 
			ewd+="/dev";
		config.setExternalWebDirectory(ewd);
		config.setGeoip(GetStringOrDefault("geoip", null));
		config.setSoundDirectory(GetStringOrDefault("soundDirectory", ""));

		if(json.isNull("stunServers")) {
			//config.getStunServers().add(new StunServer("stun.l.google.com", 19302, "google"));
			config.getStunServers().add(new StunServer("stun.ekiga.net", 3478, "ekiga"));

		}
		else {
			JSONArray stuns = json.getJSONArray("stunServers");
			for (int i = 0; i < stuns.length(); i++) {
				JSONObject stun = (JSONObject) stuns.get(i);
				config.getStunServers().add(new StunServer(
					stun.getString("ip"),
					stun.getInt("port"),
					stun.getString("nickname")));
			}
		}

		// stun fallback
		if(config.getStunServers().size()==0) {
			//config.getStunServers().add(new StunServer("stun.l.google.com", 19302, "google"));
			config.getStunServers().add(new StunServer("stun.ekiga.net", 3478, "ekiga"));
		}

		// setup the database
		if(json.isNull("db"))
			throw new SwarmConfigException("could not read db section");
	
		JSONObject db = json.getJSONObject("db");
		OneDbConfig dbConfig = new OneDbConfig();
		dbConfig.setDbDriver(db.getString("driver"));
		dbConfig.setDbUrl(db.getString("url"));
		dbConfig.setDbUsername(db.getString("username"));
		dbConfig.setDbPassword(db.getString("password"));
		dbConfig.setUseSsl(db.getBoolean("useSsl"));		
		config.setDbConfig(dbConfig);

		// setup our maint section
		MaintConfig mc = new MaintConfig();
		config.setMaint(mc);
		if(json.isNull("maint")||json.isEmpty()) {
			log.info("maint config section not found");
		}
		else {
			JSONObject mco = json.getJSONObject("maint");
			mc.setSchedule(mco.getBoolean("schedule"));
			mc.setArchives(mco.getBoolean("archives"));
			mc.setProcessVideos(mco.getBoolean("processVideos"));
			mc.setConvert(mco.getBoolean("convert"));
			mc.setVideoWalk(mco.getBoolean("videoWalk"));
			mc.setThumbnails(mco.getBoolean("thumbnails"));
			mc.setGeoip(mco.getBoolean("geoIp"));
			mc.setLogMailer(mco.getBoolean("logMailer"));
		}
		log.info("Done reading configs");

		return config;
	}

	static String GetStringOrDefault(String key, String dv) {
		if(json.isNull(key))
			return dv;
		return json.getString(key);
	}
	static int GetIntOrDefault(String key, int dv) {
		if(json.isNull(key))
			return dv;
		return json.getInt(key);
	}
}
