package shared.setups;

import java.io.File;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.nio.conn.SchemeIOSessionStrategy;
import org.apache.http.nio.conn.ssl.SSLIOSessionStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.Config;

/**
 * the jks file is in the config directory<br>
 * config/certs is a working directory for certificate maintenance<br>
 * @throws ConfigurationException
 */
public class SetupEncryption {
	
	private static final Logger log = LoggerFactory.getLogger(SetupEncryption.class);

	public static void SetupHttpsTruststore(Config sharedConfig) throws SwarmConfigException {
		
		String trustStore=sharedConfig.getConfigDir()+"/"+sharedConfig.getTruststoreFile();
		
		File f = new File(trustStore);
		if(! f.exists() || f.isDirectory())
		    throw new SwarmConfigException("no trustStore found: "+trustStore);
		
		System.setProperty("javax.net.ssl.trustStore", trustStore);
		System.setProperty("javax.net.ssl.trustStorePassword",sharedConfig.getTruststorePassword());
		log.info("setup our truststore "+trustStore);
	}

	private static String[] protocols = new String[] {"TLSv1.2"};
	public static SslContextFactory SetupHttpsKeystore(Config sharedConfig) throws SwarmConfigException {
		String keyStore=sharedConfig.getConfigDir()+"/"+sharedConfig.getKeystoreFile();
		
		File f = new File(keyStore);
		if(! f.exists() || f.isDirectory()) 
		    throw new SwarmConfigException("no keystore found: "+keyStore);
		
		SslContextFactory sslContextFactory = new SslContextFactory();
		sslContextFactory.setKeyStorePath(keyStore);
		sslContextFactory.setKeyStorePassword(sharedConfig.getKeystorePassword());
		sslContextFactory.setKeyManagerPassword(sharedConfig.getKeystorePassword());

		sslContextFactory.setExcludeCipherSuites(
			"SSL_RSA_WITH_DES_CBC_SHA",
			"SSL_DHE_RSA_WITH_DES_CBC_SHA", 
			"SSL_DHE_DSS_WITH_DES_CBC_SHA",
			"SSL_RSA_EXPORT_WITH_RC4_40_MD5",
			"SSL_RSA_EXPORT_WITH_DES40_CBC_SHA",
			"SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA",
			"SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA",
			"TLS_RSA_WITH_AES_256_CBC_SHA256",
			"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
			"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
			"TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
			"TLS_RSA_WITH_AES_256_CBC_SHA",
			"TLS_RSA_WITH_AES_256_CBC_SHA",
			"TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA",
			"TLS_ECDH_RSA_WITH_AES_256_CBC_SHA",
			"TLS_DHE_RSA_WITH_AES_256_CBC_SHA",
			"TLS_DHE_DSS_WITH_AES_256_CBC_SHA",
			"TLS_RSA_WITH_AES_128_CBC_SHA256",
			"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
			"TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
			"TLS_RSA_WITH_AES_128_CBC_SHA",
			"TLS_RSA_WITH_AES_128_CBC_SHA",
			"TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA",
			"TLS_ECDH_RSA_WITH_AES_128_CBC_SHA",
			"TLS_DHE_RSA_WITH_AES_128_CBC_SHA",
			"TLS_DHE_DSS_WITH_AES_128_CBC_SHA",
			"TLS_RSA_WITH_AES_256_GCM_SHA384",
			"TLS_RSA_WITH_AES_128_GCM_SHA256"				
				);
		sslContextFactory.setIncludeProtocols(protocols);
		log.info("setup our keystore "+keyStore);
		return sslContextFactory;
	}
	
	public static Registry<SchemeIOSessionStrategy> GetOutgoingApiEncryption(Config config) throws SwarmConfigException  {
		SSLContextBuilder builder = SSLContexts.custom();
		//try {
			try {
				builder.loadTrustMaterial(null, new TrustStrategy() {
				    @Override
				    public boolean isTrusted(X509Certificate[] chain, String authType)
				            throws CertificateException {
				        return true;
				    }
				});
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			SSLContext sslContext;
			try {
				sslContext = builder.build();

			SchemeIOSessionStrategy sslioSessionStrategy = new SSLIOSessionStrategy(sslContext, 
	                new HostnameVerifier(){
	            @Override
	            public boolean verify(String hostname, SSLSession session) {
	                return true;// TODO as of now allow all hostnames
	            }
	        });
			Registry<SchemeIOSessionStrategy> sslioSessionRegistry = RegistryBuilder.
					<SchemeIOSessionStrategy>create()
					.register("https", sslioSessionStrategy).build();
			return sslioSessionRegistry;
		
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new SwarmConfigException("no keystore found:");
			}	
	
	}
	
	
}
