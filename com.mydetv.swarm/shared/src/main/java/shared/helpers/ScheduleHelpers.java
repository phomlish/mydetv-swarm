package shared.helpers;

import java.time.LocalDateTime;

import shared.data.OneSchedule;
import shared.data.OneVideo;

public class ScheduleHelpers {

	/**
	 * calculate endDT truncated to the second<br>
	 * os.startDt + ov.length rounded up<br>
	 * and then add one second to deal with the application delay in processing<br>
	 * @param os
	 * @param ov
	 * @return
	 */
	public static void SetEndDt(OneSchedule os, OneVideo ov) {
		int seconds = (int) Math.floor(ov.getLength()+0.9999999999);
		LocalDateTime endDt = os.getDt().plusSeconds(seconds);		
		os.setDtEnd(endDt);
	}	
	
}
