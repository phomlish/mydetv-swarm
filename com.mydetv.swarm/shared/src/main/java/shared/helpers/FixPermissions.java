package shared.helpers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixPermissions {

	private static final Logger log = LoggerFactory.getLogger(FixPermissions.class);
	
	public static void FixFilePermissions(String fpn) {
		try {
			ProcessBuilder builder = new ProcessBuilder("chown","phomlish:swarm",fpn);
			builder.redirectErrorStream(true);
			builder.start();
			builder = new ProcessBuilder("chmod","g+w",fpn);
			builder.redirectErrorStream(true);
			builder.start();
		} catch (IOException e) {
			log.error("error "+e);
		}
	}
}
