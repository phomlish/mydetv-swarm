package shared.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilenameHelpers {

	private static final Logger log = LoggerFactory.getLogger(FilenameHelpers.class);
	
	// 20080101.suffix 20080101A.suffix
	public static boolean ValidateShowDate(String str) {
		if(str.length()<8)
			return false;

		try {
			Integer i;
			i = Integer.parseInt(str.substring(0, 4));
			if(i<1900) return false;
			i = Integer.parseInt(str.substring(4, 6));
			if(i<1 || i>12) return false;
			i = Integer.parseInt(str.substring(6, 8));
			if(i<1 || i>31) return false;
		} catch(NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public static boolean ValidateShowSet(String str) {
		if(str.length()<1)
			return false;

		try {
			Integer i;
			i = Integer.parseInt(str.substring(0, 1));
			if(i<1 || i>9) return false;
		} catch(NumberFormatException e) {
			return false;
		}
		return true;
	}
	public static Integer GetShowSet(String str) {
		if(str.length()<1)
			return null;
		
		try {
			Integer i;
			i = Integer.parseInt(str.substring(0, 1));
			if(i<1 || i>9) return null;
			return i;
		} catch(NumberFormatException e) {
			return null;
		}
	}
	
	public static String StripSuffix(String str) {
		String rv="";
		try {
			String[] pieces = str.split("\\.");
			if(pieces.length<2) 
				return null;
			
			for(int i=0; i<pieces.length-1; i++) {
				if(rv.length()!=0)
					rv+=".";
				rv+=pieces[i];
			}
		}catch(Exception e) {
			log.error("e:"+e);
		}
		return rv;	
	}
	
	public static String GetSuffix(String str) {
		String rv="";
		try {
			String[] pieces = str.split("\\.");
			if(pieces.length>1)
				rv=pieces[pieces.length-1];

		}catch(Exception e) {
			log.error("e:"+e);
		}
		return rv;
	}
}
