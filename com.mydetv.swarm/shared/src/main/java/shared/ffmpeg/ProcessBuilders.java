package shared.ffmpeg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessBuilders {

	private static final Logger log = LoggerFactory.getLogger(ProcessBuilders.class);

	public static void runPB(ProcessBuilder builder) {
		runPB(builder, null);
	}
	public static void runPB(ProcessBuilder builder, Logger plog) {
		builder.environment().put("PATH", "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin");
		builder.redirectErrorStream(true);

		Process process;
		
		try {
			process = builder.start();

			log.info("starting "+builder.command());

			final Thread reader = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String line="";
						try {
							while((line=readerOutput.readLine()) != null) {
								log.debug("line:"+line);
								if(plog!=null)
									plog.info(""+line);
							}
						} catch (IOException e) {
							log.error("exception reading "+e);
							if(plog!=null)
								plog.error(""+e);
						}
					} catch (Exception e) {
						if(plog!=null)
							plog.error(""+e);
						e.printStackTrace();
					}
				}
			});
			reader.start();
			reader.join();

		} catch (IOException e1) {
			if(plog!=null)
				plog.error(""+e1);
			log.error("e:"+e1);
		} catch (InterruptedException e) {
			if(plog!=null)
				plog.error(""+e);
			log.error("e:"+e);
		}

		log.info("done");		
	}
}
