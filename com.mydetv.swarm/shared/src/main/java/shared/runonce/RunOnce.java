package shared.runonce;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunOnce {

	private static final Logger log = LoggerFactory.getLogger(RunOnce.class);
	
	public static Integer countRunning(String main) {
		Integer countRunning=0;
		try {
			log.trace("check running: "+main);
			Process p = Runtime.getRuntime().exec("/usr/bin/jps -l");
			while(p.isAlive()) {}
	        InputStream in = p.getInputStream();
	        String jps = IOUtils.toString(in, "UTF-8");
	        //log.info("s:"+jps);
	        String[] lines = jps.split("\n");
	        for(String line : lines) {
	        	String[] parts = line.split(" ");
	        	log.trace("jpl line:"+line);
	        	if(parts.length==2 && parts[1].equals(main))
	        		countRunning++;
	        }
		} catch (IOException e) {
			log.error("Error:"+e);
		}
		return countRunning;
	}
}
