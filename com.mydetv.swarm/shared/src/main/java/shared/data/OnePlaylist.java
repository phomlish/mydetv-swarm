package shared.data;

public class OnePlaylist {

	public String plid;
	public String name;
	public Integer id;
	public Boolean playing = false;
	public OneSchedule os = null;		// if it is a schedule item
	public OneSchedule osNext = null;
	public OneSchedule osPrevious = null;
	public OnePlaylist opBefore = null;
	public OnePlaylist opAfter = null;
}
