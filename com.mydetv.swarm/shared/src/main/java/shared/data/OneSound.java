package shared.data;

import java.time.LocalDateTime;

public class OneSound {

	private Integer id;
	private String dn;
	private String fn;
	private String filetype;
	private int size;
	private Boolean verified;
	private LocalDateTime dtAdded;
	// constructor
	public OneSound() {
	}
	public OneSound(String dn, String fn, String ft, int size) {
		this.dn=dn;
		this.fn=fn;
		this.filetype=ft;
		this.size=size;	
	}
	// getters/setters
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public String getFn() {
		return fn;
	}
	public void setFn(String fn) {
		this.fn = fn;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
}
