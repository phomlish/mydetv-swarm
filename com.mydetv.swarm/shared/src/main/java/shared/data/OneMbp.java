package shared.data;

import java.time.LocalDateTime;

public class OneMbp {
	
	private Integer idMbp;
	private Integer idVideo;
	private String fn;
	private String fn2;
	private String fnOrig;
	private String fnVideo;
	private String details;
	private String notes;
	private LocalDateTime dtAdded;
	private String dvd;
	private String converted;
	private String edited;
	private String showdate;
	private Integer showset;

	// not in database, used for mbpsController
	private String cpn; // converted pathname
	
	// getters/setters
	public Integer getIdMbp() {
		return idMbp;
	}
	public void setIdMbp(Integer idMbp) {
		this.idMbp = idMbp;
	}
	public String getFn() {
		return fn;
	}
	public void setFn(String fn) {
		this.fn = fn;
	}
	public String getFn2() {
		return fn2;
	}
	public void setFn2(String fn2) {
		this.fn2 = fn2;
	}
	public String getConverted() {
		return converted;
	}
	public void setConverted(String converted) {
		this.converted = converted;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public String getDvd() {
		return dvd;
	}
	public void setDvd(String dvd) {
		this.dvd = dvd;
	}
	public String getEdited() {
		return edited;
	}
	public void setEdited(String edited) {
		this.edited = edited;
	}
	public String getFnOrig() {
		return fnOrig;
	}
	public void setFnOrig(String fnOrig) {
		this.fnOrig = fnOrig;
	}
	public Integer getShowset() {
		return showset;
	}
	public void setShowset(Integer showset) {
		this.showset = showset;
	}
	public String getShowdate() {
		return showdate;
	}
	public void setShowdate(String showdate) {
		this.showdate = showdate;
	}
	public Integer getIdVideo() {
		return idVideo;
	}
	public void setIdVideo(Integer idVideo) {
		this.idVideo = idVideo;
	}
	public String getFnVideo() {
		return fnVideo;
	}
	public void setFnVideo(String fnVideo) {
		this.fnVideo = fnVideo;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCpn() {
		return cpn;
	}
	public void setCpn(String cpn) {
		this.cpn = cpn;
	}
	
}
