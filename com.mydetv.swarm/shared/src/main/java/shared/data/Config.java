package shared.data;

import java.util.ArrayList;
import java.util.List;

import shared.data.broadcast.StunServer;

public class Config {
	
	private String configDir;
	private String rootDir;
	private String instance;
	private String machineName;
	private String tmpDir;
	
	private String version;
	private String hostname;
	private Integer webPort;
	
	private OneDbConfig dbConfig;
	
	private String keystoreFile;
	private String keystorePassword;
	private String truststoreFile;
	private String truststorePassword;
	
	private String apiKey; // used for process communication (like schedule calling broadcast)
	
	private String smtpHost;
	private Integer smtpPort;
	private String smtpFrom;
	
	private String videosDirectory;
	private String videosConvertedDirectory;
	private String externalWebDirectory;
	private String geoip;
	private String matomoUrl;
	private String matomoKey;
	private Integer matomoSite;
	
	// broadcast
	private String company;
	private String secretKey; // used for encrypting
	private Integer lockSystemFailedAttempts;
	
	private String janusHost;
	private String janusUrl;
	private String janusApiSecret;
	private String janusAdminUrl;
	private String janusAdminSecret;
	
	private String ffmpeg;	
	private String gstLaunch;
	
	private Integer maxConnectionsTier1;
	private Integer maxConnections;
	
	private Integer masterServerPort;
	private String jukeboxUrl;
	private boolean doBandwidth=false;
	
	private String soundDirectory;

	private List<StunServer> stunServers = new ArrayList<StunServer>();
		
	// maint
	private String ffprobe; // maint
	private Integer category;
	
	private MaintConfig maint;
	
	// getters.setters
	public OneDbConfig getDbConfig() {
		return dbConfig;
	}
	public void setDbConfig(OneDbConfig dbConfig) {
		this.dbConfig = dbConfig;
	}
	public String getRootDir() {
		return rootDir;
	}
	public void setRootDir(String rootDir) {
		this.rootDir = rootDir;
	}
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTruststoreFile() {
		return truststoreFile;
	}
	public void setTruststoreFile(String truststoreFile) {
		this.truststoreFile = truststoreFile;
	}
	public String getTruststorePassword() {
		return truststorePassword;
	}
	public void setTruststorePassword(String truststorePassword) {
		this.truststorePassword = truststorePassword;
	}
	public String getKeystoreFile() {
		return keystoreFile;
	}
	public void setKeystoreFile(String keystoreFile) {
		this.keystoreFile = keystoreFile;
	}
	public String getKeystorePassword() {
		return keystorePassword;
	}
	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}
	public String getConfigDir() {
		return configDir;
	}
	public void setConfigDir(String configDir) {
		this.configDir = configDir;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public String getTmpDir() {
		return tmpDir;
	}
	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public Integer getWebPort() {
		return webPort;
	}
	public void setWebPort(Integer webPort) {
		this.webPort = webPort;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public Integer getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getSmtpFrom() {
		return smtpFrom;
	}
	public void setSmtpFrom(String smtpFrom) {
		this.smtpFrom = smtpFrom;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public Integer getLockSystemFailedAttempts() {
		return lockSystemFailedAttempts;
	}
	public void setLockSystemFailedAttempts(Integer lockSystemFailedAttempts) {
		this.lockSystemFailedAttempts = lockSystemFailedAttempts;
	}
	public String getJanusHost() {
		return janusHost;
	}
	public void setJanusHost(String janusHost) {
		this.janusHost = janusHost;
	}
	public String getJanusUrl() {
		return janusUrl;
	}
	public void setJanusUrl(String janusUrl) {
		this.janusUrl = janusUrl;
	}
	public String getJanusAdminUrl() {
		return janusAdminUrl;
	}
	public void setJanusAdminUrl(String janusAdminUrl) {
		this.janusAdminUrl = janusAdminUrl;
	}
	public String getJanusApiSecret() {
		return janusApiSecret;
	}
	public void setJanusApiSecret(String janusApiSecret) {
		this.janusApiSecret = janusApiSecret;
	}
	public String getFfmpeg() {
		return ffmpeg;
	}
	public void setFfmpeg(String ffmpeg) {
		this.ffmpeg = ffmpeg;
	}
	public String getJanusAdminSecret() {
		return janusAdminSecret;
	}
	public void setJanusAdminSecret(String janusAdminSecret) {
		this.janusAdminSecret = janusAdminSecret;
	}
	public String getGstLaunch() {
		return gstLaunch;
	}
	public void setGstLaunch(String gstLaunch) {
		this.gstLaunch = gstLaunch;
	}
	public Integer getMaxConnectionsTier1() {
		return maxConnectionsTier1;
	}
	public void setMaxConnectionsTier1(Integer maxConnectionsTier1) {
		this.maxConnectionsTier1 = maxConnectionsTier1;
	}
	public Integer getMaxConnections() {
		return maxConnections;
	}
	public void setMaxConnections(Integer maxConnections) {
		this.maxConnections = maxConnections;
	}
	public Integer getMasterServerPort() {
		return masterServerPort;
	}
	public void setMasterServerPort(Integer masterServerPort) {
		this.masterServerPort = masterServerPort;
	}
	public String getJukeboxUrl() {
		return jukeboxUrl;
	}
	public void setJukeboxUrl(String jukeboxUrl) {
		this.jukeboxUrl = jukeboxUrl;
	}
	public boolean isDoBandwidth() {
		return doBandwidth;
	}
	public void setDoBandwidth(boolean doBandwidth) {
		this.doBandwidth = doBandwidth;
	}
	public List<StunServer> getStunServers() {
		return stunServers;
	}
	public void setStunServers(List<StunServer> stunServers) {
		this.stunServers = stunServers;
	}
	public String getFfprobe() {
		return ffprobe;
	}
	public void setFfprobe(String ffprobe) {
		this.ffprobe = ffprobe;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getVideosDirectory() {
		return videosDirectory;
	}
	public void setVideosDirectory(String videosDirectory) {
		this.videosDirectory = videosDirectory;
	}
	public String getExternalWebDirectory() {
		return externalWebDirectory;
	}
	public void setExternalWebDirectory(String externalWebDirectory) {
		this.externalWebDirectory = externalWebDirectory;
	}
	public String getVideosConvertedDirectory() {
		return videosConvertedDirectory;
	}
	public void setVideosConvertedDirectory(String videosConvertedDirectory) {
		this.videosConvertedDirectory = videosConvertedDirectory;
	}
	public Integer getMatomoSite() {
		return matomoSite;
	}
	public void setMatomoSite(Integer matomoSite) {
		this.matomoSite = matomoSite;
	}
	public String getGeoip() {
		return geoip;
	}
	public void setGeoip(String geoip) {
		this.geoip = geoip;
	}
	public String getMatomoUrl() {
		return matomoUrl;
	}
	public void setMatomoUrl(String matomoUrl) {
		this.matomoUrl = matomoUrl;
	}
	public String getMatomoKey() {
		return matomoKey;
	}
	public void setMatomoKey(String matomoKey) {
		this.matomoKey = matomoKey;
	}
	public MaintConfig getMaint() {
		return maint;
	}
	public void setMaint(MaintConfig maint) {
		this.maint = maint;
	}
	public String getSoundDirectory() {
		return soundDirectory;
	}
	public void setSoundDirectory(String soundDirectory) {
		this.soundDirectory = soundDirectory;
	}

}
