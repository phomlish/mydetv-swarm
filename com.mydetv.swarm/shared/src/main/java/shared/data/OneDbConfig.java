package shared.data;

import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;

public class OneDbConfig {
	
	private String dbUrl;
	private String dbUsername;
	private String dbPassword;
	private String dbDriver;
	private boolean useSsl = true;
	private SharedPoolDataSource sharedPoolDataSource=null;
	
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getDbUsername() {
		return dbUsername;
	}
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getDbDriver() {
		return dbDriver;
	}
	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}
	public SharedPoolDataSource getSharedPoolDataSource() {
		return sharedPoolDataSource;
	}
	public void setSharedPoolDataSource(SharedPoolDataSource sharedPoolDataSource) {
		this.sharedPoolDataSource = sharedPoolDataSource;
	}
	public boolean useSsl() {
		return useSsl;
	}
	public void setUseSsl(boolean useSsl) {
		this.useSsl = useSsl;
	}

}
