package shared.data;

import java.time.LocalDateTime;

/**
 * DateTime: joda datetime<br>
 * @author phomlish
 *
 */
public class OneSchedule {
	
	private Integer idschedule;
	private Integer channel;
	private Integer pkey;
	private LocalDateTime dt;
	// used while we are scheduling
	private LocalDateTime dtEnd;
	// custom
	public String log() {
		return "startDt:"+dt+",idSchedule:"+idschedule+",pkey:"+pkey+",channel:"+channel;
	}
	// getters/setters
	public Integer getIdschedule() {
		return idschedule;
	}
	public void setIdschedule(Integer idschedule) {
		this.idschedule = idschedule;
	}
	public LocalDateTime getDt() {
		return dt;
	}
	public void setDt(LocalDateTime dt) {
		this.dt = dt;
	}
	public Integer getPkey() {
		return pkey;
	}
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}
	public Integer getChannel() {
		return channel;
	}
	public void setChannel(Integer channel) {
		this.channel = channel;
	}
	public LocalDateTime getDtEnd() {
		return dtEnd;
	}
	public void setDtEnd(LocalDateTime dtEnd) {
		this.dtEnd = dtEnd;
	}
}
