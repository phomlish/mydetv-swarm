package shared.data;

public class MydetvChannelTypeEnum {

	public enum MydetvChannelType {
		REMOTE(0), SCHEDULED(1), MALAWI(2);
		
		private final Integer level;
		
	    private MydetvChannelType(Integer level) {
	        this.level = level;
	    }

	    public Integer getLevel() {
	        return level;
	    }
	}
	
}
