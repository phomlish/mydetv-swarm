package shared.data;

import java.time.LocalDateTime;

public class OneVideo {

	// from db
	private Integer pkey;	
	private String fn;
	private String subdir;
	private String title;
	private Integer catSub;
	private LocalDateTime dtAdded;
	private Float length;
	private long filesize;	
	private String notes;
	private String thumbnail;	
	private LocalDateTime dtInactive;
	private Boolean doNotUse;	// these can be deleted from file system and database someday
	// *** used for specific activities	
	private Boolean isConverted;	
	private VideoType videoType; // used for scheduled
	private LocalDateTime lastScheduled;  // remember the last time the video was scheduled (relevant for a channel only)
	private LocalDateTime prevLastScheduled; // just remember this for cool logging
	// used for maint walking the filesystem
	private boolean found=false;		
	
	// custom
	public String log() {
		String rv="pkey:"+pkey+","+subdir+"/"+fn+",title:"+title+",dtAdded:"+dtAdded+",length:"+length;
		int seconds = (int) (length+0.9999999999);
		int hours = seconds/(60*60);
		rv+=" = "+hours+" hours ";
		seconds = seconds-(hours*60*60);
		int minutes = seconds/60;
		rv+=minutes+" minutes ";
		seconds = seconds-(minutes*60);
		rv+=seconds+" seconds";
		rv+="";
		return rv;
	}	
	// getters/setters
	public Integer getPkey() {
		return pkey;
	}
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}
	public String getFn() {
		return fn;
	}
	public void setFn(String fn) {
		this.fn = fn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public Float getLength() {
		return length;
	}
	public void setLength(Float length) {
		this.length = length;
	}
	public String getSubdir() {
		return subdir;
	}
	public void setSubdir(String subdir) {
		this.subdir = subdir;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public long getFilesize() {
		return filesize;
	}
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	public Integer getCatSub() {
		return catSub;
	}
	public void setCatSub(Integer catSub) {
		this.catSub = catSub;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public VideoType getVideoType() {
		return videoType;
	}
	public void setVideoType(VideoType videoType) {
		this.videoType = videoType;
	}
	public LocalDateTime getLastScheduled() {
		return lastScheduled;
	}
	public void setLastScheduled(LocalDateTime lastScheduled) {
		this.lastScheduled = lastScheduled;
	}
	public LocalDateTime getPrevLastScheduled() {
		return prevLastScheduled;
	}
	public void setPrevLastScheduled(LocalDateTime prevLastScheduled) {
		this.prevLastScheduled = prevLastScheduled;
	}
	public boolean isFound() {
		return found;
	}
	public void setFound(boolean found) {
		this.found = found;
	}
	public Boolean isDoNotUse() {
		return doNotUse;
	}
	public void setDoNotUse(Boolean doNotUse) {
		this.doNotUse = doNotUse;
	}
	public Boolean isConverted() {
		return isConverted;
	}
	public void setIsConverted(Boolean isConverted) {
		this.isConverted = isConverted;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
}
