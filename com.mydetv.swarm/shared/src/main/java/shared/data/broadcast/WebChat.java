package shared.data.broadcast;

import java.time.LocalDateTime;

public class WebChat {
	
	private Integer chatId;
	private Integer wuid;
	private String message;
	private LocalDateTime dtSent;
	private Integer channelId;
	private Boolean isPrivate;
	private String username;
	
	// getters/setters
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getChatId() {
		return chatId;
	}
	public void setChatId(Integer chatId) {
		this.chatId = chatId;
	}
	public Integer getWuid() {
		return wuid;
	}
	public void setWuid(Integer wuid) {
		this.wuid = wuid;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public Boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public LocalDateTime getDtSent() {
		return dtSent;
	}
	public void setDtSent(LocalDateTime dtSent) {
		this.dtSent = dtSent;
	}	
}
