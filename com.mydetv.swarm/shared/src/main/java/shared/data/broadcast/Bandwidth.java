package shared.data.broadcast;

import java.time.LocalDateTime;

/** 
 * one bandwidth test.<br>
 * each instance will exist in toUser's bandwidthTestsToMe and fromUser's bandwidthTestsFromMe<br>
 * state:<br>
 *     created: created the test but not started<br>
 *     inprogress: working<br>
 *     finished: done<br>
 *     failed: for whatever reason<br>
 */
public class Bandwidth {

	private RtcConnection rtcConnection;
	// seems hard to remove CGBandwidth atm, maybe look again someday
	private LocalDateTime lastProgressInstant;
	private String state; 
	private Long size;
	private Long dtDiff = 0L;
	private boolean active;
	
	public Bandwidth(RtcConnection rtc) {
		rtcConnection=rtc;
		state="created";
		lastProgressInstant = LocalDateTime.now();
		active=false;
		String cid=rtcConnection.getRtcUUID();
		rtc.getToUser().getBandwidthTestsToMe().put(cid, this);
		rtc.getFromUser().getBandwidthTestsFromMe().put(cid, this);
	}
	// custom 
	public String log() {
		if(rtcConnection==null)
			return "no webrtcConnection";
			
		if(rtcConnection.getFromUser() == null || rtcConnection.getToUser()==null) 
			return "users not set";
		
		String rv = "";
		rv+="from:"+rtcConnection.getFromUser().log();
		rv+=",to:"+rtcConnection.getToUser().log();
		rv+="state:"+state;
		rv+=",active:"+active;
		return rv;
	}
	// getters/setters
	public LocalDateTime getLastProgressInstant() {
		return lastProgressInstant;
	}
	public void setLastProgressInstant(LocalDateTime lastProgressInstant) {
		this.lastProgressInstant = lastProgressInstant;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public RtcConnection getRtcConnection() {
		return rtcConnection;
	}
	public void setRtcConnection(RtcConnection rtcConnection) {
		this.rtcConnection = rtcConnection;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public Long getDtDiff() {
		return dtDiff;
	}
	public void setDtDiff(Long dtDiff) {
		this.dtDiff = dtDiff;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
