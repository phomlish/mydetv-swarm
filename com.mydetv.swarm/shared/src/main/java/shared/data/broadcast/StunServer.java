package shared.data.broadcast;

public class StunServer {

	private String ip;
	private Integer port;
	private String nickname;
	
	public StunServer(String ip, Integer port, String nickname) {
		this.ip=ip;
		this.port=port;
		this.nickname=nickname;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
