package shared.data.broadcast;

public class JanusStats {

	private Integer channelWebUsersId;
	private String state;
	private String rtcpStats;
	private String inStats;
	private String outStats;
	
	// getters/setters
	public Integer getChannelWebUsersId() {
		return channelWebUsersId;
	}
	public void setChannelWebUsersId(Integer channelWebUsersId) {
		this.channelWebUsersId = channelWebUsersId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRtcpStats() {
		return rtcpStats;
	}
	public void setRtcpStats(String rtcpStats) {
		this.rtcpStats = rtcpStats;
	}
	public String getInStats() {
		return inStats;
	}
	public void setInStats(String inStats) {
		this.inStats = inStats;
	}
	public String getOutStats() {
		return outStats;
	}
	public void setOutStats(String outStats) {
		this.outStats = outStats;
	}
}
