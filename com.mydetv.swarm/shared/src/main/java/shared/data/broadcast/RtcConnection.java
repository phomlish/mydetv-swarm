package shared.data.broadcast;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * represents a two way connection<br>
 * could be a bandwidth test (data channel) or a video connection<br>
 * fromUser: will always create the offer<br>
 * toUser: will always create the answer<br>
 * <br>
 * See ConnectionGroups.ods for more details<br>
 * @author phomlish
 *
 */
public class RtcConnection {
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(RtcConnection.class);
	
	private String rtcUUID;
	private SessionUser toUser;
	private SessionUser fromUser;	
	private String connectionType;
	
	private LocalDateTime created;
	private LocalDateTime started;
	private Boolean connected;
	private Boolean removed;
	private Boolean errored;
	private List<LocalDateTime> failed;
	
	// used for Janus
	private Long handleId;
	private Long privateId;
	
	// this is deprecated as all browsers now do trickle ice
	private JSONObject offer;
	private JSONObject answer;
	

	private TreeMap<LocalDateTime, String> states = new TreeMap<LocalDateTime, String>();
	
	// creator
	public RtcConnection(String connectionType, SessionUser fromUser, SessionUser toUser) {
		this.connectionType=connectionType;
		this.rtcUUID=UUID.randomUUID().toString();
		this.handleId=0L;
		this.privateId=0L;
		this.fromUser=fromUser;
		this.toUser=toUser;
		//toUser.getRtcConnections().put(rtcUUID, this);
		//fromUser.getRtcConnections().put(rtcUUID, this);
		states.put(LocalDateTime.now(), "created");
		created=LocalDateTime.now();
		setConnected(false);
		setRemoved(false);
		failed = new ArrayList<LocalDateTime>();
		errored=false;
	}
	// custom 
	public SessionUser getOther(SessionUser sessionUser) {
		if(sessionUser==toUser)
			return fromUser;
		else
			return toUser;
	}
	// custom log
	public String log() {
		String rv = "rtcConnection ";
		rv+="created:"+created.toString();
		rv+=",toUser:";
		if(toUser!=null && toUser.getWebUser()!=null) 
			rv+=toUser.log();
		else
			rv+="null";
		
		rv+=",fromUser:";
		if(fromUser!=null&&toUser.getWebUser()!=null)
			rv+=fromUser.log();
		else
			rv+="null";
		rv+=",connectionType:"+connectionType;
		rv+=",connected:"+connected;
		rv+=",removed:"+removed;
		rv+=",failed:"+failed.size();
		rv+=",cid:"+rtcUUID;
		return rv;
	}
	
	// getters/setters
	public String getRtcUUID() {
		return rtcUUID;
	}
	public SessionUser getToUser() {
		return toUser;
	}
	public void setToUser(SessionUser toUser) {
		this.toUser = toUser;
	}
	public SessionUser getFromUser() {
		return fromUser;
	}
	public void setFromUser(SessionUser fromUser) {
		this.fromUser = fromUser;
	}
	public String getConnectionType() {
		return connectionType;
	}
	//public void setConnectionType(String type) {
	//	this.connectionType = type;
	//}
	public JSONObject getOffer() {
		return offer;
	}
	public void setOffer(JSONObject sdp) {
		this.offer = sdp;
	}
	public JSONObject getAnswer() {
		return answer;
	}
	public void setAnswer(JSONObject sdp) {
		this.answer = sdp;
	}
	public Long getHandleId() {
		return handleId;
	}
	public void setHandleId(Long handleId) {
		this.handleId = handleId;
	}
	public Long getPrivateId() {
		return privateId;
	}
	public void setPrivateId(Long privateId) {
		this.privateId = privateId;
	}

	public void addState(String state) {
		states.put(LocalDateTime.now(), state);
		if(state.equals("connected"))
			connected=true;
		if(state.contains("removed")) {
			connected=false;
			removed=true;
		}
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public Boolean getConnected() {
		return connected;
	}
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}
	public TreeMap<LocalDateTime, String> getStates() {
		return states;
	}
	public void setStates(TreeMap<LocalDateTime, String> states) {
		this.states = states;
	}
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}
	public List<LocalDateTime> getFailed() {
		return failed;
	}
	public void setFailed(List<LocalDateTime> failed) {
		this.failed = failed;
	}
	public Boolean getErrored() {
		return errored;
	}
	public void setErrored(Boolean errored) {
		this.errored = errored;
	}
	public LocalDateTime getStarted() {
		return started;
	}
	public void setStarted(LocalDateTime started) {
		this.started = started;
	}

}
