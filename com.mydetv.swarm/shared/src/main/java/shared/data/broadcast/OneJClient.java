package shared.data.broadcast;

public class OneJClient {
	
	private Object jClient;
	private Integer janusId;
	private Integer janusAudioPort;
	private Integer janusVideoPort;
	private String janusSecret;
	private String janusAdminKey;
	
	// getters/setters
	public Object getjClient() {
		return jClient;
	}
	public void setjClient(Object jClient) {
		this.jClient = jClient;
	}
	public Integer getJanusId() {
		return janusId;
	}
	public void setJanusId(Integer janusId) {
		this.janusId = janusId;
	}
	public Integer getJanusAudioPort() {
		return janusAudioPort;
	}
	public void setJanusAudioPort(Integer janusAudioPort) {
		this.janusAudioPort = janusAudioPort;
	}
	public Integer getJanusVideoPort() {
		return janusVideoPort;
	}
	public void setJanusVideoPort(Integer janusVideoPort) {
		this.janusVideoPort = janusVideoPort;
	}
	public String getJanusSecret() {
		return janusSecret;
	}
	public void setJanusSecret(String janusSecret) {
		this.janusSecret = janusSecret;
	}
	public String getJanusAdminKey() {
		return janusAdminKey;
	}
	public void setJanusAdminKey(String janusAdminKey) {
		this.janusAdminKey = janusAdminKey;
	}

}
