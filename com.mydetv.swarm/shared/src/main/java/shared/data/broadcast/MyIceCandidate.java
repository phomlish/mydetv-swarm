package shared.data.broadcast;

public class MyIceCandidate {

	private String candidate;
	private String sdpMid;
	private Integer sdpMLineIndex;
	
	public MyIceCandidate(String candidate, String sdpMid, Integer sdpMLineIndex) {
		this.candidate = candidate;
		this.sdpMid = sdpMid;
		this.sdpMLineIndex = sdpMLineIndex;
	}
	// getters/setters
	public String getCandidate() {
		return candidate;
	}
	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}
	public String getSdpMid() {
		return sdpMid;
	}
	public void setSdpMid(String sdpMid) {
		this.sdpMid = sdpMid;
	}
	public Integer getSdpMLineIndex() {
		return sdpMLineIndex;
	}
	public void setSdpMLineIndex(Integer sdpMLineIndex) {
		this.sdpMLineIndex = sdpMLineIndex;
	}
}
