package shared.data.broadcast;

import java.net.InetAddress;
import java.time.LocalDateTime;

/**
 * FaultyConnection<br>
 * reason: failed or slow<br>
 * <pre>
 *  failed:
 *   from bw test
 *   from connection
 *  slow
 *    currently only from media player
 *    need to write browser stats to get these for p2p or u2u user to user video
 *    
 * </pre>
 * @author phomlish
 *
 */
public class FaultyConnection {

	private LocalDateTime dt;
	private String connectionType; // whatever rtc said
	private String reason; // iceFailed or slow
	
	private boolean fromIsMediaPlayer;
	private Integer fromWuid;
	private Integer fromDbChannelWebUsersId;
	private String fromIp;
	private Integer fromPort;
	private InetAddress fromIpAddress;
	
	private boolean toIsMediaPlayer;
	private Integer toWuid;
	private Integer toDbChannelWebUsersId;
	private String toIp;
	private Integer toPort;
	private InetAddress toIpAddress;	
	// used for slowlinks
	private Integer nacks;
	//creators
	public FaultyConnection() {
		
	}	
	// getters/setters
	public LocalDateTime getDt() {
		return dt;
	}
	public void setDt(LocalDateTime dt) {
		this.dt = dt;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Integer getNacks() {
		return nacks;
	}
	public void setNaks(Integer nacks) {
		this.nacks = nacks;
	}
	public boolean isFromIsMediaPlayer() {
		return fromIsMediaPlayer;
	}
	public void setFromIsMediaPlayer(boolean fromIsMediaPlayer) {
		this.fromIsMediaPlayer = fromIsMediaPlayer;
	}
	public boolean isToIsMediaPlayer() {
		return toIsMediaPlayer;
	}
	public void setToIsMediaPlayer(boolean toIsMediaPlayer) {
		this.toIsMediaPlayer = toIsMediaPlayer;
	}
	public Integer getFromWuid() {
		return fromWuid;
	}
	public void setFromWuid(Integer fromWuid) {
		this.fromWuid = fromWuid;
	}
	public Integer getFromDbChannelWebUsersId() {
		return fromDbChannelWebUsersId;
	}
	public void setFromDbChannelWebUsersId(Integer fromDbChannelWebUsersId) {
		this.fromDbChannelWebUsersId = fromDbChannelWebUsersId;
	}
	public String getFromIp() {
		return fromIp;
	}
	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}
	public Integer getFromPort() {
		return fromPort;
	}
	public void setFromPort(Integer fromPort) {
		this.fromPort = fromPort;
	}
	public InetAddress getFromIpAddress() {
		return fromIpAddress;
	}
	public void setFromIpAddress(InetAddress fromIpAddress) {
		this.fromIpAddress = fromIpAddress;
	}
	public Integer getToWuid() {
		return toWuid;
	}
	public void setToWuid(Integer toWuid) {
		this.toWuid = toWuid;
	}
	public Integer getToDbChannelWebUsersId() {
		return toDbChannelWebUsersId;
	}
	public void setToDbChannelWebUsersId(Integer toDbChannelWebUsersId) {
		this.toDbChannelWebUsersId = toDbChannelWebUsersId;
	}
	public String getToIp() {
		return toIp;
	}
	public void setToIp(String toIp) {
		this.toIp = toIp;
	}
	public Integer getToPort() {
		return toPort;
	}
	public void setToPort(Integer toPort) {
		this.toPort = toPort;
	}
	public InetAddress getToIpAddress() {
		return toIpAddress;
	}
	public void setToIpAddress(InetAddress toIpAddress) {
		this.toIpAddress = toIpAddress;
	}
}
