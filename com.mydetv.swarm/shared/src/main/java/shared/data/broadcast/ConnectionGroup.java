package shared.data.broadcast;

import java.util.ArrayList;
import java.util.List;

/**
 *     See ConnectionGroups.ods for more details<br>
 */
public class ConnectionGroup {
	
	private List<RtcConnection> upstreams;
	// who am I providing streams to
	private List<RtcConnection> downstreams;
	private List<FaultyConnection> faultyConnecion;
	// presenter can be a user or media player user
	private SessionUser provider;
	// tier has become our streaming holy grail.  -1 for needs connection, 0 for presenter, 1 for media server
	private String cgType;
	private Integer tier;
	private boolean waiting;
	
	public ConnectionGroup(String cgType, SessionUser su) {
		this.cgType=cgType;
		this.provider = su;
		this.downstreams = new ArrayList<RtcConnection>();
		this.upstreams = new ArrayList<RtcConnection>();
		this.faultyConnecion = new ArrayList<FaultyConnection>();
		this.tier=-1;
		this.waiting=false;
	}
	
	// getters/setters
	public Integer getTier() {
		return tier;
	}
	public void setTier(Integer tier) {
		this.tier = tier;
	}
	public SessionUser getProvider() {
		return provider;
	}
	public void setProvider(SessionUser provider) {
		this.provider = provider;
	}
	public List<RtcConnection> getDownstreams() {
		return downstreams;
	}
	public void setDownstreams(List<RtcConnection> downstreams) {
		this.downstreams = downstreams;
	}
	public String getCgType() {
		return cgType;
	}
	public void setCgType(String cgType) {
		this.cgType = cgType;
	}
	public boolean isWaiting() {
		return waiting;
	}
	public void setWaiting(boolean waiting) {
		this.waiting = waiting;
	}
	public List<RtcConnection> getUpstreams() {
		return upstreams;
	}
	public void setUpstreams(List<RtcConnection> upstreams) {
		this.upstreams = upstreams;
	}
	public List<FaultyConnection> getFaultyConnecion() {
		return faultyConnecion;
	}
	public void setFaultyConnecion(List<FaultyConnection> faultyConnecion) {
		this.faultyConnecion = faultyConnecion;
	}
}
