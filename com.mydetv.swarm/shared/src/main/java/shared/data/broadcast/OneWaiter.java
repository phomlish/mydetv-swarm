package shared.data.broadcast;

import java.time.LocalDateTime;

import shared.data.OneMydetvChannel;

public class OneWaiter {

	private LocalDateTime waiterKey;
	private SessionUser sessionUser;
	private OneMydetvChannel channel;
	private Integer quePosition;
	private Integer queSize;
	
	public OneWaiter(OneMydetvChannel channel, SessionUser sessionUser) {
		this.channel = channel;
		this.sessionUser=sessionUser;
		this.waiterKey = LocalDateTime.now();
	}
	public SessionUser getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(SessionUser sessionUser) {
		this.sessionUser = sessionUser;
	}
	public OneMydetvChannel getChannel() {
		return channel;
	}
	public void setChannel(OneMydetvChannel channel) {
		this.channel = channel;
	}
	public Integer getQuePosition() {
		return quePosition;
	}
	public void setQuePosition(Integer quePosition) {
		this.quePosition = quePosition;
	}
	public Integer getQueSize() {
		return queSize;
	}
	public void setQueSize(Integer queSize) {
		this.queSize = queSize;
	}
	public LocalDateTime getWaiterKey() {
		return waiterKey;
	}
	public void setWaiterKey(LocalDateTime waiterKey) {
		this.waiterKey = waiterKey;
	}
	
}
