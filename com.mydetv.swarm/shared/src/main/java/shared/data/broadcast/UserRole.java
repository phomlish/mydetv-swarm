package shared.data.broadcast;

import shared.data.webuser.WebUserRoleTypesEnum;

public class UserRole {
	private WebUserRoleTypesEnum userRoleType;

	// getters/setters
	public WebUserRoleTypesEnum getUserRoleType() {
		return userRoleType;
	}

	public void setUserRoleType(WebUserRoleTypesEnum userRoleType) {
		this.userRoleType = userRoleType;
	}
}
