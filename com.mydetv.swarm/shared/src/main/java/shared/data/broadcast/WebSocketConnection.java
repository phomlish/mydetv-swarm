package shared.data.broadcast;

import java.time.LocalDateTime;

public class WebSocketConnection {

	private Object webSocketSession;
	private Integer hashcode;
	private LocalDateTime lastPing;
	private LocalDateTime connectedDate;
	private SessionUser sessionUser;
	private String remote;
	
	// creator
	public WebSocketConnection() {
		lastPing=LocalDateTime.now();
		hashcode=0;
	}
	// getters/setters
	public Object getWebsocketSession() {
		return webSocketSession;
	}
	public void setWebsocketSession(Object webSocketSession) {
		this.webSocketSession = webSocketSession;
	}
	public SessionUser getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(SessionUser sessionUser) {
		this.sessionUser = sessionUser;
	}
	public LocalDateTime getConnectedDate() {
		return connectedDate;
	}
	public void setConnectedDate(LocalDateTime connectedDate) {
		this.connectedDate = connectedDate;
	}

	public Integer getHashcode() {
		return hashcode;
	}

	public void setHashcode(Integer hashcode) {
		this.hashcode = hashcode;
	}
	public String getRemote() {
		return remote;
	}
	public void setRemote(String remote) {
		this.remote = remote;
	}
	public LocalDateTime getLastPing() {
		return lastPing;
	}
	public void setLastPing(LocalDateTime lastPing) {
		this.lastPing = lastPing;
	}
}
