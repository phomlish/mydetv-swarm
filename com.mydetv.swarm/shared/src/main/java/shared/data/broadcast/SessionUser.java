package shared.data.broadcast;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.UUID;

import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import shared.data.OneMydetvChannel;
import shared.data.webuser.OneWebUser;

public class SessionUser {

	//private static final Logger log = LoggerFactory.getLogger(SessionUser.class);
	
	private Object lock = new Object();
	private String mydetv;
	private String trackingCode;
	private String theme;
	private OneWebUser webUser; // populated when logged in.  Set to null to log them out.
	private Boolean mediaPlayer=false;  // is this the dummy media player user?
	private String browserTimezone;
	
	// when they last did something like clicked on a webpage
	// we don't remove them from blackboard.sessionUsers for a while
	private LocalDateTime lastSeen;
	// where they came from
	private String ip; // deprecate (deprecate why?)
	private Integer port;
	private String ipport;
	
	// THESE ARE ALL ABOUT ENTERING A ROOM AND OBTAINING A WEBSOCKET CONNECTION
	// remember his websocketConnection when he enters a room
	private WebSocketConnection webSocketConnection;
	// a web session can only be in one channel at a time. 
	private OneMydetvChannel mydetvChannel;
	
	private JSONObject localVideo = null;
	// Redundant, use cgBroadcaster instead! This user is sending us live video 
	//private boolean broadcastingLive=false;
	// This user is sitting in a Broadcasting room
	private Boolean inBroadcastingRoom = false;
	// is he sitting in a broadcast page as an authorized broadcaster?
	private OneMydetvChannel broadcastChannel;
	// remember when he entered the room so we know how to prioritize new viewers
	private LocalDateTime dtEnteredRoom;

	////DetectRTC={"browser":{"fullVersion":"54.0","version":54,"name":"Firefox","isPrivateBrowsing":false,"isFirefox":true},"osName":"Mac OS X","osVersion":"10.12","isWebRTCSupported":true,"isORTCSupported":false,"isScreenCapturingSupported":true,"isAudioContextSupported":true,"isCreateMediaStreamSourceSupported":true,"isRtpDataChannelsSupported":false,"isSctpDataChannelsSupported":true,"isMobileDevice":false,"isGetUserMediaSupported":true,"displayResolution":"1440 x 900","isCanvasSupportsStreamCapturing":true,"isVideoSupportsStreamCapturing":true,"isWebSocketsSupported":true,"isWebSocketsBlocked":false,"MediaDevices":[],"hasMicrophone":false,"hasSpeakers":false,"hasWebcam":false,"isWebsiteHasWebcamPermissions":false,"isWebsiteHasMicrophonePermissions":false,"audioInputDevices":[],"audioOutputDevices":[],"videoInputDevices":[],"isSetSinkIdSupported":false,"isRTPSenderReplaceTracksSupported":true,"isRemoteStreamProcessingSupported":true,"isApplyConstraintsSupported":true,"isMultiMonitorScreenCapturingSupported":true,"isPromisesSupported":true,"MediaStream":["getAudioTracks","getVideoTracks","getTracks","getTrackById","addTrack","removeTrack","clone","id","active","onaddtrack","currentTime"],"MediaStreamTrack":["clone","stop","getConstraints","getSettings","applyConstraints","kind","id","label","enabled","readyState","onended"],"RTCPeerConnection":["setIdentityProvider","getIdentityAssertion","createOffer","createAnswer","setLocalDescription","setRemoteDescription","addIceCandidate","getConfiguration","getLocalStreams","getRemoteStreams","addStream","addTrack","removeTrack","getSenders","getReceivers","close","getStats","createDataChannel","localDescription","remoteDescription","signalingState","canTrickleIceCandidates","iceGatheringState","iceConnectionState","peerIdentity","idpLoginUrl","onnegotiationneeded","onicecandidate","onsignalingstatechange","onaddstream","onaddtrack","ontrack","onremovestream","oniceconnectionstatechange","onicegatheringstatechange","ondatachannel"]}}
	private JSONObject detectRTC;
	private Boolean goodWebrtc=false;			// analyzing detectRTC, can webrtc?
	private Boolean goodBroadcaster=false;		// analyzing detectRTC, can broadcast?
	
	// if he's watching a channel video he'll have stuff here
	private ConnectionGroup cgStream;
	// if he's broadcasting he'll have stuff here
	private ConnectionGroup cgBroadcaster;
	// if he's sending video to a user he'll have stuff here
	private ConnectionGroup cgU2U;
	
	private TreeMap<LocalDateTime, FaultyConnection> faultyConnections = new TreeMap<LocalDateTime, FaultyConnection> ();
	// save every bandwidth test we've ever done to this guy
	//    we can use them during reconnects AND when we get messages about the test status
	// key is the rtc uuid
	private HashMap<String, Bandwidth> bandwidthTestsToMe = new HashMap<String, Bandwidth>();
	private HashMap<String, Bandwidth> bandwidthTestsFromMe = new HashMap<String, Bandwidth>();
	// when a user enters a room, they download a datafile.  remember how long that took
	private Long datafileDownloadTime = 0L;
	// when he is waiting we need to remember how many waiters so we can display progress bars
	private Integer waiterQueueSize;
	private Integer waiterQueuePosition;

	public SessionUser(String ip, Integer port, String trackingCode) {
		this.mydetv = UUID.randomUUID().toString();
		this.waiterQueueSize=-1;
		this.cgU2U =  new ConnectionGroup("u2u", this);
		this.cgStream = new ConnectionGroup("stream", this);
		this.cgBroadcaster = new ConnectionGroup("broadcaster", this);
		this.lastSeen = LocalDateTime.now();
		this.trackingCode=trackingCode;
		this.ip=ip;
		this.port=port;
		this.ipport=ip+":"+port;
	}
	
	// custom getter/setters
	public String log() {
		String rv="";
		if(isLoggedIn())
			rv+=webUser.getUsername();
		else
			rv+="unknown";
		return rv;
	}
	/**
	 * is logged in (dbWebUserId>0) AND NOT requirePasswordChange
	 * @return
	 */
	public Boolean isAuthenticated() {
		//log.info("isAuthenticated:"+dbWebUserId);
		if(webUser == null)
			return false;
		if(! webUser.getRequirePasswordChange()) return true;
		return false;
	}
	/**
	 * is logged in (dbWebUserId>0)
	 * @return
	 */
	public Boolean isLoggedIn() {
		//log.info("isAuthenticated:"+dbWebUserId);
		if(webUser != null) return true;
		return false;
	}
	/*
	public RtcConnection findRtc(String cid) {
		if(rtcConnections.containsKey(cid))
			return rtcConnections.get(cid);
		log.error("Could not find rtc "+cid);
		for(RtcConnection rtc : rtcConnections.values())
			log.error("comparing "+cid+" to rtc "+rtc.getRtcUUID());
		return null;
	}
	*/
	
	public boolean isbroadcastingLive() {
		if(cgBroadcaster.getDownstreams().size()>0)
			return true;
		else
			return false;
	}
	
	//{"isPromisesSupported":true,"isWebSocketsSupported":true,"isCanvasSupportsStreamCapturing":true,"audioOutputDevices":[],"hasWebcam":true,"isMultiMonitorScreenCapturingSupported":true,"isWebSocketsBlocked":false,"isVideoSupportsStreamCapturing":true,"osVersion":"10.13","hasSpeakers":false,"browser":{"isFirefox":true,"fullVersion":"65.0","isPrivateBrowsing":false,"name":"Firefox","version":65},"isGetUserMediaSupported":true,"MediaDevices":[{"isCustomLabel":true,"kind":"videoinput","groupId":"","label":"Camera 1","id":"MN1w8vZlM2A1D73esKHji2KwNL2czPXZe4zMc9iFaHk=","deviceId":"MN1w8vZlM2A1D73esKHji2KwNL2czPXZe4zMc9iFaHk="},{"isCustomLabel":true,"kind":"audioinput","groupId":"","label":"Microphone 1","id":"3KOl5OGbYqSjWp6AHUTQvt3jJ8ZRxq1zSgBXP+rbwMI=","deviceId":"3KOl5OGbYqSjWp6AHUTQvt3jJ8ZRxq1zSgBXP+rbwMI="}],"isRemoteStreamProcessingSupported":true,"isWebsiteHasWebcamPermissions":false,"isMobileDevice":false,"isApplyConstraintsSupported":true,"displayResolution":"1440 x 900","isWebsiteHasMicrophonePermissions":false,"MediaStreamTrack":["clone","stop","getConstraints","getSettings","applyConstraints","kind","id","label","enabled","muted","onmute","onunmute","readyState","onended"],"isRTPSenderReplaceTracksSupported":true,"isScreenCapturingSupported":true,"isSctpDataChannelsSupported":true,"hasMicrophone":true,"osName":"Mac OS X","isWebRTCSupported":true,"version":"1.3.8","isORTCSupported":false,"displayAspectRatio":"1.60","isSetSinkIdSupported":false,"MediaStream":["getAudioTracks","getVideoTracks","getTracks","getTrackById","addTrack","removeTrack","clone","id","active","onaddtrack","onremovetrack"],"isRtpDataChannelsSupported":false,"RTCPeerConnection":["setIdentityProvider","getIdentityAssertion","createOffer","createAnswer","setLocalDescription","setRemoteDescription","addIceCandidate","getConfiguration","getLocalStreams","getRemoteStreams","addStream","addTrack","removeTrack","addTransceiver","getSenders","getReceivers","getTransceivers","close","getStats","createDataChannel","localDescription","currentLocalDescription","pendingLocalDescription","remoteDescription","currentRemoteDescription","pendingRemoteDescription","signalingState","canTrickleIceCandidates","iceGatheringState","iceConnectionState","peerIdentity","idpLoginUrl","onnegotiationneeded","onicecandidate","onsignalingstatechange","onaddstream","onaddtrack","ontrack","onremovestream","oniceconnectionstatechange","onicegatheringstatechange","ondatachannel","removeStream","addEventListener","removeEventListener","connectionState","onconnectionstatechange"],"isCreateMediaStreamSourceSupported":true,"audioInputDevices":[{"isCustomLabel":true,"kind":"audioinput","groupId":"","label":"Microphone 1","id":"3KOl5OGbYqSjWp6AHUTQvt3jJ8ZRxq1zSgBXP+rbwMI=","deviceId":"3KOl5OGbYqSjWp6AHUTQvt3jJ8ZRxq1zSgBXP+rbwMI="}],"videoInputDevices":[{"isCustomLabel":true,"kind":"videoinput","groupId":"","label":"Camera 1","id":"MN1w8vZlM2A1D73esKHji2KwNL2czPXZe4zMc9iFaHk=","deviceId":"MN1w8vZlM2A1D73esKHji2KwNL2czPXZe4zMc9iFaHk="}],"isAudioContextSupported":true}
	
	public boolean isFirefox() {
		JSONObject browser = detectRTC.getJSONObject("browser");
		if(browser.get("name") != null && browser.getString("name").equals("Firefox"))
			return true;
		return false;
	}
	public boolean isChrome() {
		JSONObject browser = detectRTC.getJSONObject("browser");
		if(browser.get("name") != null && browser.getString("name").equals("Chrome"))
			return true;
		return false;
	}
	public boolean haveCamera() {
		return detectRTC.getBoolean("hasWebcam");
	}
	
	// getters/setters
	public String getMydetv() {
		return mydetv;
	}
	public WebSocketConnection getWebSocketConnection() {
		return webSocketConnection;
	}
	public void setWebSocketConnection(WebSocketConnection webSocketConnection) {
		this.webSocketConnection = webSocketConnection;
	}
	public OneMydetvChannel getMydetvChannel() {
		return mydetvChannel;
	}
	public void setMydetvChannel(OneMydetvChannel mydetvChannel) {
		this.mydetvChannel = mydetvChannel;
	}
	public HashMap<String, Bandwidth> getBandwidthTestsToMe() {
		return bandwidthTestsToMe;
	}
	public HashMap<String, Bandwidth> getBandwidthTestsFromMe() {
		return bandwidthTestsFromMe;
	}
	public Long getDatafileDownloadTime() {
		return datafileDownloadTime;
	}
	public void setDatafileDownloadTime(Long datafileDownloadTime) {
		this.datafileDownloadTime = datafileDownloadTime;
	}
	public LocalDateTime getDtEnteredRoom() {
		return dtEnteredRoom;
	}
	public void setDtEnteredRoom(LocalDateTime dtEnteredRoom) {
		this.dtEnteredRoom = dtEnteredRoom;
	}
	public Object getLock() {
		return lock;
	}
	public OneMydetvChannel getBroadcastChannel() {
		return broadcastChannel;
	}
	public void setBroadcastChannel(OneMydetvChannel broadcastChannel) {
		this.broadcastChannel = broadcastChannel;
	}
	public Boolean isInBroadcastingRoom() {
		return inBroadcastingRoom;
	}
	public void setInBroadcastingRoom(Boolean inBroadcastingRoom) {
		this.inBroadcastingRoom = inBroadcastingRoom;
	}
	public JSONObject getDetectRTC() {
		return detectRTC;
	}
	public void setDetectRTC(JSONObject detectRTC) {
		this.detectRTC = detectRTC;
	}
	public Integer getWaiterQueueSize() {
		return waiterQueueSize;
	}
	public void setWaiterQueueSize(Integer waiterQueueSize) {
		this.waiterQueueSize = waiterQueueSize;
	}
	public Integer getWaiterQueuePosition() {
		return waiterQueuePosition;
	}
	public void setWaiterQueuePosition(Integer waiterQueuePosition) {
		this.waiterQueuePosition = waiterQueuePosition;
	}
	public ConnectionGroup getCgStream() {
		return cgStream;
	}
	public ConnectionGroup getCgBroadcaster() {
		return cgBroadcaster;
	}
	public ConnectionGroup getCgU2U() {
		return cgU2U;
	}
	public OneWebUser getWebUser() {
		return webUser;
	}
	public void setWebUser(OneWebUser webUser) {
		this.webUser = webUser;
	}
	public String getBrowserTimezone() {
		return browserTimezone;
	}
	public void setBrowserTimezone(String browserTimezone) {
		this.browserTimezone = browserTimezone;
	}
	public LocalDateTime getLastSeen() {
		return lastSeen;
	}
	public void setLastSeen(LocalDateTime lastSeen) {
		this.lastSeen = lastSeen;
	}
	public Boolean isMediaPlayer() {
		return mediaPlayer;
	}
	public void setMediaPlayer(Boolean mediaPlayer) {
		this.mediaPlayer = mediaPlayer;
	}
	public String getIpPort() {
		return ipport;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public JSONObject getLocalVideo() {
		return localVideo;
	}
	public void setLocalVideo(JSONObject localVideo) {
		this.localVideo = localVideo;
	}
	public Boolean getGoodWebrtc() {
		return goodWebrtc;
	}
	public void setGoodWebrtc(Boolean goodWebrtc) {
		this.goodWebrtc = goodWebrtc;
	}
	public Boolean getGoodBroadcaster() {
		return goodBroadcaster;
	}
	public void setGoodBroadcaster(Boolean goodBroadcaster) {
		this.goodBroadcaster = goodBroadcaster;
	}
	public String getTrackingCode() {
		return trackingCode;
	}
	public void setTrackingCode(String trackingCode) {
		this.trackingCode = trackingCode;
	}
	public TreeMap<LocalDateTime, FaultyConnection> getFaultyConnections() {
		return faultyConnections;
	}
	public void setFaultyConnections(TreeMap<LocalDateTime, FaultyConnection> faultyConnections) {
		this.faultyConnections = faultyConnections;
	}
	public String getIp() {
		return ip;
	}
	public Integer getPort() {
		return port;
	}
}
