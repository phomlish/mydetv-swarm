package shared.data;

import java.time.LocalDateTime;

public class OneArchive {

	private Integer idArchive;
	private Integer wuid;
	private String site;
	private String siteUrl;
	private LocalDateTime dtAdded;
	private LocalDateTime dtReceived;
	private String videoFn;
	private String videoPath;
	private long filesize;
	private String details;
	private String errorReason;
	private String thumbnail;
	private String pretitle;
	private String fnNoSuffix;  // only used for processing, not the database
	public Integer getIdArchive() {
		return idArchive;
	}
	public void setIdArchive(Integer idArchive) {
		this.idArchive = idArchive;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getSiteUrl() {
		return siteUrl;
	}
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public LocalDateTime getDtReceived() {
		return dtReceived;
	}
	public void setDtReceived(LocalDateTime dtReceived) {
		this.dtReceived = dtReceived;
	}
	public String getVideoFn() {
		return videoFn;
	}
	public void setVideoFn(String videoFn) {
		this.videoFn = videoFn;
	}
	public String getVideoPath() {
		return videoPath;
	}
	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}
	public long getFilesize() {
		return filesize;
	}
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public Integer getWuid() {
		return wuid;
	}
	public void setWuid(Integer wuid) {
		this.wuid = wuid;
	}
	public String getErrorReason() {
		return errorReason;
	}
	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getFnNoSuffix() {
		return fnNoSuffix;
	}
	public void setFnNoSuffix(String fnNoSuffix) {
		this.fnNoSuffix = fnNoSuffix;
	}
	public String getPretitle() {
		return pretitle;
	}
	public void setPretitle(String pretitle) {
		this.pretitle = pretitle;
	}	
}
