package shared.data;

public class OneChannelSchedule {

	private Integer idchannelSchedule;
	private Integer channelsId;
	private Integer pkey;
	
	// getters/setters
	public Integer getIdchannelSchedule() {
		return idchannelSchedule;
	}
	public void setIdchannelSchedule(Integer idchannelSchedule) {
		this.idchannelSchedule = idchannelSchedule;
	}
	public Integer getChannelsId() {
		return channelsId;
	}
	public void setChannelsId(Integer channelsId) {
		this.channelsId = channelsId;
	}
	public Integer getPkey() {
		return pkey;
	}
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

}
