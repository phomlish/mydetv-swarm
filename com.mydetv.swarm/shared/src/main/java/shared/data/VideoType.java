package shared.data;

import java.util.ArrayList;

public class VideoType {
	private Integer id;
	private String name;
	private Integer sizeMin;
	private Integer sizeMax;
	private Integer gap;
	private Integer altGap;  		// some mydetv channels don't have enough videos for a large gap
	private ArrayList<OneVideo> videos;
	private Integer fallback;		// count the times we have to fallback from this during scheduling so we'll know the types of videos we need to aquire
	// custom getter/setter
	public void incrementFallback() {
		fallback++;
	}
	// getter/setters
	public Integer getSizeMin() {
		return sizeMin;
	}
	public void setSizeMin(Integer sizeMin) {
		this.sizeMin = sizeMin;
	}
	public Integer getSizeMax() {
		return sizeMax;
	}
	public void setSizeMax(Integer sizeMax) {
		this.sizeMax = sizeMax;
	}
	public Integer getGap() {
		return gap;
	}
	public void setGap(Integer gap) {
		this.gap = gap;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<OneVideo> getVideos() {
		return videos;
	}
	public void setVideos(ArrayList<OneVideo> videos) {
		this.videos = videos;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAltGap() {
		return altGap;
	}
	public void setAltGap(Integer altGap) {
		this.altGap = altGap;
	}
	public Integer getFallback() {
		return fallback;
	}
	public void setFallback(Integer fallback) {
		this.fallback = fallback;
	}
	
}
