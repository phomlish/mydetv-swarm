package shared.data;

public class MaintConfig {

	private boolean schedule=false;
	private boolean archives=false;
	private boolean convert=false;
	private boolean processVideos=false;
	private boolean videoWalk=false;
	private boolean thumbnails=false;
	private boolean geoip=false;
	private boolean logMailer=false;
	public boolean doSchedule() {
		return schedule;
	}
	public void setSchedule(boolean schedule) {
		this.schedule = schedule;
	}
	public boolean doArchives() {
		return archives;
	}
	public void setArchives(boolean archives) {
		this.archives = archives;
	}
	public boolean doProcessVideos() {
		return processVideos;
	}
	public void setProcessVideos(boolean processVideos) {
		this.processVideos = processVideos;
	}
	public boolean doVideoWalk() {
		return videoWalk;
	}
	public void setVideoWalk(boolean videoWalk) {
		this.videoWalk = videoWalk;
	}
	public boolean doThumbnails() {
		return thumbnails;
	}
	public void setThumbnails(boolean thumbnails) {
		this.thumbnails = thumbnails;
	}
	public boolean doGeoip() {
		return geoip;
	}
	public void setGeoip(boolean geoip) {
		this.geoip = geoip;
	}
	public boolean doLogMailer() {
		return logMailer;
	}
	public void setLogMailer(boolean logMailer) {
		this.logMailer = logMailer;
	}
	public boolean doConvert() {
		return convert;
	}
	public void setConvert(boolean convert) {
		this.convert = convert;
	}
}
