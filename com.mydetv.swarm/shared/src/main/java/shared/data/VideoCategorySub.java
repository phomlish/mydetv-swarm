package shared.data;

import java.time.LocalDateTime;

public class VideoCategorySub {
	
	private Integer idvideo_category;
	private Integer catMain;
	private String name;
	private String description;
	private Integer idVCSI;
	private LocalDateTime dtAdded;
	
	// getters/setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIdvideo_category() {
		return idvideo_category;
	}
	public void setIdvideo_category(Integer idvideo_category) {
		this.idvideo_category = idvideo_category;
	}
	public Integer getCatMain() {
		return catMain;
	}
	public void setCatMain(Integer catMain) {
		this.catMain = catMain;
	}
	public Integer getIdVCSI() {
		return idVCSI;
	}
	public void setIdVCSI(Integer idVCSI) {
		this.idVCSI = idVCSI;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
}
