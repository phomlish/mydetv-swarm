package shared.data;

import java.time.LocalDateTime;

public class VideoCategorySubImage {

	private Integer idVCSI;
	private Integer idVCM;
	private String filename;
	private LocalDateTime dtAdded;
	private LocalDateTime dtInactive;
	// getters/setters
	public Integer getIdVCSI() {
		return idVCSI;
	}
	public void setIdVCSI(Integer idVCSI) {
		this.idVCSI = idVCSI;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
	public Integer getIdVCM() {
		return idVCM;
	}
	public void setIdVCM(Integer idVCM) {
		this.idVCM = idVCM;
	}

}
