package shared.data;

import java.util.ArrayList;
import java.util.Arrays;

public class BrowserThemes {

	public static ArrayList<String> getBrowserThemes() {
		return new ArrayList<String>(Arrays.asList("default", "crisp"));
	}
	
}
