package shared.data;

public class VideoCategoryMain {

	private Integer idVCM;
	private String name;
	private Integer idVCSI;  // this is the default image for the main category
	public Integer getIdVCM() {
		return idVCM;
	}
	public void setIdVCM(Integer idVCM) {
		this.idVCM = idVCM;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIdVCSI() {
		return idVCSI;
	}
	public void setIdVCSI(Integer idVCSI) {
		this.idVCSI = idVCSI;
	}
}
