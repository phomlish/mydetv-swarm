package shared.data.webuser;

import java.time.LocalDateTime;

public class SecurityQuestion {

	private Integer idSecurityQuestion;
	private String question;
	private LocalDateTime dtInactive;
	private Integer qgroup;
	
	// getters/setters
	public Integer getIdSecurityQuestion() {
		return idSecurityQuestion;
	}
	public void setIdSecurityQuestion(Integer idSecurityQuestion) {
		this.idSecurityQuestion = idSecurityQuestion;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
	public Integer getQgroup() {
		return qgroup;
	}
	public void setQgroup(Integer qgroup) {
		this.qgroup = qgroup;
	}


}
