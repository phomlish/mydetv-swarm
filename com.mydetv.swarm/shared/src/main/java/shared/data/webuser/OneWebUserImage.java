package shared.data.webuser;

import java.time.LocalDateTime;

public class OneWebUserImage {

	private Integer idWebUserImage;
	private Integer idWebUser;
	private String origFilename;
	private byte[] origFile;
	private String newFn;
	private Boolean imageSelected;
	private LocalDateTime dtAdded;
	private LocalDateTime dtInactive;
	private Boolean used = false;
	
	public Integer getIdWebUserImage() {
		return idWebUserImage;
	}
	public void setIdWebUserImage(Integer idWebUserImage) {
		this.idWebUserImage = idWebUserImage;
	}
	public String getOrigFilename() {
		return origFilename;
	}
	public void setOrigFilename(String origFilename) {
		this.origFilename = origFilename;
	}
	public byte[] getOrigFile() {
		return origFile;
	}
	public void setOrigFile(byte[] origFile) {
		this.origFile = origFile;
	}
	public Integer getIdWebUser() {
		return idWebUser;
	}
	public void setIdWebUser(Integer idWebUser) {
		this.idWebUser = idWebUser;
	}
	public Boolean getImageSelected() {
		return imageSelected;
	}
	public void setImageSelected(Boolean imageSelected) {
		this.imageSelected = imageSelected;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public String getNewFn() {
		return newFn;
	}
	public void setNewFn(String newFn) {
		this.newFn = newFn;
	}
	

	
}
