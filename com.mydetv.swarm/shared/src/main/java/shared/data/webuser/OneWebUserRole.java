package shared.data.webuser;

public class OneWebUserRole {

	private Integer idWebUserRole;
	private Integer webUserId;
	private Integer webUserRoleType;
	private String name;
	private String details; // used when the role type needs details: like broadcaster for which channel
	private Boolean used = false;
	
	// getters/setters
	public Integer getWebUserId() {
		return webUserId;
	}
	public void setWebUserId(Integer webUserId) {
		this.webUserId = webUserId;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public Integer getWebUserRoleType() {
		return webUserRoleType;
	}
	
	public void setWebUserRoleType(Integer webUserRoleType) {
		this.webUserRoleType = webUserRoleType;
		
		if(webUserRoleType.equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
			setName("grand master poobah");
		else if(webUserRoleType.equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()))
			setName("broadcaster "+details);
		else if(webUserRoleType.equals(WebUserRoleTypesEnum.WebUserRoleType.CHAT.getLevel()))
			setName("chat administrator "+details);
		else if(webUserRoleType.equals(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel()))
			setName("people administrator");
		else if(webUserRoleType.equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel()))
			setName("schedule administrator "+details);
		else
			setName("unknown");	
		
	}
	public Integer getIdWebUserRole() {
		return idWebUserRole;
	}
	public void setIdWebUserRole(Integer idWebUserRole) {
		this.idWebUserRole = idWebUserRole;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}
}
