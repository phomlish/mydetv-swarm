package shared.data.webuser;

import java.time.LocalDateTime;

public class OneWebUserUsername {

	private Integer idUsername;
	private Integer idWebUser;
	private String username;
	private LocalDateTime dtAdded;
	private Boolean used = false;
	
	// geters/setters
	public Integer getIdUsername() {
		return idUsername;
	}
	public void setIdUsername(Integer idUsername) {
		this.idUsername = idUsername;
	}
	public Integer getIdWebUser() {
		return idWebUser;
	}
	public void setIdWebUser(Integer idWebUser) {
		this.idWebUser = idWebUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}
	
}
