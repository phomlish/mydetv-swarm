package shared.data.webuser;

/**
 * 
 * @author phomlish
 *
 */
public class WebUserRoleTypesEnum {

	public enum WebUserRoleType {
		ADMIN(0), BROADCASTER(1), CHAT(2), PEOPLE(3), VIDEOS(4), SCHEDULE(5);
		
		private final Integer level;
		
	    private WebUserRoleType(Integer level) {
	        this.level = level;
	    }

	    public Integer getLevel() {
	        return level;
	    }
	}
}
