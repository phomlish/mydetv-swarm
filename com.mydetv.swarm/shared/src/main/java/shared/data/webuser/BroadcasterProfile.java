package shared.data.webuser;

public class BroadcasterProfile {

	private Integer bpId;
	private Integer wuid;
	// bpType:
	// 0: sound
	private Integer bpType;
	
	private Object bpData;
	// getters/setters
	public Integer getBpId() {
		return bpId;
	}
	public void setBpId(Integer bpId) {
		this.bpId = bpId;
	}
	public Integer getWuid() {
		return wuid;
	}
	public void setWuid(Integer wuid) {
		this.wuid = wuid;
	}
	public Integer getBpType() {
		return bpType;
	}
	public void setBpType(Integer bpType) {
		this.bpType = bpType;
	}
	public Object getBpData() {
		return bpData;
	}
	public void setBpData(Object bpData) {
		this.bpData = bpData;
	}
}
