package shared.data.webuser;

import shared.data.OneSound;

public class BroadcasterProfileDataSound {

	private Integer seq;
	private OneSound os;
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public OneSound getOs() {
		return os;
	}
	public void setOs(OneSound os) {
		this.os = os;
	}
}
