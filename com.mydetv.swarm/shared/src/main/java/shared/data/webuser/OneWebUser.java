package shared.data.webuser;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.TreeMap;

import shared.data.OneChatBlock;

public class OneWebUser {

	private Integer wuid;
	private Integer idWebUserLogin;	// remember this so we can logout
	private Integer dbChannelWebUsersId=0;
	
	private String username;

	private LocalDate birthday;
	private String link;
	private boolean over18;
	private String userTimezone;
	private ZoneId userDtz;
	// careful, theme is here AND in sessionUser
    // we need it before they are logged in via a cookie	
	private String theme; 
	private Integer webUserEmailId;

	private LocalDateTime dtReAuthenticated; // sometimes they need to re-authenticate for extra security
	private Boolean requirePasswordChange;	// after a change password request they must change the password
	
	private LocalDateTime dtAdded;
	private LocalDateTime dtBanned;
	private LocalDateTime dtMuted;
	private byte[] passwordSalt;
	private byte[] passwordHash;

	private OneWebUserEmail loggedInEmail;
	private TreeMap<Integer, OneWebUserEmail> emails;
	private TreeMap<Integer, OneWebUserRole> roles;
	private Boolean admin;  // roles logic is complicated, use this for the trump card
	private TreeMap<Integer, OneWebUserImage> images;
	private Integer webUserSelectedImage = -1;
	private TreeMap<Integer, OneChatBlock> chatBlocked;
	private TreeMap<LocalDateTime, OneWebUserLogin> logins;
	private HashMap<Integer,BroadcasterProfile> broadcasterProfiles;
	private Object broadcasterProfilesLock = new Object();
	// used for slickgrid
	private String strEmails;
	private String strRoles;

	// getters/setters
	public Integer getWuid() {
		return wuid;
	}
	public void setWuid(Integer wuid) {
		this.wuid = wuid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserTimezone() {
		return userTimezone;
	}
	public void setUserTimezone(String userTimezone) {
		this.userTimezone = userTimezone;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public LocalDateTime getDtBanned() {
		return dtBanned;
	}
	public void setDtBanned(LocalDateTime dtBanned) {
		this.dtBanned = dtBanned;
	}
	public TreeMap<Integer, OneWebUserEmail> getEmails() {
		return emails;
	}
	public void setEmails(TreeMap<Integer, OneWebUserEmail> emails) {
		this.emails = emails;
	}
	public TreeMap<Integer, OneWebUserRole> getRoles() {
		return roles;
	}
	public void setRoles(TreeMap<Integer, OneWebUserRole> roles) {
		this.roles = roles;
	}
	public TreeMap<Integer, OneWebUserImage> getImages() {
		return images;
	}
	public void setImages(TreeMap<Integer, OneWebUserImage> images) {
		this.images = images;
	}
	public String getStrEmails() {
		return strEmails;
	}
	public void setStrEmails(String strEmails) {
		this.strEmails = strEmails;
	}
	public String getStrRoles() {
		return strRoles;
	}
	public void setStrRoles(String strRoles) {
		this.strRoles = strRoles;
	}
	public byte[] getPasswordSalt() {
		return passwordSalt;
	}
	public void setPasswordSalt(byte[] passwordSalt) {
		this.passwordSalt = passwordSalt;
	}
	public byte[] getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public Integer getWebUserEmailId() {
		return webUserEmailId;
	}
	public void setWebUserEmailId(Integer webUserEmailId) {
		this.webUserEmailId = webUserEmailId;
	}
	public Integer getIdWebUserLogin() {
		return idWebUserLogin;
	}
	public void setIdWebUserLogin(Integer idWebUserLogin) {
		this.idWebUserLogin = idWebUserLogin;
	}
	public Integer getWebUserSelectedImage() {
		return webUserSelectedImage;
	}
	public void setWebUserSelectedImage(Integer webUserSelectedImage) {
		this.webUserSelectedImage = webUserSelectedImage;
	}
	public TreeMap<Integer, OneChatBlock> getChatBlocked() {
		return chatBlocked;
	}
	public void setChatBlocked(TreeMap<Integer, OneChatBlock> chatBlocked) {
		this.chatBlocked = chatBlocked;
	}
	public Boolean getRequirePasswordChange() {
		return requirePasswordChange;
	}
	public void setRequirePasswordChange(Boolean requirePasswordChange) {
		this.requirePasswordChange = requirePasswordChange;
	}
	public LocalDateTime getDtReAuthenticated() {
		return dtReAuthenticated;
	}
	public void setDtReAuthenticated(LocalDateTime dtReAuthenticated) {
		this.dtReAuthenticated = dtReAuthenticated;
	}
	public OneWebUserEmail getLoggedInEmail() {
		return loggedInEmail;
	}
	public void setLoggedInEmail(OneWebUserEmail loggedInEmail) {
		this.loggedInEmail = loggedInEmail;
	}
	public boolean isOver18() {
		return over18;
	}
	public void setOver18(boolean over18) {
		this.over18 = over18;
	}
	public TreeMap<LocalDateTime, OneWebUserLogin> getLogins() {
		return logins;
	}
	public void setLogins(TreeMap<LocalDateTime, OneWebUserLogin> logins) {
		this.logins = logins;
	}
	public ZoneId getUserDtz() {
		return userDtz;
	}
	public void setUserDtz(ZoneId userDtz) {
		this.userDtz = userDtz;
	}
	public LocalDateTime getDtMuted() {
		return dtMuted;
	}
	public void setDtMuted(LocalDateTime dtMuted) {
		this.dtMuted = dtMuted;
	}
	public Boolean isAdmin() {
		return admin;
	}
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	public Integer getDbChannelWebUsersId() {
		return dbChannelWebUsersId;
	}
	public void setDbChannelWebUsersId(Integer dbChannelWebUsersId) {
		this.dbChannelWebUsersId = dbChannelWebUsersId;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public HashMap<Integer,BroadcasterProfile> getBroadcasterProfiles() {
		return broadcasterProfiles;
	}
	public void setBroadcasterProfiles(HashMap<Integer,BroadcasterProfile> broadcasterProfiles) {
		this.broadcasterProfiles = broadcasterProfiles;
	}
	public Object getBroadcasterProfilesLock() {
		return broadcasterProfilesLock;
	}
	public void setBroadcasterProfilesLock(Object broadcasterProfilesLock) {
		this.broadcasterProfilesLock = broadcasterProfilesLock;
	}
}
