package shared.data.webuser;

import java.time.LocalDateTime;

public class OneWebUserEmail {

	private Integer idWebUser;
	private Integer idEmail;
	private String email;
	private Integer emailType;
	private String verificationCode;
	private String uuid;
	private LocalDateTime dtVcSent;
	private LocalDateTime dtAdded;
	private LocalDateTime dtVerified;
	private String ipAddress;			// used to log/email requests.  temp, not saved in db
	private String username;
	private Boolean canDelete;
	private Boolean used = false;
	
	public Integer getIdWebUser() {
		return idWebUser;
	}
	public void setIdWebUser(Integer idWebUser) {
		this.idWebUser = idWebUser;
	}
	public Integer getIdEmail() {
		return idEmail;
	}
	public void setIdEmail(Integer idEmail) {
		this.idEmail = idEmail;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public LocalDateTime getDtVerified() {
		return dtVerified;
	}
	public void setDtVerified(LocalDateTime dtVerified) {
		this.dtVerified = dtVerified;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public Integer getEmailType() {
		return emailType;
	}
	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}
	public LocalDateTime getDtVcSent() {
		return dtVcSent;
	}
	public void setDtVcSent(LocalDateTime dtVcSent) {
		this.dtVcSent = dtVcSent;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}

}
