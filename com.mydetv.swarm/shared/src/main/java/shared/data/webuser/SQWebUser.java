package shared.data.webuser;

import java.time.LocalDateTime;

public class SQWebUser {

	private Integer idWebUser;
	private String email;
	private byte[] salt;
	
	private Integer sqq1;
	private byte[] sqa1;
	private String question1;
	private Integer sqg1;
	
	private Integer sqq2;
	private byte[] sqa2;
	private String question2;
	private Integer sqg2;
	
	private String forgotPassword;
	private LocalDateTime dtForgotPassword;
	private String uuidForgotEmail;
	
	public Integer getSqq1() {
		return sqq1;
	}
	public void setSqq1(Integer sqq1) {
		this.sqq1 = sqq1;
	}
	public byte[] getSqa1() {
		return sqa1;
	}
	public void setSqa1(byte[] sqa1) {
		this.sqa1 = sqa1;
	}
	public String getQuestion1() {
		return question1;
	}
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	public Integer getSqg1() {
		return sqg1;
	}
	public void setSqg1(Integer sqg1) {
		this.sqg1 = sqg1;
	}
	public Integer getSqq2() {
		return sqq2;
	}
	public void setSqq2(Integer sqq2) {
		this.sqq2 = sqq2;
	}
	public byte[] getSqa2() {
		return sqa2;
	}
	public void setSqa2(byte[] sqa2) {
		this.sqa2 = sqa2;
	}
	public String getQuestion2() {
		return question2;
	}
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	public Integer getSqg2() {
		return sqg2;
	}
	public void setSqg2(Integer sqg2) {
		this.sqg2 = sqg2;
	}
	public String getForgotPassword() {
		return forgotPassword;
	}
	public void setForgotPassword(String forgotPassword) {
		this.forgotPassword = forgotPassword;
	}
	public LocalDateTime getDtForgotPassword() {
		return dtForgotPassword;
	}
	public void setDtForgotPassword(LocalDateTime dtForgotPassword) {
		this.dtForgotPassword = dtForgotPassword;
	}
	public String getUuidForgotEmail() {
		return uuidForgotEmail;
	}
	public void setUuidForgotEmail(String uuidForgotEmail) {
		this.uuidForgotEmail = uuidForgotEmail;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public byte[] getSalt() {
		return salt;
	}
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
	public Integer getIdWebUser() {
		return idWebUser;
	}
	public void setIdWebUser(Integer idWebUser) {
		this.idWebUser = idWebUser;
	}
	
	
}
