package shared.data.webuser;

import java.time.LocalDateTime;

public class Failed {

	private Integer idwebUserFailed;
	private Integer idWebUser;
	private LocalDateTime dtFailedAttempt;
	private Integer failedType;

	public Integer getIdWebUser() {
		return idWebUser;
	}
	public void setIdWebUser(Integer idWebUser) {
		this.idWebUser = idWebUser;
	}
	public LocalDateTime getDtFailedAttempt() {
		return dtFailedAttempt;
	}
	public void setDtFailedAttempt(LocalDateTime dtFailedAttempt) {
		this.dtFailedAttempt = dtFailedAttempt;
	}
	public Integer getFailedType() {
		return failedType;
	}
	public void setFailedType(Integer failedType) {
		this.failedType = failedType;
	}
	public Integer getIdwebUserFailed() {
		return idwebUserFailed;
	}
	public void setIdwebUserFailed(Integer idwebUserFailed) {
		this.idwebUserFailed = idwebUserFailed;
	}	
}
