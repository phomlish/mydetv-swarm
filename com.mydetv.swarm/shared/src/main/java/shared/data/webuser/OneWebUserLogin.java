package shared.data.webuser;

import java.time.LocalDateTime;

public class OneWebUserLogin {

	private Integer idWebUserLogin;
	private LocalDateTime dtLogin;
	private String remoteAddress;
	private LocalDateTime dtLogout;
	private String logoutReason;
	public LocalDateTime getDtLogin() {
		return dtLogin;
	}
	public void setDtLogin(LocalDateTime dtLogin) {
		this.dtLogin = dtLogin;
	}
	public String getLogoutReason() {
		return logoutReason;
	}
	public void setLogoutReason(String logoutReason) {
		this.logoutReason = logoutReason;
	}
	public LocalDateTime getDtLogout() {
		return dtLogout;
	}
	public void setDtLogout(LocalDateTime dtLogout) {
		this.dtLogout = dtLogout;
	}
	public Integer getIdWebUserLogin() {
		return idWebUserLogin;
	}
	public void setIdWebUserLogin(Integer idWebUserLogin) {
		this.idWebUserLogin = idWebUserLogin;
	}
	public String getRemoteAddress() {
		return remoteAddress;
	}
	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
	
}
