package shared.data.webuser;

import java.time.LocalDateTime;

public class Password {

	private Integer idwebUserPassword;
	private Integer idwebUser;
	private byte[] passwordHash;
	private LocalDateTime dtAdded;
	private LocalDateTime dtActive;
	private LocalDateTime dtInactive;
	private boolean requireChange;
	
	public Integer getIdwebUserPassword() {
		return idwebUserPassword;
	}
	public void setIdwebUserPassword(Integer idwebUserPassword) {
		this.idwebUserPassword = idwebUserPassword;
	}
	public Integer getIdwebUser() {
		return idwebUser;
	}
	public void setIdwebUser(Integer idwebUser) {
		this.idwebUser = idwebUser;
	}
	public byte[] getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
	public LocalDateTime getDtActive() {
		return dtActive;
	}
	public void setDtActive(LocalDateTime dtActive) {
		this.dtActive = dtActive;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
	public boolean isRequireChange() {
		return requireChange;
	}
	public void setRequireChange(boolean requireChange) {
		this.requireChange = requireChange;
	}
}
