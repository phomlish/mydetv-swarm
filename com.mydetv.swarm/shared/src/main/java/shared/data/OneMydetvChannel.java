package shared.data;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import shared.data.broadcast.OneJClient;
import shared.data.broadcast.OneWaiter;
import shared.data.broadcast.SessionUser;

public class OneMydetvChannel {

	private Integer channelId;
	private String name;
	//private TreeMap<String, WaitingGroup> waitingGroups = new TreeMap<String, WaitingGroup>();
	private TreeMap<LocalDateTime, OneWaiter> waiters = new TreeMap<LocalDateTime, OneWaiter>();
	// does the database say this is active?  
	private boolean isActive;
	// 0: remote, 1: 24x7, 2: studio, 3: test
	// only used for getting scheduled or random clips
	private Integer channelType;
	
	private OneJClient ojClient = new OneJClient();
	private OneVideo currentlyPlaying;
	
	// sessionUser.mydetv is the key
	private ConcurrentHashMap<String, SessionUser> sessionUsers = new ConcurrentHashMap<String, SessionUser>();
	// we will lock VideoHelpers.connectViewersWaiting to stay threadsafe
	private Object channelLock = new Object();
	// while we are processing a waiter we'll remember him so we don't start hooking up more ppl
	private SessionUser currentWaiter;
	
	// WHY HERE, why not in jClient???  are we recording
	private Boolean recording=false;
	// mature content is restricted
	private Boolean restricted = false;
	
	private TreeMap<Integer, OneSchedule> schedules;
	private HashMap<Integer, OneChannelSchedule> channelSchedules;
	
	// used for studio broadcasts
	private Object masterServer;
	private Thread masterServerThread;
	
	// getters/setters
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public ConcurrentHashMap<String, SessionUser> getSessionUsers() {
		return sessionUsers;
	}
	public void setSessionUsers(ConcurrentHashMap<String, SessionUser> socketUsers) {
		this.sessionUsers = socketUsers;
	}
	public Integer getChannelType() {
		return channelType;
	}
	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}
	public OneJClient getOjClient() {
		return ojClient;
	}
	public void setOjClient(OneJClient ojClient) {
		this.ojClient = ojClient;
	}
	public Boolean getRestricted() {
		return restricted;
	}
	public void setRestricted(Boolean restricted) {
		this.restricted = restricted;
	}
	public TreeMap<Integer, OneSchedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(TreeMap<Integer, OneSchedule> schedules) {
		this.schedules = schedules;
	}
	public HashMap<Integer, OneChannelSchedule> getChannelSchedules() {
		return channelSchedules;
	}
	public void setChannelSchedules(HashMap<Integer, OneChannelSchedule> channelSchedules) {
		this.channelSchedules = channelSchedules;
	}
	public Boolean isRecording() {
		return recording;
	}
	public void setRecording(Boolean recording) {
		this.recording = recording;
	}
	//public TreeMap<String, WaitingGroup> getWaitingGroups() {
	//	return waitingGroups;
	//}
	//public void setWaitingGroups(TreeMap<String, WaitingGroup> waitingGroups) {
	//	this.waitingGroups = waitingGroups;
	//}
	public Object getChannelLockDeprecated() {
		return channelLock;
	}
	public void setChannelLockDeprecated(Object channelLock) {
		this.channelLock = channelLock;
	}
	public Thread getMasterServerThread() {
		return masterServerThread;
	}
	public void setMasterServerThread(Thread masterServerThread) {
		this.masterServerThread = masterServerThread;
	}
	public Object getMasterServer() {
		return masterServer;
	}
	public void setMasterServer(Object masterServer) {
		this.masterServer = masterServer;
	}
	public SessionUser getCurrentWaiter() {
		return currentWaiter;
	}
	public void setCurrentWaiter(SessionUser currentWaiter) {
		this.currentWaiter = currentWaiter;
	}
	public TreeMap<LocalDateTime, OneWaiter> getWaiters() {
		return waiters;
	}
	public void setWaiters(TreeMap<LocalDateTime, OneWaiter> waiters) {
		this.waiters = waiters;
	}
	public OneVideo getCurrentlyPlaying() {
		return currentlyPlaying;
	}
	public void setCurrentlyPlaying(OneVideo currentlyPlaying) {
		this.currentlyPlaying = currentlyPlaying;
	}
}
