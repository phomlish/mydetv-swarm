package shared.data;

import java.time.LocalDateTime;

public class OneVideoConverted {

	private Integer videoConvertedId;
	private Integer pkey;
	private String fn;
	private String ofn;
	private String machineName;
	private String convertedType;
	private float length;
	private long filesize;
	private LocalDateTime dtConverted;
	private Integer bitrate;
	private String stream0;
	private String stream1;
	private LocalDateTime dtInactive;
	private String failedReason;
	
	private Boolean found=false;		// used while we populate to make sure we see them all
	
	// custom 
	public String log() {
		String rv="";
		rv+=videoConvertedId+" "+convertedType+" "+pkey+" "+fn;
		return rv;
	}
	// getters/setters
	public String getFn() {
		return fn;
	}
	public void setFn(String fn) {
		this.fn = fn;
	}
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public long getFilesize() {
		return filesize;
	}
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	public LocalDateTime getDtConverted() {
		return dtConverted;
	}
	public void setDtConverted(LocalDateTime dtConverted) {
		this.dtConverted = dtConverted;
	}
	public String getStream0() {
		return stream0;
	}
	public void setStream0(String stream0) {
		this.stream0 = stream0;
	}
	public String getStream1() {
		return stream1;
	}
	public void setStream1(String stream1) {
		this.stream1 = stream1;
	}
	public Integer getBitrate() {
		return bitrate;
	}
	public void setBitrate(Integer bitrate) {
		this.bitrate = bitrate;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public Integer getVideoConvertedId() {
		return videoConvertedId;
	}
	public void setVideoConvertedId(Integer videoConvertedId) {
		this.videoConvertedId = videoConvertedId;
	}
	public String getConvertedType() {
		return convertedType;
	}
	public void setConvertedType(String convertedType) {
		this.convertedType = convertedType;
	}
	public LocalDateTime getDtInactive() {
		return dtInactive;
	}
	public void setDtInactive(LocalDateTime dtInactive) {
		this.dtInactive = dtInactive;
	}
	public Integer getPkey() {
		return pkey;
	}
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}
	public String getFailedReason() {
		return failedReason;
	}
	public void setFailedReason(String failedReason) {
		this.failedReason = failedReason;
	}
	public Boolean getFound() {
		return found;
	}
	public void setFound(Boolean found) {
		this.found = found;
	}
	public String getOfn() {
		return ofn;
	}
	public void setOfn(String ofn) {
		this.ofn = ofn;
	}	
}
