package shared.data;

import java.time.LocalDateTime;

public class OneChatBlock {

	private Integer chatBlockId;
	private Integer idBlocker;
	private Integer idBlocked;
	private LocalDateTime dtAdded;
	public Integer getChatBlockId() {
		return chatBlockId;
	}
	public void setChatBlockId(Integer chatBlockId) {
		this.chatBlockId = chatBlockId;
	}
	public Integer getIdBlocker() {
		return idBlocker;
	}
	public void setIdBlocker(Integer idBlocker) {
		this.idBlocker = idBlocker;
	}
	public Integer getIdBlocked() {
		return idBlocked;
	}
	public void setIdBlocked(Integer idBlocked) {
		this.idBlocked = idBlocked;
	}
	public LocalDateTime getDtAdded() {
		return dtAdded;
	}
	public void setDtAdded(LocalDateTime dtAdded) {
		this.dtAdded = dtAdded;
	}
}
