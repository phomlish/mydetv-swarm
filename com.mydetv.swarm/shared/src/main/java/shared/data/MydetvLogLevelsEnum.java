package shared.data;

public class MydetvLogLevelsEnum {

	public enum MydetvLogType {
		EMERGENCY(0), ALERT(1), CRITICAL(2), ERROR(3), WARNING(4), NOTICE(5), INFO(6), DEBUG(7), TRACE(8);
		
		private final Integer level;
		
	    private MydetvLogType(Integer level) {
	        this.level = level;
	    }

	    public Integer getLevel() {
	        return level;
	    }
	}
}
