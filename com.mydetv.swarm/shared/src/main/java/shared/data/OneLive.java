package shared.data;

import java.time.LocalDateTime;

public class OneLive {
	private Integer idschedule_live;	
	private String title;
	private Integer subcat;
	private LocalDateTime dtStart;
	private LocalDateTime dtEnd;
	
	// getters/setters
	public Integer getIdschedule_live() {
		return idschedule_live;
	}
	public void setIdschedule_live(Integer idschedule_live) {
		this.idschedule_live = idschedule_live;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getSubcat() {
		return subcat;
	}
	public void setSubcat(Integer subcat) {
		this.subcat = subcat;
	}
	public LocalDateTime getDtStart() {
		return dtStart;
	}
	public void setDtStart(LocalDateTime dtStart) {
		this.dtStart = dtStart;
	}
	public LocalDateTime getDtEnd() {
		return dtEnd;
	}
	public void setDtEnd(LocalDateTime dtEnd) {
		this.dtEnd = dtEnd;
	}


}
