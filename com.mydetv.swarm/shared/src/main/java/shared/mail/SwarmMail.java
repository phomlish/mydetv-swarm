package shared.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.Config;

public class SwarmMail {

	private static final Logger log = LoggerFactory.getLogger(SwarmMail.class);
	
	public static Boolean SendEMail(Config config, String to, String subject, String body, String altbody) {
		
        String host = config.getSmtpHost();
        Integer port = config.getSmtpPort();
        String from = config.getSmtpFrom();
        //String pass = "RTQNHHPfQ0xWMGsMqX3u";  // not needed for my smtp server
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        //props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", host);
        
        Multipart multiPart = new MimeMultipart("alternative");
        try {
        	
        	MimeBodyPart textPart = new MimeBodyPart();
        	textPart.setText(altbody, "utf-8");
        	multiPart.addBodyPart(textPart);
        	
            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(body, "text/html; charset=utf-8");
        	multiPart.addBodyPart(htmlPart);
                    
	        Session session = Session.getDefaultInstance(props, null);
	        Message message = new MimeMessage(session);
	        message.setContent(multiPart);
	        
			message.setFrom(new InternetAddress(from));
		
	        InternetAddress toAddress = new InternetAddress(to);

	        message.addRecipient(Message.RecipientType.TO, toAddress);
	        message.setSubject(subject);
	        
	        Transport transport = session.getTransport("smtp");
	        transport.addTransportListener(new MyTransportListener());
	        transport.connect(host, from); //, pass);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
        } catch (MessagingException e) {
			log.error("error sending email "+e);
			return false;
		}
        return true;
	}
	
	public static class MyTransportListener implements TransportListener{

		@Override
		public void messageDelivered(TransportEvent e) {
			log.info("messageDelivered:"+e.toString());
		}

		@Override
		public void messageNotDelivered(TransportEvent e) {
			log.info("messageNotDelivered:"+e.toString());			
		}

		@Override
		public void messagePartiallyDelivered(TransportEvent e) {
			log.info("messagePartiallyDelivered:"+e.toString());
		}
	}
	
	public static String GetEmailHeader() {
		String header = "<head>\n";
		header+="<style>\n";
		header+="a:hover    { color: orange; }";
		header+="a:active   { color: orange; }";
		header+="a:visited  { color: orange; }";
		header+="a:link     { color: orange; }";
		header+="input 		{text-align: center;}";
		header+="</style>\n";
		header+="</head>\n";
		return header;
	}
}
