package shared.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.VideoCategoryMain;

public class DbVideoCategoryMain {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbVideoCategorySub.class);
	public HashMap<Integer, VideoCategoryMain> getMainCategories() {
		
		String dbc = "SELECT * from videoCategoryMain";

		HashMap<Integer, VideoCategoryMain> maincats = new HashMap<Integer, VideoCategoryMain>();
		Connection conn = null;
		try {
			
			conn = dbConnection.getConnection();
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
			ResultSet rs = stmt.getResultSet();
			
			while(rs.next()) {
				VideoCategoryMain ovcm = new VideoCategoryMain();
				ovcm.setIdVCM(rs.getInt("idvideoCategoryMain"));
				ovcm.setName(rs.getString("name"));
				if(rs.getInt("idVCSI")!=0)
					ovcm.setIdVCSI(rs.getInt("idVCSI"));
				maincats.put(ovcm.getIdVCM(), ovcm);
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);	
		log.debug("read "+maincats.size()+" subcats");
		return maincats;
	}
}
