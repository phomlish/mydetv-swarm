package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.broadcast.JanusStats;

public class DbJanusStats {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbJanusStats.class);
	
	public void insert(JanusStats janusStats) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call janusStats_insert (?,?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelWebUsersId", janusStats.getChannelWebUsersId());
			cs.setString("in_state", janusStats.getState());
			cs.setString("in_rtcpStats", janusStats.getRtcpStats());
			cs.setString("in_inStats", janusStats.getInStats());
			cs.setString("in_outStats", janusStats.getOutStats());
			cs.execute();

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
}
