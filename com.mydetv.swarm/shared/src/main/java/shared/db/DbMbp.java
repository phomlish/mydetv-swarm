package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneMbp;

public class DbMbp {
	
	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbMbp.class);
	
	public HashMap<Integer, OneMbp> get() {
	
		HashMap<Integer, OneMbp> mbps = new HashMap<Integer, OneMbp>();
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call mbps_get () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneMbp mbp = new OneMbp();
				mbp.setIdMbp(rs.getInt("idMbp"));
				mbp.setIdVideo(rs.getInt("idVideo"));
				
				mbp.setFn(rs.getString("fn"));
				mbp.setFn2(rs.getString("fn2"));
				mbp.setFnOrig(rs.getString("fn_orig"));
				mbp.setFnVideo(rs.getString("fnVideo"));
				mbp.setDetails(rs.getString("details"));
				mbp.setNotes(rs.getString("notes"));
				mbp.setDtAdded(rs.getTimestamp("dt_added").toLocalDateTime());
				mbp.setDvd(rs.getString("dvd"));
				mbp.setConverted(rs.getString("converted"));
				mbp.setEdited(rs.getString("edited"));
				mbp.setShowdate(rs.getString("showdate"));
				mbp.setShowset(rs.getInt("showset"));
				
				mbps.put(mbp.getIdMbp(), mbp);
				
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		
		dbConnection.tryClose(conn);	
		return mbps;
	}
	

}
