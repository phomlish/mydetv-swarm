package shared.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class DbCommand {

	@Inject private DbConnection dbConnection;
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbCommand.class);
	
	public void doCommand(String cmd) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			PreparedStatement s = conn.prepareStatement(cmd);
			s.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	public void doCommand(Connection conn, String cmd) {
		try {
			PreparedStatement s = conn.prepareStatement(cmd);
			s.execute();
			s.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}		
	}
	public ResultSet doCommandRS(String cmd) {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = dbConnection.getConnection();
			PreparedStatement s = conn.prepareStatement(cmd);
			rs = s.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
		return rs;
	}
	public ResultSet doCommandRS(Connection conn, String cmd) throws SQLException {
		ResultSet rs = null;
		PreparedStatement s = conn.prepareStatement(cmd);
		rs = s.executeQuery();
		return rs;
	}
	
}
