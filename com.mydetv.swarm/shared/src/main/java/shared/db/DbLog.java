package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.MydetvLogLevelsEnum;
import shared.data.broadcast.SessionUser;

public class DbLog {
	
	@Inject private DbConnection dbConnection;
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbLog.class);
	
	public void info(String desc) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			log(conn, desc, MydetvLogLevelsEnum.MydetvLogType.INFO.getLevel());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	public void info(Connection conn, String desc) {
		log(conn, desc, MydetvLogLevelsEnum.MydetvLogType.INFO.getLevel());
	}
	
	public void infoWebUser(String desc, SessionUser su) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.INFO.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void infoWebUser(Connection conn, String desc, SessionUser su) {
		logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.INFO.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
	}
	
	public void warnWebUser(String desc, SessionUser su) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void warnWebUser(Connection conn, String desc, SessionUser su) {
		logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
	}
	
	public void errorWebUser(String desc, SessionUser su) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.ERROR.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void errorWebUser(Connection conn, String desc, SessionUser su) {
		logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.ERROR.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
	}
	
	public void critical(String desc) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			log(conn, desc, MydetvLogLevelsEnum.MydetvLogType.CRITICAL.getLevel());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	public void critical(Connection conn, String desc) {
		log(conn, desc, MydetvLogLevelsEnum.MydetvLogType.CRITICAL.getLevel());
	}
	public void criticalWebUser(String desc, SessionUser su) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.CRITICAL.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	public void criticalWebUser(Connection conn, String desc, SessionUser su) {
		logWebUser(conn, desc, MydetvLogLevelsEnum.MydetvLogType.CRITICAL.getLevel(), su.getWebUser().getWuid(), su.getIpPort());
	}
	
	private void log(Connection conn, String desc, Integer severity) {
		try {
			
			String source = "unknown";
			StackTraceElement[] el = Thread.currentThread().getStackTrace();
			if(el.length>=4) 
				source=el[3].getClassName()+"."+el[3].getMethodName();
			String message = source+" "+desc;
			fileLog(severity, message);
			
			String simpleProc = "{ call mydetvLog_insert(?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_logDescription", desc);
			cs.setString("in_logSource", source);
			cs.setInt("in_logSeverity", severity);
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	public void logWebUser(Connection conn, String desc, Integer severity, Integer wuid, String ip) {
		try {
			String source = "unknown";
			StackTraceElement[] el = Thread.currentThread().getStackTrace();
			if(el.length>=4) 
				source=el[3].getClassName()+"."+el[3].getMethodName();
			
			String message = source+",desc:"+desc+",idWebUser:"+wuid+",ipAddress:"+ip;
			fileLog(severity, message);

			String simpleProc = "{ call mydetvLog_insertWebUser(?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_logDescription", desc);
			cs.setString("in_logSource", source);
			cs.setInt("in_logSeverity", severity);
			cs.setInt("in_idWebUser", wuid);
			cs.setString("in_ipAddress", ip);
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}

	private void fileLog(Integer severity, String message) {
		switch(severity) {
		case 0:
		case 1:
		case 2:
			log.error(message);
			break;
		case 3:
		case 4:
		case 5:
			log.warn(message);
			break;
		case 6:
			log.info(message);
			break;
		case 7:
			log.debug(message);
			break;
		case 8:
			log.trace(message);
		}
	}
	
	public String getYesterdaysLogs() {
		String rv="";
		rv+="<table>\n";
		rv+="<tr>";
		rv+="<th>added</th><th>description</th><th>severity</th>";
		rv+="<th>username</th><th>ipAddress</th><th>source</th>";
		rv+="</tr>\n";	
		
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call mydetvLog_getYesterday() }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();			
				while(rs.next()) {
					int severity = rs.getInt("logSeverity");
					if(severity==0)
						rv+="<tr bgcolor=\"Red\">";
					else if(severity==1)
						rv+="<tr bgcolor=\"Crimson\">";
					else if(severity==2)
						rv+="<tr bgcolor=\"Salmon\">";
					else if(severity==3)
						rv+="<tr bgcolor=\"OrangeRed\">";
					else if(severity==4)
						rv+="<tr bgcolor=\"Tomato\">";
					else if(severity==5)
						rv+="<tr bgcolor=\"orange\">";
					else if(severity==6)
						rv+="<tr bgcolor=\"White\">";
					else if(severity==7)
						rv+="<tr bgcolor=\"Gainsboro\">";
					else
						rv+="<tr bgcolor=\"Silver\">";
				
					
					rv+="<td>"+rs.getTimestamp("dtAdded").toLocalDateTime()+"</td>";
					rv+="<td>"+rs.getString("logDescription")+"</td>";
					rv+="<td>"+severity+"</td>";
					
					if(rs.getString("username")!=null)
						rv+="<td>"+rs.getString("username")+"</td>";
					else
						rv+="<td></td>";
					if(rs.getString("ipAddress")!=null)
						rv+="<td>"+rs.getString("ipAddress")+"</td>";
					else
						rv+="<td></td>";
					rv+="<td>"+rs.getString("logSource")+"</td>";
					

					rv+="</tr>\n";
				}
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		rv+="</table>\n";
		return rv;
	}
}
