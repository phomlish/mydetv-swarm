package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.TreeMap;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneChatBlock;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebChat;
import shared.data.webuser.OneWebUser;
import shared.db.DbConnection;

public class DbChats {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbChats.class);
	@Inject private DbConnection dbConnection;
	
	public void sendChat(SessionUser sessionUser, String message, LocalDateTime now, Boolean isPrivate) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call chatMessages_insert(?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelId", sessionUser.getMydetvChannel().getChannelId());
			cs.setInt("in_webUserId", sessionUser.getWebUser().getWuid());
			if(isPrivate) cs.setInt("in_isPrivate", 1);
			else cs.setInt("in_isPrivate", 0);
			cs.setTimestamp("in_dtSent", Timestamp.valueOf(now));
			int max = (message.length() > 1024) ? 1024 : message.length();
			cs.setString("in_message", message.substring(0,max));
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void sendChatAdmin(Integer channelId, String message, Timestamp now) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call chatMessages_insert(?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelId", channelId);
			cs.setInt("in_webUserId", 0);
			cs.setTimestamp("in_dtSent", now);
			int max = (message.length() > 1024) ? 1024 : message.length();
			cs.setString("in_message", message.substring(0,max));
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public TreeMap<Timestamp, WebChat> GetChats(SessionUser sessionUser, Integer limit, Timestamp oldest) {
		Connection conn = null;
		TreeMap<Timestamp, WebChat> chats = new TreeMap<Timestamp, WebChat>();
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call chatMessages_get(?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelId", sessionUser.getMydetvChannel().getChannelId());
			cs.setInt("in_limit", limit);
			cs.setTimestamp("in_dtSent", oldest);
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();			
				while(rs.next()) {
					WebChat webChat = new WebChat();
					webChat.setChatId(rs.getInt("chatId"));
					webChat.setWuid(rs.getInt("webuserId"));
					webChat.setMessage(rs.getString("message"));
					Timestamp dtSent = rs.getTimestamp("dtSent");
					webChat.setDtSent(dtSent.toLocalDateTime());
					webChat.setUsername(rs.getString("username"));
					webChat.setIsPrivate(rs.getBoolean("isPrivate"));
					chats.put(dtSent, webChat);
				}
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return chats;
	}
	public TreeMap<Integer, OneChatBlock> getChatBlocked(OneWebUser webUser) {
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			return(getChatBlocked(conn, webUser));
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	public TreeMap<Integer, OneChatBlock> getChatBlocked(Connection conn, OneWebUser webUser) {
		
		TreeMap<Integer, OneChatBlock> chatBlocked = new TreeMap<Integer, OneChatBlock>();
		try {
			String simpleProc = "{ call chatBlock_get(?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", webUser.getWuid());
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();			
				while(rs.next()) {
					OneChatBlock oneChatBlock = new OneChatBlock();
					oneChatBlock.setChatBlockId(rs.getInt("idchatBlock"));
					oneChatBlock.setIdBlocker(rs.getInt("idBlocker"));
					oneChatBlock.setIdBlocked(rs.getInt("idBlocked"));
					oneChatBlock.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
					chatBlocked.put(oneChatBlock.getIdBlocked(), oneChatBlock);
				}
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		
		return chatBlocked;
	}
	
	public void addChatBlock(Integer blocker, Integer blocked) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call chatBlock_insert(?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_blocker", blocker);
			cs.setInt("in_blocked", blocked);
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void deleteChatBlock(OneChatBlock oneChatBlock) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call chatBlock_delete(?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idchatBlock", oneChatBlock.getChatBlockId());
			cs.executeQuery();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
}
