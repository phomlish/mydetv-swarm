package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.VideoCategorySub;

public class DbVideoCategorySub {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbVideoCategorySub.class);
	public HashMap<Integer, VideoCategorySub> getCategories() {
		
		String dbc = "SELECT * from videoCategorySub";

		HashMap<Integer, VideoCategorySub> subcats = new HashMap<Integer, VideoCategorySub>();
		Connection conn = null;
		try {
			
			conn = dbConnection.getConnection();
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
			ResultSet rs = stmt.getResultSet();
			
			while(rs.next()) {
				VideoCategorySub ovs = new VideoCategorySub();
				ovs.setIdvideo_category(rs.getInt("idvideo_category"));
				ovs.setCatMain(rs.getInt("catMain"));
				ovs.setName(rs.getString("name"));
				ovs.setDescription(rs.getString("description"));
				if(rs.getInt("idVCSI")!=0)
					ovs.setIdVCSI(rs.getInt("idVCSI"));
				subcats.put(ovs.getIdvideo_category(), ovs);
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);	
		log.debug("read "+subcats.size()+" subcats");
		return subcats;
	}
	
	public Integer insertVideoCatSub(VideoCategorySub ovs) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc= "{ call vcs_insert (?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setString("in_name", ovs.getName());
			cs.setString("in_desc", ovs.getDescription());
			cs.setInt("in_vcm", ovs.getCatMain());
			cs.registerOutParameter("out_idVCS", java.sql.Types.INTEGER);
			cs.execute();
			Integer out_idVCS = cs.getInt("out_idVCS");
			cs.close();
			return out_idVCS;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
		return 0;
	}
	
	
}
