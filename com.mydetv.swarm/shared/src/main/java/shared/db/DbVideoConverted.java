package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneVideoConverted;

public class DbVideoConverted {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbVideoConverted.class);
			
	public Integer insertVideoConverted(OneVideoConverted ovc) {
		
		Integer rv=-1;
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();

			String simpleProc = "{ call videoConverted_insert(?,?,?,?,?,?,?,?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_pkey", ovc.getPkey());
			cs.setString("in_fn", ovc.getFn());
			cs.setString("in_ofn", ovc.getOfn());
			cs.setString("in_convertedType", ovc.getConvertedType());
			cs.setString("in_machineName", ovc.getMachineName());
			cs.setString("in_stream0", ovc.getStream0());
			cs.setString("in_stream1", ovc.getStream1());
			
			cs.setString("in_failedReason", ovc.getFailedReason());
			if(ovc.getFailedReason()==null || ovc.getFailedReason().equals("")) {
				cs.setFloat("in_length", ovc.getLength());
				cs.setLong("in_filesize", ovc.getFilesize());
				cs.setInt("in_bitrate", ovc.getBitrate());
			}
			else {
				cs.setFloat("in_length", 0);
				cs.setLong("in_filesize", 0);
				cs.setInt("in_bitrate", 0);
			}
			
			cs.registerOutParameter("out_convertedId", java.sql.Types.INTEGER);
			cs.execute();		
			ovc.setVideoConvertedId(cs.getInt("out_convertedId"));
		} catch (SQLException e) {
			log.info("Error insertVideoConverted "+e+"\n"+dbc);
		}
		dbConnection.tryClose(conn);
		return rv;
	}
	
	public HashMap<Integer, OneVideoConverted> getVideosConvertedAll() {
		HashMap<Integer, OneVideoConverted> videosConverted = new HashMap<Integer, OneVideoConverted>();
		
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call videoConverted_getAll() }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneVideoConverted ovc = new OneVideoConverted();
				ovc.setVideoConvertedId(rs.getInt("videoConvertedId"));
				ovc.setPkey(rs.getInt("videoId"));
				ovc.setMachineName(rs.getString("machineName"));
				ovc.setFn(rs.getString("fn"));
				ovc.setOfn(rs.getString("ofn"));
				ovc.setConvertedType(rs.getString("convertedType"));
				ovc.setLength(rs.getFloat("length"));
				ovc.setFilesize(rs.getLong("filesize"));
				ovc.setBitrate(rs.getInt("bitrate"));
				ovc.setDtConverted(rs.getTimestamp("dtConverted").toLocalDateTime());
				ovc.setStream0(rs.getString("stream0"));
				ovc.setStream1(rs.getString("stream1"));
				Timestamp ts = rs.getTimestamp("dtInactive");
				if(ts!=null)
					ovc.setDtInactive(ts.toLocalDateTime());
				ovc.setFailedReason(rs.getString("failedReason"));
				
				videosConverted.put(ovc.getVideoConvertedId(), ovc);
			}
			
		} catch (SQLException e) {
			log.info("Error insertVideoConverted "+e+"\n"+dbc);
		}
		dbConnection.tryClose(conn);
		return videosConverted;
	}

}
