package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.TreeMap;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneSchedule;
import shared.data.VideoType;

public class DbSchedule {
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbSchedule.class);
	@Inject private DbConnection dbConnection;
	
	public TreeMap<Integer, OneSchedule> getSchedulesAll(Integer channel) {
		Connection conn = null;
		TreeMap<Integer, OneSchedule> schedules = new TreeMap<Integer, OneSchedule>();
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call schedules_get (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_mydetvChannel", channel);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneSchedule os = new OneSchedule();
				os.setIdschedule(rs.getInt("idschedule"));
				os.setPkey(rs.getInt("pkey"));
				os.setChannel(channel);
				os.setDt(rs.getTimestamp("dt").toLocalDateTime());
				schedules.put(os.getIdschedule(), os);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return schedules;
	}
	
	public TreeMap<Integer, OneSchedule> getSchedulesAfter(Integer channel, LocalDateTime dt) {
		Connection conn = null;
		TreeMap<Integer, OneSchedule> schedules = new TreeMap<Integer, OneSchedule>();
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call schedules_getAfter (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_mydetvChannel", channel);
			cs.setTimestamp("in_dt", Timestamp.valueOf(dt));
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneSchedule os = new OneSchedule();
				os.setIdschedule(rs.getInt("idschedule"));
				os.setPkey(rs.getInt("pkey"));
				os.setChannel(channel);
				os.setDt(rs.getTimestamp("dt").toLocalDateTime());
				schedules.put(os.getIdschedule(), os);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return schedules;
	}
	
	public OneSchedule insertSchedule(OneSchedule os) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call schedule_insert (?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_mydetvChannel", os.getChannel());
			cs.setInt("in_pkey", os.getPkey());
			cs.setTimestamp("in_dt", Timestamp.valueOf(os.getDt()));
			cs.execute();
			os.setIdschedule(cs.getInt("out_idschedule")); 

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return os;
	}
	public void removeSchedule(OneSchedule os) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call schedule_delete (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_mydetvChannel", os.getChannel());
			cs.setInt("in_scheduleId", os.getIdschedule());
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}

	public TreeMap<Integer, VideoType> getVideoTypes() {
		TreeMap<Integer, VideoType> videoTypes = new TreeMap<Integer, VideoType>();
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call videoTypes_get () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				VideoType st = new VideoType();
				st.setId(rs.getInt("idVideoType"));
				st.setName(rs.getString("name"));
				st.setSizeMin(rs.getInt("sizeMin"));
				st.setSizeMax(rs.getInt("sizeMax"));
				st.setGap(rs.getInt("gap"));
				st.setAltGap(rs.getInt("altGap"));
				videoTypes.put(st.getId(), st);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return videoTypes;
	}
}
