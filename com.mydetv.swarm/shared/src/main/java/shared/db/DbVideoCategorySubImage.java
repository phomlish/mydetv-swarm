package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.VideoCategorySubImage;

public class DbVideoCategorySubImage {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbVideoCategorySubImage.class);
	public HashMap<Integer, VideoCategorySubImage> getImages() {
		
		String dbc = "SELECT * from videoCategorySubImage";
		HashMap<Integer, VideoCategorySubImage> images = new HashMap<Integer, VideoCategorySubImage>();
		Connection conn = null;
		try {
			
			conn = dbConnection.getConnection();
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
			ResultSet rs = stmt.getResultSet();
			
			while(rs.next()) {
				VideoCategorySubImage ovsi = new VideoCategorySubImage();
				ovsi.setIdVCSI(rs.getInt("idVCSI"));
				ovsi.setIdVCM(rs.getInt("idVCM"));
				ovsi.setFilename(rs.getString("filename"));
				ovsi.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
				if(rs.getTimestamp("dtInactive") != null) 
					ovsi.setDtInactive(rs.getTimestamp("dtInactive").toLocalDateTime());
				images.put(ovsi.getIdVCSI(), ovsi);
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);	
		log.debug("read "+images.size()+" images");
		return images;
	}
	
	public Integer insertVideoCatSubImage(VideoCategorySubImage ovsi) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc= "{ call vcsi_insert (?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idVCM", ovsi.getIdVCM());
			cs.setString("in_filename", ovsi.getFilename());
			cs.registerOutParameter("out_idVCSI", java.sql.Types.INTEGER);
			cs.execute();
			Integer out_idVCSI = cs.getInt("out_idVCSI");
			cs.close();
			ovsi.setIdVCSI(out_idVCSI);
			log.info("inserted "+ovsi.getIdVCSI()+","+ovsi.getFilename());
			return out_idVCSI;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
		return 0;
	}
	

}
