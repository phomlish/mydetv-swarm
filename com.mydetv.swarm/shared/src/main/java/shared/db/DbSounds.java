package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneSound;

public class DbSounds {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbSounds.class);
	
	public OneSound addSound(OneSound os) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call sound_insert(?,?,?,?,?, ?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_dn", os.getDn());
			cs.setString("in_fn", os.getFn());
			cs.setString("in_filetype", os.getFiletype());
			cs.setTimestamp("in_dtAdded",  Timestamp.valueOf(os.getDtAdded()));
			cs.setInt("in_size", os.getSize());

			cs.execute();
			os.setId(cs.getInt("out_id"));
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return os;	
	}
	/*
	public OneProfileSound addSound(OneSound os) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call sound_insert(?,?,?,?,?, ?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_dn", os.getDn());
			cs.setString("in_fn", os.getFn());
			cs.setString("in_filetype", os.getFiletype());
			cs.setTimestamp("in_dtAdded",  Timestamp.valueOf(os.getDtAdded()));
			cs.setInt("in_size", os.getSize());

			cs.execute();
			os.setId(cs.getInt("out_id"));
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return os;	
	}
	*/
	
	public HashMap<Integer, OneSound> getSounds() {		
		HashMap<Integer, OneSound> sounds = new HashMap<Integer, OneSound>();
		Connection conn = null;
		String dbc = "SELECT * from sounds where dtInactivated is null";
		
		try {
			
			conn = dbConnection.getConnection();
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
			ResultSet rs = stmt.getResultSet();
			
			while(rs.next()) {
				OneSound os = new OneSound();
				os.setId(rs.getInt("id"));
				os.setDn(rs.getString("dn"));
				os.setFn(rs.getString("fn"));
				os.setFiletype(rs.getString("filetype"));
				os.setSize(rs.getInt("size"));
				Timestamp dtAdded = rs.getTimestamp("dtAdded");
				os.setDtAdded(dtAdded.toLocalDateTime());
				sounds.put(os.getId(), os);
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);	
		log.debug("read "+sounds.size()+" sounds");
		return sounds;
	}
	
}
