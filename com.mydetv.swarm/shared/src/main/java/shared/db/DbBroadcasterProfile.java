package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.webuser.BroadcasterProfile;

public class DbBroadcasterProfile {

	@Inject private DbConnection dbConnection;
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(BroadcasterProfile.class);
	
	public HashMap<Integer,BroadcasterProfile> getBPs(Connection conn, Integer wuid) {
		
		//Connection conn = null;
		ResultSet rs = null;
		HashMap<Integer,BroadcasterProfile>  bps = new HashMap<Integer,BroadcasterProfile>();
		try {
			//conn = dbConnection.getConnection();
		
			String dbc = "SELECT bpid,wuid,bptype,bpdata from broadcasterProfile";
			dbc+=" where wuid="+wuid;
			PreparedStatement s = conn.prepareStatement(dbc);
			rs = s.executeQuery();
			while(rs.next()) {
				BroadcasterProfile obp = new BroadcasterProfile();
				obp.setBpId(rs.getInt("bpId"));
				obp.setWuid(rs.getInt("wuid"));
				obp.setBpType(rs.getInt("bpType"));
				obp.setBpData(rs.getString("bpData"));
				bps.put(obp.getBpId(), obp);
			}
		} catch (SQLException e) {
			log.error("failed getBPs:"+e);
		}
		return bps;
	}
	
	public Integer addOneBP(Integer wuid, Integer bptype, String bpdata) {
		Connection conn = null;
		Integer rv = 0;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call broadcasterProfile_Insert(?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", wuid);
			cs.setInt("in_bpType", bptype);
			cs.setString("in_bpData", bpdata);
			cs.registerOutParameter("out_id", java.sql.Types.INTEGER);
			cs.execute();
			rv=cs.getInt("out_id");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return rv;
	}
	
	/**
	 * 
	 * @param bpId: the broadcasterProfile id
	 */
	public void deleteOneBP(Integer bpId) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call broadcasterProfile_delete(?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_bpId", bpId);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void updateOneBP(Integer bpId, String bpdata) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			updateOneBP(conn, bpId, bpdata);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	public void updateOneBP(Connection conn, Integer bpId, String bpdata) {
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call broadcasterProfile_update(?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_bpId", bpId);
			cs.setString("in_bpData", bpdata);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
}
