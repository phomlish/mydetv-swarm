package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneVideo;

public class DbVideo {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbVideo.class);
	
	public HashMap<Integer, OneVideo> getVideos() {
		HashMap<Integer, OneVideo> videos = new HashMap<Integer, OneVideo>();
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call videos_get () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneVideo ov = new OneVideo();
				ov.setPkey(rs.getInt("pkey"));
				ov.setFn(rs.getString("fn"));
				ov.setSubdir(rs.getString("subdir"));
				ov.setTitle(rs.getString("title"));
				ov.setCatSub(rs.getInt("catsub"));
							
				ov.setDtAdded(rs.getTimestamp("dt_added").toLocalDateTime());
				ov.setFilesize(rs.getLong("filesize"));				
				ov.setLength(rs.getFloat("length"));
				ov.setNotes(rs.getString("notes"));
				ov.setThumbnail(rs.getString("thumbnail"));
				if(rs.getTimestamp("dtInactive")!=null)
					ov.setDtInactive(rs.getTimestamp("dtInactive").toLocalDateTime());
				ov.setDoNotUse(rs.getBoolean("doNotUse"));
				/*
				if(rs.getTimestamp("convertedDt") != null)
					ov.setConvertedDt(rs.getTimestamp("convertedDt").toLocalDateTime());
				ov.setConvertedId(rs.getInt("convertedId"));
				ov.setConvertedStatus(rs.getString("convertedStatus"));
				*/
				
				videos.put(ov.getPkey(), ov);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);		
		return videos;
	}
	
	public void updateVideoConverted(OneVideo ov, String setArgs) {
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			dbc = "UPDATE videos set "+setArgs+" where pkey="+ov.getPkey();
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
		} catch (SQLException e) {
			log.info("Error updateVideoConverted ("+dbc+") "+e);
		}
		dbConnection.tryClose(conn);			
	}
	
	public void updateVideoCategory(Integer pkey, Integer videoSubCatId) {
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			dbc = "UPDATE videos set catsub="+videoSubCatId+" where pkey="+pkey;
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
		} catch (SQLException e) {
			log.info("Error updateVideoConverted ("+dbc+") "+e);
		}
		dbConnection.tryClose(conn);			
	}
	
	public void updateTitle(Integer pkey, String title) {
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call videos_updateTitle(?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_pkey", pkey);
			cs.setString("in_title", title);
			cs.execute();
		} catch (SQLException e) {
			log.info("Error updateTitle ("+dbc+") "+e);
		}
		dbConnection.tryClose(conn);			
	}
	
	public void updateDoNotUse(Integer pkey, Boolean doNotUse) {
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			int intDoNotUse;
			if(doNotUse)
				intDoNotUse=1;
			else
				intDoNotUse=0;
			dbc = "UPDATE videos set donotuse="+intDoNotUse+" where pkey="+pkey;
			java.sql.Statement stmt = conn.createStatement();
			stmt.execute(dbc);
		} catch (SQLException e) {
			log.info("Error updateVideoConverted ("+dbc+") "+e);
		}
		dbConnection.tryClose(conn);			
	}
	public Integer insertVideo(OneVideo ov) {
		Integer pkey=0;
		Connection conn = null;
		String dbc="";
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call videos_insert(?,?,?,?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_fn", ov.getFn());
			cs.setString("in_subdir", ov.getSubdir());
			cs.setInt("in_catsub", ov.getCatSub());
			cs.setString("in_title", ov.getTitle());
			cs.setString("in_thumbnail", ov.getThumbnail());
			cs.setLong("in_filesize", ov.getFilesize());
			cs.setFloat("in_length", ov.getLength());
			cs.registerOutParameter("out_pkey", java.sql.Types.INTEGER);
			
			cs.execute();		
			ov.setPkey(cs.getInt("out_pkey"));
			pkey=ov.getPkey();
		    
		} catch (SQLException e) {
			log.info("Error insertVideo ("+dbc+") "+e);
		}
		dbConnection.tryClose(conn);
		return pkey;
	}
}
