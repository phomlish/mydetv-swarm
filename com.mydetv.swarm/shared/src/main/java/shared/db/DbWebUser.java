package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.broadcast.SessionUser;
import shared.data.webuser.Failed;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.OneWebUserImage;
import shared.data.webuser.OneWebUserLogin;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.Password;
import shared.data.webuser.SQWebUser;
import shared.data.webuser.SecurityQuestion;

public class DbWebUser {

	@Inject private DbConnection dbConnection;
	@Inject private DbChats dbChats;
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbWebUser.class);

	public HashMap<String, Integer> insertWebUser(
			String username			
			,byte[] password
			,byte[] salt
			,String userTimezone
			,OneWebUserEmail email
			,Integer sqq1
			,byte[] sqa1
			,Integer sqq2
			,byte[] sqa2
			) {
		Connection conn=null;
		HashMap<String, Integer> rv = null;
		try {
			conn = dbConnection.getConnection();
			rv=insertWebUser(conn, username, password, salt, userTimezone, email, sqq1, sqa1, sqq2, sqa2);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}		
		dbConnection.tryClose(conn);
		return rv;
	}
	
	public HashMap<String, Integer> insertWebUser(
			Connection conn
			, String username			
			,byte[] password
			,byte[] salt
			,String userTimezone
			,OneWebUserEmail email
			,Integer sqq1
			,byte[] sqa1
			,Integer sqq2
			,byte[] sqa2
			) {

		HashMap<String, Integer> rv = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc;
			simpleProc= "{ call webUser_Insert (?,?,?,?,?,?,?,?,?,?,?,?  ,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setString("in_username", username);
			cs.setBytes("in_password", password);
			cs.setBytes("in_salt", salt);
			cs.setString("in_userTimezone", userTimezone);
			cs.setString("in_email", email.getEmail());
			cs.setString("in_code", email.getVerificationCode());
			cs.setString("in_uuid", email.getUuid());
			cs.setTimestamp("in_dtVcSent", Timestamp.valueOf(email.getDtVcSent()));
			cs.setInt("in_sqq1", sqq1);
			cs.setBytes("in_sqa1", sqa1);
			cs.setInt("in_sqq2", sqq2);
			cs.setBytes("in_sqa2", sqa2);

			cs.registerOutParameter("out_idWebUser", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_idEmail", java.sql.Types.INTEGER);
			
			cs.execute();
			
			Integer out_idWebUser = cs.getInt("out_idWebUser");
			Integer out_idEmail = cs.getInt("out_idEmail");
			log.info("out_idWebUser=" + out_idWebUser+",out_idEmail:"+out_idEmail);
			rv = new HashMap<String, Integer>();
			rv.put("idWebUser", out_idWebUser);
			rv.put("idEmail", out_idEmail);
			cs.close();
			return rv;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return rv;
	}

	public int insertUsername(Connection conn, Integer idWebUser, String username) {
		int idUsername = -1;
		try{
			String simpleProc;
			simpleProc= "{ call webUser_InsertUsername (?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setString("in_username", username);
			cs.setInt("in_idWebUser", idWebUser);
			cs.registerOutParameter("out_idUsername", java.sql.Types.INTEGER);

			cs.execute();
			idUsername = cs.getInt("out_idUsername");
			log.info("idUsername=" + idUsername);
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return idUsername;
	}

	public OneWebUserEmail getEmail(String strEmail) {
		Connection conn=null;
		OneWebUserEmail email = null;
		try {
			conn = dbConnection.getConnection();
			email = getEmail(conn, strEmail);
			return email;
		} catch (SQLException e) {
			log.error("e:"+e);
		}
		dbConnection.tryClose(conn);
		return email;
	}

	public OneWebUserEmail getEmail(Connection conn, String strEmail) {
		OneWebUserEmail email = null;
		log.trace("getting "+strEmail);
		try{
			String simpleProc = "{ call webUser_getEmail (? ,?,?,?,?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setString("in_email", strEmail);	

			cs.registerOutParameter("out_emailType", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_idEmail", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_idWebUser", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_code", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_uuid", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_dtVerified", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtAdded", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtVcSent", java.sql.Types.TIMESTAMP);
			cs.execute();

			Integer emailId=cs.getInt("out_idEmail");
			if(emailId!=null && emailId>0) {
				email=new OneWebUserEmail();
				email.setEmail(strEmail);
				email.setIdEmail(emailId);
				email.setIdWebUser(cs.getInt("out_idWebUser"));
				email.setEmailType(cs.getInt("out_emailType"));
				email.setVerificationCode(cs.getString("out_code"));
				email.setUuid(cs.getString("out_uuid"));
				email.setDtAdded(cs.getTimestamp("out_dtAdded").toLocalDateTime());
				Timestamp ts = cs.getTimestamp("out_dtVerified");
				if(ts!=null)
					email.setDtVerified(ts.toLocalDateTime());
				ts=cs.getTimestamp("out_dtVcSent");
				if(ts!=null)
					email.setDtVcSent(ts.toLocalDateTime());
			}

			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return email;
	}
	public OneWebUserEmail getEmailById(Integer emailId) {
		Connection conn=null;
		OneWebUserEmail email =null;
		try{
			conn = dbConnection.getConnection();
			email = getEmailById(conn, emailId);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return email;
		
	}
	public OneWebUserEmail getEmailById(Connection conn, Integer emailId) {
		OneWebUserEmail email =null;
		try{
			String simpleProc;
			simpleProc= "{ call webUser_GetEmailById (? ,?,?,?,?,?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_emailId", emailId);	

			cs.registerOutParameter("out_emailType", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_idEmail", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_idWebUser", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_code", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_uuid", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_dtVerified", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtAdded", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtVcSent", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_email", java.sql.Types.VARCHAR);

			cs.execute();
			email= new OneWebUserEmail();
			email.setEmail(cs.getString("out_email"));
			email.setIdEmail(cs.getInt("out_idEmail"));
			email.setIdWebUser(cs.getInt("out_idWebUser"));
			email.setEmailType(cs.getInt("out_emailType"));
			email.setVerificationCode(cs.getString("out_code"));
			email.setUuid(cs.getString("out_uuid"));
			
			Timestamp ts = cs.getTimestamp("out_dtAdded");
			if(ts!=null)
				email.setDtAdded(ts.toLocalDateTime());
			
			ts = cs.getTimestamp("out_dtVerified");
			if(ts!=null)
				email.setDtVerified(ts.toLocalDateTime());
			
			ts=cs.getTimestamp("out_dtVcSent");
			if(ts!=null)
				email.setDtVcSent(ts.toLocalDateTime());

			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return email;
	}

	public void deleteWebUser(Connection conn, Integer wuid) {
		try{
			String simpleProc;
			simpleProc= "{ call webUser_delete (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", wuid);
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	public void deleteEmail(OneWebUserEmail email) {
		Connection conn=null;
		try{
			conn = dbConnection.getConnection();
			deleteEmail(conn, email);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);		
	}
	public void deleteEmail(Connection conn, OneWebUserEmail email) {
		try{
			String simpleProc;
			simpleProc= "{ call webUser_deleteEmail (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idEmail", email.getIdEmail());
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}		
	}
	public TreeMap<Integer, OneWebUserEmail> getEmails(Integer idWebUser) {
		Connection conn=null;
		try{
			conn = dbConnection.getConnection();
			return getEmails(conn, idWebUser);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	public TreeMap<Integer, OneWebUserEmail> getEmails(Connection conn, Integer idWebUser) {
		TreeMap<Integer, OneWebUserEmail> emails =new TreeMap<Integer, OneWebUserEmail>();
		String simpleProc;
		simpleProc= "{ call webUser_getEmails (?) }";
		CallableStatement cs;
		try {
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_idWebUser", idWebUser);	
			
			Boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserEmail email = new OneWebUserEmail();
					
					email.setEmail(rs.getString("email"));
					email.setIdEmail(rs.getInt("idEmail"));
					email.setIdWebUser(rs.getInt("idWebUser"));
					email.setEmailType(rs.getInt("emailType"));
					email.setVerificationCode(rs.getString("verificationCode"));
					email.setUuid(rs.getString("uuid"));
					email.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
					Timestamp ts = rs.getTimestamp("dtVerified");
					if(ts!=null)
						email.setDtVerified(ts.toLocalDateTime());
					ts=rs.getTimestamp("dtVcSent");
					if(ts!=null)
						email.setDtVcSent(ts.toLocalDateTime());
					
					emails.put(email.getIdEmail(), email);
				}
				rs.close();
			}
			cs.close();
		} catch (SQLException e) {
			log.error("e:"+e);
		}
		return emails;
	}
	/**
	 * tests if username exists in db (converts to lowercase)
	 * @param username
	 * @return
	 */
	public Integer getUsername(String username) {
		Connection conn=null;
		Integer idUsername = -1;
		try {
			conn = dbConnection.getConnection();
			idUsername = getUsername(conn, username);
		} catch (SQLException e) {
			log.error("e:"+e);
		}
		dbConnection.tryClose(conn);
		return idUsername;
	}
	/**
	 * tests if username exists in db (converts to lowercase)
	 * @param username
	 * @return
	 */
	public Integer getUsername(Connection conn, String username) {
		int idUsername=-1;
		try {	
			String simpleProc = "{ call webUser_getUsername (? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setString("in_username", username);
			cs.registerOutParameter("out_idUsername", java.sql.Types.INTEGER);

			cs.execute();
			idUsername = cs.getInt("out_idUsername");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		log.info("idUsername=" + idUsername);
		return idUsername;
	}

	public OneWebUser getWebuser(Integer wuid) {
		Connection conn=null;
		try{
			conn = dbConnection.getConnection();			
			OneWebUser owu = getWebuser(conn, wuid);
			dbConnection.tryClose(conn);
			return owu;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	public OneWebUser getWebuser(Connection conn, Integer wuid) {
		OneWebUser webUser = null;
		CallableStatement cs;
		try {	
			String simpleProc = "{ call webUser_get (? ,?,?,?,?,?,?,?,?,?,?,?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_wuid", wuid);

			cs.registerOutParameter("out_passwordHash", java.sql.Types.BLOB);
			cs.registerOutParameter("out_passwordSalt",java.sql.Types.BLOB);
			cs.registerOutParameter("out_birthday",java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_username",java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_link",java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_userTimezone",java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_theme", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_dtAdded",java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtBanned",java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_dtMuted",java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_requireChange", java.sql.Types.INTEGER);
			cs.execute();

			webUser = new OneWebUser();
			webUser.setWuid(wuid);
			webUser.setPasswordHash(cs.getBytes("out_passwordHash"));
			webUser.setPasswordSalt(cs.getBytes("out_passwordSalt"));
			if(cs.getInt("out_requireChange")==0)
				webUser.setRequirePasswordChange(false);
			else
				webUser.setRequirePasswordChange(true);
			Date bday = cs.getDate("out_birthday");
			if(bday!=null)
				webUser.setBirthday(bday.toLocalDate());
			webUser.setLink(cs.getString("out_link"));
			webUser.setUserTimezone(cs.getString("out_userTimezone"));
			webUser.setTheme(cs.getString("out_theme"));
			webUser.setUserDtz(ZoneId.of(webUser.getUserTimezone()));
			webUser.setUsername(cs.getString("out_username"));
			webUser.setDtAdded(cs.getTimestamp("out_dtAdded").toLocalDateTime());
			Timestamp ts;
			ts=cs.getTimestamp("out_dtBanned");
			if(ts!=null)
				webUser.setDtBanned(ts.toLocalDateTime());
			ts=cs.getTimestamp("out_dtMuted");
			if(ts!=null)
				webUser.setDtMuted(ts.toLocalDateTime());
			
			webUser.setRoles(getWebUserRoles(conn, webUser.getWuid()));
			webUser.setImages(getWebUserImages(conn, webUser));
			webUser.setEmails(getEmails(conn, webUser.getWuid()));
			webUser.setChatBlocked(dbChats.getChatBlocked(conn, webUser));
			// don't do this now, wait until we've logged in
			webUser.setLogins(getWebUserLogin(conn, webUser.getWuid(), 100));

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}

		return webUser;
	}

	public Integer updateWebUser(SessionUser sessionUser, LocalDate bday, String link, String tz) {
		Integer rv=-1;
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call webUser_update(?,?,?,? ,?) }";
			CallableStatement cs;
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_webUserId", sessionUser.getWebUser().getWuid());
			cs.setDate("in_bday", Date.valueOf(bday));
			cs.setString("in_link", link);
			cs.setString("in_timezone", tz);
			cs.registerOutParameter("out_rowcount", java.sql.Types.INTEGER);
			cs.execute();
			rv=cs.getInt("out_rowcount");
		} catch(SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		log.info("rv:"+rv);
		return rv;
	}

	public TreeMap<Integer, OneWebUserRole> getWebUserRoles(Connection conn, Integer webUserId) {
		TreeMap<Integer, OneWebUserRole> webUserRoles = new TreeMap<Integer, OneWebUserRole>();
		CallableStatement cs;
		try {	
			String simpleProc = "{ call webUser_getRole (?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_webUserId", webUserId);			
			boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserRole oneWebUserRole = new OneWebUserRole();				
					oneWebUserRole.setIdWebUserRole(rs.getInt("idWebUserRole"));
					oneWebUserRole.setWebUserId(webUserId);
					oneWebUserRole.setDetails(rs.getString("details"));
					oneWebUserRole.setWebUserRoleType(rs.getInt("webUserRoleType"));					
					webUserRoles.put(oneWebUserRole.getIdWebUserRole(), oneWebUserRole);
				}
				rs.close();
			}

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return webUserRoles;
	}

	public Integer insertRole(Connection conn, OneWebUserRole or) {
		try {			
			String simpleProc = "{ call webUser_InsertRole(?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_webUserId", or.getWebUserId());
			cs.setString("in_details", or.getDetails());
			cs.setInt("in_webUserRoleType", or.getWebUserRoleType());
			
			cs.registerOutParameter("out_idWebUserRole", java.sql.Types.INTEGER);
			
			cs.execute();
			Integer out_idWebUserRole=cs.getInt("out_idWebUserRole");
			log.info("inserted:"+out_idWebUserRole);
			return out_idWebUserRole;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return -1;
	}
	
	public void rolesClearAll(Connection conn, Integer wuid) {
		try {			
			String simpleProc = "{ call webUser_updateRolesClearAll(?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_webUserId", wuid);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	/**
	 * used in DbWebUsersCheck when we're checking db people tables consistency
	 * @param conn
	 * @param idWebUserRole
	 */
	public void deleteRole(Connection conn, Integer idWebUserRole) {
		try {			
			String simpleProc = "{ call webUser_deleteRole(?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebUserRole", idWebUserRole);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	public Integer insertPassword(Integer idWebUser, byte[] password, Boolean requireChange) {
		Connection conn = null;
		Integer status = -100;
		try {
			conn = dbConnection.getConnection();
			status = insertPassword(conn, idWebUser, password, requireChange);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return status;
	}
	
	public Integer insertPassword(Connection conn, Integer idWebUser, byte[] password, Boolean requireChange) {
		CallableStatement cs;
		Integer status = -100;
		try {
			String simpleProc = "{ call webUser_insertPassword(?,?,? ,?) }";
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebuser", idWebUser);
			cs.setBytes("in_password", password);
			if(requireChange)
				cs.setInt("in_requireChange", 1);
			else
				cs.setInt("in_requireChange", 0);
			cs.registerOutParameter("out_status", java.sql.Types.INTEGER);
			cs.execute();
			status =cs.getInt("out_status");

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return status;
	}

	
	public HashMap<Integer, Password> getPasswords(Integer wuid) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			return getPasswords(conn, wuid);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	
	public HashMap<Integer, Password> getPasswords(Connection conn, Integer wuid) {
		CallableStatement cs;
	
		try {
			String simpleProc = "{ call webUser_getPasswords(?) }";
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebUser", wuid);
			boolean results = cs.execute();			
			if(results) {
				HashMap<Integer, Password> passwords = new HashMap<Integer, Password>();
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					Password password = new Password();
					password.setIdwebUser(rs.getInt("idWebUser"));
					password.setIdwebUserPassword(rs.getInt("idwebUserPassword"));
					password.setPasswordHash(rs.getBytes("passwordHash"));
					Timestamp ts = rs.getTimestamp("dtAdded");
					if(ts!=null)
						password.setDtAdded(ts.toLocalDateTime());
					ts = rs.getTimestamp("dtActive");
					if(ts!=null)
						password.setDtActive(ts.toLocalDateTime());
					ts = rs.getTimestamp("dtInactive");
					if(ts!=null)
						password.setDtInactive(ts.toLocalDateTime());
					int requireChange = rs.getInt("requireChange");
					if(requireChange==0)
						password.setRequireChange(false);
					else
						password.setRequireChange(true);
					passwords.put(password.getIdwebUserPassword(), password);
				}
				return passwords;
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return null;
	}
	
	/**
	 * Gets ALL recent failed attempts (shared.data.webuser.Failed)<br>
	 *  *  (the database sproc automatically removes old failures hard coded at 20 minutes.)<br>
	 * processing all enables us to lock the login system<br>
	 * @param conn
	 * @param failedType
	 * @return
	 */
	public ArrayList<Failed> getFailed(Connection conn) {
		CallableStatement cs;
		ArrayList<Failed> allfailed = null;
		try {
			String simpleProc = "{ call webUser_getFailed() }";
			cs = conn.prepareCall(simpleProc);

			boolean results = cs.execute();			
			if(results) {
				allfailed = new ArrayList<Failed>();
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					Failed failed = new Failed();
					failed.setIdwebUserFailed(rs.getInt("idwebUserFailed"));
					failed.setIdWebUser(rs.getInt("idWebUser"));
					failed.setDtFailedAttempt(rs.getTimestamp("dtFailedAttempt").toLocalDateTime());
					failed.setFailedType(rs.getInt("failedType"));
					allfailed.add(failed);
				}
				rs.close();
			}

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return allfailed;
	}

	public Integer insertEmail(Connection conn, OneWebUserEmail email) {
		CallableStatement cs;
		Integer out_idEmail=-1;
		try {
			String simpleProc = "{ call webUser_InsertEmail(?,?,?,?,? ,?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_idWebUser", email.getIdWebUser());
			cs.setString("in_email", email.getEmail());
			cs.setString("in_code", email.getVerificationCode());
			cs.setString("in_uuid", email.getUuid());
			cs.setTimestamp("in_dtVcSent", Timestamp.valueOf(email.getDtVcSent()));
			
			cs.registerOutParameter("out_idEmail", java.sql.Types.INTEGER);
			
			cs.execute();
			out_idEmail=cs.getInt("out_idEmail");
			log.info("inserted:"+out_idEmail);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return out_idEmail;
	}
	/**
	 * insert a dummy user to track how many times we're possibly being attacked
	 * @param conn
	 * @param failedType
	 * @return
	 */
	public Integer insertFailed(Connection conn, Integer failedType) {
		CallableStatement cs;
		Integer failed=-1;
		try {
			String simpleProc = "{ call webUser_insertFailed(?,? ,?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_idWebUser", 0);	
			cs.setInt("in_failedType", failedType);
			
			cs.registerOutParameter("out_idwebUserFailed", java.sql.Types.INTEGER);
			cs.execute();
			failed=cs.getInt("out_idwebUserFailed");
			log.info("inserted:"+failed);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return failed;
	}
	
	public Integer insertFailed(Connection conn, OneWebUser webUser, Integer failedType) {
		CallableStatement cs;
		Integer failed=-1;
		try {
			String simpleProc = "{ call webUser_insertFailed(?,? ,?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_idWebUser", webUser.getWuid());
			cs.setInt("in_failedType", failedType);
			
			cs.registerOutParameter("out_idwebUserFailed", java.sql.Types.INTEGER);
			cs.execute();
			failed=cs.getInt("out_idwebUserFailed");
			log.info("inserted:"+failed);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return failed;
	}

	public Integer insertFailed(SessionUser sessionUser, Integer failedType) {
		CallableStatement cs;
		Integer failed=-1;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call webUser_insertFailed(?,? ,?) }";
			cs = conn.prepareCall(simpleProc);

			cs.setInt("in_idWebUser", sessionUser.getWebUser().getWuid());
			cs.setInt("in_failedType", failedType);
			
			cs.registerOutParameter("out_idwebUserFailed", java.sql.Types.INTEGER);
			cs.execute();
			failed=cs.getInt("out_idwebUserFailed");
			log.info("inserted:"+failed);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return failed;
	}
	
	public void webUser_updateFailedClear(Connection conn, Integer idWebUser) {
		CallableStatement cs;
		try {
			String simpleProc = "{ call webUser_updateFailedClearWebUser(?) }";
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebUser", idWebUser);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	public void updateEmailVerified(Integer idEmail) {	
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			updateEmailVerified(conn, idEmail);
		} catch(SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}

	public void updateEmailVerified(Connection conn, Integer idEmail) {
		CallableStatement cs;
		try {
			String simpleProc = "{ call webUser_updateEmailVcVerified(?) }";
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idEmail", idEmail);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}

	public void updateEmailVcSent(OneWebUserEmail email) {	
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			updateEmailVcSent(conn, email);
		} catch(SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}

	public void updateEmailVcSent(Connection conn, OneWebUserEmail email) {
		CallableStatement cs;
		try {
			String simpleProc = "{ call webUser_updateEmailVcSent(?,?,?,?) }";
			cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idEmail", email.getIdEmail());
			cs.setString("in_verificationCode", email.getVerificationCode());
			cs.setString("in_uuid", email.getUuid());
			cs.setTimestamp("in_dtVcSent", Timestamp.valueOf(email.getDtVcSent()));
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	public HashMap<Integer, SecurityQuestion> getSecurityQuestions() {
		Connection conn=null;
		HashMap<Integer, SecurityQuestion> questions = null;
		try {
			conn = dbConnection.getConnection();
			questions = getSecurityQuestions(conn);
		} catch(SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return questions;
	}
	
	public HashMap<Integer, SecurityQuestion> getSecurityQuestions(Connection conn) {
		HashMap<Integer, SecurityQuestion> questions = null;
		try {	
			String simpleProc = "{ call securityQuestions_get () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			Boolean results = cs.execute();
			if(results) {
				questions = new HashMap<Integer, SecurityQuestion>();
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					SecurityQuestion question = new SecurityQuestion();
					question.setIdSecurityQuestion(rs.getInt("idSecurityQuestion"));
					question.setQuestion(rs.getString("question"));
					question.setQgroup(rs.getInt("qgroup"));
					Timestamp ts = rs.getTimestamp("dtInactive");
					if(ts != null) 
						question.setDtInactive(ts.toLocalDateTime());
					questions.put(question.getIdSecurityQuestion(), question);
				}
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return questions;
	}
	
	public SQWebUser getSecurityQuestionsAndForgotPasswordStuffForWebuser(Integer wuid, Integer emailId) {
		Connection conn=null;
		SQWebUser sqw = null;
		try {
			conn = dbConnection.getConnection();
			sqw=getSecurityQuestionsAndForgotPasswordStuffForWebuser(conn, wuid, emailId);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return sqw;		
	}
	public SQWebUser getSecurityQuestionsAndForgotPasswordStuffForWebuser(Connection conn, Integer wuid, Integer emailId) {
		SQWebUser sqw = null;
		try {	
			String simpleProc = "{ call webUser_getSQ (?,? ,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", wuid);
			cs.setInt("in_idEmail", emailId);
			
			cs.registerOutParameter("out_email", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_salt", java.sql.Types.BLOB);
			cs.registerOutParameter("out_sqq1", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_sqa1", java.sql.Types.BLOB);
			cs.registerOutParameter("out_question1", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_sqg1", java.sql.Types.INTEGER);
			
			cs.registerOutParameter("out_sqq2", java.sql.Types.INTEGER);
			cs.registerOutParameter("out_sqa2", java.sql.Types.BLOB);
			cs.registerOutParameter("out_question2", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_sqg2", java.sql.Types.INTEGER);
			
			cs.registerOutParameter("out_forgotPassword", java.sql.Types.VARCHAR);
			cs.registerOutParameter("out_dtForgotPassword", java.sql.Types.TIMESTAMP);
			cs.registerOutParameter("out_uuidForgotPassword", java.sql.Types.VARCHAR);
			
			cs.execute();
			
			sqw = new SQWebUser();
			
			sqw.setEmail(cs.getString("out_email"));
			sqw.setSalt(cs.getBytes("out_salt"));
			
			sqw.setSqq1(cs.getInt("out_sqq1"));
			sqw.setSqa1(cs.getBytes("out_sqa1"));
			sqw.setQuestion1(cs.getString("out_question1"));
			sqw.setSqq1(cs.getInt("out_sqq1"));
			
			sqw.setSqq2(cs.getInt("out_sqq2"));
			sqw.setSqa2(cs.getBytes("out_sqa2"));
			sqw.setQuestion2(cs.getString("out_question2"));
			sqw.setSqq2(cs.getInt("out_sqq2"));
			
			sqw.setForgotPassword(cs.getString("out_forgotPassword"));
			Timestamp ts = cs.getTimestamp("out_dtForgotPassword");
			if(ts!=null)
				sqw.setDtForgotPassword(ts.toLocalDateTime());
			sqw.setUuidForgotEmail(cs.getString("out_uuidForgotPassword"));
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return sqw;
	}
	
	public void webUser_updateSQ(Connection conn, SQWebUser sqw) {
		
		try {
			String simpleProc = "{ call webUser_updateSQ (?,?,?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebuser", sqw.getIdWebUser());
			cs.setInt("in_sqq1", sqw.getSqq1());
			cs.setInt("in_sqq2", sqw.getSqq2());
			cs.setBytes("in_sqa1", sqw.getSqa1());
			cs.setBytes("in_sqa2", sqw.getSqa2());
			cs.execute();
		} catch (SQLException e) {
			log.error("e:"+e);
		}
	}
	/**
	 * Update webUser unlockPassword fields
	 * @param wuid
	 * @param up
	 * @param uuid
	 */
	public void updateUnlockPassword(Integer wuid, String up, String uuid) {
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			updateUnlockPassword(conn, wuid, up, uuid);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	/**
	 * Update webUser unlockPassword fields
	 * @param conn
	 * @param wuid
	 * @param up
	 * @param uuid
	 */
	public void updateUnlockPassword(Connection conn, Integer wuid, String up, String uuid) {
		try {	
			String simpleProc = "{ call webUser_updateUnlockPassword (?,?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", wuid);
			cs.setString("in_forgotPassword", up);
			cs.setString("in_uuidForgotPassword", uuid);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	public void updateUnlockPasswordClear(Connection conn, Integer wuid) {
		try {	
			String simpleProc = "{ call webUser_updateUnlockPasswordClear (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", wuid);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
public void insertWebUserImage(OneWebUserImage webUserImage) {
		//a152d68d-32aa-4333-8414-34e7aa6f5daa.png
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc= "{ call webUser_insertImage (?,?,?,?  ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_webUserId", webUserImage.getIdWebUser());
			cs.setString("in_origFilename", webUserImage.getOrigFilename());
			cs.setBytes("in_origFile", webUserImage.getOrigFile());
			cs.setString("in_newFn", webUserImage.getNewFn());
			cs.registerOutParameter("out_idWebUserImage", java.sql.Types.INTEGER);
			cs.execute();
			webUserImage.setIdWebUserImage(cs.getInt("out_idWebUserImage"));
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
	}
	
	public TreeMap<Integer, OneWebUserImage> getWebUserImages(OneWebUser webUser) {
		Connection conn = null;	
		try {
			conn = dbConnection.getConnection();
			return(getWebUserImages(conn, webUser));
		} catch(SQLException e) {
			log.error("failed e:"+e);
			
		} finally {
			dbConnection.tryClose(conn);
		}
		return null;
	}
	
	public TreeMap<Integer, OneWebUserImage> getWebUserImages(Connection conn, OneWebUser webUser) {
			
		TreeMap<Integer, OneWebUserImage> webUserImages = new TreeMap<Integer, OneWebUserImage>();
		try {
			String simpleProc= "{ call webUser_getImages (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_wuid", webUser.getWuid());
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
	        	   OneWebUserImage oneWebUserImage = new OneWebUserImage();
	        	   oneWebUserImage.setIdWebUser(webUser.getWuid());
	        	   oneWebUserImage.setIdWebUserImage(rs.getInt("idWebUserImage"));
	        	   oneWebUserImage.setOrigFilename(rs.getString("origFilename"));
	        	   oneWebUserImage.setOrigFile(rs.getBytes("origFile"));
	        	   oneWebUserImage.setNewFn(rs.getString("newFn"));
	        	   Integer is = rs.getInt("imageSelected");
	        	   if(is!=null && is.equals(1)) {
	        		   webUser.setWebUserSelectedImage(oneWebUserImage.getIdWebUserImage());
	        		   oneWebUserImage.setImageSelected(true);
	        	   }
	        	   else
	        		   oneWebUserImage.setImageSelected(false);
	        	   webUserImages.put(oneWebUserImage.getIdWebUserImage(),oneWebUserImage);
	               }
	            rs.close();
	            webUser.setImages(webUserImages);
			}
			
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return webUserImages;
	}
	
	public TreeMap<Integer, OneWebUserImage> getWebUserImagesPlusInactive(OneWebUser webUser) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			TreeMap<Integer, OneWebUserImage> webUserImages = new TreeMap<Integer, OneWebUserImage>();

			String simpleProc= "{ call webUser_getImagesPlusInactive (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_wuid", webUser.getWuid());
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
	        	   OneWebUserImage oneWebUserImage = new OneWebUserImage();
	        	   oneWebUserImage.setIdWebUser(webUser.getWuid());
	        	   oneWebUserImage.setIdWebUserImage(rs.getInt("idWebUserImage"));
	        	   oneWebUserImage.setOrigFilename(rs.getString("origFilename"));
	        	   oneWebUserImage.setOrigFile(rs.getBytes("origFile"));
	        	   oneWebUserImage.setNewFn(rs.getString("newFn"));
	        	   Timestamp ts;
	        	   ts = rs.getTimestamp("dtAdded");
	        	   if(ts!=null)
	        		   oneWebUserImage.setDtAdded(ts.toLocalDateTime());
	        	   ts = rs.getTimestamp("dtInactive");
	        	   if(ts!=null)
	        		   oneWebUserImage.setDtInactive(ts.toLocalDateTime());
	        	   Integer is = rs.getInt("imageSelected");
	        	   if(is!=null && is.equals(1)) {
	        		   webUser.setWebUserSelectedImage(oneWebUserImage.getIdWebUserImage());
	        		   oneWebUserImage.setImageSelected(true);
	        	   }
	        	   else
	        		   oneWebUserImage.setImageSelected(false);
	        	   webUserImages.put(oneWebUserImage.getIdWebUserImage(),oneWebUserImage);
	               }
	            rs.close();          
			}
			
			cs.close();
			dbConnection.tryClose(conn);
			return webUserImages;
		} catch (SQLException e) {
			log.error("failed e:"+e);
			dbConnection.tryClose(conn);
		}
		return null;
	}
	
	public void deleteWebUserImage(OneWebUserImage webUserImage) {	
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			deleteWebUserImage(conn, webUserImage);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
	}
	public void deleteWebUserImage(Connection conn, OneWebUserImage webUserImage) {	
		try {
			String simpleProc= "{ call webUser_deleteImage (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_webUserImageId", webUserImage.getIdWebUserImage());
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	
	public void updateWebUserImage(OneWebUserImage webUserImage) {	
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc= "{ call webUser_updateImage (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_webUserImageId", webUserImage.getIdWebUserImage());
			cs.setInt("in_webUserId", webUserImage.getIdWebUser());
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
	}
	
	public void insertLogin(Connection conn, OneWebUser webUser, String remoteAddress) {	
		try {
			String simpleProc= "{ call webUser_InsertLogin (?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_wuid", webUser.getWuid());
			cs.setString("in_remoteAddress", remoteAddress);
			cs.registerOutParameter("out_idWebUserLogin", java.sql.Types.INTEGER);
			cs.execute();
			webUser.setIdWebUserLogin(cs.getInt("out_idWebUserLogin"));
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	public void updateLogin(SessionUser su, String reason) {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			updateLogin(conn, su, reason);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
	}
	public void updateLogin(Connection conn, SessionUser su, String reason) {	
		try {
			String simpleProc= "{ call webUser_updateLogin (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idWebUserLogin", su.getWebUser().getIdWebUserLogin());
			cs.setString("in_logoutReason", reason);
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
	}
	public void updateLoginRestart() {	
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc= "{ call webUser_updateLoginRestart () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.execute();
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} finally {
			dbConnection.tryClose(conn);
		}
	}
	
	public TreeMap<LocalDateTime, OneWebUserLogin> getWebUserLogin(Connection conn, Integer wuid, Integer limit) {
		
		TreeMap<LocalDateTime, OneWebUserLogin> logins = new TreeMap<LocalDateTime, OneWebUserLogin>();
		try {
			String simpleProc= "{ call webUser_getLogin (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_wuid", wuid);
			cs.setInt("in_limit", limit);
			
			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserLogin login = new OneWebUserLogin();
					login.setIdWebUserLogin(rs.getInt("idWebUserLogin"));
					login.setDtLogin(rs.getTimestamp("dtLogin").toLocalDateTime());
					login.setRemoteAddress(rs.getString("remoteAddress"));
					Timestamp ts = rs.getTimestamp("dtLogout");
					if(ts!=null) {
						login.setDtLogout(ts.toLocalDateTime());
						login.setLogoutReason(rs.getString("logoutReason"));
					}
					logins.put(login.getDtLogin(), login);
	            }
	            rs.close();
			}
			
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return logins;
	}
	
}
