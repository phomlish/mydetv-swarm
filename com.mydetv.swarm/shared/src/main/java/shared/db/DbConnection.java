package shared.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import shared.data.OneDbConfig;
import shared.data.Config;

public class DbConnection {
	
	private static final Logger log = LoggerFactory.getLogger(DbConnection.class);	
	@Inject private Config sharedConfig;
	
	public void initDataSource() {
	
		try {
			OneDbConfig dbConfig = sharedConfig.getDbConfig();
			String driverName = null;
			log.info("initializeing mydetv db connection");	
		    String driver = dbConfig.getDbDriver();
		    String url = dbConfig.getDbUrl();
		    String username = dbConfig.getDbUsername();
		    String password = dbConfig.getDbPassword();

			if(driver.compareTo("mysql") == 0)
				driverName = "com.mysql.cj.jdbc.Driver";
			else if(driver.compareTo("mssql") ==0 )
				driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			else {
				log.error("no driver given for mydetv database");
				throw new InstantiationException("no driver");
			}

			log.info("setting up database connection to mydetv db");

			DriverAdapterCPDS cpds =  new DriverAdapterCPDS();
			cpds.setDriver(driverName);
			url +=  "?useLegacyDatetimeCode=false"
					+ "&useInformationSchema=true"
					+ "&characterEncoding=utf8"
					+ "&connectionCollation=utf8mb4_unicode_ci"
					+ "&serverTimezone=UTC"
					+ "&requireSSL=false&useSsl=false&verifyServerCertificate=false"
					+ "&maxActive=100"
					+ "&maxIdle=100"
					//+"poolPreparedStatements=true"
					;
			//if(dbConfig.useSsl()) 
				//url+= "&requireSSL=true&useSsl=true";
			
			//https://www.slideshare.net/psteitz/apachecon2014-pooldbcp
			cpds.setUrl(url);
			cpds.setUser(username);
			cpds.setPassword(password);
			SharedPoolDataSource sharedPoolDataSource = sharedConfig.getDbConfig().getSharedPoolDataSource();
			sharedPoolDataSource = new SharedPoolDataSource();
			sharedPoolDataSource.setConnectionPoolDataSource(cpds);
			sharedPoolDataSource.setMaxTotal(100);
			sharedPoolDataSource.setDefaultMaxIdle(15);
			sharedPoolDataSource.setDefaultMaxWaitMillis(10000);
			
			sharedConfig.getDbConfig().setSharedPoolDataSource(sharedPoolDataSource);
			log.info("db connection completed to " + url);
			

		} catch (Exception e) {
			log.error("Error setting up mydetv db: " + e);
		}
	}

	public Connection getConnection() throws SQLException {
		if(sharedConfig.getDbConfig().getSharedPoolDataSource() == null) 
			initDataSource();

		Boolean connected = false;
		Connection conn = null;
		int cnt=0;
		while(!connected) {
			cnt++;
			try {
				conn = sharedConfig.getDbConfig().getSharedPoolDataSource().getConnection();
				if(conn == null)
					log.error("Error: Failed to obtain a connection (DataSource has not been initialized)");
				else if(!conn.isValid(1000))
					log.error("dbconn: is not valid");
				else
					connected=true;
				
				if(!connected) {
					log.error("db not connected, sleeping 1/10 second");
					sleep(100);
				}
			} catch(CommunicationsException ex) {
				log.error("exception connecting on cnt "+cnt);
			}
			if(cnt>5) 
				throw new CommunicationsException("unable to connect", null);
			
		}
		if(cnt>1) log.error("took "+cnt+" tries to get connected");
		return conn;
	}

	public void tryClose(Connection conn) {
		if(conn==null)
			return;
		try {
			conn.close();
		} catch (SQLException e) {
			log.error("Error closing connection");
		}
	}
	
	public void printDriverStats() {
		log.info("st  db connection pool active: " + sharedConfig.getDbConfig().getSharedPoolDataSource().getNumActive() 
		+ ", idle: " + sharedConfig.getDbConfig().getSharedPoolDataSource().getNumIdle());
	}
	private void sleep(int howlong) {
		try {
			Thread.sleep(howlong);
		} catch (InterruptedException e) {
			log.info("sleep interupgted");
		}
	}

}
