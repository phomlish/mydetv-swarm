package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneArchive;

public class DbArchive {

	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbArchive.class);
	
	public Integer archiveInsert(String site, String siteUrl, Integer wuid, String pretitle) {
		Integer rv = 0;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call archive_insert(?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setString("in_site", site);
			cs.setString("in_siteUrl", siteUrl);
			cs.setInt("in_wuid", wuid);
			cs.setString("in_pretitle", pretitle);
			cs.registerOutParameter("out_insertedId", java.sql.Types.INTEGER);
			cs.execute();
			rv=cs.getInt("out_insertedId");
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return rv;
	}
	
	/**
	 * gets ALL archives from the table, suitable for doing maint in every archive<br>
	 * @return
	 */
	public TreeMap<Integer, OneArchive> get() {
		TreeMap<Integer, OneArchive> archives = new TreeMap<Integer, OneArchive>();
		
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call archive_getAll () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneArchive oa = new OneArchive();
				oa.setIdArchive(rs.getInt("idArchive"));
				oa.setWuid(rs.getInt("wuid"));
				oa.setSite(rs.getString("site"));
				oa.setSiteUrl(rs.getString("siteUrl"));
				oa.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
				Timestamp ts = rs.getTimestamp("dtReceived");
				if(ts != null)
					oa.setDtReceived(ts.toLocalDateTime());
				oa.setVideoFn(rs.getString("videoFn"));
				oa.setVideoPath(rs.getString("videoPath"));
				oa.setFilesize(rs.getLong("filesize"));
				oa.setDetails(rs.getString("details"));
				oa.setErrorReason(rs.getString("errorReason"));
				oa.setPretitle(rs.getString("pretitle"));
				archives.put(oa.getIdArchive(), oa);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return archives;
	}
	
	public Integer archiveUpdate(OneArchive oa) {
		Integer rv = 0;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call archive_update(?,?,?,?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_idArchive", oa.getIdArchive());
			cs.setString("in_videoFn", oa.getVideoFn());
			cs.setString("in_videoPath", oa.getVideoPath());
			cs.setLong("in_filesize", oa.getFilesize());
			cs.setString("in_details", oa.getDetails());
			cs.setString("in_errorReason", oa.getErrorReason());
			cs.setString("in_thumbnail", oa.getThumbnail());
			
			cs.registerOutParameter("out_rowCount", java.sql.Types.INTEGER);
			
			cs.execute();
			rv=cs.getInt("out_rowCount");
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return rv;
	}

}
