package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneChannelSchedule;

public class DbChannelSchedule {
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DbChannelSchedule.class);
	@Inject private DbConnection dbConnection;
	
	public HashMap<Integer, OneChannelSchedule> getAll(Integer channelsId) {
		Connection conn = null;	
		HashMap<Integer, OneChannelSchedule> channelSchedules = new HashMap<Integer, OneChannelSchedule>();
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelSchedule_get (?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelsId", channelsId);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				OneChannelSchedule os = new OneChannelSchedule();
				os.setIdchannelSchedule(rs.getInt("idchannelSchedule"));
				os.setChannelsId(channelsId);
				os.setPkey(rs.getInt("pkey"));
				channelSchedules.put(os.getIdchannelSchedule(), os);
			}
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} catch (Exception e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return channelSchedules;
	}
	
	public void insert(OneChannelSchedule ocs) {
		Connection conn = null;	
		Integer out_insertedId = 0;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelSchedule_Insert (?,?  ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_channelsId", ocs.getChannelsId());
			cs.setInt("in_pkey", ocs.getPkey());		
			cs.registerOutParameter("out_insertedId", java.sql.Types.INTEGER);
			
			cs.execute();
			out_insertedId = cs.getInt("out_insertedId");
		    log.debug("out_insertedId=" + out_insertedId);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} catch (Exception e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		
		if(out_insertedId>0)
			ocs.setIdchannelSchedule(out_insertedId);
		
	}
	
	public void delete(OneChannelSchedule ocs) {
		Connection conn = null;	
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelSchedule_Delete (?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			cs.setInt("in_channelsId", ocs.getChannelsId());
			cs.setInt("in_idChannelSchedule", ocs.getIdchannelSchedule());
			
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		} catch (Exception e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}

}
