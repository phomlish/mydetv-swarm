package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;


public class DbChannels {
	@Inject private DbConnection dbConnection;
	private static final Logger log = LoggerFactory.getLogger(DbChannels.class);

	public TreeMap<Integer, OneMydetvChannel> getChannels() {
		TreeMap<Integer, OneMydetvChannel> channelsMap = new TreeMap<Integer, OneMydetvChannel>();
		Connection conn = null;
		try {
			
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channels_get () }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				int channelId = rs.getInt("channels_id");
				int channelType = rs.getInt("channelType");
				log.trace("channelId:"+channelId);
				OneMydetvChannel channel = new OneMydetvChannel();
				channel.setChannelId(channelId);
				channel.setName(rs.getString("name"));
				channel.setActive(rs.getBoolean("active"));
				if(rs.getBoolean("restricted"))
					channel.setRestricted(true);
				channel.getOjClient().setJanusId(rs.getInt("janusId"));
				channel.getOjClient().setJanusAudioPort(rs.getInt("janusAudioPort"));
				channel.getOjClient().setJanusVideoPort(rs.getInt("janusVideoPort"));
				channel.getOjClient().setJanusSecret(rs.getString("janusSecret"));
				channel.getOjClient().setJanusAdminKey(rs.getString("janusAdminKey"));
				channel.setChannelType(channelType);
				channelsMap.put(channel.getChannelId(), channel);
			}

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return channelsMap;
	}
	
	public Integer addWebUser(SessionUser sessionUser) {
		Integer rv = 0;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelWebUsers_Enter(?,?,?,?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelId", sessionUser.getMydetvChannel().getChannelId());
			cs.setInt("in_webUserId", sessionUser.getWebUser().getWuid());
			cs.setString("in_detectRtc", sessionUser.getDetectRTC().toString());
			cs.setString("in_remote", sessionUser.getWebSocketConnection().getRemote());
			cs.setLong("in_datafileDownloadTime", sessionUser.getDatafileDownloadTime());
			LocalDateTime ldt = sessionUser.getWebSocketConnection().getConnectedDate();
			cs.setTimestamp("in_dt", Timestamp.valueOf(ldt));
			cs.registerOutParameter("out_ChannelWebUsersId", java.sql.Types.INTEGER);
			cs.execute();
			rv=cs.getInt("out_ChannelWebUsersId");
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return rv;
	}
	
	public void removeWebUserFromChannel(SessionUser sessionUser, Timestamp dt) {
		OneWebUser webUser = sessionUser.getWebUser();
		if(webUser==null) {
			log.error("Error, web user is already null");
			return;
		}
		if(webUser.getDbChannelWebUsersId()==0) {
			log.error("webUser's channel is already 0");
			return;
		}
		log.info("removeWebUser "+webUser.getDbChannelWebUsersId()+" "+dt.toString());
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelWebUsers_Exit(?,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelWebUsersId", webUser.getDbChannelWebUsersId());
			cs.setTimestamp("in_dt", dt);
			cs.execute();
			
		} catch (SQLException e) {
			log.error("failed SQLExceptione:"+e);
		} catch(Exception e) {
			log.error("failed Exception:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void resetChannelWebUsers() {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channelWebUsers_Reset() }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.execute();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public Integer updateName(Integer channelId, String channelName) {
		Integer outUpdated = 0;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channel_updateName(?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelsId", channelId);
			cs.setString("in_channelName", channelName);
			cs.registerOutParameter("out_updated", java.sql.Types.INTEGER);
			cs.execute();
			outUpdated = cs.getInt("out_updated");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return outUpdated;
	}
	
	public Integer updateActive(Integer channelId, Boolean channelActive) {
		Integer outUpdated = 0;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String simpleProc = "{ call channel_updateActive(?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_channelsId", channelId);
			cs.setBoolean("in_channelActive", channelActive);
			cs.registerOutParameter("out_updated", java.sql.Types.INTEGER);
			cs.execute();
			outUpdated = cs.getInt("out_updated");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return outUpdated;
	}
	
	
}
