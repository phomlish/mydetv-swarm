package shared.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.OneWebUserImage;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.OneWebUserUsername;

public class DbWebUsers {

	@Inject private DbConnection dbConnection;	
	private static final Logger log = LoggerFactory.getLogger(DbWebUsers.class);
	
	public TreeMap<Integer, OneWebUser> getWebUsers() {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			return getWebUsers(conn);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	public TreeMap<Integer, OneWebUser> getWebUsers(Connection conn) {
			
		TreeMap<Integer, OneWebUser> webUsers = new TreeMap<Integer, OneWebUser>();
		try {
			CallableStatement cs;
			
			String simpleProc = "{ call webUser_getAll () }";
			cs = conn.prepareCall(simpleProc);
			boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUser owu = new OneWebUser();
					owu.setWuid(rs.getInt("idWebUser"));
					owu.setUserTimezone(rs.getString("userTimezone"));
					owu.setUserDtz(ZoneId.of(owu.getUserTimezone()));
					Date bday = rs.getDate("birthday");
					if(bday!=null)
						owu.setBirthday(bday.toLocalDate());
					owu.setLink(rs.getString("link"));
					owu.setUsername(rs.getString("username"));

					owu.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
					Timestamp ts;
					ts=rs.getTimestamp("dtBanned");
					if(ts!=null) owu.setDtBanned(ts.toLocalDateTime());
					webUsers.put(owu.getWuid(), owu);
				}
				rs.close();
				
				log.info("read "+webUsers.size()+" webUsers");

				return webUsers;
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
			
		return null;
	}
	
	public TreeMap<Integer, OneWebUserEmail> getEmails(Connection conn) {			
		TreeMap<Integer, OneWebUserEmail> emails = new TreeMap<Integer, OneWebUserEmail>();
		
		try {
			CallableStatement cs;
			String simpleProc = "{ call webUser_getEmailsAll () }";
			cs = conn.prepareCall(simpleProc);
			boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserEmail oe = new OneWebUserEmail();
					oe.setIdEmail(rs.getInt("idEmail"));
					oe.setIdWebUser(rs.getInt("idWebUser"));
					oe.setEmail(rs.getString("email"));
					oe.setEmailType(rs.getInt("emailType"));
					oe.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
					Timestamp ts;
					ts=rs.getTimestamp("dtVerified");
					if(ts!=null) oe.setDtVerified(ts.toLocalDateTime());
					emails.put(oe.getIdEmail(), oe);
				}
				rs.close();
				log.info("read "+emails.size()+" emails");	
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}	
		return emails;
	}
	
	public TreeMap<Integer, OneWebUserUsername> getUsernames() {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			return getUsernames(conn);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	
	public TreeMap<Integer, OneWebUserUsername> getUsernames(Connection conn) {
				
		TreeMap<Integer, OneWebUserUsername> usernames = new TreeMap<Integer, OneWebUserUsername>();
		try {
			CallableStatement cs;
			String simpleProc = "{ call webUser_getUsernamesAll () }";
			cs = conn.prepareCall(simpleProc);
			boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserUsername ou = new OneWebUserUsername();
					ou.setIdUsername(rs.getInt("idUsername"));
					ou.setIdWebUser(rs.getInt("idWebUser"));
					ou.setUsername(rs.getString("username"));
					ou.setDtAdded(rs.getTimestamp("dtAdded").toLocalDateTime());
					usernames.put(ou.getIdUsername(), ou);
				}
				rs.close();
				log.info("read "+usernames.size()+" usernames");
				return usernames;
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}	
		return null;
	}
		
	public TreeMap<Integer, OneWebUserRole> getRoles() {
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			return getRoles(conn);
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}
	
	public TreeMap<Integer, OneWebUserRole> getRoles(Connection conn) {			
		TreeMap<Integer, OneWebUserRole> roles = new TreeMap<Integer, OneWebUserRole>();
		
		try {
			String simpleProc = "{ call webUser_getRoleAll () }";
			CallableStatement cs;
			cs = conn.prepareCall(simpleProc);
			boolean results = cs.execute();
			if(results) {
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
					OneWebUserRole oneWebUserRole = new OneWebUserRole();
					oneWebUserRole.setWebUserId(rs.getInt("webUserId"));
					oneWebUserRole.setIdWebUserRole(rs.getInt("idWebUserRole"));
					oneWebUserRole.setDetails(rs.getString("details"));
					oneWebUserRole.setWebUserRoleType(rs.getInt("webUserRoleType"));					
					roles.put(oneWebUserRole.getIdWebUserRole(), oneWebUserRole);
				}
				rs.close();
				log.info("read "+roles.size()+" roles");
				return roles;
			}
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return null;
	}
	
	public TreeMap<Integer, OneWebUserImage> getWebUserImages(Connection conn) {
		
		TreeMap<Integer, OneWebUserImage> webUserImages = new TreeMap<Integer, OneWebUserImage>();
		try {
			String simpleProc= "{ call webUser_getImagesAll () }";
			CallableStatement cs = conn.prepareCall(simpleProc);

			boolean results = cs.execute();
			if(results) {
				
				ResultSet rs = cs.getResultSet();
				while (rs.next()) {
	        	   OneWebUserImage image = new OneWebUserImage();
	        	   image.setIdWebUser(rs.getInt("webUserId"));
	        	   image.setIdWebUserImage(rs.getInt("idWebUserImage"));
	        	   image.setOrigFilename(rs.getString("origFilename"));
	        	   image.setOrigFile(rs.getBytes("origFile"));
	        	   
	        	   Integer is = rs.getInt("imageSelected");
	        	   if(is!=null && is.equals(1)) {
	        		   image.setImageSelected(true);
	        	   }
	        	   else
	        		   image.setImageSelected(false);
	        	   webUserImages.put(image.getIdWebUserImage(),image);
	               }
	            rs.close();
			}
			cs.close();
			log.info("read "+webUserImages.size()+" images");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		return webUserImages;
	}
	
	public Integer insertReportPerson(Integer reporter, Integer reportee, String reason) {
		Connection conn = null;
		Integer idReportPerson=0;
		try {
			conn = dbConnection.getConnection();
			
			String simpleProc= "{ call reportPerson_Insert (?,?,? ,?) }";
			CallableStatement cs = conn.prepareCall(simpleProc);
			cs.setInt("in_reporter", reporter);
			cs.setInt("in_reportee", reportee);
			cs.setString("in_reason", reason);
			cs.registerOutParameter("out_idReportPerson", java.sql.Types.INTEGER);
			cs.execute();
			idReportPerson = cs.getInt("out_idReportPerson");
			cs.close();
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return idReportPerson;
	}
}
