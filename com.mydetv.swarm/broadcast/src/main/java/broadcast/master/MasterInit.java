package broadcast.master;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import shared.data.OneMydetvChannel;

public class MasterInit {

	private static final Logger log = LoggerFactory.getLogger(MasterInit.class);
	@Inject Provider<MasterServer> masterServerProvider;
	
	public void activateMaster(OneMydetvChannel mydetvChannel) {
		log.info("activateMaster");
		if(! mydetvChannel.getChannelType().equals(2)) {
			log.error("channel is not a studio channel "+mydetvChannel.getName());
			return;
		}
		if(mydetvChannel.getMasterServer()!=null) {
			log.error("channel already has a masterServer "+mydetvChannel.getName());
			return;
		}
		log.info("activating master for "+mydetvChannel.getName());
		MasterServer masterServer = masterServerProvider.get();
		masterServer.setMydetvChannel(mydetvChannel);
		mydetvChannel.setMasterServer(masterServer);
		Thread masterThread = new Thread(masterServer);
		masterThread.setName("studio-master-"+mydetvChannel.getChannelId());
		masterThread.start();
		mydetvChannel.setMasterServerThread(masterThread);
	}
	
	public void inactivateMaster(OneMydetvChannel mydetvChannel) {
		log.info("inactivateMaster");
		MasterServer masterServer=(MasterServer) mydetvChannel.getMasterServer();
		masterServer.shutdown();
		mydetvChannel.setMasterServer(null);
		mydetvChannel.setMasterServerThread(null);
	}
}
