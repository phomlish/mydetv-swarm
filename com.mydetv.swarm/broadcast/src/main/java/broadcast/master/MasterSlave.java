package broadcast.master;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;

/**
 * DEPRECATE this, slaves will run as a daemon
 * @author phomlish
 *
 */
public class MasterSlave extends Thread {

	private static final Logger log = LoggerFactory.getLogger(MasterSlave.class);
	@Inject private Blackboard blackboard;
	private OneSlave slave;
	private boolean interuptMe = false;
	private Process process;
	private Boolean done;

	@Override
	public void run() {		
		log.info("starting slave "+slave.getName());
		loop();
	}

	private void loop() {
		done=false;
		while(!done && ! blackboard.getShuttingDown()) {
			long startTime = System.nanoTime();
			ProcessBuilder pb = new ProcessBuilder(
					"ssh", "-t", "swarm@"+slave.getName(),
					"-p", "2222",
					"/home/phomlish/bin/slave_"+slave.getName()
					);
			startProcess(pb);
			long elapsedTime = System.nanoTime() - startTime;
			// 1,000,000,000 nanoseconds = 1 second
			if(elapsedTime < 5000000000L) {
				log.error("That was too quick "+elapsedTime/1000000000+".  SLEEPING "+slave.getName()+" 5 seconds");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					log.error("slave sleep while sarting interupted "+slave.getName());
				}
			}
		}
	}
	
	private void startProcess(ProcessBuilder pb) {
		log.info("starting "+slave.getName());
		StringBuilder sb = new StringBuilder();
		interuptMe=false;
		try {
			pb.environment().put("PATH", "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin");
			pb.environment().put("LD_LIBRARY_PATH", "/usr/local/lib");
			//for(Entry<String, String> entry : pb.environment().entrySet()) {
			//	log.info(entry.getKey()+":\t"+entry.getValue());
			//}
			pb.redirectErrorStream(true);
			process=pb.start();
			final Thread reader = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String line="";
						try {
							while((line=readerOutput.readLine()) != null) {
								sb.append(line+"\n");
								log.trace(slave.getName()+" line:"+line);
							}
						} catch (IOException e) {
							log.info("exception reading "+e); //+"\n"+sb.toString());

						}
					} catch (Exception e) {
						log.info("exception running "+e); //+"\n"+sb.toString());
					}
				}
			});
			
			reader.start();
			log.trace("reader started");
			boolean finished = false;
			while(!finished) {
				if(!process.isAlive())
					finished=true;
				else if(interuptMe)
					process.destroy();
				//log.info("not done");
				process.waitFor(5, TimeUnit.SECONDS);
			}
			process.destroyForcibly();
			log.info("done with slave "+slave.getName());
		} catch (Exception e1) {
			log.error("e:"+e1+"\n"+sb.toString());
		} 
	}
	
	public void stopSlave() {
		
		log.info("stopping slave "+slave.getName());
		if(process==null) {
			log.info("no process, can't stop");
			return;
		}
		if(!process.isAlive()) {
			log.info("process is not alive, can't stop");
			return;
		}
		
		try {
			process.destroyForcibly().waitFor();
		} catch (InterruptedException e) {
			log.error("interupted destroy "+slave.getName());
		}
			
	}
	
	public OneSlave getOneSlave() {
		return slave;
	}
	public void setOneSlave(OneSlave oneSlave) {
		this.slave = oneSlave;
	}
	public void setInteruptMe(Boolean interuptMe) {
		this.interuptMe=interuptMe;
	}
	public void setDone(Boolean done) {
		this.done=done;
	}
	
}
