package broadcast.master;

/**
 * control:<br>
 * <ul>
 *     <li>0: off</li>
 *     <li>1: on</li>
 *     <li>2: flash</li>
 *     <li>3: double (flash)</li>
 * </ul>
 * color: (as ordered by usb pins on the hardware device)<br>
 * <ul>
 *     <li>0: white viewers</li>
 *     <ul>
 *         <li># of viewers</li>
 *     </ul>
 *     <li>1: yellow motion</li>
 *     <ul>
 *         <li>off: it's quiet on the front step of the house</li>
 *         <li>on: recent motion</li>
 *         <li>flash: very recent motion</li>
 *         <li>double: current motion</li>
 *     </ul>
 *     <li>2: red broadcast status</li>
 *     <ul>
 *         <li>off: not live, not recording</li>
 *         <li>on: recording</li>
 *         <li>flash: live and recording</li>
 *         <li>double: live and NOT recording</li>
 *     </ul>
 *     <li>3: blue file/disk status of recording machine</li>
 *     <ul>
 *         <li>off: everything is ok</li>
 *         <li>flash: recorded file is large and editing will be difficult</li>
 *         <li>double: disk space is critical</li>
 *     </ul>
 *     <li>4: green status of slaves</li>
 *     <ul>
 *         <li>off: only happens when a7 slave can't talk to the master</li>
 *         <li>on: all important slaves are connected</li>
 *         <li>flash: only one slave is connected</li>
 *         <li>double: no slaves are connected</li>
 *     </ul>
 * </ul>
 * state: not needed here, used by the slave to control the blinking<br>
 * @author phomlish
 *
 */
public class OneUsbPin {

	public OneUsbPin(Integer bit, String name, String control, Boolean outdir, Boolean blight) {
		this.bit=bit;
		this.name=name;
		this.setControl(control);
		this.outdir=outdir;
		this.broadcastPageLight=blight;
		this.lastState=-1;
	}
	
	private Integer bit;
	private String name;
	private String control;
	private Boolean outdir; // 
	private Boolean broadcastPageLight;
	private Integer lastState;
	
	// getters/setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public Boolean getOutDir() {
		return outdir;
	}
	public void setOutDir(Boolean outdir) {
		this.outdir = outdir;
	}
	public Boolean getBroadcastPageLight() {
		return broadcastPageLight;
	}
	public void setBroadcastPageLight(Boolean broadcastPageLight) {
		this.broadcastPageLight = broadcastPageLight;
	}
	public Integer getBit() {
		return bit;
	}
	public void setBit(Integer bit) {
		this.bit = bit;
	}
	public Integer getLastState() {
		return lastState;
	}
	public void setLastState(Integer lastState) {
		this.lastState = lastState;
	}
}
