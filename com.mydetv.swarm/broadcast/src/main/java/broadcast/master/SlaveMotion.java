package broadcast.master;

import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;

public class SlaveMotion {

	@Inject private SlaveLights slaveLights;
	@Inject private BroadcasterMessaging broadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(SlaveMotion.class);
			
	//{"verb":"motion","motionStates":[{"camera":0,"state":0},{"camera":2,"state":0},{"camera":1,"state":0},{"camera":3,"state":0}]}

	public void updateMotion(JSONObject message) {
		log.info("handling motion "+message.toString());
		JSONArray cameras;
		try {
			cameras = message.getJSONArray("motionStates");
		} catch(Exception ex) {
			log.info("error getting cameras ",ex);
			return;	
		}

		// for now we only care about the driveway camera 3
		JSONObject driveway;
		try {
			driveway = getCamera(cameras, "driveway");
		} catch (JSONException ex) {
			log.error("Error getting camera", ex);
			return;
		}
		if(driveway==null) {
			//log.info("null camera");
			return;
		}
		// trust that this is a new state for this camera
		String control="";
		switch(driveway.getInt("state")) {
		case 0:
			control="off";
			break;
		case 1:
			control="on";
			break;
		case 2:
			control="flash";
			break;
		case 3:
			control="double";
			break;
		default:
			log.error("didn't recognize "+driveway);
			return;
		}
		slaveLights.updateLight("yellow", control);
	}
	
	private JSONObject getCamera(JSONArray cameras, String name) {
		log.info("cameras len:"+cameras.length());
		for(int i=0; i<cameras.length(); i++) {
			JSONObject jo = cameras.getJSONObject(i);
			if(jo.getString("name").equals("name"))
				return jo;
		}
		return null;
	}
}
