package broadcast.master;

import javax.inject.Inject;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.CharsetUtil;

public class MasterActions {
	
	@Inject private MasterServer masterServer;
	@Inject private SlaveLights slaveLights;
	@Inject private SlaveRecording slaveRecording;
	@Inject private SlaveDiskSpace slaveDiskSpace;
	@Inject private SlaveSounds slaveSounds;
	@Inject private SlaveSwitches slaveSwitches;
	//@Inject private BroadcasterMessaging broadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(MasterActions.class);
	
	public void connectSlave(SocketChannel ch) {
		String remoteAddress = ch.remoteAddress().getAddress().getHostAddress();
		Integer remotePort = ch.remoteAddress().getPort();
		
		OneSlave slave = null;
		if(remoteAddress.contains("10.11.1.92"))
			slave=masterServer.getSlaves().get("a2");
		else if(remoteAddress.contains("10.11.1.97"))
			slave=masterServer.getSlaves().get("a7");
		else if(remoteAddress.contains("10.11.1.101"))
			slave=masterServer.getSlaves().get("a7");
		else {
			log.error("Who the heck is " + remoteAddress);
			return;
		}
		log.info("connection from "+slave.getName());
		synchronized(slave.getSlaveLock()) {
			slave.setLastComm(System.currentTimeMillis());
			
			if(slave.getConnected()) {
				String oldRemoteAddress = slave.getCh().remoteAddress().getAddress().getHostAddress();
				Integer oldRemoePort = slave.getCh().remoteAddress().getPort();
				log.error("slave "+slave.getName()+" reconnected from "
					+remoteAddress+":"+remotePort+" (old was "
					+oldRemoteAddress+":"+oldRemoePort+")");
			}
			slave.setCh(ch);		
			slave.setConnected(true);
			slave.setRemoteAddress(remoteAddress);
			
			log.info("connected slave "+slave.getName());
			sendPing(slave);
			slaveLights.updateGreen();
			initSlave(slave);
		}
	}
	
	public void removeSlave(ChannelHandlerContext ctx) {
		SocketChannel socketChannel = (SocketChannel) ctx.channel();
		OneSlave slave = masterServer.findSlaveFromAddress(socketChannel);
		synchronized(slave.getSlaveLock()) {
			if(! slave.getConnected()) {
				log.error(slave.getName()+ " was not connected, can't disconnect +"+socketChannel.remoteAddress().getAddress().getHostAddress());
				return;
			}
			
			if(slave.getCh()==null) {
				log.error(slave.getName()+" channels is null +"+socketChannel.remoteAddress().getAddress().getHostAddress());
				return;
			}
			
			if(! slave.getCh().equals(socketChannel)) {
				log.error("socket channels don't match +"+socketChannel.remoteAddress().getAddress().getHostAddress());
				return;
			}
			
			// what happened to MasterSlave thread?
			slave.setConnected(false);
			slave.setSpaceWarning("ok");
			slave.setCh(null);
			if(slave.getName().equals("a2")) 
				slaveRecording.updateRecordingStopped(slave);
			slaveDiskSpace.updateSpaceWarning();
			slaveLights.updateGreen();
		}
	}
	
	public void sendPing(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "ping");
		writeMessage(slave, msg.toString());
	}
	public void sendDisconnect(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "disconnect");
		writeMessage(slave, msg.toString());
	}
	public void sendQuit(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "quit");
		writeMessage(slave, msg.toString());
	}

	
	public void updateMotion(JSONObject message) {		
		log.error("ERROR NOT IMPL");
	}
	/**
	 * send a message to OneSlave
	 * @param slave
	 * @param message
	 */
	public void writeMessage(OneSlave slave, String message) {
		if(slave.getCh()==null || !slave.getCh().isActive()) {
			log.error("slave not active "+slave.getName());
			return;
		}
		//log.info("test :"+slave.getCh().isActive()+","+slave.getCh().isWritable());
		slave.getCh().write(Unpooled.copiedBuffer(message+"\n", CharsetUtil.UTF_8));
		slave.getCh().flush();
	}
	/**
	 * send a message to slave identified by slaveName
	 * @param slaveName
	 * @param message
	 */
	public void writeMessage(String slaveName, String message) {
		
		OneSlave slave = masterServer.getSlaves().get(slaveName);
		if(slave==null) {
			log.error("could not find slave "+slaveName);
			return;
		}

		if(slave.getCh()==null || !slave.getCh().isActive()) {
			log.error("slave not active "+slaveName);
			return;
		}
		log.debug("test :"+slave.getCh().isActive()+","+slave.getCh().isWritable());
		slave.getCh().write(Unpooled.copiedBuffer(message+"\n", CharsetUtil.UTF_8));
		slave.getCh().flush();
	}
	
	/**
	 * each slave requires different initialization commands<br>
	 * currently each slave initializes itself after connecting
	 * 
	 * @param slave
	 */
	public void initSlave(OneSlave slave) {
		if(!slave.getConnected()) {
			log.error("that was quick, dc already! "+slave.getName());
			return;
		}
		
		switch(slave.getName()) {
		case "a2":
			break;
		case "a7":
			break;
		default:
			log.error("didn't recgnize slave "+slave.getName());
		}
	}
	
}
