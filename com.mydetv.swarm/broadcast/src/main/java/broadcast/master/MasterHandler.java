package broadcast.master;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.ChannelHandler.Sharable;

@Sharable
public class MasterHandler extends ChannelInboundHandlerAdapter {

	private static final Logger log = LoggerFactory.getLogger(MasterHandler.class);
	@Inject private MasterReader masterReader;
	@Inject private MasterActions masterActions;
	
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        log.info("channel registered:"+ctx.channel().remoteAddress());
    }
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
    	log.info("channel unregistered:"+ctx.channel().remoteAddress());
    	masterActions.removeSlave(ctx);
    }
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
    	log.info("channel active:"+ctx.channel().remoteAddress());
    	masterActions.connectSlave((SocketChannel) ctx.channel());
    }
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    	log.info("channel inactive:"+ctx.channel().remoteAddress());
    }
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    	//log.info("master server got message " + msg.toString());
    	masterReader.handleMessage(ctx, msg);
    }

}
