package broadcast.master;


import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import shared.data.OneMydetvChannel;

/**
 * Will run every 29 seconds
 * @author phomlish
 *
 */
public class WatchStudioCountTask implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(WatchStudioCountTask.class);
	@Inject private Blackboard blackboard;
	@Inject private MasterServer masterServer;
	@Inject private SlaveLights slaveLights;
	final Long minimumSpace=1000000000L;
	
	@Override
	public void run() {
		try {
			gatherWhiteStudioCount();
		} catch(Exception ex) {
			log.error("gatherWhiteStudioCount task failed",ex);
		}
	}
	
	private void gatherWhiteStudioCount() {
		
		OneMydetvChannel studio = blackboard.getMydetvChannels().get(2);
		int cnt = countInChannel(studio);
		//log.info("users in studio:"+cnt);
		String scnt=String.valueOf(cnt);
		OneUsbPin white = masterServer.getUsbPinsByColor().get("white");
		String soldcnt = white.getControl();
		if(! soldcnt.equals(scnt)) {
			white.setControl(scnt);
			slaveLights.sendLightsToA7();
		}
	}

	private int countInChannel(OneMydetvChannel studio) {
		if(! studio.isActive())
			return 0;	
		return studio.getSessionUsers().size();
	}
	
}
