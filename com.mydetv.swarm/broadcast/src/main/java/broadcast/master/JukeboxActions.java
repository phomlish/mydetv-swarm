package broadcast.master;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;
import shared.data.Config;
import shared.data.OneMydetvChannel;

public class JukeboxActions {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private BroadcasterMessaging p2pBroadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(JukeboxActions.class);
	
	public String getProfile() {
		String profile = "";
		HttpClient httpClient = new HttpClient(blackboard.getSslContextFactory());
		httpClient.setFollowRedirects(false);
		try {
			httpClient.start();
			ContentResponse response = httpClient.GET(config.getJukeboxUrl() + "/api/command"+"/getProfile");
			profile=response.getContentAsString();
			log.info("response:"+profile);
		} catch (Exception e) {
			log.error("no joy updating broadcast "+e);
		} finally {
			try {
				httpClient.stop();
				httpClient.destroy();
			} catch (Exception e) {
				log.error("Error shutting down our client :"+e);
			}
		}
		return(profile);
		
	}
	
	public String silence(OneMydetvChannel mydetvChannel) {
		String profile = "";
		HttpClient httpClient = new HttpClient(blackboard.getSslContextFactory());
		httpClient.setFollowRedirects(false);
		try {
			httpClient.start();
			ContentResponse response = httpClient.GET(config.getJukeboxUrl() + "/api/command"+"/stop/silence");
			log.info("response:"+response.getContentAsString());
			profile=response.getContentAsString();
		} catch (Exception e) {
			log.error("no joy updating broadcast "+e);
		} finally {
			try {
				httpClient.stop();
				httpClient.destroy();
			} catch (Exception e) {
				log.error("Error shutting down our client :"+e);
			}
		}
		p2pBroadcastMessaging.broadcastAllBroadcasters(mydetvChannel, "silence");
		return profile;
	}
	
	public String resume(OneMydetvChannel mydetvChannel) {
		String profile = "";
		HttpClient httpClient = new HttpClient(blackboard.getSslContextFactory());
		httpClient.setFollowRedirects(false);
		try {
			httpClient.start();
			ContentResponse response = httpClient.GET(config.getJukeboxUrl() + "/api/command"+"/stopSilence");
			log.info("response:"+response.getContentAsString());	
			profile=response.getContentAsString();
		} catch (Exception e) {
			log.error("no joy updating broadcast "+e);
		} finally {
			try {
				httpClient.stop();
				httpClient.destroy();
			} catch (Exception e) {
				log.error("Error shutting down our client :"+e);
			}
		}		
		p2pBroadcastMessaging.broadcastAllBroadcasters(mydetvChannel, "play");
		return profile;
	}
}
