package broadcast.master;

import java.time.LocalDateTime;
import java.util.HashMap;

import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;
import shared.data.OneMydetvChannel;
import shared.data.OneSound;
import shared.db.DbSounds;
import shared.helpers.FilenameHelpers;

public class SlaveSounds {

	@Inject private Blackboard blackboard;
	@Inject private DbSounds dbSounds;
	@Inject private MasterServer ms;
	@Inject private MasterActions masterActions;
	@Inject private BroadcasterMessaging broadcastMessaging;
	
	private static final Logger log = LoggerFactory.getLogger(SlaveSounds.class);

	public void handleSounds(JSONObject message) {
		log.info("handling sounds "+message.toString());
		
		// create inSounds from the incoming json
		HashMap<String, OneSound> inSounds = new HashMap<String, OneSound>();
		JSONArray jSounds = message.getJSONArray("sounds");
		for (int i=0; i < jSounds.length(); i++) {
			JSONObject sound = jSounds.getJSONObject(i);
			String fn=sound.getString("fn");
			OneSound os = new OneSound(
				sound.getString("dn"),
				fn,
				FilenameHelpers.GetSuffix(fn),
				sound.getInt("size")
				);
			inSounds.put(os.getDn()+"/"+os.getFn(), os);
		}
		log.info("inSounds size: "+inSounds.size());
		
		// grab the sounds we have now in the database
		HashMap<Integer, OneSound> oldDbSounds = new HashMap<Integer, OneSound>(dbSounds.getSounds());
		log.info("oldDbSounds size: "+oldDbSounds.size());
		
		// mark all old DB sounds as unverified so we know what got gone
		for(OneSound os : oldDbSounds.values())
			os.setVerified(false);
		
		// add inSounds to newSounds 
		LocalDateTime now = LocalDateTime.now();
		for(OneSound inSound : inSounds.values()) {
			OneSound oldSound = getSound(oldDbSounds, inSound);
			if(oldSound==null) {
				inSound.setDtAdded(now);	
				inSound.setVerified(true);
				// add to db
				inSound = dbSounds.addSound(inSound);
				// put in map
				oldDbSounds.put(inSound.getId(), inSound);
				log.info("Added "+inSound.getId()+" "+inSound.getDn()+"/"+inSound.getFn());			
			}
			else {
				oldSound.setVerified(true);
			}
		}
		
		//TODO: delete the missing sounds from the database and hashmap
		for(OneSound os : oldDbSounds.values()) {
			if(! os.getVerified()) {
				log.info("missing sound "+os.getId());
			}
		}
		
		// finally save in blackboard
		synchronized(blackboard.getSoundLock()) {
			blackboard.setSounds(oldDbSounds);
		}
		log.info("oldSounds new size: "+oldDbSounds.size());
		
	}

	private OneSound getSound(HashMap<Integer, OneSound> sounds, OneSound os) {
		for(OneSound ts : sounds.values())
			if(ts.getDn().equals(os.getDn()) && ts.getFn().equals(os.getFn()))
				return ts;
		return null;
	}

	public void gatherSounds(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "gatherSounds");
		masterActions.writeMessage(slave, msg.toString());
	}
	
	public boolean playRemoteSound(String fn) {
		// only do this if the studio is active, otherwise broadcasters can ruin our jukebox experience
		OneMydetvChannel ch = blackboard.getMydetvChannels().get(2);
		if(ch==null)
			return false;
		if(!ch.isActive())
			return false;
		
		OneSlave os = ms.getSlave("a7");
		if(os == null)
			return false;
		
		JSONObject msg = new JSONObject();
		msg.put("verb", "playSound");
		msg.put("fn", fn);
		masterActions.writeMessage(os, msg.toString());
		return true;
	}
	
	public void playStartStop(JSONObject message) {
		OneMydetvChannel studio = blackboard.getMydetvChannels().get(2);
		if(!studio.isActive())
			return;
		// is studio active
		String action = message.getString("action");
		String pn = message.getString("pn");
		log.info("sound "+pn+" "+action);
		broadcastMessaging.broadcastAllBroadcasters(studio, action, pn);
	}
}
