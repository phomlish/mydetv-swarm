package broadcast.master;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.SocketChannel;

public class MasterReader {
	
	private static final Logger log = LoggerFactory.getLogger(MasterReader.class);
	@Inject private MasterServer masterServer;
	@Inject private SlaveDiskSpace slaveDiskSpace;
	@Inject private SlaveMotion slaveMotion;
	@Inject private SlaveSounds slaveSounds;
	@Inject private SlaveSwitches slaveSwitches;
	
	public void handleMessage(ChannelHandlerContext ctx, Object msg) {
		
		SocketChannel socketChannel = (SocketChannel) ctx.channel();
		OneSlave slave = masterServer.findSlaveFromAddress(socketChannel);

		if(slave==null) {
			log.error("could not figure out the host "+socketChannel.remoteAddress().getAddress().getHostAddress());
			return;
		}
		synchronized(slave.getSlaveLock()) {
			slave.setLastComm(System.currentTimeMillis());
			
			//log.info(slave.getName()+" got "+msg.toString());
			try {
				JSONObject message = new JSONObject(msg.toString());
	
				String verb = message.getString("verb");
				if(! verb.equals("pong"))
					log.info(slave.getName()+" got " + message.toString());
				switch(verb) {
				case "pong": 
					break;
				case "motion":
					slaveMotion.updateMotion(message);
					break;
				case "sounds":
					slaveSounds.handleSounds(message);
					break;
				case "soundStartStop":
					slaveSounds.playStartStop(message);
					break;
				case "spaceWarning":
					slaveDiskSpace.updateOneSlaveSpaceWarning(slave, message.getString("warning"));
					break;
				case "switches":
					slaveSwitches.processSwitches(message.getJSONArray("switches"));
					break;
				default:
					log.error("didn't recognize message "+msg);
				}
			} catch(Exception e) {
				log.error("Error processing message: "+msg.toString()+" "+e.getMessage());
			}
		}
	}
}
