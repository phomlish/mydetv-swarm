package broadcast.master;

import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import shared.data.Config;
import shared.data.OneMydetvChannel;

/**
 * Will run every 29 seconds
 * @author phomlish
 *
 */
public class WatchDiskSpaceTask implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(WatchDiskSpaceTask.class);
	@Inject private Config config;
	@Inject private MasterServer masterServer;
	@Inject private SlaveDiskSpace slaveDiskSpace;
	final Long minimumSpace=1000000000L;
	
	@Override
	public void run() {
		try {
			diskSpaceOnMaster();
		} catch(Exception ex) {
			log.error("task failed",ex);
		}
	}
	
	private void diskSpaceOnMaster() {
		try {
			Path path;
			if(config.getInstance().equals("dev"))
				path = Paths.get("/private/nfs/a6/usr5");
			else
				path = Paths.get("/usr5");
			FileStore store;

			store = Files.getFileStore(path);
			//log.info("available=" + store.getUsableSpace()+ ", total=" + store.getTotalSpace()+",min:"+minimumSpace);
			if(store.getUsableSpace()<minimumSpace) {
				
				if(! masterServer.getDiskSpaceState().equals("spaceCritical")) {
					log.info("critical space");
					masterServer.setDiskSpaceState("spaceCritical");
					slaveDiskSpace.updateSpaceWarning();
				}
			}
			else {				
				if(! masterServer.getDiskSpaceState().equals("ok")) {
					log.info("space ok");
					masterServer.setDiskSpaceState("ok");
					slaveDiskSpace.updateSpaceWarning();
				}
			}
		} catch (Exception e) {
			log.error("exception:"+e);
		} 
	}
	
}
