package broadcast.master;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;

public class SlaveDiskSpace {

	@Inject private Blackboard blackboard;
	@Inject private MasterServer masterServer;
	@Inject private SlaveLights slaveLights;
	@Inject private SlaveRecording slaveRecording;
	@Inject private BroadcasterMessaging broadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(SlaveDiskSpace.class);
	
	/**
	 * warnings:<br>
	 * ok<br>
	 * spaceCritical<br>
	 * largeFile<br>
	 * @param slave
	 * @param warning
	 */
	public void updateOneSlaveSpaceWarning(OneSlave slave, String warning) {
		if(slave.getSpaceWarning().equals(warning)) {
			log.error("Odd, I already know that "+slave.getName()+" "+warning);
			return;
		}
		slave.setSpaceWarning(warning);
		updateSpaceWarning();
	}
	
	public void updateSpaceWarning() {
		
		// find the worst warning and set the blinky light to match
		String worst="ok";
		for(OneSlave os : masterServer.getSlaves().values()) {
			if(! os.getConnected())
				continue;
			if(os.getSpaceWarning().equals("spaceCritical")) {
				worst="spaceCritical";
				break;
			}
			else if(os.getSpaceWarning().equals("largeFile")) {
				worst="largeFile";
			}			
		}
		if(masterServer.getDiskSpaceState().equals("spaceCritical"))
			worst="spaceCritical";
		
		switch(worst) {
		case "spaceCritical":
			slaveLights.updateLight("blue", "double");
			break;
		case "largeFile":
			slaveLights.updateLight("blue", "flash");
			break;
		case "ok":
			slaveLights.updateLight("blue", "off");
			break;
		default:
			log.error("didn't recognize worst spaceWarning "+worst);
		}
	}
}
