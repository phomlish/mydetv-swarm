package broadcast.master;

import io.netty.channel.socket.SocketChannel;

/**
 * spaceWarning:<br>
 * ok<br>
 * spaceCritical<br>
 * largeFile<br>
 * @author phomlish
 *
 */
public class OneSlave {
	
	private Long lastComm = 0L;
	private SocketChannel ch = null;
	private String name;
	private Boolean connected;
	private Thread masterSlaveThread;
	private String remoteAddress;
	private String spaceWarning;
	private MasterSlave masterSlave;
	private Object slaveLock = new Object();
	
	// creator
	public OneSlave(String name) {
		this.name=name;
		this.connected=false;
		this.spaceWarning="ok";
	}
	// getters/setters
	public SocketChannel getCh() {
		return ch;
	}
	public void setCh(SocketChannel ch) {
		this.ch = ch;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getLastComm() {
		return lastComm;
	}
	public void setLastComm(Long lastComm) {
		this.lastComm = lastComm;
	}
	public Boolean getConnected() {
		return connected;
	}
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}
	public Thread getMasterSlaveThread() {
		return masterSlaveThread;
	}
	public void setMasterSlaveThread(Thread masterSlaveThread) {
		this.masterSlaveThread = masterSlaveThread;
	}
	public String getRemoteAddress() {
		return remoteAddress;
	}
	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
	public String getSpaceWarning() {
		return spaceWarning;
	}
	public void setSpaceWarning(String spaceWarning) {
		this.spaceWarning = spaceWarning;
	}
	public MasterSlave getMasterSlave() {
		return masterSlave;
	}
	public void setMasterSlave(MasterSlave masterSlave) {
		this.masterSlave = masterSlave;
	}
	public Object getSlaveLock() {
		return slaveLock;
	}
	public void setSlaveLock(Object slaveLock) {
		this.slaveLock = slaveLock;
	}
}
