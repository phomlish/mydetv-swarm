package broadcast.master;

import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;

public class SlaveSwitches {

	@Inject private Blackboard blackboard;
	@Inject private MasterServer masterServer;
	@Inject private MasterActions masterActions;
	private static final Logger log = LoggerFactory.getLogger(SlaveSwitches.class);
	
	public void processSwitches(JSONArray switches) {

		log.info("ja "+switches);
		for (int i=0; i < switches.length(); i++) {
			JSONObject jo = switches.getJSONObject(i);
			Integer newState = jo.getInt("State");
			Integer bit = jo.getInt("Bit");
			OneUsbPin usb = masterServer.getUsbPinByBit(bit);
			if(! usb.getLastState().equals(newState)) {				
				switchChanged(usb, newState);			
			}
		}	
	}
	
	public void gatherSwitches(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "gatherSwitches");
		masterActions.writeMessage(slave, msg.toString());
	}
	public void gatherAdcs(OneSlave slave) {
		JSONObject msg = new JSONObject();
		msg.put("verb", "gatherAdcs");
		masterActions.writeMessage(slave, msg.toString());
	}

	private void switchChanged(OneUsbPin usb, Integer newState) {
		//log.info(usb.getBit()+" "+usb.getName()+" changed from "+usb.getLastState()+" to "+newState);
		switch(usb.getBit()) {
		case 11: // broadcast
			log.info("broadcast switch changed to "+newState);
			break;
		case 12: // applause
			log.info("applause button changed to "+newState);
			break;
		case 13: // laughter
			log.info("laughter button changed to "+newState);
			break;
		case 14: // jukebox
			log.info("jukebox button changed to "+newState);
			break;
		default:
			//log.info("ignoring switch "+usb.getName());
		}
		usb.setLastState(newState);
	}
	
	private class oneSwitch {
		private Integer bit;
		private Integer state;
		private String name;
		public Integer getBit() {
			return bit;
		}
		public void setBit(Integer bit) {
			this.bit = bit;
		}
		public Integer getState() {
			return state;
		}
		public void setState(Integer state) {
			this.state = state;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}

}
