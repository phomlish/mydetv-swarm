package broadcast.master;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.inject.Inject;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;
import broadcast.janus.JClient;
import broadcast.web.ws.WsHelpers;
import shared.data.OneMydetvChannel;

public class SlaveRecording {

	@Inject private Blackboard blackboard;
	@Inject private MasterServer masterServer;
	@Inject private MasterActions masterActions;
	@Inject private SlaveLights slaveLights;
	@Inject private BroadcasterMessaging broadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(SlaveRecording.class);
			
	/**
	 * updateRecordingStopped: when a2 dies we lose the recording
	 * @param slave
	 * find channel
	 */
	public void updateRecordingStopped(OneSlave slave) {
		for(OneMydetvChannel channel : blackboard.getMydetvChannels().values()) {
			if(channel.isRecording()) {
				channel.setRecording(false);
				updateRecordingLight(channel);
				JSONObject message = WsHelpers.UpdateELementProperty("recording", "checked", "");
				broadcastMessaging.messageAllBroadcasters(channel, message);	
			}
		}		
	}
		
	/**
	 * Please sanity check before calling<br>
	 * @param mydetvChannel
	 */
	public void startRecording(OneMydetvChannel mydetvChannel) {
		ZonedDateTime dt = LocalDateTime.now().atZone(ZoneId.of("America/New_York"));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
		String pdt = dtf.format(dt);
		
		// tell the slave to start recording
		JSONObject msg = new JSONObject();
		msg.put("verb", "recordStart");
		msg.put("fn", pdt);
		masterActions.writeMessage("a2", msg.toString());
		updateRecordingLight(mydetvChannel);
	}

	public void stopRecording(OneMydetvChannel mydetvChannel) {
		log.info("stop recording");
		JSONObject msg = new JSONObject();
		msg.put("verb", "recordStop");
		masterActions.writeMessage("a2", msg.toString());
		updateRecordingLight(mydetvChannel);
	}

	public void updateRecordingLight(OneMydetvChannel mydetvChannel) {
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		if(jClient==null) {
			log.error("bad code1");
			return;
		}
		if(mydetvChannel.getChannelType()!=2) {
			log.error("bad code2");
			return;
		}
		if(jClient.isLive()) {
			if(mydetvChannel.isRecording()) slaveLights.updateLight("red", "flash");
			else							slaveLights.updateLight("red", "double");
		}
		else {
			if(mydetvChannel.isRecording()) slaveLights.updateLight("red", "on");
			else							slaveLights.updateLight("red", "off");
		}		
	}
	
				
}
