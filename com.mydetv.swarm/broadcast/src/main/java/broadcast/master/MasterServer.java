package broadcast.master;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;
import shared.data.Config;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import shared.data.OneMydetvChannel;

/**
 * HUGE issue: when developing and studio is active in prod, both instances fight to create slaves
 * @author phomlish
 *
 */
public class MasterServer implements Runnable {

	@Inject Provider<MasterSlave> masterSlaveProvider;
	
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private MasterHandlerInit masterServerInit;
	@Inject private MasterActions masterActions;
	@Inject private BroadcasterMessaging p2pBroadcastMessaging;
	@Inject private WatchPingTask pingTask;
	@Inject private WatchDiskSpaceTask diskSpaceTask;
	@Inject private WatchStudioCountTask studioCountTask;
	
	private static final Logger log = LoggerFactory.getLogger(MasterServer.class);

	EventLoopGroup bossGroup;
	EventLoopGroup workerGroup;

	// key is light name
	private HashMap<String,OneUsbPin> usbPinsByColor = new HashMap<String,OneUsbPin>();
	// key is slave name
	private HashMap<String, OneSlave> slaves = new HashMap<String, OneSlave>();
	//private String jukeboxProfile = "unknown";
	private OneMydetvChannel mydetvChannel;
	private ScheduledExecutorService scheduler;
	private String diskSpaceState;
	
	@Override
	public void run() {		
		log.info("setting up master server on port "+config.getMasterServerPort());
		diskSpaceState="ok";
		// setup outputs
		usbPinsByColor.put("yellow", new OneUsbPin(3, "yellow","off",true, true));
		usbPinsByColor.put("green", new OneUsbPin(6, "green","double",true, true));
		usbPinsByColor.put("red", new OneUsbPin(4, "red","off",true, true));
		usbPinsByColor.put("blue", new OneUsbPin(5, "blue","off",true, true));
		usbPinsByColor.put("white", new OneUsbPin(2, "white","0",true, false));
		usbPinsByColor.put("blue_bottom", new OneUsbPin(8, "blue_bottom","off",true, false)); // buzzer
		usbPinsByColor.put("yellow_bottom", new OneUsbPin(7,"yellow_bottom","off",true, false));
		// setup inputs
		usbPinsByColor.put("i_t1", new OneUsbPin(11, "i_t1","off",false, false)); // broadcast
		usbPinsByColor.put("i_p1", new OneUsbPin(14, "i_p1","off",false, false)); // applause
		usbPinsByColor.put("i_p2", new OneUsbPin(13, "i_p2","off",false, false)); // laughter
		usbPinsByColor.put("i_p3", new OneUsbPin(12, "i_p3","off",false, false)); // jukebox
		usbPinsByColor.put("i_p4", new OneUsbPin(10, "i_p4","off",false, false)); // 
		usbPinsByColor.put("i_t2", new OneUsbPin(9, "i_t2","off",false, false)); // motion
		usbPinsByColor.put("i_t3", new OneUsbPin(15, "i_t3","off",false, false)); // sump pump
		
		// setup our slaves
		slaves.put("a2", new OneSlave("a2"));
		slaves.put("a7", new OneSlave("a7"));
		slaves.put("test", new OneSlave("test"));
		
		bossGroup = new NioEventLoopGroup();
		workerGroup = new NioEventLoopGroup();
		
		ServerBootstrap b = new ServerBootstrap();
		b.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.option(ChannelOption.SO_BACKLOG, 100)
			.childOption(ChannelOption.SO_KEEPALIVE, true)
			.childOption(ChannelOption.TCP_NODELAY, true)
			.handler(new LoggingHandler(LogLevel.INFO))
			.childHandler(masterServerInit);

		try {
			b.bind(config.getMasterServerPort()).sync();
		} catch (InterruptedException e) {
			log.error("could not setup masterServerHttp");
		}
		
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(pingTask, 15, 15, TimeUnit.SECONDS);
		scheduler.scheduleAtFixedRate(diskSpaceTask, 1, 59, TimeUnit.SECONDS);
		scheduler.scheduleAtFixedRate(studioCountTask, 1, 14, TimeUnit.SECONDS);
		
		String lights = updateBrowserLights();
		p2pBroadcastMessaging.broadcastAllBroadcasters(mydetvChannel, "updateLights", lights);
	}

	public OneSlave findSlaveFromAddress(SocketChannel socketChannel) {
		for(OneSlave os : slaves.values()) {
			String socketAddress=socketChannel.remoteAddress().getAddress().getHostAddress();
			if(os.getRemoteAddress() != null && ! os.getRemoteAddress().isEmpty() && os.getRemoteAddress().equals(socketAddress))
				return os;
		}
		return null;
	}
	public static String GetRemoteSocketAddressDeprecated(SocketChannel socketChannel) {
		return socketChannel.remoteAddress().getAddress().getHostAddress();
	}
	
	public OneSlave getSlave(String sn) {
		for(OneSlave slave : slaves.values())
			if(slave.getName().equals(sn) && slave.getConnected())
				return slave;
		return null;
	}
	public void shutdown() {
		log.info("shutting down masterServer");
		p2pBroadcastMessaging.broadcastAllBroadcasters(mydetvChannel, "removeLights");
		for(OneSlave slave : slaves.values()) {
			if(slave.getCh()!=null && slave.getCh().isActive()) {
				log.info("stopping "+slave.getName());
				masterActions.sendQuit(slave);
				slave.getMasterSlave().setDone(true);
			}
		}
		log.info("shutting down masterServer scheduler");
		scheduler.shutdown();
		scheduler=null;
		
		try {
			log.info("shutting down masterServer bossGroup");
			bossGroup.shutdownGracefully().sync();
			log.info("shutting down masterServer workerGroup");
			workerGroup.shutdownGracefully().sync();
		} catch (InterruptedException e) {
			log.info("master shutdown interupted");
		}
	}
	
	public String updateBrowserLights() {
    	String rv = "";
    	for(OneUsbPin light : usbPinsByColor.values()) {
    		if(! light.getBroadcastPageLight())
    			continue;
    		rv+="<div class='light'>\n";
    		String css;
    		if(light.getControl().equals("off"))		css=" style='opacity:0.1;'";
    		else if(light.getControl().equals("on"))	css=" style='opacity:1.0;'";
    		else if(light.getControl().equals("flash"))	css=" style='opacity:1.0; animation: blink 1s; animation-iteration-count: infinite;'";
    		else 										css=" style='opacity:1.0; animation: blink .5s; animation-iteration-count: infinite;'";
    		
    		rv+="<img "
    			+" id='light-"+light.getName()+"'"
    			+css
    			+" src='/s/icons/"+light.getName()+".gif'>\n";
    		rv+="</div>\n";
    	}
    	return rv;
	}
	
	public HashMap<String,OneUsbPin> getUsbPinsByColor() {
		return usbPinsByColor;
	}
	public OneUsbPin getUsbPinByBit(Integer bit) {		
		for(OneUsbPin usb : usbPinsByColor.values()){
			if(usb.getBit().equals(bit))
				return usb;
		}
		log.error("could not find usbpin "+bit);
		return null;
	}
	public void setUsbPinsByColor(HashMap<String,OneUsbPin> lights) {
		this.usbPinsByColor = lights;
	}
	public HashMap<String, OneSlave> getSlaves() {
		return slaves;
	}
	public void setSlaves(HashMap<String, OneSlave> slaves) {
		this.slaves = slaves;
	}
	public OneMydetvChannel getMydetvChannel() {
		return mydetvChannel;
	}
	public void setMydetvChannel(OneMydetvChannel mydetvChannel) {
		this.mydetvChannel = mydetvChannel;
	}
	public String getDiskSpaceState() {
		return diskSpaceState;
	}
	public void setDiskSpaceState(String diskSpaceState) {
		this.diskSpaceState = diskSpaceState;
	}
}
