package broadcast.master;

import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterMessaging;

public class SlaveLights {

	@Inject private MasterServer masterServer;
	@Inject private MasterActions masterActions;
	@Inject private Blackboard blackboard;
	@Inject private BroadcasterMessaging broadcastMessaging;
	private static final Logger log = LoggerFactory.getLogger(SlaveLights.class);
	
	/**
	 * updates green and sends the update to a7 and web broadcasters<br>
	 * see OneUsbPin.java for green rules
	 */
	public void updateGreen() {		
		// mandatory connections from a2,a7
		Integer count=0;
		for(OneSlave slave : masterServer.getSlaves().values()) {
			if(slave.getName().equals("a2") && slave.getConnected())
				count++;
			else if(slave.getName().equals("a7") && slave.getConnected())
				count++;
		}
		log.info("updating Green "+count);
		if(count > 1)
			updateLight("green", "on");
		else if(count == 1)
			updateLight("green", "flash");
		else
			updateLight("green", "double");	
	}
	
	/**
	 * update one light if it needs to be updated<br>
	 * Don't send updates for yellow or green to slave a7
	 * @param color
	 * @param control
	 */
	public void updateLight(String color, String control) {
		if(!masterServer.getUsbPinsByColor().containsKey(color)) {
			log.error("can't find a light with that color "+color);
			return;
		}
		
		OneUsbPin light = masterServer.getUsbPinsByColor().get(color);
		if(light.getControl().equals(control))
			return;
		
		log.info("updating "+color+" from "+light.getControl()+" to "+control);
		light.setControl(control);
		
		JSONObject message = new JSONObject();
		message.put("broadcaster", "changeLight");
		JSONObject args = new JSONObject();
		args.put("light", light.getName());
		args.put("control", light.getControl());
		message.put("args", args);
		broadcastMessaging.broadcastAllBroadcasters(masterServer.getMydetvChannel(), message);
		sendLightsToA7();
	}
	
	/**
	 * send to the physical blinky lights controlled by a7<br>
	 * since the USB  updates all the lights, we send them all
	 */
	public void sendLightsToA7() {
		OneSlave slave = masterServer.getSlaves().get("a7");

		if(! slave.getConnected()) {
			if(! blackboard.getShuttingDown())
				log.error("can't update a7, not connected");
			return;
		}

		JSONObject msg = new JSONObject();
		msg.put("verb", "lights");
		JSONArray lights = new JSONArray();
		for(OneUsbPin light : masterServer.getUsbPinsByColor().values()) {
			if(! light.getOutDir())
				continue;
			JSONObject jsonLight = new JSONObject(light);
			lights.put(jsonLight);
		}
		msg.put("lights", lights);
		masterActions.writeMessage(slave, msg.toString());
	}
}
