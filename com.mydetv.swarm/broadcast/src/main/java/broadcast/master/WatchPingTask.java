package broadcast.master;


import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Will run every 29 seconds
 * @author phomlish
 *
 */
public class WatchPingTask implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(WatchPingTask.class);
	@Inject private MasterServer masterServer;
	@Inject private MasterActions masterActions;
	@Inject private SlaveLights slaveLights;
	//private String instance;
	final Long minimumSpace=1000000000L;
	
	@Override
	public void run() {
		try {
			pingTask();
		} catch(Exception ex) {
			log.error("tasks failed",ex);
		}
	}
	
	private void pingTask() {
		
		for(OneSlave slave : masterServer.getSlaves().values()) {
			
			if(slave.getCh() == null)
				continue;
			
			//log.info("ping "+slave.getName());
			
			Long now = System.currentTimeMillis();
			Long diff = now-slave.getLastComm();
			//log.info(slave.getName()+" comparing "+now+" with "+slave.getLastComm()+" "+diff);
			if(diff>61000) {
				log.error("no messages in "+diff+" seconds, shutting down " + slave.getName());
				// not nice, but he must be stuck
				slave.getCh().disconnect();
				slave.setCh(null);
				slaveLights.updateGreen();
				continue;
			}
			if(diff>25000) {
				log.debug(slave.getName()+" no messages in "+diff+" seconds, send ping ");
				masterActions.sendPing(slave);
			}
			
		}
	}

}
