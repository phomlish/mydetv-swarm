package broadcast.auth;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.Failed;
import shared.db.DbConnection;
import shared.db.DbWebUser;

public class Validations {
	
	@Inject private DbConnection dbConnection;
	@Inject private DbWebUser dbWebUser;
	@Inject private Blackboard blackboard;
	@Inject private WebHelper webHelper;
	@Inject private UserHelper userHelper;
	@Inject private Config config;
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(Validations.class);

	// don't try to keep these in memory.  People might try to hack by using multiple machines.
	/**
	 * 3 failed account password or security attempts and they are locked.<br>
	 * We'll check every time UNLESS the system is locked.<br>
	 *  * If we are bring hacked we don't want to crush the database checking.<br>
	 *  * We will check again once the system unlocks<br>
	 * @param sessionUser
	 * @return
	 * (String)<br>
	 * system locked<br>
	 * account locked<br>
	 * validation exception<br>
	 * ok<br>
	 */
	public String checkFailed(Connection conn, Integer webUserId) {
		
		if(isSystemLoginLockedCurrently())
			return "system locked";
		
		try {
			log.trace("checkFailed");
			ArrayList<Failed> failed = dbWebUser.getFailed(conn);
			log.debug("got a total of "+failed.size()+" recently failed attempts");
			if(failed != null) {

				// should we lock the system?
				if(failed.size()>=config.getLockSystemFailedAttempts()) {
					blackboard.setSystemLoginLocked(LocalDateTime.now());
					return "system locked";
				}
				
				// should we lock the account?
				ArrayList<Failed> recentFailures = new ArrayList<Failed>();				
				for(Failed recentFailure : failed)
					if(recentFailure.getIdWebUser().equals(webUserId))
						recentFailures.add(recentFailure);					
				
				if(recentFailures.size()>=3) {
					return "account locked";
				}
			}
			
		} catch (Exception e) {
			log.error("Exception:"+e);
			return "validation exception";
		}
		return "ok";
		
	}
	
	public String checkFailed(Integer webUserId) {
		Connection conn=null;
		String status;
		try{
			conn = dbConnection.getConnection();	
			status= checkFailed(conn, webUserId);
		} catch (SQLException e) {
			log.error("failed e:"+e);
			status="validation exception";
		}		
		dbConnection.tryClose(conn);
		return status;
	}

	public boolean isSystemLoginLockedCurrently() {
		// are we currently locked
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime twentyMinutesEarlier = now.minusMinutes(20);
		if(blackboard.getSystemLoginLocked() != null && blackboard.getSystemLoginLocked().isAfter(twentyMinutesEarlier)) {
			return true;
		}	
		blackboard.setSystemLoginLocked(null);
		return false;
	}
	
	public Response showLoginLockedPage(SessionUser sessionUser) {		
		String rv = webHelper.beginHtml(sessionUser, "Login Locked");
		
		rv+="<h1 class='center'>logins/registrations are currently locked</h1>";
		rv+="<h3 class='center'>try again later</h3><br>";
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rv+="</body>\n";
		rv+="</html>\n";		
		
		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}
	
	public Boolean isReAuthenticated(SessionUser sessionUser) {		
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime before = now.minusMinutes(5);
		if(sessionUser.getWebUser().getDtReAuthenticated()==null || sessionUser.getWebUser().getDtReAuthenticated().isBefore(before)) 
			return false;
		return true;
	}
	
}
