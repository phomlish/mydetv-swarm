package broadcast.auth;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.IllegalFormatConversionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Birthday {

	private static final Logger log = LoggerFactory.getLogger(Birthday.class);
	
	public static String BdayForm(LocalDate bday) {

		int year = 0;		
		int month = 0;
		int day = 0;		
		if(bday != null) {
			year=bday.getYear();
			month=bday.getMonth().getValue();
			day=bday.getDayOfMonth();
		}
		log.info(bday+":"+year+"-"+month+"-"+day);
		String rv="";
		// birthday
		rv+="<div class='form-group bday-none row'>";
		rv+="<label class='col-form-label col-sm-2' for='bday-none'></label>";
		rv+="<div class='col-sm-3 center'>Month</div>";
		rv+="<div class='col-sm-2 center'>Day</div>";
		rv+="<div class='col-sm-2 center'>Year</div>";
		rv+="</div>";

		rv+="<div class='form-group bday-label row'>";
		rv+="<label class='col-form-label col-sm-2' for='birthday'>Birthday</label>";

		rv+="<div class='col-sm-3 center'>";

		rv+="<select id='bmonth' name='bmonth' value='false' class='form-control' onchange='changedProfile()' />";
		if(month==0) rv+="<option value='' selected></option>";
		for(int i=1; i<=12; i++) {
			String selected="";
			if(i==month)
				selected+=" selected ";
			rv+="<option value='"+i+"' "+selected+">"+new DateFormatSymbols().getMonths()[i-1]+"</option>";
		}
		rv+="</select>\n";
		rv+="</div>";

		rv+="<div class='col-sm-2 center'>";
		rv+="<select id='bday' name='bday' value='false' class='form-control' onchange='changedProfile()'/>";
		if(day==0) rv+="<option value='' selected></option>";
		for(Integer i=1; i<=31; i++) {
			String selected="";
			if(i==day)
				selected+=" selected ";
			rv+="<option value='"+i+"'"+selected+">"+String.format("%02d", i)+"</option>";
		}
		rv+="</select>\n";
		rv+="</div>";

		rv+="<div class='col-sm-2 center'>";
		rv+="<div class='center'><select id='byear' name='byear' class='form-control' onchange='changedProfile()'/>";
		for(int i=1900; i<2018; i++) {
			String selected="";
			rv+="<option ";
			if(i==year)
				selected+=" selected ";
			rv+=" value='"+i+"'"+selected+">"+i+"</option>";
		}
		if(day==0) rv+="<option value='' selected></option>";
		rv+="</select></div>\n";
		rv+="</div>";
		
		rv+="<div class='col-sm-3 center text-danger' id='bMessage'></div>";
		
		rv+="</div>"; // form-group	
		return rv;
	}
	public static LocalDate GetDate(String year, String month, String day) {
		if(year==null||year.equals("")||month==null||month.equals("")||day==null||day.equals(""))
			return null;
		
		try {
			Integer iYear = Integer.decode(year);
			Integer iMonth = Integer.decode(month);
			Integer iDay = Integer.decode(day);
			String dateString = String.format("%04d-%02d-%02d", iYear, iMonth, iDay);
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate localdate = LocalDate.parse(dateString, dtf);

			return localdate;
		} catch (IllegalFormatConversionException | DateTimeParseException | NumberFormatException e) {
			log.error("e:"+e);
			return null;
		}
	}
	public static Boolean Is18(LocalDate birthday) {
		if(birthday==null)
			return false;
		LocalDate now = LocalDate.now();
		LocalDate minus18 = now.minusYears(18).plusDays(1);
		log.trace("now:"+now+",minus18:"+minus18);
		if(birthday.isBefore(minus18))
			return true;
		return false;
	}
}
