package broadcast.auth;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;

/**
 * 
 * @author phomlish
 *
 * Encrypt password:
 * 	Generate a salt specific to the user
 *  Encrypt the password with the salt = phasePassword
 *  Encrypt the phase1 password with the secretKey for the database stored in the config file
 *  
 * Decrypt
 *  No such thing.  Encrypt again and make sure it matches
 */
public class Encrypt {

	private static final Logger log = LoggerFactory.getLogger(Encrypt.class);

	private static String PDKDF_ALGORITHM = "PBKDF2WithHmacSHA512" ;
	private static int PASSWORD_ITERATION_COUNT = 9876;
	private static int ITERATION_COUNT = 2048;
	private static int SALT_LENGTH = 128;
	private static int DERIVED_KEY_LENGTH = 512;

	//PKCS5Padding AEAD PBKDF2

	public static byte[] GenerateSalt() {
		byte[] salt = new byte[SALT_LENGTH] ; // Should be at least 64 bits
		SecureRandom secRandom = new SecureRandom() ;
		secRandom.nextBytes(salt) ; // self-seeded randomizer for salt
		//log.info("salt:"+Base64.getEncoder().encodeToString(salt));
		log.info("salt length:"+salt.length);	
		return salt;
	}

	public static byte[] EncryptPassword(char[] charPassword, byte[] salt, String configSalt) {		
		byte[] pbkdfHashedArray1 = null; 
		byte[] pbkdfHashedArray2 = null; 
		try {
			PBEKeySpec keySpecP1 = new PBEKeySpec(charPassword, salt, PASSWORD_ITERATION_COUNT, DERIVED_KEY_LENGTH);
			
			SecretKeyFactory pbkdfKeyFactory = SecretKeyFactory.getInstance(PDKDF_ALGORITHM);
			
			pbkdfHashedArray1 = pbkdfKeyFactory.generateSecret(keySpecP1).getEncoded();
			PBEKeySpec keySpecP2 = new PBEKeySpec(
					Base64.getEncoder().encodeToString(pbkdfHashedArray1).toCharArray(), 
					Base64.getDecoder().decode(configSalt), 
					PASSWORD_ITERATION_COUNT, 
					DERIVED_KEY_LENGTH);
			pbkdfHashedArray2 = pbkdfKeyFactory.generateSecret(keySpecP2).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("Severe error, no password generated! e:"+e);
			return null;
		}

		//log.info("hash:"+Base64.getEncoder().encodeToString(pbkdfHashedArray2));
		log.trace("hash length:"+pbkdfHashedArray2.length);
		return(pbkdfHashedArray2);
	}
	
	public static byte[] EncryptSecret(char[] charSecret, byte[] salt, String configSalt) {		
		byte[] pbkdfHashedArray1 = null; 
		byte[] pbkdfHashedArray2 = null; 
		try {
			PBEKeySpec keySpecP1 = new PBEKeySpec(charSecret, salt, ITERATION_COUNT, DERIVED_KEY_LENGTH);
			
			SecretKeyFactory pbkdfKeyFactory = SecretKeyFactory.getInstance(PDKDF_ALGORITHM);
			
			pbkdfHashedArray1 = pbkdfKeyFactory.generateSecret(keySpecP1).getEncoded();
			PBEKeySpec keySpecP2 = new PBEKeySpec(
					Base64.getEncoder().encodeToString(pbkdfHashedArray1).toCharArray(), 
					Base64.getDecoder().decode(configSalt), 
					PASSWORD_ITERATION_COUNT, 
					DERIVED_KEY_LENGTH);
			pbkdfHashedArray2 = pbkdfKeyFactory.generateSecret(keySpecP2).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			log.error("Severe error, no password generated! e:"+e);
			return null;
		}

		//log.info("hash:"+Base64.getEncoder().encodeToString(pbkdfHashedArray2));
		log.info("hash length:"+pbkdfHashedArray2.length);
		return(pbkdfHashedArray2);
	}
	
	public static void KeyGen() {
		KeyGenerator keygen;
		
		try {
			keygen = KeyGenerator.getInstance("AES");

			byte[] salt = new byte[512] ;
			SecureRandom secRandom = new SecureRandom() ;
			secRandom.nextBytes(salt) ;
			
			keygen.init(256, secRandom);
			byte[] key = keygen.generateKey().getEncoded();
			log.info("key:"+Base64.getEncoder().encodeToString(key));
			SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
			log.info("skeySpec:"+Base64.getEncoder().encodeToString(skeySpec.getEncoded()));
			
		} catch (NoSuchAlgorithmException e) {
			log.info("Error keygen");
		}	
	}

	static final String AB = "23456789ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz-";
	public static String GenerateCode(Blackboard blackboard, int len) {
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ) 
			sb.append( AB.charAt(blackboard.getSecureRandom().nextInt(AB.length()) ) );
		return sb.toString();
	}
	
	protected String returnStringRep(byte[] data) {
		return Base64.getEncoder().encodeToString(data);
	}

	protected byte[] returnByteArray(String data) {
		return Base64.getDecoder().decode(data);
	}
}
