package broadcast;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import broadcast.api.ApiHostCreator;
import broadcast.broadcaster.SoundHelpers;
import broadcast.business.RefreshBlackboard;
import broadcast.janus.JAdminClient;
import broadcast.janus.JClient;
import broadcast.master.MasterInit;
import broadcast.matomo.GeoIp;
import broadcast.tasks.PeopleTask;
import broadcast.tasks.PingTask;
import broadcast.tasks.StatsTask;
import broadcast.web.HttpEmbedded;
import shared.data.OneMydetvChannel;
import shared.data.Config;
import shared.db.DbChannels;
import shared.db.DbLog;
import shared.db.DbWebUser;
import shared.setups.SetupEncryption;
import shared.setups.SwarmConfigException;

public class BroadcastImpl implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(BroadcastImpl.class);	

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private HttpEmbedded httpEmbedded;
	@Inject private DbChannels dbChannels;
	@Inject private PingTask pingTask;
	@Inject private StatsTask statsTask;
	@Inject private PeopleTask peopleTask;
	@Inject private SoundHelpers soundHelpers;
	@Inject private JAdminClient jAdminClient;
	@Inject Provider<JClient> providerJClient;
	@Inject private MasterInit masterInit;
	@Inject private ShutdownHook shutdownHook;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbLog dbLog;
	@Inject private RefreshBlackboard refreshBlackboard;
	@Inject private BroadcastTest broadcastTest;
	@Inject private ApiHostCreator apiHostCreator;
	@Inject private GeoIp geoIp;

	@Override
	public void run() {
		try {
			start();
		} catch (SwarmConfigException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void start() throws SwarmConfigException {
		
		//BroadcastTest.DoTest();

		blackboard.setConvertedTypes(Arrays.asList("vp8-opus", "vp8-vorbis", "vp9-opus"));
		blackboard.setConvertedTypes(Arrays.asList("vp8-opus"));
		
		//OneEmail email = dbWebUser.getEmail("phomlish@gmail.com");
		//if(email==null) log.info("null");
		//else log.info("emailId:"+email.getIdEmail());
		//SendMail.SendTestEmails(config);
		//System.exit(0);
		try {
			// create our tmp directory. 
			String tempFPN = config.getTmpDir()+"/broadcast";
			log.info("creating "+tempFPN);
			
			// we need to create our temp people directory.  This is where we save people's images from their login provider
			File tempPeopleDir = new File(tempFPN+"/web/people");
			tempPeopleDir.mkdirs();
			if(!tempPeopleDir.exists())
				throw new SwarmConfigException("Error creating "+tempPeopleDir);

			// we need the image of the buffalo
			Path src = Paths.get(config.getRootDir()+"/web/images/buffalo.png");
			//log.info("src:"+sharedConfig.getRootDir()+"/web/people/buffalo.jpg");
			Path dest = Paths.get(tempPeopleDir+"/buffalo.png");
			//log.info("dest:"+tempPeopleDir+"/buffalo.png");
			Files.copy(src, dest, StandardCopyOption.REPLACE_EXISTING);
			File buffalo = new File(tempPeopleDir+"/buffalo.png");
			if(! buffalo.exists()) 
				throw new SwarmConfigException("Error creating "+tempPeopleDir+"/buffalo.png");
			
		} catch (IOException e) {
			throw new SwarmConfigException("Error dealing with tmp dir "+e);
		}
		
		SetupEncryption.SetupHttpsTruststore(config);
		blackboard.setSslContextFactory(SetupEncryption.SetupHttpsKeystore(config));
		blackboard.setOutgoingApiEncryption(SetupEncryption.GetOutgoingApiEncryption(config));
		dbLog.info("starting");

		//dbWebUsersCheck.getCheckWebUsers();
		dbWebUser.updateLoginRestart();
			
		//broadcastTest.doTest2();
		
		blackboard.setMydetvChannels(dbChannels.getChannels());
		dbChannels.resetChannelWebUsers();
		refreshBlackboard.refreshBlackboard();
		
		// start up the web thread
		Thread httpEmbeddedThread = new Thread(httpEmbedded);
		httpEmbeddedThread.setName("httpEmbedded");
		httpEmbeddedThread.start();
		blackboard.setThreadEmbedded(httpEmbeddedThread);

		// startup our HttpClient
		HttpClient httpClient = new HttpClient(blackboard.getSslContextFactory());
		try {
			httpClient.start();
		} catch (Exception e) {
			log.error("Error starting our httpClient");
		}
		
		blackboard.setHttpClient(httpClient);
		apiHostCreator.createMatomoHost();
		geoIp.init();
		// start our janus admin thread if we have the admin secret defined
		if(config.getJanusAdminUrl()!=null && ! config.getJanusAdminUrl().equals("")) {
			Thread janusAdminThread = new Thread(jAdminClient);
			janusAdminThread.setName("janusAdminThread");
			janusAdminThread.start();
			blackboard.setJanusAdminThread(janusAdminThread);
		}
		
		// start our mydetv channel janus and masterServer threads
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			if(! mydetvChannel.isActive())
				continue;
			//if(mydetvChannel.getChannelType()==3)
			//	continue;

			JClient jClient = providerJClient.get();
			jClient.setMydetvChannel(mydetvChannel);
			mydetvChannel.getOjClient().setjClient(jClient);

			Thread thread = new Thread(jClient);
			thread.setName("JClient:"+mydetvChannel.getChannelId());
			thread.setDaemon(true);
			thread.start();
			
			if(mydetvChannel.getChannelType().equals(2))
				masterInit.activateMaster(mydetvChannel);
			
			log.info("done setting up jclient for "+mydetvChannel.getName());
		}
		
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);
		scheduler.scheduleAtFixedRate(pingTask, 30, 30, TimeUnit.SECONDS);
		if(config.getJanusAdminUrl()!=null && ! config.getJanusAdminUrl().equals(""))
			scheduler.scheduleAtFixedRate(statsTask, 1, 1, TimeUnit.SECONDS);
		scheduler.scheduleAtFixedRate(peopleTask, 30, 30, TimeUnit.SECONDS);
		blackboard.setScheduler(scheduler);
		
		soundHelpers.getSounds();
		Runtime.getRuntime().addShutdownHook(new Thread(shutdownHook));
		
		log.info("BROADCAST HAS STARTED, turning over to the threads");
		dbLog.info("started");
	}

	public void stop() {
		log.info("broadcast stopping");
	}

	public void setInjector(Injector mainInjector) {
		blackboard.setMainInjector(mainInjector);
	}

}
