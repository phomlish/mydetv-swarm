package broadcast.broadcaster;

import java.util.HashMap;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.OneSound;
import shared.db.DbSounds;

public class SoundHelpers {

	@Inject private Blackboard blackboard;
	@Inject private DbSounds dbSounds;
	
	public void getSounds() {		
		HashMap<Integer, OneSound> sounds = dbSounds.getSounds();
		synchronized(blackboard.getSoundLock()) {
			blackboard.setSounds(sounds);
		}	
	}
}
