package broadcast.broadcaster;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import broadcast.business.ChatHelpers;
import broadcast.janus.DetachGroup;
import broadcast.janus.JClient;
import broadcast.janus.JDetach;
import broadcast.janus.JShutdown;
import broadcast.master.MasterInit;
import broadcast.master.MasterServer;
import broadcast.master.OneUsbPin;
import broadcast.master.SlaveLights;
import broadcast.master.SlaveRecording;
import broadcast.p2p.CGHelpers;
import broadcast.web.ws.WsHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;
import shared.db.DbChannels;

/**
 * handling messages from broadcasters
 * @author phomlish
 *
 */
public class BroadcasterHelpers {

	@Inject Provider<JClient> providerJClient;
	@Inject private DbChannels dbChannels;
	@Inject private ChatHelpers chatHelpers;
	@Inject private BroadcasterMessaging broadcastMessaging;
	@Inject private WsWriter wsWriter;
	@Inject private MasterInit masterInit;
	@Inject private SlaveLights slaveLights;
	@Inject private SlaveRecording slaveRecording;
	@Inject private JDetach jDetach;
	@Inject private JShutdown jShutdown;
	@Inject private CGHelpers cgHelpers;

	private static final Logger log = LoggerFactory.getLogger(BroadcasterHelpers.class);
	
	/**
	 * comes from wsReader<br>
	 * @param mydetvChannel
	 */
	public void activateChannel(OneMydetvChannel mydetvChannel) {
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();

		if(jClient==null) {
			jClient = providerJClient.get();
			jClient.setMydetvChannel(mydetvChannel);
			mydetvChannel.getOjClient().setjClient(jClient);

			Thread thread = new Thread(jClient);
			thread.setName("JClient:"+mydetvChannel.getChannelId());
			thread.setDaemon(true);
			thread.start();
		}
		else {
			log.error("I didn't think jClient would ever exist here!");
			jClient.startup();
		}

		if(mydetvChannel.getChannelType().equals(2))
			masterInit.activateMaster(mydetvChannel);

		mydetvChannel.setActive(true);
		dbChannels.updateActive(mydetvChannel.getChannelId(), true);	
		chatHelpers.sendAdminChatAllUsers(mydetvChannel.getName()+" is now available");
		JSONObject jo = WsHelpers.UpdateELementProperty("activated", "checked", "true");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, jo);
	}

	/**
	 * comes from wsReader<br>
	 * since this is async we finish this later finishInactivate<br>
	 * @param mydetvChannel
	 */
	public void inactivateChannel(OneMydetvChannel mydetvChannel) {
		log.info("inactivateChannel");

		// do this right away so ppl don't try to reconnect
		mydetvChannel.setActive(false);
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();

		try {
			jClient.setInactivating(true);
			jShutdown.fullJanusShutdown(jClient, false);  // async 
		} catch (Exception e) {
			log.error("Error shutting down jClient "+e);
		}

		// this stuff will happen right away since jClient.shutdown is not async
		if(mydetvChannel.getChannelType().equals(2))
			masterInit.inactivateMaster(mydetvChannel);
	}

	/**
	 * after the async operations are done we'll finish up the inactivate<br>
	 * updates the db the channel is not active<br>
	 * sends to broadcasters to change their controls<br>
	 * @param mydetvChannel
	 */
	public void finishInactivate(OneMydetvChannel mydetvChannel) {
		dbChannels.updateActive(mydetvChannel.getChannelId(), false);

		broadcastMessaging.broadcastAllBroadcastersStatusWarn(mydetvChannel, mydetvChannel.getName()+" is inactive");

		JSONObject jo = WsHelpers.UpdateELementProperty("activated", "checked", "false");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, jo);
	}
	
	/**
	 * removes the broadcaster if there is one<br>
	 * jClient shuts down<br>
	 *  when the program exits/crashes<br>
	 *  when the mydetv channel becomes inactive
	 * @param jClient
	 */
	public void removeBroadcasterJShutdown(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
		ConnectionGroup cgBroadcasterJClient = jClient.getMe().getCgBroadcaster();
		
		// only remote has broadcasters
		if(mydetvChannel.getChannelType() != 0)
			return;

		// maybe we don't have a broadcaster		
		if(cgBroadcasterJClient.getUpstreams().size()!=1)
			return;	

		RtcConnection rtc = cgBroadcasterJClient.getUpstreams().get(0);
		log.info("removing broadcast rtc "+rtc.log());
		
		List<RtcConnection> detachList = new ArrayList<RtcConnection>();
		detachList.add(rtc);
		DetachGroup dg = jDetach.syncDetach(mydetvChannel, detachList);
		if(dg.getDetaches().size()<1) 
			log.error("Error adding su's rtc to detach group");
		
		jDetach.waitForDetaches(jClient, dg);
		log.info("detach finished "+rtc.log());	
		
		// if he's not gone remove the rtc connection
		SessionUser broadcaster = rtc.getFromUser();
		if(broadcaster.getWebSocketConnection()!=null 
			&& broadcaster.getWebSocketConnection().getWebsocketSession()!=null) {
			Session session = (Session) broadcaster.getWebSocketConnection().getWebsocketSession();
			if(session.isOpen()) {
				ConnectionGroup cgBroadcasterSU = broadcaster.getCgBroadcaster();
				log.info("shutting down broadcaster "+broadcaster.log());
				wsWriter.sendRemoveRTCConnection(rtc.getFromUser(), rtc);
				cgHelpers.removeUpstream(cgBroadcasterJClient, rtc);
				//cgBroadcasterJClient.setUpstream(null);
				//broadcaster.setbroadcastingLive(false);
				ArrayList<RtcConnection> td = new ArrayList<RtcConnection>(cgBroadcasterSU.getDownstreams());
				for(RtcConnection removeRtc : td) {
					cgBroadcasterSU.getDownstreams().remove(removeRtc);
				}
				cgBroadcasterSU.setTier(-1);
			}
		}
	}
	/**
	 * removes the broadcaster if there is one<br>
	 * @param jClient
	 */
	public void removeBroadcasterStopLive(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();

		// only remote has broadcasters
		if(mydetvChannel.getChannelType() != 0)
			return;

		ConnectionGroup cgBroadcasterJClient = jClient.getMe().getCgBroadcaster();
		
		if(cgBroadcasterJClient.getUpstreams().size()!=1) {
			log.info("we don't have one broadcaster "+cgBroadcasterJClient.getUpstreams().size());
		}
		
		//RtcConnection rtc = cgBroadcasterJClient.getUpstream();
		RtcConnection rtc = cgBroadcasterJClient.getUpstreams().get(0);
		SessionUser broadcaster = rtc.getFromUser();
		ConnectionGroup cgBroadcasterSU = broadcaster.getCgBroadcaster();
		
		log.info("shutting down downstreams");
		List<RtcConnection> detachList = new ArrayList<RtcConnection>();
		detachList.add(rtc);
		DetachGroup dg = jDetach.syncDetach(mydetvChannel, detachList);
		if(dg.getDetaches().size()<1) 
			log.error("Error adding su's rtc to detach group");		
		jDetach.waitForDetaches(jClient, dg);
		log.info("detach finished "+rtc.log());
		
		// if he's not gone gone remove the rtc connection
		if(broadcaster.getWebSocketConnection()!=null 
			&& broadcaster.getWebSocketConnection().getWebsocketSession()!=null) {
			Session session = (Session) broadcaster.getWebSocketConnection().getWebsocketSession();
			if(session.isOpen()) {
				log.info("shutting down broadcaster "+broadcaster.log());
				wsWriter.sendRemoveRTCConnection(rtc.getFromUser(), rtc);
				cgHelpers.removeUpstream(cgBroadcasterJClient, rtc);
				//cgBroadcasterJClient.setUpstream(null);
				//broadcaster.setbroadcastingLive(false);
				ArrayList<RtcConnection> td = new ArrayList<RtcConnection>(cgBroadcasterSU.getDownstreams());
				for(RtcConnection removeRtc : td) {
					cgBroadcasterSU.getDownstreams().remove(removeRtc);
				}
				cgBroadcasterSU.setTier(-1);
			}
		}	

		// final cleanup
		jClient.setLive(false);
		jClient.setBroadcaster(jClient.getMe());
		//cgBroadcasterJClient.setUpstream(null);
		//broadcaster.getRtcConnections().remove(rtc.getRtcUUID());
	}
	
	/**
	 * removes the broadcaster if there is one<br>
	 * @param jClient
	 */
	public void removeBroadcasterSessionUserClose(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();

		// only remote has broadcasters
		if(mydetvChannel.getChannelType() != 0)
			return;

		ConnectionGroup cgBroadcasterJClient = jClient.getMe().getCgBroadcaster();
		
		if(cgBroadcasterJClient.getUpstreams().size()!=1) {
			log.info("we don't have one broadcaster "+cgBroadcasterJClient.getUpstreams().size());
			return;
		}
		
		RtcConnection rtc = cgBroadcasterJClient.getUpstreams().get(0);
		SessionUser broadcaster = rtc.getFromUser();
		ConnectionGroup cgBroadcasterSU = broadcaster.getCgBroadcaster();
		
		log.info("shutting down downstreams");
		List<RtcConnection> detachList = new ArrayList<RtcConnection>();
		detachList.add(rtc);
		DetachGroup dg = jDetach.syncDetach(mydetvChannel, detachList);
		if(dg.getDetaches().size()<1) 
			log.error("Error adding su's rtc to detach group");		
		jDetach.waitForDetaches(jClient, dg);
		log.info("detach finished "+rtc.log());
		
		// if he's not gone gone remove the rtc connection
		if(broadcaster.getWebSocketConnection()!=null 
			&& broadcaster.getWebSocketConnection().getWebsocketSession()!=null) {
			Session session = (Session) broadcaster.getWebSocketConnection().getWebsocketSession();
			if(session.isOpen()) {
				log.info("shutting down broadcaster "+broadcaster.log());
				wsWriter.sendRemoveRTCConnection(rtc.getFromUser(), rtc);
				cgHelpers.removeUpstream(cgBroadcasterJClient, rtc);
				//cgBroadcasterJClient.setUpstream(null);
				//broadcaster.setbroadcastingLive(false);
				ArrayList<RtcConnection> td = new ArrayList<RtcConnection>(cgBroadcasterSU.getDownstreams());
				for(RtcConnection removeRtc : td) {
					cgBroadcasterSU.getDownstreams().remove(removeRtc);
				}
				cgBroadcasterSU.setTier(-1);
			}
		}	

		// final cleanup
		jClient.setLive(false);
		jClient.setBroadcaster(jClient.getMe());
		//cgBroadcasterJClient.setUpstream(null);
		//broadcaster.getRtcConnections().remove(rtc.getRtcUUID());
	}
	
	public void switchToRemoteSid(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
		removeBroadcasterStopLive(jClient);
		jClient.createFfThread();
		
		JSONObject message;
	
		// we need to tell ppl with camera active to make sourceLive available
		message = WsHelpers.UpdateELementProperty("sourceLive", "checked", "");
		broadcastMessaging.messageAllBroadcastersWithLocalVideo(mydetvChannel, message);
		message = WsHelpers.UpdateELementProperty("sourceLive", "disabled", "");
		broadcastMessaging.messageAllBroadcastersWithLocalVideo(mydetvChannel, message);
		
		// we need to tell everyone to make sid unavailable
		message = WsHelpers.UpdateELementProperty("sid", "checked", "checked");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		message = WsHelpers.UpdateELementProperty("sid", "disabled", "disabled");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
	}
	
	public void switchToStudioLive(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
		
		// start the async process to switch the media player to live
		jClient.setLive(true);
		jClient.setBroadcaster(jClient.getMe());
		jClient.getFfThread().setInteruptFfmpegProcess(true);
		
		JSONObject message;

		// we need to tell everyone to make live checked
		message = WsHelpers.UpdateELementProperty("live", "checked", "true");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		
		// we need to tell everyone to make sid unchecked
		message = WsHelpers.UpdateELementProperty("sid", "checked", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		
		// make recording enabled
		message = WsHelpers.UpdateELementProperty("recording", "disabled", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		
		slaveRecording.updateRecordingLight(mydetvChannel);
	}
	
	public void switchToStudioSid(JClient jClient) {
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
		
		// start the async process to switch the media player to live
		jClient.setLive(false);
		jClient.setBroadcaster(null);
		jClient.getFfThread().setInteruptFfmpegProcess(true);

		JSONObject message;

		// we need to tell everyone to make sid disabled
		message = WsHelpers.UpdateELementProperty("sid", "checked", "checked");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
				
		// we need to tell everyone to make live available
		message = WsHelpers.UpdateELementProperty("live", "checked", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		
		// make recording disabled
		message = WsHelpers.UpdateELementProperty("recording", "disabled", "disabled");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		
		if(mydetvChannel.isRecording())
			stopRecording(jClient);
		else
			slaveRecording.updateRecordingLight(mydetvChannel);
	}
	
	// from broadcasterHelpers
	public void startRecording(JClient jClient) {
		OneMydetvChannel mydetvChannel=jClient.getMydetvChannel();
		mydetvChannel.setRecording(true);
		slaveRecording.startRecording(mydetvChannel);
		JSONObject message = WsHelpers.UpdateELementProperty("recording", "checked", "checked");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);	
	}
	// from broadcasterHelpers
	public void stopRecording(JClient jClient) {
		OneMydetvChannel mydetvChannel=jClient.getMydetvChannel();
		mydetvChannel.setRecording(false);
		slaveRecording.stopRecording(mydetvChannel);
		JSONObject message = WsHelpers.UpdateELementProperty("recording", "checked", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);	
	}
	
	
	public void fixRedLight(JClient jClient) {
		MasterServer masterServer=null;
		masterServer = (MasterServer) jClient.getMydetvChannel().getMasterServer();
		if(masterServer==null)
			return;
		
		OneUsbPin red = masterServer.getUsbPinsByColor().get("red");
		OneMydetvChannel chan=jClient.getMydetvChannel();
		boolean recording = false;
		boolean live = false;
		if(chan.isRecording())
			recording=true;
		if(jClient.isLive())
			live=true;
		
		String newControl = "";
		if(! recording && ! live)
			 newControl="off";
		else if(recording && ! live)
			newControl="on";
		else if(recording && live)
			newControl="flash";
		else
			newControl="double";
		
		if(! newControl.equals(red.getControl())) {
			red.setControl(newControl);
			slaveLights.updateLight("red", newControl);
		}
	}
}
