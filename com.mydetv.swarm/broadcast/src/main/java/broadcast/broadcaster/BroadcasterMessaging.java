package broadcast.broadcaster;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

public class BroadcasterMessaging {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BroadcasterMessaging.class);
	@Inject private WsWriter wsWriter;
	@Inject private UserMessageHelpers userMessageHelpers;
	
	public void broadcastAllBroadcastersStatusInfo(OneMydetvChannel mydetvChannel, String message) {
		List<SessionUser> broadcasters = findAllBroadcasters(mydetvChannel);
		for(SessionUser su : broadcasters) 
			userMessageHelpers.sendUserMessage(su, "info", message);		
	}
	public void broadcastAllBroadcastersStatusWarn(OneMydetvChannel mydetvChannel, String message) {
		List<SessionUser> broadcasters = findAllBroadcasters(mydetvChannel);
		for(SessionUser su : broadcasters) 
			userMessageHelpers.sendUserMessage(su, "warn", message);		
	}
	
	public void sendBroadcasterError(SessionUser su, String error) {
		JSONObject message = new JSONObject();
		message.put("broadcaster", "error");
		message.put("error", error);
		sendBroadcaster(su, message);
	}
	
	public void sendBroadcaster(SessionUser su, JSONObject message) {
		message.put("verb", "msgBroadcaster");
		wsWriter.sendMessage(su, message);			
	}
	public void sendBroadcasterStatus(SessionUser su, JSONObject message) {
		message.put("verb", "msgBroadcaster");
		wsWriter.sendMessage(su, message);			
	}
	/**
	 * Will ws send broadcast broadcast message
	 * @param mydetvChannel
	 * @param presenterMessage
	 */
	public void broadcastAllBroadcasters(OneMydetvChannel mydetvChannel, String presenterMessage) {
		JSONObject message = new JSONObject()
			.put("verb", "msgBroadcaster")
			.put("broadcaster", presenterMessage)
			;
		messageAllBroadcasters(mydetvChannel, message);
	}
	/**
	 * Will ws send broadcast broadcast message<br>
	 * @param mydetvChannel
	 * @param presenterMessage
	 * @param args
	 */
	public void broadcastAllBroadcasters(OneMydetvChannel mydetvChannel, String presenterMessage, String args) {
		JSONObject message = new JSONObject()
			.put("verb", "msgBroadcaster")
			.put("broadcaster", presenterMessage)
			.put("args", args)
			;
		messageAllBroadcasters(mydetvChannel, message);		
	}
	public void broadcastAllBroadcasters(OneMydetvChannel mydetvChannel, JSONObject jo) {
		jo.put("verb", "msgBroadcaster");
		messageAllBroadcasters(mydetvChannel, jo);		
	}
	
	/**
	 * Will ws send p2p message
	 * if you want this to be a broadcast message use broadcastAllBroadcasters instead
	 * @param mydetvChannel
	 * @param jo
	 */
	public void messageAllBroadcasters(OneMydetvChannel mydetvChannel, JSONObject jo) {
		List<SessionUser> broadcasters = findAllBroadcasters(mydetvChannel);
		for(SessionUser su : broadcasters) 
			wsWriter.sendMessage(su, jo);
	}
	/**
	 * Will ws send p2p message<br>
	 * if you want this to be a broadcast message use broadcastAllBroadcasters instead
	 * @param mydetvChannel
	 * @param jo
	 */
	public void messageAllBroadcastersWithLocalVideo(OneMydetvChannel mydetvChannel, JSONObject jo) {
		List<SessionUser> broadcasters = findAllBroadcasters(mydetvChannel);
		for(SessionUser su : broadcasters) 
			if(su.getLocalVideo()!=null) 
				wsWriter.sendMessage(su, jo);
	}
	/**
	 * Will ws send p2p message<br>
	 * if you want this to be a broadcast message use broadcastAllBroadcasters instead
	 * @param mydetvChannel
	 * @param jo
	 */
	public void messageAllBroadcastersWithoutLocalVideo(OneMydetvChannel mydetvChannel, JSONObject jo) {
		List<SessionUser> broadcasters = findAllBroadcasters(mydetvChannel);
		for(SessionUser su : broadcasters) 
			if(su.getLocalVideo()==null) 
				wsWriter.sendMessage(su, jo);
	}
	
	public List<SessionUser> findAllBroadcasters(OneMydetvChannel mydetvChannel) {
		List<SessionUser> broadcasters = new ArrayList<SessionUser>();
		for(SessionUser su : mydetvChannel.getSessionUsers().values()) {
			if(su.getBroadcastChannel()!=null && su.getBroadcastChannel().equals(mydetvChannel))
				broadcasters.add(su);
		}
		return broadcasters;
	}
	
}
