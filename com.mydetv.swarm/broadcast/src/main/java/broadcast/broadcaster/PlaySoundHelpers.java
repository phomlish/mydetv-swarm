package broadcast.broadcaster;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.master.SlaveSounds;
import shared.data.OneSound;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.BroadcasterProfile;
import shared.data.webuser.OneWebUser;
import shared.db.DbBroadcasterProfile;
import shared.db.DbSounds;

public class PlaySoundHelpers {

	
	@Inject private BroadcasterMessaging broadcastMessaging;
	@Inject private SlaveSounds slaveSounds;

	@Inject private DbBroadcasterProfile dbBP;
	
	private static final Logger log = LoggerFactory.getLogger(PlaySoundHelpers.class);
	
	// we are already in <div class='col-sm-4 left controlsColumn' id='soundColumn'>
	public String drawPlaySoundColumn(SessionUser su, String width) {
		String rv="";
		if(su.getWebUser()==null) {
			log.error("how did we get here "+su.getMydetv());
			return rv;
		}
		OneWebUser wu = su.getWebUser();
		HashMap<Integer,BroadcasterProfile> bps = new HashMap<Integer,BroadcasterProfile>();
		//synchronized(wu.getBroadcasterProfilesLock()) {
		//	bps = dbBP.getBPs(wu.getWuid(), 1); // sounds are bpype=1
		//	wu.setBroadcasterProfiles(bps);
		//}
		
		for(BroadcasterProfile bp : wu.getBroadcasterProfiles().values())
			if(bp.getBpType().equals(0))
				bps.put(bp.getBpId(), bp);
		
		// draw a tiny pencil for editing
		rv+="<a href='/sounds' target='_blank' data-toggle='tooltip' title='Edit Sounds!'>";
		rv+="<i class='fas fa-pencil-alt' aria-hidden='true' style='color:yellow' ";
		rv+="onclick='editBroadcastMessages()'></i></a> &nbsp";
		
		// draw a refresh button
		rv+="<a href='#' data-toggle='tooltip' title='Refresh Sounds!'>";
		rv+="<i class='fas fa-redo' aria-hidden='true' style='color:blue' ";
		rv+="onclick='getSoundsGridData'></i></a>";
		
		rv+="<br/>\n";
		rv+=drawSoundProfilesTable(width);
		return rv;
	}
	
	public String drawSoundsTable() {
		String rv="";
		rv+="Available Sounds";
		rv+="<div id='soundsDiv'>";
		rv+="<table id='soundsTable' class='table table-bordered compact' ";
		//rv+=width:'60%'>";
		rv+="><thead><tr>";
		rv+="<th>id</th> <th>pn</th> <th>fn</th> <th class='th-Sound'>Sound</th> <th>Play</th> <th>Add/Remove</th>";
		rv+="</tr></thead>";
		rv+="</table>";
		rv+="</div>";
		return rv;
	}
	
	public String drawSoundProfilesTable(String page) {
		String rv="";
		rv+="<div id='profileSoundsDiv'>";
		rv+="<table id='profileSoundsTable' class='table table-bordered compact'";
		//rv+=" width:'60%'>";
		rv+="><thead><tr>";
		rv+="<th>id</th> <th>pn</th> <th>fn</th> <th class='thSound'>Sound</th>";
		rv+="<th>Play Local</th> <th>Play Remote</th>";
		rv+="<th>Remove</th> <th>up/down</th> <th>Seq</th>"; //
		rv+="</tr></thead>";
		rv+="</table>";
		rv+="</div>";
		return rv;
	}
	
	public String drawSoundProfilesTableForBroadcastPage(String page) {
		String rv="";
		rv+="<div id='profileSoundsDiv'>";
		rv+="<table id='profileSoundsTable' class='table table-bordered compact'";
		//rv+=" width:'60%'>";
		rv+="><thead><tr>";
		rv+="<th>id</th><th>Play Remote</th>";
		rv+="</tr></thead>";
		rv+="</table>";
		rv+="</div>";
		return rv;
	}

}
