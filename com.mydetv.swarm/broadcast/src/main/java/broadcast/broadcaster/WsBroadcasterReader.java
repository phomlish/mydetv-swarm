package broadcast.broadcaster;

import javax.inject.Inject;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.business.Authorized;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

/**
 * incoming and outgoing broadcaster low level messages<br>
 * all authenticated/authorized validations are done in the class
 * @author phomlish
 *
 */
public class WsBroadcasterReader {

	private static final Logger log = LoggerFactory.getLogger(WsBroadcasterReader.class);
	@Inject private Authorized authorized;
	@Inject private BroadcasterMessages broadcasterMessages;
	
	
	public void wsMessageBroadcaster(JSONObject response, SessionUser su) {

		if(response.isNull("channelId")) {
			log.error(su.log()+" didn't supply channelId");
			return;
		}
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		Integer channelId = response.getInt("channelId");
		if(mydetvChannel.getChannelId() != channelId) {
			log.error(su.log()+" can't do anything, he's not in "+mydetvChannel.getName());
			return;
		}
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			log.error(su.log()+" is not authorized to manipulate broadcasting for "+mydetvChannel.getName());
			return;
		}
			
		String broadcaster = response.getString("broadcaster");
		log.warn(su.log()+" sent broadcaster command "+broadcaster);
		
		switch(broadcaster) {
		// common
		case "activateChannel":
			broadcasterMessages.wsActivateChannel(su);
			break;
		case "inactivateChannel":
			broadcasterMessages.wsInactivateChannel(su);
			break;
			
		// remote
		case "goRemoteSourceLive":
			broadcasterMessages.wsRemoteSourceLive(su, response);
			break;
		case "goRemoteSid":
			broadcasterMessages.wsRemoteSid(su, response);
			break;
			
		// studio
			
		case "goStudioLive":
			broadcasterMessages.wsStudioSourceLive(su, response);
			break;
		case "goStudioSid":
			broadcasterMessages.wsStudioSid(su, response);
			break;			
		case "goStartRecording":
			broadcasterMessages.wsStartRecording(su);
			break;
		case "goStopRecording":
			broadcasterMessages.wsStopRecording(su);
			break;
		case "silence":
			broadcasterMessages.wsSilence(su);
			break;
		case "play":
			broadcasterMessages.wsPlay(su);
			break;
		default:
			log.error("didn't recognize broadcaster:"+broadcaster);
		}
	}

}
