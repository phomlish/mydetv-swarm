package broadcast.broadcaster;

import java.time.LocalDateTime;

import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class OneBroadcaster {

	private Integer broadcasterCnt;
	private LocalDateTime dtStarted;
	private SessionUser sessionUser;
	private RtcConnection rtc;
	public Integer getBroadcasterCnt() {
		return broadcasterCnt;
	}
	public void setBroadcasterCnt(Integer broadcasterCnt) {
		this.broadcasterCnt = broadcasterCnt;
	}
	public LocalDateTime getDtStarted() {
		return dtStarted;
	}
	public void setDtStarted(LocalDateTime dtStarted) {
		this.dtStarted = dtStarted;
	}
	public SessionUser getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(SessionUser sessionUser) {
		this.sessionUser = sessionUser;
	}
	public RtcConnection getRtc() {
		return rtc;
	}
	public void setRtc(RtcConnection rtc) {
		this.rtc = rtc;
	}
}
