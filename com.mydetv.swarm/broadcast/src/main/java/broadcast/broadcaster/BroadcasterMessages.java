package broadcast.broadcaster;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.Authorized;
import broadcast.janus.JClient;
import broadcast.master.JukeboxActions;
import broadcast.web.ws.WsHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

public class BroadcasterMessages {

	private static final Logger log = LoggerFactory.getLogger(BroadcasterMessages.class);
	@Inject private BroadcasterHelpers broadcasterHelpers;
	@Inject private BroadcasterMessaging broadcastMessaging;
	@Inject private WsWriter wsWriter;
	@Inject private Authorized authorized;
	@Inject private JukeboxActions jukeboxActions;
	
	public void wsActivateChannel(SessionUser su) {
		if(su.getMydetvChannel()==null) {
			broadcastMessaging.sendBroadcasterError(su, "not in a channel");
			return;
		}
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		if(mydetvChannel.getChannelType().equals(1)) {
			broadcastMessaging.sendBroadcasterError(su, "can't activate a 24x7 channel");
			return;	
		}
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to activate "+mydetvChannel.getName());
			return;	
		}	
		if(mydetvChannel.isActive()) {
			broadcastMessaging.sendBroadcasterError(su, "channel is already active "+mydetvChannel.getName());
			return;	
		}		
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is activating");
		broadcasterHelpers.activateChannel(mydetvChannel);
	}

	public void wsInactivateChannel(SessionUser su) {
		if(su.getMydetvChannel()==null) {
			broadcastMessaging.sendBroadcasterError(su, "not in a channel");
			return;
		}
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		if(mydetvChannel.getChannelType().equals(1)) {
			broadcastMessaging.sendBroadcasterError(su, "can't inactivate a 24x7 channel");
			return;	
		}
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to inactivate "+mydetvChannel.getName());
			return;	
		}
		if(! mydetvChannel.isActive()) {
			broadcastMessaging.sendBroadcasterError(su, "channel is already inactive "+mydetvChannel.getName());
			return;	
		}
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		if(jClient==null) {
			broadcastMessaging.sendBroadcasterError(su, "meltdown jClient does not exists "+mydetvChannel.getName());
			return;	
		}
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is inactivating");
		broadcasterHelpers.inactivateChannel(mydetvChannel);
	}
	
	/**
	 * this user wants to send their live feed
	 * @param su
	 * @param jo
	 */
	public void wsRemoteSourceLive(SessionUser su, JSONObject jo) {
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to inactivate "+mydetvChannel.getName());
			return;	
		}
		
	    if(! mydetvChannel.isActive()) {
	    	log.info(su.log()+" tried to start live an inactive channel "+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
	    }
	   	if(mydetvChannel.getChannelType().equals(1)) {
	    	log.info(su.log()+" tried to start live a 24x7 channel "+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
	   	}
	   	if(jClient.getBroadcaster()!=null && jClient.getBroadcaster().equals(su)) {
	    	log.info(su.log()+" tried to start live but they are already the broadcaser "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
	   	}
	   	
	   	broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is going live");
		if(jClient.isLive()) {
	    	log.info("presenter tried to start live an already live channel "+su.log()+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
		}

		jClient.setLive(true);
		jClient.setBroadcaster(su);
		jClient.getFfThread().setDone(true);	
		jClient.getFfThread().setInteruptFfmpegProcess(true);
		
		JSONObject message;
		message = WsHelpers.UpdateELementProperty("sourceLive", "disabled", "true");
		wsWriter.sendMessage(su, message);
		message = WsHelpers.UpdateELementProperty("sourceLive", "checked", "true");
		wsWriter.sendMessage(su, message);
		
		message = WsHelpers.UpdateELementProperty("sid", "checked", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
		message = WsHelpers.UpdateELementProperty("sid", "disabled", "");
		broadcastMessaging.messageAllBroadcasters(mydetvChannel, message);
	}
	
	public void wsRemoteSid(SessionUser su, JSONObject jo) {
		log.info("wsRemoteSid");
		
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to inactivate "+mydetvChannel.getName());
			return;	
		}
		
		if(! mydetvChannel.isActive()) {
			log.error("presenter tried to wsRemoteSid an inactive channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}
		if(mydetvChannel.getChannelType().equals(1)) {
			log.error("presenter tried to wsRemoteSid a 24x7 channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}
		if(! jClient.isLive()) {
			log.error("presenter tried to wsRemoteSid a non live channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}

		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is going sid");
		broadcasterHelpers.switchToRemoteSid(jClient);		
	}

	public void wsStudioSourceLive(SessionUser su, JSONObject jo) {
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to inactivate "+mydetvChannel.getName());
			return;	
		}
		
	    if(! mydetvChannel.isActive()) {
	    	log.info(su.log()+" tried to start live an inactive channel "+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
	    }
	   	if(mydetvChannel.getChannelType().equals(1)) {
	    	log.info(su.log()+" tried to start live a 24x7 channel "+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
	   	}
	   	
		if(jClient.isLive()) {
	    	log.info("presenter tried to start live an already live channel "+su.log()+" "+mydetvChannel.getName());
	    	broadcastMessaging.sendBroadcasterError(su, "errorLiveOn");
	    	return;
		}
		
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is going live");		
		broadcasterHelpers.switchToStudioLive(jClient);		
	}
	
	public void wsStudioSid(SessionUser su, JSONObject jo) {
		log.info("wsStudioSid");
		
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to inactivate "+mydetvChannel.getName());
			return;	
		}
		
		if(! mydetvChannel.isActive()) {
			log.error("presenter tried to wsRemoteSid an inactive channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}
		if(mydetvChannel.getChannelType().equals(1)) {
			log.error("presenter tried to wsRemoteSid a 24x7 channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}
		if(! jClient.isLive()) {
			log.error("presenter tried to wsRemoteSid a non live channel "+su.log()+" "+mydetvChannel.getName());
			broadcastMessaging.sendBroadcasterError(su, "errorLiveOff");
			return;
		}
		
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" is going sid");
		broadcasterHelpers.switchToStudioSid(jClient);	
	}

	public void wsStartRecording(SessionUser su) {
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" start recording");
		broadcasterHelpers.startRecording((JClient) mydetvChannel.getOjClient().getjClient());
	}
	public void wsStopRecording(SessionUser su) {
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" stop recording");
		broadcasterHelpers.stopRecording((JClient) mydetvChannel.getOjClient().getjClient());
	}
	
	public void wsSilence(SessionUser su) {
		if(su.getMydetvChannel()==null) {
			broadcastMessaging.sendBroadcasterError(su, "not in a channel");
			return;
		}
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" silence");
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to silence jukebox "+mydetvChannel.getName());
			return;	
		}
		jukeboxActions.silence(mydetvChannel);
	}
	
	public void wsPlay(SessionUser su) {
		if(su.getMydetvChannel()==null) {
			broadcastMessaging.sendBroadcasterError(su, "not in a channel");
			return;
		}
		
		OneMydetvChannel mydetvChannel=su.getMydetvChannel();
		if(! authorized.isBroadcaster(su.getWebUser(), mydetvChannel.getChannelId())) {
			broadcastMessaging.sendBroadcasterError(su, "not authorized to play jukebox "+mydetvChannel.getName());
			return;	
		}		
		broadcastMessaging.broadcastAllBroadcastersStatusInfo(mydetvChannel, su.log()+" play jukebox");
		jukeboxActions.resume(mydetvChannel);
	}
}
