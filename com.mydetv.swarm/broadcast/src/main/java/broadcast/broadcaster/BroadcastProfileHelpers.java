package broadcast.broadcaster;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.TreeMap;

import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;

import shared.data.Config;
import shared.data.OneSound;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.BroadcasterProfile;
import shared.data.webuser.BroadcasterProfileDataSound;
import shared.data.webuser.OneWebUser;
import shared.db.DbBroadcasterProfile;
import shared.db.DbConnection;

public class BroadcastProfileHelpers {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private DbConnection dbConnection;
	@Inject private DbBroadcasterProfile dbBP;
	private static final Logger log = LoggerFactory.getLogger(BroadcastProfileHelpers.class);


	public HashMap<Integer,BroadcasterProfile> getBroadcasterProfiles(SessionUser su, Integer bpType) {
		HashMap<Integer,BroadcasterProfile>  hm = new HashMap<Integer,BroadcasterProfile>();

		if(su.getWebUser()==null)
			return hm;

		OneWebUser owu = su.getWebUser();
		if(owu.getBroadcasterProfiles()==null) 
			return hm;

		for(BroadcasterProfile bp : owu.getBroadcasterProfiles().values()) {
			if(bp.getBpType().equals(bpType))
				hm.put(bp.getBpId(), bp);
		}
		return hm;
	}

	private boolean isProfileMine(OneWebUser wu, Integer pbId) {
		if(wu.getBroadcasterProfiles()==null || wu.getBroadcasterProfiles().size()<1)
			return false;
		if(wu.getBroadcasterProfiles().containsKey(pbId))
			return true;		
		return false;
	}

	public void addBPSound(SessionUser su, Integer id) {
		Connection conn = null;
		OneWebUser wu = su.getWebUser();

		try { synchronized(wu.getBroadcasterProfilesLock()) {
			
			conn = dbConnection.getConnection();

			BroadcasterProfileDataSound bpdata = new BroadcasterProfileDataSound();
			bpdata.setOs(blackboard.getSounds().get(id));
			bpdata.setSeq(getSoundProfileCount(su, 0));
			
			BroadcasterProfile bp = new BroadcasterProfile();
			bp.setBpType(0);
			bp.setWuid(su.getWebUser().getWuid());
			bp.setBpData(bpdata);
			JSONObject jBpData = new JSONObject(bpdata);
			Integer bpId = dbBP.addOneBP(su.getWebUser().getWuid(), 0, jBpData.toString());
			bp.setBpId(bpId);
			su.getWebUser().getBroadcasterProfiles().put(bpId, bp);
		} } catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	/**
	 * 
	 * @param su
	 * @param id: the sound id, not the profile id
	 */
	public void deleteBPSound(SessionUser su, Integer id) {
		Connection conn = null;
		OneWebUser wu = su.getWebUser();

		try { synchronized(wu.getBroadcasterProfilesLock()) {
			conn = dbConnection.getConnection();		
			BroadcasterProfile profileToDelete = getBroadcasterProfileBySoundId(su, id);
			dbBP.deleteOneBP(profileToDelete.getBpId());
			wu.getBroadcasterProfiles().remove(profileToDelete.getBpId());
			reorderSoundProfiles(su);
		} } catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
	}
	
	public void updownBpSound(SessionUser su, Integer bpId, Integer ud) {
		String sud;
		if(ud==0) sud="up";
		else sud="down";
		log.info("moving "+bpId+" "+sud);
		
		BroadcasterProfile prev=null;
		Boolean decrementNext=false;
		TreeMap<Integer, BroadcasterProfile> bpSorted = getSortedSounds(su);
		
		for(BroadcasterProfile bp : bpSorted.values()) {
			// ignore non sounds
			if(!bp.getBpType().equals(0))
				continue;
			
			// moving up, prev -1, cur +1
			if(ud==0) {
				// handle moving up cur
				if(bp.getBpId().equals(bpId)) {
					BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
					bpsd.setSeq(bpsd.getSeq()-1);
					bp.setBpData(bpsd);
					JSONObject jo = new JSONObject(bpsd);
					dbBP.updateOneBP(bp.getBpId(), jo.toString());
					// handle moving down prev
					bpsd = (BroadcasterProfileDataSound) prev.getBpData();
					bpsd.setSeq(bpsd.getSeq());
					prev.setBpData(bpsd);
					jo = new JSONObject(bpsd);
					dbBP.updateOneBP(prev.getBpId(), jo.toString());
					
				}
				prev=bp;
			}
			// moving down, cur +1, next -1
			else {
				// handle moving up the next one
				if(decrementNext) {
					BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
					bpsd.setSeq(bpsd.getSeq()-1);
					bp.setBpData(bpsd);
					JSONObject jo = new JSONObject(bpsd);
					dbBP.updateOneBP(bp.getBpId(), jo.toString());
					decrementNext=false;
				} 
				// handle moving up cur
				else if(bp.getBpId().equals(bpId)) {
					BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
					bpsd.setSeq(bpsd.getSeq()+1);
					bp.setBpData(bpsd);
					JSONObject jo = new JSONObject(bpsd);
					dbBP.updateOneBP(bp.getBpId(), jo.toString());
					decrementNext=true;
				}
			}		
		}
	}

	public BroadcasterProfile getBroadcasterProfileBySoundId(SessionUser su, Integer id) {
		for(BroadcasterProfile bp : su.getWebUser().getBroadcasterProfiles().values()) {
			if(! bp.getBpType().equals(0))
				continue;
			BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
			if(bpsd.getOs().getId().equals(id))
				return bp;		}
		
		return null;
	}
	private Integer getSoundProfileCount(SessionUser su, Integer type) {
		Integer count=0;
		for(BroadcasterProfile bp : su.getWebUser().getBroadcasterProfiles().values())
			if(bp.getBpType().equals(type))
				count++;
		return count;		
	}
	
	private void reorderSoundProfiles(SessionUser su) {
		OneWebUser wu = su.getWebUser();
		
		// create hashmap of the sound profiles keyed by 'seq'
		
		TreeMap<Integer, BroadcasterProfile> bpSorted = getSortedSounds(su);
		// resequence the db and broadcasterProfiles
		Integer i = 0;
		for(BroadcasterProfile bp : bpSorted.values()) {
			BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
			Integer oldSeq = bpsd.getSeq();
			if(! oldSeq.equals(i)) {
				bpsd.setSeq(i);
				JSONObject jo = new JSONObject(bpsd);
				dbBP.updateOneBP(bp.getBpId(), jo.toString());
				bp.setBpData(bpsd);
			}
			//wu.getBroadcasterProfiles().put(bp.getBpId(), bp);
			i++;
		}
		/*
		// just here for logging:
		for(BroadcasterProfile bp : wu.getBroadcasterProfiles().values()) {
			if(bp.getBpType().equals(0)) {
				BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
				log.info("seq:"+bpsd.getSeq());
			}
		}
		log.info("new bps:"+wu.getBroadcasterProfiles());
		*/
	}
	
	/**
	 * Database and Webpage bpData is string, we convert to BroadcasterProfileDataSound class
	 * @param strBpsd
	 * @return
	 */
	public void createRealBPdata (HashMap<Integer, BroadcasterProfile> bps) {
		for(BroadcasterProfile bp : bps.values()) {
			switch(bp.getBpType()) {
			case 0:
				BroadcasterProfileDataSound bpsd = createBpsd(bp.getBpData().toString());
				bp.setBpData(bpsd);
				break;
			default:
				log.error("didn't recognize bp type "+bp.getBpType());
			}
		}
	}

	private BroadcasterProfileDataSound createBpsd(String strBpsd) {
		BroadcasterProfileDataSound bpsd = new BroadcasterProfileDataSound();
		try {
			JSONObject jBPSD = new JSONObject(strBpsd);
			JSONObject jOS = jBPSD.getJSONObject("os");
			Integer sid = jOS.getInt("id");
			if(! blackboard.getSounds().containsKey(sid)) {
				log.error("sounds doesn't contain id "+sid);
			}
			OneSound os = blackboard.getSounds().get(sid);
			bpsd.setSeq(jBPSD.getInt("seq"));
			bpsd.setOs(os);
		} catch(JSONException ex) {
			log.error("error ",ex);
		}		
		return bpsd;
	}
	
	private TreeMap<Integer, BroadcasterProfile> getSortedSounds(SessionUser su) {
		TreeMap<Integer, BroadcasterProfile> bpSorted = new TreeMap<Integer, BroadcasterProfile>();
		for(BroadcasterProfile bp : su.getWebUser().getBroadcasterProfiles().values()) {
			// skip non sounds
			if(bp.getBpType() != 0)
				continue;
			BroadcasterProfileDataSound bpsd = (BroadcasterProfileDataSound) bp.getBpData();
			bpSorted.put(bpsd.getSeq(), bp);				
		}
		return bpSorted;
	}
	
}
