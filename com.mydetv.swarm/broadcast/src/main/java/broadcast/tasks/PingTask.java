package broadcast.tasks;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.Config;
import broadcast.janus.JAdminClient;
import broadcast.janus.JAdminControl;
import broadcast.janus.JClient;
import broadcast.janus.JControl;
import broadcast.p2p.VerifyCGBW;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.OneJClient;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;

/**
 * users:<br>
 * if they have not sent a ws in a while, send them a ping<br>
 * if they have not sent a ws in a long while, kill their ws connection which will remove them from the channel<br>
 * the peopleTask will remove them from our maps completely in an extra long while<br>
 * <br>
 * media player:<br>
 * if we have not heard from the media player in a while, send a ping<br>
 * not completely sure we notice and deal with media player disconnects gracefully atm<br>
 * @author phomlish
 *
 */
public class PingTask implements Runnable {

	@Inject private Blackboard blackboard;
	@Inject private WsWriter websocketWriter;
	@Inject private JControl jControl;
	@Inject private JAdminClient jAdminClient;
	@Inject private JAdminControl jAdminControl;
	@Inject private VerifyCGBW verifyCGBW;
	@Inject private Config config;
	private static final Logger log = LoggerFactory.getLogger(PingTask.class);

	public void run() {
		try {
			log.trace("ping task");
			checkUsers();
			if(config.getJanusAdminUrl()!=null) checkJAdminClient();
			checkJclient();
		} catch(Exception e) {
			log.error("exception processing pings:"+e);
		}
	}
	
	private void checkJAdminClient() {

		Long now = System.currentTimeMillis();
		ZonedDateTime nowZDT = ZonedDateTime.now();
		log.trace("checking for jAdminClient' web sockets freshness "+now+" "+nowZDT.toString());
		
		if(jAdminClient.getSession()==null) {
			log.trace("janus admin client has no session!");
			return;
		}
		
		log.trace("looking at jAdminClient "+jAdminClient.getLastDT());
		if(jAdminClient.getLastDT() < (now-14999)) {
			log.trace("old, sending ping to jAdminClient");
			jAdminControl.ping();
		}		
	}
	private void checkJclient() {
		Long now = System.currentTimeMillis();
		ZonedDateTime nowZDT = ZonedDateTime.now();
		log.trace("checking for jClients' web sockets freshness "+now+" "+nowZDT.toString());
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			if(! mydetvChannel.isActive()) 
				continue;
			// the test channel
			if(mydetvChannel.getChannelType()==3)
				continue;
			
			log.trace("Looking at "+mydetvChannel.getName());
			OneJClient ojc = mydetvChannel.getOjClient();
			JClient jClient = (JClient) ojc.getjClient();
			if(jClient==null) {
				log.error("Error, jclient is null!");
				continue;
			}
			
			if(jClient.getSession()==null) {
				log.error("janus has no session for "+mydetvChannel.getChannelId()+"!");
				continue;
			}
			
			log.trace("looking at "+mydetvChannel.getChannelId()+" "+jClient.getLastDT());
			if(jClient.getLastDT() < (now-14999)) {
				log.trace("old, sending ping to jClient:"+mydetvChannel.getChannelId());
				JSONObject jsonObject = new JSONObject()
					.put("janus", "keepalive");							
				jControl.sendTransaction(mydetvChannel, jsonObject);
			}
		}
	}
	
	/**
	 * make sure they are responding to pings<br>
	 * We should catch issues during normal processing but we'll verifyCGBW here just in case we missed something<br>
	 * @throws IOException
	 */
	private void checkUsers() throws IOException {

		Map<Integer, WebSocketConnection> wsc = new HashMap(blackboard.getWebSocketConnections());
			
		for(Entry<Integer, WebSocketConnection> entry : wsc.entrySet()) {
			WebSocketConnection wsConnection = entry.getValue();
			SessionUser su = wsConnection.getSessionUser();
				
			log.warn("synchronized "+su.getWebUser().getUsername());
			synchronized(su.getLock()) {
				checkPing(su);
				verifyCGBW.verifyCGBWs(su);
			}
			log.warn("unsynchronized "+su.getWebUser().getUsername());
			log.trace("analyzing done "+entry.getKey()+" "+su.log());
		}
		
		log.trace("done checkUsers");
	}
	
	private void checkPing(SessionUser su) {
		LocalDateTime now = LocalDateTime.now();
		WebSocketConnection wsc = su.getWebSocketConnection();
		Session wss = (Session) wsc.getWebsocketSession();
		
		// maybe they JUST now left, give them a 5 seconds to sessionUserClose
		LocalDateTime old = now.minusSeconds(5);
		if(su.getWebSocketConnection().getLastPing().isAfter(old)) {
			log.info(su.getWebSocketConnection().getRemote()+" recently active, skipping this ping test");
			return;
		}
		
		if(! wss.isOpen()) {
			log.error("websocket is already closed, should be gone but not, removing "+su.log());
			blackboard.getWebSocketConnections().remove(wsc.hashCode());
			su.setWebSocketConnection(null);
			return;
		}
		
		// responding to pings?
		// we were hoping that closing the connection would kill this but it does not if we had a bug closing
		//if(wsConnection.getLastPing() < (now-61000)) {
		old = now.minusSeconds(61);
		if(wsc.getLastPing().isBefore(old)) {
			log.warn("not responding, killing wsc "+su.log());
			try {
				wss.disconnect();
			} catch (IOException e) {
				log.error("Error closing wss for "+su.log());
			}
			wss.close(StatusCode.SHUTDOWN, "too old");;
			log.warn("disconnected wsc"+su.log());
			return;
		}

		// need a ping?
		old = now.minusSeconds(29);
		if(wsc.getLastPing().isBefore(old)) {
			log.debug("old, sending ping "+su.log());
			JSONObject response = new JSONObject();
			response.put("verb", "ping");
			try {
				websocketWriter.sendMessage(su, response);
			} catch (Exception e) {
				log.error("unable to send ping");
			}
			log.debug("sent fresh ping "+su.log());
			return;
		}
		
		
	}
}
