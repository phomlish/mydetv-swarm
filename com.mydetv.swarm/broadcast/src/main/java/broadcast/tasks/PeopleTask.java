package broadcast.tasks;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.p2p.P2PHelpers;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.db.DbWebUser;

/**
 * remove them from our maps completely in an extra long while<br>
 * the ping task will have already removed them from the channel and cleaned up the RTCs<br>
 * @author phomlish
 *
 */
public class PeopleTask implements Runnable {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private DbWebUser dbWebUser;
	@Inject private P2PHelpers p2pHelpers;
	private static final Logger log = LoggerFactory.getLogger(PeopleTask.class);

	// currently set to run every 30 seconds
	@Override
	public void run() {
		try{
			// make a new hashmap of blackboard session users
			HashMap<String, SessionUser> sessionUsers = new HashMap<String, SessionUser>(blackboard.getSessionUsers());
			// loop through and see who might be lost or old
			for(Entry<String, SessionUser> entry : sessionUsers.entrySet()) {
				SessionUser su = entry.getValue();
				if(su==null) {
					log.error("su is null, should not happen "+entry.getKey());
					blackboard.getSessionUsers().remove(entry.getKey());
				}
				
				// if he is in a channel, skip/continue.  if his websocket closes we remove him from the channel
				OneMydetvChannel mydetvChannel = su.getMydetvChannel();
				if(mydetvChannel != null)
					continue;
			
				// if explicitly logged out, only let him linger for 5 minutes
				int linger = 5;
				//if(config.getInstance().toLowerCase().contains("dev"))
				//	linger=1;
				LocalDateTime old = LocalDateTime.now().minusMinutes(linger);
				if(su.getWebUser()==null && su.getLastSeen().isBefore(old)) {
					blackboard.getTrackingCodes().remove(su.getTrackingCode());
					blackboard.getSessionUsers().remove(su.getMydetv());
					continue;
				}
				
				// he can only sit idle for 4 hours
				linger = 4*60;
				//if(config.getInstance().toLowerCase().contains("dev"))
				//	linger=15;
				old = LocalDateTime.now().minusMinutes(linger);
				if(su.getLastSeen().isBefore(old)) {
					log.info("too old "+entry.getKey()+"("+su.log()+") removing from our maps (he'll have to reauth) :"
						+su.getMydetv());
					blackboard.getTrackingCodes().remove(su.getTrackingCode());
					dbWebUser.updateLogin(su, "timeout");
					blackboard.getSessionUsers().remove(su.getMydetv());
					continue;
				}				
			}
		}catch(Exception e){
			log.error("Exception running peopleTask:"+e);
		}
	}
	
}
