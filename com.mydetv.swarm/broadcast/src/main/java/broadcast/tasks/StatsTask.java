package broadcast.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.janus.JAdminClient;
import broadcast.janus.JAdminControl;
import broadcast.janus.JClient;
import broadcast.janus.JHelpers;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

/**
 * We'll use sessionUsers who are authenticated and in a channel to get the statuses<br>
 * 
 * @author phomlish
 *
 */
public class StatsTask implements Runnable {

	@Inject private Blackboard blackboard;
	@Inject private JHelpers jHelpers;
	@Inject private JAdminClient jAdminClient;
	@Inject private JAdminControl jAdminControl;
	private static final Logger log = LoggerFactory.getLogger(StatsTask.class);

	@Override
	public void run() {
		try {
			for(SessionUser su :blackboard.getSessionUsers().values()) {
			    if(! su.isAuthenticated())
			    	continue;
			    if(su.getWebUser()==null || su.getWebUser().getDbChannelWebUsersId()<1)
			    	continue;
			    if(su.getMydetvChannel() == null)
			    	continue;
			    OneMydetvChannel mydetvChannel = su.getMydetvChannel();
			    if(! mydetvChannel.isActive())
			    	continue;
			    JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			    if(jClient == null)
			    	continue;
			    if(jClient.getMe().getMydetv().equals(su.getMydetv()))
			    	continue;
			    RtcConnection rtc = jHelpers.getRtcOfSessionUser(jClient, su);
			    if(rtc==null) {
			    	//log.info(su.getUsername()+" has no rtc");
			    	continue;
			    }
			    log.trace("getting status for "+su.log());
			    Long sessionId = jClient.getSession_id();
			    if(jAdminClient != null && jAdminClient.isInitialized()) 
			    	jAdminControl.status(rtc, sessionId);
			}
		}catch(Exception e) {
			log.error("Exception running statusTask "+e);
		}
	}

	
}
