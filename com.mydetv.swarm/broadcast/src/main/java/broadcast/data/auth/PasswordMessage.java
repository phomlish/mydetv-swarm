package broadcast.data.auth;

import java.util.List;

import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;

//https://github.com/nulab/zxcvbn4j
public class PasswordMessage {
	private Integer score;
	private String feedback;
	private List<String> suggestions;
	public PasswordMessage(Integer score, String feedback, List<String> suggestions) {
		this.score=score;
		this.feedback=feedback;
		this.setSuggestions(suggestions);
	}
	
	public static PasswordMessage GetPasswordStrength(String password) {
		Zxcvbn zxcvbn = new Zxcvbn();
		Strength strength = zxcvbn.measure(password);
		PasswordMessage passwordMessage = new PasswordMessage(strength.getScore(), strength.getFeedback().getWarning(), strength.getFeedback().getSuggestions());
		//log.info("passwd:"+password+",strength:"+strength.getScore()+" "+strength.getFeedback().getWarning()+":"+strength.getFeedback().getSuggestions());
		return passwordMessage;
	}
	
	public static PasswordMessage GetPasswordStrength(String password, List<String> sanitizedInputs) {
		Zxcvbn zxcvbn = new Zxcvbn();
		Strength strength = zxcvbn.measure(password, sanitizedInputs);
		PasswordMessage passwordMessage = new PasswordMessage(strength.getScore(), strength.getFeedback().getWarning(), strength.getFeedback().getSuggestions());
		//log.info("passwd:"+password+",strength:"+strength.getScore()+" "+strength.getFeedback().getWarning()+":"+strength.getFeedback().getSuggestions());
		return passwordMessage;
	}
	
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public List<String> getSuggestions() {
		return suggestions;
	}
	public void setSuggestions(List<String> suggestions) {
		this.suggestions = suggestions;
	}
}
