package broadcast.data.status;

import java.util.ArrayList;
import java.util.List;

import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;


public class SCG {

	private String provider;
	private List<SRtcConnection> upstreams;
	private List<SRtcConnection> downstreams;
	private Integer tier;
	
	public SCG(ConnectionGroup cg) {
		if(cg==null)
			return;
		this.provider=cg.getProvider().log();
		this.tier=cg.getTier();
		
		this.upstreams = new ArrayList<SRtcConnection>();
		for(RtcConnection rtc:cg.getUpstreams())
			this.upstreams.add(new SRtcConnection(rtc));
		this.downstreams =  new ArrayList<SRtcConnection>();
		for(RtcConnection rtc : cg.getDownstreams())
			this.downstreams.add(new SRtcConnection(rtc));
	}
	
	// getters/setters
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public Integer getTier() {
		return tier;
	}
	public void setTier(Integer tier) {
		this.tier = tier;
	}

	public List<SRtcConnection> getUpstreams() {
		return upstreams;
	}

	public void setUpstreams(List<SRtcConnection> upstreams) {
		this.upstreams = upstreams;
	}

	public List<SRtcConnection> getDownstreams() {
		return downstreams;
	}

	public void setDownstreams(List<SRtcConnection> downstreams) {
		this.downstreams = downstreams;
	}
}
