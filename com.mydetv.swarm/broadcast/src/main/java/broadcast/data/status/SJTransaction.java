package broadcast.data.status;

import broadcast.janus.JTransaction;

public class SJTransaction {

	private String session;
	private String tid;
	private String sent;
	private SRtcConnection rtc;
	private Long dtStarted;
	private Long dtProgress;
	
	public SJTransaction(JTransaction jt) {
		if(jt==null)
			return;
		if(jt.getSession()!=null)
			this.session=jt.getSession();
		this.tid=jt.getTid();
		this.sent=jt.getSent().toString();
		if(jt.getRtc()!=null)
			this.rtc = new SRtcConnection(jt.getRtc());
		this.dtStarted=jt.getDtStarted();
		this.dtProgress=jt.getDtProgress();
	}
	
	// getters/setters
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getSent() {
		return sent;
	}
	public void setSent(String sent) {
		this.sent = sent;
	}
	public SRtcConnection getRtc() {
		return rtc;
	}
	public void setRtc(SRtcConnection rtc) {
		this.rtc = rtc;
	}
	public Long getDtStarted() {
		return dtStarted;
	}
	public void setDtStarted(Long dtStarted) {
		this.dtStarted = dtStarted;
	}
	public Long getDtProgress() {
		return dtProgress;
	}
	public void setDtProgress(Long dtProgress) {
		this.dtProgress = dtProgress;
	}
	
}
