package broadcast.data.status;

import java.util.TreeMap;

public class SChannelViewer {
	private String username;
	private Integer tier;
	private TreeMap<String, SRtcConnection> myBandwidthTestsToMe;
	private TreeMap<String, SRtcConnection> myBandwidthTestsFromMe;
	private SCG myCG;
	
	//getters/setters
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getTier() {
		return tier;
	}
	public void setTier(Integer tier) {
		this.tier = tier;
	}
	
	public TreeMap<String, SRtcConnection> getMyBandwidthTestsToMe() {
		return myBandwidthTestsToMe;
	}
	public void setMyBandwidthTestsToMe(TreeMap<String, SRtcConnection> myBandwidthTestsToMe) {
		this.myBandwidthTestsToMe = myBandwidthTestsToMe;
	}
	public TreeMap<String, SRtcConnection> getMyBandwidthTestsFromMe() {
		return myBandwidthTestsFromMe;
	}
	public void setMyBandwidthTestsFromMe(TreeMap<String, SRtcConnection> myBandwidthTestsFromMe) {
		this.myBandwidthTestsFromMe = myBandwidthTestsFromMe;
	}
	public SCG getMyCG() {
		return myCG;
	}
	public void setMyCG(SCG myCG) {
		this.myCG = myCG;
	}
}
