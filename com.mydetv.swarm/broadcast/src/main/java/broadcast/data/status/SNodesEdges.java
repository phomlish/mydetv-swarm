package broadcast.data.status;

import java.util.ArrayList;
import java.util.List;

/**
 * Nodes: the entity<br>
 * Edges: the connections between the nodes<br>
 * @author phomlish
 *
 */
public class SNodesEdges {
	
	private List<SNode> nodes = new ArrayList<SNode>();
	private List<SEdge> edges = new ArrayList<SEdge>();
	private List<SGroup> groups = new ArrayList<SGroup>();
	private Integer nodeCounter;
	
	// custom
	public void incrementNodeCounter() {
		nodeCounter++;
	}
	// getters/setters
	public List<SNode> getNodes() {
		return nodes;
	}
	public void setNodes(List<SNode> nodes) {
		this.nodes = nodes;
	}
	public List<SEdge> getEdges() {
		return edges;
	}
	public void setEdges(List<SEdge> edges) {
		this.edges = edges;
	}
	public Integer getNodeCounter() {
		return nodeCounter;
	}
	public void setNodeCounter(Integer nodeCounter) {
		this.nodeCounter = nodeCounter;
	}
	public List<SGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<SGroup> groups) {
		this.groups = groups;
	}
}
