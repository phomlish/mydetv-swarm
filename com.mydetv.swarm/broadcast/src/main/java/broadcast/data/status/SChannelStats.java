package broadcast.data.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import broadcast.janus.JClient;
import broadcast.janus.JTransaction;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class SChannelStats {

	private Integer channelId;
	private String name;
	private boolean isActive;
	private Integer channelType;
	private Integer transactionsCount;
	private List<SJTransaction> transactions;
	private Integer handlesCount;
	private List<SRtcConnection> handles;
	private List<SUserShort> users = new ArrayList<SUserShort>();
	private Integer downstreams;
	private Integer viewers;
	private Long sessionId;
	private SCG scgStream;
	private SCG scgBroadcaster;
	
	public SChannelStats() {
		
	}
	public SChannelStats(OneMydetvChannel mc) {
		if(mc==null)
			return;
		this.channelId=mc.getChannelId();
		this.name=mc.getName();
		this.isActive=mc.isActive();
		this.channelType=mc.getChannelType();
		JClient jClient = (JClient) mc.getOjClient().getjClient();
		this.sessionId = jClient.getSession_id();
		HashMap<String, JTransaction> transactions = new HashMap<String, JTransaction>(jClient.getTransactions());
		this.transactionsCount=transactions.size();
		ArrayList<SJTransaction> t = new ArrayList<SJTransaction>();
		for(Entry<String, JTransaction> entry : transactions.entrySet()) {		
			t.add(new SJTransaction(entry.getValue()));
		}
		this.setTransactions(t);
		
		HashMap<Long, RtcConnection> handles = new HashMap<Long, RtcConnection>(jClient.getHandles());
		this.handlesCount=handles.size();
		ArrayList<SRtcConnection> h = new ArrayList<SRtcConnection>();
		for(Entry<Long, RtcConnection> entry : handles.entrySet()) {		
			h.add(new SRtcConnection(entry.getValue()));
		}
		this.setHandles(h);
		
		for(Entry<String, SessionUser> entry : mc.getSessionUsers().entrySet()) {
			this.users.add(new SUserShort(entry.getValue()));
		}
		this.downstreams = jClient.getMe().getCgStream().getDownstreams().size();
		this.viewers=addDownstreams(0, jClient.getMe().getCgStream());
		
		this.scgStream=new SCG(jClient.getMe().getCgStream());
		this.scgBroadcaster=new SCG(jClient.getMe().getCgBroadcaster());
	}
	
	public Integer addDownstreams(Integer count, ConnectionGroup cg) {
		for(RtcConnection rtc : cg.getDownstreams()) {
			count++;
			SessionUser toUser = rtc.getToUser();
			count = addDownstreams(count, toUser.getCgStream());
		}
		return count;		
	}
	
	// getters/setters
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public Integer getChannelType() {
		return channelType;
	}
	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}
	public Integer getTransactionsCount() {
		return transactionsCount;
	}
	public void setTransactionsCount(Integer transactionsCount) {
		this.transactionsCount = transactionsCount;
	}
	public Integer getHandlesCount() {
		return handlesCount;
	}
	public void setHandlesCount(Integer handlesCount) {
		this.handlesCount = handlesCount;
	}

	public List<SJTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<SJTransaction> transactions) {
		this.transactions = transactions;
	}

	public List<SRtcConnection> getHandles() {
		return handles;
	}

	public void setHandles(List<SRtcConnection> handles) {
		this.handles = handles;
	}

	public Integer getViewers() {
		return viewers;
	}

	public void setViewers(Integer viewers) {
		this.viewers = viewers;
	}
	public Long getSessionId() {
		return sessionId;
	}
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}
	public List<SUserShort> getUsers() {
		return this.users;
	}
	public Integer getDownstreams() {
		return downstreams;
	}
	public void setDownstreams(Integer downstreams) {
		this.downstreams = downstreams;
	}
	public SCG getScgStream() {
		return scgStream;
	}
	public void setScgStream(SCG scgStream) {
		this.scgStream = scgStream;
	}
	public SCG getScgBroadcaster() {
		return scgBroadcaster;
	}
	public void setScgBroadcaster(SCG scgBroadcaster) {
		this.scgBroadcaster = scgBroadcaster;
	}
}
