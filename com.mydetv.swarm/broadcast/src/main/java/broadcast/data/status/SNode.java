package broadcast.data.status;

// Nodes: the entity
public class SNode {
	public SNode(Integer id, String label, String group) {
		this.id=id;
		this.label=label;
		this.setColor("aqua");
		this.group=group;
	}
	
	private Integer id;
	private String label;
	private String color;
	private String group;
	
	// getters/setters
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}  	
}
