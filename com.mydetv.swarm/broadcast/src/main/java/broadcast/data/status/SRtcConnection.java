package broadcast.data.status;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.TreeMap;

import shared.data.broadcast.RtcConnection;

public class SRtcConnection {

	private String cid;
	private String created;
	private String started;
	private String toUser;
	private String fromUser;
	private String connectiontype;
	private Boolean connected;
	private String failed;
	private Boolean removed;
	private Boolean errored;
	private TreeMap<LocalDateTime, String> states;
	private Long handleId;
	private Long privateId;
	
	public SRtcConnection(RtcConnection rtc) {
		if(rtc==null)
			return;
		this.cid=rtc.getRtcUUID();
		this.setCreated(rtc.getCreated().toString());
		if(rtc.getStarted()!=null)
			this.setStarted(rtc.getStarted().toString());
		if(rtc.getToUser()!=null && rtc.getToUser().getWebUser()!=null)
			this.toUser=rtc.getToUser().log();
		if(rtc.getFromUser()!=null&&rtc.getFromUser().getWebUser()!=null)
			this.fromUser=rtc.getFromUser().log();
		this.connectiontype=rtc.getConnectionType();
		this.connected=rtc.getConnected();
		if(rtc.getFailed()!=null)
			this.failed=rtc.getFailed().toString();
		this.setRemoved(rtc.getRemoved());
		this.errored=rtc.getErrored();
		this.states=rtc.getStates();
		this.handleId=rtc.getHandleId();
		this.privateId=rtc.getPrivateId();
	}
	// getters/setters
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getConnectiontype() {
		return connectiontype;
	}
	public void setConnectiontype(String connectiontype) {
		this.connectiontype = connectiontype;
	}
	public Long getHandleId() {
		return handleId;
	}
	public void setHandleId(Long handleId) {
		this.handleId = handleId;
	}
	public Long getPrivateId() {
		return privateId;
	}
	public void setPrivateId(Long privateId) {
		this.privateId = privateId;
	}
	public TreeMap<LocalDateTime, String> getStates() {
		return states;
	}
	public void setStates(TreeMap<LocalDateTime, String> states) {
		this.states = states;
	}
	public Boolean getConnected() {
		return connected;
	}
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}
	public String getFailed() {
		return failed;
	}
	public void setFailed(String failed) {
		this.failed = failed;
	}
	public String getStarted() {
		return started;
	}
	public void setStarted(String started) {
		this.started = started;
	}
	public Boolean getErrored() {
		return errored;
	}
	public void setErrored(Boolean errored) {
		this.errored = errored;
	}
}
