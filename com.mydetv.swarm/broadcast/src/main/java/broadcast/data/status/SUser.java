package broadcast.data.status;

import shared.data.broadcast.SessionUser;

public class SUser {

	private String username;
	private String lastSeen;
	private String ip;
	private Boolean auth;
	private Integer dbWebUserId;
	private Integer dbChannelWebUsersId;
	private String localVideo;
	private SMydetvChannel sMydetvChannel;
	private SWSConnection sWSConnection;
	private SCG scgStream;
	private SCG scgBroadcaster;
	private SCG scgU2U;
	private Long datafileDownloadTime;
	// creator
	public SUser(SessionUser su) {
		if(su==null)
			return;
		this.lastSeen=su.getLastSeen().toString();
		this.ip=su.getIpPort();
		if(su.getWebUser()==null)
			return;
		this.username=su.log();
		this.auth=su.isAuthenticated();
		this.dbWebUserId=su.getWebUser().getWuid();
		if(su.getLocalVideo()!=null)	
			this.setLocalVideo(su.getLocalVideo().toString());
		if(su.getWebUser()!=null)
			this.dbChannelWebUsersId=su.getWebUser().getDbChannelWebUsersId();
		this.scgStream=new SCG(su.getCgStream());
		this.scgBroadcaster=new SCG(su.getCgBroadcaster());
		this.scgU2U = new SCG(su.getCgU2U());
		if(su.getMydetvChannel() != null)
			this.sMydetvChannel=new SMydetvChannel(su.getMydetvChannel());
		if(su.getWebSocketConnection()!=null)
			this.sWSConnection=new SWSConnection(su.getWebSocketConnection());
		this.datafileDownloadTime=su.getDatafileDownloadTime();			
	}	
	// getters/setters
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean getAuth() {
		return auth;
	}
	public void setAuth(Boolean auth) {
		this.auth = auth;
	}
	public Integer getDbWebUserId() {
		return dbWebUserId;
	}
	public void setDbWebUserId(Integer dbWebUserId) {
		this.dbWebUserId = dbWebUserId;
	}
	public Integer getDbChannelWebUsersId() {
		return dbChannelWebUsersId;
	}
	public void setDbChannelWebUsersId(Integer dbChannelWebUsersId) {
		this.dbChannelWebUsersId = dbChannelWebUsersId;
	}

	public SMydetvChannel getsMydetvChannel() {
		return sMydetvChannel;
	}
	public void setsMydetvChannel(SMydetvChannel sMydetvChannel) {
		this.sMydetvChannel = sMydetvChannel;
	}
	public SWSConnection getsWSConnection() {
		return sWSConnection;
	}
	public void setsWSConnection(SWSConnection sWSConnection) {
		this.sWSConnection = sWSConnection;
	}
	public SCG getScgStream() {
		return scgStream;
	}
	public void setScgStream(SCG scg) {
		this.scgStream = scg;
	}
	public Long getDatafileDownloadTime() {
		return datafileDownloadTime;
	}
	public void setDatafileDownloadTime(Long datafileDownloadTime) {
		this.datafileDownloadTime = datafileDownloadTime;
	}
	public String getLastSeen() {
		return lastSeen;
	}
	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}
	public SCG getScgBroadcaster() {
		return scgBroadcaster;
	}
	public void setScgBroadcaster(SCG scgBroadcaster) {
		this.scgBroadcaster = scgBroadcaster;
	}
	public SCG getScgU2U() {
		return scgU2U;
	}
	public void setScgU2U(SCG scgU2U) {
		this.scgU2U = scgU2U;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getLocalVideo() {
		return localVideo;
	}
	public void setLocalVideo(String localVideo) {
		this.localVideo = localVideo;
	}
}
