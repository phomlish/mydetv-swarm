package broadcast.data.status;

import org.eclipse.jetty.websocket.common.WebSocketSession;

import shared.data.broadcast.WebSocketConnection;

public class SWSConnection {

	private Boolean open;
	private String localAddress;
	private String remoteAddress;
	private String lastPing;
	private String connectedDate;
	
	public SWSConnection(WebSocketConnection wsc) {
		WebSocketSession wss = (WebSocketSession) wsc.getWebsocketSession();
		this.setConnectedDate(wsc.getConnectedDate().toString());
	    this.setLastPing(wsc.getLastPing().toString());

		this.setLocalAddress(wss.getLocalAddress().getAddress().getHostAddress()
			+":"
			+wss.getLocalAddress().getPort());
		this.setRemoteAddress(wss.getRemoteAddress().getAddress().getHostAddress()
			+":"
			+wss.getRemoteAddress().getPort());
		this.setOpen(wss.isOpen());
	}
	
	// getters/setters
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public String getRemoteAddress() {
		return remoteAddress;
	}
	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
	public String getLastPing() {
		return lastPing;
	}
	public void setLastPing(String lastPing) {
		this.lastPing = lastPing;
	}
	public String getConnectedDate() {
		return connectedDate;
	}
	public void setConnectedDate(String connectedDate) {
		this.connectedDate = connectedDate;
	}
	
	
}
