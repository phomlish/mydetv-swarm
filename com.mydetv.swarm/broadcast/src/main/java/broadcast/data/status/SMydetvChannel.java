package broadcast.data.status;

import shared.data.OneMydetvChannel;

public class SMydetvChannel {
	private Integer channelId;
	private String name;
	
	public SMydetvChannel(OneMydetvChannel mc) {
		if(mc==null)
			return;
		this.channelId=mc.getChannelId();
		this.name=mc.getName();
	}
	
	// getters/setters
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
