package broadcast.data.status;

//Edges: the connections between the nodes
public class SEdge {

	public SEdge(Integer from, Integer to) {
    	this.from=from;
    	this.to=to;
	}
	private Integer from;
	private Integer to;
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getTo() {
		return to;
	}
	public void setTo(Integer to) {
		this.to = to;
	}
	
}
