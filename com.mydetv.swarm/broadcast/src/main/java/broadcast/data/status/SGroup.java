package broadcast.data.status;

public class SGroup {

	private String name;
	private String shape;
	private String color;
	public SGroup(String name, String shape, String color) {
		this.name=name;
		this.shape=shape;
		this.color=color;
	}
	public String getShape() {
		return shape;
	}
	public void setShape(String shape) {
		this.shape = shape;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
