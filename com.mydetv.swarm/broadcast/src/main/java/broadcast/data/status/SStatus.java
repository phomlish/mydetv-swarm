package broadcast.data.status;

import java.util.TreeMap;

public class SStatus {
	private String version;
	private TreeMap<Integer, SMydetvChannel> mydetvChannels = new TreeMap<Integer, SMydetvChannel>();
	private String bootTime;
	private String uptime;
	private String memoryMaximum;
	private String memoryAllocated;
	private String memoryFree;
	private String memoryUsed;
	private Integer threads = 0;
	private Integer dbPoolIdle;
	private Integer dbPoolActive;
	private Integer usersCount=0;
	private Integer usersInChannelCount;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public TreeMap<Integer, SMydetvChannel> getMydetvChannels() {
		return mydetvChannels;
	}
	public void setMyDetvChannels(TreeMap<Integer, SMydetvChannel> mydetvChannels) {
		this.mydetvChannels = mydetvChannels;
	}
	public String getBootTime() {
		return bootTime;
	}
	public void setBootTime(String bootTime) {
		this.bootTime = bootTime;
	}
	public String getUptime() {
		return uptime;
	}
	public void setUptime(String uptime) {
		this.uptime = uptime;
	}
	public String getMemoryMaximum() {
		return memoryMaximum;
	}
	public void setMemoryMaximum(String memoryMaximum) {
		this.memoryMaximum = memoryMaximum;
	}
	public String getMemoryAllocated() {
		return memoryAllocated;
	}
	public void setMemoryAllocated(String memoryAllocated) {
		this.memoryAllocated = memoryAllocated;
	}
	public String getMemoryFree() {
		return memoryFree;
	}
	public void setMemoryFree(String memoryFree) {
		this.memoryFree = memoryFree;
	}
	public String getMemoryUsed() {
		return memoryUsed;
	}
	public void setMemoryUsed(String memoryUsed) {
		this.memoryUsed = memoryUsed;
	}
	public Integer getThreads() {
		return threads;
	}
	public void setThreads(Integer threads) {
		this.threads = threads;
	}
	public Integer getUsersCount() {
		return usersCount;
	}
	public void setUsersCount(Integer usersCount) {
		this.usersCount = usersCount;
	}
	public Integer getUsersInChannelCount() {
		return usersInChannelCount;
	}
	public void setUsersInChannelCount(Integer usersInChannelCount) {
		this.usersInChannelCount = usersInChannelCount;
	}
	public Integer getDbPoolIdle() {
		return dbPoolIdle;
	}
	public void setDbPoolIdle(Integer dbPoolIdle) {
		this.dbPoolIdle = dbPoolIdle;
	}
	public Integer getDbPoolActive() {
		return dbPoolActive;
	}
	public void setDbPoolActive(Integer dbPoolActive) {
		this.dbPoolActive = dbPoolActive;
	}
}
