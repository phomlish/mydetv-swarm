package broadcast.data.status;

import shared.data.broadcast.SessionUser;

public class SUserShort {

	private String username;
	private Boolean auth;
	private Integer dbWebUserId;
	private Integer dbChannelWebUsersId;
	//private ArrayList<UserRoleType> userRoles;
	private Long datafileDownloadTime;
	private String lastSeen;
	
	public SUserShort(SessionUser su) {
		if(su==null)
			return;
		this.username=su.log();
		this.auth=su.isAuthenticated();
		this.dbWebUserId=su.getWebUser().getWuid();
		if(su.getWebUser()!=null)
			this.dbChannelWebUsersId=su.getWebUser().getDbChannelWebUsersId();
		this.datafileDownloadTime=su.getDatafileDownloadTime();	
		this.lastSeen=su.getLastSeen().toString();
	}
	
	// getters/setters
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean getAuth() {
		return auth;
	}
	public void setAuth(Boolean auth) {
		this.auth = auth;
	}
	public Integer getDbWebUserId() {
		return dbWebUserId;
	}
	public void setDbWebUserId(Integer dbWebUserId) {
		this.dbWebUserId = dbWebUserId;
	}
	public Integer getDbChannelWebUsersId() {
		return dbChannelWebUsersId;
	}
	public void setDbChannelWebUsersId(Integer dbChannelWebUsersId) {
		this.dbChannelWebUsersId = dbChannelWebUsersId;
	}
	public Long getDatafileDownloadTime() {
		return datafileDownloadTime;
	}
	public void setDatafileDownloadTime(Long datafileDownloadTime) {
		this.datafileDownloadTime = datafileDownloadTime;
	}

	public String getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}
}
