package broadcast.data;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import shared.data.OneSchedule;
import shared.data.OneVideo;

public class FpnSeek {

	private String fpn;
	private String strSeek;
	private Integer seekSeconds;
	private OneVideo ov;
	private OneSchedule os;
	private LocalDateTime actualStartime;
	// creator
	public FpnSeek(String fpn, Integer seekSeconds, OneVideo ov, OneSchedule os) {
		this.fpn=fpn;
		this.setSeekSeconds(seekSeconds);
		LocalTime timeOfSeek = LocalTime.ofSecondOfDay(seekSeconds);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.US);
		this.strSeek=timeOfSeek.format(dtf);
		this.ov=ov;
		this.os=os;
	}
	// getters/setters
	public String getFpn() {
		return fpn;
	}
	public void setFpn(String fpn) {
		this.fpn = fpn;
	}
	public OneVideo getOv() {
		return ov;
	}
	public void setOv(OneVideo ov) {
		this.ov = ov;
	}
	public OneSchedule getOs() {
		return os;
	}
	public void setOs(OneSchedule os) {
		this.os = os;
	}
	public LocalDateTime getActualStartime() {
		return actualStartime;
	}
	public void setActualStartime(LocalDateTime actualStartime) {
		this.actualStartime = actualStartime;
	}
	public String getStrSeek() {
		return strSeek;
	}
	public void setStrSeek(String strSeek) {
		this.strSeek = strSeek;
	}
	public Integer getSeekSeconds() {
		return seekSeconds;
	}
	public void setSeekSeconds(Integer seekSeconds) {
		this.seekSeconds = seekSeconds;
	}
}
