package broadcast.data.web;

import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;

public class WVideo extends OneVideo {

	// private Integer vcsId;  // not needed this comes right from the database and is part of OneVideo
	private String vcsName;
	
	private Integer vcmId;
	private String vcmName;
	
	private String pn;	
	private String pn2;
	private String strDoNotUse;
	
	
	public WVideo (OneVideo ov) {
		setPkey(ov.getPkey());
		setFn(ov.getFn());
		setSubdir(ov.getSubdir());
		setTitle(ov.getTitle());
		setCatSub(ov.getCatSub());
		setDtAdded(ov.getDtAdded());
		setLength(ov.getLength());
		setFilesize(ov.getFilesize());
		setNotes(ov.getNotes());
		setThumbnail(ov.getThumbnail());
		setDtInactive(ov.getDtInactive());
		setDoNotUse(ov.isDoNotUse());
	}
	// custom
	public void setVCM(VideoCategoryMain vcm) {
		this.vcmId=vcm.getIdVCM();
		this.vcmName=vcm.getName();
	}
	public void setVCS(VideoCategorySub vcs) {
		this.vcsName=vcs.getName();
	}
	// getters/setters

	public Integer getVcmId() {
		return vcmId;
	}
	public void setVcmId(Integer vcmId) {
		this.vcmId = vcmId;
	}
	public String getPn() {
		return pn;
	}
	public void setPn(String pn) {
		this.pn = pn;
	}
	public String getPn2() {
		return pn2;
	}
	public void setPn2(String pn2) {
		this.pn2 = pn2;
	}
	public String getStrDoNotUse() {
		return strDoNotUse;
	}
	public void setStrDoNotUse(String strDoNotUse) {
		this.strDoNotUse = strDoNotUse;
	}
	public String getVcsName() {
		return vcsName;
	}
	public void setVcsName(String vcsName) {
		this.vcsName = vcsName;
	}
	public String getVcmName() {
		return vcmName;
	}
	public void setVcmName(String vcmName) {
		this.vcmName = vcmName;
	}
}
