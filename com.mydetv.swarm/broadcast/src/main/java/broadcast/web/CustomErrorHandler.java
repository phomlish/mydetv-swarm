package broadcast.web;

import java.io.IOException;
import java.io.Writer;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.server.handler.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;

public class CustomErrorHandler extends ErrorHandler {
	
	private static final Logger log = LoggerFactory.getLogger(CustomErrorHandler.class);
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	
	@Override
    protected void writeErrorPage(HttpServletRequest req, Writer writer, int code, String message, boolean showStacks) throws IOException {
        	
		/*
		Enumeration<String> headers = req.getHeaderNames();
		while(headers.hasMoreElements()) {
			log.info("header: "+headers.nextElement());
		}
		*/
		log.info("error " +code+" referer:"+req.getRequestURI()+" "+req.getRemoteAddr()+":"+req.getRemotePort());
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		String rv = webHelper.beginHtml(sessionUser, "Error");
		rv+="<p />";
				
		rv+=addDetails(req, code, message);
		
		rv+="</div>\n";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		writer.write(rv);
	}
	

	private String addDetails(HttpServletRequest req, int code, String message) {
		String rv ="<center>";
		rv+="<b>A "+code+" error occured accessing</b> "+req.getRequestURI()+"<br><br>";
		rv+="<b>"+message+"</b><br><br>";
		
		switch(code) {
		case 403: // 
			rv+="You have not been trusted to access that functionality.<br>\n";
			rv+="If you believe this is in error please contact an administrator.<br>";
			rv+="<br><img src='/s/img/unauth.jpeg' style='max-width: 100px; height: auto;'>";
			break;
		case 404: // not found
			rv+="<br><img src='/s/img/moe.jpg' style='max-width: 100px; height: auto;'>";
			break;
		case 405: // method not allowed
			rv+=req.getMethod()+"<br>";
			rv+="<br><img src='/s/img/curly.jpg' style='max-width: 100px; height: auto;'>";
			break;
		case 408: // timeout
			rv+="<br><img src='/s/img/curly2.jpg' style='max-width: 100px; height: auto;'>";
			break;
		case 418: // teapot
			rv+="<br><img src='/s/img/larry.jpg' style='max-width: 100px; height: auto;'>";
			break;
		case 500: // internal server error
			rv+="<br><img src='/s/img/curly.jpg' style='max-width: 100px; height: auto;'>";
			break;
		default:
			rv+="<br><img src='/s/img/curly3.jpg' style='max-width: 100px; height: auto;'>";
		}

		/* still available
		 rv+="<br><img src='/s/img/curly3.jpg' style='max-width: 100px; height: auto;'>";
		 rv+="<br><img src='/s/img/larry2.jpg' style='max-width: 100px; height: auto;'>";
		 rv+="<br><img src='/s/img/.jpg' style='max-width: 100px; height: auto;'>";
		 rv+="<br><img src='/s/img/moe2.jpg' style='max-width: 100px; height: auto;'>";
		 rv+="<br><img src='/s/img/moe3.jpg' style='max-width: 100px; height: auto;'>";
		 rv+="<br><img src='/s/img/moe4.jpg' style='max-width: 100px; height: auto;'>";
		 
		 
		 */
		rv+="</center>";
		return rv;
	}
	
}
