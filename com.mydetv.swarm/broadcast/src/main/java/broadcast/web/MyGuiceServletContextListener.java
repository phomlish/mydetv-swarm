package broadcast.web;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

import broadcast.Blackboard;

public class MyGuiceServletContextListener extends GuiceServletContextListener {

	@Inject private Blackboard blackboard;
	
	@Override
	protected Injector getInjector() {
		return blackboard.getMainInjector().createChildInjector();	
	}
}
