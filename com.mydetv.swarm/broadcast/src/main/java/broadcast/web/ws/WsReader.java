package broadcast.web.ws;

import java.time.LocalDateTime;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.broadcaster.WsBroadcasterReader;
import broadcast.business.ChatHelpers;
import broadcast.business.LocalVideoHelpers;
import broadcast.business.PeopleHelpers;
import broadcast.p2p.BwHelpers;
import broadcast.p2p.ConnectionSmarts;
import broadcast.p2p.IceFailed;
import broadcast.p2p.NegotiationNeeded;
import broadcast.p2p.SessionUserInit;
import broadcast.p2p.P2PHelpers;
import broadcast.p2p.VideoU2U;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;

public class WsReader {

	@Inject private Blackboard blackboard;
	@Inject private ChatHelpers chatHelpers;
	@Inject private PeopleHelpers peopleHelpers;
	@Inject private P2PHelpers p2pHelpers;
	@Inject private VideoU2U videoU2U;
	@Inject private BwHelpers bandwidthHelpers;
	@Inject private LocalVideoHelpers localVideoHelpers;
	@Inject private ConnectionSmarts connectionSmarts;
	@Inject private IceFailed iceFailed;
	@Inject private NegotiationNeeded negotiationNeeded;
	@Inject private SessionUserInit sessionUserInitialize;
	@Inject private WsBroadcasterReader broadcaster;
	private static final Logger log = LoggerFactory.getLogger(WsReader.class);

	public void doMessage(Session session, String message) {

		String verb = "unknown";		
		JSONObject resp = new JSONObject(message);
		verb = resp.getString("verb");

		if(verb.equals("init")) {
			sessionUserInitialize.doInit(session, resp);
			return;
		}

		SessionUser su = authenticate(session);
		if(su == null) {
			log.error("ignoring "+verb+", can't associate this session with a sessionUser");
			return;
		}
		
		// this could be OK, perhaps he got lost and then we got his message
		if(su.getWebSocketConnection() == null) {
			log.error("ignoring, sessionUser has no wsConnection (might be OK if he was lost and then the message came in)");
			return;
		}
		
		// not really setting last ping, setting last time we saw anything
		su.getWebSocketConnection().setLastPing(LocalDateTime.now());
		su.setLastSeen(LocalDateTime.now());
		
		log.warn("synchronized "+su.getWebUser().getUsername()+" ("+verb+")");
		synchronized(su.getLock()) {
			processMessage(su, resp);
		}
		log.warn("unsynchronized "+su.getWebUser().getUsername()+" ("+verb+")");
	}
	private void processMessage(SessionUser su, JSONObject response) {
		String verb = response.getString("verb");
		if(!verb.contains("pong"))
			log.info("incoming message '"+verb+"' from "+su.log());

		try {

			switch(verb) {
			case "pong":
				break;
			case "sendChat":
				chatHelpers.wsSendChat(response, su);
				break;
			case "chatBlock":
				chatHelpers.chatBlock(response, su);
				break;
			case "personInfo":
				peopleHelpers.personInfo(response, su);
				break;
			case "reportPerson":
				peopleHelpers.reportPerson(response, su);
				break;
			case "negotiationNeeded":
				negotiationNeeded.wsMessageNegotiationNeeded(response, su);				
				break;
			case "iceCandidate":
				connectionSmarts.processIceMessage(response, su);
				break;
			case "iceCandidatesDone":
				connectionSmarts.processIceMessage(response, su);
				break;
			case "offer":
				connectionSmarts.processIceMessage(response, su);
				break;
			case "answer":
				connectionSmarts.processIceMessage(response, su);
				break;
			case "iceFailed":
				iceFailed.wsIceFailed(response, su);
				break;
			case "bandwidthTestSenderReady":
				bandwidthHelpers.wsMessageBandwidthTestSenderReady(response, su);
				break;
			case "bandwidthTestStart":
				bandwidthHelpers.wsMessageBandwidthTestStart(response, su);
				break;
			case "bandwidthTestFinished":
				bandwidthHelpers.wsMessageBandwidthTestFinished(response, su);
				break;
			case "videoConnected":
				p2pHelpers.wsMessageVideoConnected(response, su);
				break;
			//case "viewer":
			//	viewer.wsMessageViewer(response, sessionUser);
			//	break;
			case "broadcaster":
				broadcaster.wsMessageBroadcaster(response, su);
				break;
			case "u2u":
				videoU2U.u2u(response, su);
				break;
			case "localVideoStarted":
				localVideoHelpers.localVideoStarted(su, response);
				break;
			case "localVideoStopped":
				localVideoHelpers.localVideoStopped(su, response);
				break;
			default:
				log.error("doMessage: what are you trying to say?"+response);
				break;
			}
		} catch(Exception e) {
			log.error("malformed message, clean your act up ("+verb+")! "+e);
		}
		if(! verb.contains("pong")) log.debug("ws done "+verb);
	}

	private SessionUser authenticate(Session session) {
		Integer hashcode = Integer.valueOf(session.hashCode());
		log.debug("looking for "+hashcode+" in blackboard webSocketConnections");
		SessionUser sessionUser = null;
		if(!blackboard.getWebSocketConnections().containsKey(hashcode)) {
			log.error("I should have your wsConnection in blackboard by now");
			return null;
		}
		WebSocketConnection wsConnection = blackboard.getWebSocketConnections().get(hashcode);
		sessionUser = wsConnection.getSessionUser();

		if(sessionUser == null) {
			log.error("I should have a sessionUser by now");
		}
		return sessionUser;		
	}

}
