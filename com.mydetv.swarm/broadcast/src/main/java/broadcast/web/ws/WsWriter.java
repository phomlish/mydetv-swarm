package broadcast.web.ws;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class WsWriter {

	private @Inject Blackboard blackboard;
	@Inject private Config config;
	private static final Logger log = LoggerFactory.getLogger(WsWriter.class);
	
	// send a string that will appear in navbar statusMessage div	
	public void sendStatusMessage(SessionUser sessionUser, String message) {
		JSONObject myMessage = new JSONObject();
		myMessage.put("verb", "userMessage");
		myMessage.put("userMessage", message);
		sendMessage(sessionUser, myMessage);
	}
	
	// these are special messages we send when a user is not initialized correctly
	public void sendStatusMessage(Session session, String message) {
		JSONObject myMessage = new JSONObject();
		myMessage.put("verb", "userMessage");
		myMessage.put("mydetv", "00000000-0000-0000-000000000000");
		myMessage.put("userMessage", "<div id='sMessage'>"+message+"</div>");
		log.info("send msg "+myMessage.toString());
		session.getRemote().sendString(myMessage.toString(), new MyWsWriterCallback());
	}
	
	public void sendSetupRTCConnection(SessionUser su, RtcConnection rtc, String role) {
		JSONObject message = new JSONObject()
			.put("verb", "setupRTCConnection")
			.put("cid", rtc.getRtcUUID())
			.put("connectionType", rtc.getConnectionType())
			.put("role", role)
		    .put("stuns", config.getStunServers())
		    ;
		sendMessage(su, message);
	}
	/**
	 * send setup U2U 
	 * @param su
	 * @param rtc
	 * @param role
	 * @param other
	 */
	public void sendSetupRTCConnection(SessionUser su, RtcConnection rtc, String role, String other) {
		//for(StunServer stunServer : config.getStunServers())
		//	log.debug("stun:"+stunServer.getNickname());
		JSONObject message = new JSONObject()
			.put("verb", "setupRTCConnection")
			.put("cid", rtc.getRtcUUID())
			.put("connectionType", rtc.getConnectionType())
			.put("role", role)
		    .put("stuns", config.getStunServers())
		    .put("personUid", other)
		    ;
		sendMessage(su, message);
	}
	public void sendAnswer(SessionUser sessionUser, String cid, JSONObject sdp) {
		JSONObject message = new JSONObject();
		message.put("verb", "mydetvCustom");
		message.put("cid", cid);
		message.put("command", "answer");
		message.put("sdp", sdp);		
		sendMessage(sessionUser, message);	
	}
	public void sendOffer(SessionUser sessionUser, String cid, JSONObject sdp) {
		JSONObject message = new JSONObject();
		message.put("verb", "mydetvCustom");
		message.put("cid", cid);
		message.put("command", "offer");
		message.put("sdp", sdp);		
		sendMessage(sessionUser, message);	
	}
	public void createOfferRestart(SessionUser sessionUser, String cid) {
		JSONObject message = new JSONObject();
		message.put("verb", "mydetvCustom");
		message.put("cid", cid);
		message.put("command", "offerRestart");	
		sendMessage(sessionUser, message);	
	}
	
	public void sendRemoveRTCConnection(SessionUser su, RtcConnection rtc) {
		JSONObject message = new JSONObject()
			.put("verb", "removeRTCConnection")
			.put("cid", rtc.getRtcUUID());
		sendMessage(su, message);
	}
	
	public void sendNewVideoTitle(OneMydetvChannel channel, String title) {
		try {
			for(SessionUser su : channel.getSessionUsers().values()) {		
				if(su.getMydetvChannel()==null || ! su.getMydetvChannel().equals(channel))
					continue;
				if(su.getWebSocketConnection()==null)
					continue;
				JSONObject message = new JSONObject()
					.put("verb", "newVideoTitle")
					.put("title", title);
				sendMessage(su, message);
			}
		} catch(Exception e) {
			log.error("exception "+e);
		}
	}
	
	public void sendMydetvCustom(SessionUser su, RtcConnection rtc, String command, String args) {
		JSONObject message = new JSONObject()
			.put("verb", "mydetvCustom")
			.put("cid", rtc.getRtcUUID())
			.put("command", command)
			.put("args", args);
		sendMessage(su, message);
	}

	/*
	 * send a message
	 *     will add mydetv and messageId counter
	 */	
	public void sendMessage(SessionUser sessionUser, JSONObject message) {
		try {
			if(sessionUser.getWebSocketConnection() == null) {
				log.error("can't send message "+message.toString()+" getWebSocketConnection is null: "+sessionUser.log());
				return;
			}
			if(sessionUser.getWebSocketConnection().getWebsocketSession() == null) {
				log.error("can't send message "+message.toString()+", webSocketSession is null "+sessionUser.log());
				return;
			}
			if(! ((Session) sessionUser.getWebSocketConnection().getWebsocketSession()).isOpen()) {
				log.error("can't send message "+message.toString()+", websocket is closed "+sessionUser.log());
				return;
			}
			org.eclipse.jetty.websocket.api.Session session = (Session) sessionUser.getWebSocketConnection().getWebsocketSession();

			message.put("mydetv", sessionUser.getMydetv());
			message.put("messageId", blackboard.getMessageId().toString());
			
			String verb = message.getString("verb");
			//log.info("verb:"+verb);
			if(!verb.equals("ping")
			&& !verb.equals("chatMessage"))
				log.info("Sending message to "+sessionUser.log()+":"+message);
			
			session.getRemote().sendString(message.toString(), new MyWsWriterCallback());
		} catch (Exception e) {
			log.error("could not send message to :"+sessionUser.log()+" "+message.toString()
				+"\n"+e);
		}	
	}
	
	// didn't seem to be needed in testing, when user went to ws page he was redirected to login
	public void sendLoggedOut(Session session) {
		JSONObject myMessage = new JSONObject();
		myMessage.put("verb", "loggedOut");
		session.getRemote().sendString(myMessage.toString(), new MyWsWriterCallback());
	}
}
