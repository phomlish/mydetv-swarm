package broadcast.web.ws;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class MyWsCreator implements WebSocketCreator {

	private static final Logger log = LoggerFactory.getLogger(MyWsCreator.class);
	@Inject private WsServer wsServer;

	public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse resp) {
		
	       for (String subprotocol : req.getSubProtocols())
	        {
	            if ("binary".equals(subprotocol))
	            {
	            	log.info("setup for binary protocol");
	                resp.setAcceptedSubProtocol(subprotocol);
	                //return binaryEcho;
	            }
	            if ("text".equals(subprotocol))
	            {
	            	log.info("setup for text protocol");
	                resp.setAcceptedSubProtocol(subprotocol);
	                //return textEcho;
	            }
	        }

		return wsServer;
	}

}
