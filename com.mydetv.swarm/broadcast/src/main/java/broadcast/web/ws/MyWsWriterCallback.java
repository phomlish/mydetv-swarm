package broadcast.web.ws;

import org.eclipse.jetty.websocket.api.WriteCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyWsWriterCallback implements WriteCallback {
	
	private static final Logger log = LoggerFactory.getLogger(MyWsWriterCallback.class);

	@Override
	public void writeFailed(Throwable x) {
		log.error("writeFailed");
		
	}

	@Override
	public void writeSuccess() {
		
	}

}
