package broadcast.web.ws;

import java.time.LocalDateTime;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.p2p.SessionUserClose;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;

@WebSocket
public class WsServer {

	private static final Logger log = LoggerFactory.getLogger(WsServer.class);
	@Inject private Blackboard blackboard;
	@Inject private WsReader webSocketReader;
	@Inject private WsWriter wsWriter;
	@Inject private SessionUserClose sessionUserClose;
	@Inject private UserHelper userHelper;

	@OnWebSocketConnect
	public void onConnect(Session session) {
		try {
			synchronized(session) {
				Integer hashcode = Integer.valueOf(session.hashCode());
				log.info("Connected "+session.getRemoteAddress().getAddress()+" "+session.getRemoteAddress().getPort()+" "+session.hashCode());
				
				if(blackboard.getWebSocketConnections().containsKey(hashcode)) {
					log.error("what are you doing with two connections in same browser? I'm closing your old connection");
					WebSocketConnection wsConnection = blackboard.getWebSocketConnections().get(hashcode);
					Session s = (Session) wsConnection.getWebsocketSession();
					s.close();
				}
	
				SessionUser su = userHelper.getSessionUser(session);
				if(su==null) {
					log.error("su must have timed out");
					wsWriter.sendLoggedOut(session);
				}
				
				WebSocketConnection wsConnection = new WebSocketConnection();
				wsConnection.setWebsocketSession(session);
				wsConnection.setHashcode(hashcode);
				wsConnection.setConnectedDate(LocalDateTime.now());
				wsConnection.setLastPing(LocalDateTime.now());
				blackboard.getWebSocketConnections().put(hashcode, wsConnection); 
				log.info("put "+hashcode+" into blackboard webSocketConnections");		
			}
		} catch(Exception e) {
			log.error("onConnect Exception e"+e);
		}
	}

	@OnWebSocketMessage
	public void onMessage(Session session, String message) {
		try {
			synchronized(session) {
				webSocketReader.doMessage(session, message);
			}
		} catch(Exception e) {
			log.error("onMessage exception e:"+e+" "+message);
		}
	}

	@OnWebSocketClose
	public void onClose(Session session, int statusCode, String reason) {
		try {
			synchronized(session) {
				log.info("session "+session.hashCode()+" closed code "+statusCode+" because "+reason);
				sessionUserClose.doClose(session);
			}
		} catch(Exception e) {
			log.error("onClose Exception "+e);
		}
	}

	@OnWebSocketError
	public void onError(Session session, Throwable t) {
		log.error("error "+ t.getMessage());
	}

}
