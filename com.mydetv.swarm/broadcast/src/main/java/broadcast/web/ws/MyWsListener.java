package broadcast.web.ws;

import java.time.LocalDateTime;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.broadcast.WebSocketConnection;

public class MyWsListener implements WebSocketListener {

	private static final Logger log = LoggerFactory.getLogger(MyWsListener.class);
	@Inject private Blackboard blackboard;
	
	@Override
	public void onWebSocketClose(int statusCode, String reason) {

	}

	@Override
	public void onWebSocketConnect(Session session) {
		synchronized(blackboard.getWebSocketConnections()) {
			Integer hashcode = Integer.valueOf(session.hashCode());
			log.info("Connected ... " 
					+ session.getRemoteAddress().getAddress() + " "
					+session.getRemoteAddress().getPort() + " "
					+session.hashCode());
			
			if(blackboard.getWebSocketConnections().containsKey(hashcode)) {
				log.error("what are you doing back here? I'm closing your old connection");
				WebSocketConnection wsConnection = blackboard.getWebSocketConnections().get(hashcode);
				
				Session s = (Session) wsConnection.getWebsocketSession();
				s.close();
			}

			WebSocketConnection wsConnection = new WebSocketConnection();
			wsConnection.setWebsocketSession(session);
			wsConnection.setHashcode(hashcode);
			wsConnection.setConnectedDate(LocalDateTime.now());
			wsConnection.setLastPing(LocalDateTime.now());
			//wsConnection.setBlackboard(blackboard);
			blackboard.getWebSocketConnections().put(hashcode, wsConnection); 
			log.info("put "+hashcode+" into blackboard webSocketConnections");
		}
	}

	@Override
	public void onWebSocketError(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onWebSocketBinary(byte[] payload, int offset, int len) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onWebSocketText(String message) {
		// TODO Auto-generated method stub
		
	}

}
