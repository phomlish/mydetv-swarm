package broadcast.web.ws;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WsHelpers {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(WsHelpers.class);
	
	//$('#my-input-id').prop('disabled', false);
	//$("#success").removeAttr("disabled");
	public static JSONObject UpdateELementProperty(String ename, String pname, String pvalue) {
		JSONObject jo = new JSONObject();
		jo.put("verb", "UpdateElementProperty");
		jo.put("ename", ename);
		jo.put("pname", pname);
		jo.put("pvalue", pvalue);
		return jo;
	}
	
	public static JSONObject UpdateELementValue(String ename, String evalue) {
		JSONObject jo = new JSONObject();
		jo.put("verb", "UpdateElementValue");
		jo.put("ename", ename);
		jo.put("evalue", evalue);
		return jo;
	}
	public static JSONObject UpdateELementAddClass(String ename, String classname) {
		JSONObject jo = new JSONObject();
		jo.put("verb", "UpdateELementAddClass");
		jo.put("ename", ename);
		jo.put("classname", classname);
		return jo;
	}
	public static JSONObject UpdateELementRemoveClass(String ename, String classname) {
		JSONObject jo = new JSONObject();
		jo.put("verb", "UpdateELementRemoveClass");
		jo.put("ename", ename);
		jo.put("classname", classname);
		return jo;
	}
	public static JSONObject UpdateELementAddRemoveClass(String ename, String aclassname, String rclassname) {
		JSONObject jo = new JSONObject();
		jo.put("verb", "UpdateELementAddRemoveClass");
		jo.put("ename", ename);
		jo.put("aclassname", aclassname);
		jo.put("rclassname", rclassname);
		return jo;
	}
	
	// need disabled
}
