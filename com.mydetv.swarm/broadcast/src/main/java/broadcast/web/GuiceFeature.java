package broadcast.web;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.ServiceLocatorProvider;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;

import com.google.inject.Inject;
import com.google.inject.Injector;

public class GuiceFeature implements Feature {

	@Inject ServiceLocator sl;
	private Injector i;
	public GuiceFeature(Injector i) {
		this.i = i;
	}
	
	//@Override
	public boolean configure(FeatureContext context) {

		ServiceLocator locator = ServiceLocatorProvider.getServiceLocator(context);
		
		GuiceBridge.getGuiceBridge().initializeGuiceBridge(locator);
        GuiceIntoHK2Bridge guiceBridge = locator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(i);
		
		return true;
	}
	
}
