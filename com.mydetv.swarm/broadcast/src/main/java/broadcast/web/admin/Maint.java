package broadcast.web.admin;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
/**
 * 
 * @author phomlish
 *
 */
@JsonAutoDetect
@Path("/maint")
public class Maint  {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Authorized authorized;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Maint.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser su = userHelper.getSessionUser(req);
		
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.hasAnyRole(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		
		String rv = webHelper.beginHtml(su, "Admin Maint");
		if(authorized.isScheduleAdmin(su.getWebUser()))
			rv+="<br><a href='/api/admin/maint/"+config.getApiKey()+"'>Refresh Blackboard</a>";
		
		LocalDateTime twentyMinutesEarlier = LocalDateTime.now().minusMinutes(20);
		if(authorized.isPeopleAdmin(su.getWebUser()) 
			&& blackboard.getSystemLoginLocked() != null
			&& blackboard.getSystemLoginLocked().isAfter(twentyMinutesEarlier)) {
			rv+="<br><a href='/api/admin/maint/unlockLogin'>Unlock System Login</a>";
		}
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rb.entity(rv);
		
		return rb.build();
	}

	

}
