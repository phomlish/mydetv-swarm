package broadcast.web.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.OneMydetvChannel;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/schedules")
public class Schedules {

	@Inject private Config sharedConfig;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Authorized authorized;
	@Inject private Blackboard blackboard;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Schedules.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser su = userHelper.getSessionUser(req);
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.isScheduleAdmin(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
		
		
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		String eh="";
		eh+=webHelper.GetSlickgridHeaders();
		eh+="<script src='/s/js/swarm-grid-schedules.js"+nocache+"'></script>\n";
		eh+="<script src='/s/js/swarm-api-admin-schedules.js"+nocache+"'></script>\n";
		
		String rv = webHelper.beginHtml(su, "Admin Schedule", "people", eh);

		rv+="<div class='col-xs-3 center'>\n";
		rv+="<SELECT NAME='channel' id='schannel' onchange='doSchedule();'>\n";
	    //OneMydetvChannel selectedMydetvChannel=null;
	    for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
	    	//if(!mydetvChannel.isActive())
	    	//	continue;
	    	if(!mydetvChannel.getChannelType().equals(1))
	    		continue;
	    	if(authorized.isScheduler(su.getWebUser(), mydetvChannel.getChannelId()))
	    		rv+="<OPTION value='"+mydetvChannel.getChannelId()+"'>"+mydetvChannel.getName()+"</OPTION>\n";
	    //	if(selectedMydetvChannel==null)
	    //		selectedMydetvChannel=mydetvChannel;
	    }
	    rv+="</SELECT>\n";
	    rv+="</div>\n";
	    
		rv+="<div id='myGrid' style='width:1200px;height:400px;'></div>";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);
		return rb.build();
	}
}
