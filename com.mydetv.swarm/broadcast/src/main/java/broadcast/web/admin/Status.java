package broadcast.web.admin;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import broadcast.web.home.HomePage;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/status")
public class Status {
	
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Authorized authorized;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(HomePage.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return userHelper.redirectLogin(sessionUser);
		if(! authorized.hasAnyRole(sessionUser.getWebUser()))
			return userHelper.redirectUnauthorized(sessionUser);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		// status, users, channel status, channel nodes
		Map<String, String[]> qmap = req.getParameterMap();
		
		String eh = "";
		eh+="<script src=\"/s/js/swarm-stats.js?now="+System.currentTimeMillis()+"\"></script>\n";
		if(qmap.containsKey("fc") || qmap.containsKey("fcUser")) {
			eh+="<script src=\"/s/js/swarm-stats-fc.js?now="+System.currentTimeMillis()+"\"></script>\n";
		}
		if(qmap.containsKey("nodes")) {
			eh+="<link rel='stylesheet' href='/s/node_modules/vis/dist/vis.min.css'>\n";
			eh+="<link rel='stylesheet' href='/s/node_modules/vis/dist/vis-network.min.css'>\n";
			eh+="<script src='/s/node_modules/vis/dist/vis.min.js'></script>\n";
			eh+="<script src='/s/node_modules/vis/dist/vis-network.min.js'></script>\n";
		}	
		
		String rv = webHelper.beginHtml(sessionUser, "Status", "status", eh);
		
		if(qmap.containsKey("users"))
			rv+="<script type='text/javascript'>doUsers();</script>";
		else if(qmap.containsKey("channel")) {
			String channel = qmap.get("channel")[0];
			rv+="<script type='text/javascript'>doChannelStats("+channel+");</script>";
		}
		else if(qmap.containsKey("nodes")) {
			String channel = qmap.get("nodes")[0];
			rv+="<script type='text/javascript'>doChannelNodes("+channel+");</script>";
		}
		// TODO: is this used?
		else if(qmap.containsKey("slowlinks")) {
			String channel = qmap.get("nodes")[0];
			rv+="<script type='text/javascript'>doSlowLinks("+channel+");</script>";
		}
		else if(qmap.containsKey("fc")) {
			rv+="<script type='text/javascript'>doFC();</script>";
		}
		else if(qmap.containsKey("fcUser")) {
			rv+="<script type='text/javascript'>getFCUser();</script>";
		}
		else
			rv+="<script type='text/javascript'>doStats();</script>";
		
		rv+="<button class='btn btn-primary' onclick='goStatus();'>status</button>\n";
		rv+="<button class='btn btn-primary' onclick='goUsers();'>users</button>\n";
		rv+="<button class='btn btn-primary' onclick='goFC();'>faulty</button><br>\n";
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			if(! mydetvChannel.isActive())
				continue;	
			rv += "<div>"+mydetvChannel.getName()+" &nbsp";
			rv+="<button class='btn btn-primary' onclick='goChannel("+mydetvChannel.getChannelId()+")'>status</button> &nbsp";
			rv+="<button class='btn btn-primary' onclick='goNodes("+mydetvChannel.getChannelId()+")'>nodes</button>";
			rv+="</div>\n";
		}
		rv+="<div class='status' id='status'></div>";	
		
		rv+="</div>\n";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rb.entity(rv);
		
		return rb.build();	
	}
	

}
