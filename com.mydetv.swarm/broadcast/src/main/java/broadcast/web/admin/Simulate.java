package broadcast.web.admin;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/simulate")
public class Simulate {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Authorized authorized;
	
	private static final Logger log = LoggerFactory.getLogger(Simulate.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return userHelper.redirectLogin(sessionUser);
		if(! authorized.hasAnyRole(sessionUser.getWebUser()))
			return userHelper.redirectUnauthorized(sessionUser);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		// menu, add FC, remove FC, lock/unlock cgbw, lock/unlock su
		
		Map<String, String[]> qmap = req.getParameterMap();
		
		//String eh = "<script src=\"/s/js/swarm-stats.js?now="+System.currentTimeMillis()+"\"></script>\n";
		String eh="<script src=\"/s/js/swarm-api-admin-simulate.js?now="+System.currentTimeMillis()+"\"></script>\n";
		
		String rv = webHelper.beginHtml(sessionUser, "Simulate", "simulate", eh);
		
		
		if(qmap.containsKey("edit")) {
			String channel = qmap.get("edit")[0];
			rv+="<script type='text/javascript'>doFC_edit('"+channel+"');</script>\n";
		}
		else {

			rv+="<button class='btn btn-primary' onclick='adminSimulateShowAllUsers();'>Show All Users</button>\n";
			rv+="<label><input type='checkbox' id='simulateCGBW' onclick='handleSimulateCGBW(this);'>lock CGBW</label>";
			
			
			rv+="<table border=1>";
			rv+="<tr><th>Username</th><th>fc</th><th>ip</th><th>mydetv</th><th>edit</th></tr>\n";
			
			for(SessionUser su : blackboard.getSessionUsers().values()) {
				if(su.getWebUser()==null)
					continue;
	
				rv+="<tr>";
				rv+="<td>"+su.getWebUser().getUsername()+"</td>";
				rv+="<td>"+su.getFaultyConnections().size()+"</td>";
				rv+="<td>"+su.getIp()+":"+su.getIpPort()+"</td>";
				rv+="<td>"+su.getMydetv()+"</td>";
				rv+="<td><a href='/admin/simulate?edit="+su.getMydetv()+"'>edit</a></td>";
				rv+="</tr>\n";
			}
			rv+="</table>\n";
			rv+=showSessionUsers();
		}
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rb.entity(rv);
		
		return rb.build();	
	}
	
	
	
	private String showSessionUsers() {
		String rv="";
		rv+="<table width='90%' border='1'>";
		// header
		rv+="<tr>";
		rv+="<th>username</th>";
		rv+="<th>channel</th>";
		rv+="<th>FC ice failed</th>";
		rv+="<th>FC slow</th>";
		rv+="</tr>";
		
		
		for(SessionUser su : blackboard.getSessionUsers().values()) {
			// skip users not logged in
			if(su.getWebUser()==null)
				continue;
			
			rv+="<tr>";
			// username
			rv+="<td>";
			rv+="<a target='_blank' href=https://dev.swarm.mydetv.com:8991/admin/person?wuid="
			    +su.getWebUser().getWuid()+">"
		        +su.getWebUser().getUsername()
		        +"</a>";
			rv+="</td>";
			
			rv+="<td>";
			if(su.getMydetvChannel()==null)
				rv+="none";
			else
				rv+=blackboard.getMydetvChannels().get(su.getMydetvChannel().getChannelId()).getName();

			rv+=getFC(su);
			
			rv+="</tr>\n";
		}
		
		rv+="</table>";
		return rv;
	}
	
	private String getFC(SessionUser su) {
		String rv="";
		int cntIceFailed=0;
		int cntSlow=0;
		
		for(FaultyConnection fc :su.getFaultyConnections().values()) {
			if(fc.getReason().equals("iceFailed"))
				cntIceFailed++;
			else if(fc.getReason().equals("slow"))
				cntSlow++;
		}
		
		// ice failed
		rv+="<td>"+cntIceFailed+"</td>";		
		// slow
		rv+="<td>"+cntSlow+"</td>";
		return rv;
	}
	
}
