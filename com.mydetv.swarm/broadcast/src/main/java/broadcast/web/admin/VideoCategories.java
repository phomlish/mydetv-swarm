package broadcast.web.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.VideoCategoryMain;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/vc")
public class VideoCategories {
	
	@Inject private Config sharedConfig;
	@Inject private Authorized authorized;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Blackboard blackboard;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(VideoCategories.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser su = userHelper.getSessionUser(req);
		
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.isScheduleAdmin(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
				
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		String eh="";
		eh+="<link rel='stylesheet' href='/s/node_modules/croppie/croppie.css' >";
		eh+="<script src='/s/node_modules/croppie/croppie.min.js'></script>";
		eh+="<script src='/s/js/swarm-api-admin-videocats.js"+nocache+"'></script>";	
		eh+="<script src='/s/js/swarm-uploadimage.js"+nocache+"'></script>";
		String rv = webHelper.beginHtml(su, "Admin Video Categories", "categories", eh);
		
		rv+="<div class='row'>";
		
		rv+="<div class='col-sm-6 center'>";	
		rv+="<div id='divMaincat'>";
		rv+="<SELECT NAME='maincat' id='maincat' onchange='changeMaincat();'>";	
		for(VideoCategoryMain vcm : blackboard.getVideoMaincats().values())
			rv+="<OPTION value='"+vcm.getIdVCM()+"'>"+vcm.getName()+"</OPTION>";		
	    rv+="</SELECT>";
	    rv+="</div>";
	    rv+="</div>"; // column
	    
	    rv+="<div class='col-sm-6 center' style='display: flex;'>";
	    rv+="<div id='maincatDefaultImage'></div>";
	    rv+="&nbsp <button type='button' class='btn' onclick='setVscDefaultImageVCM();'>select new default image</button>";
	    rv+="</div>"; // column
	    
	    rv+="</div>"; // row
    
	    rv+="<div class='row'>";
	    
	    rv+="<div class='col-sm-5 center'>";
	    rv+="<div>Video Sub Categores</div>";
	    rv+="<div id='divSubcat' class='center'></div>";
	    rv+="<div style='display: flex;'>";
	    rv+="<div><input type='text' id='newVscName' placeholder='name'></div>";
	    rv+="<div>&nbsp <input type='text' id='newVscDesc' placeholder='description'></div>";
	    rv+="&nbsp <button type='button' class='btn' onclick='addNewVCS();'>Add new</button>";
	    rv+="</div>"; // flex
	    rv+="</div>";
	    
	    rv+="<div class='col-sm-7 center'>";	
	    rv+="<div>Video Sub Category Images</div>";
	    rv+="<div id='divSubcatImages' class='center'></div>";
	    rv+="<div><button type='button' class='btn' onclick='uploadVCSI();'>Upload</button></div>";	    
	    rv+="</div>";
	    
	    rv+="</div>\n"; // row
		
	    rv+="<input type='file' id='fileElem' style='display:none' onchange='checkImage();'>";
	    rv+="<div id='imageDialog' title='crop/resize/rotate'><div id='croppieDiv'></div></div>";
	    
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);
		return rb.build();
	}

}
