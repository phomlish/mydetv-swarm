package broadcast.web.admin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import javax.inject.Inject;

import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/videos")
public class Videos {

	@Inject private Config sharedConfig;
	@Inject private Authorized authorized;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;	
	private static final Logger log = LoggerFactory.getLogger(Videos.class);
	
	private static String[] convertTypes = {"vp8-opus", "vp8-vorbis", "vp9-opus"};
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
	
		SessionUser su = userHelper.getSessionUser(req);
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.isVideoAdmin(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
		
		Integer wuid = su.getWebUser().getWuid();
		log.info("wuid:"+wuid);
		
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		
		String eh="";
        eh+=webHelper.GetSlickgridHeaders();
        eh+="<script src='/s/js/swarm-api-admin-videos.js"+nocache+"'></script>\n";
        eh+="<script src='/s/js/swarm-grid-videos.js"+nocache+"'></script>\n";
        eh+="<link  href='/s/node_modules/fancybox/dist/css/jquery.fancybox.css' rel='stylesheet'>";
        eh+="<script src='/s/node_modules/fancybox/dist/js/jquery.fancybox.js'></script>";
		
        String rv = webHelper.beginHtml(su, "Admin Videos", "videos", eh);

        rv+="<div class='options-panel'>";
        rv+="<b>Search:</b>";
        rv+="<hr/>";
        rv+="<div style='padding:6px;'>";
        rv+="<label style='width:200px;float:left'>PN contains:</label>";
        rv+="<input type=text id='pnSearch' style='width:100px;'>";
        rv+="<br />";
        
        rv+="<label style='width:200px;float:left'>CM contains:</label>";
        rv+="<input type=text id='cmSearch' style='width:100px;'>";
        rv+="<br />";
        
        rv+="<label style='width:200px;float:left'>CN contains:</label>";
        rv+="<input type=text id='cnSearch' style='width:100px;'>";
        rv+="<br />";
        rv+=" </div>";
        rv+="</div>";

        rv+="<div></div>";
        
        rv+="<div id='myGrid' style='width:100%;height:1000px;'></div>";
              
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);
		return rb.build();		
	}
	
	@SuppressWarnings("unused")
	private void getConvertedVideos(HttpServletRequest req, HttpServletResponse resp, SessionUser su) throws ServletException, IOException {
		Map<String, String[]> queryMap = req.getParameterMap();
		String convertType = "vp8-vorbis";
		if(queryMap.containsKey("convertType")) {
			convertType=queryMap.get("convertType")[0];
		}
		req.setAttribute("thecontent", myContent(convertType));
        
        //ServletContext web1 = getServletContext();
		//ServletContext web2 = web1.getContext("/s");
		//RequestDispatcher dispatcher = web2.getRequestDispatcher("/jsp/videos.jsp");
		//dispatcher.forward(req,resp);
    }
    
    private String myContent(String convertType) {
    String rv="";

    rv+="<SELECT id='ctypes' autocomplete='off' onchange='fctChangeConvertType();'>";
    String selected;
    for(String ct : convertTypes) {
    	if(ct.equals(convertType))
    		selected=" selected='selected' ";
    	else
    		selected="";
    	rv+="<OPTION name='"+ct+"'"+selected+">"+ct+"</OPTION>";
    }
    
    rv+="</SELECT>";

    return rv;
    }
    
	
}
