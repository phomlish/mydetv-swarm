package broadcast.web.admin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/mbps")
public class Mbps {
	
	@Inject private Config sharedConfig;
	@Inject private Authorized authorized;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Mbps.class);

	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser su = userHelper.getSessionUser(req);
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.isVideoAdmin(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
		
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		
		String eh="";
        eh+=webHelper.GetSlickgridHeaders();
        eh+="<script src='/s/js/swarm-api-admin-mbps.js"+nocache+"'></script>\n";
        eh+="<script src='/s/js/swarm-grid-mbps.js"+nocache+"'></script>\n";
        eh+="<link  href='/s/bower_components/fancybox/dist/jquery.fancybox.min.css' rel='stylesheet'>";
        eh+="<script src='/s/bower_components/fancybox/dist/jquery.fancybox.min.js'></script>";
		
        String rv = webHelper.beginHtml(su, "Admin Videos", "videos", eh);
        
        rv+="<div class='options-panel'>";
        rv+="<b>Search:</b>";
        rv+="<hr/>";
        rv+="<div style='padding:6px;'>";
        rv+="<label style='width:200px;float:left'>PN contains:</label>";
        rv+="<input type=text id='pnSearch' style='width:100px;'>";
        rv+="<br />";
        
        rv+="<label style='width:200px;float:left'>CN contains:</label>";
        rv+="<input type=text id='cnSearch' style='width:100px;'>";
        rv+="<br />";
        rv+=" </div>";
        rv+="</div>";

        rv+="<div></div>";
        
		rv+="<div id='myGrid' style='width:1500px;height:400px;'></div>";
        
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);
		return rb.build();
	}
}
