package broadcast.web.admin;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.auth.Validations;
import broadcast.business.Authorized;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.OneMydetvChannel;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.OneWebUserImage;
import shared.data.webuser.OneWebUserLogin;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.WebUserRoleTypesEnum;
import shared.data.webuser.WebUserRoleTypesEnum.WebUserRoleType;
import shared.db.DbWebUser;

@JsonAutoDetect
@Path("/person")
public class Person {
	
	@Inject private Config sharedConfig;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private DbWebUser dbWebUser;
	@Inject private Authorized authorized;
	@Inject private Blackboard blackboard;
	@Inject private Validations validations;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Person.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser su = userHelper.getSessionUser(req);
		if(! su.isAuthenticated())
			return userHelper.redirectLogin(su);
		
		if(! authorized.isPeopleAdmin(su.getWebUser()))
			return userHelper.redirectUnauthorized(su);
		
		Integer wuid = Integer.parseInt(req.getParameter("wuid"));
		
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		String eh="<script src='/s/js/swarm-api-admin-people.js"+nocache+"'></script>\n";
		
		String rv = webHelper.beginHtml(su, "Admin People", "people", eh);
		
		//OneWebUser tuser;
		//SessionUser tsu = blackboard.getSessionUserByWuid(wuid);
		//if(tsu!=null)
		//	tuser=tsu.getWebUser();
		//else
		OneWebUser tuser = dbWebUser.getWebuser(wuid);

		if(tuser!=null) 
			rv+=draw(su.getWebUser(), tuser);
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);
		return rb.build();

	}
	private String draw(OneWebUser euser, OneWebUser tuser) {
		String rv="";
		
		rv+="<div class='row admin_people'>";
		rv+="<div class='col-sm-8'>"+drawPerson(euser,tuser)+"</div>";		
		rv+="<div class='col-sm-4'>"+drawLogins(euser,tuser)+"</div>";
		rv+="</div>";
		return rv;
	}
	
	/**
	 * 
	 * @param euser: user that is manipulating
	 * @param tuser: user to manipulate/display
	 * @return
	 */
	private String drawPerson(OneWebUser euser, OneWebUser tuser) {
		String rv="";
		rv+="<div class='d-none' id='wuid'>"+tuser.getWuid()+"</div>";
		rv+="<table class='table table-bordered'>";
		rv+="<tbody>";	
		rv+="<tr><th>username</th><td>"+tuser.getUsername()+"</td></tr>";
		rv+="<tr><th>added</th><td>"+tuser.getDtAdded()+"</td></tr>";
		rv+="<tr><th>tz</th><td>"+tuser.getUserTimezone()+"</td></tr>";
		
		rv+="<tr><th>emails</th><td>"+drawEmailsTable(tuser.getEmails())+"</td></tr>";
		
		rv+="<tr><th>birthday</th><td>"+tuser.getBirthday()+"</td></tr>";
		rv+="<tr><th>link</th><td>"+tuser.getLink()+"</td></tr>";
		
		if(authorized.isChatAdmin(euser)) {
			rv+="<tr><th>muted</th><td>";		
			rv+=" &nbsp <input type='checkbox' autocomplete='off' onchange='apMuted(this, "+tuser.getWuid()+")'";
			if(tuser.getDtMuted()!=null) {
				rv+=" checked ";
			}
			rv+="><div id='dtMuted'>";
			if(tuser.getDtMuted()!=null)
				rv+=tuser.getDtMuted();
			rv+="</div></td></tr>";
		}
		
		if(authorized.isPeopleAdmin(euser)) {
			rv+="<tr><th>banned</th><td>";	
			rv+=" &nbsp <input type='checkbox' id='cbBanned' autocomplete='off' onchange='apBanned(this, "+tuser.getWuid()+")'";
			if(tuser.getDtBanned()!=null)
				rv+=" checked ";
			
			rv+="><div id='dtBanned'>";
			if(tuser.getDtBanned()!=null)
				rv+=tuser.getDtBanned();	
			rv+="</div></td></tr>";
		
			rv+="<tr><th>locked</th><td>";
			rv+=" &nbsp <input type='checkbox' autocomplete='off' onchange='apLocked(this, "+tuser.getWuid()+")'";
			String state = validations.checkFailed(tuser.getWuid());
			if(state.equals("account locked"))
				rv+=" checked ";			
			rv+=">";		
			rv+="</td></tr>";
			
			rv+="<tr><th>roles</th><td>"+drawRoles(euser, tuser)+"</td></tr>";
		}
				
		rv+="<tr><th>images</th><td>"+drawImages(tuser)+"</td></tr>";
		tuser.getImages();
		rv+="<tbody>";
		rv+="</table>";
		return rv;
	}
	private String drawLogins(OneWebUser euser, OneWebUser tuser) {
		String rv="";
		if(tuser.getLogins()!=null) {
			rv+="<table class='table table-bordered'>";
		    rv+="<thead>";
		    rv+="<tr>";
		    rv+="<th scope='row'>Login DT</th>";
		    rv+="<th>Remote Address</th>";
		    rv+="<th>Logout DT</th>";
		    rv+="<th>Logout Reason</th>";
		    rv+="</tr>";
			rv+="</thead>";
			
		    rv+="<tbody>";
			TreeMap<LocalDateTime, OneWebUserLogin> logins = new TreeMap<LocalDateTime, OneWebUserLogin>(Collections.reverseOrder());
			logins.putAll(tuser.getLogins());
			for(OneWebUserLogin login : logins.values()) {
				rv+="<tr>";
				rv+="<td>"+login.getDtLogin().toString()+"</td>";
				rv+="<td>"+login.getRemoteAddress()+"</td>";
				if(login.getDtLogout()!=null) {
					rv+="<td>"+login.getDtLogout().toString()+"</td>";
					rv+="<td>"+login.getLogoutReason()+"</td>";
				}
				rv+="</tr>";
			}
			rv+="<tbody>";
			rv+="</table>";
		}
		return rv;
	}
	
	private String drawEmailsTable(TreeMap<Integer, OneWebUserEmail> emails) {
		String rv="";
		rv+="<table class='table table-bordered'>";
	    rv+="<thead>";
	    rv+="<tr>";
	    rv+="<th>Email</th>";
	    rv+="<th>Added</th>";
	    rv+="<th>Type</th>";
	    rv+="<th>Verified</th>";
	    rv+="</tr>";
		rv+="</thead>";
		
		rv+="<tbody>";
		for(OneWebUserEmail email : emails.values()) {
			rv+="<tr>";
			rv+="<td scope='row'>"+email.getEmail()+"</td>";
			rv+="<td>"+email.getDtAdded()+"</td>";
			rv+="<td>"+email.getEmailType()+"</td>";
			if(email.getDtVerified()!=null)
				rv+="<td>"+email.getDtVerified()+"</td>";
			rv+="</tr>";
		}
		
		rv+="<tbody>";
		rv+="</table>";
		return rv;
	}
	
	private String drawRoles(OneWebUser euser, OneWebUser tuser) {
		TreeMap<Integer, OneWebUserRole> roles = tuser.getRoles();
		// these help with the complicated logic for roles
		tuser.setAdmin(authorized.isAdmin(tuser));
		euser.setAdmin(authorized.isAdmin(euser));
		
		String rv="";
		rv+="<table class='table' id='tblRoles'>";
	    rv+="<thead>";
	    rv+="<tr>";
	    rv+="<th>Role</th>";
	    rv+="<th>Control</th>";
	    rv+="</tr>";
		rv+="</thead>";
		
		rv+="<tbody>";
		for(WebUserRoleType role : WebUserRoleTypesEnum.WebUserRoleType.values()) {
			rv+="<tr>";
			rv+="<td scope='row'>"+role.name()+"</td>";
			if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()))
				rv+="<td class='mbpLightBg'>"+drawBroadcaster(euser, tuser)+"</td>";			
			else {			
				String id="role-"+role.getLevel();
				rv+="<td><input class='cbrole' type='checkbox' id='"+id+"' name='"+id+"' autocomplete='off'";
				if(doesRolesContainRole(roles, role))
					rv+=" checked ";
				if(! authorized.canEditRole(euser, tuser, role))
					rv+=" disabled='disabled' ";
				rv+=" onchange='roleChange(this,"+role.getLevel()+",0,"+tuser.getWuid()+")'>";
				rv+="</td>";			
			}
			rv+="</tr>";
		}
		
		rv+="<tr><td colspan='2'>";
		rv+="<input type='button' name='submit roles' value='submit roles' onclick='submitRoles()'>";
		rv+="</td></tr>";
		rv+="<tbody>";
		rv+="</table>";
		return rv;
	}
	
	private String drawBroadcaster(OneWebUser euser, OneWebUser tuser) {
		TreeMap<Integer, OneMydetvChannel> channels = blackboard.getMydetvChannels();
		TreeMap<Integer, OneWebUserRole> roles = tuser.getRoles();
		String rv="";
		rv+="<table class='table'>";
	    rv+="<thead>";
	    rv+="<tr>";
	    rv+="<th>Channel</th>";
	    rv+="<th>Control</th>";
	    rv+="</tr>";
		rv+="</thead>";
		
		rv+="<tbody>";
		//canEditRoleBroadcast
		for(OneMydetvChannel channel : channels.values()) {
			rv+="<tr>";
			rv+="<td>"+channel.getName()+"</td>";
			String id="role-"+WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()+"-"+channel.getChannelId();
			rv+="<td><input type='checkbox' class='cbrole' id='"+id+"' name='"+id+"' autocomplete='off'";
			if(doesRolesContainRoleDetails(roles, WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER, channel.getChannelId().toString()))
				rv+=" checked ";
			if(! authorized.canEditRoleBroadcast(euser, tuser, channel.getChannelId()))
				rv+=" disabled='disabled' ";
			else
				rv+=" onchange='roleChange(this,"
					+WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()+","
					+channel.getChannelId()+","
					+tuser.getWuid()+")'";
			rv+=">";
			rv+="</td>";
			rv+="</tr>";
		}
		rv+="<tbody>";
		rv+="</table>";
		return rv;
	}
	
	private String drawImages(OneWebUser tuser) {
		String rv="";
		rv+="<table class='table'>";
		TreeMap<Integer, OneWebUserImage> images = dbWebUser.getWebUserImagesPlusInactive(tuser);
		
		rv+="<tbody>";
		
		for(OneWebUserImage image : images.values()) {
			if(image.getImageSelected())
				rv+="<tr class='bggreen'>";
			else if(image.getDtInactive()==null) 
				rv+="<tr class='bggrey'>";
			else
				rv+="<tr>";
			rv+="<td>";
			rv+=image.getOrigFilename()+"<br>";
			rv+=image.getDtAdded()+"<br>";
			rv+=image.getDtInactive()+"<br>";
			rv+="selected:"+image.getImageSelected()+"<br>";
			rv+="</td>";
			rv+="<td><img class='person";
			//rv+="' src='data:image/png;base64, "+Base64.getEncoder().encodeToString(image.getOrigFile())+"'>";
			String url = "/ext/people/"+tuser.getWuid()+"/"+image.getNewFn();
			rv+="' src='"+url+"'>";
			rv+="</td>";
			rv+="</tr>";
		}
		
		rv+="<tbody>";
		rv+="</table>";
		return rv;
	}
	private Boolean doesRolesContainRole(TreeMap<Integer, OneWebUserRole> roles, WebUserRoleType role) {		
		for(OneWebUserRole trole:roles.values())
			if(trole.getWebUserRoleType().equals(role.getLevel())) 
				return true;		
		return false;
	}
	private Boolean doesRolesContainRoleDetails(TreeMap<Integer, OneWebUserRole> roles, WebUserRoleType role, String details) {
		if(details==null)
			return false;
		for(OneWebUserRole trole:roles.values()) {
			if(! trole.getWebUserRoleType().equals(role.getLevel())) 
				continue;
			if(trole.getDetails()==null)
				continue;
			if(trole.getDetails().equals(details))
				return true;
		}
		return false;
	}
}
	
