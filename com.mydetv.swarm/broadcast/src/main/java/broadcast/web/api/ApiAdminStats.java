package broadcast.web.api;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.data.status.SChannelStats;
import broadcast.data.status.SStatus;
import shared.data.OneMydetvChannel;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/stats")
public class ApiAdminStats {

	private static final Logger log = LoggerFactory.getLogger(ApiAdminStats.class);
	@Inject private Blackboard blackboard;
	@Inject private Config sharedConfig;
	@Inject private ApiStatsHelpers apiStatsHelpers;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
	public Response getStatus(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	String sessionId = req.getSession().getId();
    	log.info("sessionId:"+sessionId);
    	SStatus myStatus = new SStatus();
    	myStatus.setVersion(sharedConfig.getVersion());
    	myStatus.setBootTime(blackboard.getBootTime().toString());
    	
    	Timestamp now = new Timestamp(System.currentTimeMillis());
    	Integer seconds = (int) (now.getTime() - blackboard.getBootTime().getTime())/1000;
    	int days = (int)TimeUnit.SECONDS.toDays(seconds);  
    	long hours = TimeUnit.SECONDS.toHours(seconds) - (days *24);
    	long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
    	myStatus.setUptime(days+" days, "+hours+" hours, "+minutes+" minutes");   	
    	myStatus.setThreads(java.lang.Thread.activeCount());
    	
    	Runtime runtime = Runtime.getRuntime();
    	NumberFormat format = NumberFormat.getInstance();
    	long maxMemory = runtime.maxMemory();
    	long allocatedMemory = runtime.totalMemory();
    	long freeMemory = runtime.freeMemory();
    	myStatus.setMemoryAllocated(format.format(allocatedMemory / 1024));
    	myStatus.setMemoryFree(format.format(freeMemory / 1024));
    	myStatus.setMemoryMaximum(format.format(maxMemory / 1024));
    	myStatus.setMemoryUsed(format.format((allocatedMemory - freeMemory)/1024));
    	
    	myStatus.setDbPoolActive(sharedConfig.getDbConfig().getSharedPoolDataSource().getNumActive());
    	myStatus.setDbPoolIdle(sharedConfig.getDbConfig().getSharedPoolDataSource().getNumIdle());

    	apiStatsHelpers.addChannels(myStatus);
    	
    	myStatus.setUsersCount(blackboard.getSessionUsers().size());
    	
    	Integer usersInChannelCount = 0;
    	for(SessionUser su : blackboard.getSessionUsers().values())
    		if(su.getWebUser()!=null && ! su.getWebUser().getDbChannelWebUsersId().equals(0)) 
    			usersInChannelCount++; 
    	myStatus.setUsersInChannelCount(usersInChannelCount);
    	
    	return Response.ok().entity(myStatus).build();
    }
    
    @GET 
    @Path("/channel/{channel}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChannelStats(@Context HttpServletRequest req, @PathParam("channel") Integer channel) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity(new SChannelStats()).header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity(new SChannelStats()).build();
    	
    	OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channel);
    	if(! mydetvChannel.isActive())
    		return Response.status(Status.CONFLICT).entity(new SChannelStats()).build();
    	
    	SChannelStats sChannelStats = new SChannelStats(mydetvChannel);
    	log.trace("returning:\n"+new JSONObject(sChannelStats).toString());
    	return Response.ok().entity(sChannelStats).build();
    }
    
    @GET
    @Path("/channelNodes/{channel}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChannelNodesPlayer(@Context HttpServletRequest req, @PathParam("channel") Integer channel) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channel);
    	if(! mydetvChannel.isActive())
    		return Response.ok().entity("{}").build();

    	return Response.ok().entity(apiStatsHelpers.getNodesAndEdges(mydetvChannel)).build();
    }

}
