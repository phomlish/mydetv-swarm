package broadcast.web.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.auth.Birthday;
import broadcast.auth.Encrypt;
import broadcast.auth.Validations;
import broadcast.broadcaster.BroadcastProfileHelpers;
import broadcast.business.SendMail;
import broadcast.business.UserHelper;
import shared.data.Config;
import broadcast.data.auth.PasswordMessage;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.BroadcasterProfile;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.SecurityQuestion;
import shared.db.DbBroadcasterProfile;
import shared.db.DbChats;
import shared.db.DbConnection;
import shared.db.DbLog;
import shared.db.DbWebUser;

@JsonAutoDetect
@Path("/login")
public class ApiLogin {

	private static final Logger log = LoggerFactory.getLogger(ApiLogin.class);
	@Inject private DbConnection dbConnection;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbChats dbChats;
	@Inject private DbBroadcasterProfile dbBP;
	@Inject private BroadcastProfileHelpers bpHelpers;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private Config config;
	@Inject private Validations validations;
	@Inject private DbLog dbLog;
	
	// TODO: check all api's for login system locked, dbLogging
	//if(validations.isSystemLoginLockedCurrently()) 
	//	return validations.showLoginLockedPage(sessionUser);
	
	@POST
	@Path("signin")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest req) {
		
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();
			
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(sessionUser.isAuthenticated()) {
			log.info("already signed in");
			return Response.status(Status.OK).entity("already logged in").build();
		}
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		log.info("signin");
		JSONObject jObject=null;
		String strEmail=null;
		
		Connection conn=null;
		String status = "";
		try {
			conn = dbConnection.getConnection();
			
			try {
				jObject = GetJson.GetJsonObject(req);
				strEmail=jObject.getString("email");
			} catch (IOException e) {
				log.info("malformed json:"+e);
				dbWebUser.insertFailed(conn, 1);
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}	

			// get the email record from the database
			OneWebUserEmail email = dbWebUser.getEmail(strEmail);
			if(email==null) {
				log.info("email not found");
				dbWebUser.insertFailed(conn, 1);
				return Response.status(Status.UNAUTHORIZED).entity("email/password").build();
			}
			
			// now get the webUser from the database
			OneWebUser webUser = dbWebUser.getWebuser(conn, email.getIdWebUser());
			webUser.setWebUserEmailId(email.getIdEmail());
			webUser.setLoggedInEmail(email);
			
			// was he being stupid/naughty?
			status = validations.checkFailed(conn, webUser.getWuid());
			if(! status.equals("ok")) {
				log.error("login failed, code:"+status);
				return Response.status(Status.UNAUTHORIZED).entity("account locked").build();
			}
			
			// things check out so far, is the password valid?
			byte[] encryptedPassword = Encrypt.EncryptPassword(
				jObject.getString("password").toCharArray(), 
				webUser.getPasswordSalt(), 
				config.getSecretKey());
			
			if(! Arrays.equals(encryptedPassword, webUser.getPasswordHash())) {
				log.info("bad password");
				dbWebUser.insertFailed(conn, webUser, 1);
				dbLog.info(conn, "bad password "+email.getEmail());
				status = validations.checkFailed(conn, webUser.getWuid());
				if(! status.equals("ok"))
					return Response.status(Status.UNAUTHORIZED).entity(status).build();
				return Response.status(Status.UNAUTHORIZED).entity("email/password").build();
			}
			
			if(webUser.getDtBanned()!=null) 
				return Response.status(Status.UNAUTHORIZED).entity("banned").build();
			
			dbWebUser.insertLogin(conn, webUser, req.getRemoteAddr()+":"+req.getRemotePort());
			webUser.setLogins(dbWebUser.getWebUserLogin(conn, webUser.getWuid(), 100));
			dbWebUser.webUser_updateFailedClear(conn, webUser.getWuid());
			webUser.setChatBlocked(dbChats.getChatBlocked(conn, webUser));

			webUser.setOver18(Birthday.Is18(webUser.getBirthday()));
			sessionUser.setWebUser(webUser);
			sessionUser.setTheme(webUser.getTheme());
			dbLog.infoWebUser(conn, "signed in "+email.getEmail(),sessionUser);
			
			// fill in our BroadcasterProfiles
			HashMap<Integer, BroadcasterProfile> bps = dbBP.getBPs(conn, webUser.getWuid());
			// switch from String bpData to class (like BroadcasterProfileDataSound)
			bpHelpers.createRealBPdata(bps);
			webUser.setBroadcasterProfiles(bps);
			
			dbConnection.tryClose(conn);
			if(webUser.getRequirePasswordChange())
				return Response.ok().entity("changePassword").build();
			return Response.ok().entity("ok").build();
		} catch(SQLException ex) {
			log.error("ex:"+ex);
			dbConnection.tryClose(conn);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();			
		}
	}
	
	@POST
	@Path("register")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response register(@Context HttpServletRequest req) {
		
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(sessionUser.isAuthenticated()) {
			log.info("already signed in");
			return Response.status(Status.UNAUTHORIZED).entity("already logged in").build();
		}
			
		log.info("register the user");
		JSONObject jo=null;
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			
			jo = GetJson.GetJsonObject(req);
			String username = jo.getString("username");
			String password = jo.getString("password");
			String strEmail = jo.getString("email");
			
			Integer sqq1 = jo.getInt("sqq1");
			String sqa1 = jo.getString("sqa1");
			Integer sqq2 = jo.getInt("sqq2");
			String sqa2 = jo.getString("sqa2");
			
			// these checks occurred once in the browser so no need to be nice about it if they hacked the values, 
			//  send an un-descriptive error if the checks fail here
			if(!isUsernameValid(username)) {
				log.error("username is invalid "+username);			
				return Response.serverError().build();
			}
			// check the database for existing username
			if(dbWebUser.getUsername(username)>0) {
				log.error("username is taken "+username);
				return Response.serverError().build();
			}
			// make sure it 'looks' like an email
			if(!isEmailValid(strEmail)) {
				log.error("email is invalid "+strEmail);			
				return Response.serverError().build();
			}
			
			// can't use email as username
			if(username.equals(strEmail)) {
				log.error("email is invalid "+strEmail);			
				return Response.serverError().build();
			}
			
			// did they give us answers
			if(sqa1.length()<4 || sqa2.length()<4) {
				log.error("sqa are too short "+sqa1+","+sqa2);			
				return Response.serverError().build();
			}
					
			// check security questions are valid questions (old ones do get inActivated)
			HashMap<Integer, SecurityQuestion> questions = dbWebUser.getSecurityQuestions(conn);
			if(! questions.containsKey(sqq1)) {
				log.error("sqq1 does not exist "+sqq1);			
				return Response.serverError().build();
			}
			if(! questions.containsKey(sqq2)) {
				log.error("sqq1 does not exist "+sqq2);			
				return Response.serverError().build();
			}
			
			// check the database for existing email
			OneWebUserEmail temail = dbWebUser.getEmail(conn, strEmail);
			if(temail != null) {
				log.error("email is taken");
				temail.setIpAddress(req.getRemoteAddr());
				TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn,sessionUser.getWebUser().getWuid());
				SendMail.SendDupReg(config, temail,emails);
				dbLog.warnWebUser(conn, "dup registration "+temail.getEmail(), sessionUser);
				return Response.ok().entity("ok").build();  // it's not ok, but don't tell the user that!
			}
			
			// don't let them use the email as the password
			List<String> sanitizedInputs = new ArrayList<String>();
			sanitizedInputs.add(strEmail);
			sanitizedInputs.add(username);
			PasswordMessage pm = PasswordMessage.GetPasswordStrength(password, sanitizedInputs);
			if(!(pm.getScore().equals(3) || pm.getScore().equals(4))) {
				log.info("password is weak");
				return Response.serverError().build();
			}
			
			if(sqa1.contains(username) || sqa1.contains(strEmail) || sqa1.contains(password)) {
				log.error("sqa1 contains username, email, or password");			
				return Response.serverError().build();
			}
			if(sqa2.contains(username) || sqa2.contains(strEmail) || sqa2.contains(password)) {
				log.error("sqa2 contains username, email, or password");			
				return Response.serverError().build();
			}
			
			// **** checks done ****
			// do encryptions
			byte[] userSalt = Encrypt.GenerateSalt();
			byte[] encryptedSqa1 = Encrypt.EncryptSecret(sqa1.toLowerCase().toCharArray(), userSalt, config.getSecretKey());
			byte[] encryptedSqa2 = Encrypt.EncryptSecret(sqa2.toLowerCase().toCharArray(), userSalt, config.getSecretKey());
			byte[] encryptedPassword = Encrypt.EncryptPassword(password.toCharArray(), userSalt, config.getSecretKey());
			
			OneWebUserEmail email = new OneWebUserEmail();
			// generated email validation code
			email.setEmail(strEmail);
			ApiUserSecurity.AddEmailValiationCodes(blackboard, email);
			
			String timezone = userHelper.getBrowserTimezone(req);
			
			// insert
			HashMap<String, Integer> rv = dbWebUser.insertWebUser(
					conn, username, encryptedPassword, userSalt, timezone, email, 
					sqq1, encryptedSqa1, sqq2, encryptedSqa2);
			
			if(! rv.containsKey("idWebUser") || !rv.containsKey("idEmail")) {
				log.error("An error occured creating the webuser in the database");
				return Response.serverError().build();
			}
			
			log.info("registered idWebUser:"+rv.get("idWebUser")+","+rv.get("idEmail"));
			email.setIdWebUser(rv.get("idWebUser"));
			email.setIdEmail(rv.get("idEmail"));
			email.setIpAddress(req.getRemoteAddr());
			SendMail.SendVerificationCodeEmail(config, email, "register");
			dbLog.info(conn, "registered ");
			
			return Response.ok().entity("ok").build();
			
		} catch (JSONException | IOException | SQLException e) {
			log.error("e:"+e);			
		}
		dbConnection.tryClose(conn);
		return Response.serverError().build();
	}
	
	@POST
	@Path("isUsernameAvailable")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response isUsernameAvailable(@Context HttpServletRequest req) {
		
		log.info("validating username");
		JSONObject jObject=null;
		try {
			jObject = GetJson.GetJsonObject(req);
			String username = jObject.getString("username");
			if(dbWebUser.getUsername(username)>0) 
				return Response.ok().entity("unavailable").build();
		} catch (JSONException | IOException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}
		return Response.ok().entity("ok").build();
	}
	
	@POST
	@Path("getPasswordStrength")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getPasswordStrength(@Context HttpServletRequest req) {
		
		JSONObject jsonReceived=null;
		PasswordMessage pm=null;
		JSONObject jsonReturn=null;
		try {
			jsonReceived = GetJson.GetJsonObject(req);
			String password = jsonReceived.getString("password");
			pm = PasswordMessage.GetPasswordStrength(password);
			jsonReturn=new JSONObject(pm);
			log.info("jsonReturn:"+jsonReturn);
		} catch (JSONException  | IOException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}
		return Response.ok().entity(jsonReturn.toString()).build();
	}
		
	@GET
	@Path("logout")
	public Response logout(@Context HttpServletRequest req) {
		
		log.info("logout");
		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		dbLog.infoWebUser("logout",	sessionUser);
		dbWebUser.updateLogin(sessionUser, "logout");
		sessionUser.setWebUser(null);
		
		return Response.ok().entity("ok").build();
	}
		
	private Boolean isUsernameValid(String username) {
		if(username.length()<3) 
			return false;	
		return true;
	}
	
	private Boolean isEmailValid(String email) {
		if(email.length()<3)
			return false;
		
		String[] pieces = email.split("@");
		log.info("pieces:"+pieces.length);
		if(pieces.length!=2) 
			return false;
		if(pieces[0].length()==0)
			return false;
		if(pieces[1].length()==0)
			return false;
		
		return true;
	}
	
	

}
