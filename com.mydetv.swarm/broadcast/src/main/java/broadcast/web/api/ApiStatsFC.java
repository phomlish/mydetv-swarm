package broadcast.web.api;

import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.data.status.SUser;
import broadcast.status.StatusUser;
import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/stats/fc")
public class ApiStatsFC {

	private static final Logger log = LoggerFactory.getLogger(ApiStatsUsers.class);
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private Blackboard blackboard;
	@Inject private StatusUser statusUser;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
	public Response getStatus(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	TreeMap<String, FcUser> users = getUsers();
    	return Response.ok().entity(users).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
	public Response getUserFC(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	try {
    	JSONObject jo = GetJson.GetJsonObject(req);
    	log.info("getting "+jo.getString("mydetv"));
    	} catch(Exception ex) {
    		log.error("ex",ex);
    	}
    	
    	for(SessionUser su : blackboard.getSessionUsers().values()) {
    		return showSessionUserFC(su);
    	}
    	return Response.noContent().entity("{\"status\":\"not found\"}").build();
    }
    
    private Response showSessionUserFC(SessionUser su) {
    	
    	JSONObject jo = new JSONObject();
    	jo.put("user", new JSONObject(new SUser(su)));
    	JSONArray ja = new JSONArray();
    	for(FaultyConnection fc : su.getFaultyConnections().values()) {
    		ja.put(fc);
    	}
    	jo.put("fc", new JSONObject(ja));
    	return Response.ok().entity(jo.toString()).build();
    }
    public class FcUser {
    	private String mydetv;
    	private String username;
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getMydetv() {
			return mydetv;
		}
		public void setMydetv(String mydetv) {
			this.mydetv = mydetv;
		}
    }
    private TreeMap<String, FcUser> getUsers() {
    	TreeMap<String, FcUser> users = new TreeMap<String, FcUser>();
    	
    	for(SessionUser su : blackboard.getSessionUsers().values()) {
    		if(su.getWebUser()==null || su.getWebUser().getUsername()==null) 
    			continue;
    		FcUser fcUser = new FcUser();
    		fcUser.setMydetv(su.getMydetv());
    		fcUser.setUsername(su.getWebUser().getUsername());
    		users.put(su.getMydetv(), fcUser);
    	}
    	return users;
    }
}
