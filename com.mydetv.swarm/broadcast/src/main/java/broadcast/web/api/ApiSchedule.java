package broadcast.web.api;

import java.util.TreeMap;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.business.VideosHelpers;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.VideoCategorySubImage;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.db.DbSchedule;

@JsonAutoDetect
@Path("/schedule")
public class ApiSchedule {

	private static final Logger log = LoggerFactory.getLogger(ApiSchedule.class);
	@Inject private Config sharedConfig;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private DbSchedule dbSchedule;
	@Inject private VideosHelpers videosHelpers;
	static DateTimeFormatter yyyyMMddFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
	static DateTimeFormatter dtfTime = DateTimeFormatter.ofPattern("HH:mm");
	static DateTimeFormatter yyyyMMddFormatterPretty = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@GET
	@Path("/refresh/{apikey}")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response refresh(@Context HttpServletRequest req,
			@PathParam("apikey") String apikey) {
		if(!apikey.equals(sharedConfig.getApiKey()))
			return Response.status(Status.UNAUTHORIZED).build();
		
		for(OneMydetvChannel channel : blackboard.getMydetvChannels().values())
			if(channel.getChannelType().equals(1)) 
				channel.setSchedules(dbSchedule.getSchedulesAll(channel.getChannelId()));
		log.debug("schedules refreshed");
		return Response.ok().entity("ok").build();		
	}
	
	/**
	 * Will work if we have 15 days of schedule in the database
	 * @param req
	 * @param mychannel
	 * @param td
	 * @param commercials
	 * @return
	 */
	@GET
	@Path("/{mychannel}/{td}/{commercials}")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getSchedule(@Context HttpServletRequest req,
			@PathParam("mychannel") Integer mychannel,
			@PathParam("td") String td,
			@PathParam("commercials") Integer commercials) {
		return(getSchedule(req, mychannel, td, commercials, ""));		
	}
	@GET
	@Path("/{mychannel}/{td}/{commercials}/{delta}")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getScheduleDelta(@Context HttpServletRequest req,
			@PathParam("mychannel") Integer mychannel,
			@PathParam("td") String td,
			@PathParam("commercials") Integer commercials,
			@PathParam("delta") String delta) {
		return(getSchedule(req, mychannel, td, commercials, delta));		
	}
	
	private Response getSchedule(HttpServletRequest req, Integer mychannel, String apiDate, Integer commercials, String delta) {

		SessionUser sessionUser = userHelper.getSessionUser(req);

		if(! blackboard.getMydetvChannels().containsKey(mychannel))
			return Response.ok().entity("can't find that channel").build();

		OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(mychannel);
		if(! mydetvChannel.isActive())
			return Response.ok().entity("that channel is not active").build();
		if(! mydetvChannel.getChannelType().equals(1))
			return Response.ok().entity("that channel has no schedule").build();
		if(mydetvChannel.getSchedules() == null || mydetvChannel.getSchedules().size()<1) 
			return Response.ok().entity("schedules are blank").build();

		log.debug("orig apiDate: "+apiDate);
		// get webuser or anonymous user's zoneId
		ZoneId userDtz;
		if(sessionUser.getWebUser()!=null)
			userDtz = sessionUser.getWebUser().getUserDtz();
		else
			userDtz = ZoneId.of(userHelper.getBrowserTimezone(req));

		// Immediately convert db schedule times to zoned dt relative to the user
       	ZonedDateTime userNow = LocalDateTime.now().atZone(ZoneId.of("UTC"));
       	log.debug("userNow:"+userNow);
		ZonedDateTime userNowMidnight = userNow.toLocalDate().atStartOfDay(userDtz);
		log.debug("userNowMidnight:"+userNowMidnight);
		ZonedDateTime userTwoWeeks = userNowMidnight.plusWeeks(2).plusDays(1);
        log.debug("userTwoWeeks: "+userTwoWeeks);
        
        // the target dt relative to the user's timezone
        LocalDate localDate = LocalDate.parse(apiDate, yyyyMMddFormatter);
        ZonedDateTime userTargetDt = localDate.atStartOfDay(userDtz);
		log.debug("userTargetDt orig:"+userTargetDt);
		
		// did they get here by pressing left or right arrow?
        if(delta.equals("plus")) {
        	userTargetDt=userTargetDt.plusDays(1);
        	apiDate = userTargetDt.format(yyyyMMddFormatter);
        }
        else if(delta.equals("minus")) {
        	userTargetDt=userTargetDt.minusDays(1);
        	apiDate = userTargetDt.format(yyyyMMddFormatter);
        }
        log.debug("userTargetDt after plus/minus:"+userTargetDt);
        
        ZoneId utcZone = ZoneId.of("UTC");
        ZonedDateTime realTargetDt = userTargetDt.withZoneSameInstant(utcZone);
        log.debug("realTargetDt:"+realTargetDt.toString());
        
		Boolean hasDayBefore=false;
		Boolean hasDayAfter=false;
	
		TreeMap<LocalDateTime, MySchedule> mySchedules = new TreeMap<LocalDateTime, MySchedule>();
		TreeMap<String, String> days = new TreeMap<String, String>();
		Integer cnt=0;
		
		for(OneSchedule os : mydetvChannel.getSchedules().values()) {
			ZonedDateTime scheduleZdt = os.getDt().atZone(ZoneId.of("UTC"));
			if(scheduleZdt.isBefore(userNowMidnight))
				continue;
			log.debug("channel schedule "+os.getIdschedule()+" "+os.getDt()+" "+os.getPkey());	
		}
		for(OneSchedule os : mydetvChannel.getSchedules().values()) {
			
			//ZonedDateTime scheduleZdt = os.getDt().atZone(utcZone);
			//ZonedDateTime userZdt = os.getDt().atZone(userDtz);
			ZonedDateTime scheduleZdt = os.getDt().atZone(ZoneId.of("UTC"));
				
			// skip anything before user's today
			if(scheduleZdt.isBefore(userNowMidnight) && !(scheduleZdt.isEqual(userNowMidnight)))
				continue;

			// skip anything after 2 weeks from now
			if(scheduleZdt.isAfter(userNowMidnight.plusDays(14)))
				continue;
			if(scheduleZdt.isEqual(userNowMidnight.plusDays(14)))
				continue;

			//log.debug("looking at "+os.log());
			//log.debug("           "+scheduleZdt);
			//ZonedDateTime userZdt = ZonedDateTime.of(os.getDt(), userDtz);
			ZonedDateTime userZdt = scheduleZdt.withZoneSameInstant(userDtz);
			//log.debug("           "+userZdt);
			days.put(
					userZdt.format(yyyyMMddFormatterPretty)+" "+userZdt.getDayOfWeek().name(), 
					userZdt.format(yyyyMMddFormatter));
			
			// to draw the plus and minus signs
			// we've eliminated before yesterday and after +2 weeks
			// are there days before and after our target?
			if(scheduleZdt.isBefore(realTargetDt)) {
				hasDayBefore=true;
				continue;
			}
			
			if(scheduleZdt.isAfter(realTargetDt.plusDays(1)) || scheduleZdt.isEqual(realTargetDt.plusDays(1))) {
				hasDayAfter=true;
				continue;
			}
			
			//log.debug("in range "+hasDayBefore+" "+hasDayAfter);
			MySchedule mySchedule = new MySchedule(os,userDtz);			
			mySchedules.put(mySchedule.dt, mySchedule);

			cnt++;
		}
		
		log.debug("dayBefore:"+hasDayBefore+",dayAfter:"+hasDayAfter);
		
		if(mySchedules.size()<1)
			return Response.ok().entity(apiDate+" is an invalid date").build();
		
		String rv="";
		rv+="<div class='row' id='controlsRow'>\n";
		
	    rv+="<div class='col-sm-4 center'>\n";
	    rv+="<SELECT NAME='channel' id='schannel' onchange='doSchedule();'>\n";

	    for(OneMydetvChannel oneChannel : blackboard.getMydetvChannels().values()) {
	    	if(!oneChannel.isActive())
	    		continue;
	    	if(!oneChannel.getChannelType().equals(1))
	    		continue;
	    	String selected="";
	    	if(mydetvChannel.getChannelId().equals(oneChannel.getChannelId()))
	    		selected=" selected ";
	    	rv+="<OPTION value='"+oneChannel.getChannelId()+"' "+selected+">"+oneChannel.getName()+"</OPTION>\n";
	    }
	    rv+="</SELECT>\n";
	    rv+="</div>\n";

	    rv+="<div class='col-sm-3 center'>\n";
	    rv+="<SELECT NAME='date' id='sdate' onchange='doSchedule();'>\n";
	    for(Entry<String, String> entry : days.entrySet()) {
	    	//log.debug(entry.getKey()+"|"+entry.getValue()+"|"+apiDate);
	    	String selected = "";
	    	if(apiDate.equals(entry.getValue()))
	    		selected = " selected ";
	    	rv+="<OPTION value="+entry.getValue()+" "+selected+">"+entry.getKey()+"</OPTION>\n";
	    }
	    rv+="</SELECT>\n";
	    rv+="</div>\n";
	    
	    rv+="<div class='col-sm-2 center'>\n";
	    rv+="commercials ";
	    String checked="";
	    if(commercials == 1)
	    	checked=" checked ";
	    rv+="<input id='showCommercials' type='checkbox' onchange='doSchedule();' "+checked+">\n";
	    rv+="</div>\n";
	    
	    rv+="<div class='col-sm-3 center'>\n";
    	rv+="TimeZone: "+userDtz.toString()+"<br>\n";
	    if(sessionUser.isAuthenticated())
	    	rv+="Edit your profile to change<br>\n";
	    else
	    	rv+="Login to change by editing your profile<br>\n";    	
	    rv+="</div>\n";
	    
		rv+="</div><p>\n";  // controlsRow
	
		String pmRow="";
		pmRow+="<div class='row title-row mbpDefaultBg' >\n";
		pmRow+="<div class='col-sm-4'></div>";
		pmRow+="<div class='col-sm-1' onclick='minusDay()'>\n";
		if(hasDayBefore) pmRow+="<img width=25 height=25 src='/s/icons/tiny/arrow_left.png'>\n";
		pmRow+="</div>\n";
		pmRow+="<div class='col-sm-2 scheduleDate'>\n";
		//log.debug("test:"+apiDate.substring(0,4)+"|"+apiDate.substring(4,6)+"|"+apiDate.substring(6)+"|");
		pmRow+= String.format("%04d-%02d-%02d", 
			Integer.valueOf(apiDate.substring(0,4)), 
			Integer.valueOf(apiDate.substring(4,6)), 
			Integer.valueOf(apiDate.substring(6)));
		pmRow+="</div>\n";
		pmRow+="<div class='col-sm-1' onclick='plusDay()'>\n";
		if(hasDayAfter) pmRow+="<img width=25 height=25 src='/s/icons/tiny/arrow_right.png'>\n";
		pmRow+="</div>\n";
		pmRow+="</div>\n"; // title-row
		
		// we have our day's schedule in mySchedule
		//for(MySchedule mySchedule : mySchedules.values()) {
		//	OneSchedule os = mySchedule.getOs();
		//	log.debug(""+os.getIdschedule()+" "+os.getDt()+" "+os.getPkey());
		//}
		
		int rcount=0;
		for(MySchedule mySchedule : mySchedules.values()) {
			
			//String dt = mySchedule.getDt().toString(yyyyMMddFormatter);
			if(commercials.equals(0) && mySchedule.getOvc().getIdvideo_category().equals(5))
				continue;
			
			if(rcount%10 == 0)
				rv+=pmRow;
			
			String rcolor;
			if(rcount%2==0) rcolor="mbpDarkBg";
			else rcolor="mbpLightBg";
			
			rv+="<div class='row oneScheduleRow "+rcolor+"'>\n";
			rv+="<div class='col-sm-1'></div>\n";
			rv+="<div class='col-sm-3'>"+mySchedule.getTimeStart()+" - "+mySchedule.getTimeEnd()+"</div>\n";
			rv+="<div class='col-sm-5 left'>";
			rv+=videosHelpers.videoTitleDropdownForSchedule(mydetvChannel, mySchedule.getOv(), mySchedule.getOs());
			rv+="</div>\n";
			rv+="<div class='col-sm-1 left'>"+mySchedule.getThumbnail()+"</div>\n";
			rv+="<div class='col-sm-1'></div>\n";
			rv+="</div>"; // oneScheduleRow

			rcount++;
		}

		return Response.ok().entity(rv).build();
	}

	/**
	 * Precedence:
	 *   ov image
	 *   vsc image
	 *   vmc image
	 * @param mySchedule
	 * @return
	 */
	private String calculateThumbnail(MySchedule mySchedule) {
		OneVideo ov = mySchedule.getOv();
		VideoCategorySub ovc = mySchedule.getOvc();
		VideoCategoryMain omc = mySchedule.getOmc();
		
		String url;
		if(ov.getThumbnail()!=null && ov.getThumbnail() != "" && ! ov.getThumbnail().contains("error")) 
			url = "/v/"+omc.getName()+"/"+ov.getSubdir()+"/"+ov.getThumbnail();
		else if(ovc.getIdVCSI()!=null && ovc.getIdVCSI() != 0) {
			VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(ovc.getIdVCSI());
			url = "/ext/i/vsc/"+vcsi.getFilename();
		}
		else if(omc.getIdVCSI()!=null && omc.getIdVCSI() != 0) {
			VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(omc.getIdVCSI());
			url = "/ext/i/vsc/"+vcsi.getFilename();
		}
		else 
			url = "/ext/i/icons/mydetv/mydetv.png";
		
		String img="";
		if(url.length()>0)
			img = "<img width=80 height=80 src='"+url+"'>";
		return img.replaceAll("//", "/");
	}
	
	public class MySchedule {
		private LocalDateTime dt;
		private String day;
		private String timeStart;
		private String timeEnd;
		private String thumbnail;
		private OneSchedule os;
		private OneVideo ov;
		private VideoCategorySub ovc;
		private VideoCategoryMain omc;

		private MySchedule(OneSchedule os, ZoneId userDtz) {
			this.os=os;
			this.ov = blackboard.getVideos().get(os.getPkey());
			this.ovc = blackboard.getVideoSubcats().get(ov.getCatSub());
			this.omc = blackboard.getVideoMaincats().get(ovc.getCatMain());
			
			dt=os.getDt();
			ZonedDateTime zdtUtc = dt.atZone(ZoneId.of("UTC"));
			ZonedDateTime userZdt = zdtUtc.withZoneSameInstant(userDtz);
			
			day = userZdt.format(yyyyMMddFormatter);
			timeStart = userZdt.format(dtfTime);
			OneVideo ov = blackboard.getVideos().get(os.getPkey());
			
			int duration = (int) Math.round(ov.getLength()+.49999);		
			timeEnd=userZdt.plusSeconds(duration).format(dtfTime);
			
			this.thumbnail=calculateThumbnail(this);	
		}
		// getters/setters
		public String getDay() {
			return day;
		}
		public void setDay(String day) {
			this.day = day;
		}
		public String getTimeStart() {
			return timeStart;
		}
		public void setTimeStart(String timeStart) {
			this.timeStart = timeStart;
		}
		public String getTimeEnd() {
			return timeEnd;
		}
		public void setTimeEnd(String timeEnd) {
			this.timeEnd = timeEnd;
		}
		public String getThumbnail() {
			return thumbnail;
		}
		public void setThumbnail(String thumbnail) {
			this.thumbnail = thumbnail;
		}
		public LocalDateTime getDt() {
			return dt;
		}
		public void setDt(LocalDateTime dt) {
			this.dt = dt;
		}
		public OneSchedule getOs() {
			return os;
		}
		public void setOs(OneSchedule os) {
			this.os = os;
		}
		public OneVideo getOv() {
			return ov;
		}
		public void setOv(OneVideo ov) {
			this.ov = ov;
		}
		public VideoCategorySub getOvc() {
			return ovc;
		}
		public void setOvc(VideoCategorySub ovc) {
			this.ovc = ovc;
		}
		public VideoCategoryMain getOmc() {
			return omc;
		}
		public void setOmc(VideoCategoryMain omc) {
			this.omc = omc;
		}
	}

}
