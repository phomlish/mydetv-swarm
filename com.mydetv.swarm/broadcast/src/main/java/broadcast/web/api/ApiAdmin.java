package broadcast.web.api;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.RefreshBlackboard;
import broadcast.data.web.WMydetvChannel;
import shared.data.OneMydetvChannel;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/admin")
public class ApiAdmin {

	private static final Logger log = LoggerFactory.getLogger(ApiAdmin.class);
	@Inject private Config sharedConfig;
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private RefreshBlackboard refreshBlackboard;
	
	@GET
	@Path("maint/unlockLogin")
	public Response unlockLogin(@Context HttpServletRequest req) {

    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	LocalDateTime twentyMinutesEarlier = LocalDateTime.now().minusMinutes(20);
    	if(blackboard.getSystemLoginLocked() == null || blackboard.getSystemLoginLocked().isAfter(twentyMinutesEarlier)) {
    		blackboard.setSystemLoginLocked(null);
    		return Response.serverError().entity("not locked, will set null anyhow").build();
    	}
    	
    	blackboard.setSystemLoginLocked(null);
		log.info("unlocked system logins");
    	return Response.ok().entity("ok").build();
    }
	
    @GET
    @Path("/maint/{apikey}")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response maintRefresh(@Context HttpServletRequest req, @PathParam("apikey") String apikey) {
		if(!apikey.equals(sharedConfig.getApiKey()))
			return Response.status(Status.UNAUTHORIZED).build();
		
		refreshBlackboard.refreshBlackboard();
		log.info("refreshed videos");
    	return Response.ok().entity("ok").build();
    }
	
    @GET
    @Path("/channel/{channel}")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Response getChannel(@Context HttpServletRequest req, @PathParam("channel") Integer channel) {
    	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(channel != -1 && ! authorized.isBroadcaster(sessionUser.getWebUser(), channel))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	// send a blank channel if we don't know of what he speaks
    	if(! blackboard.getMydetvChannels().containsKey(channel))
    		return Response.ok().entity(new WMydetvChannel()).build();
    	
    	OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channel);
    	WMydetvChannel w = new WMydetvChannel();
    	w.setChannelId(mydetvChannel.getChannelId());
    	w.setName(mydetvChannel.getName());
    	w.setChannelType(mydetvChannel.getChannelType());
    	w.setActive(mydetvChannel.isActive());
    	return Response.ok().entity(w).build();
    }
}
