package broadcast.web.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import shared.data.OneMydetvChannel;
import shared.data.OneVideo;
import shared.data.broadcast.SessionUser;
import shared.db.DbLog;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@JsonAutoDetect
@Path("video")
public class ApiVideo {

	private static final Logger log = LoggerFactory.getLogger(ApiVideo.class);
	@Inject private UserHelper userHelper;
	@Inject private DbLog dbLog;
	
    @POST
    @Path("bad")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
	public Response bad(@Context HttpServletRequest req) {
    	
    	SessionUser su = userHelper.getSessionUser(req);
    	if(! su.isAuthenticated())
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(su.getMydetvChannel()==null) {
    		log.error("no channel");
    		return Response.serverError().build();
    	}
    	OneMydetvChannel channel = su.getMydetvChannel();
    	try {
    		JSONObject jo = GetJson.GetJsonObject(req);
    		Integer pkey = jo.getInt("pkey");
    		OneVideo ovc = channel.getCurrentlyPlaying();
    		if(!ovc.getPkey().equals(pkey)) 
    			log.error("the video "+su.log()+" marked as bad is not curently playing in channel "+channel.getName());
    		String reason = "reporting bad video: "+pkey+ " in channel "+channel.getName()+" reason:"+jo.getString("reason");
    		dbLog.infoWebUser(reason,su);
    		return Response.ok().entity("ok").build();
    	} catch(Exception e) {
    		log.error("e:"+e);
    		return Response.serverError().build();
    	}   	
    }

}
