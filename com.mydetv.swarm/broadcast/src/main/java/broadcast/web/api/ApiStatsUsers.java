package broadcast.web.api;

import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.data.status.SUser;
import broadcast.status.StatusUser;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/stats/users")
public class ApiStatsUsers {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ApiStatsUsers.class);
	@Inject private StatusUser statusUser;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
	public Response getStatus(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	TreeMap<String, SUser> users = statusUser.getAllUsers();
    	return Response.ok().entity(users).build(); 	
    }
}
