package broadcast.web.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.ChannelSchedules;
import broadcast.business.VideosHelpers;
import shared.data.OneChannelSchedule;
import shared.data.OneMydetvChannel;
import shared.data.OneVideo;
import shared.data.OneVideoConverted;
import shared.data.broadcast.SessionUser;
import shared.db.DbChannelSchedule;

@JsonAutoDetect
@Path("/schedules")
public class ApiAdminSchedules {
	
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private DbChannelSchedule dbChannelSchedule;
	@Inject private ChannelSchedules channelSchedules;
	@Inject private VideosHelpers videosHelpers;
	private static final Logger log = LoggerFactory.getLogger(ApiAdminSchedules.class);
	
    @POST
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Response postSchedules(@Context HttpServletRequest req) {
    	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").build();
    	if(! authorized.isScheduleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	   	
    	JSONObject jo=null;
		try {
			jo = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("can't figure out channelId");
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("no channel id").build();
		}
    	Integer channelId=jo.getInt("channelId");
    	
    	if(!authorized.isScheduler(sessionUser.getWebUser(), channelId)) 
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
		if(! blackboard.getMydetvChannels().containsKey(channelId))
			return Response.serverError().entity("channel not found").build();
		
		OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channelId);
		return Response.ok().entity(getMySchedules(mydetvChannel).toString()).build();  
    }
	
	@POST
    @Path("/changeChannelScheduled")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changeChannelScheduled(@Context HttpServletRequest req) {
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isScheduleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
		JSONObject jo=null;
		JSONObject item;
		Integer channel;
		try {
			jo = GetJson.GetJsonObject(req);
			item = jo.getJSONObject("item");
			channel = jo.getInt("channel");
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
		
		if(! authorized.isScheduler(sessionUser.getWebUser(), channel))
			return Response.status(Status.FORBIDDEN).entity("not authorized").build();
		
		if(! blackboard.getMydetvChannels().containsKey(channel)) {
			return Response.serverError().entity("channel not found").build();
		}
		OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channel);
		
		Boolean scheduled = item.getBoolean("scheduled");
		Integer pkey = item.getInt("pkey");
		log.debug("channel "+channel+",pkey:"+pkey+",scheduled:"+scheduled);
		
		OneChannelSchedule ocs = channelSchedules.getChannelSchedule(mydetvChannel, pkey);
		if(ocs==null) {
			log.debug("new");
			if(scheduled) {
				log.error("meltdown, was already scheduled");
				return Response.serverError().entity("was already scheduled").build();
			}			
			ocs = new OneChannelSchedule();
			ocs.setChannelsId(mydetvChannel.getChannelId());
			ocs.setPkey(pkey);
			dbChannelSchedule.insert(ocs);
			mydetvChannel.getChannelSchedules().put(ocs.getIdchannelSchedule(), ocs);
			log.debug("inserted "+ocs.getIdchannelSchedule()+", channel "+ocs.getChannelsId()+", pkey "+ocs.getPkey());
		}
		else {
			log.debug("old");
			if(! scheduled) {
				log.error("meltdown, was not scheduled");
				return Response.serverError().entity("was already scheduled").build();
			}			
			dbChannelSchedule.delete(ocs);
			mydetvChannel.getChannelSchedules().remove(ocs.getIdchannelSchedule());
			}
		
		//mo code here

		
    	return Response.ok().entity("ok").build();
    }

	private JSONObject getMySchedules(OneMydetvChannel mydetvChannel) {
		JSONObject mySchedules = new JSONObject();
		for(OneVideo ov : blackboard.getVideos().values()) {
			if(ov.getDtInactive()!=null)
				continue;
			if(!ov.isConverted())
				continue;
			MySchedule ms = new MySchedule(ov);
			OneChannelSchedule ocs = getScheduled(mydetvChannel, ov);
			if(ocs!=null) {
				ms.setIdCS(ocs.getIdchannelSchedule());
				ms.setScheduled(true);
			}
			else {
				ms.setIdCS(0);
				ms.setScheduled(false);
			}
			OneVideoConverted ovc = videosHelpers.getVideoConverted(ov, "vp8-opus");
			if(ovc!=null)
				ms.setPn(videosHelpers.getVideoPath(ov, ovc));

			mySchedules.put(ov.getPkey().toString(), new JSONObject(ms));
		}
		//log.info("cnt:"+cnt);
		return mySchedules;
	}

	private OneChannelSchedule getScheduled(OneMydetvChannel mydetvChannel, OneVideo ov) {
		for(OneChannelSchedule ocs : mydetvChannel.getChannelSchedules().values()) {
			if(!ocs.getChannelsId().equals(mydetvChannel.getChannelId()))
				continue;
			if(!ocs.getPkey().equals(ov.getPkey()))
				continue;
			return ocs;
		}
		return null;
	}
	public class MySchedule {
		private Integer idCS;
		private Integer pkey;
		private String fn;
		private String subdir;
		private String title;
		private Integer category;
		private String catSubName;
		private String catMainName;
		private Float length;
		private Boolean scheduled;
		private String pn;
		private Boolean donotuse;

		MySchedule(OneVideo ov)  {
			this.setPkey(ov.getPkey());
			this.setFn(ov.getFn());
			this.setSubdir(ov.getSubdir());
			this.setTitle(ov.getTitle());
			this.setCategory(ov.getCatSub());
			this.setLength(ov.getLength());	
			this.setDonotuse(ov.isDoNotUse());
			this.setCatMainName(videosHelpers.getCatMainName(ov));
			this.setCatSubName(videosHelpers.getCatSubName(ov));
		}

		public Integer getPkey() {
			return pkey;
		}
		public void setPkey(Integer pkey) {
			this.pkey = pkey;
		}
		public Integer getIdCS() {
			return idCS;
		}
		public void setIdCS(Integer idCS) {
			this.idCS = idCS;
		}
		public String getFn() {
			return fn;
		}
		public void setFn(String fn) {
			this.fn = fn;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getSubdir() {
			return subdir;
		}
		public void setSubdir(String subdir) {
			this.subdir = subdir;
		}
		public Integer getCategory() {
			return category;
		}
		public void setCategory(Integer category) {
			this.category = category;
		}
		public Float getLength() {
			return length;
		}
		public void setLength(Float length) {
			this.length = length;
		}
		public Boolean getScheduled() {
			return scheduled;
		}
		public void setScheduled(Boolean scheduled) {
			this.scheduled = scheduled;
		}
		public String getPn() {
			return pn;
		}
		public void setPn(String pn) {
			this.pn = pn;
		}
		public Boolean getDonotuse() {
			return donotuse;
		}
		public void setDonotuse(Boolean donotuse) {
			this.donotuse = donotuse;
		}
		public String getCatSubName() {
			return catSubName;
		}
		public void setCatSubName(String catSubName) {
			this.catSubName = catSubName;
		}
		public String getCatMainName() {
			return catMainName;
		}
		public void setCatMainName(String catMainName) {
			this.catMainName = catMainName;
		}
	}
}
