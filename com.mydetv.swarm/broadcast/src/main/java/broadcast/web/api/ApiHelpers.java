package broadcast.web.api;

import javax.inject.Inject;

import org.json.JSONObject;

import broadcast.Blackboard;
import broadcast.business.VideosHelpers;
import broadcast.data.web.WVideo;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;

public class ApiHelpers {

	@Inject private Blackboard blackboard;
	@Inject private VideosHelpers videosHelpers;

	public JSONObject getJvcs() {
		JSONObject jvcs = new JSONObject();
		for(VideoCategorySub vcs : blackboard.getVideoSubcats().values()) {
			jvcs.put(vcs.getIdvideo_category().toString(), new JSONObject(vcs));
		}
		return jvcs;
	}
	public JSONObject getJvcm() {
		JSONObject jvcm = new JSONObject();
		for(VideoCategoryMain vcm : blackboard.getVideoMaincats().values()) {
			jvcm.put(vcm.getIdVCM().toString(), new JSONObject(vcm));
		}
		return jvcm;
	}

	public JSONObject getJsonVideos()  {

		JSONObject jVideos = new JSONObject();

		for(OneVideo ov : blackboard.getVideos().values()) {

			WVideo wv = new WVideo(ov);

			wv.setPn(videosHelpers.getVideoPath(ov));
			wv.setStrDoNotUse(ov.isDoNotUse().toString());

			VideoCategorySub vcs = blackboard.getVideoSubcats().get(ov.getCatSub());
			wv.setVCS(vcs);
			VideoCategoryMain vcm = blackboard.getVideoMaincats().get(vcs.getCatMain());
			wv.setVCM(vcm);
			if(ov.isConverted()) {
				OneVideoConverted ovc = videosHelpers.getVideoConverted(ov, "vp8-opus");
				String pn2 = videosHelpers.getVideoPath(ov, ovc);
				wv.setPn2(pn2);
			}
			jVideos.put(wv.getPkey().toString(), getJsonVideo(ov));
		}

		return jVideos;
	}

	public JSONObject getJsonVideo(OneVideo ov)  {

		WVideo wv = new WVideo(ov);

		wv.setPn(videosHelpers.getVideoPath(ov));
		wv.setStrDoNotUse(ov.isDoNotUse().toString());

		VideoCategorySub vcs = blackboard.getVideoSubcats().get(ov.getCatSub());
		wv.setVCS(vcs);
		VideoCategoryMain vcm = blackboard.getVideoMaincats().get(vcs.getCatMain());
		wv.setVCM(vcm);
		if(ov.isConverted()) {
			OneVideoConverted ovc = videosHelpers.getVideoConverted(ov, "vp8-opus");
			String pn2 = videosHelpers.getVideoPath(ov, ovc);
			wv.setPn2(pn2);
		}

		return  new JSONObject(wv);
	}

	
}
