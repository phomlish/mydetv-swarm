package broadcast.web.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.VideosHelpers;
import shared.data.OneMbp;
import shared.data.OneVideo;
import shared.data.OneVideoConverted;
import shared.data.broadcast.SessionUser;
import shared.db.DbConnection;
import shared.helpers.FilenameHelpers;

@JsonAutoDetect
@Path("/mbps")
public class ApiAdminMbps {

	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private ApiHelpers apiHelpers;
	@Inject private DbConnection dbConnection;
	@Inject private VideosHelpers videosHelpers;
	private static final Logger log = LoggerFactory.getLogger(ApiAdminMbps.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMbps(@Context HttpServletRequest req) {
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	JSONObject jo = new JSONObject();
    	jo.put("jMbps", getMbps());
    	jo.put("jvcs", apiHelpers.getJvcs());
    	jo.put("jvcm", apiHelpers.getJvcm());
    	return Response.ok().entity(jo.toString()).build();
    }	
	
	public JSONObject getMbps() {
		JSONObject jo = new JSONObject();
		for(OneMbp mbp : blackboard.getMbps().values()) {
			if(mbp.getIdVideo()==null || mbp.getIdVideo()==0)
				continue;
			if(!blackboard.getVideos().containsKey(mbp.getIdVideo())) {
				log.error("can't find the video "+mbp.getIdVideo());
				continue;
			}
			OneVideo ov = blackboard.getVideos().get(mbp.getIdVideo());
			OneVideoConverted ovc = videosHelpers.getVideoConverted(ov, "vp8-opus");
			if(ovc==null)
				continue;
			
			String cpn = videosHelpers.getVideoPath(ov, ovc);
			mbp.setCpn(cpn);
			JSONObject jMbp =  new JSONObject(mbp);
			jo.put(mbp.getIdMbp().toString(), jMbp);
		}
		return jo;
	}
	
	@POST
    @Path("/text")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response category(@Context HttpServletRequest req) {

    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		JSONObject jo=null;
		try {
			jo = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
		
		String column = jo.getString("column");
		JSONObject item = jo.getJSONObject("item");
		log.info("new text "+column+","+item);
		Integer idMbp = item.getInt("idMbp");
		String value = item.getString(column);
		if(! validateColumn(column))
			return Response.serverError().build();
		if(column.equals("showdate") && ! FilenameHelpers.ValidateShowDate(value))
			return Response.serverError().build();
		if(column.equals("showset") && FilenameHelpers.GetShowSet(value) == null)
			return Response.serverError().build();
		
		if(!blackboard.getMbps().containsKey(idMbp))
			return Response.serverError().build();
		OneMbp mbp = blackboard.getMbps().get(idMbp);
		updateBB(mbp, column, value);
		
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			String cmd = "UPDATE mbp set "+column+"=? WHERE idMbp=?";
			PreparedStatement ps = conn.prepareStatement(cmd);
			ps.setString(1, value);
			ps.setInt(2, idMbp);
			ps.execute();
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		
		dbConnection.tryClose(conn);
				
		return Response.ok().entity("ok").build();
	}
	
	private void updateBB(OneMbp mbp, String column, Object value) {
		switch(column) {
			case "showdate":
				mbp.setShowdate((String) value);
				break;
			case "showset":
				Integer showset = FilenameHelpers.GetShowSet((String) value);
				mbp.setShowset(showset);
				break;
			case "details":
				mbp.setDetails((String) value);
				break;
			case "fnVideo":
				mbp.setFnVideo((String) value);
				break;
			case "notes":
				mbp.setNotes((String) value);
				break;
		}
	}
	
	private String[] validColumns = new String[]{
			"showdate"
			,"showset"
			,"details"
			,"fnVideo"
			,"notes"
	};
	private Boolean validateColumn(String column) {
		for(String s : validColumns)
			if(s.equals(column))
				return true;
		return false;
	}
}
