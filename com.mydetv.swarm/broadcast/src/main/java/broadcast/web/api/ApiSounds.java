package broadcast.web.api;

import static java.nio.file.StandardOpenOption.READ;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcastProfileHelpers;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.master.SlaveSounds;
import shared.data.Config;
import shared.data.OneSound;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.BroadcasterProfile;

@JsonAutoDetect
@Path("/sounds")
public class ApiSounds {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private BroadcastProfileHelpers broadcastProfileHelpers;
	@Inject private SlaveSounds slaveSounds;
	private static final Logger log = LoggerFactory.getLogger(ApiSounds.class);
	
	@GET
	@Path("/getSounds")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllSounds(@Context HttpServletRequest req) {
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	JSONObject jo = new JSONObject();
    	jo.put("jSounds", getAllSounds());
    	return Response.ok().entity(jo.toString()).build();
    }	
	
	@GET
	@Path("/getProfileSounds")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProfileSounds(@Context HttpServletRequest req) {
		
    	SessionUser su = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(su == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(su.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	JSONObject jo = new JSONObject();
		HashMap<Integer,BroadcasterProfile> bps = broadcastProfileHelpers.getBroadcasterProfiles(su, 0);
    	jo.put("jProfileSounds", bps);
    	return Response.ok().entity(jo.toString()).build();
    }
	
	// add sound to broadcast profile
	@GET
	@Path("/addProfileSound")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addProfileSound(@Context HttpServletRequest req) {
		
    	SessionUser su = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(su == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(su.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	try {
			String strId = URLDecoder.decode(req.getParameter("id"), "UTF-8");
			Integer id = Integer.decode(strId);
			
			log.info("adding "+id);
			if(! blackboard.getSounds().containsKey(id))
				return Response.status(Status.BAD_REQUEST).build();
			
			broadcastProfileHelpers.addBPSound(su, id);
			
    	} catch (UnsupportedEncodingException|NumberFormatException e) {
			log.error("error geting id from req",e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
    	return Response.ok().entity("{\"success\":true}").build();
    }
	
	@GET
	@Path("/removeProfileSound")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeProfileSound(@Context HttpServletRequest req) {
		
    	SessionUser su = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(su == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(su.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	try {
			String strId = URLDecoder.decode(req.getParameter("id"), "UTF-8");
			Integer id = Integer.decode(strId);

			broadcastProfileHelpers.deleteBPSound(su, id);		
			
    	} catch (UnsupportedEncodingException|NumberFormatException e) {
			log.error("error geting id from req",e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
    	
    	return Response.ok().entity("{\"success\":true}").build();
    }
	/*
	@GET
	@Path("/updateProfileSound")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProfileSound(@Context HttpServletRequest req) {
		
    	SessionUser su = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(su == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(su.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	try { synchronized(su.getWebUser().getBroadcasterProfilesLock()) {
			String strBpid = URLDecoder.decode(req.getParameter("bpId"), "UTF-8");
			Integer bpId = Integer.decode(strBpid);
			String strOrder = URLDecoder.decode(req.getParameter("order"), "UTF-8");
			Integer order = Integer.decode(strOrder);
			
			if(! broadcastProfileHelpers.isProfileMine(su.getWebUser(), bpId))
				return Response.status(Status.BAD_REQUEST).build();	
			
			log.info("updating "+bpId);
			
			BroadcasterProfile bp = su.getWebUser().getBroadcasterProfiles().get(bpId);
			String data = bp.getBpData().toString();
		    JSONObject jo = new JSONObject(data);
		    jo.put("order", order);
		    
			dbBP.updateOneBP(bpId, jo.toString());
			bp.setBpData(jo.toString());
			
		} } catch (UnsupportedEncodingException|NumberFormatException e) {
			log.error("error geting id from req",e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
    	
    	return Response.ok().entity("{\"success\":true}").build();
    }
	*/
	@GET
	@Path("/updownProfileSound")
	@Produces(MediaType.APPLICATION_JSON)
	public Response upDownProfileSound(@Context HttpServletRequest req) {
		
    	SessionUser su = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(su == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isBroadcaster(su.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	try {
			String strBpid = URLDecoder.decode(req.getParameter("bpId"), "UTF-8");
			Integer bpId = Integer.decode(strBpid);
			String strUD = URLDecoder.decode(req.getParameter("ud"), "UTF-8");
			Integer ud = Integer.decode(strUD);
					
			broadcastProfileHelpers.updownBpSound(su, bpId, ud);
			
		} catch (UnsupportedEncodingException|NumberFormatException e) {
			log.error("error geting id from req",e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
    	
    	return Response.ok().entity("{\"success\":true}").build();
    }
	
	private JSONObject getAllSounds() {
		JSONObject jo = new JSONObject();
		for(OneSound os : blackboard.getSounds().values()) {
			JSONObject jSound =  new JSONObject(os);
			jo.put(os.getId().toString(), jSound);
		}
		return jo;
	}
	
	@GET
	@Path("/playRemoteSound")
	public Response playRemoteSound(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServletException, IOException
	{
		SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(request);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	Integer id;
		String strId = URLDecoder.decode(request.getParameter("id"), "UTF-8");
		try {
			id = Integer.decode(strId);
		} catch (NumberFormatException e) {
			return Response.serverError().entity("could not decode "+strId).build();
		}

		if(! blackboard.getSounds().containsKey(id))
			return Response.status(Status.BAD_REQUEST).entity("could not find sound with id "+strId).build();
		
		OneSound os = blackboard.getSounds().get(id);
		String pn = os.getDn()+"/"+os.getFn();
    	boolean res = slaveSounds.playRemoteSound(pn);
		if(!res)
			return Response.serverError().entity("slave could not play "+pn).build();

		ResponseBuilder rb = Response.status(200);
		return rb.build();
	}
    	
	// streams a sound file to browser
	@GET
	@Path("/getSound")
	public Response doGetSound(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServletException, IOException
	{
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(request);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		String strId = URLDecoder.decode(request.getParameter("id"), "UTF-8");
		Integer id;
		try {
			id = Integer.decode(strId);
		} catch (NumberFormatException ex) {
			return Response.serverError().entity("not a valid pkey "+strId).build();
		}
		
		if(! blackboard.getSounds().containsKey(id))
			return Response.serverError().entity("can't find that sound "+strId).build();
		
		OneSound os = blackboard.getSounds().get(id);
		
		// sound file path
		String sp = os.getDn()+"/"+os.getFn();
		
		response = doMyGetSound(request, response, sp);
		ResponseBuilder rb = Response.status(200);
		return rb.build();
	}
	
	private static final int BUFFER_LENGTH = 1024 * 16;
	private static final long EXPIRE_TIME = 1000 * 60 * 60 * 24;
	private static final Pattern RANGE_PATTERN = Pattern.compile("bytes=(?<start>\\d*)-(?<end>\\d*)");
	private HttpServletResponse doMyGetSound(HttpServletRequest request, HttpServletResponse response, String sp) {
		try {
			
			java.nio.file.Path sound = Paths.get(config.getSoundDirectory(), sp);
			log.info("sending:"+sound.toString());
			int length = (int) Files.size(sound);
			int start = 0;
			int end = length - 1;
	
			String range = request.getHeader("Range");
			log.info("range:"+range);
			if(range==null) {
				log.error("no range");
			}
			else {
			Matcher matcher = RANGE_PATTERN.matcher(range);
			
			if (matcher.matches()) {
				String startGroup = matcher.group("start");
				start = startGroup.isEmpty() ? start : Integer.valueOf(startGroup);
				start = start < 0 ? 0 : start;
	
				String endGroup = matcher.group("end");
				end = endGroup.isEmpty() ? end : Integer.valueOf(endGroup);
				end = end > length - 1 ? length - 1 : end;
			}
			}
			int contentLength = end - start + 1;

			response.reset();
			response.setBufferSize(BUFFER_LENGTH);
			response.setHeader("Content-Disposition", String.format("inline;filename=\"%s\"", sp));
			response.setHeader("Accept-Ranges", "bytes");
			response.setDateHeader("Last-Modified", Files.getLastModifiedTime(sound).toMillis());
			response.setDateHeader("Expires", System.currentTimeMillis() + EXPIRE_TIME);
			response.setContentType(Files.probeContentType(sound));
			response.setHeader("Content-Range", String.format("bytes %s-%s/%s", start, end, length));
			response.setHeader("Content-Length", String.format("%s", contentLength));
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

			int bytesRead;
			int bytesLeft = contentLength;
			ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
	
			try (
				SeekableByteChannel input = Files.newByteChannel(sound, READ);
				OutputStream output = response.getOutputStream()
				) {
	
					input.position(start);
	
					while ((bytesRead = input.read(buffer)) != -1 && bytesLeft > 0) {
						buffer.clear();
						output.write(buffer.array(), 0, bytesLeft < bytesRead ? bytesLeft : bytesRead);
						bytesLeft -= bytesRead;
					}
				}
			
		} catch (IOException e) {
			log.error("Error "+e);
		}
		return response;
	}
}
