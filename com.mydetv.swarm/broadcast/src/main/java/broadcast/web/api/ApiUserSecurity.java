package broadcast.web.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.auth.Encrypt;
import broadcast.auth.Validations;
import broadcast.business.Authenticated;
import broadcast.business.SendMail;
import broadcast.business.UserHelper;
import shared.data.Config;
import broadcast.data.auth.PasswordMessage;
import broadcast.web.auth.Security;
import shared.data.MydetvLogLevelsEnum;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.Password;
import shared.data.webuser.SQWebUser;
import shared.data.webuser.SecurityQuestion;
import shared.db.DbConnection;
import shared.db.DbLog;
import shared.db.DbWebUser;

@JsonAutoDetect
@Path("/user-security")
public class ApiUserSecurity {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbConnection dbConnection;
	@Inject private UserHelper userHelper;
	@Inject private Security security;
	@Inject private DbLog dbLog;
	@Inject private Validations validations;
	private static final Logger log = LoggerFactory.getLogger(ApiUserSecurity.class);

	@POST
	@Path("/reauth")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response reauth(@Context HttpServletRequest req) {
		SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
		if(sessionUser == null) 
			return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();

		Connection conn=null;
		try {

			JSONObject jo = GetJson.GetJsonObject(req);
			String password = jo.getString("password");

			if(password==null || password=="")
				return Response.ok().entity("invalid").build();

			conn = dbConnection.getConnection();

			String status = validations.checkFailed(conn, sessionUser.getWebUser().getWuid());
			if(! status.equals("ok")) {
				log.error("login failed, code:"+status);
				return Response.ok().entity(status).build();
			}
			byte[] ePassword = Encrypt.EncryptPassword(password.toCharArray(), sessionUser.getWebUser().getPasswordSalt(), config.getSecretKey());
			if(Arrays.equals(ePassword,sessionUser.getWebUser().getPasswordHash())) {
				sessionUser.getWebUser().setDtReAuthenticated(LocalDateTime.now());
				dbLog.infoWebUser("reauthenticaion succeeded",sessionUser);			
				return Response.ok().entity("ok").build();
			}
			else {
				dbWebUser.insertFailed(sessionUser, 1);
				dbLog.warnWebUser("reauthenticaion failed",sessionUser);
				return Response.ok().entity("invalid").build();
			}

		} catch (IOException | SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn); 	
			return Response.serverError().build();	
		}
	}

	// this happens from /auth/verify-email or /auth/user-security
	// /auth/verify-email: sends link for logged in user's email that they logged in with
	// /auth/security: sends code for request parameter
	@GET
	@Path("sendNewEmailVerificationlink")
	@Produces(MediaType.TEXT_HTML)
	public Response sendNewEmailVerificationlink(@Context HttpServletRequest req) {

		log.info("submit code");
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}

		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			
			OneWebUserEmail email=null;
			
			String strEmailId = req.getParameter("emailId");
			log.info("strEmailId:"+strEmailId);
			
			// coming from /auth/user-security
			if(strEmailId != null) {
				Integer emailId = Integer.parseInt(strEmailId);
				email=dbWebUser.getEmailById(conn, emailId);
			}
			// coming from /auth/verify-email
			else
				email = sessionUser.getWebUser().getLoggedInEmail();
			
			if(email==null || email.getIdEmail()<1) 
				return Response.serverError().build();
			 
			if(email.getDtVerified()!=null) 
				return Response.status(Status.UNAUTHORIZED).entity("already verified").build();

			String status = validations.checkFailed(conn, email.getIdWebUser());
			if(status.equals("system locked"))
				return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
			else if(status.equals("account locked")) 
				return Response.status(Status.UNAUTHORIZED).entity("account locked").build();
			else if(status.equals("validation exception"))
				return Response.serverError().build();

			AddEmailValiationCodes(blackboard, email);
			dbWebUser.updateEmailVcSent(conn, sessionUser.getWebUser().getLoggedInEmail());
			email.setIpAddress(req.getRemoteAddr());
			SendMail.SendVerificationCodeEmail(config, email, "resendcode");
			dbLog.infoWebUser(conn, "verification code sent for "+email.getEmail(),sessionUser);
			return Response.ok().entity("ok").build();
		} catch (SQLException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}
	}
	// this happens from /auth/verify-email.  sends link for logged in user's email that they logged in with
	@GET
	@Path("sendnewcode")
	@Produces(MediaType.TEXT_HTML)
	public Response sendnewcode(@Context HttpServletRequest req) {

		log.info("submit code");
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}

		String emailId = req.getParameter("emailId");
		log.info("emailId:"+emailId);
		OneWebUserEmail email = sessionUser.getWebUser().getLoggedInEmail();
		if(email==null || email.getIdEmail()<1) 
			return Response.serverError().build();

		if(email.getDtVerified()!=null) 
			return Response.status(Status.UNAUTHORIZED).entity("already verified").build();

		Connection conn = null;
		try {
			conn = dbConnection.getConnection();

			String status = validations.checkFailed(conn, email.getIdWebUser());
			if(status.equals("system locked"))
				return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
			else if(status.equals("account locked")) 
				return Response.status(Status.UNAUTHORIZED).entity("account locked").build();
			else if(status.equals("validation exception"))
				return Response.serverError().build();

			AddEmailValiationCodes(blackboard, email);
			dbWebUser.updateEmailVcSent(conn, sessionUser.getWebUser().getLoggedInEmail());
			email.setIpAddress(req.getRemoteAddr());
			SendMail.SendVerificationCodeEmail(config, email, "resendcode");
			dbLog.infoWebUser(conn, "verification code sent for "+email.getEmail(),sessionUser);
			return Response.ok().entity("ok").build();
		} catch (SQLException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}
	}
	@POST
	@Path("/sendFPC") // forgot password code (this is actually the receiving the code)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response sendForgotPasswordCode(@Context HttpServletRequest req)
	{

		//if(! userHelper.isMydetvCookieKnown(req))
		//	return Response.status(Status.FORBIDDEN).build();

		Connection conn=null;
		try {

			JSONObject jo = GetJson.GetJsonObject(req);
			String strEmail = jo.getString("email");

			// we check for this in the js, so if they are hacking screw them tell them all is ok
			if(strEmail==null || strEmail=="") {
				log.error("no email sent");
				return Response.ok().entity("ok").build();
			}

			conn = dbConnection.getConnection();

			OneWebUserEmail email = dbWebUser.getEmail(conn, strEmail);
			if(email==null) {
				// doesn't exist, but don't tell the user that!
				log.info("email does not exist "+strEmail);
				dbLog.info("tried to send forgot email to non exisent email "+strEmail+" "+req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity("ok").build();
			}
			
			if(email.getDtVerified()==null) {
				log.info("email no verified "+strEmail);
				dbLog.info("tried to send forgot email to unverified email "+strEmail+" "+req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity("ok").build();			
			}

			boolean tooRecent = false;
			HashMap<Integer, Password> passwords = dbWebUser.getPasswords(email.getIdWebUser());
			LocalDateTime oneDayAgo = LocalDateTime.now().minusDays(1);
			for(Password op : passwords.values()) {
				// if they already requested one go ahead and allow them to request another
				if(op.isRequireChange())
					continue;
				// if they changed their password too recently we give up
				if(op.getDtActive() != null && op.getDtActive().isAfter(oneDayAgo))			
					tooRecent=true;
			}

			if(tooRecent) {
				log.info("too recent changes "+strEmail);
				TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn,email.getIdWebUser());
				SendMail.SendTooManyPasswordChanges(config, email, emails);	
				dbConnection.tryClose(conn);
				return Response.ok().entity("ok").build();
			}
			
			String code = Encrypt.GenerateCode(blackboard, 8);
			String uuid = UUID.randomUUID().toString();
			dbWebUser.updateUnlockPassword(conn, email.getIdWebUser(), code, uuid);
			email.setIpAddress(req.getRemoteAddr());
			TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn, email.getIdWebUser());
			SendMail.SendForgotPasswordCode(config, email, emails, code, uuid);
			dbLog.logWebUser(conn, "reauth "+email.getEmail(),email.getIdWebUser(),MydetvLogLevelsEnum.MydetvLogType.INFO.getLevel(), req.getRemoteAddr());
			dbConnection.tryClose(conn);
			return Response.ok().entity("ok").build();

		} catch (IOException | SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn);
			return Response.serverError().build();
		}  
	}
	
	/**
	 * this is actually receiving (js did the sendAnswers)
	 * @param req
	 * @return
	 */
	@POST
	@Path("sendAnswers") // 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response sendAnswers(@Context HttpServletRequest req)
	{

		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		Connection conn=null;
		try {
			conn = dbConnection.getConnection();

			JSONObject jo = GetJson.GetJsonObject(req);
			Integer wuid = jo.getInt("w");
			Integer eid = jo.getInt("e");
			String code = jo.getString("c");
			String uuid = jo.getString("u");
			String sqa1 = jo.getString("sqa1");
			String sqa2 = jo.getString("sqa2");

			dbLog.logWebUser(conn, "processing ansers",0,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
			
			// do everything we can to validate that this unlogged in user is who he says he is...
			OneWebUser webUser = dbWebUser.getWebuser(conn, wuid);
			if(webUser==null) {
				log.error("didn't find that web user "+wuid);
				dbLog.logWebUser(conn, "didn't find that web user "+wuid,0,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}

			SQWebUser sqw = dbWebUser.getSecurityQuestionsAndForgotPasswordStuffForWebuser(wuid, eid);
			if(sqw==null || sqw.getEmail()==null || sqw.getEmail()=="") {
				log.error("provided codes are null "+wuid);
				dbLog.logWebUser(conn, "provided codes don't match",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}

			// we have done our best, now we respond with hints that could be a source of hacking

			// are they in lock down?
			String rv = validations.checkFailed(conn, wuid);
			if(! rv.equals("ok")) {
				log.error(rv+" "+wuid);
				dbLog.logWebUser(conn, rv,wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity(rv).build();
			}

			if(sqw.getUuidForgotEmail()==null || sqw.getForgotPassword()==null) {
				log.error("already used "+wuid);
				dbLog.logWebUser(conn, "already used",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity("already used").build();
			}

			if(!sqw.getUuidForgotEmail().equals(uuid) || !sqw.getForgotPassword().equals(code)) {
				log.error("not latest "+wuid);
				dbLog.logWebUser(conn, "not latest",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity("not latest").build();
			}

			LocalDateTime old = LocalDateTime.now().minusMinutes(15);
			if(sqw.getDtForgotPassword().isBefore(old)) {
				log.error("expired "+wuid);
				dbLog.logWebUser(conn, "expired",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.ok().entity("expired").build();
			}

			// provided answers
			byte[] pa1 = Encrypt.EncryptSecret(sqa1.toCharArray(), sqw.getSalt(), config.getSecretKey());
			byte[] pa2 = Encrypt.EncryptSecret(sqa2.toCharArray(), sqw.getSalt(), config.getSecretKey());
			if(! Arrays.equals(pa1, sqw.getSqa1())
					|| ! Arrays.equals(pa2, sqw.getSqa2())) {
				log.error("provided answers don't match "+wuid);
				dbLog.logWebUser(conn, "provided answers don't matchh",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbWebUser.insertFailed(conn, webUser, 2);

				rv = validations.checkFailed(conn, wuid);
				if(! rv.equals("ok")) {
					log.error(rv+" "+wuid);
					dbLog.logWebUser(conn, rv,wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
					dbConnection.tryClose(conn);
					return Response.ok().entity("bad "+rv).build();
				}

				dbConnection.tryClose(conn);				
				return Response.ok().entity("bad answers").build();
			}

			// everything checked out, generate a new password
			String newPassword = Encrypt.GenerateCode(blackboard, 8);
			byte[] ePassword = Encrypt.EncryptPassword(newPassword.toCharArray(), webUser.getPasswordSalt(), config.getSecretKey());
			Integer status = dbWebUser.insertPassword(conn, wuid, ePassword, true);

			if(status <1) {
				log.error("insert failed"+wuid);
				dbLog.logWebUser(conn, "inert failed",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}

			dbWebUser.updateUnlockPasswordClear(conn, wuid);
			dbWebUser.webUser_updateFailedClear(conn, wuid);
			dbLog.logWebUser(conn, "new password ",wuid,MydetvLogLevelsEnum.MydetvLogType.WARNING.getLevel(), req.getRemoteAddr());
			
			dbConnection.tryClose(conn);
			return Response.ok().entity(newPassword).build();

		} catch (IOException | SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn);
			return Response.serverError().build();
		}  
	}
	@POST
	@Path("/addemail")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addemail(@Context HttpServletRequest req)
	{
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

		if(! validations.isReAuthenticated(sessionUser))
			return Response.status(Status.UNAUTHORIZED).entity("not reauth").build();

		JSONObject jo;
		Integer idEmail;
		Connection conn=null;
		try {

			jo = GetJson.GetJsonObject(req);
			String email = jo.getString("email");

			// idiot tried to add his current email!
			if(sessionUser.getWebUser().getLoggedInEmail().getEmail().equals(email))
				return Response.ok().entity("invalid").build();

			conn = dbConnection.getConnection();

			OneWebUserEmail oEmail = dbWebUser.getEmail(conn, email);
			if(oEmail != null) {
				log.info("email already taken");
				return Response.ok().entity("invalid").build();
			}

			oEmail = new OneWebUserEmail();
			oEmail.setIdWebUser(sessionUser.getWebUser().getWuid());
			oEmail.setEmail(email);
			AddEmailValiationCodes(blackboard, oEmail);
			idEmail = dbWebUser.insertEmail(conn, oEmail);
			if(idEmail<1) {
				log.info("error insertEmail "+email);
				return Response.serverError().build();
			}
			oEmail.setIdEmail(idEmail);
			oEmail.setIpAddress(req.getRemoteAddr());
			oEmail.setUsername(sessionUser.log());
			SendMail.SendVerificationCodeEmail(config, oEmail, "addemail");
			dbLog.infoWebUser(conn, "new email "+oEmail.getEmail(),sessionUser);
		} catch (IOException | SQLException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}			

		dbConnection.tryClose(conn);
		return Response.ok().entity(idEmail).build();    	
	}

	@GET
	@Path("/getemaildiv")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getemaildiv(@Context HttpServletRequest req)
	{
		SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
		if(sessionUser == null) 
			return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();

		return Response.ok().entity(security.getEmailsDiv(sessionUser)).build();

	}

	@GET
	@Path("/deleteEmail")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteEmail(@Context HttpServletRequest req)

	{
		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

		if(! validations.isReAuthenticated(sessionUser))
			return Response.status(Status.UNAUTHORIZED).entity("not reauth").build();

		Integer idEmail=-1;
		try {
			String strIdEmail = req.getParameter("idEmail");
			idEmail = Integer.decode(strIdEmail);
		} catch(NumberFormatException e) {
			log.info("didn't send the email id");
			return Response.serverError().build();
		}

		OneWebUserEmail email = dbWebUser.getEmailById(idEmail);
		if(email==null) {
			log.info("could not find email "+idEmail);
			return Response.serverError().build();
		}

		if(! email.getIdWebUser().equals(sessionUser.getWebUser().getWuid())) {
			log.info("That is not your email "+idEmail+" webUser:"+sessionUser.getWebUser().getWuid());
			return Response.serverError().build();
		}

		dbWebUser.deleteEmail(email);
		dbLog.infoWebUser("delete email "+email.getEmail(),sessionUser);
		return Response.ok().entity("ok").build();  	
	}


	@POST
	@Path("updatePassword")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePassword(@Context HttpServletRequest req) {

		Connection conn = null;
		PasswordMessage pm=null;
		try {

			if(! userHelper.isMydetvCookieKnown(req))
				return Response.status(Status.FORBIDDEN).build();

			if(validations.isSystemLoginLockedCurrently())
				return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

			SessionUser sessionUser = userHelper.getSessionUser(req);
			if(! sessionUser.isAuthenticated())
				return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

			if(! validations.isReAuthenticated(sessionUser))
				return Response.status(Status.UNAUTHORIZED).entity("not reauth").build();


			conn = dbConnection.getConnection();
			// don't let them use their email as their password
			TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn, sessionUser.getWebUser().getWuid());
			List<String> sanitizedInputs = new ArrayList<String>();
			for(OneWebUserEmail email : emails.values())  
				sanitizedInputs.add(email.getEmail());

			JSONObject jsonReceived = GetJson.GetJsonObject(req);
			String password = jsonReceived.getString("password");
			pm = PasswordMessage.GetPasswordStrength(password, sanitizedInputs);
			if(!(pm.getScore().equals(3) || pm.getScore().equals(4))) {
				log.error("The browser validated this, hack alert! "+sessionUser.getWebUser().getWuid());
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}

			byte[] userSalt = sessionUser.getWebUser().getPasswordSalt();
			byte[] encryptedPassword = Encrypt.EncryptPassword(password.toCharArray(), userSalt, config.getSecretKey());

			if(Arrays.equals(encryptedPassword, sessionUser.getWebUser().getPasswordHash())) {
				dbConnection.tryClose(conn);
				return Response.ok("You can't change to your current password.").build();
			}

			HashMap<Integer, Password> passwords = dbWebUser.getPasswords(conn, sessionUser.getWebUser().getWuid());
			for(Password op : passwords.values()) {
				LocalDateTime oneDayAgo = LocalDateTime.now().minusDays(1);
				if(op.getDtActive() != null && op.getDtActive().isAfter(oneDayAgo)) {
					dbConnection.tryClose(conn);
					return Response.ok("Password changed too recently.").build();
				}
				if(Arrays.equals(encryptedPassword, op.getPasswordHash())) {
					dbConnection.tryClose(conn);
					return Response.ok("Same password as recently used.").build();
				}
			}

			Integer status = dbWebUser.insertPassword(conn, sessionUser.getWebUser().getWuid(), encryptedPassword, false);
			log.info("status:"+status);
			if(status<0) {
				log.error("status:"+status);
				dbConnection.tryClose(conn);
				return Response.ok("error "+status).build();
			}
			sessionUser.getWebUser().setPasswordHash(encryptedPassword);

			dbLog.infoWebUser(conn, "new password",sessionUser);
			dbConnection.tryClose(conn);
			return Response.ok("ok").build();

		} catch (JSONException  | IOException | SQLException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}
	}
	@GET
	@Path("deleteAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteAccount(@Context HttpServletRequest req) {

		if(! userHelper.isMydetvCookieKnown(req))
			return Response.status(Status.FORBIDDEN).build();

		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

		if(! validations.isReAuthenticated(sessionUser))
			return Response.status(Status.UNAUTHORIZED).entity("not reauth").build();

		Connection conn = null;
		try {
			conn = dbConnection.getConnection();

			dbWebUser.deleteWebUser(conn, sessionUser.getWebUser().getWuid());
			dbLog.warnWebUser(conn, "delete account",sessionUser);
			sessionUser.setWebUser(null);
			dbConnection.tryClose(conn);
			return Response.ok("ok").build();

		} catch (SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn);
			return Response.serverError().build();
		}
	}

	@POST
	@Path("changeTemporaryPassword")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTemporaryPassword(@Context HttpServletRequest req) {

		Connection conn = null;
		PasswordMessage pm=null;
		try {

			if(! userHelper.isMydetvCookieKnown(req))
				return Response.status(Status.FORBIDDEN).build();

			if(validations.isSystemLoginLockedCurrently())
				return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

			SessionUser sessionUser = userHelper.getSessionUser(req);
			if(! sessionUser.isLoggedIn())
				return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

			JSONObject jsonReceived = GetJson.GetJsonObject(req);
			String currentPassword = jsonReceived.getString("currentPassword");
			String newPassword = jsonReceived.getString("newPassword");

			// do this first!  make sure they know the temp password
			byte[] userSalt = sessionUser.getWebUser().getPasswordSalt();
			byte[] encryptedCurrentPassword = Encrypt.EncryptPassword(currentPassword.toCharArray(), userSalt, config.getSecretKey());
			if(! Arrays.equals(encryptedCurrentPassword, sessionUser.getWebUser().getPasswordHash())) {
				return Response.ok("The current password you supplied does not match your temporary password.").build();
			}

			conn = dbConnection.getConnection();

			// don't let them use their email as their password
			TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn, sessionUser.getWebUser().getWuid());
			List<String> sanitizedInputs = new ArrayList<String>();
			for(OneWebUserEmail email : emails.values())  
				sanitizedInputs.add(email.getEmail());

			pm = PasswordMessage.GetPasswordStrength(newPassword, sanitizedInputs);
			if(!(pm.getScore().equals(3) || pm.getScore().equals(4))) {
				log.error("The browser validated this, hack alert! "+sessionUser.getWebUser().getWuid());
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}

			byte[] encryptedNewPassword = Encrypt.EncryptPassword(newPassword.toCharArray(), userSalt, config.getSecretKey());
			if(Arrays.equals(encryptedNewPassword, sessionUser.getWebUser().getPasswordHash())) {
				dbConnection.tryClose(conn);
				return Response.ok("You can't use your temporary password.").build();
			}

			HashMap<Integer, Password> passwords = dbWebUser.getPasswords(conn, sessionUser.getWebUser().getWuid());
			for(Password op : passwords.values()) {
				if(Arrays.equals(encryptedNewPassword, op.getPasswordHash())) {
					dbConnection.tryClose(conn);
					return Response.ok("Same password as recently used.").build();
				}
			}

			Integer status = dbWebUser.insertPassword(conn, sessionUser.getWebUser().getWuid(), encryptedNewPassword, false);
			log.info("status:"+status);
			if(status<0) {
				log.error("status:"+status);
				dbConnection.tryClose(conn);
				return Response.serverError().build();
			}
			sessionUser.getWebUser().setPasswordHash(encryptedNewPassword);

			dbLog.infoWebUser(conn, "new password",sessionUser);
			dbConnection.tryClose(conn);
			sessionUser.setWebUser(null);
			return Response.ok("ok").build();

		} catch (JSONException  | IOException | SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn);
			return Response.serverError().build();
		}
	}

	@POST
	@Path("updateSQ")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSecurityQuestions(@Context HttpServletRequest req) {

		Connection conn=null;
		try {

			if(! userHelper.isMydetvCookieKnown(req)) {
				return Response.status(Status.FORBIDDEN).build();
			}

			if(validations.isSystemLoginLockedCurrently())
				return Response.status(Status.UNAUTHORIZED).entity("system locked").build();

			SessionUser sessionUser = userHelper.getSessionUser(req);			
			if(! sessionUser.isAuthenticated())
				return Response.status(Status.UNAUTHORIZED).entity("not auth").build();

			if(! validations.isReAuthenticated(sessionUser))
				return Response.status(Status.UNAUTHORIZED).entity("not reauth").build();

			JSONObject jo = GetJson.GetJsonObject(req);
			Integer sqq1 = jo.getInt("sqq1");
			Integer sqq2 = jo.getInt("sqq2");
			String sqa1 = jo.getString("sqa1");
			String sqa2 = jo.getString("sqa2");

			conn = dbConnection.getConnection();
			HashMap<Integer, SecurityQuestion> questions = dbWebUser.getSecurityQuestions(conn);
			if(! questions.containsKey(sqq1)) {
				log.error("sqq1 does not exist "+sqq1);			
				return Response.serverError().build();
			}
			if(! questions.containsKey(sqq2)) {
				log.error("sqq2 does not exist "+sqq2);			
				return Response.serverError().build();
			}
			if(sqa1.length()<4 || sqa2.length()<4) {
				log.error("sqa length too short");			
				return Response.serverError().build();
			}
			SQWebUser sqw =new SQWebUser();
			sqw.setIdWebUser(sessionUser.getWebUser().getWuid());
			sqw.setSqq1(sqq1);
			sqw.setSqq2(sqq2);
			sqw.setSqa1(Encrypt.EncryptSecret(sqa1.toLowerCase().toCharArray(), sessionUser.getWebUser().getPasswordSalt(), config.getSecretKey()));
			sqw.setSqa2(Encrypt.EncryptSecret(sqa2.toLowerCase().toCharArray(), sessionUser.getWebUser().getPasswordSalt(), config.getSecretKey()));

			dbWebUser.webUser_updateSQ(conn, sqw);
			dbLog.infoWebUser(conn, "update sq ",sessionUser);
			return Response.ok("ok").build();

		} catch (JSONException | IOException | SQLException e) {
			log.error("e:"+e);			
		}

		dbConnection.tryClose(conn);
		return Response.serverError().build();
	}

	public static void AddEmailValiationCodes(Blackboard blackboard, OneWebUserEmail email) {
		String verificationCode = Encrypt.GenerateCode(blackboard, 8);
		String uuid = UUID.randomUUID().toString();
		email.setVerificationCode(verificationCode);
		email.setDtVcSent(LocalDateTime.now());
		email.setUuid(uuid);
	}
}


