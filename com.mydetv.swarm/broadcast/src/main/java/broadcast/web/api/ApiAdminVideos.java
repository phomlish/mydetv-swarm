package broadcast.web.api;


import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/videos")
public class ApiAdminVideos {

	@Inject private Authorized authorized;
	@Inject private Authenticated authenticated;
	@Inject private ApiHelpers apiHelpers;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ApiAdminVideos.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVideos(@Context HttpServletRequest req) {
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	JSONObject jo = new JSONObject();
    	jo.put("jVideos", apiHelpers.getJsonVideos());
    	jo.put("jvcs", apiHelpers.getJvcs());
    	jo.put("jvcm", apiHelpers.getJvcm());
    	return Response.ok().entity(jo.toString()).build();
    }	
		

}
