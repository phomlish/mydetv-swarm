package broadcast.web.api;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.auth.Birthday;
import broadcast.auth.Validations;
import broadcast.business.Authenticated;
import broadcast.business.UserHelper;
import broadcast.web.auth.Profile;
import shared.data.BrowserThemes;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUserImage;
import shared.db.DbCommand;
import shared.db.DbWebUser;
import shared.helpers.FixPermissions;

@JsonAutoDetect
@Path("/profile")
public class ApiUserProfile {

	@Inject private Config config;
	@Inject private Authenticated authenticated;
	@Inject private DbWebUser dbWebUser;
	@Inject private UserHelper userHelper;
	@Inject private DbCommand dbCommand;
	@Inject private Validations validations;
	private static final Logger log = LoggerFactory.getLogger(ApiUserProfile.class);
	
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response update(
		@Context HttpServletRequest req) {
    	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
	
		JSONObject jo=null;
		try {
			jo = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().build(); 			
		}
		String profilelink=jo.getString("profilelink");
		String tz = jo.getString("tz");
		String byear = jo.getString("byear");
		String bmonth = jo.getString("bmonth");
		String bday = jo.getString("bday");
		
		if(profilelink != null && ! profilelink.isEmpty()) {
			if(profilelink.startsWith("http://") && profilelink.substring("http://".length()).length()<3)
				return Response.serverError().build(); 
			if(profilelink.startsWith("https://") && profilelink.substring("https://".length()).length()<4)
				return Response.serverError().build(); 
		}
		
		ArrayList<String> zoneList = new ArrayList<String>(ZoneId.getAvailableZoneIds());
		if(! zoneList.contains(tz)) 
			return Response.serverError().build(); 
		LocalDate bdate = Birthday.GetDate(byear, bmonth, bday);

		Integer rc = dbWebUser.updateWebUser(sessionUser, bdate, profilelink, tz);
		if(rc<1) 
			return Response.serverError().build(); 
		
		sessionUser.getWebUser().setLink(profilelink);
		sessionUser.getWebUser().setUserTimezone(tz);
		//sessionUser.getWebUser().setUserDtz(DateTimeZone.forID(tz));
		//ZoneId zoneId = ZoneOffset.of("+08:00")
		sessionUser.getWebUser().setUserDtz(ZoneId.of(tz));
		sessionUser.getWebUser().setBirthday(bdate);
		sessionUser.getWebUser().setOver18(Birthday.Is18(bdate));
    	return Response.ok().entity("ok").build();   	
    }
	
    @POST
    @Path("changeTheme")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
    public Response changedTheme(@Context HttpServletRequest req) {
		log.info("deleteProfileImage");
		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}

		JSONObject jObject;
		try {
			jObject = GetJson.GetJsonObject(req);

			String theme = jObject.getString("theme");
			if(sessionUser.getTheme().equals(theme)) {
				return Response.serverError().entity("trying to change theme to same "+theme).build();
			}

			if(! BrowserThemes.getBrowserThemes().contains(theme))
				return Response.serverError().entity("unknown theme "+theme).build();
			
			String dbc = "UPDATE webUser set theme='"+theme+"' where idWebUser="+sessionUser.getWebUser().getWuid();
			dbCommand.doCommand(dbc);
			
			sessionUser.setTheme(theme);
			sessionUser.getWebUser().setTheme(theme);
			return Response.ok().entity("ok").build();
		} catch (IOException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}	
    }
    
	@POST
	@Path("deleteProfileImage")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteProfileImage(@Context HttpServletRequest req) {
		
		log.info("deleteProfileImage");
		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}

		JSONObject jObject;
		try {
			jObject = GetJson.GetJsonObject(req);

			Integer imageId = jObject.getInt("imageId");
			if(! sessionUser.getWebUser().getImages().containsKey(imageId)) {
				log.info("could not find "+imageId);
				return Response.serverError().build();
			}
			OneWebUserImage oneWebUserImage = sessionUser.getWebUser().getImages().get(imageId);
			if(sessionUser.getWebUser().getWebUserSelectedImage().equals(imageId)) {
				return Response.ok().entity("selected image").build();
			}
			dbWebUser.deleteWebUserImage(oneWebUserImage);
			sessionUser.getWebUser().getImages().remove(oneWebUserImage.getIdWebUserImage());
			return Response.ok().entity("ok").build();
		} catch (IOException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}		
	}
	
	@POST
	@Path("selectProfileImage")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProfileImage(
			@Context HttpServletRequest req
	       ) {
		
		log.info("selectProfileImage");
		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not signed in").build();
		}

		JSONObject jObject;
		try {
			jObject = GetJson.GetJsonObject(req);
			Integer imageId = jObject.getInt("imageId");
			if(! sessionUser.getWebUser().getImages().containsKey(imageId)) {
				return Response.serverError().build();
			}
			
			for(OneWebUserImage oneWebUserImage : sessionUser.getWebUser().getImages().values()) {
				oneWebUserImage.setImageSelected(false);
			}
			
			OneWebUserImage oneWebUserImage = sessionUser.getWebUser().getImages().get(imageId);
			oneWebUserImage.setImageSelected(true);
			sessionUser.getWebUser().setWebUserSelectedImage(imageId);
			
			dbWebUser.updateWebUserImage(oneWebUserImage);
			
			jObject = new JSONObject();
			jObject.put("imageId", imageId);			
			return Response.ok().entity(jObject.toString()).build();
		} catch (IOException e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}		
	}
	
	@GET
	@Path("geImagesDiv")
	@Produces(MediaType.TEXT_HTML)
	public Response geImagesDiv(@Context HttpServletRequest req) {
		
		log.info("geImagesDiv");
		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}
		
		try {
			String rv = Profile.GetImagesDiv(sessionUser);
			return Response.ok().entity(rv).build();
		} catch (Exception e) {
			log.error("e:"+e);
			return Response.serverError().build();
		}		
	}
	
	@POST
	@Path("uploadProfileImage")
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadProfileImage(
		@Context HttpServletRequest req,
		@DefaultValue("true") @FormDataParam("enabled") boolean enabled,
		@FormDataParam("origFilename") String origFilename,
		@FormDataParam("imageData") InputStream uploadedInputStream,
		@FormDataParam("imageData") FormDataContentDisposition fileDetail) {

		if(! userHelper.isMydetvCookieKnown(req)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(validations.isSystemLoginLockedCurrently())
			return Response.status(Status.UNAUTHORIZED).entity("system locked").build();
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in");
			return Response.status(Status.UNAUTHORIZED).entity("not auth").build();
		}
		
	    log.info("origFilename:"+origFilename);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    try {
	        
		    int nRead;
		    byte[] data = new byte[16384];
		    while ((nRead = uploadedInputStream.read(data, 0, data.length)) != -1) {
		    	 buffer.write(data, 0, nRead);
		    }
	    } catch (IOException e) {
	    	return Response.serverError().entity("upload failed "+e).build();
	    }
	    
	    byte[] bytes = buffer.toByteArray();
	    OneWebUserImage webUserImage = new OneWebUserImage();
	    webUserImage.setIdWebUser(sessionUser.getWebUser().getWuid());
	    webUserImage.setOrigFilename(origFilename);
	    webUserImage.setOrigFile(bytes);
	    webUserImage.setImageSelected(false);
	    
	    // create the image on the filesystem
	    try {
	    	String nfn = UUID.randomUUID().toString()+".png";
	    	java.nio.file.Path path = java.nio.file.Paths.get(
	    		config.getExternalWebDirectory(),
	    		"people",
	    		webUserImage.getIdWebUser().toString());
			if(! Files.exists(path)) {			
				Files.createDirectories(path);
				FixPermissions.FixFilePermissions(path.toString());				
			}
			path = java.nio.file.Paths.get(path.toString(), nfn);
			Files.write(path, bytes);
			FixPermissions.FixFilePermissions(path.toString());
			
			webUserImage.setNewFn(nfn);
		} catch (IOException e) {
			return Response.serverError().entity("upload failed "+e).build();
		}
		
	    dbWebUser.insertWebUserImage(webUserImage);
	    sessionUser.getWebUser().getImages().put(webUserImage.getIdWebUserImage(), webUserImage);
	    
	    JSONObject jsonObject = new JSONObject();
	    jsonObject.put("imageId", webUserImage.getIdWebUserImage());
	    jsonObject.put("imageCount", sessionUser.getWebUser().getImages().size());
	    
		return Response.ok().entity(jsonObject.toString()).build();
		
	}
}
