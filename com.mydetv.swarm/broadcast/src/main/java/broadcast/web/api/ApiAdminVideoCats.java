package broadcast.web.api;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import shared.data.Config;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.VideoCategorySubImage;
import shared.data.broadcast.SessionUser;
import shared.db.DbCommand;
import shared.db.DbVideoCategorySub;
import shared.db.DbVideoCategorySubImage;
import shared.helpers.FixPermissions;

@JsonAutoDetect
@Path("/admin/videocats")
public class ApiAdminVideoCats {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private DbCommand dbCommand;
	@Inject private DbVideoCategorySub dbVideoCategorySub;
	@Inject private DbVideoCategorySubImage dbVideoCategorySubImage;
	private static final Logger log = LoggerFactory.getLogger(ApiAdminVideoCats.class);
	
	@GET
	@Path("/getsubcat")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doGetSubcat(@Context HttpServletRequest req, @Context HttpServletResponse resp)
	{	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		Integer maincatId = Integer.parseInt(req.getParameter("maincat"));
		if(! blackboard.getVideoMaincats().containsKey(maincatId))
			return Response.serverError().entity("maincat not found").build();
		VideoCategoryMain vcm = blackboard.getVideoMaincats().get(maincatId);
		
		// make a sorted map by subcat name
		TreeMap<String, VideoCategorySub> subcats = new TreeMap<String, VideoCategorySub>();	
		for(VideoCategorySub vsc : blackboard.getVideoSubcats().values())
			if(vsc.getCatMain()==maincatId) 
				subcats.put(vsc.getName(), vsc);
		if(subcats.size()==0)
			return Response.serverError().entity("no subcats found").build();
		
		JSONObject rv = new JSONObject();
		
		JSONObject jsubcats = new JSONObject();
		for(VideoCategorySub vsc : subcats.values()) {
			JSONObject jvsc = new JSONObject();
			jvsc.put("idMain", maincatId);
			jvsc.put("id", vsc.getIdvideo_category());
			jvsc.put("name", vsc.getName());
			jvsc.put("description", vsc.getDescription());
			if(vsc.getIdVCSI()==null)
				jvsc.put("image", 0);
			else {
				jvsc.put("image", vsc.getIdVCSI());
				VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(vsc.getIdVCSI());
				jvsc.put("filename", vcsi.getFilename());
			}
			jsubcats.put(vsc.getName(),jvsc);
		}
		rv.put("jsubcats", jsubcats);
		
		JSONObject jvscis = new JSONObject();
		for(VideoCategorySubImage vcsi : blackboard.getVideoSubCatImages().values()) {
			if(vcsi.getIdVCM()!=vcm.getIdVCM())
				continue;
			JSONObject jvsci = new JSONObject();
			jvsci.put("id", vcsi.getIdVCSI());
			jvsci.put("filename", vcsi.getFilename());
			jvsci.put("fpn", "/ext/i/vsc/"+vcsi.getFilename());
			jvscis.put(vcsi.getIdVCSI().toString(), jvsci);
		}
		rv.put("jvscis", jvscis);
		
		JSONObject vcmi = new JSONObject();
		if(vcm.getIdVCSI()==null)
			vcmi.put("filename", "none");
		else {		
			VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(vcm.getIdVCSI());
			vcmi.put("filename", vcsi.getFilename());
		}
		rv.put("vcmi", vcmi);
		return Response.ok().entity(rv.toString()).build();
	}
	
	@POST
	@Path("uploadVCSI")
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadSubcatImage(
		@Context HttpServletRequest req,
		@DefaultValue("true") @FormDataParam("enabled") boolean enabled,
		@FormDataParam("origFilename") String origFilename,
		@FormDataParam("vcm") Integer vcm,
		@FormDataParam("imageData") InputStream uploadedInputStream,
		@FormDataParam("imageData") FormDataContentDisposition fileDetail) {

    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
	    log.info("origFilename:"+origFilename+",vcm:"+vcm);
	    
	    String filename = UUID.randomUUID().toString()+".png";    
		String fpn = config.getExternalWebDirectory()+"/i/vsc/"+filename;

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        VideoCategorySubImage vcsi;
	    try {
	        
		    int nRead;
		    byte[] data = new byte[16384];
		    while ((nRead = uploadedInputStream.read(data, 0, data.length)) != -1) {
		    	 buffer.write(data, 0, nRead);
		    }
		    byte[] bytes = buffer.toByteArray();
		    FileUtils.writeByteArrayToFile(new File(fpn), bytes);
		    FixPermissions.FixFilePermissions(fpn);
		    vcsi = new VideoCategorySubImage();
		    vcsi.setFilename(filename);
		    vcsi.setIdVCM(vcm);
		    Integer vcsiId = dbVideoCategorySubImage.insertVideoCatSubImage(vcsi);
		    blackboard.getVideoSubCatImages().put(vcsiId, vcsi);
		    
	    } catch (IOException e) {
	    	return Response.serverError().entity("upload failed "+e).build();
	    }
	    JSONObject rv = new JSONObject();
	    rv.put("id", vcsi.getIdVCSI());
	    rv.put("filename", vcsi.getFilename());
	    rv.put("fpn", "/ext/i/vsc/"+filename);
		return Response.ok().entity(rv.toString()).build();		
	}

	@POST
	@Path("selectNewDefaultImageVCM")
	@Produces(MediaType.APPLICATION_JSON)
	public Response selectNewDefaultImageVCM(@Context HttpServletRequest req) {
			
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		try {
			JSONObject jo = GetJson.GetJsonObject(req);
			Integer ivcm = jo.getInt("vcm");
			Integer ivcsi = jo.getInt("vcsi");
			
			if(!blackboard.getVideoMaincats().containsKey(ivcm))
				return Response.serverError().entity("vcm not found").build();
			VideoCategoryMain vcm = blackboard.getVideoMaincats().get(ivcm);
			
			// we're removing a default image
			if(ivcsi==0) {
				String dbc = "UPDATE videoCategoryMain set idVCSI=NULL where idvideoCategoryMain="+vcm.getIdVCM();
				dbCommand.doCommand(dbc);
				vcm.setIdVCSI(null);
				JSONObject rv = new JSONObject();
				rv.put("filename", "none");
				return Response.ok().entity(rv.toString()).build();
			}
			
			if(!blackboard.getVideoSubCatImages().containsKey(ivcsi))
				return Response.serverError().entity("vcsi not found").build();
			VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(ivcsi);
			
			String dbc = "UPDATE videoCategoryMain set idVCSI="+vcsi.getIdVCSI()+" where idvideoCategoryMain="+vcm.getIdVCM();
			dbCommand.doCommand(dbc);
			vcm.setIdVCSI(vcsi.getIdVCSI());
			
			JSONObject rv = new JSONObject();
			rv.put("filename", vcsi.getFilename());
			return Response.ok().entity(rv.toString()).build();
			
		} catch (IOException e) {
			return Response.serverError().entity("error "+e).build();
		}
	}
	
	@POST
	@Path("selectNewDefaultImageVCS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response selectNewDefaultImageVCS(@Context HttpServletRequest req) {
			
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		try {
			JSONObject jo = GetJson.GetJsonObject(req);
			Integer ivcs = jo.getInt("vcs");
			Integer ivcsi = jo.getInt("vcsi");
			
			if(!blackboard.getVideoSubcats().containsKey(ivcs)) 
				return Response.serverError().entity("vcs not found").build();
			VideoCategorySub vcs = blackboard.getVideoSubcats().get(ivcs);
			
			// we're removing a default image
			if(ivcsi==0) {
				String dbc = "UPDATE videoCategorySub set idVCSI=NULL where idvideo_category="+vcs.getIdvideo_category();
				dbCommand.doCommand(dbc);
				vcs.setIdVCSI(null);
				JSONObject rv = new JSONObject();
				rv.put("filename", "none");
				return Response.ok().entity(rv.toString()).build();
			}
			
			if(!blackboard.getVideoSubCatImages().containsKey(ivcsi))
				return Response.serverError().entity("vcsi not found").build();
			VideoCategorySubImage vcsi = blackboard.getVideoSubCatImages().get(ivcsi);
			
			String dbc = "UPDATE videoCategorySub set idVCSI="+vcsi.getIdVCSI()+" where idvideo_category="+vcs.getIdvideo_category();
			dbCommand.doCommand(dbc);
			vcs.setIdVCSI(vcsi.getIdVCSI());
			
			JSONObject rv = new JSONObject();
			rv.put("filename", vcsi.getFilename());
			return Response.ok().entity(rv.toString()).build();
			
		} catch (IOException e) {
			return Response.serverError().entity("error "+e).build();
		}
		
	}
	
	@POST
	@Path("addNewVCS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewVCS(@Context HttpServletRequest req) {
			
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		try {
			JSONObject jo = GetJson.GetJsonObject(req);
			Integer vcm = jo.getInt("vcm");
			String name = jo.getString("name");
			String description = jo.getString("desc");
			
			VideoCategorySub vcs = new VideoCategorySub();
			vcs.setName(name);
			vcs.setDescription(description);
			vcs.setCatMain(vcm);
			Integer idVCS = dbVideoCategorySub.insertVideoCatSub(vcs);
			vcs.setIdvideo_category(idVCS);
			
			blackboard.getVideoSubcats().put(vcs.getIdvideo_category(), vcs);
			return Response.ok().entity(idVCS).build();
			
		} catch (IOException e) {
			return Response.serverError().entity("error "+e).build();
		}		
	}
	
	@POST
	@Path("changeSubcatDesc")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeSubcatDesc(@Context HttpServletRequest req) {
			
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		try {
			JSONObject jo = GetJson.GetJsonObject(req);
			Integer vcsId = jo.getInt("vscId");
			String description = jo.getString("desc");
			
			if(!blackboard.getVideoSubcats().containsKey(vcsId))
				return Response.serverError().entity("could not find vsc "+vcsId).build();
			VideoCategorySub vcs = blackboard.getVideoSubcats().get(vcsId);
			
			String dbc = "UPDATE videoCategorySub set description='"+description+"' where idvideo_category="+vcsId;
			dbCommand.doCommand(dbc);
			vcs.setDescription(description);

			blackboard.getVideoSubcats().put(vcs.getIdvideo_category(), vcs);
			JSONObject rv = new JSONObject();
			rv.put("status", "ok");
			return Response.ok().entity(rv.toString()).build();
			
		} catch (IOException e) {
			return Response.serverError().entity("error "+e).build();
		}		
	}
	
}
