package broadcast.web.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.UserMessageHelpers;
import broadcast.p2p.SessionUserClose;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.WebUserRoleTypesEnum;
import shared.data.webuser.WebUserRoleTypesEnum.WebUserRoleType;
import shared.db.DbCommand;
import shared.db.DbConnection;
import shared.db.DbWebUser;
import shared.db.DbWebUsersCheck;

@JsonAutoDetect
@Path("/admin/people")
public class ApiAdminPeople {

	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private DbConnection dbConnection;
	@Inject private DbCommand dbCommand;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbWebUsersCheck dbWebUsersCheck;
	@Inject private SessionUserClose sessionUserClose;
	@Inject private UserMessageHelpers userMessageHelpers;
	private static final Logger log = LoggerFactory.getLogger(ApiAdminPeople.class);
	
    @GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPeople(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
    	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();

    	TreeMap<Integer, OneWebUser> webUsers = dbWebUsersCheck.getCheckWebUsers();
    	JSONObject jWebUsers = new JSONObject();
    	for(OneWebUser owu : webUsers.values()) {
    		JSONObject jowu = new JSONObject();
    		jowu.put("username", owu.getUsername());
    		jowu.put("wuid", owu.getWuid());
    		jowu.put("dtAdded", owu.getDtAdded());
    		jowu.put("strEmails", owu.getStrEmails());
    		jowu.put("strRoles", owu.getStrRoles());
    		jWebUsers.put(owu.getWuid().toString(), jowu); 		
    	}

    	return Response.ok().entity(jWebUsers.toString()).build();  	
    }
    
    @GET
    @Path("/ban/{wuid}")
	@Produces(MediaType.TEXT_HTML)
	public Response ban(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
		log.info("ban "+wuid);
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	if(sessionUser.getWebUser().getWuid().equals(wuid))
    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("can't ban yourself").build();
    	
    	SessionUser target = blackboard.getSessionUserByWuid(wuid);
    	if(target!=null) {
        	// kill them if they have a websocket
        	if(target.getWebSocketConnection()!=null) {
        		userMessageHelpers.sendUserMessage(target, "error", "You have been banned by an admin");
        		Session session= (Session) target.getWebSocketConnection().getWebsocketSession();
        		sessionUserClose.doClose(session);
        	}
        	blackboard.getSessionUsers().remove(target.getMydetv());
        	// log them out
        	target.setWebUser(null);
    	}
    	
    	String dbc =  "update webUser set dtBanned=UTC_TIMESTAMP() where idWebUser="+wuid;
    	dbCommand.doCommand(dbc);
    	
    	return Response.ok().entity("ok").build();
    }
    @GET
    @Path("/unban/{wuid}")
	@Produces(MediaType.TEXT_HTML)
	public Response unban(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
		log.info("ban "+wuid);
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	if(sessionUser.getWebUser().getWuid().equals(wuid))
    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("can't unban yourself").build();
    	
    	SessionUser target = blackboard.getSessionUserByWuid(wuid);
    	if(target!=null && target.getWebUser()!=null)
    		target.getWebUser().setDtBanned(null);
    	
    	String dbc =  "update webUser set dtBanned=NULL where idWebUser="+wuid;
    	dbCommand.doCommand(dbc);
    	
    	return Response.ok().entity("ok").build();
    }
    
    @GET
    @Path("/unlock/{wuid}")
	@Produces(MediaType.TEXT_HTML)
	public Response unlock(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
		log.info("ban "+wuid);
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	if(sessionUser.getWebUser().getWuid().equals(wuid))
    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("can't unlock yourself").build();
    	
    	String dbc =  "update webUserFailed set dtInactive=UTC_TIMESTAMP() ";
    	dbc+="where idWebUser="+wuid+" and dtInactive is null";
    	dbCommand.doCommand(dbc);
    	
    	return Response.ok().entity("ok").build();
    }
    
    @GET
    @Path("/mute/{wuid}")
	@Produces(MediaType.TEXT_HTML)
	public Response mute(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
		log.info("ban "+wuid);
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	if(sessionUser.getWebUser().getWuid().equals(wuid))
    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("can't mute yourself").build();
    	
    	SessionUser target = blackboard.getSessionUserByWuid(wuid);
    	if(target!=null) {
        	if(target.getWebSocketConnection()!=null)
        		userMessageHelpers.sendUserMessage(target, "error", "You have been muted by an admin");	
        	if(target.getWebUser()!=null)
        		target.getWebUser().setDtMuted(LocalDateTime.now());
    	}
    	
    	String dbc =  "update webUser set dtMuted=UTC_TIMESTAMP() where idWebUser="+wuid;
    	dbCommand.doCommand(dbc);
    	
    	return Response.ok().entity("ok").build();
    }
    @GET
    @Path("/unmute/{wuid}")
	@Produces(MediaType.TEXT_HTML)
	public Response unmute(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
		log.info("ban "+wuid);
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	if(sessionUser.getWebUser().getWuid().equals(wuid))
    		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("can't unmute yourself").build();
    	
    	SessionUser target = blackboard.getSessionUserByWuid(wuid);
    	if(target!=null) {
    		if(target.getWebSocketConnection()!=null)
        		userMessageHelpers.sendUserMessage(target, "error", "You have been unmuted by an admin");	
        	if(target.getWebUser()!=null)
        		target.getWebUser().setDtMuted(null);
    	}
    	String dbc =  "update webUser set dtMuted=NULL where idWebUser="+wuid;
    	dbCommand.doCommand(dbc);
    	
    	return Response.ok().entity("ok").build();
    }
    
    @POST
    @Path("roles")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public Response testmaintRefresh(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.isPeopleAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
		JSONObject roles=null;
		try {
			roles = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
    	
		Integer wuid = roles.getInt("wuid");
		log.info("wuid:"+wuid);
		roles.remove("wuid");
		
		JSONObject allroles = new JSONObject();		
		for(WebUserRoleType rt : WebUserRoleTypesEnum.WebUserRoleType.values()) {			
			if(rt.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel())) {
				for(OneMydetvChannel channel : blackboard.getMydetvChannels().values()) {
					allroles.put(rt.getLevel().toString()+"-"+channel.getChannelId(), false);
				}
				
			}
			else
				allroles.put(rt.getLevel().toString(), false);
		}
		
    	for(String key : roles.keySet()) {
    		String srole = key.substring(5);
    		Boolean checked = roles.getBoolean(key);
    		log.trace("srole:"+srole+",checked:"+checked);
   			allroles.put(srole, checked);		
    	}
	
		log.debug("allroles:"+allroles.toString());
		
		// before we make the changes, check a few things for consistency
		// the browser checked these so no need to be nice about it, just return 'internal server error'
		if(allroles.getBoolean("0")) {
			Integer cnt=0;
			for(String key : allroles.keySet()) {
				if(allroles.getBoolean(key))
					cnt++;
			}
			if(cnt>1)
				return Response.serverError().entity("Error admin+").build(); 
		}
		
		if(allroles.getBoolean(WebUserRoleTypesEnum.WebUserRoleType.CHAT.getLevel().toString()) 
		&& allroles.getBoolean(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel().toString()))
			return Response.serverError().entity("Error chat+people").build();
		
		// now check that the user is allowed to change these
		// again, no need to be nice about it
		ArrayList<OneWebUserRole> newRoles = new ArrayList<OneWebUserRole>();
    	for(String key : allroles.keySet()) {
    		Boolean checked = allroles.getBoolean(key);
    		log.trace("checking srole:"+key+",checked:"+checked);

    		if(checked) {
    			OneWebUserRole owr = new OneWebUserRole();
    			owr.setWebUserId(wuid);
    			
        		if(key.length()>1) {
        			
        			Integer channel = Integer.parseInt(key.substring(2));
        			log.info("checking broadcast channel "+channel);
        			owr.setWebUserRoleType(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel());
        			owr.setDetails(channel.toString());
        			if(! authorized.isBroadcaster(sessionUser.getWebUser(), channel)) {
        				log.info("not authorized broadcaster"+sessionUser.getWebUser().getWuid()+" "+channel);
        				return Response.serverError().entity("not authorized").build();
        			}
        		}
        		else {
        			log.info("checking role "+key);
        			Integer role=Integer.parseInt(key);   
        			owr.setWebUserRoleType(role);
        			
        			   			
        			if(! authorized.isAuthorized(sessionUser.getWebUser(), role)) {
        				log.info("not authorized "+sessionUser.getWebUser().getWuid()+" "+role);
        				return Response.serverError().entity("not authorized").build();
        			}
        		}
        		newRoles.add(owr);
    		}
    	}
		
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			dbWebUser.rolesClearAll(conn, wuid);
			for(OneWebUserRole  owr : newRoles)
	        	dbWebUser.insertRole(conn, owr);
			
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		
    	return Response.ok().entity("ok").build();
    }

    
}
