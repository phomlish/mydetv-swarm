package broadcast.web.api;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import shared.data.broadcast.SessionUser;
import shared.db.DbLog;

@JsonAutoDetect
@Path("/dmca")
public class ApiDmca {

	@Inject private UserHelper userHelper;
	@Inject private DbLog dbLog;
	private static final Logger log = LoggerFactory.getLogger(ApiDmca.class);
	
	@POST
	@Path("submit")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest req) {
		
		SessionUser su = userHelper.getSessionUser(req);
		
		try {
			JSONObject jo = GetJson.GetJsonObject(req);
			jo.put("dt", LocalDateTime.now());
			String desc = jo.toString();
			if(su.getWebUser()!=null)
				dbLog.criticalWebUser(desc, su);
			else 
				dbLog.critical(desc);
			log.info("dmca:"+desc);
			return Response.ok().entity("ok").build();
		} catch(Exception e) {
			log.error("Error occured "+e);
			return Response.serverError().build();
		}		
	}
}
