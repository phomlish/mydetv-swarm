package broadcast.web.api;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import shared.data.Config;
import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/admin/simulate")
public class ApiAdminSimulate {

	private static final Logger log = LoggerFactory.getLogger(ApiAdminSimulate.class);
	@Inject private Blackboard blackboard;
	@Inject private Config sharedConfig;
	@Inject private ApiStatsHelpers apiStatsHelpers;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    //@Path("/channel/{channel}")
    @Path("/showAllPeople")
	public Response showAllPeople(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	JSONArray ja = new JSONArray();    	
    		
		for(SessionUser su : blackboard.getSessionUsers().values()) {
			// skip users not logged in
			if(su.getWebUser()==null)
				continue;
			
			JSONObject jo = new JSONObject();
			jo.put("username", su.getWebUser().getUsername());
			jo.put("wuid", su.getWebUser().getWuid());
			if(su.getMydetvChannel()==null)
				jo.put("channel", "none");
			else
				jo.put("channel", blackboard.getMydetvChannels().get(su.getMydetvChannel().getChannelId()).getName());
			
			int cntIceFailed=0;
			int cntSlow=0;			
			for(FaultyConnection fc :su.getFaultyConnections().values()) {
				if(fc.getReason().equals("iceFailed"))
					cntIceFailed++;
				else if(fc.getReason().equals("slow"))
					cntSlow++;
			}
			jo.put("iceFailed", cntIceFailed);
			jo.put("slow", cntSlow);
			
			ja.put(jo);
		}
    	return Response.ok().entity(ja.toString()).build();
    }
    
    @GET
    @Path("/lockCGBW")
	public Response lockCGBW(@Context HttpServletRequest req) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	return Response.ok().build();
    	
    }
    
    @GET
    @Path("/getUserFC/{wuid}")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getUserFC(@Context HttpServletRequest req, @PathParam("wuid") Integer wuid) {
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null) 
    		return Response.status(Status.UNAUTHORIZED).entity("not authenticated").header(HttpHeaders.WWW_AUTHENTICATE, "/") .build();
    	if(! authorized.hasAnyRole(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).entity("not authorized").build();
    	
    	
    	log.info("wuid:"+wuid);
    	JSONObject jo = new JSONObject();
    	JSONArray iceFailed = new JSONArray();
    	JSONArray slow = new JSONArray();
    	
    	jo.put("wuid", wuid);
    	SessionUser su = blackboard.getSessionUserByWuid(wuid);
    	if(su==null) {
    		jo.put("username", "unknown");
    	}
    	else {
    		jo.put("username", su.getWebUser().getUsername());
    		
    		for(FaultyConnection fc : su.getFaultyConnections().values()) {
    			JSONObject fcjo = new JSONObject();
    			fcjo.put("fromIsMediaPlayer", 	fc.isFromIsMediaPlayer());
    			fcjo.put("toIsMediaPlayer", 	fc.isToIsMediaPlayer());
    			fcjo.put("connectionType", 		fc.getConnectionType());
    			fcjo.put("dt", 					fc.getDt());
    			fcjo.put("fromIp", 				fc.getFromIp());
    			fcjo.put("toIp", 				fc.getToIp());
    			fcjo.put("fromWuid", 			fc.getFromWuid());
    			fcjo.put("nacks", 				fc.getNacks());
    			
    			if(fc.getReason().equals("iceFailed")) 
    				iceFailed.put(fcjo);
    			else if(fc.getReason().equals("iceFailed")) 
    				slow.put(fcjo);
    			else
    				log.error("didn't recognize reason "+fc.getReason());
    				
    		}
    	}
    	
    	
    	jo.put("iceFailed", iceFailed);
    	
    	
    	
    	jo.put("slow", slow);
    	return Response.ok().entity(jo.toString()).build();
    	
    }
	
}
