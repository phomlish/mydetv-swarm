package broadcast.web.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import shared.db.DbArchive;

// TODO: lock down to authorized users
@JsonAutoDetect
@Path("/archive")
public class ApiArchive {
	
	private static final Logger log = LoggerFactory.getLogger(ApiArchive.class);
	@Inject private DbArchive dbArchive;
	@Inject private Blackboard blackboard;
	
    @POST
    @Path("/insert")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insert(@Context HttpServletRequest req) {
    
    	log.info("test:"+blackboard.getCount());
    	try {
			JSONObject jo = GetJson.GetJsonObject(req);
			//String mydetv = jo.getString("mydetv");
			String url = jo.getString("url");
			String site=getSite(url);
			String url2 = jo.getString("url2");
			String fullUrl=url+"/"+getUrl2(url2);
			fullUrl = URLDecoder.decode(fullUrl, "UTF-8");
			String pretitle = jo.getString("pretitle");
			log.info("site:"+site+",fullUrl:"+fullUrl+",pretitle:"+pretitle);
			dbArchive.archiveInsert(site, fullUrl, -1, pretitle);
		} catch (IOException e) {
			log.error("exception:"+e);
			return Response.serverError().entity(e).build();
		}
    	
    	return Response.ok().entity("ok").build();
    }

    //https://archive.org/download/1931_Cartoon_Within_a_Cartoon_1
    private String getSite(String url) {
    	try {
	    	String regexString = "(.*?)(://)(.*?)($)";
	    	Pattern pattern = Pattern.compile(regexString,Pattern.CASE_INSENSITIVE);
	    	Matcher matcher = pattern.matcher(url);
	    	while (matcher.find()) {
	    		log.info(matcher.group(3));
	    		String[] pieces = matcher.group(3).split("/");
	    		log.info("piece:"+pieces[0]); 	
	    		return pieces[0];
	    	}
    	} catch(Exception ex) {
    		log.error("exception reading site "+ex);
    	}
    	return "";
    }
    //siteUrl:<a href="MakingEmMove.gif">MakingEmMove.gif</a>
    private String getUrl2(String url) {
    	try {
    		String newurl = URLDecoder.decode(url, "UTF-8");
	    	String regexString = "(<a hRef=\")(.*?)(>)(.*?)(</a>)";
	    	Pattern pattern = Pattern.compile(regexString,Pattern.CASE_INSENSITIVE);
	    	Matcher matcher = pattern.matcher(newurl);
	    	while (matcher.find()) {
	    		log.info(matcher.group(4));
	    		return matcher.group(4);
	    	}
    	}  catch(Exception ex) {
    		log.error("exception reading site "+ex);
    	}
    	return "";
    }
    
    
}
