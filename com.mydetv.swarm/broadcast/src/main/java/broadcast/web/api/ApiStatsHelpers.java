package broadcast.web.api;

import java.util.ArrayList;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.data.status.SEdge;
import broadcast.data.status.SGroup;
import broadcast.data.status.SMydetvChannel;
import broadcast.data.status.SNode;
import broadcast.data.status.SNodesEdges;
import broadcast.data.status.SStatus;
import broadcast.janus.JClient;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class ApiStatsHelpers {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ApiStatsHelpers.class);
	@Inject private Blackboard blackboard;
	
	
    void addChannels(SStatus myStatus) {
    	for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
    		if(! mydetvChannel.isActive())
    			continue;	
    		myStatus.getMydetvChannels().put(mydetvChannel.getChannelId(), new SMydetvChannel(mydetvChannel));
    	}
    }
    
    public SNodesEdges getNodesAndEdges(OneMydetvChannel mydetvChannel) {
    	JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
    	ConnectionGroup cg = jClient.getMe().getCgStream();
    	
    	SNodesEdges nodesEdges = new SNodesEdges();
    	Integer parent=1;
    	nodesEdges.setNodeCounter(1);
    	
    	// remote & studio always have a presenter 
    	if(mydetvChannel.getChannelType() == 0 || mydetvChannel.getChannelType() == 2) {
    		SessionUser presenter = jClient.getBroadcaster();
    		String presenterName = "unk";
    		if(mydetvChannel.getChannelType() == 0) {
    			if(jClient.isLive()) 
    				presenterName=presenter.log();
    			else
    				presenterName="sid";
    		}
    		else {
    			if(jClient.isLive()) 
    				presenterName="live";
    			else
    				presenterName="sid";
    		}
    		
    		SNode node0 = new SNode(parent, presenterName+" "+0, "broadcasting");
    		node0.setColor("hotpink");
    		nodesEdges.getNodes().add(node0);   		 		
    		parent++;
    		nodesEdges.incrementNodeCounter();
    		
    		SEdge edge = new SEdge(1, parent);
    		nodesEdges.getEdges().add(edge);
    	}


		SNode node = new SNode(parent, cg.getProvider().log()+" "+cg.getTier(), "channel");
		node.setColor("lightgreen");
		nodesEdges.getNodes().add(node);
    	nodesEdges = addNodesEdges(nodesEdges, cg, parent);
    	
    	nodesEdges.setGroups(createGroups());
    	return nodesEdges;
    }
    
    private SNodesEdges addNodesEdges(SNodesEdges nodesEdges, ConnectionGroup cgv, Integer parent) {
    	
    	for(RtcConnection rtc : cgv.getDownstreams()) {
    		SessionUser su = rtc.getToUser();
    		nodesEdges.incrementNodeCounter();
    		SNode node = new SNode(nodesEdges.getNodeCounter(), su.log()+" "+su.getCgStream().getTier(), "viewer");
    		if(su.isInBroadcastingRoom())
    			node.setGroup("broadcaster");
    		SEdge edge = new SEdge(parent, nodesEdges.getNodeCounter());
    		nodesEdges.getNodes().add(node);
    		nodesEdges.getEdges().add(edge);
    		
    		nodesEdges = addNodesEdges(nodesEdges, su.getCgStream(), nodesEdges.getNodeCounter());
    	}    	
    	return nodesEdges;
    }
    
    private ArrayList<SGroup> createGroups() {
    	ArrayList<SGroup> groups = new ArrayList<SGroup>();
    	SGroup sgroup;

    	sgroup = new SGroup("channel", "circle", "lightgreen");
    	groups.add(sgroup);
    	sgroup = new SGroup("viewer", "box", "Aquamarine");
    	groups.add(sgroup);
    	sgroup = new SGroup("slowlink", "box", "Lavender");
    	groups.add(sgroup);
    	sgroup = new SGroup("broadcaster", "ellipse", "yellow");
    	groups.add(sgroup);
    	sgroup = new SGroup("broadcasting", "ellipse", "hotpink");
    	groups.add(sgroup);

    	return groups;
    }
   
}
