package broadcast.web.api;

import static java.nio.file.StandardOpenOption.READ;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.VideosHelpers;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.OneVideoConverted;
import shared.data.broadcast.SessionUser;
import shared.db.DbVideo;

@JsonAutoDetect
@Path("/video")
public class ApiAdminVideo {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private Authenticated authenticated;
	@Inject private Authorized authorized;
	@Inject private DbVideo dbVideo;
	@Inject private VideosHelpers videosHelpers;
	private static final Logger log = LoggerFactory.getLogger(ApiAdminVideo.class);
	
    @POST
    @Path("/category")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response category(@Context HttpServletRequest req) {
    	
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
    	JSONObject item=null;
		try {
			item = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
		
		log.info("new category "+item);
		Integer pkey = item.getInt("pkey");
		if(! blackboard.getVideos().containsKey(pkey))
			return Response.serverError().entity("pkey "+pkey+" not found").build();
		OneVideo ov = blackboard.getVideos().get(pkey);
		
		Integer catId = item.getInt("catId");
		if(catId==0) {
			return Response.serverError().entity("invalid catId "+catId).build();
		}
		if(! blackboard.getVideoSubcats().containsKey(catId))
			return Response.serverError().entity("catId "+catId+" not found").build();
		
		
		dbVideo.updateVideoCategory(pkey, catId);
		ov.setCatSub(catId);
		
    	return Response.ok().entity("ok").build();
    }
    
    @POST
    @Path("/donotuse")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response donotuse(@Context HttpServletRequest req) {

    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		JSONObject item=null;
		try {
			item = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
		
		log.info("do not use "+item);
		Integer pkey = item.getInt("pkey");
		if(! blackboard.getVideos().containsKey(pkey))
			return Response.serverError().entity("pkey "+pkey+" not found").build();
		OneVideo ov = blackboard.getVideos().get(pkey);
		
		Boolean doNotUse = item.getBoolean("doNotUse");
		
		dbVideo.updateDoNotUse(pkey, doNotUse);
		ov.setDoNotUse(doNotUse);
		
    	return Response.ok().entity("ok").build();
    }
    
    @POST
    @Path("/title")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTitle(@Context HttpServletRequest req) {

    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(req);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		JSONObject item=null;
		try {
			item = GetJson.GetJsonObject(req);
		} catch (IOException e) {
			log.error("Error reading json");
			return Response.serverError().entity("Error reading json").build(); 			
		}
		
		log.info("title item "+item);
		String title = item.getString("title");
		Integer pkey = item.getInt("pkey");
		if(! blackboard.getVideos().containsKey(pkey))
			return Response.serverError().entity("pkey "+pkey+" not found").build();
		OneVideo ov = blackboard.getVideos().get(pkey);

		log.info("updating "+pkey+" title to "+title);
		dbVideo.updateTitle(pkey, title);
		ov.setTitle(title);
		
    	return Response.ok().entity("ok").build();
    }
    
    private static final int BUFFER_LENGTH = 1024 * 16;
	private static final long EXPIRE_TIME = 1000 * 60 * 60 * 24;
	private static final Pattern RANGE_PATTERN = Pattern.compile("bytes=(?<start>\\d*)-(?<end>\\d*)");
	  
	@GET
	@Path("/getVideo")
	public Response doGetVideo(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServletException, IOException
	{
		
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(request);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		String strPkey = URLDecoder.decode(request.getParameter("pkey"), "UTF-8");
		Integer pkey;
		try {
			pkey = Integer.decode(strPkey);
		} catch (NumberFormatException ex) {
			return Response.serverError().entity("not a valid pkey "+strPkey).build();
		}
		
		if(! blackboard.getVideos().containsKey(pkey)) 
			return Response.serverError().entity("can't find that video "+strPkey).build();
		
		OneVideo ov = blackboard.getVideos().get(pkey);
		
		OneVideoConverted ovc = videosHelpers.getVideoConverted(ov, "vp8-opus");
		if(ovc==null) 
			return Response.serverError().entity("can't find a converted video for "+strPkey).build();
		
		String vfn = videosHelpers.getVideoPath(ov, ovc);
		response = doMyGet(request, response, vfn);
		ResponseBuilder rb = Response.status(200);
		return rb.build();
	}
	@GET
	@Path("/getConverted")
	public Response doGetConverted(@Context HttpServletRequest request, @Context HttpServletResponse response) throws ServletException, IOException
	{
    	SessionUser sessionUser = authenticated.getFullyAuthenticatedSessionUser(request);
    	if(sessionUser == null)
    		return Response.status(Status.UNAUTHORIZED).build();
    	if(! authorized.isVideoAdmin(sessionUser.getWebUser()))
    		return Response.status(Status.FORBIDDEN).build();
    	
		String vfn = URLDecoder.decode(request.getParameter("video"), "UTF-8");
		log.info("vfn:"+vfn);
		response = doMyGet(request, response, vfn);
		ResponseBuilder rb = Response.status(200);
		return rb.build();
	}
	
	private HttpServletResponse doMyGet(HttpServletRequest request, HttpServletResponse response, String vfn) {
		try {
			
			java.nio.file.Path video = Paths.get(config.getVideosConvertedDirectory(),"/", vfn);
			log.info("sending:"+video.toString());
			int length = (int) Files.size(video);
			int start = 0;
			int end = length - 1;
	
			String range = request.getHeader("Range");
			log.info("range:"+range);
			if(range==null) {
				log.error("no range");
			}
			else {
			Matcher matcher = RANGE_PATTERN.matcher(range);
			
			if (matcher.matches()) {
				String startGroup = matcher.group("start");
				start = startGroup.isEmpty() ? start : Integer.valueOf(startGroup);
				start = start < 0 ? 0 : start;
	
				String endGroup = matcher.group("end");
				end = endGroup.isEmpty() ? end : Integer.valueOf(endGroup);
				end = end > length - 1 ? length - 1 : end;
			}
			}
			int contentLength = end - start + 1;

			response.reset();
			response.setBufferSize(BUFFER_LENGTH);
			response.setHeader("Content-Disposition", String.format("inline;filename=\"%s\"", vfn));
			response.setHeader("Accept-Ranges", "bytes");
			response.setDateHeader("Last-Modified", Files.getLastModifiedTime(video).toMillis());
			response.setDateHeader("Expires", System.currentTimeMillis() + EXPIRE_TIME);
			response.setContentType(Files.probeContentType(video));
			response.setHeader("Content-Range", String.format("bytes %s-%s/%s", start, end, length));
			response.setHeader("Content-Length", String.format("%s", contentLength));
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);


			int bytesRead;
			int bytesLeft = contentLength;
			ByteBuffer buffer = ByteBuffer.allocate(BUFFER_LENGTH);
	
			try (
				SeekableByteChannel input = Files.newByteChannel(video, READ);
				OutputStream output = response.getOutputStream()
				) {
	
					input.position(start);
	
					while ((bytesRead = input.read(buffer)) != -1 && bytesLeft > 0) {
						buffer.clear();
						output.write(buffer.array(), 0, bytesLeft < bytesRead ? bytesLeft : bytesRead);
						bytesLeft -= bytesRead;
					}
				}
			
		} catch (IOException e) {
			//log.error("Error "+e);
		}
		return response;
	}
}
