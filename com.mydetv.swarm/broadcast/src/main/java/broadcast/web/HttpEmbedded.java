package broadcast.web;

import java.io.File;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.servlet.GuiceFilter;

import broadcast.Blackboard;
import broadcast.web.jsp.JspGuiceServletContextListener;
import broadcast.web.ws.MyWsCreator;
import shared.data.Config;

@Singleton
public class HttpEmbedded extends ResourceConfig implements Runnable {

	@Inject private MyGuiceServletContextListener myGuiceServletContextListener;
	@Inject private JspGuiceServletContextListener jspGuiceServletContextListener;
	@Inject private MyWsCreator myWsCreator;
	@Inject private CustomErrorHandler customErrorHandler;
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	private static final Logger log = LoggerFactory.getLogger(HttpEmbedded.class);

	private Server server;
	File tmpDir;
	
	public void run() {

		try {		
			
			// first clear out any old temp stuff
			String tmpFPN = config.getTmpDir()+"/broadcast/web/tmp";
			new File(tmpFPN).delete();
			tmpFPN = config.getTmpDir()+"/broadcast/web";
			log.info("creating "+tmpFPN);
			tmpDir = new File(tmpFPN);
			
			// tell jetty to use log4j
			System.setProperty("org.eclipse.jetty.util.log.class","org.eclipse.jetty.util.log.Slf4jLog");	

			// tell jetty not to cache connections
			Resource.setDefaultUseCaches(false);

			// setup jetty's threadpool
			QueuedThreadPool threadPool = new QueuedThreadPool();
			threadPool.setMaxThreads(500);
			threadPool.setMinThreads(10);
			threadPool.setName("webThread-");

			// create our server
			server = new Server(threadPool);
			server.addBean(new ScheduledExecutorScheduler());

			HttpConfiguration http_config = new HttpConfiguration();
			http_config.setSecureScheme("https");
			http_config.setSecurePort(8080);
			http_config.setOutputBufferSize(32768);
			http_config.setRequestHeaderSize(8192);
			http_config.setResponseHeaderSize(8192);
			http_config.setSendServerVersion(false);
			http_config.setSendDateHeader(false);

			// setup our ssl connection factory 
			SslConnectionFactory sslConnectionFactory = new 
					SslConnectionFactory(blackboard.getSslContextFactory(), "http/1.1");

			// SSL HTTPS Configuration
			HttpConfiguration https_config = new HttpConfiguration(http_config);
			https_config.addCustomizer(new SecureRequestCustomizer());
			https_config.setSendServerVersion(false);

			// and finally, our SSL Connector
			ServerConnector sslConnector = new ServerConnector(server, sslConnectionFactory, new HttpConnectionFactory(https_config));
			sslConnector.setPort(config.getWebPort());
			server.addConnector(sslConnector);

			// This webapp will use jsps and jstl. We need to enable the
			// AnnotationConfiguration in order to correctly set up the jsp container

			Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
			classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration",
					"org.eclipse.jetty.plus.webapp.EnvConfiguration",
					"org.eclipse.jetty.plus.webapp.PlusConfiguration");
			classlist.addBefore(
					"org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
					"org.eclipse.jetty.annotations.AnnotationConfiguration");
			log.info("rootDir:"+config.getRootDir());
			HandlerList handlerList = new HandlerList();
			handlerList.addHandler(buildApiContext());		
			handlerList.addHandler(buildAdminContext());
			handlerList.addHandler(buildAuthContext());
			//handlerList.addHandler(buildJspContext());
			handlerList.addHandler(buildWebSockets());
			handlerList.addHandler(buildExternalWebContext());
			handlerList.addHandler(buildExternalVideoContext());
			handlerList.addHandler(buildStaticWebContext());
			// slickgrid has no way to set it's images directory so we make a handler here
			handlerList.addHandler(buildImagesContext());
			// and then add home controller to pick up / as the homepage
			handlerList.addHandler(buildHomeContext());

			server.setHandler(handlerList);

			server.start();
			log.info("jetty started");		      
			server.join();

		} catch (Exception e) {
			log.error("exception:"+e);
			System.exit(0);
		}		
	}

	private ContextHandler buildHomeContext() {	
		String home = "broadcast.web.home";
		String path = "/";
		final ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setContextPath(path);
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages(home);
		resourceConfig.register(new GuiceFeature(myGuiceServletContextListener.getInjector()));
		ServletContainer servletContainer = new ServletContainer(resourceConfig);
		ServletHolder servletHolder = new ServletHolder("classes", servletContainer);
		servletContextHandler.addServlet(servletHolder, "/*");
		servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST)); 
		servletContextHandler.addEventListener(myGuiceServletContextListener);			
		servletContextHandler.setErrorHandler(customErrorHandler);		
		return servletContextHandler;
	}
	private ContextHandler buildAdminContext() {	
		String admin = "broadcast.web.admin";
		String path = "/admin";
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages(admin);
		resourceConfig.register(new GuiceFeature(myGuiceServletContextListener.getInjector()));
		ServletContainer servletContainer = new ServletContainer(resourceConfig);		
		final ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setContextPath(path);
		ServletHolder servletHolder = new ServletHolder(servletContainer);	
		servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST)); 
		servletContextHandler.addEventListener(myGuiceServletContextListener);			
		servletContextHandler.addServlet(servletHolder, "/*");
		servletContextHandler.setErrorHandler(customErrorHandler);
		return servletContextHandler;
	}

	private ContextHandler buildApiContext() {
		String api = "broadcast.web.api";
		String path = "/api";	
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages(api, MultiPartFeature.class.getName());
		resourceConfig.register(MultiPartFeature.class);
		resourceConfig.register(new GuiceFeature(myGuiceServletContextListener.getInjector()));

		ServletContainer servletContainer = new ServletContainer(resourceConfig);		
		final ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setContextPath(path);

		ServletHolder servletHolder = new ServletHolder(servletContainer);	
		servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST)); 
		servletContextHandler.addEventListener(myGuiceServletContextListener);			
		servletContextHandler.addServlet(servletHolder, "/*");
		return servletContextHandler;
	}

	private ContextHandler buildWebSockets()
	{		
		String path = "/ws";
		ContextHandler contextHandler = new ContextHandler(path);
		contextHandler.setAllowNullPathInfo(true);

		WebSocketHandler wsHandler = new WebSocketHandler() {
			@Override
			public void configure(WebSocketServletFactory factory) {
				factory.setCreator(myWsCreator);
			}
		};
		contextHandler.setHandler(wsHandler);
		return contextHandler;
	}

	private ContextHandler buildAuthContext() {
		String auth = "broadcast.web.auth";
		String path = "/auth";
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages(auth);
		resourceConfig.register(new GuiceFeature(myGuiceServletContextListener.getInjector()));

		ServletContainer servletContainer = new ServletContainer(resourceConfig);		
		final ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setContextPath(path);
		ServletHolder servletHolder = new ServletHolder(servletContainer);	
		servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST)); 
		servletContextHandler.addEventListener(myGuiceServletContextListener);			
		servletContextHandler.addServlet(servletHolder, "/*");
		servletContextHandler.setErrorHandler(customErrorHandler);
		return servletContextHandler;
	}

	@SuppressWarnings("unused")
	private ContextHandler buildJspContext() {
		String jsp = "broadcast.web.jsp";
		String path = "/jsp";
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages(
				jsp, 
				JacksonFeature.class.getName(),
				"com.fasterxml.jackson.jaxrs.json.annotation",
				"com.fasterxml.jackson.jaxrs.json",
				"com.fasterxml.jackson.jaxrs",
				"com.fasterxml.jackson");
		resourceConfig.register(new GuiceFeature(jspGuiceServletContextListener.getInjector()));

		//ServletContainer servletContainer = new ServletContainer(resourceConfig);		
		final ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContextHandler.setContextPath(path);
		servletContextHandler.setErrorHandler(customErrorHandler);
		return servletContextHandler;
	}

	private WebAppContext buildStaticWebContext() throws ConfigurationException {
		String s = "/s";
		String path =  "/web";
		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath(s);
		webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.maxCachedFiles", "0");
		webAppContext.setResourceBase(config.getRootDir() + path);
		webAppContext.setDescriptor("/WEB-INF/web.xml");
		webAppContext.setTempDirectory(tmpDir);
		webAppContext.setErrorHandler(customErrorHandler);
		if(config.getInstance().equals("prod"))
			webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");

		return webAppContext;
	}

	private WebAppContext buildExternalWebContext() throws ConfigurationException {
		String s = "/ext";
		String path = config.getExternalWebDirectory();
		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath(s);
		webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.maxCachedFiles", "0");
		webAppContext.setResourceBase(path);
		webAppContext.setTempDirectory(tmpDir);
		webAppContext.setErrorHandler(customErrorHandler);
		if(config.getInstance().equals("prod"))
			webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");

		return webAppContext;
	}
	private WebAppContext buildExternalVideoContext() throws ConfigurationException {
		String s = "/v";
		String path = config.getVideosDirectory();
		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath(s);
		webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.maxCachedFiles", "0");
		webAppContext.setResourceBase(path);
		webAppContext.setTempDirectory(tmpDir);
		webAppContext.setErrorHandler(customErrorHandler);
		if(config.getInstance().equals("prod"))
			webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");

		return webAppContext;
	}

	private WebAppContext buildImagesContext() throws ConfigurationException {

		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setContextPath("/images");
		webAppContext.setResourceBase(config.getRootDir()+"/web/images");
		webAppContext.setTempDirectory(tmpDir);
		webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.maxCachedFiles", "0");
		
		if(config.getInstance().equals("prod"))
			webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
		return webAppContext;
	}

	public void shutdown() throws Exception {
		server.stop();
	}
}
