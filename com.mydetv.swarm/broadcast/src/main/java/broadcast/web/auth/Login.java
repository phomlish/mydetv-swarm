package broadcast.web.auth;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.auth.Validations;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/")
public class Login {
	private static final Logger log = LoggerFactory.getLogger(Login.class);
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Validations validations;

	@GET
	public Response login(@Context HttpServletRequest req, @Context HttpServletResponse resp) 
	{
		// this is the only 'page' that needs to check for the cookie and redirect if not found
		if(!userHelper.isMydetvCookieKnown(req)) {
			log.info("no cookie, I'll create one but they will be redirected to apply the cookie");
			SessionUser sessionUser = userHelper.getSessionUser(req);
			return(userHelper.redirectToPage(sessionUser, "/auth?reload=1"));
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(sessionUser.isAuthenticated()) {
			log.info("already signed in");
			if(sessionUser.isLoggedIn())
				return Response.seeOther(UriBuilder.fromUri("/auth/change-password").build()).build();
			return Response.seeOther(UriBuilder.fromUri("/").build()).build();
		}
		
		if(validations.isSystemLoginLockedCurrently()) 
			return validations.showLoginLockedPage(sessionUser);
		
		String rv = webHelper.beginHtml(sessionUser, "Login");
		rv+="<script type='text/javascript'>loginInit();</script>";

		rv+="<div class='container col-lg-12'>"; // login container
		
		rv+="<div class='row'>"; // login message row		
		rv+="<div class='col-sm-2'></div>"; // spacer column
		rv+="<div class='col-sm-8'>"; // login message column
		rv+="<div class='center redText' id='loginMessage'></div>";
		rv+="</div>"; // login message column
		rv+="</div>"; // login message row
		
		// login
		rv+="<div class='row'>"; // login row
		rv+="<div class='col-sm-2'></div>"; // spacer column
		rv+="<div class='col-sm-8 mbpLightBg'>"; // login column
		
		rv+="<form name='loginform' class='form-horizontal mbpLightBg' align='center' onsubmit='return submitlogin();'>";
		
		// email
		rv+="<div class='form-group row form-group-top-row' style='margin-top:1%'>";
		rv+="<label for='email' class='col-form-label col-sm-2' >Email</label>";
		rv+="<div class='col-sm-8'>";
		rv+="<input type='text' id='email' name='email' onchange='vemail()' size='80' required='true' class='form-control' />";
		rv+="</div>";
		rv+="<div class='col-sm-1' id='reqemail'>Required</div>";
		rv+="</div>"; // form-group

		// password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >Password</label>";
		rv+="<div class='col-sm-8'>";
		rv+="<input type='password' id='password' name='password' onchange='vpassword()' required='true' class='form-control' />";
		rv+="</div>";
		rv+="<div class='col-sm-1' id='reqpassword'>Required</div>";
		rv+="</div>"; // form-group
		
		rv+="<div class='form-group row'>";
		rv+="<div class='col-sm-3'></div>";
        rv+="<div class='col-sm-2 center'>";  
        rv+="<input type='submit' name='signin' id='signin' value='sign in' class='btn btn-primary'/>";
        
        rv+="</div>"; // col
		rv+="<div class='col-sm-2 center'><div id='spinnerDiv'></div></div>";
		//rv+="<div class='col-sm-1'></div>";
        rv+="<div class='col-sm-2 center '>";  
        rv+="<input type='button' name='forgot' id='forgot' value='forgot password' class='btn btn-primary' onclick=\"location.href='/auth/forgot-password';\"/>";
        rv+="</div>"; // col
        rv+="</div>"; // form-group

		rv+="</form>";
		rv+="</div></div>"; // login column/row
		
		rv+="<div class='row spacer'>"; //spacer row
		rv+="<div class='col-sm-3'></div>"; 
		rv+="<div class='col-sm-6'></div>"; 
		rv+="</div>";
		
		// register
		rv+="<div class='row'>"; //register row
		rv+="<div class='col-sm-2'></div>"; // spacer column
		rv+="<div class='col-sm-8 mbpLightBg'>"; // register column
		rv+="<form name='registerform' action='/auth/register' class='form-horizontal ' role='form' align='center'>";
		
		rv+="<div class='row register-row'>";
        rv+="<div class='col-sm-12 center '>";  
        rv+="<input type='submit' name='register' id='register' value='register' class='btn btn-primary' />";
        rv+="</div>"; // col
        rv+="</div>"; // register-form-group
        
		rv+="</div>"; // register column
		rv+="</div>"; // register row
		
		rv+="</div>"; // login container
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
			
		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}
	
}
