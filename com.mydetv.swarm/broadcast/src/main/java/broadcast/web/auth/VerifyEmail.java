package broadcast.web.auth;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUserEmail;
import shared.db.DbConnection;
import shared.db.DbWebUser;

@JsonAutoDetect
@Path("verify-email")
public class VerifyEmail {

	private static final Logger log = LoggerFactory.getLogger(VerifyEmail.class);
	@Inject private WebHelper webHelper;
	@Inject private UserHelper userHelper;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbConnection dbConnection;

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response doPost(@Context HttpServletRequest req, @Context UriInfo uri) {

		MultivaluedMap<String, String> queryParams = uri.getQueryParameters();
		for (String k:queryParams.keySet()) 
			log.info(k+" "+queryParams.get(k));
		

		SessionUser sessionUser = userHelper.getSessionUser(req);
		String rv = webHelper.beginHtml(sessionUser, "Verify Email");
		
		Integer w = Integer.decode(queryParams.getFirst("w"));
		Integer e = Integer.decode(queryParams.getFirst("e"));
		String c = queryParams.getFirst("c");
		String u = queryParams.getFirst("u");
		String result = processFromEmail(sessionUser, w, e, c, u);
		rv+=processResult(sessionUser, result);
		
		rv+="</div>";
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		log.info("ok");

		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}

	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
		log.info("get");

		Map<String, String[]> map = req.getParameterMap();

		SessionUser sessionUser = userHelper.getSessionUser(req);
		String rv = webHelper.beginHtml(sessionUser, "Verify Email");
		if(map.size()==4) {
			String result = processFromEmail(sessionUser, req);
			rv+=processResult(sessionUser, result);
		}
		else 
			rv+=processSessionUser(sessionUser);

		rv+="</div>";
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		log.info("ok");

		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}

	private String processResult(SessionUser sessionUser, String result) {
		String rv="";
		if(result.equals("ok")) {
			rv+="Your code was accepted<br>";

			if(sessionUser.isAuthenticated())
				rv+="Please go Home to update your pages.<br>";
			else
				rv+="Please go Home and login.<br>";

		}
		else {
			rv+="Your code was rejected ("+result+")<br>";
			rv+="Please go Home";
			if(!sessionUser.isAuthenticated())
				rv+=", login";
			rv+=" and try again.<br>";
		}
		return rv;
		
	}
	private String processSessionUser(SessionUser sessionUser) {

		String rv="";

		rv+="<div class='row'><div class='col-lg-12 center'>";
		rv+="Verify Email<br>\n";

		rv+="You should have received an email with a link to verify your email.<br>";
		rv+="Please check your email and click that link.<br>";
		rv+="Check your email's span/junk folder if you didn't receive the email.<br>";
		rv+="</div></div>";  // top instructions

		rv+="<div class='row spacer-row'>&nbsp</div>";

		// send new code
		rv+="<div class='row'>"; // new code row
		rv+="<div class='col-sm-4'></div>"; // spacer column
		rv+="<div class='col-sm-4 mbpLightBg'>"; // new code column

		rv+="<div class='row'><div class='col-sm-12 center'>";
		rv+="If you need a new code you can have one emailed to you here.";
		rv+="</div></div>";

		// email
		rv+="<input type='text' id='emailnc' name='emailnc' size='40' class='form-control d-none'";
		rv+=" value='"+sessionUser.getWebUser().getLoggedInEmail().getEmail()+"'/>";

		rv+="<div class='row'>"; // submit button
		rv+="<div class='col-sm-12 center'>";
		rv+="<input type='button' name='sendcode' id='sendcode' value='Email New Code' onclick='sendnewcode();' class='btn btn-primary' />";
		rv+="</div></div>"; // submit button

		rv+="<div class='row spacer-row'>&nbsp</div>";

		rv+="</div></div>"; // new code row/column

		return rv;
	}

	private String processFromEmail(SessionUser sessionUser, HttpServletRequest req) {

		try {

			Integer w = Integer.decode(req.getParameter("w"));
			Integer e = Integer.decode(req.getParameter("e"));
			String c = req.getParameter("c");
			String u = req.getParameter("u");
			log.info("eid:"+e+",wuid:"+w+",code:"+c+",uuid:"+u);
			return(processFromEmail(sessionUser, w, e, c, u));
		} catch(NumberFormatException e) {
			log.error("e:"+e);
			return "system error";
		}
	}

	private String processFromEmail(SessionUser sessionUser, Integer w, Integer e, String c, String u) {
		if(c.length()!=8) 
			return("invalid code");
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			OneWebUserEmail email = dbWebUser.getEmailById(conn, e);
			if(email==null)
				return("Unable to find that email");

			if(email.getEmail()==null || email.getEmail()=="")
				return("Unable to find that email");

			if(email.getVerificationCode()==null || email.getVerificationCode().equals(""))
				return("already verified");

			if(email.getDtVerified()!=null) {
				log.info("already verified on "+email.getDtVcSent().toString());
				return("already verified");
			}

			if(email.getUuid()==null || !email.getUuid().equals(u)) {
				log.info("uuid does not match "+u+" vs db "+email.getUuid());
				return("Unable to verify. Did you request a new link and accidentally clicked the old one?");
			}

			if(email.getVerificationCode()==null || !email.getVerificationCode().equals(c)) {
				log.info("code does not match "+c+" vs db "+email.getVerificationCode());
				return("unable to verify");
			}

			LocalDateTime now = LocalDateTime.now();
			//LocalDateTime before = now.minusHours(24);
			LocalDateTime before = now.minusMinutes(2);
			if(email.getDtVcSent().isBefore(before)) {
				log.info("code expired "+email.getDtVcSent().toString());
				return("code expired");
			}

			// all checks passed. update db			
			dbWebUser.updateEmailVerified(email.getIdEmail());
			email.setDtVerified(now);
			email.setVerificationCode("");
			email.setUuid("");

			if(sessionUser.isAuthenticated())
				sessionUser.getWebUser().setLoggedInEmail(email);
			dbConnection.tryClose(conn);
			return("ok");
		} catch(SQLException ex) {
			log.error("e:"+ex);
			return "system error";
		}


	}
}
