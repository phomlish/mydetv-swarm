package broadcast.web.auth;

import java.time.ZoneId;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.auth.Birthday;
import broadcast.auth.Validations;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.BrowserThemes;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUserImage;

@Path("/profile")
public class Profile {

	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private Validations validations;
	private static final Logger log = LoggerFactory.getLogger(Profile.class);
	
	 @GET
		public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp)
		{
			if(!userHelper.isMydetvCookieKnown(req)) {
				log.info("no cookie, I'll create one but they will be redirected to apply the cookie");
				SessionUser sessionUser = userHelper.getSessionUser(req);
				return(userHelper.redirectToPage(sessionUser, "/auth/register?reload=1"));
			}
			
			SessionUser sessionUser = userHelper.getSessionUser(req);
			if(! sessionUser.isAuthenticated()) {
				log.info("not signed in, redirecting to /");
				return Response.seeOther(UriBuilder.fromUri("/").build()).build();
			}
			
			if(validations.isSystemLoginLockedCurrently()) 
				return validations.showLoginLockedPage(sessionUser);
			
			String eh="";
			eh+="<link rel='stylesheet' href='/s/node_modules/croppie/croppie.css' >\n";
			eh+="<script src='/s/node_modules/croppie/croppie.min.js'></script>\n";
			String rv = webHelper.beginHtml(sessionUser, "Profile", "", eh);
			rv+="<script type='text/javascript'>profileInit();</script>";
			
			rv+="<div class='row'>";
			rv+="<div class='col-lg-12 center redText' id='profileMessage'>";
			/* maybe someday we'll be nicer about session expired
			String reload = req.getParameter("reload");
			log.info("reload:"+reload);
			if(reload != null && ! reload.equals("")) {
				rv+="Your session expired, page";
			}
			*/
			rv+="</div>"; // col
			rv+="</div>"; // row
			
			
			// update profile ***
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			rv+="<div class='row' id='updateProfileRow'>";
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>";  // profile column
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Update Your Profile ";
			rv+=WebHelper.ProfileHelp();
			rv+="</div></div>";
			
			rv+=getProfileForm(sessionUser);
			
			rv+="</div>"; // // profile column
			rv+="</div>"; // updateProfileRow 
					
			// profile images ********
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			rv+="<div class='row'>"; // images row
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>"; // images column
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Images Control ";
			rv+=WebHelper.ImagesHelp();
			rv+="</div></div>";
					
			rv+="<div id='imagesDiv'>";
			rv+=GetImagesDiv(sessionUser);
			rv+="</div>\n";
			
			rv+="</div>"; // images column		
			rv+="</div>"; // images row
			// end profile images ********
			
			rv+="<div id='imageDialog' title='crop/resize/rotate'><div id='croppieDiv'></div></div>";

			rv+="</div>"; //container
			
			rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
			ResponseBuilder rb = Response.status(Response.Status.OK);
			rb = userHelper.addHeaders(sessionUser, rb);
			rb.entity(rv);		
			return rb.build();
		}
	
		private String getProfileForm(SessionUser sessionUser) {
			String rv="";
			//rv+="<form id='profileForm' name='profileForm' align='center' class='form-horizontal' autocomplete='off'>\n";
			rv+=Birthday.BdayForm(sessionUser.getWebUser().getBirthday());
			
			// homepage
			rv+="<div class='form-group row form-group-top-row '>";
			rv+="<label class='col-form-label col-sm-2' for='link'>Home Page</label>";
			rv+="<div class='col-sm-7'>";
			rv+="<input type='text' id='profilelink' name='profilelink' onchange='changedProfile()' size='120' ";
			rv+="  placeholder='https://yourwebsite' class='form-control' value='";
			if(sessionUser.getWebUser().getLink()!=null)
				rv+=sessionUser.getWebUser().getLink();
			rv+="'/>";
			rv+="</div>";
			rv+="<div class='col-sm-3 center text-danger' id='lMessage'></div>";
			rv+="</div>"; // form-group
			
			// timezone
			rv+="<div class='form-group row'>";
			rv+="<label class='col-form-label col-sm-2' for='timezone'>Timezone</label>";
			rv+="<div class='col-sm-4'>";
			rv+="<select id='tz' name='tz' onchange='changedProfile();'>"+getTimezones(sessionUser.getWebUser().getUserTimezone())+"</select>\n";
			rv+="</div>";
			rv+="<div class='col-sm-3 center text-danger' id='tMessage'></div>";
			rv+="</div>"; // form-group
	
			// theme
			rv+="<div class='form-group row'>";
			rv+="<label class='col-form-label col-sm-2' for='theme'>Theme</label>";
			rv+="<div class='col-sm-4'>";
			rv+="<select id='theme' name='theme' onchange='changeTheme();' autocomplete='off'>";
			String currentTheme = sessionUser.getTheme();
			
			for(String theme : BrowserThemes.getBrowserThemes()) {
				rv+="<Option name='"+theme+"' value='"+theme+"'";
				if(theme.equals(currentTheme))
					rv+=" selected ";
				rv+=">"+theme+"</Option>";
			}
			rv+="</select>\n";
			rv+="</div>";
			rv+="<div class='col-sm-3 center text-danger' id='tMessage'></div>";
			rv+="</div>"; // form-group
			
			rv+="<div class='form-group row'>";
			rv+="<div class='col-sm-12 center'>";  
			rv+="<input type='button' name='updateProfile' id='updateProfile' value='update' onclick='updateUserProfile();' disabled class='btn btn-primary form-group' />";
			rv+="</div>"; // col
			rv+="</div>"; // form-group

			//rv+="</form>";
			return rv;
		}
		
		public static String GetImagesDiv(SessionUser sessionUser) {
			String rv="";
			rv+="<div class='row pirow' id='peopleRow0'>"; // one image row
			rv+="<div class='col-sm-1'></div>";
			int i=0;
			for(OneWebUserImage o :sessionUser.getWebUser().getImages().values()) {
				String selected="";
				if(o.getImageSelected())
					selected = " personSelected ";
				
				rv+=NewRow(i);
				//byte[] img = o.getOrigFile();
				rv+="<div class='col-sm-2' title='"+o.getOrigFilename()+"&#13;click to delete/select' id='personImgDiv-"+i+"'>";
				rv+="<a class='dropdown-toggle profilePictureDropdown' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
				rv+="<img class='person"+selected+"'";
				rv+=" id='personImg-"+o.getIdWebUserImage()+"'";
				String url = "/ext/people/"+sessionUser.getWebUser().getWuid()+"/"+o.getNewFn();
				rv+=" src='"+url+"'>";
				//rv+=" src='data:image/jpg;base64, "+Base64.encodeAsString(img)+"'>";
				rv+="</a>";
				
				rv+="<div class='dropdown-menu' aria-labelledby='profilePictureDropdown'>";
				String myid = ""+i+"-"+o.getIdWebUserImage();
				rv+="<a class='dropdown-item' id='dpi-"+myid+"' href='javascript:;' onclick='editProfileImage(this);'>Delete</a>";
				rv+="<a class='dropdown-item' id='spi-"+myid+"' href='javascript:;' onclick='editProfileImage(this);'>Select</a>";
				rv+="</div>"; // dropdown-menu
				rv+="</div>"; // personImgDiv
				i++;
			}
			for(; i<10; i++) {
				rv+=NewRow(i);
				rv+="<div class='col-sm-2 imageAvailable' id='personImgDiv-"+i+"'>";
				rv+="<a class='dropdown-toggle profilePictureDropdown' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
				rv+="<img class='person' src='/s/people/blank-profile-picture.png'></a>";
				rv+="<div class='dropdown-menu' aria-labelledby='profilePictureDropdown'>";
				rv+="<a class='dropdown-item' href='javascript:;' onclick='clickFile();'>Upload</a>";
				rv+="</div>";				
				rv+="</div>";
			}			
			rv+="</div>"; // one image row
			
			// upload button
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";	
			rv+="<div class='row'><div class='col-sm-12'>";  // upload row/column
			
			rv+="<input type='file' id='fileElem' style='display:none' onchange='checkImage();'>";
			rv+="<div class='center'>";  // button
			rv+="<input type='button' name='fileSelect' id='fileSelect' value='Upload New Image' onclick='clickFile();'";
			rv+=" class='btn btn-primary";
			if(sessionUser.getWebUser().getImages().size()>=10) 
				rv+=" disabled ";
			rv+="' />";
			rv+="</div>"; // button
			
			rv+="</div></div>"; // upload row/column
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			return rv;
		}
		private static String NewRow(int i) {
			if(i==5) {
				String rv="";
				rv+="</div>"; //row
				rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
				rv+="<div class='row pirow' id='peopleRow1'>"; // the images row
				rv+="<div class='col-sm-1'></div>";
				return rv;
			}
			return "";
		}
		private String getTimezones(String myzone) {
			if(myzone==null || myzone.equals("")) 
				myzone="UTC";
			
			String rv = "";
			
			ArrayList<String> zoneList = new ArrayList<String>(ZoneId.getAvailableZoneIds());
			zoneList.sort(String.CASE_INSENSITIVE_ORDER);
			// make UTC the top guy
			rv+="<option value='UTC'";
			if(myzone.equals("UTC"))
				rv+=" selected";
			rv+=">UTC</option>\n";
			
			// add everything except Etc and UTC	
			for(String zone : zoneList) {
				if(zone.startsWith("Etc"))
					continue;
				if(ignoreZone(zone))
					continue;
				rv+="<option value='"+zone+"'";
				if(myzone.equals(zone))
					rv+=" selected";
				rv+=">"+zone+"</option>\n";
			}
			
			// add in the Etc
			// add everything except Etc and UTC	
			for(String zone : zoneList) {
				if(! zone.startsWith("Etc"))
					continue;
				if(ignoreZone(zone))
					continue;
				rv+="<option value='"+zone+"'";
				if(myzone.equals(zone))
					rv+=" selected";
				rv+=">"+zone+"</option>\n";
			}			
			return rv;	
		}
		
		private Boolean ignoreZone(String zone) {
			if(zone.startsWith("System"))
				return true;
			if(zone.equals("UTC"))
				return true;
			if(zone.equals("CET"))
				return true;
			if(zone.equals("EET"))
				return true;
			if(zone.equals("EST5EDT"))
				return true;
			if(zone.equals("GB"))
				return true;
			if(zone.equals("GB-Eire"))
				return true;
			if(zone.equals("GMT0"))
				return true;
			if(zone.equals("PST8PDT"))
				return true;
			
			
			if(zone.equals("UCT"))
				return true;
			
			
			return false;
		}
	    
		class Update {
			private String mydetv;
			private String name;
			private String value;
			public String getMydetv() {
				return mydetv;
			}
			public void setMydetv(String mydetv) {
				this.mydetv = mydetv;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public String getValue() {
				return value;
			}
			public void setValue(String value) {
				this.value = value;
			}
		}
}
