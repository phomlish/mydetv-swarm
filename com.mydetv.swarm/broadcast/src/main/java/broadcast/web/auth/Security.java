package broadcast.web.auth;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.auth.Validations;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.SQWebUser;
import shared.data.webuser.SecurityQuestion;
import shared.db.DbConnection;
import shared.db.DbWebUser;

@Path("/security")
public class Security {
	
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private DbWebUser dbWebUser;
	@Inject private Validations validations;
	@Inject private DbConnection dbConnection;
	
	private static final Logger log = LoggerFactory.getLogger(Security.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp)
	{
		if(!userHelper.isMydetvCookieKnown(req)) {
			log.info("no cookie, I'll create one but they will be redirected to apply the cookie");
			SessionUser sessionUser = userHelper.getSessionUser(req);
			return(userHelper.redirectToPage(sessionUser, "/auth/security?reload=1"));
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			log.info("not signed in, redirecting to /");
			return Response.seeOther(UriBuilder.fromUri("/").build()).build();
		}
	
		if(validations.isSystemLoginLockedCurrently()) 
			return validations.showLoginLockedPage(sessionUser);
		
		String rv = webHelper.beginHtml(sessionUser, "Security");
			
		if(! validations.isReAuthenticated(sessionUser))
			rv+=getReAuthenticate(sessionUser);
		else {
			sessionUser.getWebUser().setDtReAuthenticated(LocalDateTime.now());  // remember they are still re-authenticated
			rv+=getFullForm(sessionUser);
		}
		
		rv+="</div>"; //container
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
	
		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}
	
	private String getReAuthenticate(SessionUser sessionUser) {
		String rv="";
		
		rv+="<div class='row'>";  // password row
		rv+="<div class='col-sm-1'></div>";			
		rv+="<div class='col-sm-10 mbpLightBg'>"; // password column
		
		rv+="<script type='text/javascript'>reauthInit();</script>";
		
		rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
		
		rv+="<div class='row'>";
		rv+="<div class='col-lg-12 center'>";
		rv+="Please enter your password to edit your security settings";
		rv+="</div></div>";

		rv+="<input type='password' id='password' name='password' style='display:none''>";
		
		rv+="<div class='form-group row form-group-top-row '>";
		rv+="<label class='col-form-label col-sm-2' for='link'>Password</label>";
		rv+="<div class='col-sm-7'>";
		rv+="<input type='password' id='newPassword0' name='newPassword0' size='80' ";
		rv+=" onblur='reenterPassword()' class='form-control'>";
		rv+="</div>";
		rv+="<div class='col-sm-3 center text-danger' id='pMessage'></div>";
		rv+="</div>"; // form-group
		
		rv+="<div class='form-group row'>";
		rv+="<div class='col-sm-4'></div>";
		rv+="<div class='col-sm-4 center'>";  
		rv+="<input type='button' name='btnEnterPassword' id='btnEnterPassword' value='submit' onclick='doReenterPassword();' disabled class='btn btn-primary form-group' />";
		rv+="</div>"; // col
		rv+="<div class='col-sm-2 center'><div id='spinnerDiv'></div></div>";
		rv+="</div>"; // form-group
		
		rv+="</div>"; // password column
		rv+="</div>"; // password row
		
		return rv;
	}
	
	private String getFullForm(SessionUser sessionUser) {
		Connection conn = null;
		String rv="";
		try {
			conn = dbConnection.getConnection();

			rv+="<script type='text/javascript'>securityInit();</script>";
			rv+="<div class='row'>";
			rv+="<div class='col-lg-12 center redText' id='securityMessage'>";
	
			rv+="</div>"; // col
			rv+="</div>"; // row
			
			// password ********
			rv+="<div class='row'>";  // password row
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>"; // password column
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Change Password ";
			rv+=WebHelper.PasswordHelp();
			rv+="</div></div>";
			
			rv+="<div class='row'>";
			rv+="<div class='col-sm-12 center text-danger' id='pMessage'></div>";
			rv+="</div>";
				
			rv+="<div class='row'>"; // the box password row
			rv+="<div class='col-sm-1'></div>";
			rv+="<div class='col-sm-10 my-auto'>";  // password box column
			rv+="<form id='passwordForm' name='passwordForm' align='center' class='form-horizontal' autocomplete='off'>\n";
			
			rv+="<input type='password' id='password' name='password' style='display:none''>";
			
			rv+="<div class='form-group row form-group-top-row '>";
			rv+="<label class='col-form-label col-sm-2' for='link'>Password</label>";
			rv+="<div class='col-sm-7'>";
			rv+="<input type='password' id='newPassword0' name='newPassword0' size='80' ";
			rv+=" onchange='changedPassword()' class='form-control' title='"+WebHelper.PasswordRules+"'>";
			rv+="</div>";
			rv+="<div class='col-sm-3 center text-danger' id='pMessage'></div>";
			rv+="</div>"; // form-group
			
			rv+="<div class='form-group row form-group-top-row '>";
			rv+="<label class='col-form-label col-sm-2' for='link'>Confirm Password</label>";
			rv+="<div class='col-sm-7'>";
			rv+="<input type='password' id='newPassword1' name='newPassword1' size='80' ";
			rv+=" onchange='changedPassword()' class='form-control'>";
			rv+="</div>";
			rv+="</div>"; // form-group
			
			rv+="<div class='form-group row'>";
			rv+="<div class='col-sm-4'></div>";
			rv+="<div class='col-sm-4 center'>";  
			rv+="<input type='button' name='updatePassword' id='updatePassword' value='update password' onclick='doUpdatePassword();' disabled class='btn btn-primary form-group' />";
			rv+="</div>"; // col
			rv+="<div class='col-sm-2 center'><div id='spinnerDiv'></div></div>";
			rv+="</div>"; // form-group
			
			rv+="</div>";  // password box
			rv+="</form>";
			rv+="</div>"; // password row			
			rv+="</div>"; // password column
			rv+="</div>"; // row
			
			// end password ***
			
			// edit emails ****
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			rv+="<div class='row'>"; // emails row
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>"; // emails column
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Emails Control ";
			rv+=WebHelper.EmailHelp();
			rv+="</div></div>";
		
			rv+=getEmails(conn, sessionUser);
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			rv+="</div>"; // emails column		
			rv+="</div>"; // emails row		
			// end edit emails ***
			
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			
			// security questions ***
			rv+="<div class='row'>"; // sq row
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>"; // sq column
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Security Questions Control ";
			rv+=WebHelper.SqHelp();
			rv+="</div></div>";
			
			rv+=getSecurityQuestions(conn, sessionUser);
			rv+="<input type='button' name='updateSq' id='updateSq' value='update security questions' onclick='fnUpdateSq();' disabled class='btn btn-primary form-group' />";
			
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			rv+="</div>"; // sq column		
			rv+="</div>"; // sq row
			// end security questions ***
			
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			
			// perm delete account ***
			rv+="<div class='row'>"; // perm delete row
			rv+="<div class='col-sm-1'></div>";			
			rv+="<div class='col-sm-10 mbpLightBg'>"; // perm delete column
			
			rv+="<div class='row'><div class='col-sm-12 redText'>";
			rv+="Permanently Delete Account ";
			rv+=WebHelper.DeleteAccountHelp();
			rv+="</div></div>";
			
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			
			rv+="<div class='row'>";
			rv+="<div class='col-sm-4'></div>";
			rv+="<div class='col-sm-4'>";
			rv+="<input type='button' id='deleteAccount' class='' value='Permanently Delete Account' onclick='fnDeleteAccount();'>";			
			rv+="</div>"; // col
			rv+="</div>"; // row
			
			rv+="<div class='row spacer-row'><div class='col-lg-12'>&nbsp</div></div>";
			
			rv+="</div>"; // perm delete column
			rv+="</div>\n"; // perm delete row
			// perm delete account ***
						
			return rv;
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return null;
	}

	public String getEmails(Connection conn, SessionUser sessionUser) {
		
		String rv="";
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-12 center text-danger' id='eMessage'></div>";
		rv+="</div>";

		rv+="<div id='emailsDiv'>";
		rv+=getEmailsDiv(conn, sessionUser);
		rv+="</div>"; // emailsDiv
		
		return rv;
	}
	public String getEmailsDiv(SessionUser sessionUser) {
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();
			return getEmailsDiv(conn, sessionUser);
		} catch (SQLException e) {
			log.error("e:"+e);
		}
		dbConnection.tryClose(conn);
		return "";
	}
	
	public String getEmailsDiv(Connection conn, SessionUser sessionUser) {
		TreeMap<Integer, OneWebUserEmail> emails = dbWebUser.getEmails(conn, sessionUser.getWebUser().getWuid());
		String rv="";
		
		// 
		Integer cntVerified=0;
		for(OneWebUserEmail email : emails.values()) {
			if(email.getDtVerified()!=null)
				cntVerified++;
		}
		// mark them all non delete-able
		for(OneWebUserEmail email : emails.values())
			email.setCanDelete(false);
		
		// if we have two or more verified we can delete any
		if(cntVerified>1)
			for(OneWebUserEmail email : emails.values())
				email.setCanDelete(true);
		// else we can only delete the ones that are not verified
		else
			for(OneWebUserEmail email : emails.values())
				if(email.getDtVerified()==null)
					email.setCanDelete(true);
		
		Integer i=0;
		for(OneWebUserEmail email : emails.values()) {
			rv+="<div class='row'>"; // an email row
			String notop="";
			if(i>0) notop=" border-top-0 ";
			
			// spacer column
			rv+="<div class='col-sm-1'></div>";
			
			// email address column
			rv+="<div class='col-sm-4 border"+notop+"' id='emailAddress-"+email.getIdEmail()+"'>"+email.getEmail()+"</div>";
			
			// verified column
			rv+="<div class='col-sm-2 border border-left-0 border-right-0"+notop+"'>";
			if(email.getDtVerified()!=null) {				
				rv+="verified <i class='fas fa-check'></i>";
				rv+="</div>";
				rv+="<div class='col-sm-3 border border-left-0 border-right-0"+notop+"'>";
			}
			else {
				rv+="verified <i class='fas fa-ban'></i>";
				rv+="</div>";
				rv+="<div class='col-sm-3 border border-left-0 border-right-0"+notop+"'>";
				rv+="<input type='button' value='Send New Verification Code' title='send new verification code'";
				rv+=" id='btnRC-"+email.getIdEmail()+"' onclick='fnSendNewCode("+email.getIdEmail()+");'>";

			}
			rv+="</div>";
			
			// delete
			rv+="<div class='col-sm-1 border border-left-0"+notop+"'>";  // delete column
			if(email.getCanDelete()) {
				rv+="<input type='button' value='Delete'";
				rv+=" id='btnVCode-"+email.getIdEmail()+"' onclick='fnDeleteEmail("+email.getIdEmail()+");'>";					
			}
			rv+="</div>";  // // delete column
			
			rv+="</div>\n"; // end an email row
			i++;
		}

		// add email
		if(emails.size()>=3)
			return rv;
	
		rv+="<div class='row'>"; // add email row
		rv+="<div class='col-sm-1'></div>"; // spacer column
		rv+="<div class='col-sm-4 noPadding border border-top-0'>"; // add email address
		rv+="<input type='text' class='w-100 center' id='iptAddEmail' name='iptAddEmail' onchange='addEmailCheck();' placeholder='new email address'>";
		rv+="</div>"; // add email address
		
		rv+="<div class='col-sm-2 border-bottom'>";
		rv+="<input type='button' id='btnAddEmail' disabled value='Add Email' onclick='fnAddEmail();'>";
		rv+="</div>";
		
		rv+="<div id='mAddEmail' class='col-sm-4 border-right border-bottom'></div>";
		rv+="</div>";  // add email row
		
		return rv;
	}
	
	public String getSecurityQuestions(Connection conn, SessionUser sessionUser) {
		String rv="";
		rv+="<div class='row'><div class='col-sm-12'>";
		rv+="<div class='redText' id='sqMessage'></div>";
		rv+="</div></div>";
		
		HashMap<Integer, SecurityQuestion> questions = dbWebUser.getSecurityQuestions();
		HashMap<Integer, SecurityQuestion> qGroup1 = new HashMap<Integer, SecurityQuestion>();
		HashMap<Integer, SecurityQuestion> qGroup2 = new HashMap<Integer, SecurityQuestion>();
		for(SecurityQuestion question : questions.values()) {
			if(question.getQgroup()==1) 
				qGroup1.put(question.getIdSecurityQuestion(), question);
			else
				qGroup2.put(question.getIdSecurityQuestion(), question);
		}
		
		SQWebUser sqWebUser = dbWebUser.getSecurityQuestionsAndForgotPasswordStuffForWebuser(conn, sessionUser.getWebUser().getWuid(), sessionUser.getWebUser().getLoggedInEmail().getIdEmail());
		
		for(int i=1; i<3; i++) {
			rv+="<div class='form-group row left '>";
			rv+="<label for='sq' class='col-form-label col-sm-2' >Security Question "+i+"</label>";
			rv+="<div class='col-sm-4'>";
			rv+="<select id='sqq"+i+"' name='sqq"+i+"'>";
			HashMap<Integer, SecurityQuestion> mygroup;
			if(i==1) 	mygroup=qGroup1;
			else		mygroup=qGroup2;
				
			for(SecurityQuestion question : mygroup.values()) {
				if(question.getDtInactive()!=null)
					continue;
				rv+="<option value='"+question.getIdSecurityQuestion()+"'";
				if(question.getIdSecurityQuestion()==sqWebUser.getSqq1())
					rv+=" selected ";
				
				rv+=">"+question.getQuestion()+"</option>";
			}		
			rv+="</select>\n";
			rv+="</div>";
				
			rv+="<div class='col-sm-4'>";
			rv+="<input type='text' id='sqa"+i+"' name='sqa"+i+"' onchange='vsqa()' size='80' required='true' ";
			rv+=" title='Must be at least 4 characters\n' class='form-control' />";
				
			rv+="</div>";
				
			rv+="<div class='col-sm-2 left' id='rsq"+i+"'></div>";
			rv+="</div>"; // form-group
		}
		
		return rv;
	}
	
	public Boolean isChosen(SQWebUser sqWebUser, SecurityQuestion sq) {
		
		return false;
	}
	
}
