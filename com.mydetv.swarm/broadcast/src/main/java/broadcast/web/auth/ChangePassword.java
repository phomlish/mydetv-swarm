package broadcast.web.auth;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("change-password")
public class ChangePassword {

	private static final Logger log = LoggerFactory.getLogger(ChangePassword.class);
	@Inject private WebHelper webHelper;
	@Inject private UserHelper userHelper;
	
	@GET
	public Response changePassword(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
		if(!userHelper.isMydetvCookieKnown(req)) {
			SessionUser sessionUser = userHelper.getSessionUser(req);
			return(userHelper.redirectToPage(sessionUser, "/auth/change-password?reload=1"));
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isLoggedIn()) {
			log.info("not signed in, redirecting to /");
			return Response.seeOther(UriBuilder.fromUri("/").build()).build();
		}
		
		String rv = webHelper.beginHtml(sessionUser, "Change Password");
		rv+="<div class='container col-lg-12'>";
		
		if(! sessionUser.getWebUser().getRequirePasswordChange()) {
			log.info("not required");
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="<br>You can only use this page to change a temporary password<br>";
			rv+="If you need to change your password please click your profile image (top right) and choose 'Edit Security'";
			rv+="</div></div>";
			rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
			ResponseBuilder rb = Response.status(Response.Status.OK);
			rb = userHelper.addHeaders(sessionUser, rb);
			rb.entity(rv);		
			return rb.build();
		}		

		rv+="<script type='text/javascript'>fnChangePasswordInit();</script>";
		
		rv+="<div class='row'>"; // changePassword message row
		rv+="<div class='col-sm-2'></div>"; // spacer column
		rv+="<div class='col-sm-8'>"; // changePassword message column
		rv+="<div class='center redText' id='changePasswordMessage'></div>";
		rv+="</div>"; // changePassword message column
		rv+="</div>"; // changePassword message row
		
		// change password
		rv+="<div class='row'>"; // changePassword row
		rv+="<div class='col-sm-1'></div>"; // spacer column
		rv+="<div class='col-sm-10 mbpLightBg'>"; // changePassword column
		
		rv+="<div class='row'><div class='col-sm-12'>";
		rv+="Change Password ";
		rv+=WebHelper.PasswordHelp();
		rv+="</div></div>";
		
		// current password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >Password</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='password' id='currentPassword' name='currentPassword' onchange='vcurpasscp();' required='true' class='form-control' ";
		rv+="autocomplete='nope'/>";
		rv+="</div>";		
		rv+="<div class='col-sm-6 left' id='rcurrentpassword'></div>";
		rv+="</div>"; // form-group
		
		// new password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >New Password</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='password' id='newPassword' name='newPassword' onchange='vpasscp();' required='true' class='form-control' ";
		rv+="title='"+WebHelper.PasswordRules+"' autocomplete='nope'/>";
		rv+="</div>";		
		rv+="<div class='col-sm-6 left' id='rnewpassword'></div>";
		rv+="</div>"; // form-group
		
		// confirm password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >Confirm Password</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='password' id='confirmPassword' name='confirmPassword' onchange='vpasscp();' required='true' class='form-control' autocomplete='nope'/>";
		rv+="</div>";		
		rv+="<div class='col-sm-6 left' id='rconfirmpassword'></div>";
		rv+="</div>"; // form-group
		
		rv+="<div class='form-group row'>";
		rv+="<div class='col-sm-4'></div>";
		rv+="<div class='col-sm-4 center'>";
		rv+="<input type='button' name='btnChangePassword' id='btnChangePassword' value='change password' onclick='fnChangePasswordSubmit();' disabled class='btn btn-primary form-group' />";
		rv+="</div>"; // col
		rv+="<div class='col-sm-2 center'><div id='spinnerDiv'></div></div>";
		rv+="</div>"; // form-group
		
		rv+="</div></div>"; // changePassword column/row
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}
}
