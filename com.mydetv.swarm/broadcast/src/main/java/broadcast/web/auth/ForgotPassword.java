package broadcast.web.auth;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.SQWebUser;
import shared.db.DbConnection;
import shared.db.DbWebUser;

@Path("forgot-password")
public class ForgotPassword {

	private static final Logger log = LoggerFactory.getLogger(ForgotPassword.class);
	@Inject private WebHelper webHelper;
	@Inject private UserHelper userHelper;
	@Inject private DbWebUser dbWebUser;
	@Inject private DbConnection dbConnection;
	
	@GET
	public Response forgotPassword(@Context HttpServletRequest req, @Context HttpServletResponse resp) {

		SessionUser sessionUser = userHelper.getSessionUser(req);
	
		String rv = webHelper.beginHtml(sessionUser, "Forgot Password");
		rv+="<div class='container col-lg-12'>";
	
		// forgot
		rv+="<div class='row'>"; // forgot row
		rv+="<div class='col-sm-3'></div>"; // spacer column
		rv+="<div class='col-sm-6 mbpLightBg'>"; // forgot column
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-12'>";
		rv+="Forgot Password ";
		rv+=WebHelper.ForgotPasswordHelp();
		rv+="</div></div>";
		
		rv+="<div class='row'><div class='col-sm-12'>&nbsp</div></div>";
		
		if(sessionUser.isAuthenticated()) {
			log.info("already signed in");
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="You are already signed in.<br>";
			rv+="To change your password please click on your profile image (top right) and select 'Edit Security'<br>";
			rv+="<br></div></div>";
			return closeDivDone(Response.Status.OK, sessionUser, rv);
		}
		
		// if we got here from clicking the link in the email
		if(req.getParameterMap().containsKey("e"))
			return processFromEmail(req, rv, sessionUser);    	  
		else
			return finishForgotPasswordForm(req, rv, sessionUser);
	}
	
	private Response finishForgotPasswordForm(HttpServletRequest req, String rv, SessionUser sessionUser) {
		rv+="<script type='text/javascript'>forgotPasswordClearEmail();</script>";
		rv+="<div class='row'><div class='col-sm-12'>";
		rv+="Please enter a *verified email address associated with your account.";
		rv+="</div></div>";
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-2'></div>";
		
		rv+="<div class='col-sm-8'>";
		rv+="<input type='text' id='real-email' name='real-email'  size='30' ";
		rv+=" title='username@example.com' required='true' class='form-control' autocomplete='nope'";
		rv+=" onchange='vEmailForgotEmail();'/><br>";		
		rv+="<input type='button' name='sendFP' id='sendFP' value='Send Unlock Code' onclick='requestForgotPassword();' disabled class='btn btn-primary form-group' />";
		
		rv+="</div></div>";
		
		rv+="<div class='row'><div class='col-sm-12'>";
		rv+="* If you have not verified an email<br>you will not be able to use this 'Forgot Password' page.";
		rv+="</div></div>";
		
		return closeDivDone(Response.Status.OK, sessionUser, rv);
	}
	private Response processFromEmail(HttpServletRequest req, String rv, SessionUser sessionUser) {
		Integer eid;
    	Integer wuid;
    	String code;
    	String uuid;
	    try {
	    	eid = Integer.decode(req.getParameter("e"));
	    	wuid = Integer.decode(req.getParameter("w"));
	    	code = req.getParameter("c");
	    	uuid = req.getParameter("u");
	    } catch(java.lang.NumberFormatException e) {
	    	log.info("error "+e);
	    	return sendError(sessionUser);
	    }
	    log.info("they clicked the link "+eid);
	    	
	    if(code.length()!=8) 
	   		return sendError(sessionUser);
	    	
	    Connection conn = null;
	    try {
			conn = dbConnection.getConnection();
			
			SQWebUser sqw = dbWebUser.getSecurityQuestionsAndForgotPasswordStuffForWebuser(wuid, eid);
			if(sqw==null 
				|| sqw.getEmail()==null || sqw.getEmail()==""
				//|| sqw.getUuidForgotEmail()==null || !sqw.getUuidForgotEmail().equals(uuid)
				//|| sqw.getForgotPassword()==null || !sqw.getForgotPassword().equals(code)
				) {
				dbConnection.tryClose(conn);
				return sendError(sessionUser);
			}
			
			if(sqw.getDtForgotPassword()==null) {
				rv+="<div class='row'><div class='col-sm-12'>";
				rv+="That cde is invalid.  Did you alrady use it?<br>";
				rv+="<input type='button' value='Try Again' onclick=\"location.href='/auth/forgot-password'\" class='btn btn-primary form-group' />";
				rv+="</div></div>";
				dbConnection.tryClose(conn);
				return closeDivDone(Response.Status.OK, sessionUser, rv);
			}
			
			
			LocalDateTime old = LocalDateTime.now().minusMinutes(15);
			if(sqw.getDtForgotPassword().isBefore(old)) {
				rv+="<div class='row'><div class='col-sm-12'>";
				rv+="Your code has expired.<br>";
				rv+="<input type='button' value='Try Again' onclick=\"location.href='/auth/forgot-password'\" class='btn btn-primary form-group' />";
				rv+="</div></div>";
				dbConnection.tryClose(conn);
				return closeDivDone(Response.Status.OK, sessionUser, rv);
			}
			
			//rv+="<script type='text/javascript'>forgotPasswordClearEmail();</script>";
			rv+="<div id='e' class='d-none'>"+eid+"</div>";
			rv+="<div id='w' class='d-none'>"+wuid+"</div>";
			rv+="<div id='c' class='d-none'>"+code+"</div>";
			rv+="<div id='u' class='d-none'>"+uuid+"</div>";
			
			rv+="<div class='row'><div class='col-sm-12'>";
			rv+="Please answer the security questions for "+sqw.getEmail();
			rv+="</div></div>";
			
			rv+="<div class='row'><div class=col-sm-12'>&nbsp</div></div>";
			
			rv+="<div class='form-group row'>";
			rv+="<label for='q1' class='col-form-label col-sm-6' >";
			rv+=sqw.getQuestion1();
			rv+="</label>";
			rv+="<div class='col-sm-6'>";
			rv+="<input type='text' id='sqa1' name='sqa1' size='40' required='true' onchange='fnAnswer();' class='form-control' />";
			rv+="</div>";
			rv+="</div>"; // form-group
			
			rv+="<div class='form-group row'>";
			rv+="<label for='q2' class='col-form-label col-sm-6' >";
			rv+=sqw.getQuestion2();
			rv+="</label>";
			rv+="<div class='col-sm-6'>";
			rv+="<input type='text' id='sqa2' name='sqa2' size='40' required='true' onchange='fnAnswer();' class='form-control' />";
			rv+="</div>";
			rv+="</div>"; // form-group
			
			rv+="<input type='button' name='sendAnswers' id='sendAnswers' value='Submit Answers' onclick='fnSendAnswers();' disabled class='btn btn-primary form-group' />";
			
			rv+="</div></div>";
			
			rv+="<div class='row'><div class=col-sm-12 center'>";
			rv+="If you have forgotten your security question answers you will need an administrators help.<br>";
			rv+="Submit a request under 'Account Issues' at <a href='https://groups.google.com/forum/#!forum/mydetv-swarm'>https://groups.google.com/forum/#!forum/mydetv-swarm</a>";
			rv+="</div></div>";
			
			dbConnection.tryClose(conn);
			return closeDivDone(Response.Status.OK, sessionUser, rv);
		} catch (SQLException e) {
			log.error("e:"+e);
			dbConnection.tryClose(conn);
			return sendError(sessionUser);
		}		  
	}
	private Response closeDivDone(javax.ws.rs.core.Response.Status status, SessionUser su, String rv) {
		
		rv+="</div>";
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rv+="</body>\n";
		rv+="</html>\n";	
		
		ResponseBuilder rb = Response.status(status);
		rb = userHelper.addHeaders(su, rb);
		rb.entity(rv);		
		return rb.build();
	}
	
	private Response sendError(SessionUser su) {	
		ResponseBuilder rb = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		rb = userHelper.addHeaders(su, rb);
		return rb.build();
	}
}
