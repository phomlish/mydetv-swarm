package broadcast.web.auth;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.auth.Validations;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.SecurityQuestion;
import shared.db.DbWebUser;

@JsonAutoDetect
@Path("register")
public class Register {

	private static final Logger log = LoggerFactory.getLogger(Register.class);
	@Inject private WebHelper webHelper;
	@Inject private UserHelper userHelper;
	@Inject private DbWebUser dbWebUser;
	@Inject private Validations validations;

	@GET
	public Response register(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
		if(!userHelper.isMydetvCookieKnown(req)) {
			SessionUser sessionUser = userHelper.getSessionUser(req);
			return(userHelper.redirectToPage(sessionUser, "/auth/register?reload=1"));
		}
		
		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(sessionUser.isAuthenticated()) {
			log.info("already signed in, redirecting to /");
			return Response.seeOther(UriBuilder.fromUri("/").build()).build();
		}
			
		if(validations.isSystemLoginLockedCurrently())
			return validations.showLoginLockedPage(sessionUser);
		
		String rv = webHelper.beginHtml(sessionUser, "Register");	
		rv+="<div class='container col-lg-12'>";
		rv+="<script type='text/javascript'>registerInit();</script>";
		rv+="<div class='row'>"; // register message row
		rv+="<div class='col-sm-2'></div>"; // spacer column
		rv+="<div class='col-sm-8'>"; // register message column
		rv+="<div class='center redText' id='registerMessage'></div>";
		rv+="</div>"; // register message column
		rv+="</div>"; // register message row
		
		// register
		rv+="<div class='row'>"; // register row
		rv+="<div class='col-sm-1'></div>"; // spacer column
		rv+="<div class='col-sm-10 mbpLightBg'>"; // register column
		
		rv+="<div class='row'><div class='col-sm-12'>";
		rv+="Register ";
		rv+=WebHelper.RegisterHelp();
		rv+="</div></div>";

		// create these to fool browsers to not remember / autofill things
		rv+="<input id='email' style='display:none' type='text' name='email' />";
		rv+="<input id='password' style='display:none' type='password' name='password' />";
		rv+="<input id='username' style='display:none' type='text' name='username' />";
		
		// email
		rv+="<div class='form-group row form-group-top-row'>";
		rv+="<label for='email' class='col-form-label col-sm-2' >Email</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='text' id='real-email' name='real-email' onchange='vemailreg()' size='80' ";
		rv+=" title='Will require validation' required='true' class='form-control' autocomplete='nope'/>";
		rv+="</div>";
		rv+="<div class='col-sm-6 left' id='remail'></div>";
		rv+="</div>"; // form-group

		// password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >Password</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='password' id='real-password' name='real-password' onchange='vpasswordreg();' required='true' class='form-control' ";
		rv+="title='"+WebHelper.PasswordRules+"' autocomplete='nope'/>";
		rv+="</div>";		
		rv+="<div class='col-sm-6 left' id='rpassword'></div>";
		rv+="</div>"; // form-group
		
		// confirm password
		rv+="<div class='form-group row' >";
		rv+="<label for='password' class='col-form-label col-sm-2' >Confirm Password</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='password' id='real-confirm-password' name='real-confirm-password' onchange='vconfirmpasswordreg();' required='true' class='form-control' autocomplete='nope'/>";
		rv+="</div>";		
		rv+="<div class='col-sm-6 left' id='rconfirmpassword'></div>";
		rv+="</div>"; // form-group

		// username
		rv+="<div class='form-group row'>";
		rv+="<label for='username' class='col-form-label col-sm-2' >Username</label>";
		rv+="<div class='col-sm-4'>";
		rv+="<input type='text' id='real-username' name='real-username' onchange='vusernamereg()' size='80' required='true' ";
		rv+=" title='"+WebHelper.UsernameRules+"' class='form-control' autocomplete='nope'/>";
		rv+="</div>";
		rv+="<div class='col-sm-6 left' id='rusername'></div>";
		rv+="</div>"; // form-group
		
		rv+=getSecurityQuestions();
		
		rv+="<div class='form-group row'>";
		rv+="<div class='col-sm-4'></div>";
		rv+="<div class='col-sm-4 center'>";
		rv+="<input type='button' name='register' id='register' value='register' onclick='registerSubmit();' disabled class='btn btn-primary form-group' />";
		rv+="</div>"; // col
		rv+="<div class='col-sm-2 center'><div id='spinnerDiv'></div></div>";
		rv+="</div>"; // form-group
		
		rv+="</div></div>"; // register column/row
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();

		ResponseBuilder rb = Response.status(Response.Status.OK);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}

	private String getSecurityQuestions() {
		String rv="";
		HashMap<Integer, SecurityQuestion> questions = dbWebUser.getSecurityQuestions();
		HashMap<Integer, SecurityQuestion> qGroup1 = new HashMap<Integer, SecurityQuestion>();
		HashMap<Integer, SecurityQuestion> qGroup2 = new HashMap<Integer, SecurityQuestion>();
		for(SecurityQuestion question : questions.values()) {
			if(question.getQgroup()==1) 
				qGroup1.put(question.getIdSecurityQuestion(), question);
			else
				qGroup2.put(question.getIdSecurityQuestion(), question);
		}
		
		for(int i=1; i<3; i++) {
			rv+="<div class='form-group row left '>";
			rv+="<label for='sq' class='col-form-label col-sm-2' >Security Question "+i+"</label>";
			rv+="<div class='col-sm-4'>";
			rv+="<select id='sqq"+i+"' name='sqq"+i+"'>";
			HashMap<Integer, SecurityQuestion> mygroup;
			if(i==1) 	mygroup=qGroup1;
			else		mygroup=qGroup2;
				
			for(SecurityQuestion question : mygroup.values()) {
				if(question.getDtInactive()!=null)
					continue;
				rv+="<option value='"+question.getIdSecurityQuestion()+"'>"+question.getQuestion()+"</option>";
			}		
			rv+="</select>\n";
			rv+="</div>";
				
			rv+="<div class='col-sm-4'>";
			rv+="<input type='text' id='sqa"+i+"' name='sqa1' onchange='vusersq("+i+")' size='80' required='true' ";
			rv+=" title='Must be at least 4 characters\n' class='form-control' />";
				
			rv+="</div>";
				
			rv+="<div class='col-sm-2 left' id='rsq"+i+"'></div>";
			rv+="</div>"; // form-group
		}
		/*
		rv+="<div class='form-group row left '>";
		rv+="<label for='sq' class='col-form-label col-sm-2' >Security Question 2</label>";
		rv+="</div>";
		
		rv+="<div class='form-group row left '>";
		rv+="<label for='sq' class='col-form-label col-sm-2' >Security Question 3</label>";
		rv+="</div>";
		*/
		
		return rv;
	}

}
