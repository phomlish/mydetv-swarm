package broadcast.web.jsp;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

public class JspModules extends AbstractModule {

	@Override
	protected void configure() {
		bind(ListService.class).to(DummyListService.class).in(Scopes.SINGLETON);
	}

}
