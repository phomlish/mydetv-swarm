package broadcast.web.jsp;

import com.google.inject.Inject;

import broadcast.Blackboard;

public class Counter {

	public int counter=0;
	public String last;
	@Inject private Blackboard bb;
	
	public int getCounter() {
		if(bb==null)
			return 99999999;
		return counter++;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	
}
