package broadcast.web.jsp;

import java.util.List;

public interface ListService {
    List<String> names();
}
