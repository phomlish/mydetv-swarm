package broadcast.web.jsp;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;

import broadcast.Blackboard;

public class JspGuiceServletContextListener extends GuiceServletContextListener {

	@Inject private Blackboard blackboard;
	
	@Override
	public Injector getInjector() {
	    return blackboard.getMainInjector().createChildInjector(
	    	new ServletModule() {
	    		@Override protected void configureServlets() {
	    			serve("/j/list").with(ListController.class);
	                serve("/j/counter.html").with(CounterServlet.class);
	                }
	            },
	        new JspModules()
	    );
	}	
}
