package broadcast.web.jsp;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import broadcast.Blackboard;

@Singleton
public class ErrorController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6083563064620381093L;
	private static final Logger log = LoggerFactory.getLogger(ErrorController.class);
	@Inject private Blackboard blackboard;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    	String referer = req.getHeader("referer");
        // not found /jsp/list.jsp
        ServletContext web1 = getServletContext();
		ServletContext web2 = web1.getContext("/s");
		RequestDispatcher dispatcher = web2.getRequestDispatcher("/jsp/error.jsp");
		resp.addHeader("referer", referer);
		
		dispatcher.forward(req,resp);
		if(blackboard == null) 
			log.info("no bb");
		else
			log.info("bb");
        //req.getRequestDispatcher("/s/jsp/counter.jsp").forward(req, resp);
    }

}
