package broadcast.web.jsp;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;

public class DummyListService implements ListService {
	
	@Inject private Blackboard blackboard;
	private static final Logger log = LoggerFactory.getLogger(DummyListService.class);
	
    public List<String> names() {
    	if(blackboard==null)
    		log.info("no inject");
    	else
    		log.info("INJECT !!!!!!!!!!");
        return Arrays.asList("Dave", "Jimmy", "Nick");
    }
}
