package broadcast.web.jsp;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import broadcast.Blackboard;

@Singleton
public class CounterServlet extends HttpServlet {
	
	private static final long serialVersionUID = 5002669686483494118L;
	private static final Logger log = LoggerFactory.getLogger(CounterServlet.class);
	@Inject private Blackboard blackboard;
	@Inject private Counter counter;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		if(blackboard==null)
			log.info("not injected");
		else
			log.info("Injected!!!!!!");
		Enumeration<String> params = getInitParameterNames();
		while(params.hasMoreElements()) {
			log.info("param:"+params.nextElement());
		}

		req.setAttribute("counter", counter.getCounter());
		req.setAttribute("last", counter.getLast());
		counter.setLast(req.getRequestURI());
		ServletContext web1 = getServletContext();
		ServletContext web2 = web1.getContext("/s");
		RequestDispatcher dispatcher = web2.getRequestDispatcher("/jsp/counter.jsp");
		dispatcher.forward(req,resp);
		//ResponseBuilder rb = Response.seeOther(UriBuilder.fromUri(URI.create("/s/counter.jsp")).build());
		log.info("test:"+req.getContextPath());
		//RequestDispatcher aDispatcher = req.getRequestDispatcher("/counter.jsp");
		//req.getRequestDispatcher("/s/jsp/counter.jsp").forward(req,resp);
		//aDispatcher.forward(req,resp);
	}

}
