package broadcast.web.home;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/about")
public class AboutPage {

	@Inject private Config sharedConfig;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AboutPage.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp, @QueryParam("code") String code, @QueryParam("state") String state) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);

		if(! sessionUser.isAuthenticated())
			return userHelper.redirectLogin(sessionUser);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String rv = webHelper.beginHtml(sessionUser, "About");

		if(sharedConfig.getInstance().equals("dev")) {
			rv+="<center><h3>instance=dev, you are reading outdated version files</h3>\n";
			rv+="we don't automatically update these for development<br>\n";
			rv+="if they are missing you need to copy them manually after a mvn clean package from<br>\n";
			rv+="com.mydetv.swarm/broadcast/target/classes/version.txt<br>\n";
			rv+="com.mydetv.swarm/broadcast/target/classes/META-INF/maven/dependencies.properties<br>\n";
			rv+=" to your conf directory<br>\n";
			rv+="</center>\n";
		}

		String fpn="version.txt";
		rv+="<br>"+fpn+":<br>\n";
		try(BufferedReader br = new BufferedReader(new FileReader(fpn))) {
		    String line = br.readLine();

		    while (line != null) {
		        rv+=line+"<br>\n";
		        line = br.readLine();
		    }
		} catch(Exception e) {
			rv="unable to read "+fpn+" "+e+"<br>\n";
		}

		fpn="dependencies.properties";
		rv+="<br>"+fpn+":<br>\n";
		try(BufferedReader br = new BufferedReader(new FileReader(fpn))) {
		    String line = br.readLine();

		    while (line != null) {
		        rv+=line+"<br>\n";
		        line = br.readLine();
		    }
		} catch(Exception e) {
			rv="unable to read "+fpn+" "+e+"<br>\n";
		}		
		
		rv+="</div>\n";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		rb.entity(rv);		
		return rb.build();
	}

}
