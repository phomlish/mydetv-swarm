package broadcast.web.home;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/schedule")
public class SchedulePage {
	
private static final Logger log = LoggerFactory.getLogger(HomePage.class);

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		log.info("incoming "+req.getRemoteAddr()+":"+req.getRemotePort());
		SessionUser sessionUser = userHelper.getSessionUser(req);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String addnow = "";
		if(config.getInstance().toLowerCase().contains("dev"))
			addnow="?now="+System.currentTimeMillis();	
		String eh="";
		eh+="<script src='/s/js/swarm-schedule.js"+addnow+"'></script>\n";
		eh+="<script src='/s/js/swarm-api-dmca.js"+addnow+"'></script>\n";
		String rv = webHelper.beginHtml(sessionUser, "Schedule", "schedule", eh);
		
		rv+="<div class='container col-lg-12' id='schedDiv'>";

		ZoneId userDtz;
		OneMydetvChannel targetChannel = null;
		if(sessionUser.getWebUser()!=null) {
			userDtz = sessionUser.getWebUser().getUserDtz();
			if(sessionUser.getMydetvChannel()!=null)
				targetChannel=sessionUser.getMydetvChannel();
		}
		else {
			userDtz = ZoneId.of(sessionUser.getBrowserTimezone());
		}
		
		if(targetChannel==null || ! targetChannel.isActive() || ! targetChannel.getChannelType().equals(1)) 
			for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
				if(mydetvChannel.getChannelType()==1) {
					targetChannel=mydetvChannel;
					break;
				}
			}
		
		ZonedDateTime userNow = LocalDateTime.now().atZone(userDtz);
		ZonedDateTime userNowMidnight =userNow.toLocalDate().atStartOfDay(userDtz);;
		DateTimeFormatter yyyyMMddFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String apiDate = userNowMidnight.format(yyyyMMddFormatter);
		rv+="<div class='container col-lg-12' id='schedDiv'></div>";
		rv+="</div>"; // container
		String targetDate=apiDate;

		rv+="<script type='text/javascript'>initSchedule('"+targetChannel.getChannelId()+"','"+targetDate+"');</script>\n";
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();

		rb.entity(rv);
		
		return rb.build();
	}


}
