package broadcast.web.home;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/test")
public class TestPage {

	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);
		
		userHelper.createDummyUser(sessionUser);
		
		
		String rv = webHelper.beginHtml(sessionUser, "Home", "home");
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-2'></div>";
		rv+="<div class='col-sm-8'>";
		rv+="<noscript><center><font size='3' color='red'>";
		rv+="you don't have javascript enabled.  This site requires javascript";
		rv+="</font></center></noscript><br><br>";
		
		
		rv+="<br><center>Only <b><i>DESKTOPS</i></b> are supported currently.<br>";
		rv+="The officially supported browsers are the <b><i>lastest</i></b> versions of Firefox and Chrome.<br>";
		rv+="Please share your experience at <a href='https://groups.google.com/forum/#!forum/mydetv-swarm' target='_blank' title='opens in new tab'>https://groups.google.com/forum/#!forum/mydetv-swarm</a><br>";
		rv+="</center>\n";
		
		rv+="</div>";
		rv+="</div>\n";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb=userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}
	
}
