package broadcast.web.home;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/videos")
public class VideosPage {

	@Inject private Config config;
	@Inject private UserHelper userHelper;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(VideosPage.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp, @QueryParam("code") String code, @QueryParam("state") String state) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);

		//if(! sessionUser.isAuthenticated())
		//	return userHelper.redirectLogin(sessionUser);
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String nocache = "";
		if(config.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		
		String rv="<!DOCTYPE html>\n";
		rv+="<html>\n";
		rv+="<head>\n";
		rv+="<meta charset=\"utf-8\">\n";
		if(config.getInstance().equals("dev"))
			rv+="<link rel='stylesheet' href='/s/bower_components/bootstrap/dist/css/bootstrap.css'>\n";
		else
			rv+="<link rel='stylesheet' href='/s/bower_components/bootstrap/dist/css/bootstrap.min.css'>\n";
		rv+="<link rel='stylesheet' href='/s/bower_components/jquery-ui/themes/base/jquery-ui.min.css'>\n";
		
		rv+="<script src='/s/bower_components/jquery/dist/jquery.min.js'></script>\n";
		rv+="<script src='/s/bower_components/jquery-ui/jquery-ui.min.js'></script>\n";
		rv+="<script src='/s/bower_components/js-cookie/src/js.cookie.js'></script>\n";
		
		rv+="<link  href='/s/bower_components/fancybox/dist/jquery.fancybox.min.css' rel='stylesheet'>";
		rv+="<script src='/s/bower_components/fancybox/dist/jquery.fancybox.min.js'></script>";
		rv+="<script src='/s/js/swarm-api-videos.js'></script>";
		
		if(sessionUser!=null && sessionUser.getTheme()!=null) {
		if(sessionUser.getTheme().equals("crisp"))
			rv+="<link rel='stylesheet' href='/s/css/swarm-crisp.css"+nocache+"'>\n";
		else
			rv+="<link rel='stylesheet' href='/s/css/swarm.css"+nocache+"'>\n";
		}
		
		rv+="<title>Stream</title>\n";
		rv+="</head>\n";
		rv+="  <body>\n";

	    rv+="<a data-fancybox=bigbuckbunny data-width=640 data-height=360 ";
	    rv+=" href='/api/video/get?video=mbp/20050312.vp8-opus.webm'>";
	    rv+="   <img src='/s/img/Video-Icon.png'>";
	    rv+="</a>";
		
		/*
		rv+="<div id='single-video' class='fancybox-video' >";
		rv+="<video controls width='20%' height='auto' preload='none'>";
		rv+="<source ";
		rv+=" src='/api/video/get?video=mbp/20050312.vp8-opus.webm''";
		rv+=" type='video/mp4'>";
		rv+="</video>";
		rv+="</div>";
*/
		/*

		rv+="    <div>";
		rv+="      <video id='video' controls autoplay>";
		rv+="        <source src='/api/video/get?video=mbp/20050312.vp8-opus.webm' />";
		rv+="     </video>";
		rv+="   </div>";
		*/
		
		
		rv+=" </body>";
		rv+="</html>";
		rv+="ok";
		
		rb.entity(rv);
		return rb.build();
	}

}
