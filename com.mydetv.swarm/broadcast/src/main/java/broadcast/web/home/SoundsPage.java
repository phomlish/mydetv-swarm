package broadcast.web.home;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.broadcaster.PlaySoundHelpers;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/sounds")
public class SoundsPage {

	@Inject private Config config;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private PlaySoundHelpers psHelpers;
	
	@GET
	public Response doGet(@Context HttpServletRequest req, @Context HttpServletResponse resp, @QueryParam("code") String code, @QueryParam("state") String state) throws ServletException, IOException
	{
		SessionUser sessionUser = userHelper.getSessionUser(req);

		//if(! sessionUser.isAuthenticated())
		//	return userHelper.redirectLogin(sessionUser);
		
		String addnow = "";
		if(config.getInstance().toLowerCase().contains("dev"))
			addnow="?now="+System.currentTimeMillis();
		
		String extraHeaders = "";
		
		extraHeaders+="<script src='/s/node_modules/datatables.net/js/jquery.dataTables.min.js'></script>\n";
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/datatables.net-dt/css/jquery.dataTables.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js'></script>\n";
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css'>\n";
		
		extraHeaders+="<script src='/s/js/swarm-sounds.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-api-sounds.js"+addnow+"'></script>\n";	
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String rv = webHelper.beginHtml(sessionUser, "sounds", "sounds", extraHeaders);

		rv+="<div class='row'>";
		rv+="<div class='col-sm-5'>";	
		rv+="Profile Sounds";
		rv+=psHelpers.drawSoundProfilesTable("sounds");
		rv+="</div>\n"; // profiles
		rv+="<div class='col-sm-1'>";
		rv+="<div id='spinner2Div' class='fa fa-spinner fa-spin'></div>";
		rv+="</div>\n";	
		rv+="<div class='col-sm-5'>";
		rv+=psHelpers.drawSoundsTable();
		rv+="</div>"; // sounds
		rv+="</div>"; // row
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		rb.entity(rv);		
		return rb.build();
	}
}



