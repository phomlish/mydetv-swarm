package broadcast.web.home;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.broadcast.SessionUser;
import shared.helpers.FilenameHelpers;

@JsonAutoDetect
@Path("/dmca")
public class DmcaPage {
	
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	private static final Logger log = LoggerFactory.getLogger(DmcaPage.class);
	
	@GET
	public Response doGet(@Context HttpServletRequest req)
	{
		log.info("do get");
		return drawPage(req);
	}
	
	private Response drawPage(@Context HttpServletRequest req) {
		SessionUser sessionUser = userHelper.getSessionUser(req);
		
		String addnow = "";
		if(config.getInstance().toLowerCase().contains("dev"))
			addnow="?now="+System.currentTimeMillis();	
		String eh = "<script src='/s/js/swarm-api-dmca.js"+addnow+"'></script>\n";
		String rv = webHelper.beginHtml(sessionUser, "DMCA", "dmca", eh);
		
		rv+="<div style='background-color:var(--mbpOffWhite);color:black'>";
		String strChannelId = null, strPkey=null, strOsId=null;
		try {
			//jo=GetJson.GetJsonObject(req);
			strChannelId = req.getParameter("c");
			strPkey = req.getParameter("p");
			strOsId = req.getParameter("o");

		} catch(Exception e) {
			log.info("no read "+e);
		}
		rv+="<br><h3>We take <b>Digital Millennium Copyright Act</b> takedown requests seriously<br>";
		rv+="and will respond so as to comply with said act.</h3><br>";
		
		if(strChannelId==null) {
			rv+="For your convienence you can pre-fill the offending media details from the ";
			rv+="<a href='/schedule' target='_blank'>schedule page</a>";
			rv+=" or the <a href='/p2p' target='_blank'>view channel page</a>";
			rv+=" by clicking on the video title and selecting DMCA from the drop down.";			
			rv+="<br><br>";
			
		}
		rv+="<div class='left dmcaBlock'>";
		rv+="My name is <input type='text' id='cpname' placeholder='Your Name'>";
		rv+=" and I am the <input type='text' id='cptitle' placeholder='Your Company Title/Position'>";
		rv+=" of <input type='text' id='cname' placeholder='Company Name'><br><br>";
		
		rv+="Your website is infringing on a copyright owned by my company.<br>";
		
		rv+=ODescriptionHelp();
		rv+="<textarea rows='8' cols='120' id='odesc' placeholder='Complete Description of Copyrighted Work Including Copyright Holder If Different From Above'></textarea>";
		rv+="<br><br>";
	
		rv+=TDescriptionHelp();
		rv+="<textarea rows='8' cols='120' id='titem' placeholder='Offending Item'>"+getOffendingItem(strChannelId, strPkey, strOsId)+"</textarea>";
		rv+="<br><br>";
			
		rv+="Consider the submission of this information as official notification under Section 512(c) of the Digital Millennium Copyright Act (”DMCA”). ";
		rv+=" I seek the removal of the aforementioned infringing material from your servers. ";
		rv+=" I am providing this notice in good faith and with the reasonable belief that rights my company owns or a entity that my company represents are being infringed.";
		rv+=" Under penalty of perjury I certify that the information contained in the notification is both true and accurate and that I have the authority to act on behalf of the owner of the copyright(s) involved.<br>"; 
		
		rv+="<br>My contact information:<br>";
		rv+="<input type='text' id='address' size='80' placeholder='Address'><br>";
		rv+="<input type='text' id='city' size='40' placeholder='City'>,";
		rv+="<input type='text' id='state' size='4' placeholder='State'>";
		rv+="<input type='text' id='zipcode' placeholder='Zipcode'>";
		rv+="<input type='text' id='country' size='20' placeholder='Country'><br>";
		rv+="<input type='text' id='phone' size='40' placeholder='Phone'><br>";
		rv+="<input type='text' id='email' size='40' placeholder='Email'><br>";
		
		rv+="<br><input type='button' name='sendDMCA' id='sendDMCA' value='Submit' onclick='submitDmca();' class='btn btn-danger'>";
		
		rv+="</div>";
		rv+="</div>";


		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
		
	}
	
	private String getOffendingItem(String strChannelId, String strPkey, String strOsId) {
		String defaultRv="";
		String rv="";
		if(strChannelId==null) {
			return defaultRv;
		}
		try {
			
			Integer channelId=Integer.parseInt(strChannelId);
			Integer pkey = Integer.parseInt(strPkey);
			if(!blackboard.getMydetvChannels().containsKey(channelId)) {
				log.error("channel not found "+channelId);
				return "Error looking up the video\r\n"+defaultRv;
			}
			OneMydetvChannel channel = blackboard.getMydetvChannels().get(channelId);
			
			if(! blackboard.getVideos().containsKey(pkey)) {
				log.error("pkey not found "+pkey);
				return "Error looking up the video\r\n"+defaultRv;
			}
			OneVideo ov = blackboard.getVideos().get(pkey);
			
			String title = ov.getTitle();
			if(title==null) 
				title=FilenameHelpers.StripSuffix(ov.getFn());
			rv+="offending item '"+title+"' ("+pkey+")\r\n";
			rv+=" witnessed on channel "+channel.getName()+"\r\n";

			if(strOsId!=null) {
				log.info("looking up schedule "+strOsId);
				Integer osId = Integer.parseInt(strOsId);
				if(! channel.getSchedules().containsKey(osId)) {
					log.error("error finding schedule "+osId+" for channel "+channel.getChannelId());
				}
				else {
					OneSchedule os = channel.getSchedules().get(osId);
					int l = (int) Math.ceil(ov.getLength());
					LocalDateTime end = os.getDt().plusSeconds(l);
					rv+=" between "+os.getDt()+" and "+end;
				}
				
			}
			
			return rv;
			
		} catch(Exception e) {
			log.error("Error "+e);
			return "Error looking up the video\r\n"+defaultRv;
		}

	}
	
	private static String ODescriptionHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showDMCA_ODescHelp();'></i>";
		rv+="<div id='oDescriptionHelp' hidden>";
		rv+="Describe the copyright you are claiming to have been infringed.<br>";
		rv+="If you are not the copyright holder state the copyright holder.<br>";
		rv+="</div>";
		return rv;
	}
	
	private static String TDescriptionHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showDMCA_TDescHelp();'></i>";
		rv+="<div id='tDescriptionHelp' hidden>";
		rv+="Enter the channel where this video was witnessed along with the date and time.<br>";
		rv+="</div>";
		return rv;
	}

}
