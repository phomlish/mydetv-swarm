package broadcast.web.home;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.business.UserHelper;
import broadcast.business.VideosHelpers;
import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/p2p")
public class P2PPage {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(P2PPage.class);
	@Inject private Config config;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private VideosHelpers videosHelpers;
	
    @GET
	public Response getSession(@Context HttpServletResponse resp, @Context HttpServletRequest req, @QueryParam("channel") String strChannel, @QueryParam("state") String state) {	
        
    	SessionUser sessionUser = userHelper.getSessionUser(req);		
		if(! sessionUser.isAuthenticated())
			return userHelper.redirectLogin(sessionUser);
		
		OneMydetvChannel channel = webHelper.getChannelFromString(strChannel);
							
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String extraHeaders = "";
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/emojione/extras/css/emojione.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/emojione/lib/js/emojione.min.js'></script>\n";		
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/emojionearea/dist/emojionearea.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/emojionearea/dist/emojionearea.min.js'></script>\n";
		
		String addnow = "";
		if(config.getInstance().toLowerCase().contains("dev"))
			addnow="?now="+System.currentTimeMillis();
		extraHeaders+="<script src='/s/js/swarm-p2p.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-rtc.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-u2u.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-reporting.js"+addnow+"'></script>\n";
			
		String rv = webHelper.beginHtml(sessionUser, "p2p", "p2p", extraHeaders);
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-2'>";
		rv+="chat ";
		rv+=WebHelper.ChatHelp();
		rv+="</div>";
		rv+="<div class='col-sm-5 center smallfont'>"+channel.getName()+"</div>";
		rv+="<div class='col-sm-5 center smallfont' id='videoTitle'>";
		rv+=videosHelpers.videoTitleDropdown(sessionUser, channel);
		rv+="</div>";
		rv+="</div>\n";
		
		rv+="<div class='row mainWindowRow'>\n";
		
		rv+="<div class='col-sm-2' id='personColumn'>";
		rv+="<div id='people'></div>";
		rv+="</div>\n"; // person column
		
		rv+="<div class='col-sm-4' id='chatColumn'>";
		rv+="<div class='message_box' id='message_box'></div>";					
		rv+="</div>\n"; // <!-- chat column -->";
		
		rv+="<div class='col-sm-6 center' id='videoColumn'>\n";
		
		rv+="<div id='progressRowTurn' class='row d-none'>\n";
		rv+="<div class='col-sm-12 center' id='progressColumn'>\n";
		rv+="<div id='progressTurn' class='progress progressLarge'>\n";
		rv+="<div class='progress-bar progress-bar-orange' id='progress-bar-turn' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%'></div>\n";
		rv+="</div>\n"; // progress
		rv+="</div>\n"; //progressColumn
		rv+="</div>\n"; //progressRow
		
		rv+="<div id='progressRowBw' class='row d-none'>\n";
		rv+="<div class='col-sm-12 center' id='progressColumn'>\n";
		rv+="<div id='progressBw' class='progress progressLarge'>\n";
		rv+="<div class='progress-bar progress-bar-green' id='progress-bar-bw' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%'></div>\n";
		rv+="</div>\n"; // progress
		rv+="</div>\n"; //progressColumn
		rv+="</div>\n"; //progressRow
		
		rv+="<div class='row videoRow'>\n";
		rv+="<div class='col-sm-12 center' id='remoteVideoColumn'>\n";
		rv+="<div class='stream videoLarge' id='remoteVideoStream'>\n";
		rv+="<img id='remoteVideoDancer' class='videoDancer' src='/s/img/dancer.gif'>\n";
		rv+="<img id='remoteVideoSpinner' class='videoSpinner d-none' src='/s/img/spinner.gif'>\n";
		rv+="<video id='remoteVideo' class='videoLarge d-none' controls></video>\n";
		rv+="</div>\n"; // remoteVideoStream
		rv+="</div>\n"; // remoteVideoColumn
		rv+="</div>\n"; // videoRow
		
		rv+="</div>\n"; // videoColumn
		
		rv+="</div>\n"; // mainRow
		rv+=webHelper.chatRow();
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();		
		
		rb.entity(rv);
		
		return rb.build();

    }
    
}
