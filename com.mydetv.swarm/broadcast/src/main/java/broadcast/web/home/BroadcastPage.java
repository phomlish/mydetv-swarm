package broadcast.web.home;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.broadcaster.PlaySoundHelpers;
import broadcast.business.UserHelper;
import broadcast.business.VideosHelpers;
import broadcast.business.WebHelper;
import broadcast.janus.JClient;
import broadcast.master.JukeboxActions;
import broadcast.master.MasterServer;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/broadcast")
public class BroadcastPage {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(P2PPage.class);
	@Inject private Config config;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	@Inject private JukeboxActions jukebox;
	@Inject private VideosHelpers videosHelpers;
	@Inject private PlaySoundHelpers playSoundHelpers;
	
    @GET
	public Response getSession(@Context HttpServletResponse resp, 
			@Context HttpServletRequest req, 
			@QueryParam("channel") String strChannel) {	
        	
    	SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated())
			return userHelper.redirectLogin(sessionUser);
		
		OneMydetvChannel mydetvChannel = webHelper.getChannelFromString(strChannel);
		sessionUser.setBroadcastChannel(mydetvChannel);
		ResponseBuilder rb = Response.status(200);
		rb = userHelper.addHeaders(sessionUser, rb);
		
		String addnow = "";
		if(config.getInstance().toLowerCase().contains("dev"))
			addnow="?now="+System.currentTimeMillis();
		
		String extraHeaders = "";		
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/emojione/extras/css/emojione.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/emojione/lib/js/emojione.min.js'></script>\n";		
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/emojionearea/dist/emojionearea.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/emojionearea/dist/emojionearea.min.js'></script>\n";
			
		extraHeaders+="<script src='/s/node_modules/datatables.net/js/jquery.dataTables.min.js'></script>\n";
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/datatables.net-dt/css/jquery.dataTables.min.css'>\n";
		extraHeaders+="<script src='/s/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js'></script>\n";
		extraHeaders+="<link rel='stylesheet' href='/s/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css'>\n";
		
		extraHeaders+="<script src='/s/js/swarm-p2p.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-rtc.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-u2u.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-broadcast.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-localvideo.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-reporting.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-sounds.js"+addnow+"'></script>\n";
		extraHeaders+="<script src='/s/js/swarm-api-sounds.js"+addnow+"'></script>\n";
		
		String rv = webHelper.beginHtml(sessionUser, "Broadcast", "broadcast", extraHeaders);
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-2'>";
		rv+="chat ";
		rv+="</div>";
		rv+="<div class='col-sm-5 center smallfont'>"+mydetvChannel.getName()+"</div>";
		rv+="<div class='col-sm-5 center smallfont' id='videoTitle'>";
		rv+=videosHelpers.videoTitleDropdown(sessionUser, mydetvChannel);
		rv+="</div>";
		rv+="</div>\n";

		rv+="<div class='row mainWindowRow'>\n";
		
		rv+="<div class='col-sm-2' id='personColumn'>\n";
		rv+="<div id='people'></div>\n";
		rv+="</div>\n"; // person column
		
		rv+="<div class='col-sm-4' id='chatColumn'>\n";
		rv+="<div class='message_box' id='message_box'></div>\n";					
		rv+="</div>\n"; // <!-- chat column -->";
		
		rv+="<div class='col-sm-6 center' id='videoColumn'>\n";
		
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
    	rv+="<div id='channelId' class='d-none'>"+mydetvChannel.getChannelId()+"</div>\n";
    	rv+="<div id='channelType' class='d-none'>"+mydetvChannel.getChannelType()+"</div>\n";
    	
		// controls
    	
		if(mydetvChannel.getChannelType()==0)
			rv+=getControlsRemote(jClient);
		else
			rv+=getControlsStudio(jClient, sessionUser);

		
		rv+="<div class='row videoRow'>\n";	
		// remote
		if(mydetvChannel.getChannelType()==0) {
			rv+="<div class='col-sm-6 center' id='localVideoColumn'>\n";
			rv+="<div class='stream videoSmall' id='localVideoStream'>\n";
			rv+="<img id='localVideoDancer' class='videoDancer' src='/s/img/dancer.gif'>\n";
			rv+="<img id='localVideoSpinner' class='videoSpinner d-none' src='/s/img/spinner.gif'>\n";
			rv+="<video id='localVideo' class='videoSmall d-none' autoplay conrols></video>\n";
			rv+="</div>\n"; // localStream
			rv+="</div>\n"; // localVideoColumn
			
			rv+="<div class='col-sm-6 center' id='remoteVideoColumn'>\n";
			rv+="<div class='stream videoSmall' id='remoteVideoStream'>\n";
			rv+="<img id='remoteVideoDancer' class='videoDancer' src='/s/img/dancer.gif'>\n";
			rv+="<img id='remoteVideoSpinner' class='videoSpinner d-none' src='/s/img/spinner.gif'>\n";
			rv+="<video id='remoteVideo' class='videoSmall d-none' autoplay controls></video>\n";
			rv+="</div>\n"; // remoteVideoStream
			rv+="</div>\n"; // remoteVideoColumn
			
			rv+="</div>\n"; // row
						
			rv+="<div class='row'>\n";
			rv+="<div class='col-sm-6 center'>local</div>\n";
			rv+="<div class='col-sm-6 center'>remote</div>\n";
		}
		// studio
		else {				
			rv+="<div class='col-sm-12 center' id='remoteVideoColumn'>\n";
			rv+="<div class='stream videoLarge' id='remoteVideoStream'>\n";
			rv+="<img id='remoteVideoDancer' class='videoDancer' src='/s/img/dancer.gif'>\n";
			rv+="<img id='remoteVideoSpinner' class='videoSpinner d-none' src='/s/img/spinner.gif'>\n";
			rv+="<video id='remoteVideo' class='videoLarge d-none' autoplay controls></video>\n";
			rv+="</div>\n"; // remoteVideoStream
			rv+="</div>\n"; // remoteVideoColumn		
		}	
		rv+="</div>\n"; // videoRow
		
		rv+="</div>\n"; // videoColumn
		
		rv+="</div>\n"; // mainWindowRow
		
		rv+=webHelper.chatRow();
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		rb.entity(rv);		
		return rb.build();
    }
    
	public String getControlsRemote(JClient jClient) {
		String rv="";
		String checked="";
		rv+="<div class='row' id='controlsRow'>\n";	
		rv+="<div class='col-sm-12 left controlsColumn'>\n";
		
		rv+=drawActive(jClient);
		
		// my local video
		rv+="<label class='lblBcast'>my local video ";
		rv+="<input id='changeLocalVideo' type='checkbox' onchange='onChangeLocalVideo();' autocomplete='off'>";
		rv+="</label><br />";

		rv+="<label class='lblBcast'>send my local video ";
		rv+="<input name='sourceLive' id='sourceLive' type='checkbox' disabled onchange='onRemoteSourceLive();' autocomplete='off'>";
		rv+="</label><br />";
		
		// sid
		rv+="<label class='lblBcast'>send station identification ";
		String disabled="";
		if(jClient==null) 			{ checked=""; disabled="disabled"; }
		else if(jClient.isLive()) 	{ checked=""; disabled=""; }
		else 						{ checked="checked"; disabled="disabled"; }
		
		rv+="<input name='sid' id='sid' value='sid' type='checkbox' "+checked+" "+disabled+" onchange='onRemoteSid();' autocomplete='off'>";
		rv+="</label><br />";
		
		rv+="</div></div>"; // controlsColumn controlsRow
		return rv;
	}
    
    private String getControlsStudio(JClient jClient, SessionUser sessionUser) {
    	String rv="";
    	String checked="";
		
    	// draw lights
    	rv+="<div class='row justify-content-center' id='lightsRow'>";	
		rv+="<div class='col-sm-3 center flex-center' id='lightsColumn'>";		
    	MasterServer masterServer=null;
    	if(jClient!=null)
    		masterServer = (MasterServer) jClient.getMydetvChannel().getMasterServer();
		if(masterServer!=null) {
			rv+="<div class='lights' id='lights'>\n";
		    rv+=masterServer.updateBrowserLights();
		}
		else {
			rv+="<div class='lights d-none' id='lights'>\n";
			rv+=drawOffLights();
		}
    	rv+="</div></div></div>\n"; // lights lightsColumn lightsRow
    	
    	rv+="<div class='row' id='controlsRow'>\n";	// 3 columns
		rv+="<div class='col-sm-3 left controlsColumn'>\n";	
		
		rv+=drawActive(jClient);
		
    	rv+="<label class='lblBcast'>Station Identification ";
    	if(jClient==null) checked="checked='checked'";
    	else if(jClient.isLive()) checked=""; 
    	else checked="checked";
    	rv+="<input name='videoSource' id='sid' value='sid' type='radio' "+checked+" onchange='onStudioSid();'autocomplete='off'>";
		rv+="</label><br />";
    	
    	rv+="<label class='lblBcast'>Live ";
    	if(jClient==null) checked="";
    	else if(jClient.isLive()) checked="checked='checked'"; 
    	else checked="";
    	rv+="<input name='videoSource' id='live' value='live' type='radio' "+checked+" onchange='onStudioLive();' autocomplete='off'";
		rv+="</label><br />";

    	rv+="<label class='lblBcast'>Recording ";
    	if(jClient==null) 
    		checked="disabled='disabled'";
    	else if(jClient.getMydetvChannel().isRecording()) 
    		checked="checked='checked'";
    	else if(! jClient.isLive())
    		checked="disabled='disabled'";		
		rv+="<input class='lblBcast' id='recording' type='checkbox' "+checked+" onchange='onRecording();' autocomplete='off'><br />";
		rv+="</label><br />";
		  	
		rv+="</div>\n"; // close the controls column

		// jukebox column
		rv+="<div class='col-sm-4 left controlsColumn' id='jukeboxColumn'>\n";
		
    	String profile = jukebox.getProfile();
		
		rv+="<div id='jukebox'>Jukebox Current Profile <b>"+profile+"</b></div><br />";
		
		rv+="<label class='lblBcast'>Silence ";
		rv+="<input class='lblBcast' type='radio' name='profile' id='silence' value='silence' onChange='onProfileSilence();' autocomplete='off'";
		if(profile.equals("silence"))
			rv+=" checked ";
		rv+="/>";
		rv+="</label><br />";
		
		rv+="<label class='lblBcast'>Play ";
		rv+="<input class='lblBcast' type='radio' name='profile' id='play' value='play' onChange='onProfilePlay();' autocomplete='off'";
		if(! profile.equals("silence"))
			rv+=" checked ";
		rv+="/>";
		rv+="</label>";
		
    	rv+="</div>\n"; // jukeboxColumn
    	
    	// play sound column
    	rv+="<div class='col-sm-5 left controlsColumn' id='soundColumn'>\n";
    	rv+=playSoundHelpers.drawPlaySoundColumn(sessionUser, "broadcast");	
    	rv+="</div>\n"; // play sound column
    	
    	rv+="</div>\n"; // controls row
    	return rv;
    }
    
    private String drawActive(JClient jClient) {
    	String rv="";
    	rv+="<label class='lblBcast'>active ";
		String checked="";
		if(jClient!=null) checked="checked";
		rv+="<input id='channelActivate' type='checkbox' "+checked+" onchange='onChannelActivate();' autocomplete='off'>";
		rv+="</label><br />";
    	return rv;
    }
    private String drawOffLights() {
    	String rv="";
		List<String>  lights = Arrays.asList("yellow", "green", "red", "blue", "white");
    	for(String light : lights) {
    		rv+="<div class='light'>\n";
    		rv+="<img id='light-"+light+"' src='/s/icons/"+light+".gif'>\n";
    		rv+="</div>\n";
    	}
    	return rv;
    }
    
}
