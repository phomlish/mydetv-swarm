package broadcast.web.home;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import broadcast.Blackboard;
import broadcast.business.BrowserHelpers;
import broadcast.business.UserHelper;
import broadcast.business.WebHelper;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

@JsonAutoDetect
@Path("/")
public class HomePage  {

	@Inject private Blackboard blackboard;
	@Inject private UserHelper userHelper;
	@Inject private WebHelper webHelper;
	
	//private static final Logger log = LoggerFactory.getLogger(Home.class);
	@GET
	public Response home(@Context HttpServletRequest req, @Context HttpServletResponse resp) throws ServletException, IOException
	{

		SessionUser sessionUser = userHelper.getSessionUser(req);
		if(! sessionUser.isAuthenticated()) {
			if(sessionUser.isLoggedIn())
				return userHelper.redirectChangePassword(sessionUser);
			else
				return userHelper.redirectLogin(sessionUser);
		}
		
		if(sessionUser.getWebUser().getLoggedInEmail().getDtVerified() == null)
			return userHelper.redirectToPage(sessionUser, "/auth/verify-email");
		
		String rv = webHelper.beginHtml(sessionUser, "Home", "home");
		
		rv+="<div class='row'>";
		rv+="<div class='col-sm-2'></div>";
		rv+="<div class='col-sm-8'>";
		rv+="<noscript><center><font size='3' color='red'>";
		rv+="you don't have javascript enabled.  This site requires javascript";
		rv+="</font></center></noscript><br><br>";
		
		String userAgent = req.getHeader("user-agent");
		if(!BrowserHelpers.isBrowserTypeOK(userAgent)) {
			rv+="<br><center>Only <b><i>DESKTOPS</i></b> using Firefox or Chrome are fully supported.<br>";
			rv+="</center>\n";
		} else if(! BrowserHelpers.isBrowserCurrent(userAgent)) {
			rv+="<br><center>Only <b><i>DESKTOPS</i></b> using the <i>latest</i> versions of Firefox or Chrome are fully supported.<br>";
			rv+="Please update your browser.<br>";
			rv+="</center>\n";
		}
		else {
			rv+=channels(sessionUser);
		}
	
		rv+="</div>";
		rv+="</div>\n";
		
		rv+=webHelper.closeMainContainerAddFooterCloseBodyCloseHtml();
		
		ResponseBuilder rb = Response.status(200);
		rb=userHelper.addHeaders(sessionUser, rb);
		rb.entity(rv);		
		return rb.build();
	}

	private String channels(SessionUser sessionUser) {	
		String rv="";
		
		rv += "<div class='row'>";
		rv+="<div class='col-sm-2'></div>";
		rv+="<div class='col-sm-8 center'>Currently Available Channels</div>";
		rv+="</div>\n"; // row
		
		rv += "<div class='row'>\n"; // full channel row
		rv+="<div class='col-sm-2'></div>\n";
		rv+="<div class='col-sm-8 mbpOffWhite'>\n"; // full channel column
		Integer countChannels = 0;

		rv+="<div class='container'>";  // channels container
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			if(! mydetvChannel.isActive())
				continue;
			
			if(mydetvChannel.getRestricted())
				if(! (sessionUser.getWebUser().isOver18()))
					continue;
			
			countChannels++;
				
			rv+="<div class='row'>\n";
			
			rv+="<div class='col-sm-12'>\n";
			rv+="<a href='/p2p?channel="+mydetvChannel.getChannelId()+"'>"+mydetvChannel.getName()+"</a>\n";
			rv+="</div>\n";

			rv+="</div>"; // row - in for
		}
		rv+="</div>"; // channels container
		
		if(countChannels == 0)
			rv+="<div class='row'><div class='column-sm-12 center'>There are no active broadcasts at this time</div></div>";
		
		rv+="</div>\n"; // full channel column
		rv+="</div>";  // full channel row

		return rv;
	}
	
}
