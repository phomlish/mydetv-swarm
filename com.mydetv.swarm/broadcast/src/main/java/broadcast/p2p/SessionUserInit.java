package broadcast.p2p;

import java.time.LocalDateTime;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.business.ChatHelpers;
import broadcast.business.PeopleHelpers;
import broadcast.business.UserMessageHelpers;
import broadcast.business.WebHelper;
import broadcast.business.BrowserHelpers;
import broadcast.janus.JClient;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;
import shared.db.DbChannels;
import shared.db.DbLog;

public class SessionUserInit {

	@Inject private Blackboard blackboard;
	@Inject private ChatHelpers chatHelpers;
	@Inject private DbChannels dbChannels;
	@Inject private VideoWaiters videoWaiters;
	@Inject private PeopleHelpers peopleHelpers;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private WsWriter wsWriter;
	@Inject private DbLog dbLog;
	private static final Logger log = LoggerFactory.getLogger(SessionUserInit.class);
	
	public void doInit(Session session, JSONObject response) {
		
		Integer hashcode = Integer.valueOf(session.hashCode());
		
		Integer channelId = response.getInt("channel");
		String mydetv = response.getString("mydetv");
		
		// validate: make sure we know about this sessionUser
		if(! blackboard.getSessionUsers().containsKey(mydetv)) {
			String msg = "can't init, don't know about you "+mydetv;
			log.error(msg);
			wsWriter.sendStatusMessage(session, msg);
			return;
		}
		SessionUser su = blackboard.getSessionUsers().get(mydetv);
		
		// we have our sessionUser, time to them lockdown
		log.info("initializing "+su.log());
		log.warn("synchronized su");
		synchronized(su.getLock()) {
		
			// validate: make sure sessionUser does not have an existing wsConnection
			if(su.getWebSocketConnection() != null) {
				log.error("can't init, sessionUser already has a webSocketConnection "
					+su.getMydetv()+" old:"+su.getWebSocketConnection().getHashcode()+" new:"+hashcode);
				wsWriter.sendStatusMessage(session, "you can only vew one channel at a time.<br>Try to find the tab viewing the channel.");
				return;
			}
	
			// validate: make sure we know about this wsConnection in the blackboard
			if(! blackboard.getWebSocketConnections().containsKey(hashcode)) {
				log.error("can't init, don't know about webSocketConnection "+hashcode);
				return;
			}
			WebSocketConnection wsConnection = blackboard.getWebSocketConnections().get(hashcode);
			Session wss = (Session) wsConnection.getWebsocketSession();
			wsConnection.setRemote(wss.getRemoteAddress().getAddress().getHostAddress()+":"+wss.getRemoteAddress().getPort());
	
			// validate: make sure his channel maps out
			if(! blackboard.getMydetvChannels().containsKey(channelId)) {
				log.error("can't init, channel "+channelId+" does not map out "+hashcode);
				return;
			}
			OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channelId);
	
			// validate: make sure he is not already a socket user
			if(mydetvChannel.getSessionUsers().contains(hashcode)) {
				log.error("can't init, already a channel socket user "+hashcode);
				return;
			}
	
			// make sure he doesn't already have a mydetv channel (one session per web session)
			if(su.getMydetvChannel() != null) {
				log.error("can't init, is already in a mydetvChannel "+hashcode);
				return;
			}
			
			if(mydetvChannel.getSessionUsers().contains(su.getMydetv())) {
				log.error("he's already in the channel's list of users");
				// maybe not an error since we don't remove them right away...
			}
	
			dbLog.infoWebUser("entered channel "+mydetvChannel.getChannelId(), su);
			log.info("finished validating entry to channel "+su.log());
			
			// tests passed, add him in
			mydetvChannel.getSessionUsers().put(su.getMydetv(), su);
			su.setWebSocketConnection(wsConnection);
			su.setMydetvChannel(mydetvChannel);
			su.setDetectRTC(response.getJSONObject("DetectRTC"));
			BrowserHelpers.AnalyzeDetectRTC(su);
			log.trace("detectRTC:"+WebHelper.PrettyPrint(su.getDetectRTC().toString()));
			su.setDatafileDownloadTime(response.getLong("datafileDownloadTime"));
			log.trace("datafileDownloadTime:"+su.getDatafileDownloadTime());
			su.setDtEnteredRoom(LocalDateTime.now());
			String pathname = response.getString("pathname");
			log.trace("pathname:"+pathname);
			if(pathname.equals("/broadcast"))
				su.setInBroadcastingRoom(true);
			
			wsConnection.setSessionUser(su);
			Integer channelWebUsersId = dbChannels.addWebUser(su);
			su.getWebUser().setDbChannelWebUsersId(channelWebUsersId);
	
			peopleHelpers.drawPeople(mydetvChannel, su);
			chatHelpers.showRecentChats(su);
	
			// send chats to everyone in the channel that someone entered the room
			String message = su.log()+" entered the room";
			chatHelpers.sendAdminChat(su, mydetvChannel, message);
			
			if(su.getGoodWebrtc())
				userMessageHelpers.sendUserMessage(su, "info", "welcome");
			else
				userMessageHelpers.sendUserMessage(su, "warn", "your browser is not fully webrtc compliant");
			// TODO: create a thread for each session user so we can wait for janus to become live
			JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			if(jClient != null) // && jClient.isReady())
				su.setWaiterQueueSize(0);
			
			videoWaiters.connectViewersWaiting(mydetvChannel);
		}
		log.warn("unsynchronized su");
	}
	
}
