package broadcast.p2p;

import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.Bandwidth;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class BWTests {

	private static final Logger log = LoggerFactory.getLogger(BWTests.class);
	@Inject private BwHelpers bwHelpers;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private P2PHelpers p2pHelpers;
	
	public void doBandwidthTests(OneMydetvChannel channel, ArrayList<SessionUser> candidates) {
		SessionUser waiter = channel.getCurrentWaiter();
		// TODO: seems we should have set this when we finished with it...
		for(Bandwidth bw : waiter.getBandwidthTestsToMe().values())
			bw.setActive(false);
		
		userMessageHelpers.sendUserMessage(waiter, "info", "finding best connection from "+candidates.size()+" candidates");
		
		for(SessionUser fromUserCandidate : candidates) {
			log.info(waiter.log()+" get/make bandwidth test from "+fromUserCandidate.log());
			bwHelpers.getOrMakeBandwidthTestToFrom(waiter, fromUserCandidate);
		}
		
		log.info(waiter.log()+" has getBandwidthTestsToMe:"+waiter.getBandwidthTestsToMe().size());
		
		// get the bw tests finished and pending
		ArrayList<Bandwidth> createdCandidates = new ArrayList<Bandwidth>();
		ArrayList<Bandwidth> finishedCandidates = new ArrayList<Bandwidth>();
		ArrayList<Bandwidth> failedCandidates = new ArrayList<Bandwidth>();
		for(Bandwidth bw : waiter.getBandwidthTestsToMe().values()) {			
			RtcConnection rtc = bw.getRtcConnection();
			SessionUser fromUser = rtc.getFromUser();
			// if the other guy is not in the room anymore, skip them
			if(fromUser.getMydetvChannel()==null || fromUser.getMydetvChannel()!=channel)
				continue;
			if(bw.getState().equals("finished"))
				finishedCandidates.add(bw);
			else if(bw.getState().equals("failed"))
				failedCandidates.add(bw);
			else if(bw.getState().equals("created"))
				createdCandidates.add(bw);
			else if(bw.getState().equals("inprogress"))
				log.error("bw state inprogress, should not happen");
			else
				log.error("failed to recognize bw state "+bw.getState());
		}
		
		log.info(waiter.log()+" candidates new:"+createdCandidates.size()
			+", finished:"+finishedCandidates.size()
			+", failed:"+failedCandidates.size());		

		// doing poor mans analytics select our candidates
		// if created > 1 then pick at most 3 to add to our list
		ArrayList<Bandwidth> chosenCandidates = new ArrayList<Bandwidth>();
		if(createdCandidates.size()>0) {

			ArrayList<Bandwidth> tmpCandidates = new ArrayList<Bandwidth>(createdCandidates);
			Collections.shuffle(tmpCandidates);
			Integer max = tmpCandidates.size();
			if(max>3) 
				max = 3;
			
			for(int i=0; i<max; i++)
				chosenCandidates.add(tmpCandidates.get(i));
		}

		log.info(waiter.log()+" new bandwidth tests:"+chosenCandidates.size());
		chosenCandidates.addAll(finishedCandidates);
		for(Bandwidth bw : chosenCandidates) 
			bw.setActive(true);
		
		// find first not finished BW test
		Bandwidth first=null;		
		for(Bandwidth bandwidth : chosenCandidates) {
			if(bandwidth.getState().equals("finished"))
				continue;
			
			if(first==null) {
				first=bandwidth;
				break;
			}
		}
		
		if(first==null) {
			log.info("looks like all the BW tests are already finished, find the best for "+waiter.log());
			bwHelpers.areAllBandwidthTestsFinished(waiter);
		}
		else {
			log.info("working on "+first.getRtcConnection().getToUser().log()
				+"    from "+first.getRtcConnection().getFromUser().log()
				+" state "+first.getState());
			p2pHelpers.initProgressbarBw(waiter, chosenCandidates.size(), finishedCandidates.size());	
			bwHelpers.startBandwidthTest(first);	
		}
	}

}
