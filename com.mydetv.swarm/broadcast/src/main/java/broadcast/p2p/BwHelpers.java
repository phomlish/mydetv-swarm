package broadcast.p2p;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.broadcast.Bandwidth;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class BwHelpers {

	@Inject private WsWriter wsWriter;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private P2PHelpers p2pHelpers;
	@Inject private ConnectionSmarts connectionSmarts;
	private static final Logger log = LoggerFactory.getLogger(BwHelpers.class);
	
	/**
	 * We look through all toUser's BandwidthTestsToMe for a matching fromUser<br>
	 * @param toUser
	 * @param fromUser
	 * @return
	 */
	public Bandwidth findBandwidthTestToFrom_To(SessionUser toUser, SessionUser fromUser) {
		Bandwidth found = null;
		for(Bandwidth bw : toUser.getBandwidthTestsToMe().values()) {
			if(bw.getRtcConnection().getFromUser().equals(fromUser))
				return bw;
		}
		return found;
	}
			
	/*
	 * getOrMakeBandwidthTestToFrom<br>
	 * 	 if we already did a test to/from then we'll return that bandwidthTest<br>
	 *   otherwise make a new bandwidthTest<br>
	 */
	public Bandwidth getOrMakeBandwidthTestToFrom(SessionUser toUser, SessionUser fromUser) {
		
		Bandwidth bw = findBandwidthTestToFrom_To(toUser, fromUser);

		if(bw==null) {
			log.info("creating bt to "+toUser.log()+" from "+fromUser.log());
			RtcConnection rtcConnection = new RtcConnection("bw", fromUser, toUser);
			bw = new Bandwidth(rtcConnection);
		}
		else {
			log.info("we found an existing bw to "
				+bw.getRtcConnection().getToUser().log()
				+" from "+bw.getRtcConnection().getFromUser().log()
				+",state:"+bw.getState()
				+",active:"+bw.isActive());
		}
		return bw;
	}	
	
	/*
	 * startBandwidthTest
	 *     if already finished check we are done and return
	 *     else, start this test
	 */
	public void startBandwidthTest(Bandwidth bandwidth) {
		RtcConnection rtc = bandwidth.getRtcConnection();
		// we might have added a finished bandwidth test, so don't do it again
		if(bandwidth.getState().equals("finished")) {
			areAllBandwidthTestsFinished(rtc.getToUser());	
			return;
		}

		SessionUser toUser = rtc.getToUser();
		SessionUser fromUser = rtc.getFromUser();
		
		log.info("starting bandwidth test from "+fromUser.log()+" to "+toUser.log());;
		userMessageHelpers.sendUserMessage(toUser, "info", "connection analyzing->"
		    +fromUser.getWebUser().getWuid()+"."+fromUser.getWebUser().getDbChannelWebUsersId());
		// let's make sure we reset offer/answer
		rtc.setAnswer(null);
		rtc.setOffer(null);
		wsWriter.sendSetupRTCConnection(toUser, rtc, "receiver");
		wsWriter.sendSetupRTCConnection(fromUser, rtc, "sender");
		bandwidth.setState("inprogress");
		bandwidth.setLastProgressInstant(LocalDateTime.now());
		rtc.setStarted(LocalDateTime.now());
	}

	// this will come from the receiver saying I'm ready, tell sender to start
	public void wsMessageBandwidthTestStart(JSONObject jsonObject, SessionUser toUser) {
		
		String cid = jsonObject.getString("cid");
		Bandwidth bt = toUser.getBandwidthTestsToMe().get(cid);
		RtcConnection rtc = bt.getRtcConnection();
		rtc.addState("connected");
		rtc.setConnected(true);
		log.info("sending bandwidthTestStart for "+rtc.getRtcUUID()
			+" to "+rtc.getFromUser().log());
		
		wsWriter.sendMydetvCustom(rtc.getFromUser(), rtc, "bandwidthTestStart", "");
	}

	//bandwidthTestFinished:2017-09-06T17:00:24.029Z 2017-09-06T17:00:24.211Z
	// sessionUser is the receiver; he sends this when the test is done
	// TODO: put a timeout on bandwidth tests
	// this bw test was going TO sessionUser	
	public void wsMessageBandwidthTestFinished(JSONObject jsonObject, SessionUser sessionUser) {
		String cid = jsonObject.getString("cid");
		Bandwidth bt = sessionUser.getBandwidthTestsToMe().get(cid);
		RtcConnection rtc = bt.getRtcConnection();

		String dtStart = jsonObject.getString("dtStart");
		String dtEnd = jsonObject.getString("dtEnd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		long dtDiff = 0L;
		try {
			Date s = sdf.parse(dtStart);
			Date e = sdf.parse(dtEnd);
			dtDiff = e.getTime() - s.getTime();			
		} catch (ParseException e1) {
			log.error("no can do start and end dates:"+e1);
		}	

		log.info("bandwidthTestFinished:"+dtStart+" "+dtEnd+" diff:"+dtDiff+" "+bt.log());
		userMessageHelpers.sendUserMessage(sessionUser, "info", "connection analyzation finished->"
		    +rtc.getFromUser().getWebUser().getWuid()
		    +"."+rtc.getFromUser().getWebUser().getDbChannelWebUsersId());
		
		// tell this guy he is finished
		bt.setState("finished");
		bt.setDtDiff(dtDiff);

		SessionUser toUser = rtc.getToUser();
		SessionUser fromUser = rtc.getFromUser();
		if(! sessionUser.equals(toUser)) {
			log.error("I misunderstood");
		}
		
		log.info("telling toUser "+toUser.log()+" and fromUser "+fromUser.log()+" to close this connection");
		wsWriter.sendRemoveRTCConnection(fromUser, rtc);
		wsWriter.sendRemoveRTCConnection(toUser, rtc);
		rtc.addState("removed");
		rtc.setRemoved(true);

		// are we all done?
		areAllBandwidthTestsFinished(toUser);		
	}

	/**
	 * walk up the CGVs adding in all the previous bandwidth tests from user to user until we reach tier 1
	 * we can ignore tier0 if it exists, everyone will have that duration
	 * 
	 * @param Bandwidth best
	 * @return Long : duration of all the bandwidths in the path
	 */
	// see all that logging?  yeah, it was hard to write.
	private Long getFullBandwdth(Bandwidth bw) {
		SessionUser fromUser = bw.getRtcConnection().getFromUser();
		SessionUser toUser = bw.getRtcConnection().getToUser();
		
		log.info("gathering fullbandwidth to "+toUser.log()+" from "+fromUser.log());

		// seed with this bandwidth test's duration
		Long fullBandwidthDuration = bw.getDtDiff();
		log.info("seeding with "+fullBandwidthDuration);
				
		ConnectionGroup cgv = fromUser.getCgStream();
		log.info("starting our upward trek with "+fromUser.log()+"'s tier "+cgv.getTier());
		
		// i: to prevent an endless loops bug
		int i=0;
		while(cgv!=null && i<10) {
			log.info("considering provider "+cgv.getProvider().log()+" tier "+cgv.getTier());
			
			// if we've reached tier2 we're done, just add his DatafileDownloadTime
			if(cgv.getTier().equals(2)) {
				log.info("tier2- adding datafileDownloadTime and we're done:"+toUser.getDatafileDownloadTime());			
				return (fullBandwidthDuration+toUser.getDatafileDownloadTime());
			}
		
			// this is rtc of provider's upstream
			//RtcConnection rtcUpstream = cgv.getUpstream();
			// if he has no upstream, we are done
			if(cgv.getUpstreams().size()!=1) {
//			if(rtcUpstream == null) {
				log.error("error can't happen, could not find Video rtcUpstream for "+cgv.getProvider().log());
				return fullBandwidthDuration;
			}
			
			RtcConnection rtcUpstream = cgv.getUpstreams().get(0);
			SessionUser btTo = 		rtcUpstream.getToUser();
			SessionUser btFrom = 	rtcUpstream.getFromUser();
			log.info("btTo:"+btTo.log()+", btFrom:"+btFrom.log());

			// find the bandwidth test for btTo to btFrom
			//Bandwidth bt = findBandwidth(btTo, btFrom);
			Bandwidth bt = findBandwidthTestToFrom_To(btTo, btFrom);
			if(bt == null) {
				log.error("meltdown, bt=null");
				return fullBandwidthDuration;
			}
			
			if(!bt.getRtcConnection().getToUser().equals(btTo)) {
				log.error("huh, found wrong toUser");
				return fullBandwidthDuration;
			}
			if(!bt.getRtcConnection().getFromUser().equals(btFrom)) {
				log.error("huh, found wrong fromUser");
				return fullBandwidthDuration;
			}			
			
			log.info("we need to add the bandwidth test "+bt.getDtDiff()+" from "+btFrom.log()+" to "+bw.getRtcConnection().getToUser().log());
			fullBandwidthDuration += bt.getDtDiff();

			// can we go up a tier?
			/*
			if(btFrom.getCgStream().getUpstream() == null)
				cgv=null;
			else
				cgv = btFrom.getCgStream().getUpstream().getToUser().getCgStream();
			*/
			if(btFrom.getCgStream().getUpstreams().size()!=1)
				cgv=null;
			else
				cgv=btFrom.getCgStream().getUpstreams().get(0).getToUser().getCgStream();
			
			i++;
		}
		if(i>9) {
			log.error("**** **** **** error our loop exploded");
		}
		log.info("returning "+fullBandwidthDuration);
		return fullBandwidthDuration;
	}
	
	// sender received dataChannelOnopen and has a file of 'size' ready to send
	public void wsMessageBandwidthTestSenderReady(JSONObject jsonObject, SessionUser fromUser) {
		String cid = jsonObject.getString("cid");
		Bandwidth bt = fromUser.getBandwidthTestsFromMe().get(cid);
		bt.setSize(jsonObject.getLong("size"));

		RtcConnection rtc = bt.getRtcConnection();
		String pl = "to:"+rtc.getToUser().log()+",from:"+rtc.getFromUser().log()+"'";

		rtc.setConnected(true);
		log.info("send bandwidthTestPrepare to "+rtc.getToUser().log()+pl);
		// connections are ready, we need receiver to respond back so we can time the test
		wsWriter.sendMydetvCustom(rtc.getToUser(), rtc, "bandwidthTestPrepare", bt.getSize().toString());
		log.info("sent bandwidthTestPrepare to "+rtc.getToUser().log()+pl);
		return;
	}

	/**
	 * check our list of CGBandwidth to see if they are all finished.
	 * If not, do another one
	 * If so, find the best and connect the video
	 * @param sessionUser
	 */
	public void areAllBandwidthTestsFinished(SessionUser sessionUser) {
		
		// are there more bt to do?
		for(Bandwidth bt2 : sessionUser.getBandwidthTestsToMe().values()) {
			if(bt2.isActive() && bt2.getState().contains("created")) {
				log.info("go do this bandwidth test "
					+" to "+  bt2.getRtcConnection().getToUser().log()
					+" from "+bt2.getRtcConnection().getFromUser().log());
				startBandwidthTest(bt2);
				return;
			}
		}

		log.info("bandwidth tests are all be done, find the best "+sessionUser.log());
		long dtDiff = Long.MAX_VALUE;
		Bandwidth best = null;
		Integer count=0;
		for(Bandwidth bt2 : sessionUser.getBandwidthTestsToMe().values()) {
			if(!bt2.isActive())
				continue;
			count++;
			Long fullBandwidth = getFullBandwdth(bt2);
			RtcConnection webrtcConnection = bt2.getRtcConnection();
			log.info("    bt to "+sessionUser.log()+" from "+webrtcConnection.getFromUser().log()+" : "+fullBandwidth);
			if(bt2.getState().contains("finished") && fullBandwidth<dtDiff) {
				best = bt2;
				dtDiff = fullBandwidth;
			}
		}

		if(best == null) {
			log.error("this is terrible, all bandwidth tests failed!");
			return;
		}

		log.info("best time:"+dtDiff+" out of "+count+" tests");
		log.info("connect video");
	
		p2pHelpers.hideProgressBarBw(sessionUser);
		userMessageHelpers.sendUserMessage(sessionUser, "info", "connection best->"
			+best.getRtcConnection().getFromUser().getWebUser().getWuid()
			+"."+best.getRtcConnection().getFromUser().getWebUser().getDbChannelWebUsersId()
			+" "+dtDiff);
		p2pHelpers.connectVideo(best);
	}
	

	
}
