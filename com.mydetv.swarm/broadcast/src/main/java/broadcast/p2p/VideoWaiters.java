package broadcast.p2p;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.business.UserMessageHelpers;
import shared.data.Config;
import broadcast.janus.JClient;
import broadcast.janus.JConnectViewer;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.OneWaiter;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class VideoWaiters {

	private static final Logger log = LoggerFactory.getLogger(VideoWaiters.class);
	@Inject private JConnectViewer jConnectViewer;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private P2PHelpers p2pHelpers;
	@Inject private CGHelpers cgHelpers;
	@Inject private VerifyCGBW verifyCGBW;
	@Inject private ConnectionSmarts connectionSmarts;
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private BWTests bwTests;
	@Inject private FCHelpers fcHelpers;
	
	// when someone starts presenting we need to connect the viewers waiting
	public void wsMessagePresenting(JSONObject jsonMessage, SessionUser sessionUser) {
		connectViewersWaiting(sessionUser.getMydetvChannel());
	}
	
	/**
	 * happens often, when someone enters or whenever we are not sure if there are ppl waiting
	 * find the ppl in the room waiting for a video connection<br>
	 *  only do one guy at a time<br>
	 *  
	 * @param mydetvChannel
	 */
	public void connectViewersWaiting(OneMydetvChannel channel) {
		if(!channel.isActive())
			return;
		log.info("***** gathering the waiting for "+channel.getName());
		synchronizedConnectViewersWaiting(channel);
		
	}
	
	// TODO: huge analytics opportunity
	private void synchronizedConnectViewersWaiting(OneMydetvChannel channel) {
		
		JClient jClient = (JClient) channel.getOjClient().getjClient();
		
		// if there are no users in the channel we are done
		if(channel.getSessionUsers().size()==0) {
			if(channel.getWaiters().size()>0)
				log.error("no sessionUsers in the channel but we have waiters, huh");
			return;
		}
	
		// check if the current waiter failed
		SessionUser curWaiter = channel.getCurrentWaiter();
		log.warn("perhaps we need to check if they failed");

		// we'll have to wait for the current waiter to finish
		if(channel.getCurrentWaiter()!=null) {
			log.info("we have to wait, we're still hooking up "+channel.getCurrentWaiter().log());
			return;
		}
		
		removeGoneWaiters(channel);
		addWaiters(channel);
		
		if(channel.getWaiters().size()==0) {
			log.info("no waiters, everyone must be happy");
			return;
		}
		
		Boolean foundSomeone=false;
		// find a waiter and hook them up
		for(Entry<LocalDateTime, OneWaiter> entry  : channel.getWaiters().entrySet()) {
			
			if(foundSomeone)
				continue;
			
			OneWaiter waiter = entry.getValue();
			SessionUser su = waiter.getSessionUser();
			
			// there might not be any candidates available for this waiter
			ArrayList<SessionUser> candidates = gatherCandidates(channel, waiter);
			
			if(candidates.size()==0) {
				log.info("still no candidates available for "+waiter.getSessionUser().log());
				userMessageHelpers.sendUserMessage(waiter.getSessionUser(), "error", "still hoping for a connection");
				continue;
			}
			
			foundSomeone=true;
			channel.setCurrentWaiter(waiter.getSessionUser());
			
			p2pHelpers.hideProgressBarTurn(channel.getCurrentWaiter());
						
			// either connect to media player or another user
			if(candidates.size()==1 && candidates.get(0).equals(jClient.getMe())) {
				userMessageHelpers.sendUserMessage(su, "info", "connecting to the media player");
				jConnectViewer.connectViewer(su);
			}
			else
				bwTests.doBandwidthTests(channel, candidates);
		}
		if(!foundSomeone) 
			log.info("We didn't find any waiters with candidates");
	}
	
	/**
	 * remove waiters that have left the channel
	 */
	private void removeGoneWaiters(OneMydetvChannel channel) {
		log.info("removeGoneWaiters begin "+channel.getWaiters().size());
		TreeMap<LocalDateTime, OneWaiter> tmpWaiters = new TreeMap<LocalDateTime, OneWaiter>(channel.getWaiters());
		for(Entry<LocalDateTime, OneWaiter> entry : tmpWaiters.entrySet()) {
			OneWaiter waiter = entry.getValue();
			SessionUser su = waiter.getSessionUser();
			if(su==null || su.getWebUser()==null 
			|| su.getMydetvChannel()==null 
			|| su.getMydetvChannel().getChannelId()!=channel.getChannelId()) {
				log.info("we lost a waiter ");
				subtactOneWaiter(channel, waiter);
				channel.getWaiters().remove(entry.getKey());
				continue;
			}
		}
		log.info("removeGoneWaiters end "+channel.getWaiters().size());
	}
	
	/**
	 * add people that are not yet in our list of waiters<br>
	 * we put new people with no slow links or ice failed at the top of the list<br>
	 * @param channel
	 */
	private void addWaiters(OneMydetvChannel channel) {
		
		log.info("addNewWaiters begin "+channel.getWaiters().size());
		ConcurrentHashMap<String, SessionUser> sessionUsers = channel.getSessionUsers();
		JClient jClient = (JClient) channel.getOjClient().getjClient();
		// add good waiters
		for(SessionUser su : sessionUsers.values()) {
			
			// skip the media player
			if(jClient.getMe()==su) continue;
			
			if(! config.getInstance().toLowerCase().contains("dev") 
			&& ! su.getGoodWebrtc()) continue;
			
			if(isSessionUserAlreadyInWaiters(channel, su)) continue;
			if(isSessionUserAlreadyConnected(channel, su)) continue;
			
			if(fcHelpers.checkFaultyConnection(su, jClient.getMe()))
				continue;
			addWaiter(channel, su);

		}
		// add everyone else (slowlinks and even icefailed)
		for(SessionUser su : sessionUsers.values()) {
			if(jClient.getMe()==su) continue;
			if(! su.getGoodWebrtc()) continue;
			if(isSessionUserAlreadyInWaiters(channel, su)) continue;
			if(isSessionUserAlreadyConnected(channel, su)) continue;
			addWaiter(channel, su);
		}
		log.info("addNewWaiters end "+channel.getWaiters().size());
	}
	
	private void addWaiter(OneMydetvChannel channel, SessionUser su) {
		log.info("new waiter:"+su.log());
		
		OneWaiter waiter = new OneWaiter(channel, su);

		waiter.setQuePosition(channel.getWaiters().size());
		waiter.setQueSize(channel.getWaiters().size()+1);
		channel.getWaiters().put(waiter.getWaiterKey(), waiter);
		
		String message = "waiting your turn "+waiter.getQuePosition()+" out of "+waiter.getQueSize();
		userMessageHelpers.sendUserMessage(su, "info", message);
		p2pHelpers.initProgressbarTurn(su, waiter.getQueSize());

	}
	
	// gets called when one gets connected or disappears
	public void subtactOneWaiter(OneMydetvChannel channel, OneWaiter waiter) {
		channel.getWaiters().remove(waiter.getWaiterKey());
		for(OneWaiter owaiter : channel.getWaiters().values()) {
			SessionUser su = owaiter.getSessionUser();
			// if they are not in the channel, skip them
			if(su==null || su.getWebUser()==null 
			|| su.getMydetvChannel()==null 
			|| su.getMydetvChannel().getChannelId()!=channel.getChannelId()) 
				continue;
			owaiter.setQuePosition(owaiter.getQuePosition()-1);
			if(owaiter.getQuePosition()<0) {
				log.error("I got scrazzled");
				continue;
			}
			String message = "waiting your turn "+owaiter.getQuePosition()+" out of "+owaiter.getQueSize();
			userMessageHelpers.sendUserMessage(su, "info", message);
			p2pHelpers.updateProgressbarTurn(su, owaiter.getQuePosition());
		}
	}
	
	// called from 
	//     wsMessageVideoConnected
	//     sessionUserClose->removeWaiter
	public void subtactOneWaiter(OneMydetvChannel channel, SessionUser targetWaiter) {
		
		// find this waiter
		OneWaiter waiter=null;
		for(OneWaiter twaiter : channel.getWaiters().values()) {
			if(twaiter.getSessionUser().equals(targetWaiter)) {
				waiter=twaiter;
				break;
			}
		}
		
		if(waiter==null) {
			log.error("meltdown, could not find this waiter");
		}
		else
			channel.getWaiters().remove(waiter.getWaiterKey());
		
		for(OneWaiter owaiter : channel.getWaiters().values()) {
			SessionUser su = owaiter.getSessionUser();
			// if they are not in the channel, skip them
			if(su==null || su.getWebUser()==null 
			|| su.getMydetvChannel()==null 
			|| su.getMydetvChannel().getChannelId()!=channel.getChannelId()) 
				continue;
			owaiter.setQuePosition(owaiter.getQuePosition()-1);
			if(owaiter.getQuePosition()<0) {
				log.error("I got scrazzled");
				continue;
			}
			String message = "waiting your turn "+owaiter.getQuePosition()+" out of "+owaiter.getQueSize();
			userMessageHelpers.sendUserMessage(su, "info", message);
			p2pHelpers.updateProgressbarTurn(su, owaiter.getQuePosition());
		}
	}
	
	private Boolean isSessionUserAlreadyInWaiters(OneMydetvChannel channel, SessionUser su) {
		// is he in the list of waiters?
		for(OneWaiter waiter : channel.getWaiters().values())
			if(waiter.getSessionUser()==su)
				return true;
		// is he the current waiter?
		if(channel.getCurrentWaiter()==su)
			return true;
		return false;
	}
	
	private Boolean isSessionUserAlreadyConnected(OneMydetvChannel channel, SessionUser su) {
		if(su.getCgStream().getUpstreams().size()==0)
			return false;
		
		//RtcConnection rtc = su.getCgStream().getUpstream();
		RtcConnection rtc = su.getCgStream().getUpstreams().get(0);
		if(rtc!=null && rtc.getConnected())
			return true;
		return false;
	}
	
	private ArrayList<SessionUser> gatherCandidates(OneMydetvChannel channel, OneWaiter waiter) {
		SessionUser su = waiter.getSessionUser();
		JClient jClient = (JClient) channel.getOjClient().getjClient();
		SessionUser jSessionUser = jClient.getMe();
		ConnectionGroup jcg = jSessionUser.getCgStream();
		
		// we need to create our own count ignoring presenters if any
		Integer cntUsedSlots = 0;
		for(RtcConnection rtc : jcg.getDownstreams()) {
			log.info("to:"+rtc.getToUser().log());
			if(! rtc.getToUser().isInBroadcastingRoom())
				cntUsedSlots++;
		}
		log.info("perhaps media player has slots "+cntUsedSlots+" used vs "+config.getMaxConnectionsTier1()+" max");

		if(cntUsedSlots<config.getMaxConnectionsTier1()) {
			//if(su.getCgStream().getUpstreamsErrored().contains(jSessionUser)) {
		//		log.info("cant use media player, it errored to "+su.log());
		//	}
		//	else {
				ArrayList<SessionUser> candidates = new ArrayList<SessionUser>();
				candidates.add(jSessionUser);
				return candidates;
		//	}
		}
		
		log.info("no slots from media player, try to find a tier2 or greater");
		ArrayList<SessionUser> candidates = gatherUpstreamConnectionCandidates(channel, su);
		// we have a list of candidates, could be large
		log.info(su.log()+" candidates:"+candidates.size());
		return candidates;
	}
	
	/**
	 * gather ppl in the channel that can send video to this guy
	 *  exclude the user who we're hooking up<br>
	 *  exclude ppl with no slots<br>
	 *  exclude presenters<br>
	 * 
	 * @param mydetvChannel
	 * @param exclude
	 * @return
	 */
	// TODO: exclude ppl with no upstream
	private ArrayList<SessionUser> gatherUpstreamConnectionCandidates(OneMydetvChannel channel, SessionUser su) {
		log.info("gatherConnectionCandidates");
		
		ArrayList<SessionUser> candidates = new ArrayList<SessionUser>();
		
		// extra logging, can remove someday
		for(SessionUser fromUserCandidate : channel.getSessionUsers().values()) {
			log.info("in room:"+fromUserCandidate.log());
		}
		
		for(SessionUser fromUserCandidate : channel.getSessionUsers().values()) {			
			// it's me, don't test to myself
			if(fromUserCandidate.getMydetv().contains(su.getMydetv())) 
				continue;

			// don't hook up ppl in presenting room, they cound be busy presenting we don't want to use their bandwidth
			if(fromUserCandidate.isInBroadcastingRoom())
				continue;
			
			// ignore the presenter.  probably redundant to the if above
			if(fromUserCandidate.getCgStream().getTier()==(-1))
				continue;
			
			ConnectionGroup cgv = fromUserCandidate.getCgStream();
			
			Integer maxSlots;
			if(cgv.getTier()==1)	maxSlots=config.getMaxConnectionsTier1();
			else					maxSlots=config.getMaxConnections();
			
			if(cgv.getDownstreams().size()>=maxSlots) {
				log.info("at max downstreams, skipping "+fromUserCandidate.log()+" "+cgv.getDownstreams().size());
				continue;
			}
		 			
			log.info("adding candidate "+fromUserCandidate.log());
			candidates.add(fromUserCandidate);
		}
		return candidates;
	}
	
	// when user leaves room or dc, make sure he wasn't a waiter
	public void removeWaiter(OneMydetvChannel channel, SessionUser su) {
		
		// is he a waiter?
		if(! isSessionUserAlreadyInWaiters(channel, su)) 
			return;
		
		channel.setCurrentWaiter(null);
		subtactOneWaiter(channel, su);
	}
	
}
