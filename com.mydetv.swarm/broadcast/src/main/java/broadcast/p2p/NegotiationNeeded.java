package broadcast.p2p;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class NegotiationNeeded {

	private static final Logger log = LoggerFactory.getLogger(NegotiationNeeded.class);
	@Inject private CGHelpers cgHelpers;
	
	public void wsMessageNegotiationNeeded(JSONObject response, SessionUser sessionUser) {
	
		// rtc connectionType
		String connectionType = response.getString("connectionType");
		String cid = response.getString("cid");
		RtcConnection rtc = cgHelpers.findRtcFromConnectionType(sessionUser, connectionType, cid);
		log.debug("negotiation needed from "+sessionUser.log()+" "+connectionType+":"+cid);
		log.error("REWRITE???");
		/*
		if(connectionType.equals("u2u"))
			offer.sendOfferOther(response, sessionUser);
		else if(connectionType.equals("video"))
			offer.sendOfferOther(response, sessionUser);
		else if(connectionType.equals("bandwidth"))
			offer.sendOfferBW(response, sessionUser);
		else
			log.error("didn't write for connectionType :"+connectionType);
			*/
	}
	


	

}
