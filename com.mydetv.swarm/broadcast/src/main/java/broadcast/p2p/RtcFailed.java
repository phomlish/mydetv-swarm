package broadcast.p2p;

import java.time.Duration;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class RtcFailed {

	private static final Logger log = LoggerFactory.getLogger(RtcFailed.class);
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private VideoWaiters videoWaiters;
	@Inject private CGHelpers cgHelpers;
	
	public void iceFailed(RtcConnection rtc) {
		log.warn("ice failed too many times "+rtc.log());
		rtcFailed(rtc);
	}
	
	public void stuckConnection(RtcConnection rtc) {
		log.warn("rtc got stuck "+rtc.log());
		rtcFailed(rtc);
	}
	
	private void rtcFailed(RtcConnection rtc) {		
		Duration duration = Duration.between(LocalDateTime.now(), rtc.getCreated());
		log.info("rtcFailed took "+duration.getNano());
		
		rtc.setErrored(true);
		
		SessionUser toUser = rtc.getToUser();
		SessionUser fromUser = rtc.getFromUser();
		
		String cType = rtc.getConnectionType();
		
		// send them a message
		switch(cType) {
		case "bw":
		case "video":
		case "janus":
			userMessageHelpers.sendUserMessage(toUser, "error", "stream failed");
			break;

		case "jtier0":
			userMessageHelpers.sendUserMessage(fromUser, "error", "broadcast connection failed");
			break;
		case "u2u":
			userMessageHelpers.sendUserMessage(toUser, "error", "error connecting from "+fromUser.log());
			userMessageHelpers.sendUserMessage(fromUser, "error", "error connecting to "+toUser.log());
			break;
		default:
			log.error("unrecognized type "+cType);
		}
		
		// now fix the connection group
		switch(cType) {
		case "bw":
		case "video":
		case "janus":
			//toUser.getCgStream().getUpstreamsErrored().add(fromUser);
			//fromUser.getCgStream().getDownstreamsErrored().add(toUser);
			videoWaiters.connectViewersWaiting(toUser.getMydetvChannel());
			break;
		case "jtier0":
			// nothing to be done here except have the user try again
			break;
		case "u2u":
			log.error("never did write this");
			break;
		default:
			log.error("unrecognized type "+cType);
		}
	}
}
