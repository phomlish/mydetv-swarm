package broadcast.p2p;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;
import shared.db.DbLog;

public class IceFailed {
	
	private static final Logger log = LoggerFactory.getLogger(IceFailed.class);
	@Inject private WsWriter wsWriter;
	@Inject private DbLog dbLog;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private RtcFailed rtcFailed;
	@Inject private CGHelpers cgHelpers;
	@Inject private FCHelpers fcHelpers;

	public void wsIceFailed(JSONObject response, SessionUser su) {
		// rtc connectionType
		String connectionType = response.getString("connectionType");
		String cid = response.getString("cid");
		
		RtcConnection rtc = cgHelpers.findRtcFromConnectionType(su, connectionType, cid);

		log.error("iceFailed for "+su.log()+" "+connectionType+":"+rtc.getConnectionType());
		fcHelpers.addFc(rtc, "iceFailed", su, 0);
		// remember we failed
			
		if(rtc.getFromUser()==su)
			rtc.addState("iceFailedFrom");
		else
			rtc.addState("iceFailedTo");
		
		if(su==rtc.getFromUser()) {
			rtc.getFailed().add(LocalDateTime.now());
			if(rtc.getFailed().size()>=3) {
				rtcFailed.iceFailed(rtc);
			}
			else {
				log.warn("sending ice reconnect"+rtc.log());
				userMessageHelpers.sendUserMessage(rtc.getFromUser(), "warn", "ice restart");
				userMessageHelpers.sendUserMessage(rtc.getToUser(), "warn", "ice restart");
				wsWriter.createOfferRestart(su, rtc.getRtcUUID());
				dbLog.warnWebUser("iceRestart", su);
				rtc.setStarted(LocalDateTime.now());
			}
		}

	}
	
}
