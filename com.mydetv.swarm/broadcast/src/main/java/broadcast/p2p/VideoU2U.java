package broadcast.p2p;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.business.PeopleHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

/**
 * check out our document U2U for details<br>
 * A: requester<br>
 * B: requestee<br>
 * @author phomlish
 *
 */
public class VideoU2U {
	
	private static final Logger log = LoggerFactory.getLogger(VideoU2U.class);
	@Inject private WsWriter wsWriter;
	@Inject private Blackboard blackboard;
	@Inject private PeopleHelpers peopleHelpers;
	@Inject private CGHelpers cgHelpers;
	
	public void u2u(JSONObject message, SessionUser su) {
		String action = message.getString("action");
		switch(action) {
		case "initialize":
			sendInitialize(message, su);
			break;		
		case "accept":
			accepted(message, su);
			break;
		default:
			log.error("what privateVideo action is "+action);
		}
	}
	// TODO: make sure they are not already sending
	/**
	 * We setup an RTC from the requester to the requestee
	 * @param message
	 * @param userA
	 */
	private void sendInitialize(JSONObject message, SessionUser userA) {

		
		String strOther = message.getString("person");
		log.info("trying to start private video with "+strOther);

		JSONObject jo = new JSONObject()
			.put("verb", "u2u")
			.put("action", "failed");
		
		if(userA.getWebUser().getDtMuted()!=null) {
			log.error("error, you have been mutted by an admin");
			jo.put("message", "you have been mutted by an admin");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		
		if(! userA.haveCamera()) {
			log.error("error, requester has no webcam");
			jo.put("message", "you need to have a webcam");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		
		OneMydetvChannel mydetvChannel = userA.getMydetvChannel();
		if(mydetvChannel==null) {
			log.error("error, you need to be in a channel");
			jo.put("message", "you need to be in a channel");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		
		SessionUser userB = null;
		for(SessionUser tsu : blackboard.getSessionUsers().values()) {
			if(tsu.getMydetvChannel()==null)
				continue;
			if(! tsu.getMydetvChannel().equals(mydetvChannel))
				continue;
			if(strOther.equals(tsu.getWebUser().getWuid()+"-"+tsu.getWebUser().getDbChannelWebUsersId())) {
				userB=tsu;
				break;
			}
		}
		
		if(userB==null) {
			log.error("I could not find that user");
			jo.put("message", "I could not find that user");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		if(userB.equals(userA)) {
			log.error("you can't video to yourself");
			jo.put("message", "you can't video to yourself");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		if(userB.getWebUser().getDtMuted()!=null) {
			log.error("error, they have been mutted by an admin");
			jo.put("message", "they have been mutted by an admin");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		if(peopleHelpers.isBlocked(userA, userB)) {
			log.info("the user has refused");
			jo.put("message", "refused");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		if(! userB.haveCamera()) {
			log.error("error, requestee has no webcam");
			jo.put("message", "they don't have a webcam");
			wsWriter.sendMessage(userA, jo);
			return;
		}
		
		jo.put("action", "startCamera");
		wsWriter.sendMessage(userA, jo);
			
		ConnectionGroup cgA = userA.getCgU2U();
		ConnectionGroup cgB = userB.getCgU2U();
		
		RtcConnection rtcA2B = new RtcConnection("u2u", userA, userB);
		
		cgA.getDownstreams().add(rtcA2B);
		cgB.getUpstreams().add(rtcA2B);
		
		sendRequestVideoChat(rtcA2B);
		rtcA2B.setStarted(LocalDateTime.now());
	}

	// **** **** ****
	// send messages
	private void accepted(JSONObject message, SessionUser userB) {
		String cid = message.getString("cid");
		
		// find the rtc from our initialize
		//RtcConnection rtcA2B = userB.findRtc(cid);
		RtcConnection rtcA2B = cgHelpers.findRtcFromConnectionType(userB, "u2u", cid);
		SessionUser userA = rtcA2B.getFromUser();
		log.info("userB "+userB.log()+" accepted from "+userA.log());

		wsWriter.sendSetupRTCConnection(userA, rtcA2B, "sender");
		wsWriter.sendSetupRTCConnection(userB, rtcA2B, "receiver", userA.getWebUser().getWuid()+"-"+userA.getWebUser().getDbChannelWebUsersId());
		
		// create a new rtc
		RtcConnection rtcB2A = new RtcConnection("u2u", userA, userB);

		ConnectionGroup cgA = userA.getCgU2U();
		ConnectionGroup cgB = userB.getCgU2U();
		cgB.getDownstreams().add(rtcB2A);
		cgA.getUpstreams().add(rtcB2A);
		
		wsWriter.sendSetupRTCConnection(userB, rtcB2A, "sender");
		wsWriter.sendSetupRTCConnection(userA, rtcB2A, "receiver", userB.getWebUser().getWuid()+"-"+userB.getWebUser().getDbChannelWebUsersId());	
		rtcB2A.setStarted(LocalDateTime.now());
	}
	
	// ask the other guy, can we U2U
	private void sendRequestVideoChat(RtcConnection rtc) {
		log.info("requesting video from "+rtc.getFromUser().log()+" to "+rtc.getToUser().log());
		JSONObject message = new JSONObject()
		.put("verb", "u2u")
		.put("action", "requestVideo")
		.put("from", rtc.getFromUser().log())
		.put("cid", rtc.getRtcUUID())
		;
		wsWriter.sendMessage(rtc.getToUser(), message);
	}
}
