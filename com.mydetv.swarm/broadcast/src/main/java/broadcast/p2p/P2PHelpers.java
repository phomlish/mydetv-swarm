package broadcast.p2p;

import java.time.LocalDateTime;
import java.util.HashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.UserMessageHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.broadcast.Bandwidth;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class P2PHelpers {

	@Inject private WsWriter wsWriter;

	private static final Logger log = LoggerFactory.getLogger(P2PHelpers.class);
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private VideoWaiters videoWaiters;
	@Inject private VerifyCGBW verifyConnections;
	@Inject private RtcFailed rtcFailed;

	/**
	 * after we do BwHelpers.areAllBandwidthTestsFinished we need to connect video<br>
	 * 
	 * @param best
	 */
	public void connectVideo(Bandwidth best) {
		RtcConnection bestWebrtcConnection = best.getRtcConnection();

		SessionUser toUser = bestWebrtcConnection.getToUser();
		SessionUser fromUser = bestWebrtcConnection.getFromUser();
		connectVideo(fromUser, toUser);
	}


	public void wsMessageVideoConnected(JSONObject response, SessionUser toUser) {

		log.info("wsMessageVideoConnected:"+toUser.log());
		ConnectionGroup cg = toUser.getCgStream();

		if(cg.getUpstreams().size()!=1) {
			log.error("could not find upstream "+cg.getUpstreams().size()+" "+toUser.log());
			return;
		}
		RtcConnection rtc = cg.getUpstreams().get(0);
		//RtcConnection rtc = cg.getUpstream();
		rtc.addState("connected");
		rtc.setConnected(true);

		SessionUser fromUser = rtc.getFromUser();
		log.info("video connected to "+toUser.log()+" "+cg.getTier()
		+" from "+fromUser.log()+" "+fromUser.getCgStream().getTier());
		userMessageHelpers.sendUserMessage(toUser, "info", "enjoy the show");
		fromUser.getMydetvChannel().setCurrentWaiter(null);
		videoWaiters.subtactOneWaiter(fromUser.getMydetvChannel(), toUser);

		// if he has immediate downstreams then we need to tell him to connect this video stream to them
		for(RtcConnection trtc : cg.getDownstreams()) {
			JSONObject connectRemoteStreamTo = new JSONObject()
					.put("verb", "connectRemoteStream")
					.put("cid", trtc.getRtcUUID())
					;
			wsWriter.sendMessage(toUser, connectRemoteStreamTo);
		}

		// descend ALL the downstreams and fix their tiers
		fixDownstreamsTiers(cg);

		// anyone else waiting around?
		videoWaiters.connectViewersWaiting(toUser.getMydetvChannel());
	}

	// careful, perhaps they already left
	private void fixDownstreamsTiers(ConnectionGroup cg) {
		log.info("fixDownstreamsTiers "+cg.getProvider().log());
		for(RtcConnection rtc : cg.getDownstreams()) {
			SessionUser toUser = rtc.getToUser();
			log.info("toUser:"+toUser.log()+" fromUser:"+rtc.getFromUser().log());
			ConnectionGroup cgDownstream = rtc.getToUser().getCgStream();
			cgDownstream.setTier(cg.getTier()+1);
			fixDownstreamsTiers(cgDownstream);
		}		
	}

	private void connectVideo(SessionUser fromUser, SessionUser toUser) {
		// create a new rtc toUser/fromUser
		RtcConnection rtc = new RtcConnection("video", fromUser, toUser);
		String cid = rtc.getRtcUUID();
		log.info("created new webrtcConnection for toUser "+toUser.log()+" from "+fromUser.log()+" "+cid);	

		ConnectionGroup cgvFromUser = fromUser.getCgStream();

		ConnectionGroup cgvToUser = toUser.getCgStream();
		cgvToUser.setTier(cgvFromUser.getTier()+1);
		// set it in his UserSession
		toUser.getCgStream().setTier(cgvToUser.getTier());

		// set this as his upstream
		//cgvToUser.setUpstream(rtc);
		cgvToUser.getUpstreams().add(rtc);
		// add this webrtcConnection to fromUser's connectionGroupVideo downstreams
		cgvFromUser.getDownstreams().add(rtc);

		wsWriter.sendSetupRTCConnection(toUser, rtc, "receiver");
		wsWriter.sendSetupRTCConnection(fromUser, rtc, "sender");
		rtc.setStarted(LocalDateTime.now());
	}

	public void initProgressbarBw(SessionUser su, Integer total, Integer finished) {
		JSONObject message = new JSONObject()
				.put("verb", "initProgressbarBw")
				.put("total", total)
				.put("finished", finished);
		;
		wsWriter.sendMessage(su, message);
	}
	public void hideProgressBarBw(SessionUser su) {
		JSONObject message = new JSONObject()
				.put("verb", "hideProgressbarBw");
		wsWriter.sendMessage(su, message);
	}
	public void initProgressbarTurn(SessionUser su, Integer total) {
		JSONObject message = new JSONObject()
				.put("verb", "initProgressbarTurn")
				.put("total", total)
				;
		wsWriter.sendMessage(su, message);
	}
	public void updateProgressbarTurn(SessionUser su, Integer current) {
		JSONObject message = new JSONObject()
				.put("verb", "updateProgressbarTurn")
				.put("current", current)
				;
		wsWriter.sendMessage(su, message);
	}
	public void hideProgressBarTurn(SessionUser su) {
		JSONObject message = new JSONObject()
				.put("verb", "hideProgressbarTurn");
		wsWriter.sendMessage(su, message);
	}

	/**
	 * calling process is responsible for synchronizing blackboard.getLockRtcConnections()<br>
	 * comes from PeopleTask where it checks every user in the system<br>
	 * comes from VideoWaiters when we check if current waiter failed<br>
	 * @param su
	 */
	/*
	public void verifyRtcSyncronized(SessionUser su) {

		if(su==null) return;
		if(su.getWebUser()==null) return;
		if(su.isMediaPlayer()) return;

		log.debug("analyzing "+su.log());

		HashMap<String, RtcConnection> rtcConnections = new HashMap<String, RtcConnection>(su.getRtcConnections());
		for(RtcConnection rtc : rtcConnections.values()) {

			log.info("looking at "+su.log()+"'s rtc connection:"+rtc.log());

			// if either guy left, remove the rtc
			if(rtc.getToUser()==null || rtc.getToUser().getWebUser()==null
			|| rtc.getFromUser()==null || rtc.getFromUser().getWebUser()==null) {
				removeRtcFromUsers(rtc);
				continue;
			}

			// don't remove bw tests
			if(rtc.getRemoved() && rtc.getConnectionType().equals("bw"))
				continue;

			// of course don't remove connected
			if(rtc.getConnected())
				continue;

			// we can skip errored
			if(rtc.getErrored()) 
				continue;

			// older than 2 minutes is failed!
			LocalDateTime old = LocalDateTime.now().minusSeconds(121);
			if(! rtc.getConnected() && rtc.getStarted().isBefore(old)) {
				rtcFailed.stuckConnection(rtc);
			}

			// maybe we need to deal with dead connections, or does iceFailed do it?

			// after a while maybe they won't fail anymore (they moved closer to an Internet ley line)
			//old = LocalDateTime.now().minusMinutes(15);				
		}
		log.debug("unlocking blackboard.lockRtcConnections");

	}

	private void removeRtcFromUsers(RtcConnection rtc) {
		SessionUser toUser = rtc.getToUser();
		SessionUser fromUser = rtc.getFromUser();
		if(toUser==null) {
			log.info("to user is gone");
			if(fromUser!=null)
				fromUser.getRtcConnections().remove(rtc.getRtcUUID());				
		}
		else if(!toUser.getRtcConnections().containsKey(rtc.getRtcUUID())) {
			log.error("Error, to user "+toUser.log()+" doesn't have "+rtc.getRtcUUID());
		}
		else {
			toUser.getRtcConnections().remove(rtc.getRtcUUID());
		}

		if(fromUser==null) {
			log.info("from user is gone");
			//if(toUser!=null)
			//	toUser.getRtcConnections().remove(rtc.getRtcUUID());		
		}
		else if(!fromUser.getRtcConnections().containsKey(rtc.getRtcUUID())) {
			log.error("Error, to user "+fromUser.log()+" doesn't have "+rtc.getRtcUUID());
		}
		else {
			//fromUser.getRtcConnections().remove(rtc.getRtcUUID());
		}
	}
	*/
	
	

}
