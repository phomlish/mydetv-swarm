package broadcast.p2p;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.websocket.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.broadcaster.BroadcasterHelpers;
import broadcast.business.ChatHelpers;
import broadcast.business.PeopleHelpers;
import broadcast.business.UserMessageHelpers;
import broadcast.janus.JClient;
import broadcast.janus.JDetach;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.Bandwidth;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;
import shared.db.DbChannels;

public class SessionUserClose {

	@Inject private Blackboard blackboard;
	@Inject private ChatHelpers chatHelpers;
	@Inject private DbChannels dbChannels;
	@Inject private PeopleHelpers peopleHelpers;
	@Inject private VideoWaiters videoWaiters;
	@Inject private WsWriter wsWriter;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private JDetach jDetach;
	@Inject private BroadcasterHelpers broadcastHelpers;
	@Inject private CGHelpers cgHelpers;

	private static final Logger log = LoggerFactory.getLogger(SessionUserClose.class);

	/*
	 * doClose
	 * 	comes from WebSocketServer.onClose
	 *  process
	 *      updates db channelWebUsers that he left
	 *      deals with the loss of his video	
	 *      chats mydetvChannel users that he left
	 *      redraw the list of ppl with him greyed out
	 *      remove him from the blackboard wsConnections map
	 *  error if 
	 *  	websocket is not in blackboard
	 *  	sessionUser has no wsConnection
	 */
	/**
	 * 
	 * @param session
	 * @param statusCode
	 * @param reason
	 */
	public void doClose(Session session) {
		
		try {
			Integer hashcode = Integer.valueOf(session.hashCode());
			log.info("websocket closed "+hashcode);

			if(!blackboard.getWebSocketConnections().containsKey(hashcode)) {
				log.error("meltdown, can't find session in blackboard "+hashcode);
				return;
			}
			WebSocketConnection wsConnection = blackboard.getWebSocketConnections().get(hashcode);

			// let's see if this websocket initialized
			SessionUser sessionUser = wsConnection.getSessionUser();
			if(sessionUser == null) {
				log.info("sessionUser must not have initialized");
				blackboard.getWebSocketConnections().remove(hashcode);
				return;
			}

			// check the impossible, we have the session user but he doesn't have the wsconnection defined
			if(sessionUser.getWebSocketConnection() == null) {
				log.error("meltdown, sessionUser has no wsConnection.  impossible!");
				blackboard.getWebSocketConnections().remove(hashcode);
				return;
			}

			doCloseSyncronized(sessionUser);
			
		} catch(Exception ex) {
			log.error("Exception caught :",ex);
		}
	}

	/**
	 * We synchronize on su.lock
	 * @param su
	 */
	public void doCloseSyncronized(SessionUser su) {

		log.warn("synchronized su");
		synchronized(su.getLock()) {
			OneMydetvChannel mydetvChannel = su.getMydetvChannel();
			
			videoWaiters.removeWaiter(mydetvChannel, su);
			
			// update db that he left the channel
			log.info("updating database channel-webUsers "+su.getMydetv());
			dbChannels.removeWebUserFromChannel(su, Timestamp.from(Instant.now()));

			// deal with his CGs and inform others he's gone
			lostWebsocketConnectionCleanupCGs(su);

			// chat everyone that they left
			log.info("sendAdminChat "+su.log());
			String message = su.log()+"  left the room";
			chatHelpers.sendAdminChat(su, mydetvChannel, message);

			// remove this guy from other's people images
			log.info("peopleHelpers.drawPeople "+su.log());
			peopleHelpers.removePerson(mydetvChannel, su);

			// zero DbChannelWebUsers let's us know he is not watching a channel
			su.getWebUser().setDbChannelWebUsersId(0);		

			su.setBroadcastChannel(null);
		}
		log.warn("unsynchronized su");
	}

	/**
	 * tell others he's gone
	 * @param sessionUser
	 */
	private void lostWebsocketConnectionCleanupCGs(SessionUser sessionUser) {
		log.info("lostWebsocketConnection "+sessionUser.log());
		OneMydetvChannel mydetvChannel = sessionUser.getMydetvChannel();
		
		// if he was a broadcaster remove him
		if(sessionUser.isbroadcastingLive()) {
			JClient jClient = (JClient) sessionUser.getMydetvChannel().getOjClient().getjClient();
			broadcastHelpers.removeBroadcasterSessionUserClose(jClient);
		}
		sessionUser.setInBroadcastingRoom(false);

		// remove from up/down streams (tier1 and greater)
		ConnectionGroup cg = sessionUser.getCgStream();
		if(cg.getUpstreams().size()>0) 
			removeFromCgStreamUpstreams(sessionUser);

		if(cg.getDownstreams().size()>0)
			removeFromDownstreams(sessionUser);

		// if he was a broadcaster remove him
		ConnectionGroup cgBroadcaster = sessionUser.getCgBroadcaster();
		if(cgBroadcaster.getDownstreams().size()>0) {
			if(mydetvChannel.getOjClient()!=null && mydetvChannel.getOjClient().getjClient()!=null) {
				JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
				broadcastHelpers.switchToRemoteSid(jClient);
			}
			else {
				log.error("Error, we are a broadcaser but jClient is null");
			}
		}

		// deal with u2u connections
		removeU2U(sessionUser);

		log.info("remove websocketConnection from blackboard "+sessionUser.getWebSocketConnection().hashCode());
		blackboard.getWebSocketConnections().remove(sessionUser.getWebSocketConnection().hashCode());
		
		// clear his pointers
		log.info("clear pointers for "+sessionUser.log());
		clearSessionUser(sessionUser);

		// always go see if we have anyone waiting
		videoWaiters.connectViewersWaiting(mydetvChannel);
	}

	// remove this user from his upstream providers
	private void removeFromCgStreamUpstreams(SessionUser su) {
		log.info("removing "+su.log()+" from his cgStream upstreams");

		ConnectionGroup cg = su.getCgStream();
	
		if(cg.getUpstreams().size()==0) {
			if(! cg.getTier().equals(0))
				log.error("no upstreams and "+su.log()+" is not tier0");
			return;
		}

		List<RtcConnection> upstreams = new ArrayList<RtcConnection>(cg.getUpstreams());
		for(RtcConnection rtc : upstreams) {
			removeFromCgStreamUpstream(su, rtc);
		}

	}
	
	private void removeFromCgStreamUpstream(SessionUser su, RtcConnection rtc) {
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		SessionUser fromUser = rtc.getFromUser();
		log.info("remove "+su.log()+" from "+fromUser.log()+" downstreams");

		if(rtc.getConnectionType().equals("video")) {
			fromUser.getCgStream().getDownstreams().remove(rtc);
			wsWriter.sendRemoveRTCConnection(fromUser, rtc);
			rtc.addState("removed");
			rtc.setRemoved(true);
		}
		else if(rtc.getConnectionType().equals("janus")) {
			JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			if(jClient==null) {
				log.error("Error, jClient is null already!");
			}
			ArrayList<RtcConnection> detaches = new ArrayList<RtcConnection>();
			detaches.add(rtc);
			jDetach.syncDetach(mydetvChannel, detaches);
		}
		else {
			log.error("how did this cg get in here:"+su.log()+" "+rtc.getConnectionType());
		}
	}
	
	/**
	 * remove from our immediate downstreams<br>
	 * also set all of their downstreams to be 'waiting'<br>
	 * @param sessionUser
	 */
	private void removeFromDownstreams(SessionUser sessionUser) {
		log.info("removing "+sessionUser.log()+" downstreams");
		ConnectionGroup cg = sessionUser.getCgStream();
		List<RtcConnection> downstreams = new ArrayList<RtcConnection> (cg.getDownstreams());
		for(RtcConnection rtc : downstreams) {
			SessionUser toUser = rtc.getToUser();
			wsWriter.sendRemoveRTCConnection(toUser, rtc);
			rtc.addState("removed");
			rtc.setRemoved(true);
			cg.getDownstreams().remove(rtc);
			
			log.info("removing upstream from toUser "+toUser.log());
			ConnectionGroup toUserCg = toUser.getCgStream();
			//toUserCg.setUpstream(null);
			cgHelpers.removeUpstream(toUserCg, rtc);
			toUserCg.setTier(-1);
			//toUser.getRtcConnections().remove(rtcConnection.getRtcUUID());
			setDownstreamsWaiting(toUser);
			userMessageHelpers.sendUserMessage(toUser, "info", "lost upstream");
		}		
	}

	private void removeU2U(SessionUser su) {
		if(su.getCgU2U().getDownstreams().size()>0) {
			log.info("removing u2u downstreams for "+su.log());
			for(RtcConnection rtc : su.getCgU2U().getDownstreams()) {
				wsWriter.sendRemoveRTCConnection(rtc.getToUser(), rtc);
				rtc.setRemoved(true);
				rtc.addState("removed");
			}
		}

		if(su.getCgU2U().getUpstreams().size()>0) {
			log.info("removing u2u upstreams for "+su.log());
			for(RtcConnection rtc : su.getCgU2U().getUpstreams()) {
				wsWriter.sendRemoveRTCConnection(rtc.getFromUser(), rtc);
				rtc.setRemoved(true);
				rtc.addState("removed");
			}
		}
	}

	/**
	 * make sure you are done gracefully closing everything depending on these pointers<br>
	 * remove websocketConnection from blackboard<br>t
	 * remove sessionUser from mydetvChannel
	 * clear sessionUser's websocketConnection<br>
	 * clear sessionUser's mydetvChannel<br>
	 * set sessionUser's CGV upstream to null
	 * set sessionUser's CGV tier to -1
	 * set sessionUser's CGV downstreams to a new list
	 * @param su
	 */
	private void clearSessionUser(SessionUser su) {
		log.info("clearing sessionUser "+su.log());
		
		// clear websocket connection
		Integer hashcode = Integer.valueOf(su.getWebSocketConnection().getWebsocketSession().hashCode());
		blackboard.getWebSocketConnections().remove(hashcode);
		if(su.getMydetvChannel().getSessionUsers().containsKey(su.getMydetv()))	
			su.getMydetvChannel().getSessionUsers().remove(su.getMydetv());
		su.setWebSocketConnection(null);
		
		
		su.setMydetvChannel(null);
		// remember he just left so we clear him completely after a long while
		su.setLastSeen(LocalDateTime.now());

		// clear connection groups
		su.getCgStream().setTier(-1);
		cgHelpers.removeAllUpstreams(su.getCgStream());
		cgHelpers.removeAllDownstreams(su.getCgStream());

		cgHelpers.removeAllUpstreams(su.getCgU2U());
		cgHelpers.removeAllDownstreams(su.getCgU2U());

		

	}

	/**
	 * recurse down the downstreams setting them all as waiting<br>
	 * @param sessionUser
	 */
	private void setDownstreamsWaiting(SessionUser sessionUser) {
		log.info("setting "+sessionUser.log()+"'s downstreams to waiting");
		ConnectionGroup cg = sessionUser.getCgStream();
		List<RtcConnection> downstreams = new ArrayList<RtcConnection> (cg.getDownstreams());
		for(RtcConnection rtcConnection : downstreams) {
			SessionUser toUser = rtcConnection.getToUser();
			log.warn(toUser.log()+" is waiting");
			toUser.getCgStream().setWaiting(true);
			setDownstreamsWaiting(toUser);
		}		
	}
}
