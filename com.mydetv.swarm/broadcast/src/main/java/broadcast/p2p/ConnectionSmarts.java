package broadcast.p2p;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.janus.JUser;
import broadcast.janus.JVideoRoom;
import broadcast.web.ws.WsWriter;
import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class ConnectionSmarts {

	private static final Logger log = LoggerFactory.getLogger(ConnectionSmarts.class);
	@Inject private Blackboard blackboard;
	@Inject private JUser jFromUser;
	@Inject private JVideoRoom jLive;
	@Inject private JUser jIceCandidate;
	@Inject private WsWriter wsWriter;
	@Inject private CGHelpers cgHelpers;
	
	public void processIceMessage(JSONObject response, SessionUser su) {
		String connectionType = response.getString("connectionType");
		String cid = response.getString("cid");
		String verb = response.getString("verb");
		JSONObject sdp;
		RtcConnection rtc;
		
		JSONObject message = new JSONObject()
				.put("verb", "mydetvCustom")
				.put("cid", cid)
				.put("command", verb)	
				;
		
			rtc = cgHelpers.findRtcFromConnectionType(su, connectionType, cid);
			if(verb.equals("answer")) {
				if(rtc.getAnswer()!=null)
					log.warn("We already have our answer, must have gotten it from IceDone");
				
				sdp = response.getJSONObject("answer");
				rtc.setAnswer(sdp);
				
				if(connectionType.equals("janus")
				|| connectionType.equals("jtier0")) {
					jFromUser.wsAnswer(response, su);
					return;
				}
				message.put("sdp", sdp);
				
			}
			else if(verb.equals("offer")) {
				if(rtc.getOffer()!=null)
					log.warn("We already have our offer, must have gotten it from IceDone");
				
				sdp = response.getJSONObject("offer");
				rtc.setOffer(sdp);
				
				if(connectionType.equals("jtier0")) {
					jLive.configureOfferRemotePresenter(response, su);
					return;
				}
				
				message.put("sdp", sdp);
			}
			else if(verb.equals("iceCandidate")) {
				if(rtc.getFromUser().equals(su))
					rtc.addState("iceFrom");
				else
					rtc.addState("iceTo");
				
				if(connectionType.equals("janus")
				|| connectionType.equals("jtier0")) {
					jIceCandidate.wsIceCandidate(response, su);
					return;
				}
				
				sdp = response.getJSONObject("candidate");
				message.put("iceCandidate", sdp);
			}
			else if(verb.equals("iceCandidatesDone")) {
				
				log.info("rtc:"+rtc.log());
				if(rtc.getFromUser()==su)
					rtc.addState("iceDoneFrom");
				else
					rtc.addState("iceDoneTo");
				
				sdp = response.getJSONObject("sdp");
				String sdpType=sdp.getString("type");
				log.info("sdpType:"+sdpType);
				message.put("sdp", sdp);
				if(sdpType.equals("answer")) {
					if(rtc.getAnswer()==null) {
						log.warn("we got ice done and no answer set yet, not doing trickle ice?");
						rtc.setAnswer(sdp);
						message.put("command", "answer");
						
					}
					else {
						log.debug("ignoring iceDone, we already have an answer ");
						return;
					}
				}
				else if(sdpType.equals("offer")) {
					if(rtc.getOffer()==null) {
						log.warn("we got ice done and no offer set yet, not doing trickle ice?");
						rtc.setOffer(sdp);
						message.put("command", "offer");
					}
					else {
						log.debug("ignoring iceDone, we already have an offer "+rtc.getOffer().toString());
						return;
					}
				}
				else {
					log.error("didn't recognize iceDone sdp type "+sdpType);
					return;
				}
			}
			else {
				log.error("huh, verb "+verb);
				return;
			}

			wsWriter.sendMessage(rtc.getOther(su), message);
			
		

	}
	

}
