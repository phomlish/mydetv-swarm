package broadcast.p2p;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class VerifyCGBW {

	private static final Logger log = LoggerFactory.getLogger(VerifyCGBW.class);
	@Inject private RtcFailed rtcFailed;
	@Inject private Blackboard blackboard;
	
	/**
	 * verify everything about this user's RTCs
	 * comes from PingTask where it checks every user in the system<br>
	 * comes from VideoWaiters when we check if current waiter failed
	 * @param su
	 */
	public void verifyCGBWs(SessionUser su) {

		if(su==null) return;
		if(su.getWebUser()==null) return;
		
		log.warn("synchronized blackboard lockCGBW");
		synchronized(blackboard.getLockCGBWs()) {
			verifyCGs(su);			
		}
		log.warn("unsynchronized blackboard lockCGBW");
	}
	
	private void verifyCGs(SessionUser su) {
		
		verifyCG(su, su.getCgStream());
		verifyCG(su, su.getCgBroadcaster());
		verifyCG(su, su.getCgU2U());
		//TODO: verifyBw

	}
	
	private void verifyCG(SessionUser su, ConnectionGroup cg) {
		
		try {
		List<RtcConnection> rtcConnections;
		
		// check our upstreams
		rtcConnections = new ArrayList<RtcConnection>(cg.getUpstreams());
		for(RtcConnection rtc : rtcConnections) 
			if(! verifyRtc(su, cg, rtc))
				cg.getDownstreams().remove(rtc);
		
		// check our downstreams
		rtcConnections = new ArrayList<RtcConnection>(cg.getDownstreams());
		for(RtcConnection rtc : rtcConnections) 
			if(! verifyRtc(su, cg, rtc))
				cg.getDownstreams().remove(rtc);
		} catch(Exception e) {
			log.error("Excepion "+e);
		}
	}
	
	private boolean verifyRtc(SessionUser su, ConnectionGroup cg, RtcConnection rtc) {
		log.info("looking at "+su.log()+"'s rtc connection:"+rtc.log());

		// if either guy left, remove the rtc
		if(rtc.getToUser()==null || rtc.getToUser().getWebUser()==null) {
			
			return false;
		}
		if(rtc.getFromUser()==null || rtc.getFromUser().getWebUser()==null) {
			return false;
		}

		// of course don't remove connected
		if(rtc.getConnected())
			return true;

		// we can skip errored
		if(rtc.getErrored()) 
			return true;

		// older than 15 seconds is failed!
		LocalDateTime old = LocalDateTime.now().minusSeconds(15);
		if(! rtc.getConnected() && rtc.getStarted().isBefore(old)) {
			rtcFailed.stuckConnection(rtc);
		}

		return true;
		// maybe we need to deal with dead connections, or does iceFailed do it?

		// after a while maybe they won't fail anymore (they moved closer to an Internet ley line)
		//old = LocalDateTime.now().minusMinutes(15);		
		
	}

}
