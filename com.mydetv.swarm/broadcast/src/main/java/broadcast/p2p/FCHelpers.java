package broadcast.p2p;

import java.time.LocalDateTime;

import org.eclipse.jetty.server.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class FCHelpers {

	private static final Logger log = LoggerFactory.getLogger(FCHelpers.class);	
	
	public Boolean checkFaultyConnection(SessionUser fromUser, SessionUser toUser) {
		Integer countRecentlyFailed=0;
		
		for(FaultyConnection fc : fromUser.getFaultyConnections().values()) {
			if(toUser==null||toUser.getWebUser()==null||toUser.getWebUser().getWuid()==null||toUser.getWebUser().getWuid()<1) {
				log.warn("toUser does no exist, maybe they lgged out already?");
				continue;
			}
			if(fc.getToWuid()!=toUser.getWebUser().getWuid())
				continue;
			
			countRecentlyFailed++;
		}

		if(countRecentlyFailed>5) 
			return true;
		return false;
	}
	
	public Boolean checkIceFailed(SessionUser fromUser, SessionUser toUser) {
		Integer countRecentlyFailed=0;
		
		for(FaultyConnection fc : fromUser.getFaultyConnections().values()) {
			if(! fc.getReason().equals("failed"))
				continue;
			if(toUser==null||toUser.getWebUser()==null||toUser.getWebUser().getWuid()==null||toUser.getWebUser().getWuid()<1) {
				log.warn("toUser does no exist, maybe they lgged out already?");
				continue;
			}
			if(fc.getToWuid()!=toUser.getWebUser().getWuid())
				continue;
			
			countRecentlyFailed++;
		}

		if(countRecentlyFailed>5) 
			return true;
		return false;
	}
	
	public Boolean checkSlowlinks(SessionUser fromUser, SessionUser toUser) {
		Integer countRecentlySlow=0;
		
		for(FaultyConnection fc : fromUser.getFaultyConnections().values()) {
			if(! fc.getReason().equals("slow"))
				continue;
			if(toUser==null||toUser.getWebUser()==null||toUser.getWebUser().getWuid()==null||toUser.getWebUser().getWuid()<1) {
				log.warn("toUser does no exist, maybe they lgged out already?");
				continue;
			}
			if(fc.getToWuid()!=toUser.getWebUser().getWuid())
				continue;
			
			countRecentlySlow++;
		}
		if(countRecentlySlow>3) 
			return true;
		return false;
	}
	
	public void checkTooManyIceFailed(RtcConnection rtc) {
		SessionUser toUser = rtc.getToUser();
		SessionUser fromUser = rtc.getFromUser();
		

	}
	
	private Failed countIceFailed(SessionUser toTarget, RtcConnection rtc) {
		Failed failed = new Failed();
		
		for(FaultyConnection fc : rtc.getToUser().getFaultyConnections().values()) {
			if(toTarget!=rtc.getToUser())
				continue;
			if(! fc.getConnectionType().equals(rtc.getConnectionType()))
				continue;
			failed.toFailed++;
		}
		for(FaultyConnection fc : rtc.getFromUser().getFaultyConnections().values()) {
			if(toTarget!=rtc.getFromUser())
				continue;
			if(! fc.getConnectionType().equals(rtc.getConnectionType()))
				continue;
			failed.fromFailed++;
		}
		log.info("test");
		return failed;
	}
	
	class Failed {
		int toFailed=0;
		int fromFailed=0;
	}
	
	public void addFc(RtcConnection rtc, String reason, SessionUser toUser, Integer nacks) {		
		FaultyConnection fc = new FaultyConnection();
		fc.setConnectionType(rtc.getConnectionType());
		fc.setReason(reason);
		fc.setDt(LocalDateTime.now());
		fc.setNaks(nacks);
		
		fc.setToIsMediaPlayer(false);
		fc.setToWuid(toUser.getWebUser().getWuid());
		fc.setToDbChannelWebUsersId(toUser.getWebUser().getDbChannelWebUsersId());
		fc.setToIp(toUser.getIp());
		fc.setToPort(toUser.getPort());
		
		fc.setFromIsMediaPlayer(false);
		fc.setFromWuid(rtc.getFromUser().getWebUser().getWuid());
		fc.setFromDbChannelWebUsersId(rtc.getFromUser().getWebUser().getDbChannelWebUsersId());
		fc.setFromIp(rtc.getFromUser().getIp());
		fc.setFromPort(rtc.getFromUser().getPort());
		
		toUser.getFaultyConnections().put(LocalDateTime.now(), fc);
		}
	
	private FaultyConnection createMediaPlayer2U(String connectionType, String reason, SessionUser toUser, Integer nacks) {		
		FaultyConnection fc = new FaultyConnection();
		fc.setConnectionType(connectionType);
		fc.setReason(reason);
		fc.setDt(LocalDateTime.now());
		fc.setNaks(nacks);
		
		fc.setToIsMediaPlayer(false);
		fc.setToWuid(toUser.getWebUser().getWuid());
		fc.setToDbChannelWebUsersId(toUser.getWebUser().getDbChannelWebUsersId());
		fc.setToIp(toUser.getIp());
		fc.setToPort(toUser.getPort());
		
		fc.setFromIsMediaPlayer(true);
		
		return fc;
		}

	private FaultyConnection U2createMediaPlayer(String connectionType, String reason, SessionUser fromUser, Integer nacks) {		
		FaultyConnection fc = new FaultyConnection();
		fc.setConnectionType(connectionType);
		fc.setReason(reason);
		fc.setDt(LocalDateTime.now());
		fc.setNaks(nacks);
		
		fc.setToIsMediaPlayer(true);
		
		fc.setFromIsMediaPlayer(false);
		fc.setFromWuid(fromUser.getWebUser().getWuid());
		fc.setFromDbChannelWebUsersId(fromUser.getWebUser().getDbChannelWebUsersId());
		fc.setFromIp(fromUser.getIp());
		fc.setFromPort(fromUser.getPort());
		
		return fc;
		}

}

