package broadcast.p2p;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class CGHelpers {

	private static final Logger log = LoggerFactory.getLogger(CGHelpers.class);
	
	public RtcConnection findUpstream(ConnectionGroup cg, SessionUser su) {		
		for(RtcConnection rtc : cg.getUpstreams())
			if(rtc.getFromUser().equals(su))
				return rtc;		
		return null;
	}
	
	public void removeUpstream(ConnectionGroup cg, RtcConnection rtc) {
		if(cg.getUpstreams().contains(rtc))
			cg.getUpstreams().remove(rtc);
		else
			log.error("could not find "+rtc.log());
	}
	
	/**
	 * nothing graceful here, just removes the RTCs from the CG<br>
	 * called from SessionUserClose.clearSessionUser<br>
	 * @param cg
	 */
	public void removeAllUpstreams(ConnectionGroup cg) {
		List<RtcConnection> upstreams = new ArrayList<RtcConnection>(cg.getUpstreams());
		for(RtcConnection rtc : upstreams)
			cg.getUpstreams().remove(rtc);
	}
	
	/**
	 * nothing graceful here, just removes the RTCs from the CG<br>
	 * called from SessionUserClose.clearSessionUser<br>
	 * @param cg
	 */
	public void removeAllDownstreams(ConnectionGroup cg) {
		List<RtcConnection> downstreams = new ArrayList<RtcConnection>(cg.getDownstreams());
		for(RtcConnection rtc : downstreams)
			cg.getDownstreams().remove(rtc);
	}
	
	
	
	public RtcConnection findRtcUpstream(ConnectionGroup cg, String cid) {
		for(RtcConnection rtc : cg.getUpstreams())
			if(rtc.getRtcUUID().equals(cid))
				return rtc;
		return null;
	}
	
	public RtcConnection findRtcDownstream(ConnectionGroup cg, String cid) {
		for(RtcConnection rtc : cg.getDownstreams())
			if(rtc.getRtcUUID().equals(cid))
				return rtc;
		return null;
	}
	
	public RtcConnection findRtcBwToMe(SessionUser su, String cid) {
		if(su.getBandwidthTestsToMe().containsKey(cid))
			return su.getBandwidthTestsToMe().get(cid).getRtcConnection();		
		return null;
	}
	
	public RtcConnection findRtcBwFromMe(SessionUser su, String cid) {
		if(su.getBandwidthTestsFromMe().containsKey(cid))
			return su.getBandwidthTestsFromMe().get(cid).getRtcConnection();		
		return null;
	}
	
	public RtcConnection findRtcBw(SessionUser su, String cid) {
		if(su.getBandwidthTestsToMe().containsKey(cid))
			return su.getBandwidthTestsToMe().get(cid).getRtcConnection();		
		if(su.getBandwidthTestsFromMe().containsKey(cid))
			return su.getBandwidthTestsFromMe().get(cid).getRtcConnection();		
		return null;
	}
	
	/**
	 * Using connectionType figure out the su's CG then find the RTC<br>
	 * @param su
	 * @param connectionType : string
	 * @param cid
	 * @return
	 */
	public RtcConnection findRtcFromConnectionType(SessionUser su, String connectionType, String cid) {
		if(connectionType.equals("u2u"))
			return(findRtc(su.getCgU2U(), cid));
		else if(connectionType.equals("video"))
			return(findRtc(su.getCgStream(), cid));
		else if(connectionType.equals("bw"))
			return(findRtcBw(su, cid));
		else if(connectionType.equals("janus"))
			return(findRtc(su.getCgStream(), cid));
		else if (connectionType.equals("jtier0"))
			return(findRtc(su.getCgBroadcaster(), cid));
		else {
			log.error("didn't write for "+connectionType);
			return null;
		}
	}
	
	/**
	 * the RTC could be in upstreams or downstreams so we search both in the CG<br>
	 * @param cg
	 * @param cid
	 * @return
	 */
	private RtcConnection findRtc(ConnectionGroup cg, String cid) {
		for(RtcConnection rtc : cg.getUpstreams())
			if(rtc.getRtcUUID().equals(cid))
				return rtc;
		for(RtcConnection rtc : cg.getDownstreams())
			if(rtc.getRtcUUID().equals(cid))
				return rtc;
		return null;
	}
}
