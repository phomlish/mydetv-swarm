package broadcast.matomo;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.AsnResponse;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Subdivision;

import shared.data.Config;
import shared.setups.SwarmException;

//https://github.com/maxmind/GeoIP2-java

public class GeoIp {

	@Inject private Config config;
	private static final Logger log = LoggerFactory.getLogger(GeoIp.class);
	
	DatabaseReader asnReader;
	DatabaseReader cityReader;
	DatabaseReader countryReader;
	
	public boolean init() {
		
		String geoIpDate = findLatestDatabase();
		if(geoIpDate==null)
			return false;
		log.info("reading geoip from "+geoIpDate);
		//String geoIpDate = "20190205";
		String fn;	
		try {
			fn=config.getGeoip()+"/GeoLite2-ASN_"+geoIpDate+"/GeoLite2-ASN.mmdb";
			File asnFile = new File(fn);
			if(!asnFile.exists())
				throw new SwarmException("error reading "+fn);
			DatabaseReader asnReader = new DatabaseReader.Builder(asnFile).withCache(new CHMCache()).build();
			
			fn=config.getGeoip()+"/GeoLite2-City_"+geoIpDate+"/GeoLite2-City.mmdb";
			File cityFile = new File(fn);
			if(!cityFile.exists())
				throw new SwarmException("error reading "+fn);
			DatabaseReader cityReader = new DatabaseReader.Builder(cityFile).withCache(new CHMCache()).build();
			
			fn=config.getGeoip()+"/GeoLite2-Country_"+geoIpDate+"/GeoLite2-Country.mmdb";
			File countryFile = new File(fn);
			if(!countryFile.exists())
				throw new SwarmException("error reading "+fn);
			DatabaseReader countryReader = new DatabaseReader.Builder(countryFile).withCache(new CHMCache()).build();
		
			log.info("geoIp initialized for "+geoIpDate);
			this.asnReader=asnReader;
			this.cityReader=cityReader;
			this.countryReader=countryReader;
			return true;
		} catch (IOException | SwarmException e) {
			log.error("Error initializing "+geoIpDate+", "+e);
			return false;
		}
	}
	
	//			//geoIp.getIpDetails("52.170.237.95", r);
	public String getIpDetails(String ip, PiwikRequest preq) {
		if(asnReader==null || cityReader==null || countryReader==null)
			return "";
		
		try {
			InetAddress ipAddress = InetAddress.getByName(ip);
			if(ipAddress.isSiteLocalAddress())
				ipAddress = InetAddress.getByName("71.162.237.5");
			
			CityResponse cityResponse = cityReader.city(ipAddress);
			if(cityResponse!=null) {
				if(cityResponse.getCity()!=null) 
					preq.setVisitorCity(cityResponse.getCity().getName());
				if(cityResponse.getCountry()!=null) {
					cityResponse.getCountry().getIsoCode();
					Locale locale = new Locale("en", cityResponse.getCountry().getIsoCode());
					PiwikLocale piwikLocale = new PiwikLocale(locale);
					preq.setVisitorCountry(piwikLocale);

				}
				if(cityResponse.getLocation()!=null) {
					preq.setVisitorLatitude(cityResponse.getLocation().getLatitude());
					preq.setVisitorLongitude(cityResponse.getLocation().getLongitude());
				}
				if(cityResponse.getMostSpecificSubdivision()!=null) {
					Subdivision subdivision = cityResponse.getMostSpecificSubdivision();
					//log.info("region:"+subdivision.getIsoCode());
					preq.setVisitorRegion(subdivision.getIsoCode());
				}
			}
			
			AsnResponse asnResponse = asnReader.asn(ipAddress);
			if(asnResponse!=null) {
				log.info("asn:"+asnResponse.getAutonomousSystemOrganization());
				CustomVariable cv = new CustomVariable("asn", asnResponse.getAutonomousSystemOrganization());
				preq.setVisitCustomVariable(cv, 2);
				return asnResponse.getAutonomousSystemOrganization();
			}
			return "";
		} catch (IOException | GeoIp2Exception e) {
			log.error("bad ip "+e);
			return "";
		}
	}
	
	private String findLatestDatabase() {
		LocalDate ldFound=null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		
		File folder = new File(config.getGeoip());
		if(! folder.exists()) {
			log.error("folder "+config.getGeoip()+" does not exist");
			return "";
		}
		log.info("finding latest geoip files in "+config.getGeoip());
		
		for (final File fileEntry : folder.listFiles()) {
			if(! fileEntry.isDirectory())
				continue;
			String name = fileEntry.getName();
			if(! name.contains("GeoLite2-City_"))
				continue;
			String strDate = name.substring("GeoLite2-City_".length(), "GeoLite2-City_".length()+8);
			LocalDate ld = LocalDate.parse(strDate, formatter);
			if(ldFound==null)
				ldFound=ld;
			else if(ldFound.isBefore(ld))
				ldFound=ld;			
		}
		if(ldFound!=null)
			return ldFound.format(formatter);
		return null;
	}
}
