package broadcast.matomo;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import broadcast.Blackboard;
import broadcast.api.OneApiHost;
import broadcast.api.OneApiRequest;
import broadcast.api.RequestThread;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.db.DbLog;

public class MatomoHelper {

	private static final Logger log = LoggerFactory.getLogger(MatomoHelper.class);
	@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private Provider<RequestThread> requestThreadProvider;
	@Inject private GeoIp geoIp;
	@Inject private DbLog dbLog;
	
	public void sendTracking(SessionUser su, HttpServletRequest req, String state) {
		HashMap<String, String> headers = getAllHeaders(req);
		try {
			if(1==1)
				return;
			
			OneApiHost apiHost = blackboard.getMatomoHost();
			String inpath = req.getRequestURL().toString();
			if(req.getQueryString()!=null)
				inpath+="?"+req.getQueryString();
			URL actionUrl = new URL(inpath);
		
			PiwikRequest r = new PiwikRequest(config.getMatomoSite(), actionUrl);
			r.setVisitorId(su.getTrackingCode());
			r.setAuthToken(apiHost.getApiHostConfig().getApiKey());
			r.setResponseAsImage(true);
			r.setUserId(su.getMydetv());
			
			CustomVariable cv = new CustomVariable("mydetv", su.getMydetv());
			r.setVisitCustomVariable(cv, 1);
			String asn = geoIp.getIpDetails(req.getRemoteAddr(), r);
			
			if(su.getWebUser()==null) {
				r.setUserId(su.getMydetv());
			}
			else {
				r.setUserId(su.log());
				cv = new CustomVariable("wuid", su.getWebUser().getWuid().toString());
				r.setVisitCustomVariable(cv, 3);
				cv = new CustomVariable("username", su.log());
				r.setVisitCustomVariable(cv, 4);
				cv = new CustomVariable("email", su.getWebUser().getLoggedInEmail().getEmail());
				r.setVisitCustomVariable(cv, 5);
			}
			
			if(headers.containsKey("User-Agent"))
				r.setHeaderUserAgent(req.getHeader("User-Agent"));
			if(headers.containsKey("Referer"))
				r.setReferrerUrlWithString(req.getHeader("Referer"));
			if(headers.containsKey("Accept-Language"))
				r.setHeaderAcceptLanguage(req.getHeader("Accept-Language"));
			
			r.setVisitorIp(req.getRemoteAddr());

			String message = "connection "+su.getIpPort()+" to "+inpath+" "+state;
			message+="("+asn+")";
			if(! state.equals("ok"))
				dbLog.info(message);
			
			String s = r.getUrlEncodedQueryString();
			OneApiRequest apiRequest = new OneApiRequest(su, s, apiHost);
			URI uri=null;
			String url = apiHost.getApiHostConfig().getUrl()+"?"+s;
			log.debug("request:"+url);
			try {
				uri = new URI(url);
			} catch (URISyntaxException e) {
				log.error("bad uri:"+e);
				return;
			}
			HttpGet httpGet = new HttpGet(uri);

			RequestThread requestThread = requestThreadProvider.get();
			requestThread.init(apiHost.getHttpClient(), httpGet, apiRequest);
			requestThread.start();

		} catch (IOException e) {
			log.error("unable to start request "+e);
		}

	}

	public String randomTrackingCode() {
		
		Boolean avail = false;
		String trackingCode="";
		while(!avail) {
			long m = blackboard.getSecureRandom().nextLong();
			//trackingCode = Long.toHexString(m).substring(0, 16); // not always 16 characters
			trackingCode = String.format("%016X", m);
			if(! blackboard.getTrackingCodes().contains(trackingCode)) 
				avail=true;
		}
		//log.info("trackingCode:"+trackingCode);
		return trackingCode;
	}
	
	private HashMap<String, String> getAllHeaders(HttpServletRequest req) {
		HashMap<String, String> headers = new HashMap<String, String>();
		Enumeration<String> headerNames = req.getHeaderNames();
		while (headerNames.hasMoreElements()) {
		    String name = headerNames.nextElement();
		    //log.info(name+":"+req.getHeader(name));
		    headers.put(name, req.getHeader(name));
		}
		return headers;
	}
}
