package broadcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.janus.JClient;
import broadcast.janus.JShutdown;
import broadcast.master.MasterServer;
import broadcast.web.HttpEmbedded;
import shared.data.OneMydetvChannel;

public class ShutdownHook implements Runnable {

	@Inject private Blackboard blackboard;
	@Inject private HttpEmbedded httpEmbedded;
	@Inject private JShutdown jShutdown;
	private static final Logger log = LoggerFactory.getLogger(ShutdownHook.class);
			
	public void run() {
		log.info("starting gracefull shutdown");
		try {
			
			blackboard.setShuttingDown(true);
				
			for(OneMydetvChannel mydetvChannel: blackboard.getMydetvChannels().values()) {
				if(! mydetvChannel.isActive())
					continue;
				log.info("shutting down jclient for "+mydetvChannel.getName());
				JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
				jShutdown.fullJanusShutdown(jClient, false);
				if(mydetvChannel.getMasterServer() != null) {
					log.info("shutting down master server for "+mydetvChannel.getName());
					MasterServer master = (MasterServer) mydetvChannel.getMasterServer();
					master.shutdown();
					}
			}
			httpEmbedded.shutdown();
			blackboard.getScheduler().shutdown();
			// we might want to kill any ffmpeg's running

		} catch (Exception e) {
			System.out.println("Error gracefully shutting down: "+e);
		} 
		
	}
}
