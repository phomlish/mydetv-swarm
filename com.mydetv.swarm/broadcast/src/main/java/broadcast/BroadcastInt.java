package broadcast;

import com.google.inject.Injector;

import shared.setups.SwarmConfigException;

public interface BroadcastInt {

	void start() throws SwarmConfigException;	
	void stop();
	void setInjector(Injector injector);
}
