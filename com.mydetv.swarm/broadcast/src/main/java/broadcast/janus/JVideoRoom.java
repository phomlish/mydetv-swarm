	package broadcast.janus;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.VideosHelpers;
import shared.data.Config;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JVideoRoom {

	private static final Logger log = LoggerFactory.getLogger(JVideoRoom.class);
	@Inject private JControl jControl;
	@Inject private JHelpers jHelpers;
	@Inject private WsWriter wsWriter;
	@Inject private Config config;
	@Inject private VideosHelpers videosHelpers;
	
	public void publishLive(JClient jClient, JSONObject received) throws UnrecognizedException {
		
		OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
		SessionUser su = jClient.getMe();
		if(su.getCgBroadcaster().getUpstreams().size()!=1) {
			log.error("could not find upstream "+su.getCgBroadcaster().getUpstreams().size()+" "+su.log());
			return;
		}
		RtcConnection rtc = su.getCgBroadcaster().getUpstreams().get(0);
		//RtcConnection rtc = j.getCgBroadcaster().getUpstream();
		
		// this plugin calls it private_id
		if(received.isNull("plugindata"))				
			throw new UnrecognizedException("no received pluginData");
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no received data");
		JSONObject rData = rPlugindata.getJSONObject("data");
		Long privateId = rData.getLong("private_id");
		log.info("private_id:"+privateId);
		rtc.setPrivateId(privateId);
		
		JSONObject body = new JSONObject()
				.put("id", mydetvChannel.getOjClient().getJanusId())
				.put("room", mydetvChannel.getOjClient().getJanusId())
				.put("ptype", "publisher")
				.put("private_id", rtc.getPrivateId())
				.put("request", "publish");				
		JSONObject jsonObject = new JSONObject()
				.put("janus", "message")
				.put("body", body)			
				.put("opaque_id", rtc.getRtcUUID())
				.put("handle_id", rtc.getHandleId());
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc, true);
		jHelpers.removeTransaction(jClient, received);
	}
	
	public void videoroomEvent(JClient jClient, JSONObject received) throws UnrecognizedException {
		String transaction = received.getString("transaction");
		JTransaction jt = jClient.getTransactions().get(transaction);
		
		log.info("trans:"+transaction+",jt:"+jt.getTid());
		RtcConnection rtc = jt.getRtc();
		log.info("rtc:"+rtc.getRtcUUID()+" from "+rtc.getFromUser().log()+" to "+rtc.getToUser().log());
		if(received.isNull("plugindata"))				
			throw new UnrecognizedException("no received pluginData");
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no received data");
		JSONObject rData = rPlugindata.getJSONObject("data");
		
		if(! rData.isNull("configured")) {
			if(! received.isNull("jsep")) {
				JSONObject jsep = received.getJSONObject("jsep");
				SessionUser sessionUser = rtc.getFromUser();
				log.info("sending janus live answer to "+sessionUser.log());
				wsWriter.sendAnswer(sessionUser, rtc.getRtcUUID(), jsep);
			}
			else {
				log.info("no jsep sent with configured which is probably ok");
				return;
			}
		}
		else if(! rData.isNull("unpublished")) {
			log.info("not dealing with unpublished event");
		}
		else
			throw new UnrecognizedException("didn't recognize event :"+rData.toString());
	}
	
	public void configureOfferRemotePresenter(JSONObject response, SessionUser su) {
		log.info("offer:"+response.toString());
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		//JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		//RtcConnection rtc = jClient.getMe().getCgBroadcaster().getUpstream();
		if(su.getCgBroadcaster().getUpstreams().size()!=1) {
			log.error("could not find upstream "+su.getCgBroadcaster().getUpstreams().size()+" "+su.log());
			return;
		}
		RtcConnection rtc = su.getCgBroadcaster().getUpstreams().get(0);
		
		JSONObject body = new JSONObject()
			.put("id", mydetvChannel.getOjClient().getJanusId())
			.put("audio", true)
			.put("video", true)
			.put("private_id", rtc.getPrivateId())
			.put("request", "configure");
		JSONObject offer = response.getJSONObject("offer");
		JSONObject jsonObject = new JSONObject()
			.put("janus", "message")
			.put("body", body)
			.put("jsep", offer)
			.put("opaque_id", rtc.getRtcUUID())
			.put("handle_id", rtc.getHandleId());
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc, true);	
	}
		
	public void attachAsSender(SessionUser su) {	
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();

		ConnectionGroup cgUser = su.getCgBroadcaster();
			
		RtcConnection rtc = new RtcConnection("jtier0", su, jClient.getMe());
		String cid = rtc.getRtcUUID();
		
		// we need to initialize the cgLiveUp
		ConnectionGroup cgLiveUp = jClient.getMe().getCgBroadcaster();
		cgLiveUp.setTier(0);
		cgLiveUp.setProvider(su);
		//cgLiveUp.setUpstream(rtc);
		cgLiveUp.getUpstreams().add(rtc);
		cgUser.setTier(0);
		cgUser.setProvider(su);
		cgUser.getDownstreams().add(rtc);

		//su.getRtcConnections().put(cid, rtc);
			
		JSONObject jsonObject = new JSONObject()
			.put("janus", "attach")
			.put("opaque_id", rtc.getRtcUUID())
			.put("plugin", "janus.plugin.videoroom");
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc, true);
		rtc.setStarted(LocalDateTime.now());
	}
		
	public void forwardStream(SessionUser su) throws UnrecognizedException {
		log.info("forwardStream");
		OneMydetvChannel mydetvChannel = su.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		ConnectionGroup cgBroadcaster = jClient.getMe().getCgBroadcaster();
		if(su.getCgBroadcaster().getUpstreams().size()!=1) {
			log.error("could not find upstream "+su.getCgBroadcaster().getUpstreams().size()+" "+su.log());
			return;
		}
		RtcConnection rtc = su.getCgBroadcaster().getUpstreams().get(0);
		//RtcConnection rtc = cgBroadcaster.getUpstream();
		
		JSONObject body = new JSONObject()
			.put("request", "rtp_forward")
			.put("publisher_id", mydetvChannel.getOjClient().getJanusId())
			.put("room", mydetvChannel.getOjClient().getJanusId())
			.put("host", config.getJanusHost())
			.put("audio_port", mydetvChannel.getOjClient().getJanusAudioPort())
			.put("audio_pt", 111)
			.put("video_port", mydetvChannel.getOjClient().getJanusVideoPort())
			.put("video_pt", 100)
			.put("secret", "2b46e73a-a6c4-405f-b020-1d0dabc3943a")
			;	
		JSONObject jsonObject = new JSONObject()
			.put("janus", "message")
			.put("handle_id", rtc.getHandleId())
			.put("body", body)
			.put("opaque_id", rtc.getRtcUUID())
			.put("plugin", "janus.plugin.videoroom");
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc, true);
		
		videosHelpers.createAndSendVideoTitle(mydetvChannel);
	}
	/**
	 * after success on attach, join as sender<br>
	 * @param mydetvChannel
	 * @param rtc
	 */
	public void joinAsSender(OneMydetvChannel mydetvChannel, RtcConnection rtc) {
		JSONObject body = new JSONObject()
			.put("id", mydetvChannel.getOjClient().getJanusId())
			.put("room", mydetvChannel.getOjClient().getJanusId())
			.put("ptype", "publisher")
			.put("request", "join");				
		JSONObject jsonObject = new JSONObject()
			.put("janus", "message")
			.put("body", body)			
			.put("opaque_id", rtc.getRtcUUID())
			.put("handle_id", rtc.getHandleId());
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc, true);
		
		wsWriter.sendSetupRTCConnection(rtc.getFromUser(), rtc, "sender");	
	}
	
	public void attached(JClient jClient, JSONObject received) throws UnrecognizedException {
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		RtcConnection rtc = jt.getRtc();
		JSONObject jsep = received.getJSONObject("jsep");
		
		SessionUser sessionUser = rtc.getToUser();
		log.info("sending janus offer to "+sessionUser.log());
		wsWriter.sendOffer(sessionUser, rtc.getRtcUUID(), jsep);
		
	}
	// not currently used
	public void listParticipants(RtcConnection rtc) {
		OneMydetvChannel mydetvChannel = rtc.getFromUser().getMydetvChannel();
		JSONObject body = new JSONObject()
			.put("id", mydetvChannel.getOjClient().getJanusId())
			.put("room", mydetvChannel.getOjClient().getJanusId())
			.put("request", "listparticipants");				
		JSONObject jsonObject = new JSONObject()
			.put("janus", "message")
			.put("body", body)			
			.put("opaque_id", rtc.getRtcUUID())
			.put("handle_id", rtc.getHandleId());
		jControl.sendTransaction(mydetvChannel, jsonObject, rtc);
	}

}
