package broadcast.janus;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.JanusStats;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;
import shared.db.DbJanusStats;

public class JAdminHandler {

	private static final Logger log = LoggerFactory.getLogger(JAdminHandler.class);
	@Inject private Blackboard blackboard;
	@Inject private JAdminClient jClient;
	@Inject private DbJanusStats dbJanusStats;
	
	/**
	 * track these by the transaction id<br>
	 * @param message
	 */
	public void onWebSocketText(String message) {
		log.trace("onWebSocketText:\n"+message);
		JSONObject received = new JSONObject(message);
		String transaction = received.getString("transaction");
		if(! jClient.getTransactions().containsKey(transaction)) {
			log.info("don't see we are tracking this transaction "+transaction);
			return;
		}
		JTransaction jTransaction = jClient.getTransactions().get(transaction);
		
		String janus = jTransaction.getSent().getString("janus");
		//log.info("matching "+janus);
		switch(janus) {
		case "list_sessions":
			break;
		case "handle_info":
			status(received);
			break;
		}
		jClient.getTransactions().remove(transaction);		
	}
	
	public void status(JSONObject received) {
		JanusStats janusStats = new JanusStats();

		if(received.isNull("handle_id")) {
			log.info("handle_id is null");
			return;
		}
		Long handleId = received.getLong("handle_id");
		
		SessionUser su = getSessionUserFromHandle(handleId);
		if(su==null) {
			log.info("can't match this handle to a sessionUser "+handleId);
			return;
		}
		janusStats.setChannelWebUsersId(su.getWebUser().getDbChannelWebUsersId());
		
		if(received.isNull("info")) {
			log.info("no info");
			return;
		}
		JSONObject info = received.getJSONObject("info");
		
		if(info.isNull("plugin_specific")) {
			log.info("no plugin_specific");
			return;
		}		
		JSONObject plugin_specific = info.getJSONObject("plugin_specific");
		
		if(plugin_specific.isNull("state")) {
			//log.info("no state");
			return;
		}
		String state = plugin_specific.getString("state");
		janusStats.setState(state);
		
		if(info.isNull("streams")) {
			log.info("no streams detected");
			return;
		}
		JSONArray streams = info.getJSONArray("streams");
		if(streams.length()!=1) {
			log.info("odd, streams length is "+streams.length());
			return;
		}
		JSONObject stream = streams.getJSONObject(0);
		JSONObject rtcpStats = stream.getJSONObject("rtcp_stats");		
		//log.info(rtcpStats.toString());
		janusStats.setRtcpStats(rtcpStats.toString());
		
		JSONArray components = stream.getJSONArray("components");
		if(components.length()!=1) {
			log.info("odd components length is "+components.length());
			return;
		}
		
		JSONObject component = components.getJSONObject(0);
		JSONObject inStats = component.getJSONObject("in_stats");
		janusStats.setInStats(inStats.toString());
		JSONObject outStats = component.getJSONObject("out_stats");
		janusStats.setOutStats(outStats.toString());
		
		dbJanusStats.insert(janusStats);
	}
	
	private SessionUser getSessionUserFromHandle(Long handleId) {
		SessionUser sessionUser = null;
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			if(! mydetvChannel.isActive())
				continue;
			JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			if(jClient.getHandles().containsKey(handleId)) {
				RtcConnection rtc = jClient.getHandles().get(handleId);
				return rtc.getToUser();
			}
		}
		return sessionUser;
	}
}
