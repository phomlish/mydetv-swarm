package broadcast.janus;

public class UnrecognizedException extends Exception {

	public UnrecognizedException(String message) {
		super(message);
	}
}
