package broadcast.janus;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.broadcast.RtcConnection;

public class JAdminControl {
	
	private static final Logger log = LoggerFactory.getLogger(JAdminControl.class);
	@Inject private JAdminClient jClient;
	@Inject private Config config;
	
	public void sendTransaction(JSONObject jsonObject, Boolean track) {
		String tid = UUID.randomUUID().toString();
		jsonObject
			.put("transaction", tid)
			.put("admin_secret", config.getJanusAdminSecret());
		
		if(track) {
			JTransaction jTrans = new JTransaction();
			jTrans.setTid(tid);
			jTrans.setSent(jsonObject);
			jClient.getTransactions().put(tid, jTrans);
		}		
		send(jsonObject);
	}
	
	private void send(JSONObject jsonObject) {
		log.trace("sending:"+WebHelper.PrettyPrint(jsonObject.toString()));
		Future<Void> fut = jClient.getSession().getRemote().sendStringByFuture(jsonObject.toString());
        try {
			fut.get(2,TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.error("InterruptedException "+e);
		} catch (ExecutionException e) {
			log.error("ExecutionException "+e);
		} catch (TimeoutException e) {
			log.error("TimeoutException "+e);
		} // wait for send to complete.   
	}
	
	public void status(RtcConnection rtc, Long sessionId) {
		JSONObject jsonObject = new JSONObject()
				.put("janus", "handle_info")
				.put("opaque_id", rtc.getRtcUUID())
				.put("session_id", sessionId)
				.put("handle_id", rtc.getHandleId());
			sendTransaction(jsonObject, true);	
	}
	
	public void ping() {
		JSONObject jsonObject = new JSONObject()
			.put("janus", "list_sessions");		
		sendTransaction(jsonObject, true);
	}

}
