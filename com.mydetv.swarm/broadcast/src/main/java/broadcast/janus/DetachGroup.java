package broadcast.janus;

import java.util.HashMap;

import shared.data.broadcast.RtcConnection;

public class DetachGroup {

	private HashMap<String, RtcConnection> detaches = new HashMap<String, RtcConnection>();
	private Thread sleeperThread;
	
	public HashMap<String, RtcConnection> getDetaches() {
		return detaches;
	}

	public void setDetaches(HashMap<String, RtcConnection> detaches) {
		this.detaches = detaches;
	}

	public Thread getSleeperThread() {
		return sleeperThread;
	}

	public void setSleeperThread(Thread sleeperThread) {
		this.sleeperThread = sleeperThread;
	}	
}
