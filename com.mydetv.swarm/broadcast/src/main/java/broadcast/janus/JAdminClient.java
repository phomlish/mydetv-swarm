package broadcast.janus;

import java.net.URI;
import java.util.HashMap;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.Config;

public class JAdminClient extends Thread {
	
	private static final Logger log = LoggerFactory.getLogger(JAdminClient.class);
	@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private JAdminListener jAdminListener;
	private boolean initialized=false;

	private WebSocketClient client;
	private Session session;
	private HashMap<String, JTransaction> transactions = new HashMap<String, JTransaction>();
	private Long lastDT = 0L;			// need to keep it alive
	
	@Override
	public void run() {
		log.info("starting our janus admin thread");
		connect();
	}
	
	public void connect() {
		//sharedConfig.getSslContextFactory().setTrustAll(true);  // if you can't figure out ssl certs
		
		try {
			HttpClient hclient = new HttpClient(blackboard.getSslContextFactory());
			hclient.start();
			client = new WebSocketClient(hclient);
			client.start();
			URI uri = new URI(config.getJanusAdminUrl());
			ClientUpgradeRequest request = new ClientUpgradeRequest();
			request.setSubProtocols("janus-admin-protocol");
			log.info("connecting to "+uri);
			//Future<Session> fut = client.connect(wsHandler,uri,request);
			client.connect(jAdminListener,uri,request);
			initialized=true;
		} catch (Exception e) {
			log.error("Exception caught: "+e);
		}
	}
	public void shutdown() throws Exception {
		client.stop();
		client.destroy();
		client=null;
	}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public Long getLastDT() {
		return lastDT;
	}
	public void setLastDT(Long lastDT) {
		this.lastDT = lastDT;
	}

	public HashMap<String, JTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(HashMap<String, JTransaction> transactions) {
		this.transactions = transactions;
	}

	public boolean isInitialized() {
		return initialized;
	}

	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}
}
