package broadcast.janus;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import broadcast.Blackboard;
import shared.data.Config;
import broadcast.master.MasterActions;
import broadcast.master.SlaveRecording;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.OneJClient;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;

/**
 * state:<br>
 *     static: playing video<br>
 *     tier0user: playing from a user<br>
 *     tier0device: playing from a device<br>
 *     
 * @author phomlish
 *
 */
public class JClient extends Thread {
	
	private static final Logger log = LoggerFactory.getLogger(JClient.class);
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private JVideoRoom jVideoRoom;	
	@Inject Provider<JListener> providerJListener;
	@Inject private JShutdown jShutdown;
	@Inject Provider<FFThread> providerFFThread;
	@Inject private SlaveRecording slaveRecording;
		
	// private variables
	private JListener jListener;					// web socket listener 
	private Session wsSession;						// web socket session
	private WebSocketClient wsClient;				// the connection to Janus
	private FFThread ffThread;						// the loop spawning ffmpeg processes
	private OneMydetvChannel mydetvChannel;			// the channel this jClient is devoted to
	private String convertedType = "vp8-opus";		// eventually we'll spawn multiple ffThreads, one for each convertedType we handle
	private Long lastDT = 0L;						// need to keep wsSession alive, pings handled in PingTask
	private Long session_id=0L;						// the Janus session id
	
	// These are created and removed meticulously after use.  status web pages can help verify cleanup is happening.
	private HashMap<String, JTransaction> transactions = new HashMap<String, JTransaction>();
	private HashMap<Long, RtcConnection> handles = new HashMap<Long, RtcConnection>();

	// CGSream and CGBroadcast are provided by the dummy SessionUser we bind to this Janus instance
	private SessionUser me;
	
	// we don't need a broadcaster unless this is a remote feed
	// TODO: remove broadcaster, use cgBroadcast upstream exists instead
	private SessionUser broadcaster = null;
	// playing SID clips or live
	private boolean live = false;
	// we'll set this true if we are shutting down
	private boolean shuttingDown = false;
	// do we have our handle id and are ready to work?  
	//    janus handler success tells us it is ready
	//    if a user joins before we are 'ready' we won't give them a feed
	private boolean ready = false;
	private boolean restart;			// only set when we get a web socket error
	private boolean inactivating=false;	// set when we inactivate the channel.
	private List<SessionUser> lost;  // removes all the ppl that are connected to disconnect
	
	// during a shutdown/web socket error/channel inactivate we have to wait for detaches since they are async
	private Object detachLock = new Object();
	private List<DetachGroup> detachGroups = new ArrayList<DetachGroup>();

	@Override
	public void run() {
		OneJClient ojc = mydetvChannel.getOjClient();
		ojc.setjClient(this);
		
		me = new SessionUser(config.getJanusUrl(),-1, "");
		me.setMediaPlayer(true);
		me.setWebUser(new OneWebUser());
		me.getWebUser().setUsername("mydetv channel "+mydetvChannel.getChannelId());
		
		me.setMydetvChannel(mydetvChannel);
		
		me.getCgStream().setProvider(me);
		me.getCgStream().setTier(1);
		me.getCgBroadcaster().setProvider(me);
		me.getCgBroadcaster().setTier(1);
		
		// broadcaster is me until someone else takes over
		if(mydetvChannel.getChannelType() == 0 || mydetvChannel.getChannelType() == 2)
			broadcaster=me;
		
		jListener = providerJListener.get();
		jListener.init(this);
		
		startup();
	}
	
	// called when instantiated and again if we deactivate/activate a channel
	public void startup() {
		createFfThread();
		connectJanus();
	}	
	
	public void createFfThread() {
		log.info("creating ffthread for "+mydetvChannel.getName());
		ffThread = providerFFThread.get();
		ffThread.setjClient(this);
		Thread thread = new Thread(ffThread);
		thread.setName("ffThread-"+mydetvChannel.getChannelId());
		ffThread.setMydetvChannel(mydetvChannel);
		thread.start();
	}
	
	public void connectJanus() {
		
		try {			
			log.info("creating wsClient for "+mydetvChannel.getName());
			wsClient = new WebSocketClient(blackboard.getHttpClient());
			wsClient.start();
			
			URI uri = new URI(config.getJanusUrl());
			ClientUpgradeRequest request = new ClientUpgradeRequest();
			request.setSubProtocols("janus-protocol");
			log.info("connecting to "+uri);
			//Future<Session> future = 
				wsClient.connect(jListener,uri,request);
			//future.get();
		} catch (Exception e) {
			log.error("Exception caught: "+e);
		}
	}
	
	/*
	 * ffThread will report here when it dies<br>
	 * * shutdown sets done=true and we wait for ffThreadDied<br>
	 */
	public void ffThreadDied() {
		log.info("ffThread died");
		mydetvChannel.setCurrentlyPlaying(null);		
		ffThread=null;
		
		// are we doing a complete jClient shutdown?
		if(shuttingDown) {
			try {
				jShutdown.finishFullJanusShutdown(this);
				return;
			} catch (Exception e) {
				log.error("error finishing jClient shutdown after ffthread died "+e);
			}
		}
		
		if(! live) {
			log.info("ffThread died and is not live, done dealing with ffThreadDied");
			return;
		}
		
		if(broadcaster == null) {
			log.info("ffThread died with no broadcasters, done dealing with ffThreadDied");
			return;
		}
		// studio needs to deal with blinky lights. 
		if(mydetvChannel.getChannelType()==2) 
			slaveRecording.updateRecordingLight(mydetvChannel);
		
		jVideoRoom.attachAsSender(broadcaster);
	}
	
	// getters/setters
	public OneMydetvChannel getMydetvChannel() {
		return mydetvChannel;
	}
	public void setMydetvChannel(OneMydetvChannel mydetvChannel) {
		this.mydetvChannel = mydetvChannel;
	}
	public FFThread getFfThread() {
		return ffThread;
	}
	public void setFfThread(FFThread ffThread) {
		this.ffThread = ffThread;
	}
	public SessionUser getMe() {
		return me;
	}
	public void setMe(SessionUser me) {
		this.me = me;
	}
	public Session getSession() {
		return wsSession;
	}
	public void setSession(Session session) {
		this.wsSession = session;
	}
	public Long getLastDT() {
		return lastDT;
	}
	public void setLastDT(Long lastDT) {
		this.lastDT = lastDT;
	}
	public Long getSession_id() {
		return session_id;
	}
	public void setSession_id(Long session_id) {
		this.session_id = session_id;
	}
	public HashMap<String, JTransaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(HashMap<String, JTransaction> transactions) {
		this.transactions = transactions;
	}
	public HashMap<Long, RtcConnection> getHandles() {
		return handles;
	}
	public void setHandles(HashMap<Long, RtcConnection> handles) {
		this.handles = handles;
	}
	public SessionUser getBroadcaster() {
		return broadcaster;
	}
	public void setBroadcaster(SessionUser presenter) {
		this.broadcaster = presenter;
	}
	public boolean isReady() {
		return ready;
	}
	public void setReady(boolean ready) {
		this.ready = ready;
	}
	public boolean isShuttingDown() {
		return shuttingDown;
	}
	public void setShuttingDown(boolean shuttingDown) {
		this.shuttingDown = shuttingDown;
	}
	public boolean isLive() {
		return live;
	}
	public void setLive(boolean live) {
		this.live = live;
	}
	public String getConvertedType() {
		return convertedType;
	}
	public void setConvertedType(String convertedType) {
		this.convertedType = convertedType;
	}
	public boolean isInactivating() {
		return inactivating;
	}
	public void setInactivating(boolean inactivating) {
		this.inactivating = inactivating;
	}
	public Object getDetachLock() {
		return detachLock;
	}
	public void setDetachLock(Object detachLock) {
		this.detachLock = detachLock;
	}
	public List<DetachGroup> getDetachGroups() {
		return detachGroups;
	}
	public void setDetachGroups(List<DetachGroup> detachGroups) {
		this.detachGroups = detachGroups;
	}
	public void setRestart(Boolean restart) {
		this.restart=restart;
	}
	public Boolean getRestart() {
		return restart;
	}
	public List<SessionUser> getLost() {
		return lost;
	}
	public void setLost(List<SessionUser> lost) {
		this.lost = lost;
	}
	public WebSocketClient getWsClient() {
		return wsClient;
	}
	public void setWsClient(WebSocketClient wsClient) {
		this.wsClient = wsClient;
	}
}
