package broadcast.janus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.Sched;
import broadcast.business.VideosHelpers;
import broadcast.data.FpnSeek;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.OneVideo;

public class FFThread extends Thread {

	private static final Logger log = LoggerFactory.getLogger(FFThread.class);
	@Inject private Config config;
	@Inject private Config sharedConfig;
	@Inject private VideosHelpers videosHelpers;
	@Inject private Sched sched;
	
	private JClient jClient;
	private OneMydetvChannel mydetvChannel;
	// done is the loop control.  
	// set to true if you really want it to die and not come back until you create a new ffthread
	private boolean done = false;
	// we have an x second watcher to see if we need to interrupt the process builder process 
	private boolean interuptFfmpegProcess = false;
	
	// no getters/setters
	private Process process;
	private String videoUrl;
	
	@Override
	public void run() {		
		log.info("started ffThread for "+jClient.getMydetvChannel().getName());
		loop();
	}
	
	public void loop() {
		while(!done) {
			broadcast.data.FpnSeek fpnSeek=null;
			OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
			log.info("loop channelType "+mydetvChannel.getChannelType());
			videoUrl = "";
			try {
				long startTime = System.nanoTime();
				
				// remote always sends sid until it becomes 'live' with a broadcaster/presenter
				if(mydetvChannel.getChannelType().equals(0)) {					
					OneVideo sid = videosHelpers.getRandomClip();
					mydetvChannel.setCurrentlyPlaying(null);
					videoUrl = videosHelpers.getVideoUrl(sid, jClient.getConvertedType());
					log.info("sending "+mydetvChannel.getName()+" "+videoUrl+" "+sid.log());
					videosHelpers.createAndSendVideoTitle(mydetvChannel);
					startProcess(ProcessBuilders.sendFile(
						config.getFfmpeg(), 
						videoUrl,
						jClient.getMydetvChannel().getOjClient().getJanusAudioPort(),
						jClient.getMydetvChannel().getOjClient().getJanusVideoPort()
						));
				}
				
				// studio
				else if(mydetvChannel.getChannelType().equals(2)) {
					log.info("dealing with studio");
					mydetvChannel.setCurrentlyPlaying(null);
					// live
					if(jClient.isLive()) {
						// dev sends a random clip since we don't always plug in the v4l device and never figured out the webcam for the mac
						if(sharedConfig.getInstance().equals("dev")) {
							OneVideo sid = videosHelpers.getRandomClip();
							mydetvChannel.setCurrentlyPlaying(sid);
							videoUrl = videosHelpers.getVideoUrl(sid, jClient.getConvertedType());
							log.info("sending "+mydetvChannel.getName()+" "+videoUrl+" "+sid.log());
							
							videosHelpers.createAndSendVideoTitle(mydetvChannel);
							startProcess(ProcessBuilders.sendFile(
								config.getFfmpeg(), 
								videoUrl,
								jClient.getMydetvChannel().getOjClient().getJanusAudioPort(),
								jClient.getMydetvChannel().getOjClient().getJanusVideoPort()
								));
						}
						else {
							log.info("sending "+mydetvChannel.getName()+" v4l2");
							videosHelpers.createAndSendVideoTitle(mydetvChannel);
							startProcess(ProcessBuilders.v4l2Gstreamer(
								config.getGstLaunch(), 
								jClient.getMydetvChannel().getOjClient().getJanusAudioPort(), 
								jClient.getMydetvChannel().getOjClient().getJanusVideoPort()
								));
						}
					}
					// not live
					else {
						OneVideo sid = videosHelpers.getRandomClip();
						mydetvChannel.setCurrentlyPlaying(sid);
						videoUrl = videosHelpers.getVideoUrl(sid, jClient.getConvertedType());
						log.info("sending "+mydetvChannel.getName()+" "+videoUrl+" "+sid.log());
						videosHelpers.createAndSendVideoTitle(mydetvChannel);
						startProcess(ProcessBuilders.sendFile(
							config.getFfmpeg(), 
							videoUrl,
							jClient.getMydetvChannel().getOjClient().getJanusAudioPort(),
							jClient.getMydetvChannel().getOjClient().getJanusVideoPort()
							));
						}
					}

				// 24x7 scheduled
				else if(mydetvChannel.getChannelType().equals(1)) {
					log.info("scheduled "+jClient.getMydetvChannel().getName());
					fpnSeek = sched.get24x7PlayNow(jClient.getMydetvChannel(), "vp8-opus");

					log.info("sending "+mydetvChannel.getName()+" "+fpnSeek.getFpn()+" seek:"+fpnSeek.getStrSeek());
					fpnSeek.setActualStartime(LocalDateTime.now());
					mydetvChannel.setCurrentlyPlaying(fpnSeek.getOv());
					videosHelpers.createAndSendVideoTitle(mydetvChannel);
					startProcess(ProcessBuilders.sendFileWithSeek(
						config.getFfmpeg(), 
						fpnSeek.getFpn(),
						jClient.getMydetvChannel().getOjClient().getJanusAudioPort(),
						jClient.getMydetvChannel().getOjClient().getJanusVideoPort(),
						fpnSeek.getStrSeek()
						));
					
					log.info(fpnSeekLog(fpnSeek));
					/*
					LocalDateTime now = LocalDateTime.now();
					Integer millisUntilScheduledEnd = (int) ChronoUnit.MILLIS.between(fpnSeek.getScheduledEndTime(),now);
					log.info("millisUntilScheduledEnd:"+millisUntilScheduledEnd);
					if(millisUntilScheduledEnd>3000) {
						log.info("we ended early by "+millisUntilScheduledEnd+" millis, sleeping");
						Thread.sleep(millisUntilScheduledEnd-2000);
					}
					*/
				}
				else if(mydetvChannel.getChannelType().equals(3)) {
					fpnSeek = sched.getSpecial(jClient.getMydetvChannel(), "vp8-opus");
					log.info("sending "+mydetvChannel.getName()+" "+fpnSeek.getFpn()+" seek:"+fpnSeek.getStrSeek());
					fpnSeek.setActualStartime(LocalDateTime.now());
					mydetvChannel.setCurrentlyPlaying(fpnSeek.getOv());
					videosHelpers.createAndSendVideoTitle(mydetvChannel);
					startProcess(ProcessBuilders.sendFileWithSeek(
						config.getFfmpeg(), 
						fpnSeek.getFpn(),
						jClient.getMydetvChannel().getOjClient().getJanusAudioPort(),
						jClient.getMydetvChannel().getOjClient().getJanusVideoPort(),
						fpnSeek.getStrSeek()
						));
					
					log.info(fpnSeekLog(fpnSeek));
					
				}
				else {
					log.error("didn't reognize channel type "+mydetvChannel.getChannelType());
				}
				// we finished a loop, make sure it wasn't too quick (like when a video is corrupt)
				long elapsedTime = System.nanoTime() - startTime;
				// 1,000,000,000 nanoseconds = 1 second
				if(elapsedTime < 5000000000L) {
					log.error("That was too quick "+videoUrl+" "+elapsedTime/1000000000+".  SLEEPING 5 seconds");
					Thread.sleep(1000);
				}
			
			} catch(InterruptedException e) {
				log.info("interupted");
				//doSleep(1000);
			} catch (Exception e) {			
				log.error("exception:"+e);
				if(fpnSeek==null) {
					log.error("fpnSeek was null");
					doSleep(10000);
				}
				else if(fpnSeek.getFpn()==null || fpnSeek.getFpn().length()<1) 
					log.error("fpnSeek fpn was blank");
				else
					log.error("some other issue");
			}
		} // end of loop
		
		log.info("Ffthread for "+jClient.getMydetvChannel().getName()+" is ended, done, kaput, disappearing, telling jClient");
		mydetvChannel.setCurrentlyPlaying(null);
		videosHelpers.createAndSendVideoTitle(mydetvChannel);
		jClient.ffThreadDied();		
	}
	
	private String fpnSeekLog(FpnSeek fpnSeek) {

		OneVideo ov = fpnSeek.getOv();
		Integer length = (int) Math.ceil(ov.getLength());
		Integer seek = fpnSeek.getSeekSeconds();
		Integer ls = length-seek;
		LocalDateTime estimatedStopTime = fpnSeek.getActualStartime().plusSeconds(ls);
		
		String rv="ffThread returned from playing file\n\n";
		rv+="length:"+length+",seek:"+seek+",length-seek:"+ls+"\n";
		rv+="actual start time:"  +fpnSeek.getActualStartime()+"\n";
		rv+="estimated stop time:"+estimatedStopTime+"\n";
		if(fpnSeek.getOs()!=null)
			rv+=sched.logSched(LocalDateTime.now(), fpnSeek.getOs());
		else {
			rv+="no schedule, must have been sid";
			rv+="ov:"+ov.log()+"\n";
		}
		return rv;
	}
	
	private void doSleep(Integer sleep) {
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			log.error("sleep interupted");
		}
	}
	private void startProcess(ProcessBuilder pb) {
		StringBuilder sb = new StringBuilder();
		interuptFfmpegProcess=false;
		log.info("sending "+pb.command());
		try {
			pb.environment().put("PATH", "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin");
			pb.environment().put("LD_LIBRARY_PATH", "/usr/local/lib");
			//for(Entry<String, String> entry : pb.environment().entrySet()) {
			//	log.info(entry.getKey()+":\t"+entry.getValue());
			//}
			pb.redirectErrorStream(true);
			process=pb.start();
			final Thread reader = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
						String line="";
						try {
							while((line=readerOutput.readLine()) != null) {
								sb.append(line+"\n");
							}
						} catch (IOException e) {
							log.info("exception reading "+e); //+"\n"+sb.toString());

						}
					} catch (Exception e) {
						log.info("exception running "+e); //+"\n"+sb.toString());
					}
				}
			});			
			reader.start();
			log.trace("reader started");
						
			// 1 second loop to watch the process.  When it dies or is interrupted are finished with the process 
			boolean finished = false;
			while(!finished) {
				if(!process.isAlive()) {
					log.info("process not alive");
					finished=true;
				}
				else if(interuptFfmpegProcess) {
					log.info("interuptMe");
					process.destroy();
					Thread.sleep(500);
				}
				else
					process.waitFor(1, TimeUnit.SECONDS);
			}
			log.info("done with ffthread process"); //\n"+sb.toString());
		} catch (Exception e1) {
			log.error("e:"+e1+"\n"+sb.toString());
		} 
	}
	
	// getters/setters
	public JClient getjClient() {
		return jClient;
	}
	public void setjClient(JClient jClient) {
		this.jClient = jClient;
	}
	public boolean isInteruptFfmpegProcess() {
		return interuptFfmpegProcess;
	}
	public void setInteruptFfmpegProcess(boolean interuptFfmpegProcess) {
		this.interuptFfmpegProcess = interuptFfmpegProcess;
	}
	public void setDone(Boolean done) {
		this.done=done;
	}
	public OneMydetvChannel getMydetvChannel() {
		return mydetvChannel;
	}
	public void setMydetvChannel(OneMydetvChannel mydetvChannel) {
		this.mydetvChannel = mydetvChannel;
	}
}
