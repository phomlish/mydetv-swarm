package broadcast.janus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * does processbuilder eat slashes?
 * @author phomlish
 *
 */
public class ProcessBuilders {

	private static final Logger log = LoggerFactory.getLogger(ProcessBuilders.class);
	
	//ffmpeg -re -i /private/nfs/a6/usr6/mydetv/webm/clips/sid2.webm -vcodec copy -an -f rtp rtp://10.11.1.96:5120 -vn -f rtp rtp://10.11.1.96:5118 
		public static ProcessBuilder sendFile(String ffmpeg, String fn, Integer aport, Integer vport) {
			ProcessBuilder pb = new ProcessBuilder(
				ffmpeg, 
				"-re", "-i",  fn,
				"-vcodec", "copy", "-an", "-f", "rtp", "rtp://10.11.1.96:"+vport,
				"-acodec", "copy", "-vn", "-f", "rtp", "rtp://10.11.1.96:"+aport
				);
			return pb;
		}
		
		// ffmpeg -re -i /private/nfs/a6/usr3/videos/clips/people.vp8.webm 
		// -vcodec copy -an -f rtp rtp://127.0.0.1:5024  
		// -acodec copy -vn -f rtp rtp://127.0.0.1:5022
		public static ProcessBuilder sendFileWithSeek(String ffmpeg, String fn, Integer aport, Integer vport, String seek) {
			if(seek.equals("")) {
				return sendFile(ffmpeg, fn, aport, vport);
			}
			log.trace("seeking to "+seek);
			ProcessBuilder pb = new ProcessBuilder(
				ffmpeg, 
				"-ss", seek,
				"-re", "-i",  fn,
				"-vcodec", "copy", "-an", "-f", "rtp", "rtp://10.11.1.96:"+vport,
				"-acodec", "copy", "-vn", "-f", "rtp", "rtp://10.11.1.96:"+aport
				);
			return pb;
		}
		
		public static ProcessBuilder transcodeAndSendFile(String ffmpeg, String fn, Integer aport, Integer vport) {
			String cmd = ""+ffmpeg+" -i "+fn;
			cmd+=" -c:v libvpx -crf 10 -b:v 512K";
			cmd+=" -an -f rtp rtp://10.11.1.96:"+vport;
			cmd+=" -c:a libopus -b:a 128k -vbr on -compression_level 10";
			cmd+=" -vn -f rtp rtp://10.11.1.96:"+aport;
			String[] parts = cmd.split(" ");
			ProcessBuilder pb = new ProcessBuilder(parts);
			return pb;
		}
		
		public static ProcessBuilder v4l2Gstreamer(String gstreamer, Integer aport, Integer vport) {
			StringBuilder sb = new StringBuilder();
			sb.append(gstreamer);
			sb.append(" -v -e");
			sb.append(" alsasrc num-buffers=-1 device=hw:1 provide-clock=false do-timestamp=true");
			sb.append(" ! \"audio/x-raw,format=S16LE,rate=44100,channels=2\"");
			sb.append(" ! audioconvert ! audioresample");
			sb.append(" ! opusenc ! rtpopuspay ! udpsink host=10.11.1.96 port="+aport);
			sb.append(" v4l2src device=/dev/video0 norm=NTSC");
			sb.append(" ! videoscale ! \"video/x-raw,width=640,height=480,framerate=15/1,interlace-mode=mixed\"");
			sb.append(" ! videoconvert ! \"video/x-raw,width=640,height=480\" ! videorate");
			sb.append(" ! vp8enc threads=4 ! rtpvp8pay ! udpsink host=10.11.1.96 port="+vport);
			log.info("starting\n"+sb.toString());
			ProcessBuilder pb = new ProcessBuilder(
				gstreamer,
				"-v","-e",
				"alsasrc","num-buffers=-1","device=hw:1","provide-clock=false","do-timestamp=true",
				"!","audio/x-raw,format=S16LE,rate=44100,channels=2",
				"!","audioconvert","!","audioresample",
				"!","opusenc","!","rtpopuspay","!","udpsink","host=10.11.1.96","port="+aport,
				"v4l2src","device=/dev/video0","norm=NTSC",
				"!","videoscale","!","video/x-raw,width=640,height=480,framerate=15/1,interlace-mode=mixed",
				"!","videoconvert","!","video/x-raw,width=640,height=480","!","videorate",
				"!","vp8enc","threads=4","!","rtpvp8pay","!","udpsink","host=10.11.1.96","port="+vport);
			return pb;
		}
}
