package broadcast.janus;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.broadcaster.BroadcasterHelpers;
import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.ConnectionGroup;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

/**
 * used when we are truly shutting down the jClient<br>
 * @author phomlish
 *
 */
public class JShutdown {

	private static final Logger log = LoggerFactory.getLogger(JShutdown.class);
	@Inject private JDetach jDetach;
	@Inject private BroadcasterHelpers jBroadcaster;
	@Inject private WsWriter wsWriter;
	@Inject private BroadcasterHelpers p2pBroadcastHelpers;
	
	/**
	 * called by<br>
	 * * shutdown hook<br>
	 * * onWebSocketError<br>
	 * * inactivateChannel<br>
	 */
	public void fullJanusShutdown(JClient jClient, boolean restart) throws Exception  {
		jClient.setRestart(restart);
		log.info("shutting down (restart="+restart+")");

		jClient.setShuttingDown(true);
		// shutdown the browser rtc connections for all the downstreams
		shutdownAllDownstreams(jClient);
		
		// detach all our downstreams
		List<RtcConnection> detachList = new ArrayList<RtcConnection>();	
		for(RtcConnection rtc : jClient.getMe().getCgStream().getDownstreams())
			detachList.add(rtc);
		jDetach.syncDetach(jClient.getMydetvChannel(), detachList);
		
		for(DetachGroup dg : jClient.getDetachGroups()) {
			jDetach.waitForDetaches(jClient, dg);
		}
		
		p2pBroadcastHelpers.removeBroadcasterJShutdown(jClient);
		
		FFThread ffThread = jClient.getFfThread();
		
		// wait for ffthread to finish
		if(ffThread!=null) {
			ffThread.setDone(true);
			ffThread.setInteruptFfmpegProcess(true);
			ffThread.interrupt();

			finishFullJanusShutdown(jClient);
		}
		// we can finish right away
		else
			finishFullJanusShutdown(jClient);
	}
	
	/**
	 * called immediately when no ffthread exists<br>
	 * or called after ffthread is interrupted<br>
	 * @throws Exception 
	 */
	public void finishFullJanusShutdown(JClient jClient) throws Exception {
		
		SessionUser me = jClient.getMe();
		
		log.info("finishShutdown "+jClient.getLost().size());
		// deal with lost
		for(SessionUser su : jClient.getLost()) {
			//su.getCgStream().setDownstreams(new ArrayList<RtcConnection>());
			List<RtcConnection> deleteRtcs;
			deleteRtcs = new ArrayList<RtcConnection>(su.getCgStream().getDownstreams());			
			log.info("finishShutdown delete downstreams:"+deleteRtcs.size());
			for(RtcConnection rtc : deleteRtcs) {
				log.info("removing "+rtc.log());
				su.getCgStream().getDownstreams().remove(rtc);
				//su.getRtcConnections().remove(rtc.getRtcUUID());
			}
			deleteRtcs = new ArrayList<RtcConnection>(su.getCgStream().getUpstreams());			
			log.info("finishShutdown delete upstreams:"+deleteRtcs.size());
			for(RtcConnection rtc : deleteRtcs) {
				log.info("removing "+rtc.log());
				su.getCgStream().getDownstreams().remove(rtc);
			}
			
			if(su.getCgStream().getUpstreams().size()!=1) {
				log.error("could not find upstream "+su.getCgStream().getUpstreams().size()+" "+su.log());
			}
			else {
				RtcConnection rtc = su.getCgStream().getUpstreams().get(0);
				log.info("removing upstream rtc "+rtc.log());
				su.getCgStream().getUpstreams().remove(0);
			}
			
			//log.info("removing upstream rtc "+su.getCgStream().getUpstream().log());
			//su.getRtcConnections().remove(su.getCgStream().getUpstream().getRtcUUID());
			//su.getCgStream().setUpstream(null);
			
			su.getCgStream().setTier(-1);
		}
		
		// deal with me
		log.info("finishShutdown me");
		List<RtcConnection> deleteRtc = new ArrayList<RtcConnection>(me.getCgStream().getDownstreams());			
		for(RtcConnection rtc : deleteRtc) {
			log.info("removing "+rtc.log());
			me.getCgStream().getDownstreams().remove(rtc);
		}
		// TODO: deal with upstream presenter...
		//me.getCgStream().setDownstreams(new ArrayList<RtcConnection>());
		
		log.info("finish shutdown");
		WebSocketClient wsClient = jClient.getWsClient();
		if(wsClient!=null) 
			wsClient.stop();
		
		if(wsClient!=null) 
			wsClient.destroy();
		
		if(wsClient!=null)
			wsClient=null;
		
		jClient.setSession(null);
		jClient.setSession_id(0L);
		
		// if we are inactivating we need to set db and send messages to the browsers
		
		if(jClient.isInactivating())
			jBroadcaster.finishInactivate(jClient.getMydetvChannel());
		
		
		log.info("shutdown complete");
		if(jClient.getRestart()) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				log.error("sleep interupted "+e);
			}	
			log.info("restarting");
			jClient.connectJanus();
		}
		else {
			OneMydetvChannel mydetvChannel = jClient.getMydetvChannel();
			mydetvChannel.getOjClient().setjClient(null);
		}
	}
	
	/**
	 * called from shutdown<br>
	 * Shutdown the downstream users.
	 */	
	private void shutdownAllDownstreams(JClient jClient) {
		
		// gather the downstreams and remove their downstreams and upstreams
		List<SessionUser> lost = new ArrayList<SessionUser>();
		jClient.setLost(lost);
		
		ConnectionGroup cgStream = jClient.getMe().getCgStream();
		lost = gatherDownstreamsRecurseTheTree(cgStream, lost);
		log.info("gathered "+lost.size()+" connected to tell them to shutdown their streams");
		if(lost.size()<1)
			return;
			
		// this list will include janus->user and user->user
		for(SessionUser lostDownstreamUser : lost) {
			
			log.info("    "+lostDownstreamUser.log());
			ConnectionGroup cgvDownstreamUser = lostDownstreamUser.getCgStream();

			// removing our upstream
			if(cgvDownstreamUser.getUpstreams().size()!=1) {
				log.error("could not find upstream "+cgvDownstreamUser.getUpstreams().size()+" "+lostDownstreamUser.log());
			}
			else {
				RtcConnection rtc = cgvDownstreamUser.getUpstreams().get(0);
				wsWriter.sendRemoveRTCConnection(lostDownstreamUser, rtc);
			}			
			//wsWriter.sendRemoveRTCConnection(lostDownstreamUser, cgvDownstreamUser.getUpstream());
			
			// remove our downstreams
			for(RtcConnection rtc : cgvDownstreamUser.getDownstreams()) {
				wsWriter.sendRemoveRTCConnection(lostDownstreamUser, rtc);

			}
		}		
		return;
	}
	
	public List<SessionUser> gatherDownstreamsRecurseTheTree(ConnectionGroup cgv, List<SessionUser> lost) {		
		for(RtcConnection webrtcConnection : cgv.getDownstreams()) {
			SessionUser su = webrtcConnection.getToUser();
			lost.add(su);
			lost = gatherDownstreamsRecurseTheTree(su.getCgStream(),  lost);
		}
		return lost;
	}
	
}
