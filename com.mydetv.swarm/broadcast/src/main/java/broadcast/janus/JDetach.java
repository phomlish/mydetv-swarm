package broadcast.janus;

import java.util.List;
import java.util.Map.Entry;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.p2p.CGHelpers;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.RtcConnection;

/**
 * since we have to wait for the async we wrap requests to detach in a synchronized
 * @author phomlish
 *
 */
public class JDetach {

	private static final Logger log = LoggerFactory.getLogger(JDetach.class);
	@Inject private JControl jControl;
	@Inject private JHelpers jHelpers;
	@Inject private CGHelpers cgHelpers;
	
	public DetachGroup syncDetach(OneMydetvChannel mydetvChannel, List<RtcConnection> detaches) {
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		DetachGroup dg = new DetachGroup();
		jClient.getDetachGroups().add(dg);
		synchronized(jClient.getDetachLock()) {
			for(RtcConnection rtc:detaches) {
				String tid = jControl.detach(rtc);
				dg.getDetaches().put(tid, rtc);
			}
		}
		return dg;
	}
	
	// {"janus":"success","session_id":1661449416376890,"transaction":"f2d62126-2a4e-4daa-aa5a-cae428b264e4"}
	public void syncHandleDetach(JClient jClient, JSONObject received) throws JException, UnrecognizedException {
		log.info("handle detach "+received);
		if(received.isNull("transaction"))
			throw new JException("detached does not include transaction");
			
		synchronized(jClient.getDetachLock()) {
			String tid = received.getString("transaction");
			DetachGroup dg = findDetachGroup(jClient, tid);
			if(dg==null)
				throw new JException("can't find our tid "+tid+" in any detachGroup");
			
			RtcConnection rtc = dg.getDetaches().get(tid);
			log.info("detached "+rtc.log());
			
			if(rtc.getConnectionType().equals("jtier0")) {
				//jClient.getMe().getCgStream().setUpstream(null);
				jClient.getMe().getCgStream().getUpstreams().remove(rtc);
			}
			else if(rtc.getConnectionType().equals("janus")){
				jClient.getMe().getCgStream().getDownstreams().remove(rtc);
			}
			else {
				log.error("trying to figure out type "+rtc.log());
			}
			
			dg.getDetaches().remove(tid);
					
			// remove the transaction
			jHelpers.removeTransaction(jClient, received);
			// remove the handle
			jClient.getHandles().remove(rtc.getHandleId());
			log.info("detach removed "+received);
		}		
	}
	
	/**
	 * called by<br>
	 *     BroadcastHelpers.removeBroadcaster
	 *     
	 * @param jClient
	 * @param dg
	 */
	public void waitForDetaches(JClient jClient, DetachGroup dg) {
		DetachSleeperRunnable dsr = new DetachSleeperRunnable();
		dsr.setDetachGroup(dg);
		dsr.seJClient(jClient);
		Thread sThread = new Thread(dsr);
			sThread.run();
			try {
				sThread.join();
			} catch (InterruptedException e) {
				log.info("interupted");
			}
		
	}

	private DetachGroup findDetachGroup(JClient jClient, String tid) {
		for(DetachGroup dg : jClient.getDetachGroups())
			if(dg.getDetaches().containsKey(tid))
				return dg;
		return null;		
	}
	
	
	class DetachSleeperRunnable implements Runnable {

		JClient jClient;
		DetachGroup dg;
		
		@Override
		public void run() {
			Boolean done=false;
			
			Integer cntLoops=0;
			while(!done && cntLoops<10) {

				Integer cnt=0;
				for(Entry<String, RtcConnection> entry : dg.getDetaches().entrySet()) {
					log.info("waiting for "+entry.getKey()+" "+entry.getValue().log());
					cnt++;
				}
				
				if(cnt==0) 
					done=true;
				else {
					try {
						Thread.sleep(1000);
						cntLoops++;
					} catch (InterruptedException e) {
						log.error("thread interupted");
					}
				}
			}
		}	

		public void seJClient(JClient jClient) {
			this.jClient=jClient;
		}
		public void setDetachGroup(DetachGroup dg) {
			this.dg=dg;
		}
			
	}
}
