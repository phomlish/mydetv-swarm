package broadcast.janus;

import java.time.LocalDateTime;

import com.google.inject.Inject;

import broadcast.web.ws.WsWriter;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JConnectViewer {
	
	//private static final Logger log = LoggerFactory.getLogger(JConnectViewer.class);
	
	@Inject private WsWriter websocketWriter;
	@Inject private JControl jControl;
	
	public void connectViewer(SessionUser toUser) {
		OneMydetvChannel mydetvChannel = toUser.getMydetvChannel();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		
		RtcConnection rtc = new RtcConnection("janus", jClient.getMe(), toUser);
		String cid=rtc.getRtcUUID();

		jClient.getMe().getCgStream().getDownstreams().add(rtc);

		//toUser.getCgStream().setUpstream(rtc);
		toUser.getCgStream().getUpstreams().add(rtc);
		toUser.getCgStream().setTier(2);
		//toUser.getRtcConnections().put(cid, rtc);
		
		websocketWriter.sendSetupRTCConnection(toUser, rtc, "receiver");
		rtc.setStarted(LocalDateTime.now());
		
		// tell janus to create an offer
		jControl.attachStream(mydetvChannel, cid, rtc);
	}

}
