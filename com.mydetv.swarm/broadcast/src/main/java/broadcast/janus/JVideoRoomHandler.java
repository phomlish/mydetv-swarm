package broadcast.janus;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.web.ws.WsWriter;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JVideoRoomHandler {

	private static final Logger log = LoggerFactory.getLogger(JVideoRoomHandler.class);
	@Inject private JHelpers jHelpers;
	@Inject private JVideoRoom jVideoRoom;
	@Inject private WsWriter wsWriter;
	
	public void janusEvent(JClient jClient, JSONObject received) throws UnrecognizedException {
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no received data");
		JSONObject rData = rPlugindata.getJSONObject("data");
		
		if(rData.isNull("videoroom"))
			throw new UnrecognizedException("no videoroom");
		
		String videoroom = rData.getString("videoroom");
		if(videoroom.equals("joined")) 
			jVideoRoom.publishLive(jClient, received);
		else if(videoroom.equals("attached"))
			jVideoRoom.attached(jClient, received);
		else if(videoroom.equals("event")) {
			if(! rData.isNull("configured")) {
				if(! received.isNull("jsep")) {
					String transaction = received.getString("transaction");
					JTransaction jt = jClient.getTransactions().get(transaction);
					RtcConnection rtc = jt.getRtc();
					JSONObject jsep = received.getJSONObject("jsep");
					SessionUser sessionUser = rtc.getFromUser();
					log.info("sending janus live answer to "+sessionUser.log());
					wsWriter.sendAnswer(sessionUser, rtc.getRtcUUID(), jsep);
				}
				else {
					log.info("no jsep sent with configured which is probably ok, we wait for the browser to hook up the video");
				}
			}
			else if(! rData.isNull("unpublished")) {
				log.info("not dealing with unpublished event");
			}
			else
				throw new UnrecognizedException("didn't recognize videoroom event :"+rData.toString());
			jHelpers.removeTransaction(jClient, received);
		}
		else 
			log.error("didn't recognize "+videoroom);		
	}
	
	public void handleSuccess(JClient jClient, JSONObject received) throws UnrecognizedException {
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		RtcConnection rtc = jt.getRtc();
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no received data");
		
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no data for videoroom success:"+jt.getTid());		
		JSONObject data = rPlugindata.getJSONObject("data");
		
		if(data.isNull("videoroom"))
			throw new UnrecognizedException("no videoroom element in success");
		
		String videoroom = data.getString("videoroom");
		if(videoroom.equals("event"))
			throw new UnrecognizedException("events should have been caught before success was called");
		
		switch(videoroom) {
		case "participants":
			log.info("got participants");
			return;
		case "rtp_forward":
			log.info("got rtp_forward success, ignoring");
			jHelpers.removeTransaction(jClient, received);
			return;
		default:
			log.info("didn't recognize success videoroom element:"+videoroom);
		}
		
		if(data.isNull("id"))
			throw new UnrecognizedException("no id in videoroom data for success:"+jt.getTid());

		String janusSent = jt.getSent().getString("janus");
		switch(janusSent) {
			case "attach":
				rtc.setHandleId(data.getLong("id"));
				log.info("got stream id:"+jt.getRtc().getHandleId()+" for "+rtc.getConnectionType()+" "+jClient.getMydetvChannel().getName());
				jClient.getHandles().put(jt.getRtc().getHandleId(), rtc);
				if(rtc.getConnectionType().equals("jtier0"))
					jVideoRoom.joinAsSender(jClient.getMydetvChannel(), rtc);
				else
					log.error("didn't recognize "+rtc.getConnectionType());
				break;
			default:
				throw new UnrecognizedException("didn't recognize success:"+janusSent);
		}
		
		jClient.getTransactions().remove(jt.getTid());
		
		
	}
}
