package broadcast.janus;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.p2p.FCHelpers;
import broadcast.p2p.VideoWaiters;
import shared.data.broadcast.FaultyConnection;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JHandlerHelpers {

	@Inject private VideoWaiters videoWaiters;
	@Inject private JStreamingHandler jStreamingHandler;
	@Inject private JVideoRoomHandler jVideoRoomHandler;
	@Inject private JVideoRoom jVideoRoom;
	@Inject private JDetach jDetach;
	@Inject private JHelpers jHelpers;
	@Inject private FCHelpers fcHelpers;
	@Inject private JControl jControl;
	private static final Logger log = LoggerFactory.getLogger(JHandlerHelpers.class);
	
	public void handleAck(JClient jClient, JSONObject received) throws UnrecognizedException {
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		String janusSent = jt.getSent().getString("janus");
		if(janusSent.equals("trickle")
		|| janusSent.equals("keepalive"))
			jClient.getTransactions().remove(jt.getTid());
		else
			jt.setDtProgress(System.currentTimeMillis());
	}
	
	public void handleKeepalive(JClient jClient, JSONObject received) throws UnrecognizedException {
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		jClient.getTransactions().remove(jt.getTid());
	}

	public void handleTrickle(JClient jClient, JSONObject received) throws UnrecognizedException, JException {
		log.info("trickle");
	}
	public void handleSuccess(JClient jClient, JSONObject received) throws UnrecognizedException, JException {
		
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		String janusSent = jt.getSent().getString("janus");
		if(janusSent.equals("detach")) {
			jDetach.syncHandleDetach(jClient, received);
			return;
		}
		
		if(! received.isNull("plugindata")) {
			JSONObject plugindata = received.getJSONObject("plugindata");
			if(plugindata.isNull("plugin"))
				throw new UnrecognizedException("plugin in plugindata:"+jt.getTid());
			String plugin = plugindata.getString("plugin");
			log.info("handling plugin success "+plugin);
			
			if(plugin.equals("janus.plugin.videoroom"))
				jVideoRoomHandler.handleSuccess(jClient, received);
			else if(plugin.equals("janus.plugin.streaming"))
				jStreamingHandler.handleSuccess(jClient, received);
			else 
				throw new UnrecognizedException("didn't recognize success plugin:"+plugin);
			return;
		}
		if(received.isNull("data"))
			throw new UnrecognizedException("no data for success:"+jt.getTid());		
		JSONObject data = received.getJSONObject("data");
		
		if(data.isNull("id"))
			throw new UnrecognizedException("no id in data for success:"+jt.getTid());
		
		if(jt.getRtc() != null)
			log.info("handling success when we sent "+janusSent+" for "+jt.getRtc().getConnectionType());
		switch(janusSent) {
			case "create":
				jClient.setSession_id(data.getLong("id"));
				log.info("got our session_id:"+jClient.getSession_id()+" for "+jClient.getMydetvChannel().getName());
				jClient.setReady(true);
				videoWaiters.connectViewersWaiting(jClient.getMydetvChannel());
				break;
			
			case "attach":
				RtcConnection rtc = jt.getRtc();
				rtc.setHandleId(data.getLong("id"));
				log.info("got stream id:"+jt.getRtc().getHandleId()+" for "+rtc.getConnectionType()+" "+jClient.getMydetvChannel().getName());
				jClient.getHandles().put(jt.getRtc().getHandleId(), rtc);
				if(rtc.getConnectionType().equals("janus"))
					jControl.watch(jClient.getMydetvChannel(), rtc);
				else if(rtc.getConnectionType().equals("jtier0"))
					jVideoRoom.joinAsSender(jClient.getMydetvChannel(), rtc);
				else
					log.error("didn't recognize "+rtc.getConnectionType());
				break;
				
			default:
				throw new UnrecognizedException("didn't recognize success:"+janusSent);
		}
		
		jClient.getTransactions().remove(jt.getTid());
		
	}

	public void handleEvent(JClient jClient, JSONObject received) throws UnrecognizedException {
		
		if(received.isNull("plugindata"))				
			throw new UnrecognizedException("no received pluginData");
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		
		String plugin = rPlugindata.getString("plugin");
		switch(plugin) {
			case "janus.plugin.videoroom":
				jVideoRoomHandler.janusEvent(jClient, received);
				break;
			case "janus.plugin.streaming":
				jStreamingHandler.streamingEvent(jClient, received);
				break;
			default:
				log.error("didn't recognize plugin "+plugin);
		}
	}
	public void handleWebrtcup(JClient jClient, JSONObject received) throws UnrecognizedException {
		RtcConnection rtc = jHelpers.getRtcFromHandle(jClient, received);
		SessionUser su = rtc.getToUser();
		log.info("webrtcup from "+su.log());	
		
		if(rtc.getToUser().equals(jClient.getMe())) {
			jVideoRoom.forwardStream(su);
		}
		else {
			//userMessageHelpers.sendUserMessage(su, "info", "enjoy the show");
		}
	}
	
	/*
	"janus": "slowlink",
   "session_id": 2650028289109309,
   "sender": 2246697817361260,
   "uplink": true,
   "nacks": 9
	 */
	public void handleSlowlink(JClient jClient, JSONObject received) throws UnrecognizedException {
		SessionUser su = jHelpers.getSessionUserFromHandle(jClient, received);
		RtcConnection rtc = jHelpers.getRtcFromHandle(jClient, received);
		Integer nacks = 0;
		if(! received.isNull("nacks"))
			nacks = received.getInt("nacks");
		//FaultyConnection fc = fcHelpers.createMediaPlayer2U(rtc.getConnectionType(), "slow", su, nacks);
		fcHelpers.addFc(rtc, "slow", rtc.getToUser(), nacks);
		//su.getFaultyConnections().put(LocalDateTime.now(), fc);
		log.warn(su.log()+" has slowlinks:"+nacks);
	}
}
