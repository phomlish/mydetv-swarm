package broadcast.janus;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JHelpers {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(JHelpers.class);
	
	public void removeTransaction(JClient jClient, JSONObject message) throws UnrecognizedException {
		if(message.isNull("transaction"))
			throw new UnrecognizedException("no transaction");
		String transaction = message.getString("transaction");
		if(! jClient.getTransactions().containsKey(transaction))
			throw new UnrecognizedException("no transaction matching");
		jClient.getTransactions().remove(transaction);
	}
	public RtcConnection getRtcOfSessionUser(JClient jClient, SessionUser su) {
		for(RtcConnection rtc : jClient.getHandles().values())
			if(rtc.getToUser().equals(su))
				return(rtc);
		return null;
	}
	public JTransaction getJTransaction(JClient jClient, JSONObject received) throws UnrecognizedException {
		if(received.isNull("transaction"))
			throw new UnrecognizedException("no transaction");
		String tid = received.getString("transaction");
		if(! jClient.getTransactions().containsKey(tid))
			throw new UnrecognizedException("jclient has no transaction matching "+tid);
		return(jClient.getTransactions().get(tid));
	}
	public RtcConnection getRtcFromHandle(JClient jClient, JSONObject received) throws UnrecognizedException {		
		if(received.isNull("sender"))
			throw new UnrecognizedException("no sender for started");
		Long sender = received.getLong("sender");
		if(!jClient.getHandles().containsKey(sender))
			throw new UnrecognizedException("can't find handle "+sender);		
		return(jClient.getHandles().get(sender));
	}
	public SessionUser getSessionUserFromHandle(JClient jClient, JSONObject received) throws UnrecognizedException {
		SessionUser sessionUser = null;
		if(received.isNull("sender"))
			throw new UnrecognizedException("no sender for started");
		Long sender = received.getLong("sender");
		
		if(!jClient.getHandles().containsKey(sender))
			throw new UnrecognizedException("can't find handle "+sender);		
		RtcConnection rtc = jClient.getHandles().get(sender);
		sessionUser = rtc.getToUser();
		return sessionUser;
	}
	
}
