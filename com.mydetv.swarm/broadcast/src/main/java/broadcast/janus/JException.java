package broadcast.janus;

public class JException extends Exception {

	public JException(String message) {
		super(message);
	}
}
