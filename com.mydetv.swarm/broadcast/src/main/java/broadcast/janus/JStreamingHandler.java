package broadcast.janus;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.web.ws.WsWriter;
import shared.data.broadcast.RtcConnection;

public class JStreamingHandler {

	private static final Logger log = LoggerFactory.getLogger(JStreamingHandler.class);
	@Inject private JControl jControl;
	@Inject private WsWriter wsWriter;
	@Inject private JHelpers jHelpers;
	
	public void streamingEvent(JClient jClient, JSONObject received) throws UnrecognizedException {
		JSONObject rPlugindata = received.getJSONObject("plugindata");
		if(rPlugindata.isNull("data"))
			throw new UnrecognizedException("no received data");
		JSONObject rData = rPlugindata.getJSONObject("data");
		
		if(rData.isNull("result"))
			throw new UnrecognizedException("no data result");
		JSONObject rResult = rData.getJSONObject("result");
		
		if(rResult.isNull("status"))
			throw new UnrecognizedException("no result status");
		String status = rResult.getString("status");
		
		JTransaction jt;
		RtcConnection rtc;
		switch(status) {		
		case "preparing":
			jt = jHelpers.getJTransaction(jClient, received);
			if(received.isNull("jsep"))
				throw new UnrecognizedException("no jsep");
			JSONObject jsep = received.getJSONObject("jsep");
			
			if(jt.getRtc() == null) 
				throw new UnrecognizedException("no rtc");
			 rtc = jt.getRtc();
			
			wsWriter.sendOffer(rtc.getToUser(), rtc.getRtcUUID(), jsep);
			jClient.getTransactions().remove(jt.getTid());
			return;
			
		// not doing anything with it...
		case "starting":
			//log.info("janus started for "+jClient.getMydetvChannel().getName());
			jt = jHelpers.getJTransaction(jClient, received);
			jClient.getTransactions().remove(jt.getTid());
			return;
			
		// just for practice, let's see if we can match this handle
		case "started":
			if(received.isNull("sender"))
				throw new UnrecognizedException("no sender for started");
			Long sender = received.getLong("sender");
			
			if(!jClient.getHandles().containsKey(sender))
				throw new UnrecognizedException("can't find handle "+sender);		
			rtc = jClient.getHandles().get(sender);
			log.info("streaming to "+rtc.getToUser().log());
			return;
		
		case "stopped":
		case "stopping":
			return;
		}
		// if we get here our case missed something
		throw new UnrecognizedException("Didn't recognize status:"+status);		
	}
	
	public void handleSuccess(JClient jClient, JSONObject received) throws UnrecognizedException {
		JTransaction jt = jHelpers.getJTransaction(jClient, received);
		RtcConnection rtc = jt.getRtc();
			
		if(received.isNull("data"))
			throw new UnrecognizedException("no data for success:"+jt.getTid());		
		JSONObject data = received.getJSONObject("data");
		
		if(data.isNull("id"))
			throw new UnrecognizedException("no id in data for success:"+jt.getTid());

		String janusSent = jt.getSent().getString("janus");
		switch(janusSent) {
			case "attach":
				rtc.setHandleId(data.getLong("id"));
				log.info("got stream id:"+jt.getRtc().getHandleId()+" for "+rtc.getConnectionType()+" "+jClient.getMydetvChannel().getName());
				jClient.getHandles().put(jt.getRtc().getHandleId(), rtc);
				if(rtc.getConnectionType().equals("janus"))
					jControl.watch(jClient.getMydetvChannel(), rtc);
				else
					log.error("didn't recognize "+rtc.getConnectionType());
				break;
			default:
				throw new UnrecognizedException("didn't recognize success:"+janusSent);
		}
		
		jClient.getTransactions().remove(jt.getTid());
	}
}
