package broadcast.janus;

import org.json.JSONObject;

import shared.data.broadcast.RtcConnection;

public class JTransaction {

	private String session;
	private String tid;
	private JSONObject sent;
	private RtcConnection rtc;
	private Long dtStarted = System.currentTimeMillis();
	private Long dtProgress=System.currentTimeMillis();
	
	// getters/setters
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public Long getDtStarted() {
		return dtStarted;
	}
	public void setDtStarted(Long dtStarted) {
		this.dtStarted = dtStarted;
	}
	public Long getDtProgress() {
		return dtProgress;
	}
	public void setDtProgress(Long dtProgress) {
		this.dtProgress = dtProgress;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public JSONObject getSent() {
		return sent;
	}
	public void setSent(JSONObject sent) {
		this.sent = sent;
	}
	public RtcConnection getRtc() {
		return rtc;
	}
	public void setRtc(RtcConnection rtc) {
		this.rtc = rtc;
	}

	
}
