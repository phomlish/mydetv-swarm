package broadcast.janus;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.business.WebHelper;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.RtcConnection;

public class JControl {

	private static final Logger log = LoggerFactory.getLogger(JControl.class);
	@Inject private Config config;

	public String sendTransaction(OneMydetvChannel mydetvChannel, JSONObject jsonObject) {
		return(sendTransaction(mydetvChannel, jsonObject, null));
	}
	public String sendTransaction(OneMydetvChannel mydetvChannel, JSONObject jsonObject, RtcConnection rtc) {
		return(sendTransaction(mydetvChannel, jsonObject, rtc, true));
	}	
	public String sendTransaction(OneMydetvChannel mydetvChannel, JSONObject jsonObject, RtcConnection rtc, Boolean track) {
		//String str = jsonObject.toString();
		JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
		if(jClient==null)
			log.info("bad");
		String tid = UUID.randomUUID().toString();
		jsonObject.put("transaction", tid);
		jsonObject.put("session_id", jClient.getSession_id());
		if(config.getJanusApiSecret() != null)
			jsonObject.put("apisecret", config.getJanusApiSecret());
		
		if(track) {
			JTransaction jTrans = new JTransaction();
			jTrans.setTid(tid);
			jTrans.setSent(jsonObject);
			jTrans.setRtc(rtc);
			jClient.getTransactions().put(tid, jTrans);
		}
		
		if(! jsonObject.getString("janus").equals("keepalive")
		&& ! jsonObject.getString("janus").equals("trickle"))
			log.info("sending:"+WebHelper.PrettyPrint(jsonObject.toString()));
		
		send(jClient, jsonObject);
		return tid;
	}

	private void send(JClient jClient, JSONObject jsonObject) {
		if(jClient==null) {
			log.error("hold on now, jClient is null");
		}
		if(jClient.getSession()==null) {
			log.error("hold on now, jClient session is null");
		}
		Future<Void> fut = jClient.getSession().getRemote().sendStringByFuture(jsonObject.toString());
        try {
			fut.get(2,TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.error("InterruptedException "+e);
		} catch (ExecutionException e) {
			log.error("ExecutionException "+e);
		} catch (TimeoutException e) {
			log.error("TimeoutException "+e);
		} // wait for send to complete.   
	}
	
	public void attachStream(OneMydetvChannel mydetvChannel, String cid, RtcConnection rtc) {		
		JSONObject jsonObject = new JSONObject()
			.put("janus", "attach")
			.put("opaque_id", cid)
			.put("plugin", "janus.plugin.streaming");
		sendTransaction(mydetvChannel, jsonObject, rtc, true);		
	}	

	public void watch(OneMydetvChannel mydetvChannel, RtcConnection rtc) {
		JSONObject body = new JSONObject()
			.put("id", mydetvChannel.getOjClient().getJanusId())
			//.put("id", 111)  // used to test a remote connection
			.put("request", "watch");				
		JSONObject jsonObject = new JSONObject()
			.put("janus", "message")
			.put("body", body)			
			.put("opaque_id", rtc.getRtcUUID())
			.put("handle_id", rtc.getHandleId());
		sendTransaction(mydetvChannel, jsonObject, rtc);	
	}
	
	public void start(RtcConnection rtc) {
		JSONObject body = new JSONObject()
				.put("id", rtc.getToUser().getMydetvChannel().getOjClient().getJanusId())
				.put("request", "start");				
		JSONObject jsonObject = new JSONObject()
				.put("janus", "message")
				.put("body", body)
				.put("opaque_id", rtc.getRtcUUID())
				.put("handle_id", rtc.getHandleId());
			sendTransaction(rtc.getToUser().getMydetvChannel(), jsonObject, rtc);	
	}
	
	// no don't, this kills the mount point
	public void destroyDontUseThis(RtcConnection rtc) {
		JSONObject body = new JSONObject()
				.put("id", rtc.getToUser().getMydetvChannel().getOjClient().getJanusId())
				.put("secret", rtc.getToUser().getMydetvChannel().getOjClient().getJanusSecret())
				.put("admin_key", rtc.getToUser().getMydetvChannel().getOjClient().getJanusAdminKey())
				.put("request", "destroy");				
		JSONObject jsonObject = new JSONObject()
				.put("janus", "message")
				.put("body", body)
				.put("opaque_id", rtc.getRtcUUID())
				.put("handle_id", rtc.getHandleId());
			sendTransaction(rtc.getToUser().getMydetvChannel(), jsonObject, rtc);	
	}

	/**
	 * I think this stops the jClient session
	 * @param rtc
	 */
	public void stop(RtcConnection rtc) {
		JSONObject body = new JSONObject()
				.put("id", rtc.getToUser().getMydetvChannel().getOjClient().getJanusId())
				.put("request", "stop")
				;				
		JSONObject jsonObject = new JSONObject()
				.put("janus", "message")
				.put("body", body)
				.put("opaque_id", rtc.getRtcUUID())
				.put("handle_id", rtc.getHandleId());
			sendTransaction(rtc.getToUser().getMydetvChannel(), jsonObject, rtc, true);	
	}
	
	public String detach(RtcConnection rtc) {
		JSONObject jsonObject = new JSONObject()
				.put("janus", "detach")
				.put("opaque_id", rtc.getRtcUUID())
				.put("handle_id", rtc.getHandleId());
			return(sendTransaction(rtc.getToUser().getMydetvChannel(), jsonObject, rtc));	
	}
	

}
