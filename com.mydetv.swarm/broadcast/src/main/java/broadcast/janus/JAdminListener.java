package broadcast.janus;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class JAdminListener implements WebSocketListener {
	
	private static final Logger log = LoggerFactory.getLogger(JAdminListener.class);
	@Inject private JAdminClient jAdminClient;
	@Inject private JAdminHandler jAdminHandler;
	
	@Override
	public void onWebSocketClose(int statusCode, String reason) {
		log.info("onWebSocketClose");
		jAdminClient.setSession(null);
		jAdminClient.connect();
	}
	
	@Override
	public void onWebSocketConnect(Session session) {
		jAdminClient.setSession(session);
		log.info("onWebSocketConnect ");
	}

	@Override
	public void onWebSocketBinary(byte[] payload, int offset, int len) {
		log.info("onWebSocketBinary");
	}

	@Override
	public void onWebSocketText(String message) {
		jAdminHandler.onWebSocketText(message);
	}
	@Override
	public void onWebSocketError(Throwable cause) {
		log.info("onWebSocketError: "+cause.getMessage());
		if(! cause.getMessage().equals("Connection refused"))
			log.error("Unrecognized error:"+cause.getMessage());
		else
			log.info("onWebSocketError: "+cause.getMessage());
		
		
		try {
			jAdminClient.shutdown();
		} catch (Exception e1) {
			log.error("Error shutting down");
		}
		
		try {
			
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			log.error("sleep interupted "+e);
		}
		
		jAdminClient.setSession(null);
		jAdminClient.connect();
	}
	
}
