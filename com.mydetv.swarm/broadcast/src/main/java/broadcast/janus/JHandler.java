package broadcast.janus;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class JHandler {

	private static final Logger log = LoggerFactory.getLogger(JHandler.class);
	@Inject private JHandlerHelpers jHandlerHelpers;
	
	private JClient jClient;

	public void init(JClient jClient) {
		this.setjClient(jClient);	
	}
	public void onWebSocketText(String message) {
		
		JSONObject received = new JSONObject(message);
		
		try {			
			String janus = received.getString("janus");
			if(! janus.equals("ack") && ! janus.equals("keepalive"))
				log.info("onWebSocketText:"+received);
			
			switch(janus) {
				case "ack":
					jHandlerHelpers.handleAck(jClient, received);
					return;
				case "keepalive":
					jHandlerHelpers.handleKeepalive(jClient, received);
					return;
				case "trickle":
					
					return;
				case "success":
					jHandlerHelpers.handleSuccess(jClient, received);
					return;
				case "event":
					jHandlerHelpers.handleEvent(jClient, received);
					return;
				case "webrtcup":
					jHandlerHelpers.handleWebrtcup(jClient, received);
					return;
				case "slowlink":
					jHandlerHelpers.handleSlowlink(jClient, received);
					return;
				case "media":
				case "detached":
				case "hangup":
					log.debug("eating "+janus+", no need to do anything");
					return;
				default:
					log.error("Unrecognized janus:\n"+message);
					return;
				}
			
		} catch (UnrecognizedException e) {
			log.error(e+"\n"+message);
		} catch (Exception e) {
			log.error("UNEXPECTED Exception "+e+"\n"+message);
		}
	}

	public void setjClient(JClient jClient) {
		this.jClient = jClient;
	}

}
