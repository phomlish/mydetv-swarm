package broadcast.janus;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import broadcast.Blackboard;

public class JListener implements WebSocketListener {

	private static final Logger log = LoggerFactory.getLogger(JListener.class);

	@Inject Provider<JHandler> providerJHandler;
	@Inject private JControl jControl;
	@Inject private Blackboard blackboard;
	private JClient jClient;
	private JHandler jHandler;
	@Inject private JShutdown jShutdown;

	public void init(JClient jClient) {
		this.jClient = jClient;
		jHandler = providerJHandler.get();
		jHandler.init(jClient);
	}
	
	@Override
	public void onWebSocketClose(int statusCode, String reason) {
		log.info("onWebSocketClose "+jClient.getMydetvChannel().getName());
		// if this is not 24x7 we might have closed and not want to re-open
		if(jClient != null && ! jClient.isShuttingDown()) {
			jClient.setSession(null);
			jClient.setSession_id(0L);
			if(! blackboard.getShuttingDown())
				jClient.connectJanus();
		}
	}

	@Override
	public void onWebSocketConnect(Session session) {
		jClient.setSession(session);
		log.info("onWebSocketConnect mydetvChannel:"+jClient.getMydetvChannel().getName());
		     
		JSONObject jsonObject = new JSONObject()
			.put("janus", "create");
		jControl.sendTransaction(jClient.getMydetvChannel(), jsonObject);       
	}

	@Override
	public void onWebSocketError(Throwable cause) {
		log.info("onWebSocketError for "+jClient.getMydetvChannel().getName()+": "+cause.getMessage());
		if(cause.getMessage().equals("Connection refused"))
			log.info("onWebSocketError: "+cause.getMessage());
		else if(cause.getClass()==org.eclipse.jetty.websocket.api.UpgradeException.class) {
			log.error("error UpgradeException");
			return;
		}
		else {
			log.error("Unrecognized error:"+cause.getClass().getName()+" : "+cause.getMessage());
			return;
		}
		
		try {
			jShutdown.fullJanusShutdown(jClient, true);
		} catch (Exception e1) {
			log.error("Exception shutting down "+jClient.getMydetvChannel().getName(),e1);
		}
	}

	@Override
	public void onWebSocketBinary(byte[] payload, int offset, int len) {
		log.info("onWebSocketBinary");
	}

	@Override
	public void onWebSocketText(String message) {
		jHandler.onWebSocketText(message);
	}
    
	
	
    
}
