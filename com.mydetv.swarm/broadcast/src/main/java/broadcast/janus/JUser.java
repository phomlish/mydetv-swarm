package broadcast.janus;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.p2p.CGHelpers;
import shared.data.broadcast.RtcConnection;
import shared.data.broadcast.SessionUser;

public class JUser {

	private static final Logger log = LoggerFactory.getLogger(JUser.class);
	@Inject private JControl jControl;
	@Inject private CGHelpers cgHelpers;
	
	public void wsAnswer(JSONObject answer, SessionUser su) {
		log.info("processing answer "+answer);
		
		//RtcConnection rtc = su.getCgStream().getUpstream();
		/*
		if(su.getCgStream().getUpstreams().size()!=1) {
			log.error("could not find rtc "+su.getCgStream().getUpstreams().size()+" "+su.log());
			return;
		}
		RtcConnection rtc = su.getCgStream().getUpstreams().get(0);
		*/
		String cid = answer.getString("cid");
		String connectionType = answer.getString("connectionType");
		RtcConnection rtc = cgHelpers.findRtcFromConnectionType(su, connectionType, cid);
		
		JSONObject body = new JSONObject()
			.put("request", "start");
			
		//JSONObject jsep = new JSONObject(response);
		JSONObject send = new JSONObject()
			.put("janus", "message")
			.put("handle_id", rtc.getHandleId())
			.put("jsep", answer.getJSONObject("answer"))
			.put("body", body);

		jControl.sendTransaction(su.getMydetvChannel(), send, rtc);
	}

	public void wsIceCandidate(JSONObject iceCandidate, SessionUser sessionUser) {
		
		String cid = iceCandidate.getString("cid");
		String connectionType = iceCandidate.getString("connectionType");
		RtcConnection rtc = cgHelpers.findRtcFromConnectionType(sessionUser, connectionType, cid);
		
		JSONObject candidate = iceCandidate.getJSONObject("candidate");
		JSONObject send = new JSONObject()
			.put("janus", "trickle")
			.put("handle_id", rtc.getHandleId())
			.put("candidate", candidate);
		jControl.sendTransaction(sessionUser.getMydetvChannel(), send, rtc);
	}
	
	public void wsIceCandidateDone(JSONObject iceCandidate, SessionUser sessionUser) {
		
		String cid = iceCandidate.getString("cid");
		String connectionType = iceCandidate.getString("connectionType");
		RtcConnection rtc = cgHelpers.findRtcFromConnectionType(sessionUser, connectionType, cid);
		
		JSONObject candidate = new JSONObject()
			.put("completed", true);
		JSONObject send = new JSONObject()
			.put("janus", "trickle")
			.put("handle_id", rtc.getHandleId())
			.put("candidate", candidate);
		jControl.sendTransaction(sessionUser.getMydetvChannel(), send, rtc);
	}

	
}
