package broadcast.business;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TreeMap;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.web.ws.WsWriter;
import shared.data.OneChatBlock;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUserRole;
import shared.db.DbWebUsers;

public class PeopleHelpers {

	@Inject private WsWriter websocketWriter;
	@Inject private Blackboard blackboard;
	@Inject private WsWriter wsWriter;
	@Inject private DbWebUsers dbWebUsers;
	private static final Logger log = LoggerFactory.getLogger(PeopleHelpers.class);

	/**
	 * when a new user joins a channel (doInit)<br>
	 * 
	 * @param mydetvChannel
	 * @param newPerson
	 */
	public void drawPeople(OneMydetvChannel mydetvChannel, SessionUser newPerson) {

		TreeMap<Integer, SessionUser> sorted = new TreeMap<Integer, SessionUser>();

		// add ppl in the channel
		// we sort by when they entered the channel (DbChannelWebUsersId)
		for(SessionUser sessionUser : mydetvChannel.getSessionUsers().values()) {
			if(sessionUser.getWebUser()==null || sessionUser.getWebUser().getDbChannelWebUsersId()==0)
				continue;
			sorted.put(sessionUser.getWebUser().getDbChannelWebUsersId(), sessionUser);
		}
		
		String rv = "<div class='people' id='people'>\n";
		for(SessionUser sessionUser : sorted.values())		
			rv+=drawPerson(mydetvChannel, newPerson, sessionUser);
		rv+="</div>\n"; // people
		log.info("draw people: "+rv);
		
		// fill out this guys full list of web users in the channel
		JSONObject message = new JSONObject()
				.put("verb", "people")
				.put("action", "replace")
				.put("people", rv);
		wsWriter.sendMessage(newPerson, message);

		// **** send everyone else the new person ****
		JSONObject newPersonJo = new JSONObject()
				.put("verb", "people")
				.put("action", "newperson");
		
		for(SessionUser sessionUser : mydetvChannel.getSessionUsers().values()) {
			if(sessionUser.equals(newPerson))
				continue;
			if(sessionUser.getWebSocketConnection() == null 
			|| sessionUser.getWebSocketConnection().getWebsocketSession() == null)
				continue;
			Session wss = (Session) sessionUser.getWebSocketConnection().getWebsocketSession();
			if( ! wss.isOpen())
				continue;
			newPersonJo.put("person", drawPerson(mydetvChannel, sessionUser, newPerson));
			wsWriter.sendMessage(sessionUser, newPersonJo);
		}	
	}

	// when they leave (doClose), remove them
	public void removePerson(OneMydetvChannel mydetvChannel, SessionUser person) {
		JSONObject dimPerson = new JSONObject()
				.put("verb", "people")
				.put("action", "removeperson")
				.put("person", person.getWebUser().getWuid()+"-"+person.getWebUser().getDbChannelWebUsersId());

		for(SessionUser sessionUser : mydetvChannel.getSessionUsers().values()) {
			if(sessionUser.equals(person))
				continue;
			if(sessionUser.getWebUser()==null || sessionUser.getWebUser().getDbChannelWebUsersId()==0)
				continue;
			if(sessionUser.getWebSocketConnection() == null 
					|| sessionUser.getWebSocketConnection().getWebsocketSession() == null)
				continue;
			Session wss = (Session) sessionUser.getWebSocketConnection().getWebsocketSession();
			if( ! wss.isOpen())
				continue;
			wsWriter.sendMessage(sessionUser, dimPerson);
		}	
	}
	
	private String drawPerson(OneMydetvChannel mydetvChannel, SessionUser forPerson, SessionUser newPerson) {
		log.info("draw "+newPerson.log()+" for "+forPerson.log());
		Boolean me = false;
		if(forPerson.getWebUser().getDbChannelWebUsersId()==newPerson.getWebUser().getDbChannelWebUsersId())
			me=true;
		Boolean blocked = isBlocked(forPerson, newPerson);
		
		// if they recently left we dim their image
		Boolean dim = false;		
		if(newPerson.getMydetvChannel() == null || (newPerson.getMydetvChannel().getChannelId().compareTo(mydetvChannel.getChannelId())!=0))
			dim=true;
		
		String personId = newPerson.getWebUser().getWuid()+"-"+newPerson.getWebUser().getDbChannelWebUsersId();

		String rv="";
		rv+="<div class='personDiv";
		if(blocked) rv+=" personBlocked ";
		rv+="' id='person-"+personId+"'>\n"; 
		
		//rv+="<span id='blocked-"+personId+"'>";
		//if(blocked) rv+="<span class='fas fa-thumbs-down'></span>";
		//rv+="</span>";
		
		// if he is still in the channel and it's not me
		if(! dim && !me) {
			rv+="<video id='personVideo-"+personId+"' class='personVideo d-none' autoplay ";
			rv+="></video>\n";
			rv+="<div class='personButton' href='#' role='button' id='dd-"+personId+"'";
			rv+=" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>\n";
		}

		rv+="<img class='person";
		if(dim) 
			rv+=" personDim ";
		rv+="' id='personImg-"+personId+"'";
		rv+=" title='"+newPerson.log()+"'";
		if(newPerson.getWebUser().getWebUserSelectedImage().equals(-1))
			rv+="src='/s/images/buffalo.png'";
		else {
			//OneWebUserImage oneWebUserImage = newPerson.getWebUser().getImages().get(newPerson.getWebUser().getWebUserSelectedImage());
			//rv+="src='data:image/png;base64, "+Base64.getEncoder().encodeToString(oneWebUserImage.getOrigFile())+"'";
			String selectedImage = newPerson.getWebUser().getImages().get(newPerson.getWebUser().getWebUserSelectedImage()).getNewFn();
			String url = "/ext/people/"+newPerson.getWebUser().getWuid()+"/"+selectedImage;
			rv+="src='"+url+"'";
			
		}
		rv+=">"; // img

		if(! me) {
			rv+="</div>"; // personButton
			
			rv+="<div class='dropdown-menu personDropdown'  aria-labelledby='dd-"+personId+"'>\n";
			rv+="<a class='dropdown-item personItem' href='#' onclick='personInfo(\""+personId+"\");'>info & settings</a>\n";
			rv+="<a class='dropdown-item personItem' href='#' onclick='privateChat(\""+personId+"\");'>private chat</a>\n";
			rv+="<a class='dropdown-item personItem' href='#' onclick='blockPerson(\""+personId+"\");'>block/unblock</a>\n";	
			if(newPerson.haveCamera() && forPerson.haveCamera()) {
				rv+="<a class='dropdown-item personItem' href='#' onclick='privateVideo(\""+personId+"\");'>private video</a>\n";
				rv+="<a class='dropdown-item personItem' href='#' onclick='mutePerson(\""+personId+"\");'>mute</a>\n";	
			}
			rv+="<a class='dropdown-item personItem' href='#' onclick='reportPerson(\""+personId+"\");'>report</a>\n";	
			rv+="</div>"; // personDropdown
		}

		rv+="</div>\n"; // personDiv
		return rv;
	}

	public void personInfo(JSONObject response, SessionUser sessionUser) {
		JSONObject jo = new JSONObject(response.toString());
		String strPersonId = jo.getString("personId");
		String[] pieces = strPersonId.split("-");
		Integer personId = Integer.parseInt(pieces[0]);
		log.info("return info for "+personId);
		SessionUser target = blackboard.getSessionUserByWuid(personId);

		JSONObject out = new JSONObject();
		out.put("verb", "personInfo");

		if(target==null) {
			out.put("personId", "");
			websocketWriter.sendMessage(sessionUser, out);
			return;
		}

		out.put("personId", strPersonId);
		out.put("link", target.getWebUser().getLink());
		out.put("username", target.log());

		String roles="participant";
		for(OneWebUserRole role : target.getWebUser().getRoles().values()) {		
			roles+=", "+role.getName();
		}
		out.put("roles", roles);

		String bday = "";
		if(target.getWebUser().getBirthday()!=null) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd");
			bday=dtf.format(target.getWebUser().getBirthday());
		}
		out.put("bday", bday);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd, YYYY HH:mm:ss");
		ZoneId oldZone = ZoneId.of("UTC");
		ZoneId newZone = ZoneId.of(target.getWebUser().getUserTimezone());
		LocalDateTime newDateTime = target.getWebUser().getDtAdded().atZone(oldZone)
				.withZoneSameInstant(newZone)
				.toLocalDateTime();
		out.put("added",dtf.format(newDateTime));

		out.put("tz", target.getWebUser().getUserTimezone());
		websocketWriter.sendMessage(sessionUser, out);
	}

	/**
	 * is target blocked from su
	 * @param su
	 * @param target
	 * @return
	 */
	public Boolean isBlocked(SessionUser su, SessionUser target) {
		for(OneChatBlock cb : su.getWebUser().getChatBlocked().values())
			if(cb.getIdBlocked().equals(target.getWebUser().getWuid()))
				return true;
		return false;
	}
	public Boolean isBlocked(SessionUser su, Integer target) {
		for(OneChatBlock cb : su.getWebUser().getChatBlocked().values())
			if(cb.getIdBlocked().equals(target))
				return true;
		return false;
	}
	public OneChatBlock getChatBlock(SessionUser su, Integer target) {
		for(OneChatBlock cb : su.getWebUser().getChatBlocked().values())
			if(cb.getIdBlocked().equals(target))
				return cb;
		return null;
	}
	
	public void reportPerson(JSONObject response, SessionUser su) {
		JSONObject jo = new JSONObject(response.toString());
		String strPersonId = jo.getString("personId");
		String[] pieces = strPersonId.split("-");
		Integer personId = Integer.parseInt(pieces[0]);
		log.info("reporting "+personId);
		SessionUser target = blackboard.getSessionUserByWuid(personId);
		String reason = jo.getString("reason");
		
		Integer rid = dbWebUsers.insertReportPerson(su.getWebUser().getWuid(), target.getWebUser().getWuid(), reason);
		log.info(su.log()+" reported "+target.log()+" rid:"+rid);
		
		JSONObject out = new JSONObject();
		out.put("verb", "reportPerson");
		out.put("result", "ok");
	}

}
