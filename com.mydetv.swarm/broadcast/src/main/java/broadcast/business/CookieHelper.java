package broadcast.business;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.NewCookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;

public class CookieHelper {

	private static final Logger log = LoggerFactory.getLogger(CookieHelper.class);
	@Inject private Config sharedConfig;
	
	/**
	 * make a temporary cookie that only lasts for the session
	 * @param name
	 * @param value
	 * @return
	 */
	public NewCookie makeSessionCookie(String name, String value) {
		NewCookie cookie = new NewCookie(name, sharedConfig.getHostname()+":"+value, 
				"/", sharedConfig.getHostname(), 1, "", -1, true);
		return cookie;
	}
	
	/**
	 * make a permanent cookie that lasts for 10 years
	 * @param name
	 * @param value
	 * @return
	 */
	public NewCookie makeStickyCookie(String name, String value) {
		NewCookie cookie = new NewCookie(name, sharedConfig.getHostname()+":"+value, 
				"/", sharedConfig.getHostname(), 1, "", 60 * 60 * 24 * 365 * 10, true);
		return cookie;
	}
	
	public String findCookieValue(HttpServletRequest req, String name) {
		Cookie cookie = findCookie(req, name);
		if(cookie==null)
			return null;
		
		return cookie.getValue();
	}
	
	private Cookie findCookie(HttpServletRequest req, String name) {
		Cookie cookie = null;
		if(req.getCookies() == null || req.getCookies().length<1) {
			log.info("no cookies for "+name);
			return cookie;
		}
		int found=0;
		for(Cookie c : req.getCookies()) {
			if(c.getName().equals(name)) {
				cookie=c;
				log.trace("cookie "+found+",name:"+c.getName()+",value:"+c.getValue());
				found++;
			}
		}
		if(found>1) {
			log.info("You have more than one cookie "+name+" "+found);
		}
		return cookie;
	}
	
	/**
	 * find a cookie with a value including the hostname<br>
	 * IE: dev.swarm.mydetv.com:e3863434-6680-4a94-b637-f1dc5f8379a2<br>
	 * We do this to differentiate between swarm and dev.swarm cookies
	 * @param req
	 * @param name
	 * @return
	 */
	public String findMydetvCookieValue(HttpServletRequest req, String name) {
		if(req.getCookies() == null || req.getCookies().length<1) {
			log.info("no cookies found from the browser");
			return null;
		}
		
		int found=0;
		
		String rv=null;
		for(Cookie c : req.getCookies()) {
			// strip the hostname from the cookie
			String[] cSplit = c.getValue().split(":");
			if(cSplit.length!=2)
				continue;
			if(! cSplit[0].equals(sharedConfig.getHostname()))
				continue;
			
			//if(c.getName().equals(name) && blackboard.getSessionUsers().containsKey(cSplit[1])) {
			if(c.getName().equals(name)) {	
				rv = cSplit[1];
				log.debug("cookie "+found+" "+c.getValue());
				found++;
			}
		}
		if(found>1) {
			log.info("You have more than one cookie named :"+name+"found:"+found);
		}
		return rv;
	}
	
	


}
