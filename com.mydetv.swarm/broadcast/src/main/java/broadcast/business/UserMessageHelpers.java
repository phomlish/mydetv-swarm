package broadcast.business;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.web.ws.WsWriter;
import shared.data.Config;
import shared.data.broadcast.SessionUser;

public class UserMessageHelpers {
	
	private static final Logger log = LoggerFactory.getLogger(UserMessageHelpers.class);
	@Inject private WsWriter wsWriter;
	@Inject private Config sharedConfig;

	static DateTimeFormatter tf = DateTimeFormatter.ofPattern("HH:mm:ss");
	
	public String initializeUserMessage() {
		String message = "<font color='green'>Initializing</font>";
		
		String rv="";
		rv+="<li class='userMessageButton'>\n";
		rv+="<div id='userMessage' onclick='userStatusAlert()'>"+message+"</div>\n";
		rv+="</li>\n";
		rv += "<ul id='userMessagesOld' style='display: none'>\n";
		rv+="<li>"+message+"</li>\n";
		rv+="</ul>\n";
		return rv;
	}
	
	/**
	 * users will always have messages as the navbar load gives them 'initializing'<br>
	 * 
	 * @param sessionUser
	 * @param mtype
	 * @param message
	 */
	public void sendUserMessage(SessionUser sessionUser, String mtype, String message) {
		if(sessionUser.isMediaPlayer())
			return;
		log.info("adding for "+sessionUser.log()+" "+message);
		
		LocalDateTime now = LocalDateTime.now();
		switch(mtype) {
		case "error":
			message = "<font color='red'>"+message+"</font>\n";
			break;
		case "warn":
			message = "<font color='yellow'>"+message+"</font>\n";
			break;
		case "debug":
			if(sharedConfig.getInstance().equals("dev"))
				message = "<font color='grey'>"+message+"</font>\n";
			else
				return;
		default:
			message = "<font color='green'>"+message+"</font>\n";		    	
		}

		if(sharedConfig.getInstance().equals("dev"))
			message = tf.format(now)+" "+message;
		wsWriter.sendStatusMessage(sessionUser, message);
	}

}
