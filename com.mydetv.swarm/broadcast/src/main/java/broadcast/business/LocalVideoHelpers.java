package broadcast.business;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.broadcaster.BroadcasterHelpers;
import broadcast.janus.JClient;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

public class LocalVideoHelpers {

	private static final Logger log = LoggerFactory.getLogger(LocalVideoHelpers.class);
	@Inject private BroadcasterHelpers p2pBroadcastHelpers;
	
	public void localVideoStarted(SessionUser su, JSONObject jo) {
		if(su.getLocalVideo()!=null) {
			log.error(su.log()+" already had a local video");
		}		
		su.setLocalVideo(jo.getJSONObject("media"));
		
		// why did they start their local video?
		if(su.getBroadcastChannel()!=null) {
			// ignore maybe?
		}
		else {
			// u2u maybe...
		}
		
	}
	
	public void localVideoStopped(SessionUser su, JSONObject jo) {
		if(su.getLocalVideo()==null) {
			log.error(su.log()+" didn't have a local video");
		}
		su.setLocalVideo(null);
		if(su.getCgBroadcaster().getDownstreams().size()>0) {
			log.info("shutting down the broadcaster");
			if(su.getBroadcastChannel()==null) {
				log.error("odd, how did they close their localvideo and they are not even in a channel");
				return;
			}
			OneMydetvChannel mydetvChannel = su.getBroadcastChannel();
			if(!mydetvChannel.isActive()) {
				log.info("they stopped their local video but the channel is already inactive");
				return;
			}
			JClient jClient = (JClient) mydetvChannel.getOjClient().getjClient();
			if(jClient==null) {
				log.info("they stopped their local video jClient is null");
				return;
			}
			p2pBroadcastHelpers.switchToRemoteSid(jClient);
		}
		// why did they start their local video?
		if(su.getBroadcastChannel()!=null) {
			// ignore maybe?
		}
		else {
			// u2u maybe...
		}
		
	}
	
	
}
