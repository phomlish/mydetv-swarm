package broadcast.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.OneChannelSchedule;
import shared.data.OneMydetvChannel;

public class ChannelSchedules {

	private static final Logger log = LoggerFactory.getLogger(ChannelSchedules.class);
	
	public OneChannelSchedule getChannelSchedule(OneMydetvChannel mydetvChannel, Integer pkey) {
		OneChannelSchedule ocs = null;
		
		for(OneChannelSchedule tocs : mydetvChannel.getChannelSchedules().values()) {
			if(tocs.getChannelsId()!=mydetvChannel.getChannelId()) {
				log.error("meltdown, how did this get here:"+tocs.getIdchannelSchedule());
				return null;
			}
			//log.info("looking for "+pkey+" versus "+tocs.getPkey());
			if(! tocs.getPkey().equals(pkey))
				continue;
			if(ocs!=null) {
				log.error("Error, multiple schedules "+tocs.getIdchannelSchedule()+" "+ocs.getIdchannelSchedule());
				return null;
			}
			ocs=tocs;
		}
		
		return ocs;
	}
		
		
	
}
