package broadcast.business;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.Config;
import broadcast.data.FpnSeek;
import broadcast.janus.JClient;
import broadcast.web.ws.WsWriter;
import shared.data.OneMbp;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoCategorySub;
import shared.data.VideoCategoryMain;
import shared.data.OneVideoConverted;
import shared.data.broadcast.OneJClient;
import shared.data.broadcast.SessionUser;
import shared.helpers.FilenameHelpers;

public class VideosHelpers {
	private static final Logger log = LoggerFactory.getLogger(VideosHelpers.class);
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private Authorized authorized;
	@Inject private WsWriter wsWriter;

	public OneVideo getRandomClip() {
		int index = blackboard.getSecureRandom().nextInt(blackboard.getClips().size());		
		return blackboard.getClips().get(index);
	}
	
	public FpnSeek getRandomClip(JClient jClient, Integer secondsUntilEnd) {
		OneVideo ov = getRandomClip();
		Integer secondsToSeek = Integer.valueOf(ov.getLength().intValue()) - secondsUntilEnd;
		if(secondsToSeek<0)
			secondsToSeek=0;
		log.info("random seek "+secondsToSeek+"\n"+ov.log());
		return new FpnSeek(getVideoUrl(ov,jClient.getConvertedType()),secondsToSeek, ov, null);
	}
	

	public OneVideoConverted getVideoConverted(OneVideo ov, String convertedType) {
		for(OneVideoConverted ovc : blackboard.getVideosConverted().values()) {
			if(! ovc.getPkey().equals(ov.getPkey()))
				continue;
			if(ovc.getConvertedType().equals(convertedType)) {
				return ovc;
			}
		}
		log.error("Error finding converted video for "+ov.log());
		return null;
	}

	// helpers for converted videos
	public List<OneVideoConverted> getAllVideoConverted(OneVideo ov) {
		ArrayList<OneVideoConverted> videosConverted = new ArrayList<OneVideoConverted>();

		for(OneVideoConverted ovc : blackboard.getVideosConverted().values()) {
			if(! ovc.getPkey().equals(ov.getPkey()))
				continue;
				videosConverted.add(ovc);
		}
		return videosConverted;
	}
	
	public String getVideoUrl(OneVideo ov, String convertedType) {
		return("file://"+getVideoFilesystemPath(ov, getVideoConverted(ov, convertedType)));
	}
	public String getCatMainName(OneVideo ov) {
		if(! blackboard.getVideoSubcats().containsKey(ov.getCatSub())) 
			return "";
		VideoCategorySub ovcs = blackboard.getVideoSubcats().get(ov.getCatSub());
		if(! blackboard.getVideoMaincats().containsKey(ovcs.getCatMain())) 
			return "";
		VideoCategoryMain ovcm = blackboard.getVideoMaincats().get(ovcs.getCatMain());
		return ovcm.getName();
	}
	public String getCatSubName(OneVideo ov) {
		if(! blackboard.getVideoSubcats().containsKey(ov.getCatSub())) 
			return "";
		VideoCategorySub ovcs = blackboard.getVideoSubcats().get(ov.getCatSub());
		return ovcs.getName();
	}
	public String getVideoFilesystemPath(OneVideo ov, OneVideoConverted ovc) {			
		String videourl = config.getVideosConvertedDirectory() + "/"+getVideoPath(ov, ovc);
		return videourl;
	}
	public String getVideoPath(OneVideo ov, OneVideoConverted ovc) {
		String subdir = ov.getSubdir();
		VideoCategorySub osc = blackboard.getVideoSubcats().get(ov.getCatSub());
		VideoCategoryMain vcm = blackboard.getVideoMaincats().get(osc.getCatMain());
		String subcatSubdir = vcm.getName();			
		String videourl = "";
		if(subcatSubdir!=null && subcatSubdir.length()>0)
			videourl+=subcatSubdir+"/";
		if(subdir != null && subdir.length()>0) 
			videourl+=subdir+"/";

		if(ovc.getFn()==null)
			return "";
		videourl+=ovc.getFn();
		return videourl;
	}
	
	public String getVideoPath(OneVideo ov) {
		String subdir = ov.getSubdir();
		VideoCategorySub osc = blackboard.getVideoSubcats().get(ov.getCatSub());
		VideoCategoryMain vcm = blackboard.getVideoMaincats().get(osc.getCatMain());
		String subcatSubdir = vcm.getName();				
		String videourl = "";
		if(subcatSubdir!=null && subcatSubdir.length()>0)
			videourl+=subcatSubdir+"/";
		if(subdir != null && subdir.length()>0) 
			videourl+=subdir+"/";
		videourl+=ov.getFn();
		return videourl;
	}
	
	private String prettyTitle(OneVideo ov) {

		String rv="";
		if(ov.getTitle()==null || ov.getTitle().equals("")) 
			rv+=FilenameHelpers.StripSuffix(ov.getFn());
		else
			rv=ov.getTitle();

		if(blackboard.getVideoSubcats().containsKey(ov.getCatSub())) {		
			VideoCategorySub ovcs = blackboard.getVideoSubcats().get(ov.getCatSub());
			VideoCategoryMain ovcm = blackboard.getVideoMaincats().get(ovcs.getCatMain());

			if(ovcm.getName().equals("mbp")) {
				if(!blackboard.getMbps().containsKey(ov.getPkey()))
					return rv;
				OneMbp mbp = blackboard.getMbps().get(ov.getPkey());
				
				if(mbp.getShowdate()!=null && ! mbp.getShowdate().equals("")) {
					rv+=" "+mbp.getShowdate();
					if(mbp.getShowset()!=null &&  mbp.getShowset()>0) {
						rv+=" set "+mbp.getShowset();
					}
				}		
			}
		}
		
		if(rv!=null)
			return rv;
		return "";
	}
	

	public String videoTitleDropdownForSchedule(OneMydetvChannel channel, OneVideo ov, OneSchedule os) {

		String title=prettyTitle(ov);
		String onclick = " onclick='dmcaSchedule("+channel.getChannelId()+","+ov.getPkey()+","+os.getIdschedule()+");' ";
		String rv="";
		rv+="<div class='dropdown-toggle' data-toggle='dropdown'>";
		rv+=title;
		rv+="<ul class='dropdown-menu'>";
		rv+="<li><a href='#'"+onclick+">Submit DMCA Notice</a></li>";
		rv+="<li><form id='dmca' action='/dmca' method='POST'>";
		rv+="<input id='donaldduck' type='hidden' name='q' value='a'>";
		rv+="<button type='submit'>Send your message</button>";
		rv+="</form></li>";
		rv+="</ul>";
		rv+="</div>";
		return rv;
	}
	
	public void createAndSendVideoTitle(OneMydetvChannel channel) {
		MyTitle title = prettyTitleWithShowDetail(channel);
		
		for(SessionUser su : channel.getSessionUsers().values()) {		
			if(su.getMydetvChannel()==null || ! su.getMydetvChannel().equals(channel))
				continue;
			if(su.getWebSocketConnection()==null)
				continue;
			JSONObject message = new JSONObject()
				.put("verb", "newVideoTitle")
				.put("title", videoTitleDropdown(su, title));
			wsWriter.sendMessage(su, message);
		}
	}
	
	/**
	 * used for p2p and broadcast page to show the current video title when they load the page
	 * @param su
	 * @param channel
	 * @return
	 */
	public String videoTitleDropdown(SessionUser su, OneMydetvChannel channel) {
		MyTitle title = prettyTitleWithShowDetail(channel);
		return(videoTitleDropdown(su, title));		
	}
	
	private String videoTitleDropdown(SessionUser su, MyTitle title) {

		if(! title.isActive())
			return "";
		
		String ocp = title.getChannel().getChannelId()+","; // on click parameters
		if(title.isLive())
			ocp += "0";
		else
			ocp += ""+title.getOv().getPkey();

		String rv="";
		
		rv+="<div class='dropdown-toggle' data-toggle='dropdown'>";
		rv+="<div id='videoTitleText'>"+title.getTitle()+"</div>";
		if(title.getOv()!=null)
			rv+="<div id='ofn' class='d-none'>"+title.getOv().getFn()+"</div>";
		rv+="<span class='caret'></span>";
		rv+="<ul class='dropdown-menu'>";
		rv+="<li><a href='#' onclick='badVideo("+ocp+");'>Bad Video</a></li>";
		rv+="<li><a href='#' onclick='dmca("+ocp+");'>Submit DMCA Notice</a></li>";
		if(authorized.isAdmin(su.getWebUser())) {
			rv+="<li><a href='#' onclick='editVideoTitle("+ocp+");'>Edit Title</a></li>";
		}
		rv+="</ul>";
		rv+="</div>";
		
		return rv;
	}
	

	
	private MyTitle prettyTitleWithShowDetail(OneMydetvChannel channel) {
		MyTitle myTitle = new MyTitle();
		myTitle.setChannel(channel);
		try {			
			// could be 'live'
			if(channel.getCurrentlyPlaying()==null) {
				OneJClient ojc = channel.getOjClient();
				JClient jClient = (JClient) ojc.getjClient();
				if(jClient==null) {
					myTitle.setActive(false);
					myTitle.setTitle("not active");
				}
				else if(jClient.isLive()) {
					myTitle.setLive(true);
					myTitle.setTitle("live from "+jClient.getBroadcaster().log());
				}
				return myTitle;
			}
			else {
				myTitle.setActive(true);
				myTitle.setLive(false);
				OneVideo ov = channel.getCurrentlyPlaying();
				myTitle.setOv(ov);
				log.info("prettyTitleWithShowDetail for "+ov.log());
				myTitle.setTitle(prettyTitle(ov));
			}
		} catch(Exception e) {
			log.error("Exception "+e+" "+e.getCause());
		}
		return myTitle;
	}
	
	class MyTitle {
		private OneVideo ov;
		private OneMydetvChannel channel;
		private String title;
		private boolean live;
		private boolean active;
		public OneVideo getOv() {
			return ov;
		}
		public void setOv(OneVideo ov) {
			this.ov = ov;
		}
		public OneMydetvChannel getChannel() {
			return channel;
		}
		public void setChannel(OneMydetvChannel channel) {
			this.channel = channel;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public boolean isLive() {
			return live;
		}
		public void setLive(boolean live) {
			this.live = live;
		}
		public boolean isActive() {
			return active;
		}
		public void setActive(boolean active) {
			this.active = active;
		}		
	}
}
