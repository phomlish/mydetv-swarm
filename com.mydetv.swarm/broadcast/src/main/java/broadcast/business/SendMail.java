package broadcast.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.Config;
import shared.data.webuser.OneWebUserEmail;
import shared.mail.SwarmMail;

import java.util.*;

/**
 * <b>Consistency is critical!</b>  Make sure all outgoing emails have the same look and feel.<br>
 * This will keep our users from becoming suspicious of our emails.<br>
 * @author phomlish
 *
 */
public class SendMail {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(SendMail.class);
		
	/**
	 * entry points:<br>
	 *     register<br>
	 *     addemail<br>
	 *     resendcode<br>
	 * @param config
	 * @param email
	 * @return
	 */
	public static void SendVerificationCodeEmail(Config config, OneWebUserEmail email, String vctype) {
		SwarmMail.SendEMail(
			config, 
			email.getEmail(),
			"My Delaware TV Email Verification Code for "+email.getEmail(),
			SendVerificationCodeHtmlBody(config, email, vctype),
			SendVerificationCodeTextBody(config, email, vctype));
	}

	public static void SendReVerificationWarning(Config config, OneWebUserEmail email, TreeMap<Integer, OneWebUserEmail> toEmails) {
		for(OneWebUserEmail toEmail : toEmails.values()){
			if(toEmail.getDtVerified()!=null)
				continue;
			SwarmMail.SendEMail(
				config, 
				toEmail.getEmail(),
				"My Delaware TV Security Warning for "+email.getEmail(),
				SendReVerificationWarningHtmlBody(config, email, toEmail),
				SendReVerificationWarningTextBody(config, email, toEmail));
		}
	}
	
	public static void SendDupReg(Config config, OneWebUserEmail email, TreeMap<Integer, OneWebUserEmail> toEmails) {
		for(OneWebUserEmail toEmail : toEmails.values()) {
			if(toEmail.getDtVerified()!=null)
				continue;
			SwarmMail.SendEMail(
				config, 
				toEmail.getEmail(),
				"My Delaware TV Security Warning for "+email.getEmail(),
				DupRegHtmlBody(config, email, toEmail),
				DupRegTextBody(config, email, toEmail));
		}
	}
	
	public static void SendForgotPasswordCode(Config config, OneWebUserEmail email, TreeMap<Integer, OneWebUserEmail> toEmails, String code, String uuid) {
		for(OneWebUserEmail toEmail : toEmails.values()) {
			//if(toEmail.getDtVerified()==null)
			//	continue;
			SwarmMail.SendEMail(
				config, 
				toEmail.getEmail(),
				"My Delaware TV Forgot Password for "+email.getEmail(),
				SendForgotPasswordCodeHtmlBody(config, email, toEmail, code, uuid),
				SendForgotPasswordCodeTextBody(config, email, toEmail, code, uuid));
		}
	}
	
	/**
	 * only send to verified emails
	 * @param config
	 * @param email
	 * @param toEmails
	 */
	public static void SendTooManyPasswordChanges(Config config, OneWebUserEmail email, TreeMap<Integer, OneWebUserEmail> toEmails) {
		for(OneWebUserEmail toEmail : toEmails.values()) {
			if(toEmail.getDtVerified()!=null)
				continue;
			@SuppressWarnings("unused")
			boolean sent = SwarmMail.SendEMail(
				config, 
				toEmail.getEmail(),
				"My Delaware TV Forgot Password for "+email.getEmail(),
				SendTooManyPasswordChangesHtmlBody(config, email, toEmail),
				SendTooManyPasswordChangesTextBody(config, email, toEmail));
		}
	}

	// **** the bodies ****
	private static String DupRegHtmlBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='"+Website(config)+"/s/images/buffalo.png'><br>\n";
		body+="<div>";
		body+="Someone has tied to register <b>"+email.getEmail()+"</b> with <a href='"+Website(config)+"'>My Delaware TV</a><br>";
		body+="You have already registered with that email address.<br>";
		body+="If you made that request in error please ignore this email.<br>";
		body+="</div><br>\n";
		
		body+=WarningHtml(config, email, toEmail);
		body+="</body>\n";
		body+="</html>\n";
		
		return body;
	}
	private static String DupRegTextBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body = "";
		body+="My Delaware TV from "+Website(config)+" Warning Message - Duplicate Registration\n\n";
		body+="Someone has tied to register "+email.getEmail()+"\n";
		body+="You have already registered with that email address.\n";
		body+="If you made that request in error please ignore this email.\n";
		body+=WarningText(config, email, toEmail);
		return body;
	}
	
	private static String SendReVerificationWarningHtmlBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='"+Website(config)+"/s/images/buffalo.png'><br>\n";
		body+="<div>";
		body+="Someone has tried to re-verify the email <b>"+email.getEmail()+"</b> with <a href='"+Website(config)+"'>My Delaware TV</a><br>";
		body+="If you accidentally attempted to re-verify that email please ignore this warning.\n";
		body+="</div><br>\n";
		
		body+=WarningHtml(config, email, toEmail);
		body+="</body>\n";
		body+="</html>\n";
		
		return body;
	}
	private static String SendReVerificationWarningTextBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body = "";
		body+="My Delaware TV from "+Website(config)+" Warning Message - email re-verification Attempted\n\n";
		body+="Someone has tried to re-verify the email "+email.getEmail()+"\n";
		body+="If you accidentally attempted to re-verify that email please ignore this warning\n";
		body+=WarningText(config, email, toEmail);
		return body;
	}
	
	// register, addemail, resendcode
	private static String SendVerificationCodeHtmlBody(Config config, OneWebUserEmail email, String vctype) {
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='"+Website(config)+"/s/images/buffalo.png'><br>\n";
		body+="<div>";
		if(vctype.equals("addemail"))
			body+="You have linked <b>"+email.getEmail()+"</b> to your account";
		else if(vctype.equals("resendcode"))
			body+="You have requested a new email verification code for <b>"+email.getEmail()+"</b>";
		else
			body+="You have registered <b>"+email.getEmail()+"</b>";
		
		body+=" at <a href='"+Website(config)+"'>My Delaware TV</a><br>";
		
		body+="<br>Please verify this email.<br>\n";
		body+="</div><br>\n";	
		
		String link = Website(config)+"/auth/verify-email";
		link+="?u="+email.getUuid();
		link+="&w="+email.getIdWebUser();
		link+="&e="+email.getIdEmail();
		link+="&c="+email.getVerificationCode();
		
		body+="<form method=\"post\" action=\""+link+"\">\n";
		body+="<input type=\"submit\" value=\"Verify Email\">\n";
		body +="</form>\n";
		
		body+=WarningHtml(config, email, email);
		body+="</body>\n";
		body+="</html>\n";
		return body;
	}
	
	private static String SendVerificationCodeTextBody(Config config, OneWebUserEmail email, String vctype) {
		
		String body = "";
		String link = Website(config)+"/auth/verify-email";
		link+="?u="+email.getUuid();
		link+="&w="+email.getIdWebUser();
		link+="&e="+email.getIdEmail();
		link+="&c="+email.getVerificationCode();
		
		body+="My Delaware TV from "+Website(config)+" Informational Message\n\n";
		if(vctype.equals("addemail"))
			body+="You have linked "+email.getEmail()+" to your account";
		else if(vctype.equals("resendcode"))
			body+="You have requested a new email verification code for "+email.getEmail();
		else
			body+="You have registered "+email.getEmail();
		
		body+=" at My Delaware TV\n\n";
		body+="Please go to "+link+" to verify your email.\n";
		
		body += WarningText(config, email, email);

		return body;
	}

	private static String SendForgotPasswordCodeHtmlBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail, String code, String uuid) {
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='"+Website(config)+"/s/images/buffalo.png'><br>\n";
		body+="<div>";
		body+="You have requested to reset your password for <b>"+email.getEmail()+"</b> with <a href='"+Website(config)+"'>My Delaware TV</a><br>";
		body+="<br>Please click here to reset your password.<br>";
		body+="</div><br>\n";	
		
		String link = Website(config)+"/auth/forgot-password";
		link+="?u="+uuid;
		link+="&w="+email.getIdWebUser();
		link+="&e="+email.getIdEmail();
		link+="&c="+code;
		
		body+="<form method=\"post\" action=\""+link+"\">\n";
		body+="<input type=\"submit\" value=\"Reset Password\">\n";
		body +="</form>\n";
		
		body+=WarningHtml(config, email, toEmail);
		body+="</body>\n";
		body+="</html>\n";
		return body;
	}
	
	private static String SendForgotPasswordCodeTextBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail, String code, String uuid) {
		
		String body = "";
		String link = Website(config)+"/auth/forgot-password";
		link+="?u="+uuid;
		link+="&w="+email.getIdWebUser();
		link+="&e="+email.getIdEmail();
		link+="&c="+code;
		body+="My Delaware TV from "+Website(config)+" Informational Message\n\n";
		body+="You have requested to change your password for "+email.getEmail()+" with My Delaware TV\n\n";
		body+="Please go to "+link+" to reset your password.\n";
		
		body += WarningText(config, email, toEmail);

		return body;
	}
	
	private static String SendTooManyPasswordChangesHtmlBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='"+Website(config)+"/s/images/buffalo.png'><br>\n";
		body+="<div>";
		body+="Someone has tried to request a forgoten password link for the email <b>"+email.getEmail()+"</b> with <a href='"+Website(config)+"'>My Delaware TV</a><br>";
		body+="The account password has been changed too recently.  Please try again later.\n";
		body+="</div><br>\n";
		
		body+=WarningHtml(config, email, toEmail);
		body+="</body>\n";
		body+="</html>\n";
		
		return body;
	}
	private static String SendTooManyPasswordChangesTextBody(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String body = "";
		body+="My Delaware TV from "+Website(config)+" Warning Message - too many password resets\n\n";
		body+="Someone has tried to request a forgoten password link for the email "+email.getEmail()+"\n";
		body+="The account password has been changed too recently.  Please try again later.\n";
		body+=WarningText(config, email, toEmail);
		return body;
	}

	

	private static String Website(Config config) {	
		return "https://"+config.getHostname()+":"+config.getWebPort();
	}
	private static String WarningText(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {
		String rv="";
		rv+="\n******************************************************************************************\n";
		rv+="Someone has attempted to use the email address "+email.getEmail()+"\n";
		rv+=" to access "+config.getCompany()+" at "+Website(config)+"\n";
		rv+=" from ip address "+email.getIpAddress()+".\n\n";
		rv+="You are receiving this message because you ("+toEmail.getEmail()+") have linked "+email.getEmail()+" with your account.\n\n";
		rv+="If you did not make this request and/or believe your account or email has been compromised\n";
		rv+=" please contact ";
		rv +="mailto:support@mydetv.com?subject=Compromised-Account-"+email.getEmail();
		rv+="\n******************************************************************************************\n";
		return rv;
	}
	private static String WarningHtml(Config config, OneWebUserEmail email, OneWebUserEmail toEmail) {		
		String mailto = "mailto:support@mydetv.com?subject=Compromised Account";
		mailto+="&Body=%0AI believe my account using "+email.getEmail()+" was compromised.%0A%0A";
		
		String rv="";
		
		rv+="<br><br><div style=\"text-align:center;\">";
		rv+="<div style=\"text-align:left; display: inline-block\"><small>";
		rv+="Someone has attempted to use "+email.getEmail()+" email address  to access "+config.getCompany()+"<br>";
		rv+="at "+Website(config)+" from ip address "+email.getIpAddress()+"<br><br>";
		rv+="You are receiving this message because you ("+toEmail.getEmail()+") have linked "+email.getEmail()+" with your account.<br>";
		rv+="If you did not make this request and/or believe your account has been compromised<br>";
		rv+="please email: <a href=\""+mailto+"\">support@mydetv.com</a><br>";
		rv+="</small></div></div>\n";
		return rv;
	}
	
	public static void SendTestEmails(Config config) {
		OneWebUserEmail email = new OneWebUserEmail();
		email.setEmail("phomlish@homlish.net");
		email.setVerificationCode("abcdefgh");
		email.setUuid("ABCD-EF");
		email.setIdWebUser(5);
		email.setIdEmail(3);
		email.setIpAddress("8.8.8.8");
		TreeMap<Integer, OneWebUserEmail> toEmails = new TreeMap<Integer, OneWebUserEmail>();
		toEmails.put(email.getIdEmail(), email);
		SendVerificationCodeEmail(config, email, "addemail");
		SendReVerificationWarning(config, email,toEmails);
		SendDupReg(config, email,toEmails);
		SendForgotPasswordCode(config, email, toEmails, "xyz", "DEFG-ABCD");
		SendTooManyPasswordChanges(config, email, toEmails);
	}
}
