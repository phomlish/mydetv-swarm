package broadcast.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.WebUserRoleTypesEnum;
import shared.data.webuser.WebUserRoleTypesEnum.WebUserRoleType;

/*
 * check the webroles to see if the sessionUser is authorized
 */
public class Authorized {

	@Inject private Blackboard blackboard;
	private static final Logger log = LoggerFactory.getLogger(Authorized.class);

	public Boolean canEditRole(OneWebUser euser, OneWebUser tuser,  WebUserRoleType role) {
		if(euser==null)
			return false;
		if(euser.getWuid().equals(tuser.getWuid()))
			return false;
		if(euser.isAdmin())
			return true;
		if(tuser.isAdmin() && ! euser.isAdmin())
			return false;
		
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
			return isAdmin(euser);
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.CHAT.getLevel()))
			return isChatAdmin(euser);
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel()))
			return isPeopleAdmin(euser);
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.VIDEOS.getLevel()))
			return isVideoAdmin(euser);
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel()))
			return isScheduleAdmin(euser);
		if(role.getLevel().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()))
			log.error("isAuthorized does not work for broadcasters, use isBroadcaster");
		return false;
	}
	public Boolean canEditRoleBroadcast(OneWebUser euser, OneWebUser tuser, Integer channel) {
		if(euser==null)
			return false;
		if(euser.getWuid().equals(tuser.getWuid()))
			return false;
		if(euser.isAdmin())
			return true;
		if(tuser.isAdmin() && ! euser.isAdmin())
			return false;
		if(! blackboard.getMydetvChannels().containsKey(channel))
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()) 
					&& oneWebUserRole.getDetails().equals(channel.toString()))
				return true;
		}
		return false;
		
	}
	public Boolean isAuthorized(SessionUser su, Integer role) {
		return(isAuthorized(su.getWebUser(), role));
	}
	public Boolean isAuthorized(OneWebUser euser, Integer role) {
		if(euser==null)
			return false;
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
			return isAdmin(euser);
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.CHAT.getLevel()))
			return isChatAdmin(euser);
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel()))
			return isPeopleAdmin(euser);
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.VIDEOS.getLevel()))
			return isVideoAdmin(euser);
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel()))
			return isScheduleAdmin(euser);
		if(role.equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()))
			log.error("isAuthorized does not work for broadcasters, use isBroadcaster");
		return false;
	}
	
	public boolean isAdmin(OneWebUser owu) {
		if(owu==null)
			return false;
		for(OneWebUserRole oneWebUserRole : owu.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
		}
		return false;
	}
	public boolean hasAnyRole(OneWebUser owu) {
		if(owu==null)
			return false;
		if(owu.getRoles().size()>0)
			return true;
		return false;
	}
	/**
	 * is he is authorized to broadcast for any channel
	 * @param sessionUser
	 * @return
	 */
	public boolean isBroadcaster(OneWebUser euser) {
		if(euser==null)
			return false;
		if(isAdmin(euser))
			return true;
		
		for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values()) {
			Integer mydetvChannelId = mydetvChannel.getChannelId();

			for(OneWebUserRole oneWebUserRole : euser.getRoles().values())
				if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()) 
				&& oneWebUserRole.getDetails().equals(mydetvChannelId.toString()))
					return true;
	
		}
		return false;
	}

	/**
	 * is he authorized to broadcast for this channel
	 * @param sessionUser
	 * @param mydetvChannelId
	 * @return
	 */
	public boolean isBroadcaster(OneWebUser euser, Integer mydetvChannelId) {
		if(euser==null)
			return false;
		if(! blackboard.getMydetvChannels().containsKey(mydetvChannelId))
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.BROADCASTER.getLevel()) 
					&& oneWebUserRole.getDetails().equals(mydetvChannelId.toString()))
				return true;
		}
		return false;
	}

	public boolean isPeopleAdmin(OneWebUser euser) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel()))
				return true;
			
		}
		return false;
	}
	public boolean isChatAdmin(OneWebUser euser) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.CHAT.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.PEOPLE.getLevel()))
				return true;
			
		}
		return false;
	}
	public boolean isVideoAdmin(OneWebUser euser) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.VIDEOS.getLevel()))
				return true;
			
		}
		return false;
	}
	public boolean isScheduleAdmin(OneWebUser euser) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel()))
				return true;
			
		}
		return false;
	}

	public boolean isScheduler(OneWebUser euser, Integer channelId) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			Integer details=-1;
			details = Integer.decode(oneWebUserRole.getDetails());
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel())
			&& channelId==details)
				return true;
			
		}
		return false;
	}

	public boolean isScheduler(OneWebUser euser, String channelId) {
		if(euser==null)
			return false;
		for(OneWebUserRole oneWebUserRole : euser.getRoles().values()) {
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.ADMIN.getLevel()))
				return true;
			if(oneWebUserRole.getWebUserRoleType().equals(WebUserRoleTypesEnum.WebUserRoleType.SCHEDULE.getLevel())
			&& channelId.equals(oneWebUserRole.getDetails()))
				return true;
			
		}
		return false;
	}
}
