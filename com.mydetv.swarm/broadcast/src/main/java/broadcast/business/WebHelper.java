package broadcast.business;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.Config;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

public class WebHelper {

	@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private Authorized authorized;
	@Inject private Config sharedConfig;
	@Inject private UserMessageHelpers userMessageHelpers;
	private static final Logger log = LoggerFactory.getLogger(WebHelper.class);

	public static String PrettyPrint(String message) {
		String rv="";
		try {
			JSONObject jo = new JSONObject(message);
			rv = jo.toString();
		} catch(Exception e) {
			log.error("Exception:"+e);
		}
		return rv;
	}

	/**
	 * adds <html><body><navbar></navbar><div mainContainer>
	 */
	public String beginHtml(SessionUser sessionUser, String title) {
		return(beginHtml(sessionUser, title, "", ""));
	}
	
	public String beginHtml(SessionUser sessionUser, String title, String page) {
		return(beginHtml(sessionUser, title, page, ""));
	}
	/**
	 * adds <html><body><navbar></navbar><div mainContainer>
	 */
	public String beginHtml(SessionUser sessionUser, String title,  String page, String extraHeaders) {
		String rv = makeHeaderBeginning(sessionUser);
		rv+=extraHeaders;
		rv+="<title>"+config.getCompany()+" "+title+"</title>\n";
		rv+="</head>\n";
		rv+="<body id='bootstrap-overrides' class='mt-5 pt-5'>\n";
		rv+=navbar(sessionUser, page);
		rv+="<div id='mainContainer' class='container-fluid center navbarShown'>\n";
		return rv;
	}
	
	private String makeHeaderBeginning(SessionUser sessionUser) {
		String nocache = "";
		if(sharedConfig.getInstance().equals("dev"))
			nocache="?"+System.currentTimeMillis();
		String rv="<!DOCTYPE html>\n";
		rv+="<html>\n";
		rv+="<head>\n";
		rv+="<meta charset=\"utf-8\">\n";
		rv+="<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n";

		rv+="<link rel='shortcut icon' href='/s/icons/mydetvGreen/favicon.ico' type='image/ico' />\n";
		rv+="<link rel='shortcut icon' href='/s/icons/mydetvGreen/favicon-32x32.png' type='image/png' />\n";
		rv+="<link rel='shortcut icon' href='/s/icons/mydetvGreen/favicon-16x16.png' type='image/png' />\n";
		
		rv+="<link rel='stylesheet' href='/s/node_modules/fontawesome-free/css/all.min.css'>\n";		

		rv+="<link rel='stylesheet' href='/s/node_modules/ekko-lightbox/dist/ekko-lightbox.css'>\n";
		if(sharedConfig.getInstance().equals("dev"))
			rv+="<link rel='stylesheet' href='/s/node_modules/bootstrap/dist/css/bootstrap.css'>\n";
		else
			rv+="<link rel='stylesheet' href='/s/node_modules/bootstrap/dist/css/bootstrap.min.css'>\n";
		
		rv+="<link rel='stylesheet' href='/s/node_modules/jquery-ui/themes/base/all.css'>\n";
		
		rv+="<script src='/s/node_modules/jquery/dist/jquery.min.js'></script>\n";
		rv+="<script src='/s/node_modules/jquery-ui/external/jquery/jquery.js'></script>\n";
		rv+="<script src='/s/node_modules/bootstrap/dist/js/bootstrap.min.js'></script>\n";
		rv+="<script src='/s/node_modules/js-cookie/src/js.cookie.js'></script>\n";
		rv+="<script src='/s/node_modules/ekko-lightbox/dist/ekko-lightbox.min.js'></script>\n";
		rv+="<script src='/s/node_modules/webrtc-adapter/out/adapter.js'></script>\n";
		
		rv+="<script src='/s/node_modules/detectrtc/DetectRTC.min.js'></script>\n";
		rv+="<script src='/s/node_modules/bootbox/bootbox.js'></script>\n";
				
		// tool tips tippy
		rv+="<script src='/s/node_modules/popper.js/dist/umd/popper.min.js'></script>\n";
		//rv+="<script src='/s/popper.js-1.14.3/dist/umd/popper.min.js'></script>\n";
		rv+="<script src='/s/tooltip-2.0.1/dist/tooltip.min.js'></script>\n";
		rv+="<link rel='stylesheet' href='/s/tooltip-2.0.1/dist/tooltip.css'>\n";
		//rv+="<link rel='stylesheet' href='/s/node_modules/tippy.js/dist/tippy.css'>\n";
		//rv+="<script src='/s/node_modules/tippy.js/dist/'></script>\n";
		
		if(sessionUser!=null && sessionUser.getTheme()!=null && sessionUser.getTheme().equals("crisp")) {
			rv+="<link rel='stylesheet' href='/s/css/swarm-crisp.css"+nocache+"'>\n";
		}
		else {
			rv+="<link rel='stylesheet' href='/s/css/swarm-default.css"+nocache+"'>\n";
		}
		rv+="<link rel='stylesheet' href='/s/css/swarm-common.css"+nocache+"'>\n";
		
		rv+="<script src='/s/js/swarm.js"+nocache+"'></script>\n";
		rv+="<script src='/s/js/swarm-login.js"+nocache+"'></script>\n";
		return rv;
	}

	public String closeMainContainerAddFooterCloseBodyCloseHtml() {
		String rv="";
		
		rv+="</div>\n";  // mainContainer
		
		rv+="<div class='footer'>";
		rv+="<footer class='py-5'>";
		rv+="<div class='span'>";
		rv+="<div class='text-center small'>&copy; 2017, 2018, 2019 ";
		rv+=config.getCompany();
		rv+="</div>"; //copyright
		rv+="</div>\n"; // span
		rv+="</footer>\n";
		rv+="</div>"; // div footer
		
		rv+="</body>\n";
		rv+="</html>\n";
		
		return rv;
	}

	public OneMydetvChannel getChannelFromString(String strChannel) {
		Integer intChannel = Integer.decode(strChannel);
		if(blackboard.getMydetvChannels().containsKey(intChannel))
			return blackboard.getMydetvChannels().get(intChannel);
		return null;
	}
	
	public String navbar(SessionUser sessionUser, String page) {
		String nb="";
		nb+="<nav id='mainNavbar' class='navbar navbar-expand-lg fixed-top mbpLightBg'>\n";
		
		//nb+="<div class='collapse navbar-collapse id='exCollapsingNavbar2'>";
		nb+=getButtons(sessionUser, page);
		//nb+=getButtonsTest();
		//nb+="</div>";
		nb+="</nav>\n";
		nb+="<span class='center fixed-top chevronSpanOffset' id='chevronSpan'>";
		nb+="<div id='mychevronDown' onclick='toggleChevron()' class='fa fa-chevron-down center d-none'></div>";
		nb+="<div id='mychevronUp'   onclick='toggleChevron()' class='fa fa-chevron-up center'></div>";
		nb+="</span>\n";
		
		return nb;
	}

	private String getButtons(SessionUser sessionUser, String page) {
		String active = "";
		String navbar="";
		navbar+= "<ul class='navbar-nav mr-auto'>\n";

		if(page.equals("home")) active="active";
		else active="";
		navbar+= "<li class='nav-item nav-button "+active+"'>";
		navbar+="<a class='nav-link' href='/'>Home</a>";
		navbar+="</li>\n";

		// about
	    navbar+="<li class='nav-button nav-item dropdown'>";
	    navbar+="<a class='nav-link dropdown-toggle' href='#' id='navbarAboutDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
	    navbar+="About";
	    navbar+="</a>";
	    navbar+="<div class='dropdown-menu' aria-labelledby='navbarAboutDropdown'>";
	    navbar+="<a class='dropdown-item' href='https://www.madbuffaloproductions.com' target='_blank' title='opens in new tab'>Mad Buffalo Productions</a>";
	    navbar+="<a class='dropdown-item' href='https://www.mydetv.com' target='_blank' title='opens in new tab'>My Delaware TV</a>";
	    if(authorized.isAdmin(sessionUser.getWebUser()))
	    	navbar+="<a class='dropdown-item' href='/about' target='_blank' title='opens in new tab'>Broadcast</a>";
	    navbar+="</li>";
		
		// schedule
		navbar+= "<li class='nav-button nav-item'>";
		navbar+="<a class='nav-link' href='/schedule' target='_blank' title='opens in new tab'>Schedule</a>";
		navbar+="</li>\n"; 
			
		// contact	
	    navbar+="<li class='nav-button nav-item dropdown'>";
	    navbar+="<a class='nav-link dropdown-toggle' href='#' id='navbarContactDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
	    navbar+="Contact";
	    navbar+="</a>";
	    navbar+="<div class='dropdown-menu' aria-labelledby='navbarContactDropdown'>";
	    navbar+="<a class='dropdown-item' href='https://blog.madbuffaloproductions.com' target='_blank' title='opens in new tab'>Blog - Mad Buffalo Productions</a>";
	    navbar+="<a class='dropdown-item' href='https://groups.google.com/forum/#!forum/mad-buffalo-productions' target='_blank' title='opens in new tab'>Mailing Mist - Mad Buffalo Productions</a>";
	    navbar+="<a class='dropdown-item' href='https://groups.google.com/forum/#!forum/mydetv-swarm' target='_blank' title='opens in new tab'>Mailing List - My Delaware TV</a>";
	    navbar+="</li>";

		// broadcast
		if(authorized.isBroadcaster(sessionUser.getWebUser())) {
		    navbar+="<li class='nav-button nav-item dropdown'>";
		    navbar+="<a class='nav-link dropdown-toggle' href='#' id='navbarBroadcastDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
		    navbar+="Broadcast";
		    navbar+="</a>";
		    navbar+="<div class='dropdown-menu' aria-labelledby='navbarBroadcastDropdown'>";
		    for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values())
		    	if(authorized.isBroadcaster(sessionUser.getWebUser(), mydetvChannel.getChannelId()))
		    		navbar+="<a class='dropdown-item' href='/broadcast?channel="+mydetvChannel.getChannelId()+"'>"+mydetvChannel.getName()+"</a>";
		    navbar+="</li>";
		}

		// admin
		if(sessionUser.getWebUser()!=null && sessionUser.getWebUser().getRoles().size()>0) {
		    navbar+="<li class='nav-button nav-item dropdown'>";
		    navbar+="<a class='nav-link dropdown-toggle' href='#' id='navbarAdminDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
		    navbar+="Admin";
		    navbar+="</a>";
		    navbar+="<div class='dropdown-menu' aria-labelledby='navbarAdminDropdown'>";
		    if(authorized.isAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='https://www.mydetv.com/janus/admin.html' target='_blank'>JAdmin</a>";
		    if(authorized.isAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/maint' target='_blank'>Maintenence</a>";
		    if(authorized.isVideoAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/mbps' target='_blank'>Mbps</a>";		    
		    if(authorized.isPeopleAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/people' target='_blank'>People</a>";
		    if(authorized.isScheduleAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/schedules' target='_blank'>Schedule</a>";
		    if(authorized.isAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/simulate' target='_blank'>Simulate</a>";
		    if(authorized.hasAnyRole(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/status' target='_blank'>Status</a>";
		    if(authorized.isAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/vc' target='_blank'>Video Categories</a>";
		    if(authorized.isVideoAdmin(sessionUser.getWebUser()))
		    	navbar+="<a class='dropdown-item' href='/admin/videos' target='_blank'>Videos</a>";
		    
		    navbar+="</li>";
		}

		// they are viewing a video page
		if(page.toLowerCase().equals("p2p") || page.toLowerCase().equals("broadcast")) {
			navbar+="<li class='nav-button nav-item dropdown'>";
		    navbar+="<a class='nav-link dropdown-toggle' href='#' id='navbarFeedbackDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
		    navbar+="Feedback";
		    navbar+="</a>";
		    navbar+="<div class='dropdown-menu' aria-labelledby='navbarFeedbackDropdown'>";
		    navbar+="<a class='dropdown-item' href='#' onclick='submitBug()'>Submit a Bug</a>";
		    navbar+="<a class='dropdown-item' href='#' onclick='submitBadVideo()'>Submit Bad Video</a>";
		    navbar+="</li>";
		    
		    navbar+= userMessageHelpers.initializeUserMessage();
		}

		navbar+="</ul>\n";  // navbar-nav ul
		
		navbar+="<ul class='navbar-nav ml-auto'>";
		if(! sessionUser.isAuthenticated()) {
				    
			//navbar+="<li class='nav-item'>";
			//navbar+="<video id='profileVideo' class='hide' autoplay ondblclick='makeFullScreen(this)'></video>\n";
			//navbar+="</li>";
			
			navbar+="<li class='nav-item dropdown'>";
			navbar+="<a class='nav-link' href='#'>";
			
			navbar+="<img id='profilePicture' class='profilePicture' title='please login/register'";
			navbar+=" src='/s/images/buffalo.png'>";
			navbar+="</a>";
			navbar+="</li>";
			
			navbar+="<li class='nav-item'>";
			navbar+="<a class='nav-link'>Guest</a>";
			navbar+="</li>";
		}
		else {
		    
			navbar+="<li class='nav-item'>";
			navbar+="<video id='profileVideo' class='hide' autoplay ondblclick='makeFullScreen(this)'></video>\n";
			navbar+="</li>";
			
			navbar+="<li class='nav-item dropdown'>";
			navbar+="<a class='nav-link dropdown-toggle profilePictureDropdown' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
			navbar+="<img id='profilePicture' class='profilePicture' title='click to edit/logout'";
			if(sessionUser.getWebUser().getWebUserSelectedImage().equals(-1))
				navbar+=" src='/s/images/buffalo.png'";
			else {
				//byte[] selectedImage = sessionUser.getWebUser().getImages().get(sessionUser.getWebUser().getWebUserSelectedImage()).getOrigFile();				
				//navbar+=" src='data:image/jpg;base64, "+Base64.getEncoder().encodeToString(selectedImage)+"'";
				String selectedImage = sessionUser.getWebUser().getImages().get(sessionUser.getWebUser().getWebUserSelectedImage()).getNewFn();
				String url = "/ext/people/"+sessionUser.getWebUser().getWuid()+"/"+selectedImage;			
				navbar+=" src='"+url+"'";
			}
				
			navbar+=">\n";	
			navbar+="</a>";
			navbar+="<div class='dropdown-menu' aria-labelledby='navbarDropdown'>";
			navbar+="<a class='dropdown-item' href='/auth/profile' target='_blank'>Edit Profile</a>";
			navbar+="<a class='dropdown-item' href='/auth/security' target='_blank'>Edit Security</a>";
			navbar+="<a class='dropdown-item' href='#' onclick='logout();'>Logout</a>";
			navbar+="</div>";
			navbar+="</li>";
			
			navbar+="<li class='nav-item'>";
			navbar+="<a class='nav-link'>"+sessionUser.log()+"</a>";
			navbar+="</li>";
		}
		navbar+="</ul>\n";
		return navbar;

	}
	
	public String GetSlickgridHeaders() {
		String eh="";
		//eh+="<script src='/s/js/jquery.event.drag-2.3.0.js'></script>\n";
		//eh+="<script src='/s/js/jquery.event.drop-2.3.0.js'></script>\n";

		//eh+="<link rel='stylesheet' href='/s/node_modules/jquery-ui/themes/base/sortable.css'>\n";
		
		eh+="<script src='/s/node_modules/slickgrid/lib/jquery.event.drag-2.3.0.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/lib/jquery.event.drop-2.3.0.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/lib/jquery-ui-1.11.3.js'></script>\n";
		
	
		eh+="<link rel='stylesheet' href='/s/node_modules/slickgrid/slick.grid.css'>\n";
		eh+="<link rel='stylesheet' href='/s/node_modules/slickgrid/css/select2.css'>\n";
				
		eh+="<script src='/s/node_modules/slickgrid/slick.core.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/plugins/slick.cellrangedecorator.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/plugins/slick.cellrangeselector.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/plugins/slick.cellselectionmodel.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/slick.formatters.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/slick.editors.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/slick.grid.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/lib/select2.js'></script>\n";
		eh+="<script src='/s/node_modules/slickgrid/slick.dataview.js'></script>\n";
		return eh;
	}
	
	public String chatRow() {
		String rv="";
		rv+="<div class='row chatrow'>"; // send chat row
		rv+="<div class='col-sm-1' id='sendChatButtonColumn'>\n";
		rv+="<button id='send-btn' class='button-chat' onclick='sendChat();'>Send</button>\n";
		rv+="</div>"; // sendChatButtonColumn
		
		rv+="<div class='col-sm-10' id='sendChatColumn'>\n";	
		rv+="<div class='' id=''>";				
		rv+="<input type='text' id='chatMessage' autocomplete='off' />";
		rv+="</div>\n"; // sendChatColumn
		
		rv+="</div>\n"; // send chat row
		return rv;
	}
	
	public String chatRowOld() {
		String rv="";
		rv+="<div class='row chatrow'>"; // send chat row	
		rv+="<div class='col-sm-1' id='sendChatButtonColumn'>\n";
		rv+="<button id='send-btn' class='button-chat' onclick='sendChat();'>Send</button>\n";
		rv+="</div>"; // sendChatButtonColumn
		
		rv+="<div class='col-sm-10' id='sendChatColumn'>\n";	
		rv+="<div class='emoji-picker-container' id='emoji-picker-container'>";				
		rv+="<input type='text' name='chatMessage' id='chatMessage' placeholder='Message'";
		rv+="    maxlength='255' data-emojiable='true' data-emoji-input='unicode'/>";
		rv+="</div>\n"; //  <!-- emoji-picker-container -->";	
		rv+="</div>\n"; // sendChatColumn
		
		rv+="</div>\n"; // send chat row
		return rv;
	}

	public String getTestRow() {
		String rv = "";
		rv+="<div class='row'>";
		//rv+="<div class='col-sm-1' id='testblue' style='background-color:blue'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:yellow'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:black'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:aqua'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:BlueViolet'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:coral'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:red'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:darkgrey'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:darkorange'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:deeppink'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:forestgreen'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:ivory'>test</div>";
		rv+="<div class='col-sm-1' style='background-color:lime'>test</div>";
		rv+="</div>\n"; // row
		return rv;
	}
	
	public static String UsernameRules="Must be at least 3 characters.\nNames deemed offensive by Davey Poland will have the account banned.\nCan't be your email.";
	public static String RegisterHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showRegisterHelp();'></i>";
		rv+="<div id='registerHelp' hidden>";
		rv+="<div>";
		rv+="An email will be sent to the address you supply with a link to verify your email. ";
		rv+="You will not be able to fully access the site until you have clicked the link. ";
		rv+="Your email will never be shared with anyone ever.<br>";
		rv+="<b class='redText'>Unverified accounts will be deleted after 4 hours.</b><br>";
		rv+="<br>";
		
		rv+=PasswordRules+"<br>";
		rv+="<br>";
		
		rv+="Username: ";
		String[] lines = UsernameRules.split("\n");
		for(String line : lines) 
			rv+=line+" ";
		rv+="<br>";
		
		rv+="Sequrity Questions: ";
		rv+=SQHelp;
		
		rv+="</div>";
		
		rv+="</div>";
		return rv;
	}
	
	public static String PasswordRules="Your password must be strong as determined by a complex secret algorithm which was writen in an altered state by Todd Dedmon while listening to Jerry Garcia.";
	public static String PasswordHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showPasswordHelp();'></i>";
		rv+="<div id='passwordHelp' hidden>";
		rv+="<div>";
		rv+="You can only change your password once a day.<br>";
		rv+="You cannot use any recently used passwords.<br>";
		rv+=PasswordRules+"<br>";
		rv+="</div>";
		rv+="</div>";
		return rv;
	}
	
	public static String ForgotPasswordHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showForgotPasswordHelp();'></i>";
		rv+="<div id='forgotPasswordHelp' hidden>";
		rv+="You will be sent an authorization code via email.<br>";
		rv+="Using the authorization code, you will be asked to supply your security questions.<br>";
		rv+="When all steps are accepted you will be able to change your password.<br>";
		rv+="<br>";
		rv+="You will not be able to re-use recently used passwords.<br>";
		rv+="Multiple failed attempts to supply your security questions will lock your account.<br>";
		rv+="</div>";
		return rv;
	}
	
	public static String ProfileHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showProfileHelp();'></i>";
		rv+="<div id='profileHelp' hidden>";
		rv+="You must be over 18 to view resricted conent.<br>";			
		rv+="Publish your home page so other members can see visit your corner of the internet.<br>";
		rv+="Set your timzone so mssages will have the correct time stamp.<br>";
		rv+="</div>";
		return rv;
	}
	public static String ImagesHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showImagesHelp();'></i>";
		rv+="<div class='redText'><b>Images that break the rules could get your account banned.</b></div>";
		rv+="<div id='imagesHelp' hidden>";
		rv+="You will be able to crop and rotate the image.<br>";
		rv+="A maximum of 10 images can be saved.<br>";
		rv+="<br>";
		rv+="<b>Image Requirements:</b><br>";
		rv+="Must be an image of your face and have no other faces in the image.<br>";
		rv+="Must be 2 meg or less.<br>";
		rv+="Must be a png, jpg or gif.<br>";
		rv+="Must not be offensive as judged by Annie Johnson<br>";
		rv+="<br>";
		rv+="<b class='redText'>Images that break the rules could get your account banned.</b><br>";	
		rv+="</div>";
		return rv;
	}
	public static String EmailHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showEmailHelp();'></i>";
		rv+="<div id='emailHelp' hidden>";
		rv+="You can link up to 3 emails to your account.<br>";
		rv+="New email addresses will have a verification code emailed to them.<br>";
		rv+="Verification codes must be validated before the email address can be used to access the site.<br>";
		rv+="Verification codes are only valid for 15 minutes<br>";
		rv+="You can only delete an email if you have at least two verified emails.<br>";
		rv+="<b class='redText'>Unverified accounts will be deleted after 4 hours.</b><br>";
		rv+="</div>";
		return rv;
	}
	public static String SQHelp = ""
			+"You must remember these.<br>"
			+"You will use them if your account becomes locked or if you forget your password.<br>"
			+"Don't use answers that change.  If your favorite food often changes perhaps ask yourself what was my childhood favorite food.<br>"
			+"Spelling matters, case does not.<br>"
			+"Apple does not equal Apples.<br>"
			+"Apple does equal apple.<br>"
			+"Can't contain your email, username, or password.<br>";
	
	public static String SqHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showSqHelp();'></i>";
		rv+="<div id='sqHelp' hidden>";
		rv+=SQHelp;	
		rv+="</div>";
		return rv;
	}
	public static String DeleteAccountHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showDeleteAccountHelp();'></i>";
		rv+="<div id='deleteAccountHelp' hidden>";
		rv+="Your account will be permanently deleted.<br>";	
		rv+="This action is not reversable.<br>";			
		rv+="All personally identifiable information will be deleted from our databases.<br>";		
		rv+="</div>";
		return rv;
	}
	public static String ChatHelp() {
		String rv="";
		rv+="<i class='fas fa-question-circle' onclick='showChatHelp();'></i>";
		rv+="<div id='chatHelp' hidden>";
		rv+="click on a person's image for options.<br><br>";
		rv+="<b>private chat</b>: only people selected will receive the chat.<br>";
		rv+="<b>block/unblock</b>: you will not receive this person's chats. ";
		rv+="They won't be able to request you to share your webcam or initiate a private chat with you. ";
		rv+="This setting is saved in your profile.<br>";
		rv+="<b>private video</b>: the other person will be asked if they wish to share their webcam with you.<br>";
		rv+="<b>report</b>: report offensive behaviour to the moderators. Moderators can restrict, mute, or permanently ban offensive people.  False reports could get you banned.<br>";
		rv+="</div>\n";
		return rv;
	}
}
