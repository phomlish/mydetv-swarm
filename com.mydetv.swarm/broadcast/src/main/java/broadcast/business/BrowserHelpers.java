package broadcast.business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shared.data.broadcast.SessionUser;

public class BrowserHelpers {
	
	private static final Logger log = LoggerFactory.getLogger(BrowserHelpers.class);

	public static void AnalyzeDetectRTC(SessionUser su) {

		su.setGoodWebrtc(false);
		su.setGoodBroadcaster(false);
		
		if(su.getDetectRTC()==null) return;
		JSONObject jo = su.getDetectRTC();
		
		if(jo.isNull("isWebRTCSupported")) return;
		if(! jo.getBoolean("isWebRTCSupported")) return;
		
		if(jo.isNull("isMobileDevice")) return;
		if(jo.getBoolean("isMobileDevice")) return;
		
		if(jo.isNull("isSctpDataChannelsSupported")) return;
		if(! jo.getBoolean("isSctpDataChannelsSupported")) return;
				
		if(jo.isNull("browser")) return;
		JSONObject browser = jo.getJSONObject("browser");
		if(browser.isNull("name")) return;
		String name = browser.getString("name").toLowerCase();
		if(! name.contains("firefox") 
		&& !name.contains("chrome")) return;

		// seems to be a good webrtc sender/receiver
		su.setGoodWebrtc(true);
		
		if(jo.isNull("hasWebcam")) return;
		if(! jo.getBoolean("hasWebcam")) return;
		
		if(jo.isNull("isGetUserMediaSupported")) return;
		if(! jo.getBoolean("isGetUserMediaSupported")) return;
		
		if(jo.isNull("isCreateMediaStreamSourceSupported")) return;
		if(! jo.getBoolean("isCreateMediaStreamSourceSupported")) return;

		if(jo.isNull("audioInputDevices")) return;
		if(jo.isNull("videoInputDevices")) return;
		
		JSONArray ja;
		ja=jo.getJSONArray("audioInputDevices");
		if(ja.length()==0)  return;
		ja=jo.getJSONArray("videoInputDevices");
		if(ja.length()==0)  return;
		
		// seems to be a good broadcaster
		su.setGoodBroadcaster(true);		
	}
	
	public static Boolean isBrowserTypeOK(String userAgent) {
		if(! userAgent.toLowerCase().contains("firefox") && ! userAgent.toLowerCase().contains("chrome"))
			return false;
		return true;
	}
	//Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:64.0) Gecko/20100101 Firefox/64.0
	//Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36
	public static Boolean isBrowserCurrent(String userAgent) {
		if(! userAgent.toLowerCase().contains("firefox") && ! userAgent.toLowerCase().contains("chrome"))
			return false;
		
		String regexString=null;
		Integer minVersion=-1;
		if(userAgent.toLowerCase().contains("firefox")) {
			regexString = "(.*?)( Firefox/)(.*?)(\\.)";
			minVersion=64;
		}
		else if(userAgent.toLowerCase().contains("chrome")) {
			regexString = "(.*?)( Chrome/)(.*?)(\\.)";
			minVersion=71;
		}

	    Pattern pattern = Pattern.compile(regexString,Pattern.CASE_INSENSITIVE);
	   	Matcher matcher = pattern.matcher(userAgent);
	    String strVersion=null;
	   	while (matcher.find()) {
	    	//log.info(matcher.group(3));
	   		strVersion=matcher.group(3);
	    }
	    if(strVersion!=null) {
	   		Integer version = getInt(strVersion);
	   		if(version>=minVersion)
	   			return true;
	    }
		
		return false;
	}
	
	private static Integer getInt(String str) {
		try {
		return Integer.decode(str);
		} catch(NumberFormatException e) {
			log.error("error geting version from "+str+" "+e);
			return -1;
		}
	}
	
}
