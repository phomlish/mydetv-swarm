package broadcast.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.web.ws.WsWriter;
import shared.data.OneChatBlock;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebChat;
import shared.db.DbChats;

public class ChatHelpers {

	@Inject private Blackboard blackboard;
	@Inject private DbChats dbChats;
	@Inject private WsWriter websocketWriter;
	@Inject private PeopleHelpers peopleHelpers;
	@Inject private UserMessageHelpers userMessageHelpers;
	@Inject private Authorized authorized;
	private static final Logger log = LoggerFactory.getLogger(ChatHelpers.class);
	
	// this is the raw chat coming from a websocket 
	public void wsSendChat(JSONObject response, SessionUser sessionUser) throws IOException {
		
		// are they muted?
		if(sessionUser.getWebUser().getDtMuted()!=null) {
			userMessageHelpers.sendUserMessage(sessionUser, "error", "you have been mutted by an admin");
			return;
		}
		
		String theMessage = response.getString("theMessage");		
		JSONArray people = response.getJSONArray("people");
		log.info("people:"+people.toString());
		log.info("sending chat "+theMessage);
		LocalDateTime now = LocalDateTime.now();
		Boolean isPrivate=false;
		if(people.length()>0) isPrivate=true;
		
		for(SessionUser su : sessionUser.getMydetvChannel().getSessionUsers().values()) {
			if(su.getWebUser().getDtMuted()!=null && ! authorized.isPeopleAdmin(sessionUser.getWebUser())) {
				log.info("skip muted");
				continue;
			}
			if(peopleHelpers.isBlocked(su, sessionUser)) {
				log.info("skip blocked");
				continue;
			}
			if(isPrivate) {
				String person="person-"+su.getWebUser().getWuid()+"-"+su.getWebUser().getDbChannelWebUsersId();
				String me = "person-"+sessionUser.getWebUser().getWuid()+"-"+sessionUser.getWebUser().getDbChannelWebUsersId();
				if(doesArrayContain(person, people) || person.equals(me))
					chatFromUserToUser(theMessage, sessionUser, su, now, true);
				dbChats.sendChat(sessionUser, theMessage, now, isPrivate);
				continue;
				}

			chatFromUserToUser(theMessage, sessionUser, su, now, false);
		}
		
	}
	
	private Boolean doesArrayContain(String person, JSONArray people) {
		for (int i = 0; i < people.length(); i++) {
			if (people.get(i).toString().equals(person))
				return true;
		}
		return false;
	}
	/**
	 * send to everyone in this channel
	 * @param fromUser
	 * @param mydetvChannel
	 * @param message
	 */
	public void sendAdminChat(SessionUser fromUser, OneMydetvChannel mydetvChannel, String message) {
		try {
			LocalDateTime now = LocalDateTime.now();
			for(SessionUser su : mydetvChannel.getSessionUsers().values())
				if(! su.getMydetv().equals(fromUser.getMydetv()))
					chatFromUserToUser(message, null, su, now, false);	
		} catch(Exception e) {
			log.error("Error sending admin chat ");
		}
	}
	
	/**
	 * Send to everyone in every channel
	 * @param fromUser
	 * @param message
	 */
	public void sendAdminChatAllUsers(String message) {
		try {
			LocalDateTime now = LocalDateTime.now();
			for(OneMydetvChannel mydetvChannel : blackboard.getMydetvChannels().values())
				for(SessionUser su : mydetvChannel.getSessionUsers().values())
					chatFromUserToUser(message, null, su, now, false);
			
		} catch(Exception e) {
			log.error("Error sending admin chat ");
		}
	}	
	
	private void chatFromUserToUser(String message, SessionUser fromUser, SessionUser toUser, LocalDateTime dt, Boolean privateChat) {
		String prettyNow = tzDateTime(toUser, dt);
		String rv = "";
		rv += "<div class=\"chatDate\">"+prettyNow;
		if(privateChat) 
			rv+=" (private)";
		rv+="</div>";
		rv += "<div class=\"\">";
		if(fromUser != null)
			rv += "<div class=\"chatName inline\">"+fromUser.log()+"</div>";
		rv += "<div class=\"chatMessage inline\">&nbsp"+message+"</div>";
		rv += "</div>";
		JSONObject out = new JSONObject();
		out.put("verb", "chatMessage");
		out.put("message", rv);
		websocketWriter.sendMessage(toUser, out);
	}
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
	public String tzDateTime(SessionUser sessionUser, LocalDateTime dt) {
		String rv="";	
		
		if(sessionUser.getWebUser().getUserTimezone() == null || sessionUser.getWebUser().getUserTimezone().equals(""))
			rv = dtf.format(dt);
		else {
			ZoneId zoneId = ZoneId.of(sessionUser.getWebUser().getUserTimezone());
			log.info("zoneId:"+zoneId);
			ZonedDateTime zonedDateTime = ZonedDateTime.of(dt, zoneId);
			log.info("dt:"+dt+",ndt:"+zonedDateTime);
			rv = dtf.format(zonedDateTime);
		}		
		return rv;
	}
	public void showRecentChats(SessionUser sessionUser) {
		Timestamp now = Timestamp.from(Instant.now());
		ZonedDateTime zonedDateTime = now.toInstant().atZone(ZoneId.of("UTC"));
		Timestamp oldest = Timestamp.from(zonedDateTime.minus(4, ChronoUnit.HOURS).toInstant());
		TreeMap<Timestamp, WebChat> chats = dbChats.GetChats(sessionUser, 100, oldest);
		String fullMessage = "";
		for(WebChat webChat : chats.values()) {
			if(!peopleHelpers.isBlocked(sessionUser, webChat.getWuid())) {
				String prettyNow = tzDateTime(sessionUser, webChat.getDtSent());
				
				fullMessage += "<div class=\"chatDate\">"+prettyNow+"</div>";
				fullMessage += "<div class=\"\">";
				fullMessage += "<div class=\"chatName inline\">"+webChat.getUsername()+"</div>";
				fullMessage += "<div class=\"chatMessage inline\">&nbsp"+webChat.getMessage()+"</div>";
				fullMessage += "</div>";
			}
		}
			
		JSONObject out = new JSONObject();
		out.put("verb", "chatMessages");
		out.put("message", fullMessage);
		websocketWriter.sendMessage(sessionUser, out);
	}
	
	public void chatBlock(JSONObject response, SessionUser sessionUser) {
		JSONObject jo = new JSONObject(response.toString());
		
		String strPersonId = jo.getString("personId");
		String[] pieces = strPersonId.split("-");
		Integer personId = Integer.parseInt(pieces[0]);
		log.info("strPersonId:"+strPersonId+",personId "+personId);
		
		SessionUser target = blackboard.getSessionUserByWuid(personId);
		
		JSONObject out = new JSONObject();
		out.put("verb", "chatBlock");
		
		if(target==null) {
			out.put("result", "not found");
			websocketWriter.sendMessage(sessionUser, out);
			return;
		}

		out.put("personId", strPersonId);
		
		// you can't block yourself
		if(target.getWebUser().getWuid().equals(sessionUser.getWebUser().getWuid())){
			out.put("result", "can't block yourself");
			websocketWriter.sendMessage(sessionUser, out);
			return;
		}
		OneChatBlock cb = peopleHelpers.getChatBlock(sessionUser, personId);
		
		if(cb==null) {
			dbChats.addChatBlock(sessionUser.getWebUser().getWuid(), target.getWebUser().getWuid());
			out.put("result", "blocked");	
		}
		else {
			dbChats.deleteChatBlock(cb);
			out.put("result", "unblocked");
		}
		sessionUser.getWebUser().setChatBlocked(dbChats.getChatBlocked(sessionUser.getWebUser()));
		websocketWriter.sendMessage(sessionUser, out);
	}
	
}
