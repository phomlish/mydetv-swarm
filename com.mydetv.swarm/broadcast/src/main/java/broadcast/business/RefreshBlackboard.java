package broadcast.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import shared.data.OneChannelSchedule;
import shared.data.OneMbp;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.VideoCategorySubImage;
import shared.data.OneVideoConverted;
import shared.db.DbChannelSchedule;
import shared.db.DbMbp;
import shared.db.DbSchedule;
import shared.db.DbVideo;
import shared.db.DbVideoCategorySub;
import shared.db.DbVideoCategorySubImage;
import shared.db.DbVideoCategoryMain;
import shared.db.DbVideoConverted;
import shared.helpers.ScheduleHelpers;

public class RefreshBlackboard {
	
	private static final Logger log = LoggerFactory.getLogger(RefreshBlackboard.class);
	
	@Inject private Blackboard blackboard;
	@Inject private DbVideoCategoryMain dbVideoCategoryMain;
	@Inject private DbVideoCategorySub dbVideoCategorySub;
	@Inject private DbVideoCategorySubImage dbVideoCategorySubImage;
	@Inject private DbVideoConverted dbVideoConverted;
	@Inject private DbVideo dbVideo;
	@Inject private DbMbp dbMbp;
	@Inject private DbSchedule dbSchedule;
	@Inject private DbChannelSchedule dbChannelSchedule;
	
	HashMap<Integer, VideoCategoryMain> maincats;
	HashMap<Integer, VideoCategorySub> cats;
	HashMap<Integer, OneMbp> mbps;
	HashMap<Integer, OneVideoConverted> vcs;
	HashMap<Integer, VideoCategorySubImage> vcsi;
	HashMap<Integer, OneVideo> videos;
	HashMap<Integer, TreeMap<Integer, OneSchedule>> schedules;
	HashMap<Integer, HashMap<Integer, OneChannelSchedule>> channelSchedules;
	
	public void refreshBlackboard() {
		synchronized(blackboard.getRefreshLock()) {
			maincats = dbVideoCategoryMain.getMainCategories();
			cats = dbVideoCategorySub.getCategories();
			vcsi=dbVideoCategorySubImage.getImages();
			mbps = dbMbp.get();
			vcs = dbVideoConverted.getVideosConvertedAll();
			videos = dbVideo.getVideos();
			
			for(OneVideo ov : videos.values())
				if(isConverted(ov))
					ov.setIsConverted(true);
				else
					ov.setIsConverted(false);	
			
			// after we populate above we can get our clips
			ArrayList<OneVideo> clips = new ArrayList<OneVideo>();
			for(OneVideo ov : videos.values()) {
				if(! ov.getCatSub().equals(17))
					continue;
				if(ov.isConverted())
					clips.add(ov);
			}
			schedules = new HashMap<Integer, TreeMap<Integer, OneSchedule>>();
			channelSchedules = new HashMap<Integer, HashMap<Integer, OneChannelSchedule>>();
			
			for(OneMydetvChannel channel : blackboard.getMydetvChannels().values()) {
				TreeMap<Integer, OneSchedule> sched = dbSchedule.getSchedulesAll(channel.getChannelId());
				for(OneSchedule os : sched.values()) {
					if(! videos.containsKey(os.getPkey()))
						log.error("could not find the schedule's video "+os.getIdschedule());
					else {							
						OneVideo ov = videos.get(os.getPkey());
						ScheduleHelpers.SetEndDt(os, ov);
					}
				}
				schedules.put(channel.getChannelId(), sched);
				
				HashMap<Integer, OneChannelSchedule> csched = dbChannelSchedule.getAll(channel.getChannelId());
				channelSchedules.put(channel.getChannelId(), csched);
			}
			
			// *** ALL DONE getting from database, now swap in the values to the blackboard
			
			blackboard.setVideoMaincats(maincats);
			blackboard.setVideoSubcats(cats);
			blackboard.setVideoSubCatImages(vcsi);
			blackboard.setMbps(mbps);
			blackboard.setVideosConverted(vcs);
			blackboard.setVideos(videos);
			blackboard.setClips(clips);
			
			for(OneMydetvChannel channel : blackboard.getMydetvChannels().values()) {
				channel.setSchedules(schedules.get(channel.getChannelId()));
				channel.setChannelSchedules(channelSchedules.get(channel.getChannelId()));
			}
		}
	}
	
	private boolean isConverted(OneVideo ov) {
		int cnt=0;
		for(OneVideoConverted ovc : vcs.values()) {
			if(!ovc.getPkey().equals(ov.getPkey()))
				continue;
			if(ovc.getFailedReason()!=null)
				continue;
			if(ovc.getDtInactive()!=null)
				continue;
			//log.info("is "+ov.log()+" converted");
			for(String convertedType : blackboard.getConvertedTypes()) {
				if(convertedType.equals(ovc.getConvertedType()))
					cnt++;
			}
		}
		if(cnt==blackboard.getConvertedTypes().size())
			return true;
		return false;
	}

}
