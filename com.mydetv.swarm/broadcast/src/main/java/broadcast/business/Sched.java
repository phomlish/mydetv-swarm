package broadcast.business;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.data.FpnSeek;
import broadcast.janus.JClient;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.broadcast.OneJClient;

/**
 * @author phomlish
 *
 */

public class Sched {

	private static final Logger log = LoggerFactory.getLogger(Sched.class);
	@Inject private Blackboard blackboard;
	@Inject private VideosHelpers videosHelpers;
		
	/**
	 * we'll get what should be currently playing<br>
	 * @param mydetvChannel
	 */
	public FpnSeek get24x7PlayNow(OneMydetvChannel mydetvChannel, String convertedType) {
		OneJClient ojc = mydetvChannel.getOjClient();
		JClient jClient = (JClient) ojc.getjClient();
		Integer chanId = mydetvChannel.getChannelId();
			
		LocalDateTime now = LocalDateTime.now().withNano(0);
		OneSchedule osCurrent=null;
		OneSchedule osNext=null;
		for(OneSchedule os : mydetvChannel.getSchedules().values()) {
			if(os.getDt().isAfter(now)) {
				osNext=os;
				break;
			}
			else {
				osCurrent=os;
			}
		}

		Integer secondsUntilStartCurrent = (int) ChronoUnit.SECONDS.between(now, osCurrent.getDt());
		Integer secondsUntilEndCurrent = (int) ChronoUnit.SECONDS.between(now, osCurrent.getDtEnd());
		log.info("\ncurrent "+logSched(now, osCurrent));
		log.info("secondsUntilStartCurrent:"+secondsUntilStartCurrent+",secondsUntilEndCurrent:"+secondsUntilEndCurrent);

		Integer secondsUntilStartNext = (int) ChronoUnit.SECONDS.between(now, osNext.getDt());
		Integer secondsUntilEndNext = (int) ChronoUnit.SECONDS.between(now, osNext.getDtEnd());
		log.info("\nnext "+logSched(now, osNext));
		log.info("secondsUntilStartNext:"+secondsUntilStartNext+",secondsUntilEndNext:"+secondsUntilEndNext);
		
	
		// careful, we have videos with length of 5.4 seconds
		
		// if next is supposed to start real soon just do it 
		if(secondsUntilStartNext<=1) {
			log.info("next should start real soon, just do it now");
			return getFpnSeek(osNext, chanId, jClient.getConvertedType());
		}
		// if next is supposed to start real soon sleep a bit and then do it 
		else if(secondsUntilStartNext<5) {
			log.info("next should start soon, we will sleep "+(secondsUntilStartNext-1));
			doSleep((secondsUntilEndNext-1));
			return getFpnSeek(osNext, chanId, jClient.getConvertedType());
		}
		// if current is already done play SID for a bit
		else if(secondsUntilEndCurrent<0) {
			log.info("current is already done, play sid");
			Integer secondsUntilNext =(int) now.plusSeconds(1).until(osNext.getDt(), ChronoUnit.SECONDS);
			return(videosHelpers.getRandomClip(jClient, secondsUntilNext));
		}
		// if current is already done play SID for a bit
		else if(secondsUntilEndCurrent<6) {
			log.info("current is close to done, play sid");
			Integer secondsUntilNext =(int) now.plusSeconds(1).until(osNext.getDt(), ChronoUnit.SECONDS);
			return(videosHelpers.getRandomClip(jClient, secondsUntilNext));
		}
		else {
			log.info("current is in play, we will play it (possibly with seek)");
			return getFpnSeek(osCurrent, chanId, jClient.getConvertedType());
		}
	}
	
	private int[] specials = {679,681,687,2569,2575};
	private int currentSpecial = 0;
	public FpnSeek getSpecial(OneMydetvChannel mydetvChannel, String convertedType) {
		OneJClient ojc = mydetvChannel.getOjClient();
		JClient jClient = (JClient) ojc.getjClient();
		Integer chanId = mydetvChannel.getChannelId();
			
		OneVideo ov = blackboard.getVideos().get(679);
		FpnSeek fpnSeek = new FpnSeek(videosHelpers.getVideoUrl(ov, convertedType), 0, ov, null);
		

		return fpnSeek;
		
	}
	
	// get a new LocalDateTime.now in case we slept above
	private FpnSeek getFpnSeek(OneSchedule sched, Integer chanId, String convertedType) {
		LocalDateTime now = LocalDateTime.now().withNano(0);
		OneVideo ov = blackboard.getVideos().get(sched.getPkey());	
		
		Integer secondsFromBegining = (int) ChronoUnit.SECONDS.between(sched.getDt(), now);

		Integer seek;
		// if it is behind (>5 seconds).  This should only happen after a crash/restart.
		if(secondsFromBegining>5) {
			
			seek=secondsFromBegining;
			LocalDateTime newStart = now;
			Integer videoSeconds = (int) Math.ceil(ov.getLength());
			Integer newVideoSeconds = videoSeconds-seek;
			LocalDateTime newEnd = newStart.plusSeconds(newVideoSeconds);
			log.error("scrazzled sched chan "+chanId+" is behind\n"
				+"secondsFromBegining:"+secondsFromBegining+"\n"
				+"seek:"+seek+"\n"
				+"newStart "+newStart+"\n"
				+"newEnd   "+newEnd);			
		}
		else
			seek=0;
		
		return new FpnSeek(videosHelpers.getVideoUrl(ov, convertedType), seek, ov, sched);
	}
	
	public String logSched(LocalDateTime now, OneSchedule sched) {
		Integer pkey = sched.getPkey();
		OneVideo ov = blackboard.getVideos().get(pkey);
		LocalDateTime startDt = sched.getDt();
		LocalDateTime endDt = sched.getDt().plusSeconds(Integer.valueOf(ov.getLength().intValue()));
		//Integer millisUntilEnd = (int) ChronoUnit.MILLIS.between(now, endDt);
		//Float te = (float) (millisUntilEnd/1000);
		String rv = "";
		rv+="now     "+now+"\n";
		rv+="startDt "+startDt+"\n";
		rv+="endDt   "+endDt+"\n";
		//rv+="secondsUntilEnd "+te+"\n";
		rv+="ov:"+ov.log()+"\n";
		rv+="os:"+sched.log();
		
		return rv;
	}
	
	private void doSleep(Integer milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			log.error("sleep interupted");
		}
	}
}
