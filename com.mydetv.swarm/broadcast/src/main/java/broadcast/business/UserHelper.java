package broadcast.business;

import java.net.HttpCookie;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.jetty.websocket.api.Session;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;
import broadcast.matomo.MatomoHelper;
import shared.data.Config;
import shared.data.broadcast.SessionUser;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserRole;

public class UserHelper {

	@Inject private Config sharedConfig;
	@Inject private Blackboard blackboard;
	@Inject private CookieHelper cookieHelper;
	@Inject private MatomoHelper matomoHelper;
	private static final Logger log = LoggerFactory.getLogger(UserHelper.class);
		
	public ResponseBuilder addHeaders(SessionUser sessionUser, ResponseBuilder rb) {
		rb.cookie(cookieHelper.makeSessionCookie("mydetv", sessionUser.getMydetv()));

		// does the webuser have a different theme then the existing cookie?
		if(sessionUser.getWebUser()!=null
	    && sessionUser.getTheme()!=null
	    && ! sessionUser.getTheme().equals("")
	    && ! sessionUser.getTheme().equals(sessionUser.getTheme()))
			sessionUser.setTheme(sessionUser.getTheme());
		
		rb.cookie(cookieHelper.makeStickyCookie("theme", sessionUser.getTheme()));
		rb.header("Cache-Control", "max-age=0, must-revalidate");
		return rb;
	}
	public Response redirectLogin(SessionUser sessionUser) {		
		log.info("redirectLogin /auth");
		ResponseBuilder rb = Response.seeOther(UriBuilder.fromUri(URI.create("/auth")).build());
		rb = addHeaders(sessionUser, rb);
		return rb.build();
	}
	public Response redirectChangePassword(SessionUser sessionUser) {
		log.info("redirectLogin /auth/change-password");
		ResponseBuilder rb = Response.seeOther(UriBuilder.fromUri(URI.create("/auth/change-password")).build());
		rb = addHeaders(sessionUser, rb);
		return rb.build();
	}
	public Response redirectToPage(SessionUser sessionUser, String page) {
		log.info("redirectToPage "+page);
		ResponseBuilder rb = Response.seeOther(UriBuilder.fromUri(URI.create(page)).build());
		rb = addHeaders(sessionUser, rb);
		return rb.build();
	}
	public Response redirectUnauthorized(SessionUser sessionUser) {
		log.info("redirectUnauthorized");
		ResponseBuilder rb = Response.status(Status.FORBIDDEN);
		rb = addHeaders(sessionUser, rb);
		return rb.build();
	}
	
	/**
	 * Get a session user for a webpage<br>
	 * The webpage should then:<br>
	 *     if(! sessionUser.isAuthenticated())<br>
	 *     return userHelper.redirectAuth(sessionUser);<br>
	 * Browser cookies are brain dead, I have to send the hostname in the cookie value so the browser can return the correct one<br>
	 * otherwise we're all confused by swarm versus dev.swarm cookies<br>
	 * @param req
	 * @return
	 */
	public SessionUser getSessionUser(HttpServletRequest req) {
		
		log.debug("getSessionUser "+req.getRemoteAddr()+":"+req.getRemotePort());
		SessionUser sessionUser;
		
		// does he have the magic cookie?
		String mydetv = cookieHelper.findMydetvCookieValue(req, "mydetv");
		String inpath = req.getRequestURL().toString();
		if(req.getQueryString()!=null)
			inpath+="?"+req.getQueryString();
		//log.info("inpath:"+inpath);
		log.info("cookie from "+req.getRemoteAddr()+":"+req.getRemotePort()+" to "+inpath);
		
		// SOME OF THESE COULD LOG TWICE DUE TO AUTHENTICATION REDIRECTS
		// no or bad mydetv cookie
		String state;
		if(mydetv==null || mydetv.equals("")) {
			state="bad cookie";
			String trackingCode = matomoHelper.randomTrackingCode();
			sessionUser = new SessionUser(req.getRemoteAddr(),req.getRemotePort(),trackingCode);
			sessionUser.setBrowserTimezone(getBrowserTimezone(req));
			sessionUser.setTheme(getTheme(req));
			mydetv=sessionUser.getMydetv();
	        log.info("connection bad cookie "+sessionUser.getIpPort()+" to "+inpath);
	        blackboard.getSessionUsers().put(mydetv, sessionUser);
		}
		// has a cookie, but I don't recognize it, here's a freshy
		else if(! blackboard.getSessionUsers().containsKey(mydetv)) {
			state="unknown cookie";
			String trackingCode = matomoHelper.randomTrackingCode();
			sessionUser = new SessionUser(req.getRemoteAddr(),req.getRemotePort(),trackingCode);
			sessionUser.setBrowserTimezone(getBrowserTimezone(req));
			sessionUser.setTheme(getTheme(req));
			mydetv=sessionUser.getMydetv();
			log.info("connection unknown cookie |"+mydetv+"| "+sessionUser.getIpPort()+" to "+inpath);		
			blackboard.getSessionUsers().put(mydetv, sessionUser);
		}
		else {
			state="ok";
			sessionUser = blackboard.getSessionUsers().get(mydetv);
			log.debug("connection known "+sessionUser.getIpPort()+" to "+inpath);
			
			if(sessionUser.getWebUser() !=null)
				log.debug("and I know your name ("+sessionUser.getIpPort()+"):"+sessionUser.log());
		}
		
		// don't require full login for dev.  works for some testing but not everything!
		//if(sessionUser.getWebUser()==null && sharedConfig.getInstance().toLowerCase().contains("dev"))
		//	userHelper.createDummyUser(sessionUser);
		
		// remember he just clicked something
		sessionUser.setLastSeen(LocalDateTime.now());
		matomoHelper.sendTracking(sessionUser, req, state);
		return sessionUser;
	}
	
	public SessionUser getSessionUser(Session session) {
		List<HttpCookie> cookies = session.getUpgradeRequest().getCookies();
		 if (cookies.isEmpty()) {
			 return null;
		 }
		 for(HttpCookie cookie : cookies) {
			 String[] cSplit = cookie.getValue().split(":");
			if(cSplit.length!=2)
				continue;
			 if(!cookie.getName().equals("mydetv"))
				 continue;
			 String domain = cSplit[0];
			 String value = cSplit[1];
			 log.info("cookie:"+cookie.getName()+" "+domain+" "+value);

			if(! domain.equals(sharedConfig.getHostname()))
				continue;			 
			 if(blackboard.getSessionUsers().containsKey(value)) 
				 return blackboard.getSessionUsers().get(value);
		 }		 	 
		return null;
	}
	
	public Boolean isMydetvCookieKnown(HttpServletRequest req) {
		String mydetv = cookieHelper.findMydetvCookieValue(req, "mydetv");
		if(mydetv != null && ! mydetv.equals(""))
			return true;
		return false;
	}
	
	public String getBrowserTimezone(HttpServletRequest req) {
		String timezone = cookieHelper.findCookieValue(req, "btz");
		if(timezone==null)
			timezone="America/New_York";
		
		try {
			ZoneId.of(timezone);
		} catch(Exception e) {
			log.info("your browser is insane, "+timezone+" is invalid, here, use America/New_York");
			timezone="America/New_York";
		}
		log.trace("I set your timezone to "+timezone);
		return timezone;
	}
	
	public String getTheme(HttpServletRequest req) {
		String theme = cookieHelper.findMydetvCookieValue(req, "theme");
		if(theme==null || theme.equals(""))
			return "default";
		return theme;
	}
	
	public void createDummyUser(SessionUser sessionUser) {
		OneWebUser wu = new OneWebUser();
		wu.setWuid(1);
		wu.setUsername("dummy");
		wu.setBirthday(LocalDate.now().minusYears(23));
		TreeMap<Integer, OneWebUserRole> roles = new TreeMap<Integer, OneWebUserRole>();
		OneWebUserRole role = new OneWebUserRole();
		role.setWebUserRoleType(0);
		roles.put(0, role);
		wu.setRoles(roles);
		wu.setRequirePasswordChange(false);
		//sessionUser.setTheme("crisp");
		sessionUser.setWebUser(wu);
	}
	@SuppressWarnings("unused")
	private String getNoAuthUsername(HttpServletRequest req) {
		String username = "";
		String ip = req.getRemoteAddr();
		log.info("ip:"+ip);
		if(ip.contains("10.11.1.101"))
			username="EUSAL";
		else if(ip.contains("10.11.1.52"))
			username="EUSAL";
		else if(ip.contains("40.121.143.114"))
			username="EUSAA";
		else if(ip.contains("23.101.207"))
			username="WUSAA";
		else if(ip.contains("40.117.233"))
			username="EUSAA";
		else if(ip.contains("13.9"))
			username="CHKGA";
		else if(ip.contains("104.211.191"))
			username="WINDA";
		else if(ip.contains("13.73.111"))
			username="SAUSA";
		else if(ip.contains("52.138.143"))
			username="WIRLA";
		else if(ip.contains("52.231.200"))
			username="SKORA";
		else if(ip.contains("52.166.201"))
			username="WNLDA";		
		else if(ip.contains("104.41.56"))
			username="CBRAA";	
		
		if(username.length()==0) 
			username = RandomStringUtils.random(4, "abcdefghijklmnopqrstuvwxyz");
		
		username += getUserAgent(req);

		return username;
	}

	private String getUserAgent(HttpServletRequest req) {
		String ua="";
		String userAgent = req.getHeader("User-Agent").toLowerCase();
		log.trace("ua:"+userAgent);
		if(userAgent.indexOf("firefox")>=0)
			ua+="F";
		else if(userAgent.indexOf("chrome")>=0)
			ua+="C";
		else
			ua="U";
		
		if(userAgent.indexOf("windows") >= 0) 
			ua+="W";
		else if(userAgent.indexOf("mac") >= 0) 
			ua+="M";
		else if(userAgent.indexOf("linux") >= 0) 
			ua+="L";
		else
			ua="U";
			
		return ua;
	}	
}
