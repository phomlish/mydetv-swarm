package broadcast.business;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import shared.data.broadcast.SessionUser;

public class Authenticated {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Authenticated.class);
	@Inject private Blackboard blackboard;
	@Inject private CookieHelper cookieHelper;

	public SessionUser getFullyAuthenticatedSessionUser(HttpServletRequest req) {
		String mydetv = cookieHelper.findMydetvCookieValue(req, "mydetv");
		// if no cookie, no deal
		if(mydetv.equals(""))
			return null;
		
		// if I don't recognize your cookie, no deal
		if(! blackboard.getSessionUsers().containsKey(mydetv))
			return null;
		
		SessionUser sessionUser = blackboard.getSessionUsers().get(mydetv);	

		// if you are not authenticated, no deal
		if(sessionUser.getWebUser()==null)
			return null;
		
		if(sessionUser.getWebUser().getRequirePasswordChange()) 
			return null;
		
		// ok, deal
		return sessionUser;		
	}
}
