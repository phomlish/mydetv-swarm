package broadcast.status;

import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import broadcast.Blackboard;
import broadcast.data.status.SUser;
import shared.data.OneMydetvChannel;
import shared.data.broadcast.SessionUser;

public class StatusUser {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(StatusUser.class);
	@Inject private Blackboard blackboard;

	public TreeMap<String, SUser> getAllUsers() {
		return(getAllUsers(-1));
	}
	
	public TreeMap<String, SUser> getAllUsers(Integer channelId) {
		TreeMap<String, SUser> users = new TreeMap<String, SUser>();
		
		Map<String, SessionUser> map;
		if(channelId.equals(-1))
			map = blackboard.getSessionUsers();
		else {
			OneMydetvChannel mydetvChannel = blackboard.getMydetvChannels().get(channelId);
			map = mydetvChannel.getSessionUsers();
		}
		if(map.size()==0)
			return new TreeMap<String, SUser>();
					
		for(SessionUser su : map.values()) {  		
    		users.put(su.getMydetv(), new SUser(su));
    	}
		return users;
	}
}
