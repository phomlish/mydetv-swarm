package broadcast.api;

import java.util.concurrent.TimeUnit;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import broadcast.Blackboard;

public class ApiPooledHost {

	private static final Logger log = LoggerFactory.getLogger(ApiPooledHost.class);
	@Inject private Blackboard blackboard;
	
	OneApiHost apiHost;
	CloseableHttpAsyncClient httpClient;
	
	public void createApiThread(OneApiHost apiHost) {
		log.info("initializing "+apiHost.getUri().getHost()+" "+apiHost.getUri());
		this.apiHost=apiHost;
		
		ConnectingIOReactor ioreactor;		
		try {			
			ioreactor = new DefaultConnectingIOReactor();
		} catch (IOReactorException e) {
			log.error("e:"+e.toString());
			return;
		}
		apiHost.setConnectingIOReactor(ioreactor);
		 
		// https://stackoverflow.com/questions/30689995/what-does-setdefaultmaxperroute-and-setmaxtotal-mean-in-httpclient
		PoolingNHttpClientConnectionManager pool = 
				new PoolingNHttpClientConnectionManager(ioreactor,blackboard.getOutgoingApiEncryption());
		pool.setDefaultMaxPerRoute(20);
        pool.setMaxTotal(20);
        pool.closeIdleConnections(2, TimeUnit.MINUTES);       
		apiHost.setPoolingNHttpClientConnectionManager(pool);
		
        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectionRequestTimeout(5*1000)
            .build();
        
		httpClient = HttpAsyncClientBuilder.create()
		        .setConnectionManager(pool)
		        .setDefaultRequestConfig(requestConfig)
		        .build();
		        
		httpClient.start();
		apiHost.setHttpClient(httpClient);
		log.info("initialized "+apiHost.getUri().getHost());
		
	}
	
}
