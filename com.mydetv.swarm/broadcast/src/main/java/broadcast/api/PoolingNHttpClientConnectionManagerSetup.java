package broadcast.api;

import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PoolingNHttpClientConnectionManagerSetup {

	private static final Logger log = LoggerFactory.getLogger(PoolingNHttpClientConnectionManagerSetup.class);
	
	public static PoolingNHttpClientConnectionManager init()  {
               
		try {
			ConnectingIOReactor ioreactor;
			ioreactor = new DefaultConnectingIOReactor(IOReactorConfig.custom()
			        .setConnectTimeout(30000)
			        .setSoTimeout(30000)
			        .build());
			PoolingNHttpClientConnectionManager pool = new PoolingNHttpClientConnectionManager(ioreactor);
	        pool.setDefaultMaxPerRoute(5);
	        pool.setMaxTotal(5);
	        return pool;
		} catch (IOReactorException e) {
			log.error("e:"+e);
		}  
        return null;		
	}
}