package broadcast.api;

public class OneApiHostConfig {

	private Integer id;
	private String url;
	private String apiKey;
	private Integer version;
	private String apiType;
	private String apiShortname;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getApiType() {
		return apiType;
	}
	public void setApiType(String apiType) {
		this.apiType = apiType;
	}
	public String getApiShortname() {
		return apiShortname;
	}
	public void setApiShortname(String apiShortname) {
		this.apiShortname = apiShortname;
	}
	
}