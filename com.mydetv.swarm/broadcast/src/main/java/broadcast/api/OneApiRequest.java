package broadcast.api;

import java.time.LocalDateTime;

import shared.data.broadcast.SessionUser;

public class OneApiRequest {

	private SessionUser su;
	private String requestContent;
	private OneApiHost apiHost;
	
	private Integer apiConnectionSerial;
	private Integer apiRequestSerial;
	private LocalDateTime startedRequest;
	private Boolean requestClosed = false;
	
	public OneApiRequest(SessionUser su, String request, OneApiHost apiHost) {
		this.setSu(su);
		this.requestContent=request;
		this.apiHost=apiHost;
		this.apiRequestSerial=apiHost.getApiRequestSerialNext();
	}
	
	public String log() {
		try {
			String rv=" host:";
			rv += (apiHost==null) ? "unk" : apiHost.getApiHostConfig().getApiShortname();
			rv+=",req#:";
			rv+= (apiRequestSerial==null) ? "unk" : apiRequestSerial;
			rv+=",started:";
			rv+=(startedRequest==null) ? "unk" : startedRequest;
			//if(su!=null) 
			//	rv+=" toolbar ("+su.log()+")";
			return rv;
		} catch(Exception e) {
			return e.toString();
		}
	}
	// getters/setters
	public String getRequest() {
		return requestContent;
	}
	public void setRequest(String request) {
		this.requestContent = request;
	}
	public LocalDateTime getStartedRequest() {
		return startedRequest;
	}
	public void setStartedRequest(LocalDateTime startedRequest) {
		this.startedRequest = startedRequest;
	}
	public Integer getRequestSerial() {
		return apiRequestSerial;
	}
	public OneApiHost getApiHost() {
		return apiHost;
	}
	public void setApiHost(OneApiHost apiHost) {
		this.apiHost = apiHost;
	}
	public Boolean getRequestClosed() {
		return requestClosed;
	}
	public void setRequestClosed(Boolean requestClosed) {
		this.requestClosed = requestClosed;
	}
	public Integer getApiConnectionSerial() {
		return apiConnectionSerial;
	}
	public void setApiConnectionSerial(Integer apiConnectionSerial) {
		this.apiConnectionSerial = apiConnectionSerial;
	}

	public SessionUser getSu() {
		return su;
	}

	public void setSu(SessionUser su) {
		this.su = su;
	}
}