package broadcast.api;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneApiHost {

	private static final Logger log = LoggerFactory.getLogger(OneApiHost.class);
	// identifier of the host
	private Integer hostCounter;
	private OneApiHostConfig apiHostConfig;

	// this is the serial number of the api connection
	private Integer apiConnectionSerial=0;
	// and the serial number of the request
	private Integer apiRequestSerial=0;
	
	private URI uri = null;
	private int port;

	// new
	private PoolingNHttpClientConnectionManager poolingNHttpClientConnectionManager;
	private CloseableHttpAsyncClient httpClient;
	private ConnectingIOReactor connectingIOReactor;
	private ApiPooledHost apiPooledHost;
	private Integer borrowFailed = 0;

	// constructor
	public OneApiHost(OneApiHostConfig apiHostConfig, Integer hostCounter) {
		try {
			this.apiHostConfig=apiHostConfig;			
			this.hostCounter=hostCounter;
						
			uri = new URI(apiHostConfig.getUrl());
			String scheme = uri.getScheme() == null? "http" : uri.getScheme();
			if ("https".equalsIgnoreCase(scheme)) {
				port=443;
			}
			else
				port=80;

		} catch (URISyntaxException e) {
			log.error("Error initializing apiHost :"+apiHostConfig.getUrl());
		}
		log.info("apiHost initialized "+apiHostConfig.getApiShortname());
	}
	
	// custom getters/setters
	public Integer getApiConnectionSerialNext() {
		if(apiConnectionSerial>=Integer.MAX_VALUE)
			apiConnectionSerial=0;
		return (apiConnectionSerial++);
	}
	public Integer getApiRequestSerialNext() {
		if(apiRequestSerial>=Integer.MAX_VALUE)
			apiRequestSerial=0;
		return (apiRequestSerial++);
	}
	// getters/setters
	public Integer getHostCounter() {
		return hostCounter;
	}
	public void setHostCounter(Integer hostCounter) {
		this.hostCounter = hostCounter;
	}
	public Integer getApiConnectionSerial() {
		return apiConnectionSerial;
	}
	public void setApiConnectionSerial(Integer apiConnectionSerial) {
		this.apiConnectionSerial = apiConnectionSerial;
	}
	public OneApiHostConfig getApiHostConfig() {
		return apiHostConfig;
	}
	public void setApiHostConfig(OneApiHostConfig apiHostConfig) {
		this.apiHostConfig = apiHostConfig;
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public Integer getApiRequestSerial() {
		return apiRequestSerial;
	}
	public void setApiRequestSerial(Integer apiRequestSerial) {
		this.apiRequestSerial = apiRequestSerial;
	}
	public PoolingNHttpClientConnectionManager getPoolingNHttpClientConnectionManager() {
		return poolingNHttpClientConnectionManager;
	}
	public void setPoolingNHttpClientConnectionManager(PoolingNHttpClientConnectionManager poolingNHttpClientConnectionManager) {
		this.poolingNHttpClientConnectionManager = poolingNHttpClientConnectionManager;
	}
	public CloseableHttpAsyncClient getHttpClient() {
		return httpClient;
	}
	public void setHttpClient(CloseableHttpAsyncClient httpClient) {
		this.httpClient = httpClient;
	}
	public ConnectingIOReactor getConnectingIOReactor() {
		return connectingIOReactor;
	}
	public void setConnectingIOReactor(ConnectingIOReactor connectingIOReactor) {
		this.connectingIOReactor = connectingIOReactor;
	}
	public ApiPooledHost getApiPooledHost() {
		return apiPooledHost;
	}
	public void setApiPooledHost(ApiPooledHost apiPooledHost) {
		this.apiPooledHost = apiPooledHost;
	}
	public Integer getBorrowFailed() {
		return borrowFailed;
	}
	public void setBorrowFailed(Integer borrowFailed) {
		this.borrowFailed = borrowFailed;
	}		
	public void incrementBorrowFailed() {
		borrowFailed++;
	}
}
