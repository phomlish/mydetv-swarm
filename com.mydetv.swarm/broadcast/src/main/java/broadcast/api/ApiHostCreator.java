package broadcast.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import broadcast.Blackboard;
import shared.data.Config;


/**
 * We set the pool parameters in this class<br>
 * shared worker that creates an outgoing OneApiHost<br>
 * blackboard has the maps of the OneApiHosts<br>
 * @author phomlish
 *
 */
public class ApiHostCreator {

	private static final Logger log = LoggerFactory.getLogger(ApiHostCreator.class);
	@Inject private Config config;
	@Inject private Blackboard blackboard;
    @Inject Provider<ApiPooledHost> apiPooledHostCreatorProvider;
	
    public String createShortUrl(String url) {
    	String shortUrl;
    	shortUrl = url.replace("https://", "");
		shortUrl = shortUrl.replace("http://", "");
		String[] lines = shortUrl.split("\\.");
		if(lines.length>0) 
			shortUrl=lines[0];
    	return shortUrl;
    }

    public void createMatomoHost() {
    	OneApiHostConfig apiHostConfig = new OneApiHostConfig();
    	apiHostConfig.setId(config.getMatomoSite());
    	apiHostConfig.setUrl(config.getMatomoUrl());
    	apiHostConfig.setApiShortname(createShortUrl(apiHostConfig.getUrl()));
    	apiHostConfig.setApiKey(config.getMatomoKey());
		apiHostConfig.setApiType("matomo");
		OneApiHost apiHost = createApi(apiHostConfig);
		blackboard.setMatomoHost(apiHost);
    	
    }
	public OneApiHost createApi(OneApiHostConfig apiHostConfig) {

		OneApiHost apiHost;		
		if(apiHostConfig.getApiType().equals("matomo")) {
			apiHost = new OneApiHost(apiHostConfig, 0);
			blackboard.setMatomoHost(apiHost);
		}
		else {
			log.error("Unrecognized api host type "+apiHostConfig.getApiType());
			return null;
		}
		
		ApiPooledHost apiPooledHost = apiPooledHostCreatorProvider.get();
		apiHost.setApiPooledHost(apiPooledHost);
		apiPooledHost.createApiThread(apiHost);
		return apiHost;
		
	}
		
}
