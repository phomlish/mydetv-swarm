package broadcast.api;

import java.time.LocalDateTime;
import java.util.concurrent.Future;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.protocol.HttpContext;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RequestThread extends Thread {

	private static final Logger log = LoggerFactory.getLogger(RequestThread.class);
	
    private CloseableHttpAsyncClient client;
    private HttpContext context;
    private HttpGet httpGet;
    private HttpPost httpPost;
    private OneApiRequest apiRequest;
    private OneApiHost apiHost;
    
    public void init(CloseableHttpAsyncClient client, HttpGet req, OneApiRequest apiRequest){
    	this.client = client;
        this.httpGet = req;
        finishCreatingThread(apiRequest);
    }
    public void init(CloseableHttpAsyncClient client, HttpPost post, OneApiRequest apiRequest){
    	this.client = client;
        this.httpPost = post;
        finishCreatingThread(apiRequest);
    }
 
    // thread start/run
    @Override
    public void run() {
        try {
        	apiRequest.setStartedRequest(LocalDateTime.now());
        	Future<HttpResponse> future;
        	if(httpPost!=null)
        		future = client.execute(httpPost, context, null);
        	else
        		future = client.execute(httpGet, context, null);
        	
            HttpResponse response = future.get();
            //HttpEntity entity = response.getEntity();
            //String responseString = EntityUtils.toString(entity, "UTF-8");
            
            if(response.getStatusLine().getStatusCode() != HttpStatus.OK_200)
				throw new Exception(detailedError("returned message was not status OK",response.getStatusLine().toString()));
            //if (response.getAllHeaders().length<1)
	        //	throw new Exception(detailedError("returned message had no headers",responseString));  
	        //String contentType = getHeader(response.getAllHeaders(), "Content-Type");
	        //if(! contentType.contains("application/json"))
	        //	throw new Exception(detailedError("returned message content-type not json",responseString));
	        
	        //log.info("resp:"+responseString);
            log.debug("for "+apiRequest.getRequest());
			//if(apiRequest.getApiHost().getApiHostConfig().getApiType().equals("us"))
			//{}
			//else
			//	log.error("unknown api type "+apiRequest.getApiHost().getApiHostConfig().getApiType());
            closeApiRequest(apiRequest);
        } catch (Exception ex) {
            log.error("exception "+apiRequest.log()+", "+ex);
            apiHost.incrementBorrowFailed();
            closeApiRequest(apiRequest);
        }
    }
    // helpers
    private void finishCreatingThread(OneApiRequest apiRequest) {	    	
        context = HttpClientContext.create();
    	this.apiRequest=apiRequest;
    	this.apiHost=apiRequest.getApiHost();
    	apiRequest.setApiConnectionSerial(apiHost.getApiConnectionSerialNext());
    }
    
	private String detailedError(String cause, String detail) {
		String rv = "";
		rv+="cause:"+cause;
		rv+=",host:"+apiHost.getUri().getHost();
		rv+="\ndetail:"+detail;
		return rv;
	}
	/*
	private String getHeader(Header[] headers, String tHeader) {
		for(Header header : headers) {
			if(header.getName().equals(tHeader)) 
				return header.getValue();
		}
		return "";
	}
	*/
	public void closeApiRequest(OneApiRequest apiRequest) {
		if(apiRequest==null) {
			log.error("request is already null!");
			return;
		}
		if(apiRequest.getRequestClosed()) {
			log.info("no need to re-close the request, already closed");
			return;
		}
		apiRequest.setRequestClosed(true);
		log.debug("closed apiRequest "+apiRequest.getApiHost().getApiHostConfig().getApiType());
	}
}