package broadcast;

import java.io.IOException;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import shared.data.Config;
import shared.setups.ReadConfigJson;

public class Main  {
	
	static {System.setProperty("user.timezone", "UTC");}
	static {System.setProperty("logback.configurationFile", "logback-broadcast.xml");}
	private static Logger log = LoggerFactory.getLogger(Main.class);
	
	@Inject private static BroadcastImpl swarm;

	public static void main(String[] mainArgs)
	{		
		try {
			if(mainArgs.length!=1)
				throw new ConfigurationException("you must provide the config directory on the command line...");
			
			/*
	        Map<String, String> env = System.getenv();
	        log.info("Environment:");
	        for (String envName : env.keySet()) {
	            log.info(envName+" "+env.get(envName));
	        }
	        */
			
			Config config = ReadConfigJson.GetConfig(mainArgs[0]);
			
			Injector mainInjector = Guice.createInjector(new Bindings(config));
			swarm = mainInjector.getInstance(BroadcastImpl.class);
			swarm.setInjector(mainInjector);   	    	
			swarm.start();
			
			if (Boolean.parseBoolean(System.getenv("RUNNING_IN_ECLIPSE")) == true) {
				log.info("you are running in eclipse. Press enter in the console to test the shutdown hook");
				try {
                    System.in.read();
				} catch (IOException e) {
                   log.error("shudown:"+e);
				}
				System.exit(0);
			}

		} catch (Exception e) {
			log.warn("Check previous ERROR/WARNING logs as well!!!!", e);
		}
	}
}
