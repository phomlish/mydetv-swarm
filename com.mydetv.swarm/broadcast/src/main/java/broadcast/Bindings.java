package broadcast;

import com.google.inject.Binder;
import com.google.inject.Module;

import broadcast.auth.Validations;
import broadcast.broadcaster.BroadcastProfileHelpers;
import broadcast.broadcaster.BroadcasterHelpers;
import broadcast.broadcaster.BroadcasterMessages;
import broadcast.broadcaster.BroadcasterMessaging;
import broadcast.broadcaster.WsBroadcasterReader;
import broadcast.business.Authenticated;
import broadcast.business.Authorized;
import broadcast.business.ChatHelpers;
import broadcast.business.CookieHelper;
import broadcast.business.LocalVideoHelpers;
import broadcast.business.PeopleHelpers;
import broadcast.business.RefreshBlackboard;
import broadcast.business.Sched;
import broadcast.business.UserHelper;
import broadcast.business.UserMessageHelpers;
import broadcast.business.VideosHelpers;
import broadcast.business.WebHelper;
import broadcast.business.BrowserHelpers;
import broadcast.janus.JAdminClient;
import broadcast.janus.JAdminControl;
import broadcast.janus.JAdminHandler;
import broadcast.janus.JAdminListener;
import broadcast.janus.JConnectViewer;
import broadcast.janus.JControl;
import broadcast.janus.JDetach;
import broadcast.janus.JHandlerHelpers;
import broadcast.janus.JHelpers;
import broadcast.janus.JShutdown;
import broadcast.janus.JStreamingHandler;
import broadcast.janus.JVideoRoom;
import broadcast.janus.JVideoRoomHandler;
import broadcast.janus.JUser;
import broadcast.master.MasterServer;
import broadcast.master.WatchDiskSpaceTask;
import broadcast.master.WatchPingTask;
import broadcast.master.WatchStudioCountTask;
import broadcast.matomo.GeoIp;
import broadcast.matomo.MatomoHelper;
import broadcast.master.JukeboxActions;
import broadcast.master.MasterActions;
import broadcast.master.MasterHandler;
import broadcast.master.MasterHandlerInit;
import broadcast.master.MasterInit;
import broadcast.master.MasterReader;
import broadcast.p2p.BWTests;
import broadcast.p2p.BwHelpers;
import broadcast.p2p.CGHelpers;
import broadcast.p2p.ConnectionSmarts;
import broadcast.p2p.FCHelpers;
import broadcast.p2p.IceFailed;
import broadcast.p2p.NegotiationNeeded;
import broadcast.p2p.RtcFailed;
import broadcast.p2p.SessionUserInit;
import broadcast.p2p.VerifyCGBW;
import broadcast.p2p.P2PHelpers;
import broadcast.p2p.VideoU2U;
import broadcast.p2p.VideoWaiters;
import broadcast.status.StatusUser;
import broadcast.tasks.PeopleTask;
import broadcast.tasks.PingTask;
import broadcast.tasks.StatsTask;
import broadcast.web.CustomErrorHandler;
import broadcast.web.HttpEmbedded;
import broadcast.web.JettyModule;
import broadcast.web.MyGuiceServletContextListener;
import shared.data.Config;
import shared.db.DbArchive;
import shared.db.DbBroadcasterProfile;
import shared.db.DbChats;
import shared.db.DbConnection;
import shared.db.DbMbp;
import shared.db.DbSchedule;
import shared.db.DbSounds;
import shared.db.DbVideo;
import shared.db.DbVideoCategorySub;
import shared.db.DbWebUser;
import shared.setups.SetupEncryption;

public class Bindings implements Module {

	Config config;
	public Bindings(Config config) {
		this.config = config;
	}
	
	public void configure(Binder binder) {
		
		binder.bind(Config.class).toInstance(config);
		binder.bind(Blackboard.class).asEagerSingleton();
		binder.bind(SetupEncryption.class).asEagerSingleton();
		binder.bind(Validations.class).asEagerSingleton();
		binder.bind(BroadcastTest.class).asEagerSingleton();
		
		// from shared
		binder.bind(DbConnection.class).asEagerSingleton();
		binder.bind(DbVideoCategorySub.class).asEagerSingleton();
		binder.bind(DbSchedule.class).asEagerSingleton();
		binder.bind(DbVideo.class).asEagerSingleton();
		binder.bind(DbMbp.class).asEagerSingleton();
		binder.bind(DbWebUser.class).asEagerSingleton();
		binder.bind(DbArchive.class).asEagerSingleton();
		binder.bind(DbSounds.class).asEagerSingleton();
		binder.bind(DbBroadcasterProfile.class).asEagerSingleton();
		binder.bind(shared.db.DbWebUsers.class).asEagerSingleton();
		
		// from broadcast
		binder.bind(ShutdownHook.class).asEagerSingleton();
		binder.bind(DbChats.class).asEagerSingleton();
		
		binder.bind(PingTask.class).asEagerSingleton();
		binder.bind(StatsTask.class).asEagerSingleton();
		binder.bind(PeopleTask.class).asEagerSingleton();
		
		binder.bind(HttpEmbedded.class).asEagerSingleton();
		binder.bind(CustomErrorHandler.class).asEagerSingleton();	
		binder.bind(JettyModule.class).asEagerSingleton();
		binder.bind(MyGuiceServletContextListener.class).asEagerSingleton();
		
		// business layer
		binder.bind(Authenticated.class).asEagerSingleton();
		binder.bind(Authorized.class).asEagerSingleton();
		binder.bind(Sched.class).asEagerSingleton();
		binder.bind(CookieHelper.class).asEagerSingleton();
		binder.bind(UserHelper.class).asEagerSingleton();
		binder.bind(BroadcastProfileHelpers.class).asEagerSingleton();
		binder.bind(WebHelper.class).asEagerSingleton();		
		binder.bind(PeopleHelpers.class).asEagerSingleton();
		binder.bind(ChatHelpers.class).asEagerSingleton();
		binder.bind(VideosHelpers.class).asEagerSingleton();
		binder.bind(LocalVideoHelpers.class).asEagerSingleton();
		binder.bind(UserMessageHelpers.class).asEagerSingleton();
		binder.bind(RefreshBlackboard.class).asEagerSingleton();
		binder.bind(BrowserHelpers.class).asEagerSingleton();
		
		// p2p stuff
		binder.bind(SessionUserInit.class).asEagerSingleton();
		binder.bind(BWTests.class).asEagerSingleton();
		binder.bind(BwHelpers.class).asEagerSingleton();
		binder.bind(P2PHelpers.class).asEagerSingleton();
		binder.bind(VideoWaiters.class).asEagerSingleton();
		binder.bind(IceFailed.class).asEagerSingleton();
		binder.bind(NegotiationNeeded.class).asEagerSingleton();
		binder.bind(VideoU2U.class).asEagerSingleton();	
		binder.bind(ConnectionSmarts.class).asEagerSingleton();
		binder.bind(RtcFailed.class).asEagerSingleton();
		binder.bind(CGHelpers.class).asEagerSingleton();
		binder.bind(VerifyCGBW.class).asEagerSingleton();
		binder.bind(FCHelpers.class).asEagerSingleton();
		
		// broadcaster
		binder.bind(WsBroadcasterReader.class).asEagerSingleton();
		binder.bind(BroadcasterMessages.class).asEagerSingleton();
		binder.bind(BroadcasterHelpers.class).asEagerSingleton();
		binder.bind(BroadcasterMessaging.class).asEagerSingleton();
		
		// janus stuff.  note: check BroadcastImpl to see how we create the channel janus bindings
		binder.bind(JAdminClient.class).asEagerSingleton();
		binder.bind(JAdminControl.class).asEagerSingleton();
		binder.bind(JAdminHandler.class).asEagerSingleton();
		binder.bind(JAdminListener.class).asEagerSingleton();
		// and some singletons for channel janus
		binder.bind(JHelpers.class).asEagerSingleton();
		binder.bind(JConnectViewer.class).asEagerSingleton();
		binder.bind(JControl.class).asEagerSingleton();
		binder.bind(JUser.class).asEagerSingleton();
		binder.bind(JVideoRoomHandler.class).asEagerSingleton();
		binder.bind(JStreamingHandler.class).asEagerSingleton();
		binder.bind(JVideoRoom.class).asEagerSingleton();
		binder.bind(JDetach.class).asEagerSingleton();
		binder.bind(JShutdown.class).asEagerSingleton();
		binder.bind(JHandlerHelpers.class).asEagerSingleton();
		
		// master/slave stuff
		binder.bind(MasterInit.class).asEagerSingleton();
		binder.bind(MasterServer.class).asEagerSingleton();
		binder.bind(MasterHandlerInit.class).asEagerSingleton();
		binder.bind(MasterHandler.class).asEagerSingleton();
		binder.bind(MasterActions.class).asEagerSingleton();
		binder.bind(MasterReader.class).asEagerSingleton();
		binder.bind(JukeboxActions.class).asEagerSingleton();
		binder.bind(WatchPingTask.class).asEagerSingleton();
		binder.bind(WatchDiskSpaceTask.class).asEagerSingleton();
		binder.bind(WatchStudioCountTask.class).asEagerSingleton();
		
		// status stuff
		binder.bind(StatusUser.class).asEagerSingleton();
		
		// outgoing api stuff
		binder.bind(MatomoHelper.class).asEagerSingleton();
		binder.bind(GeoIp.class).asEagerSingleton();
	}
}
