package broadcast;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.http.config.Registry;
import org.apache.http.nio.conn.SchemeIOSessionStrategy;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.google.inject.Injector;

import broadcast.api.OneApiHost;
import broadcast.master.OneUsbPin;
import shared.data.OneMbp;
import shared.data.OneMydetvChannel;
import shared.data.OneSound;
import shared.data.OneVideo;
import shared.data.VideoCategorySub;
import shared.data.VideoCategorySubImage;
import shared.data.VideoCategoryMain;
import shared.data.OneVideoConverted;
import shared.data.broadcast.SessionUser;
import shared.data.broadcast.WebSocketConnection;

public class Blackboard {
	
	
	private Injector mainInjector;
	private SslContextFactory sslContextFactory;
	private Registry<SchemeIOSessionStrategy> outgoingApiEncryption;
	private HttpClient httpClient;
	private SecureRandom secureRandom = new SecureRandom();
	private Timestamp bootTime = new Timestamp(System.currentTimeMillis());
	private int count=0;
	private Thread threadEmbedded;
	private Thread threadMaster;
	private Thread janusAdminThread;
	private ScheduledExecutorService scheduler;
	private Boolean shuttingDown=false;
	private OneApiHost matomoHost;
	private ArrayList<String> trackingCodes = new ArrayList<String>();
	// locks
	// documented in "SU & CG & RTC.odt"
	private Object lockCGBWs = new Object();
	
	private LocalDateTime systemLoginLocked = null;
	
	// a session user is a web visitor
	private Map<String, SessionUser> sessionUsers = new HashMap<String, SessionUser>();
	
	// a websocket connection is a web visitor to a channel page, only one allowed at a time
	// key is: Integer hashcode = Integer.valueOf(session.hashCode());
	private Map<Integer, WebSocketConnection> webSocketConnections = new HashMap<Integer, WebSocketConnection>();
	// let's keep track of the messages we're sending
	private Integer messageId = 0;
	
	// keeping track of our channels, initializes from the database
	private TreeMap<Integer, OneMydetvChannel> channels = new TreeMap<Integer, OneMydetvChannel>();

	private Object refreshLock = new Object();
	private HashMap<Integer, VideoCategorySub> videoSubcats;
	private HashMap<Integer, VideoCategoryMain> videoMaincats;
	private HashMap<Integer, VideoCategorySubImage> videoSubCatImages;
	private HashMap<Integer, OneVideo> videos;
	private HashMap<Integer, OneVideoConverted> videosConverted;
	private HashMap<Integer, OneMbp> mbps;
	private List<String> convertedTypes = new ArrayList<String>();
	private ArrayList<OneVideo> clips;
	// Master/Slave
	private Object soundLock = new Object();
	private HashMap<Integer, OneSound> sounds = new HashMap<Integer, OneSound>();

	//
	// custom getter
	public SessionUser getSessionUserByWuid(Integer id) {
		for(SessionUser su : sessionUsers.values())
			if(su.getWebUser()!=null && su.getWebUser().getWuid().equals(id))
				return su;
		return null;
	}
	// getters/setters
	public int getCount() {
		return count++;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Injector getMainInjector() {
		return mainInjector;
	}
	public void setMainInjector(Injector mainInjector) {
		this.mainInjector = mainInjector;
	}
	public Thread getThreadEmbedded() {
		return threadEmbedded;
	}
	public void setThreadEmbedded(Thread threadEmbedded) {
		this.threadEmbedded = threadEmbedded;
	}
	public Map<String, SessionUser> getSessionUsers() {
		return sessionUsers;
	}
	public void setSessionUsers(Map<String, SessionUser> sessionUsers) {
		this.sessionUsers = sessionUsers;
	}
	public Map<Integer, WebSocketConnection> getWebSocketConnections() {
		return webSocketConnections;
	}
	public void setWebSocketConnections(Map<Integer, WebSocketConnection> webSocketConnections) {
		this.webSocketConnections = webSocketConnections;
	}
	public Integer getMessageId() {
		return messageId++;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public Timestamp getBootTime() {
		return bootTime;
	}
	public void setBootTime(Timestamp bootTime) {
		this.bootTime = bootTime;
	}
	public TreeMap<Integer, OneMydetvChannel> getMydetvChannels() {
		return channels;
	}
	public void setMydetvChannels(TreeMap<Integer, OneMydetvChannel> channels) {
		this.channels = channels;
	}
	public Thread getThreadMaster() {
		return threadMaster;
	}
	public void setThreadMaster(Thread threadMaster) {
		this.threadMaster = threadMaster;
	}
	public ScheduledExecutorService getScheduler() {
		return scheduler;
	}
	public void setScheduler(ScheduledExecutorService scheduler) {
		this.scheduler = scheduler;
	}
	public Thread getJanusAdminThread() {
		return janusAdminThread;
	}
	public void setJanusAdminThread(Thread janusAdminThread) {
		this.janusAdminThread = janusAdminThread;
	}
	public SecureRandom getSecureRandom() {
		return secureRandom;
	}
	public void setSecureRandom(SecureRandom secureRandom) {
		this.secureRandom = secureRandom;
	}
	public LocalDateTime getSystemLoginLocked() {
		return systemLoginLocked;
	}
	public void setSystemLoginLocked(LocalDateTime systemLoginLocked) {
		this.systemLoginLocked = systemLoginLocked;
	}
	public Boolean getShuttingDown() {
		return shuttingDown;
	}
	public void setShuttingDown(Boolean shuttingDown) {
		this.shuttingDown = shuttingDown;
	}
	public HttpClient getHttpClient() {
		return httpClient;
	}
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	public List<String> getConvertedTypes() {
		return convertedTypes;
	}
	public void setConvertedTypes(List<String> convertedTypes) {
		this.convertedTypes = convertedTypes;
	}
	// REQUIRE MAINT LOCKING
	public Object getRefreshLock() {
		return refreshLock;
	}
	public void setRefreshLock(Object refreshLock) {
		this.refreshLock = refreshLock;
	}
	
	public HashMap<Integer, VideoCategoryMain> getVideoMaincats() {
		synchronized(refreshLock) {
			return videoMaincats;
		}
	}
	public void setVideoMaincats(HashMap<Integer, VideoCategoryMain> videoMaincats) {
		this.videoMaincats = videoMaincats;
	}
	public ArrayList<OneVideo> getClips() {
		synchronized(refreshLock) {
			return clips;
		}
	}
	public void setClips(ArrayList<OneVideo> clips) {
		this.clips = clips;
	}
	public HashMap<Integer, OneMbp> getMbps() {
		synchronized(refreshLock) {
			return mbps;
		}
	}
	public void setMbps(HashMap<Integer, OneMbp> mbps) {
		this.mbps = mbps;
	}
	public HashMap<Integer, OneVideoConverted> getVideosConverted() {
		synchronized(refreshLock) {
			return videosConverted;
		}
	}
	public void setVideosConverted(HashMap<Integer, OneVideoConverted> videosConverted) {
		this.videosConverted = videosConverted;
	}
	public HashMap<Integer, VideoCategorySub> getVideoSubcats() {
		synchronized(refreshLock) {
			return videoSubcats;
		}
	}
	public void setVideoSubcats(HashMap<Integer, VideoCategorySub> videoSubcats) {
		this.videoSubcats = videoSubcats;
	}
	public HashMap<Integer, OneVideo> getVideos() {
		synchronized(refreshLock) {
			return videos;
		}
	}
	public void setVideos(HashMap<Integer, OneVideo> videos) {
		this.videos = videos;
	}
	public HashMap<Integer, VideoCategorySubImage> getVideoSubCatImages() {
		return videoSubCatImages;
	}
	public void setVideoSubCatImages(HashMap<Integer, VideoCategorySubImage> videoSubCatImages) {
		this.videoSubCatImages = videoSubCatImages;
	}
	public SslContextFactory getSslContextFactory() {
		return sslContextFactory;
	}
	public void setSslContextFactory(SslContextFactory sslContextFactory) {
		this.sslContextFactory = sslContextFactory;
	}
	public Registry<SchemeIOSessionStrategy> getOutgoingApiEncryption() {
		return outgoingApiEncryption;
	}
	public void setOutgoingApiEncryption(Registry<SchemeIOSessionStrategy> outgoingApiEncryption) {
		this.outgoingApiEncryption = outgoingApiEncryption;
	}
	public OneApiHost getMatomoHost() {
		return matomoHost;
	}
	public void setMatomoHost(OneApiHost matomoHost) {
		this.matomoHost = matomoHost;
	}
	public ArrayList<String> getTrackingCodes() {
		return trackingCodes;
	}
	public void setTrackingCodes(ArrayList<String> trackingCodes) {
		this.trackingCodes = trackingCodes;
	}
	public Object getLockCGBWs() {
		return lockCGBWs;
	}
	public Object getSoundLock() {
		return soundLock;
	}
	public void setSoundLock(Object soundLock) {
		this.soundLock = soundLock;
	}
	public HashMap<Integer, OneSound> getSounds() {
		return sounds;
	}
	public void setSounds(HashMap<Integer, OneSound> sounds) {
		this.sounds = sounds;
	}
}
