<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@ page session="true"%>
<body>
<jsp:useBean id='list' scope='session' class='broadcast.web.jsp.DummyListService' type="broadcast.web.jsp.ListService" />

<h1>JSP1.2 Beans: 1</h1>
Paul<br>
<c:forEach items="${names}" var="name">
  ${name}<br/>
</c:forEach>

</body>
</html>