#!/bin/bash

cd node_modules
fa="fontawesome-free"
echo checking $fa

if [ ! -L $fa ]; then
    ln -s \@fortawesome/fontawesome-free fontawesome-free
    echo created $fa
else
    echo $fa exists, not creating
fi

