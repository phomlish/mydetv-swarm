<html>
<%@ page session="true"%>
<body>
<jsp:useBean id='list' scope='session' class='com.mydetv.swarm.web.jsp.ListService' type="com.mydetv.swarm.web.jsp.ListService" />

<h1>JSP1.2 Beans: 1</h1>

<c:forEach items="${names}" var="name">
  ${name}<br/>
</c:forEach>

</body>
</html>