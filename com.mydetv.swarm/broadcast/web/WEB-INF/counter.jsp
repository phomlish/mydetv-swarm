<html>
<%@ page session="true"%>
<body>
<jsp:useBean id='counter' scope='session' class='broadcast.web.jsp.Counter' type="broadcast.web.jsp.Counter" />

<h1>JSP1.2 Beans: 1</h1>

Counter accessed <jsp:getProperty name="counter" property="counter"/> times.<br/>
Counter last accessed by <jsp:getProperty name="counter" property="last"/><br/>
<jsp:setProperty name="counter" property="last" value="<%= request.getRequestURI()%>"/>

<a href="counter.jsp">Goto counter.jsp</a>

<c:forEach items="${names}" var="name">
  ${name}<br/>
</c:forEach>


</body>
</html>