var email;
var loginform;
function loginInit() {
     
    $(document).ready(function(){
        console.log("loginInit");
        $('#loginform').submit(function(event) {
            console.log("signin submit");
            return false;
        });
        
        email = $('#email').val();
        console.log("initializing email:"+email);
        vemail();
        vpassword();
    });
}

function registerInit() {
    $(document).ready(function(){
        console.log("registerInit");
        $('#remail').text("");
        $('#rpassword').text("");
        $('#rconfirmpassword').text("");
        $('#rusername').text("");
        $('#rsq1').text("");
        $('#rsq2').text("");
        
        $('#real-email').val("");
        $('#real-password').val("");
        $('#real-confirm-password').val("");
        $('#real-username').val("");
        $('#sqa1').val("");
        $('#sqa2').val("");
    });
}

function profileInit() {
    $(document).ready(function(){
        doProfileInit();
    });
}
function securityInit() {
    $('#iptAddEmail').val("");
}
function reauthInit() {
    console.log("reauthInit");
    // none of this is working
    $('#newPassword0').bind('input', function() {
        alert('test');
    });
    $('#btnEnterPassword').prop("disabled", true); // not working
}
function vemail() {
    if(email===undefined) 
        email = $('#email').val();
    console.log("vemail:"+email);
    var pattern = /^(.*)+[@]+[^@]/;  // one @ in middle of string
    if(email.length===0) {
        $('#reqemail').text("Required").removeClass('redText').removeClass('greenText');
    }
   else  if(pattern.test(email)) {
        console.log("good");
        $('#reqemail').text("").removeClass('redText').removeClass('greenText');
    }
    else {
         console.log("bad");
         $('#reqemail').text("Invalid").addClass('redText').removeClass('greenText');
    }
    checkReady();
    email=undefined;
}

function vpassword() {
    console.log("vpassword");
    var password = $('#password').val();
    console.log("vpassword:"+password);
    if(password.length===0) {
        console.log("missing");
        $('#reqpassword').text("Required").removeClass('redText');
    }
    else if(password.length>=5) {
        console.log("good");
        $('#reqpassword').text("").removeClass('redText');
    }
    else {
         console.log("bad");
         $('#reqpassword').text("Invalid").addClass('redText');
    }
    checkReady();
}

function checkReady() {
    if($('#reqpassword').text()==="" && $('#remail').text()==="") {
        $('#signin').prop("disabled", false);
    }
    else {
        $('#signin').prop("disabled", true);
    }
}

function submitlogin() {
    
    var data = {
        email : $('#email').val(),
        password : $('#password').val()
    };
    console.log("submitlogin ",data);
    $('#signin').prop( "disabled", true );
    $("#spinnerDiv").addClass("fa fa-spinner fa-spin");
    
    $.ajax({
        url: "/api/login/signin",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:"+result);
            switch (result) {
                case "ok":
                case "already logged in":
                    window.location.replace("/");
                    break;
                case "changePassword":
                    bootbox.alert("You have signed in with a temporary password.  You must change it now.", function() {
                        window.location.replace("/auth/change-password");
                    });
                    break;
                default:
                    $('#loginMessage').text("Invalid, Please try again").addClass('redText').removeClass('greenText');
            }
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
            $('#signin').prop( "disabled", true );
            
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+" thrownError:"+thrownError+" textStatus:"+textStatus+" responseText:"+response);
            switch(xhr.status) {
                case 401:
                    switch(response) {
                        case "email/password":
                            $('#loginMessage').text("Invalid email or password, please try again");
                            $('#email').val("");
                            $('#password').val("");
                            break;
                        case "account locked":
                            $('#loginMessage').text("Your account is locked due to too many failed login attempts, please try later.");
                            break;
                        case "system locked":
                            $('#loginMessage').text("The login/registration system is currently locked, please try later.");
                            break;
                        case "banned":
                        	$('#loginMessage').text("Your account has been banned");
                        	break;
                        default:
                            $('#loginMessage').text("unhandled error: "+response);
                    }
                    $('#loginMessage').addClass('redText').removeClass('greenText');
                    break;
                case 403:
                    bootbox.alert("your session expired, please try again", function() {
                        window.location.replace("/auth"); 
                    });
                    break;
                default:
                    console.log("unhandled error xhr.status:"+xhr.status+" thrownError:"+thrownError);
                    $('#loginMessage').text("an error occured");
            }
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
        }
    });
    return false;
}

function registerSubmit() {
    var data = {
        email:      $('#real-email').val(),
        password:   $('#real-password').val(),
        username:   $('#real-username').val(),
        sqq1:        $('#sqq1').val(),
        sqa1:        $('#sqa1').val(),
        sqq2:        $('#sqq2').val(),
        sqa2:        $('#sqa2').val()
    };
    console.log("email:"+email);
    $("#spinnerDiv").addClass("fa fa-spinner fa-spin");
    $('#register').prop( "disabled", true );
    $('#registerMessage').html("");
    $.ajax({
        url: "/api/login/register",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "text",
        contentType: "application/json",
        success: function (result) {
            console.log("result:"+result);
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
            var msg = "Your registration was accepted.<br>";
            msg+="A link to verify your email has been sent.<br>";
            msg+="Please check your email and click that link now.<br>";
            msg+="If you don't see the email please check your spam/junk folder.<br>";
            msg+="The link will expire so please use it soon.<br>";
            bootbox.alert(msg, function() {
                window.location.replace("/");
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+" thrownError:"+thrownError);
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
            switch(xhr.status) {
                case 403:
                    alertForbidden403(xhr.status, "/auth/register");
                    break;
                case 401:
                    alertUnauthorized401(xhr.status);
                    break;
                default:
                    bootbox.alert("Your registration failed.  Support was notified.  Feel free to try again.");
                    registerInit();             
            }
        }
    });
}

function validateRegistrationForm() {
	$('#registerMessage').html("");
	$('#register').prop( "disabled", true );
	
    console.log("validateRegistrationForm");
    
    var username = $('#real-username').val();
    var email = $('#real-email').val();
    var confirmPassword=$('#real-confirm-password').val();
    var password = $('#real-password').val();   
    
    // create some lower case values for testing
    var lcUsername = username.toLowerCase();
    var lcEmail = email.toLowerCase();
    var lcPassword = password.toLowerCase();
    // answers will be sent to the server as lower case
    var sqa1 = $('#sqa1').val();
    if(sqa1.length > 0) sqa1 = sqa1.toLowerCase();
    var sqa2 = $('#sqa2').val();
    if(sqa2.length > 0) sqa2 = sqa2.toLowerCase();
    console.log("email:"+email+"username:"+username+"password:"+password+",confirmPassword:"+confirmPassword);
    
    var bad=0;
    
    // check the responses from the field entries
    if($('#remail').text()!=="" && $('#remail').text()!=='will validate') bad++;
    if($('#rpassword').text()!=='' && $('#rpassword').text()!=='strong') bad++;  
    if($('#rconfirmpassword').text()!=="" && $('#rconfirmpassword').text()!=='matched') bad++;
    if($('#rusername').text()!=="" && $('#rusername').text()!=='available') bad++;   
    if($('#rsq1').text()!=="" && $('#rsq1').text()!=='ok') bad++;   
    if($('#rsq2').text()!=="" && $('#rsq2').text()!=='ok') bad++;

    if(bad>0) {
        $('#register').prop( "disabled", true );
        console.log("validateRegistrationForm bad");
        $('#registerMessage').html("Please fix the errors.");
        return;
    }
    
    //test@armyspy.com
    
    // check username==password,email,sq
    console.log("username:"+username+",email:"+email+",index:"+username.indexOf(email));
    if(username!='') {   	
       	if(lcPassword!='' && (lcPassword.indexOf(lcUsername) != -1 || lcUsername.indexOf(lcEmail)!=-1)) {
       		$('#registerMessage').html("Password can't contain your username");
       		return;
        }     	
       	if(lcEmail!='' && (lcEmail.indexOf(lcUsername) != -1 || lcUsername.indexOf(lcEmail)!=-1)) {
       		$('#registerMessage').html("Username can't contain your email");
       		return;
       	}       	
        if(sqa1!='' && (sqa1.indexOf(lcUsername) != -1 || lcUsername.indexOf(sqa1)!=-1)) {
       		console.log("sqa1:"+sqa1);
       		$('#registerMessage').html("Security answer 1 can't contain your email, username, or password");
        	return;
       	}       
        if(sqa2!='' && (sqa2.indexOf(lcUsername) != -1 || lcUsername.indexOf(sqa2)!=-1)) {
            $('#registerMessage').html("Security answer 2 can't contain your email, username, or password");
            return;
        }
    }
    
    //check email==password,sq
    console.log("email:"+email+",lcPassword:"+lcPassword+",index:"+email.indexOf(lcPassword));
    if(email!='') {
    	if(lcPassword!='' && (lcEmail.indexOf(lcPassword) != -1 || lcUsername.indexOf(lcPassword)!=-1)) {
            $('#registerMessage').html("password can't contain your email");
            return;
        }
        if(sqa1!='' && (sqa1.indexOf(lcEmail) != -1 ||  lcEmail.indexOf(sqa1) != -1)) {
            $('#registerMessage').html("Security answer can't contain your email, username, or password");
            return;
        }
        else if(sqa2!='' && (sqa2.indexOf(lcEmail) != -1 || lcEmail.indexOf(sqa2) != -1)) {
            $('#registerMessage').html("Security answer can't contain your email, username, or password");
            return;
        }    	
    }
      
    // check password==sq
    if(password!='') {
        if(sqa1!='' && (sqa1.indexOf(lcPassword) != -1 ||  lcPassword.indexOf(sqa1) != -1)) {
            $('#registerMessage').html("Security answer can't contain your email, username, or password");
            return;
        }
        if(sqa2!='' && (sqa2.indexOf(lcPassword) != -1 || lcPassword.indexOf(sqa2) != -1)) {
            $('#registerMessage').html("Security answer can't contain your email, username, or password");
            return;
        }    	
    }
    
    if(username!=""
    && email!=""
    && password!=""
    && confirmPassword!=""
    && sqa1!=""
    && sqa2!="") {
    	$('#register').prop( "disabled", false );
        console.log("validateRegistrationForm good");
    }
    
}

// using this for email causes 'is slowing down your browser'
// $('#remail').text("will validate").removeClass('redText').addClass('greenText');
function vemailreg() {
    email = $('#real-email').val();
    console.log("vemailreg '"+email+"'");
    if(email=='') {
    	$('#remail').text("").removeClass('redText').addClass('greenText');
    	validateRegistrationForm();
    	return;
    }
    var pattern = /^(.*)+[@]+[^@]/;  // one @ in middle of string
    if(pattern.test(email)) {
        console.log("email good");
        $('#remail').text("will validate").removeClass('redText').addClass('greenText');
    }
    else {
         console.log("email bad");
         $('#remail').text("Invalid").addClass('redText');
    }

    validateRegistrationForm();
}

function vpasswordreg() {
    console.log("vpasswordreg");
    var password = $('#real-password').val();
    console.log("vpassword:"+password);
    if(password=='') {
    	$('#rpassword').text("").removeClass('redText').addClass('greenText');
    	validateRegistrationForm();
    	return;
    }
    var data = {
        password : password
    };
    $.ajax({
        url: "/api/login/getPasswordStrength",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("ok:"+result.score+",feedback:"+result.feedback+",sug:"+result.suggestions);
            if(result.score===3 || result.score===4) {
                $('#rpassword').text("strong").removeClass('redText').addClass('greenText');          
                validateRegistrationForm();
                return;
            }

            var rpassword='Weak Password';
            if(result.feedback.length>0) {
                rpassword+='\n'+result.feedback;
            }
            if(result.suggestions.length>0) {
                rpassword+="\n"+result.suggestions;
            }
            $('#rpassword').text(rpassword).removeClass('greenText').addClass('redText');
            $('#rconfirmpassword').text("");
            $('#register').prop( "disabled", true );
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+", thrownError:"+thrownError);
            $('#rpassword').text("An error occured, please try again.");
            $('#register').prop( "disabled", true );
        }
    });
}

function vconfirmpasswordreg() {
    var password = $('#real-password').val();
    var confirmpassword = $('#real-confirm-password').val();
    if(confirmpassword=='') {
    	$('#rconfirmpassword').text("").removeClass('redText').addClass('greenText');
    	validateRegistrationForm();
    	return;
    }
    if(password != confirmpassword) {
        $('#rconfirmpassword').text("does not match").removeClass('greenText').addClass('redText');    
    }
    else {
        $('#rconfirmpassword').text("matched").removeClass('redText').addClass('greenText');
        validateRegistrationForm();
    }  
}
function vusernamereg() {
    var username = $('#real-username').val();
    console.log("username:"+username+" "+username.length+",lc:"+username.toLowerCase());
    if(username=='') {
    	$('#rusername').text("").removeClass('redText').addClass('greenText');
    	validateRegistrationForm();
    	return;
    }
    if(username.length<3) {
        $('#rvusername').text("must be at least 3 characters").addClass('redText');
        $('#register').prop( "disabled", true);
        return;
    }
    
    var data = {
        username : username
    };
    
    $.ajax({
        url: "/api/login/isUsernameAvailable",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "text",
        contentType: "application/json",
        success: function (result) {
            console.log("test:"+result);
            if(result=="ok") {
                $('#rusername').text("available").removeClass('redText').addClass('greenText');
                validateRegistrationForm();
                return;
            }
            $('#register').prop( "disabled", true );
            $('#rusername').text("unavailable").removeClass('greenText').addClass('redText');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+"thrownError:"+thrownError);
            $('#register').prop( "disabled", true );
            $('#rusername').text("an error occured, please try again.").removeClass('greenText').addClass('redText');
        }
    }); 
}

// from register new user page
function vusersq(i) {
    console.log("i:"+i.toString());
    var answer = $('#sqa'+i).val();
    console.log("sqa:"+answer);
    if(answer=='') {
    	$('#rsq'+i).text("").removeClass('redText').addClass('greenText');
    	validateRegistrationForm();
    	return;
    }
    if(answer.length<4) {
        $('#rsq'+i).text("answer too short").removeClass('greenText').addClass('redText');
    }
    else {
        $('#rsq'+i).text("ok").removeClass('redText').addClass('greenText');
        validateRegistrationForm();
    }
}
// from security page - editing questions
function vsqa() {
    $('#updateSq').prop( "disabled", true );
    $('#sqMessage').text("");
    var answer1 = $('#sqa1').val();
    var answer2 = $('#sqa2').val();
    if(answer1.length===0 || answer2.length===0) {
        //do nothing
    }
    else if(answer1.length<4 || answer2.length<4) {
        $('#sqMessage').text("Security Question answers must be 4 characters or more.");
    }
    else {
        $('#rsq'+i).text("ok").removeClass('redText').addClass('greenText');
        $('#updateSq').prop("disabled", false);
    }
}
function fnUpdateSq() {
    var data = {
        sqq1 : $('#sqq1').val(),
        sqq2 : $('#sqq2').val(),
        sqa1 : $('#sqa1').val(),
        sqa2 : $('#sqa2').val(),
    };
    $.ajax({
        url: "/api/user-security/updateSQ",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "text",
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("ok");
                    break;
                default:
                    
            }
            // success finally
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    boobox.alert("You are not logged in.  Please go home, login, and try again");
                    break;
                case 403:
                    boobox.alert("Your session expired.  Please go home, login, and try again");
                    break;
                default:
                    bootbox.alert("An error occured");
            }
        }
    });
}

function changedPassword() {
    console.log("changedPassword");
    var password0 = $('#newPassword0').val();
    var password1 = $('#newPassword1').val();
    if(password0==="" || password1==="")
        return;
    if(password0 != password1) {
        $('#pMessage').text("Passwords don't match");
        $('#updatePassword').prop( "disabled", true );
        return;
    }
    console.log("changedPassword:",password0,password1);
    var data = {
        password : password0
    };
    $.ajax({
        url: "/api/login/getPasswordStrength",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("ok:"+result.score+",feedback:"+result.feedback+",sug:"+result.suggestions);
            if(result.score===3 || result.score===4) {
                $('#pMessage').text("");
                $('#updatePassword').prop( "disabled", false);
                return;
            }
         
            var pMessage='Weak Password';
            if(result.feedback.length>0) {
                pMessage+='\n'+result.feedback;
            }
            if(result.suggestions.length>0) {
                pMessage+="\n"+result.suggestions;
            }
            $('#pMessage').text(pMessage);
            $('#updatePassword').prop( "disabled", true );
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+", thrownError:"+thrownError);
            $('#pMessage').text("An error occured, please try again.");
            $('#updatePassword').prop( "disabled", true );
        }
    });
}

function doUpdatePassword() {
    $('#pMessage').text("");
    var password0 = $('#newPassword0').val();
    var password1 = $('#newPassword1').val();
    console.log("updatePassword:",password0,password1);
    var data = {
        password : password0
    };
    $("#spinnerDiv").addClass("fa fa-spinner fa-spin");
    $.ajax({
        url: "/api/user-security/updatePassword",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("ok:",result);
            if(result=='ok') {
                bootbox.alert("your password was changed");              
                $('#newPassword0').val("");
                $('#newPassword1').val("");
            }
            else 
                $('#pMessage').text(result);

            $('#updatePassword').prop( "disabled", true );
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+", thrownError:"+thrownError);
            $('#pMessage').text("An error occured, please try again.");
            $('#updatePassword').prop( "disabled", true );
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
        }
    });
}

function fnDeleteEmail(idEmail) {
    console.log("deleting email "+idEmail);
    var emailAddress = $('#emailAddress-'+idEmail).text();
    bootbox.confirm("Are you sure you want to delete "+emailAddress, function(result) {

        if(result) {
            $.ajax({
                url: "/api/user-security/deleteEmail?idEmail="+idEmail,
                type: "GET",
                success: function (result) {
                    console.log("result:",result);
                    replaceEmailDiv();
                },
                error: function (xhr, textStatus, thrownError) {
                    var response = xhr.responseText;
                    console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
                    switch(xhr.status) {
                        case 401:
                            alertUnauthorized401(response);
                            break;
                        case 403:
                            alertForbidden403(response);
                            break;
                        default:
                            alertServerError500();
                        }
                    }
            });
        }
    });
}

function fnDeleteAccount() {
    console.log("deleting account");
    var usure="<center>Are you sure you want to permanently delete your account?<br>";
    usure+="This is a non reversable action.<br>";
    usure+="All personally identifiable information will be removed from our databases.<br><br>";
    usure+="Boo will miss you<br><br>";
    usure+="<img src='/s/img/Boo.jpg'><br>";
    bootbox.confirm(usure, function(result) {

        if(result) {
            $.ajax({
                url: "/api/user-security/deleteAccount",
                type: "GET",
                success: function (result) {
                    bootbox("Your account has been purged.  G'bye", function() {
                        window.location.replace("/auth"); 
                    });
                },
                error: function (xhr, textStatus, thrownError) {
                    var response = xhr.responseText;
                    console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
                    switch(xhr.status) {
                        case 401:
                            alertUnauthorized401(response);
                            break;
                        case 403:
                            alertForbidden403(response);
                            break;
                        default:
                            alertServerError500();
                    }
                    // error finally
                }
            });
        } // bootbox usure
    });
  
}

var myCroppie;
var origFilename;
var orig_bday, orig_bmonth, orig_byear, orig_tz, orig_profilelink;
function doProfileInit() {
    var fileSelect = document.getElementById("fileSelect");
    var fileElem = document.getElementById("fileElem");
    var fileList = document.getElementById("fileList");
    
    orig_bday = $('#bday').val();
    orig_bmonth = $('#bmonth').val();
    orig_byear = $('#byear').val();
    orig_tz = $('#tz').val();
    orig_profilelink = $('#profilelink').val();
    //console.log("profilelink:"+orig_profilelink);
    
    fileSelect.addEventListener("click", function (e) {
        if (fileElem) {
            fileElem.click();
        }
        e.preventDefault(); // prevent navigation to "#"
    }, false);
}

function updateUserProfile() {
    $('#updateProfile').attr('disabled', true);
    var data = {
        bmonth : $('#bmonth').val(),
        bday : $('#bday').val(),
        byear : $('#byear').val(),
        profilelink : $('#profilelink').val(),
        tz : $('#tz').val()      
    };
    
    $.ajax({
        url: "/api/profile/update",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            bootbox.alert("Your profile was updated.");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    boobox.alert("You are not logged in.  Please go home, login, and try again");
                    break;
                case 403:
                    boobox.alert("Your session expired.  Please go home, login, and try again");
                    break;
                default:
                    bootbox.alert("An error occured");
            }
        }
    });
}
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

var saved_bday="", saved_bmonth="", saved_byear="", saved_tz="", saved_link="";
function changedProfile() {
    var newProfileMessage="";
    
    console.log("changedProfile");
    
    var bday = $('#bday').val();
    var bmonth = $('#bmonth').val();
    var byear = $('#byear').val();
    var tz = $('#tz').val();
    console.log("bday pieces:"+bmonth+"|"+bday+"|"+byear);
    var myprofilelink = document.getElementById("profilelink").value;
    var errors=0;
    var changed=0;
    // if everything is same as orig g'bye
    if(bday==orig_bday && bmonth==orig_bmonth && byear==orig_byear && tz==orig_tz && myprofilelink==orig_profilelink) {
        console.log("nothing changed");
        $('#updateProfile').attr('disabled', true);
        return;
    }
    var sday = pad(bday,2);
    var smonth = pad(bmonth,2);
    var sdate = byear+"-"+smonth+"-"+sday; //+" 00:00:00 GMT";
    var cdate = Date.parse(sdate);
    if(bday==="" || bmonth==="" || byear==="") {
        console.log("not a complete date");
    }
    else if(isNaN(cdate)) {
        console.log("invalid date "+sdate+":"+smonth+"|"+sday+"|"+byear);
        $('#bMessage').text("invalid");
        errors++;
    }
    else if(!(bday==orig_bday && bmonth==orig_bmonth && byear==orig_byear)) {
        $('#bMessage').text("");
       changed++;
    }

    if(myprofilelink!=orig_profilelink) {
        var lc_profilelink = myprofilelink.toLowerCase();
        var c1 = lc_profilelink.indexOf('http://');
        var c2 = lc_profilelink.indexOf('https://');
        console.log("c1:"+c1+",c2:"+c2);
        if(lc_profilelink.indexOf('http://')==-1 && lc_profilelink.indexOf('https://')==-1) {
            $('#lMessage').text("invalid");
            errors++;
        }
        else {
            $('#lMessage').text("");
            changed++;
        }
    }
    
    if(tz!=orig_tz) {
        changed++;
    }
    
    if(errors>0) {
        $('#updateProfile').attr('disabled', true);
        $('#profileMessage').text("there are errors in your selections");
        return;
    }
    
    if(changed>0) {
        $('#updateProfile').attr('disabled', false);
        $('#profileMessage').text("");
    }
    else 
        $('#updateProfile').attr('disabled', true);
    
    saved_bday = $('#bday').val();
    saved_bmonth = $('#bmonth').val();
    saved_byear = $('#byear').val();
    saved_tz = $('#tz').val();
    saved_link = $('#profilelink').val();
}

function changeTheme() {
	var theme = $('#theme').val();
	console.log("changing theme to "+theme);
    var data = {
        theme : theme
    };
    $.ajax({
        url: "/api/profile/changeTheme",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "text",
        success: function (result) {
            console.log("result:",result);  
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            bootbox.alert("error");
            switch(xhr.status) {
            case 401:  // unauthorized
                alertUnauthorized401(response);
                break;
            case 403: // forbidden
                bootbox.alert("Your session expired.  Please sign in and try again.");
                break;
            default:
                console.log("unhandled error xhr.status:"+xhr.status+" thrownError:"+thrownError);
                bootbox.alert("an error occured");
            }
        }
    });
	
}

function clickFile() {
    fileElem.click();
}

function checkImage() {

    $('#uploadWarningSize').removeClass('redText');
    $('#uploadWarningMax').removeClass('redText');
    $('#uploadWarningType').removeClass('redText');

    var files = $('#fileElem');
    if(files===undefined)
        return;
    if(files[0] === undefined) {
        console.log("no file");
        return;
    }
    //var file = files[0];
    var file = document.getElementById('fileElem').files[0];
    
    var supportedFormats = ['image/jpg','image/jpeg','image/gif','image/png'];
    var errors=0;
    //
    
    //var file = files[0];
    console.log("size:"+file.size+",type:"+file.type);
    if (file.size > 1024*1024*5) {
        $('#uploadWarningSize').addClass('redText');
        errors++;
    }
    if (0 > supportedFormats.indexOf(file.type)) {
        $('#uploadWarningType').addClass('redText');
        errors++;
    }
    if(errors>0) {
        $('#uploadImageButton').prop( "disabled", true);
        return;
    }
    console.log("image "+file.name+" seems fine");
    $('#uploadImageButton').prop( "disabled", false);
    getImageFromUserFilesystem(file);
}

function getImageFromUserFilesystem(file) {
    
    var img=new Image();
    img.id='imageBlob';
    img.src = URL.createObjectURL(file);

    //img.src = window.webkitURL.createObjectURL(file);
    img.file=file;
    console.log("check1");
    //img.onloadend = function() {
    //    cropImage(img, img.file);      
    //  };
      img.onload = function() {
        cropImage(img, img.file); 
      };

    console.log("set");
}

function cropImage(img, file) {
    origFilename=file.name;
    console.log("open dialog for "+origFilename);  
    var opt = {
        autoOpen: false,
        title: 'Edit Image',
        width: 350,
        height: 400,
        dialogClass: "no-close",
        buttons: [
            {
                text: "Accept",
                click: function() {
                    acceptImage();
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Rotate Left",
                click: function() {
                    rotateImage(90);
                }
            },
            {
                text: "Rotate Right",
                click: function() {
                    rotateImage(-90);
                }
            }
        ]
    };
    var imageDialog = $("#imageDialog").dialog(opt);
    imageDialog.dialog("open");
    
    console.log("crop");
    
    var croppieDiv=$('#croppieDiv');
    if(!croppieDiv.data('croppie')) {
        myCroppie = croppieDiv.croppie({
            viewport: { width: 192, height: 192 },
            boundary: { width: 200, height: 200 },
            exif:true,
            enableOrientation: true
        });
    }
    
    myCroppie.croppie('bind', {
        url: img.src
    });
}
function rotateImage(amount) {
    myCroppie.croppie('rotate', amount);
}
function acceptImage() {
    console.log("acceptImage");
    var data = myCroppie.get();
        myCroppie.croppie('result', {
            type: 'blob',
            size: 'viewport',
            format: 'png'
            
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            console.log("croppie result ",resp);
            sendFile(resp);
        });
}
var imageData;
var myFile;
function sendFile(file) {
    myFile=file;
    var reader = new FileReader();
    
    var xhr = new XMLHttpRequest();
    this.xhr = xhr;
    xhr.open("POST", "/api/profile/uploadProfileImage", true);
    xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');  
    xhr.addEventListener("load", replaceImagesDiv, false);
    xhr.addEventListener("error", uploadFailed, false);
    xhr.addEventListener("abort", uploadCanceled, false);
    console.log("sendFile");
    reader.onload = function(evt) {
        imageData = evt.target.result;
        var formData = new FormData();
        formData.append("origFilename", origFilename);
        formData.append("imageData", file);
        console.log("check3 "+imageData.length);
        xhr.send(formData);
    };
    
    reader.readAsBinaryString(file); 
}

function replaceImagesDiv() {
    $.ajax({
        url: "/api/profile/geImagesDiv",
        type: "GET",
        contentType: "text/html",
        success: function (result) {
            console.log("result:",result);
            $('#imagesDiv').html(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    bootbox.alert("Not logged in, please go home and sart over.");
                    break;
                case 403:
                    bootbox.alert("Your session expired, please refresh this page");
                    break;
                default:
                    console.log("unhandled error xhr.status:"+xhr.status+" thrownError:"+thrownError);
                    bootbox.alert("An error occured");
            }
        }
    });  
}
function uploadFailed(evt) {
  alert("There was an error attempting to upload the file.");
}

function uploadCanceled(evt) {
  alert("The upload has been canceled by the user or the browser dropped the connection.");
}

function editProfileImage(obj) {
    console.log("editProfileImage:",obj);
    var id=$(obj).attr('id');
    console.log("id:",id);
    var array = id.split('-');
    var type = array[0];
    var wid=array[2];
    var i = array[1];
    console.log("id:"+id+",i:"+i+",wid:"+wid);
    if(type=='dpi')
        deleteProfileImage(i,wid);
    else if(type=='spi')
        selectProfileImage(i,wid);
    else
        console.log("didn't recognize type:"+type);
}

function deleteProfileImage(i,wid) {
    console.log("deleteProfileImage i:"+i+",wid:"+wid);
    var data = {
        "i":i,
        "imageId":wid
    };
    $.ajax({
        url: "/api/profile/deleteProfileImage",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "text",
        contentType: "application/json",
        success: function (result) {
            if(result=="selected image") {
                bootbox.alert("Can't delete default image.  Please select a new default image and then try the delete again.");
                return;
            }
            replaceImagesDiv();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            switch(xhr.status) {
                case 401:
                    boobox.alert("You are not logged in.  Please go home, login, and try again");
                    break;
                case 403:
                    boobox.alert("Your session expired.  Please go home, login, and try again");
                    break;
                default:
                    bootbox.alert("An error occured");
            }
        }
    });
}
function createAvailableProfileImage() {
    var rv="";
    rv+="<div class='col-sm-2 imageAvailable' id='personImgDiv-9'>";
    rv+="<a class='dropdown-toggle profilePictureDropdown' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
    rv+="<img class='person' src='/s/people/blank-profile-picture.png'>";
    rv+="</a>";
    rv+="<div class='dropdown-menu' aria-labelledby='profilePictureDropdown'>";
    rv+="<a class='dropdown-item' href='javascript:;' onclick='clickFile();'>Upload</a>";
    rv+="</div>";		
    rv+="</div>";
    return rv;
}

function selectProfileImage(i,wid) {
    console.log("selectProfileImage wid:"+wid+",i:"+i);
    var data = {
        "imageId":wid
    };
    $.ajax({
        url: "/api/profile/selectProfileImage",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (result) {          
            console.log("image seleced ",result);
            // make the profile image this new one
            var target = '#personImg-'+result.imageId;
            //console.log("target "+target);
            $('#profilePicture').attr('src', $(target).attr('src'));
            
            // make all the images borderless
            $(".pirow img").removeClass('personSelected');
            // give newly selected image the border
            $(target).addClass('personSelected');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+" thrownError:"+thrownError);
            bootbox.alert("an error occured "+thrownError);
        }
    });
}

function addEmailCheck() {  
    $('mAddEmail').text("");
    var email = $('#iptAddEmail').val();
    console.log("add email "+email);
    var pattern = /^(.*)+[@]+[^@]/;  // one @ in middle of string
    if(email.length===0) {
        $('#btnAddEmail').prop( "disabled", true );
        $('#mAddEmail').text("");
    }
   else  if(pattern.test(email)) {
        console.log("good");
        $('#btnAddEmail').prop( "disabled", false );
        $('#mAddEmail').text("");
    }
    else {
         console.log("bad");
         $('#btnAddEmail').prop( "disabled", true );
         $('#mAddEmail').text("invalid email");
    }
}

function fnAddEmail() {
    $('#btnAddEmail').prop( "disabled", true );
    var data = {
        email : $('#iptAddEmail').val(),
    };
    var mAddEmail = $('mAddEmail');
    $.ajax({
        url: "/api/user-security/addemail",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "text",
        success: function (result) {
            //console.log("result:",result);
            switch (result) {
                case "invalid":
                    mAddEmail.text("invalid");                
                    break;
                default:
                    mAddEmail.text(result);
                    replaceEmailDiv();
            }           
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            mAddEmail.text("an error occured");
            bootbox.alert("An error occured.\nPlease refresh the page and try again.");
            switch(xhr.status) {
            case 401:  // unauthorized
                alertUnauthorized401(response);
                break;
            case 403: // forbidden
                window.location.replace("/");
                bootbox.alert("Your session expired.  Please sign in and try again.");
                break;
            default:
                console.log("unhandled error xhr.status:"+xhr.status+" thrownError:"+thrownError);
                bootbox.alert("an error occured");
            }
        }
    });
}

function replaceEmailDiv() {
    $.ajax({
        url: "/api/user-security/getemaildiv",
        type: "GET",
        success: function (result) {
            //console.log("result:",result);
            $('#emailsDiv').html(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus);
            bootbox.alert("An error occured");
        }
    });
}

function showRegisterHelp() {
    bootbox.alert($('#registerHelp').html());
}
function showPasswordHelp() {
    bootbox.alert($('#passwordHelp').html());
}
function showProfileHelp() {
    bootbox.alert($('#profileHelp').html());
}
function showImagesHelp() {
    bootbox.alert($('#imagesHelp').html());
}
function showEmailHelp() {
    bootbox.alert($('#emailHelp').html());
}
function showSqHelp() {
    bootbox.alert($('#sqHelp').html());
}
function showDeleteAccountHelp() {
    bootbox.alert($('#deleteAccountHelp').html());
}
function showForgotPasswordHelp() {
    bootbox.alert($('#forgotPasswordHelp').html());
}
function reenterPassword() {
    console.log("reenterPassword");
    var password = $('#newPassword0').val();
    //console.log("password:"+password);
    if(password.length===0) {
        console.log("missing");
        $('#btnEnterPassword').prop("disabled", true);
    }
    else if(password.length>=5) {
        console.log("good");
        $('#btnEnterPassword').prop("disabled", false);
    }
    else {
         console.log("bad");
         $('#btnEnterPassword').prop("disabled", true);
    }
}

function doReenterPassword() {
    $("#spinnerDiv").addClass("fa fa-spinner fa-spin");
    console.log("doReenterPassword");
    var data = {
        password : $('#newPassword0').val(),
    };
    $.ajax({
        url: "/api/user-security/reauth",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "text",
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("ok");
                    window.location.replace("/auth/security"); 
                    break;
                default:
                    bootbox.alert("Your password was invalid");
            }
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus);
            bootbox.alert("An error occured.");
            $("#spinnerDiv").removeClass("fa fa-spinner fa-spin");
        }
    });
}

// this sends a new email validation link to the logged in user's email that they logged in with
function sendnewcode() {
    var url = "/api/user-security/sendNewEmailVerificationlink";
    sendNewEmailVerificationlink(url);
}
function fnSendNewCode(emailId) {
    var url="/api/user-security/sendNewEmailVerificationlink?emailId="+emailId;
    sendNewEmailVerificationlink(url);
}

function sendNewEmailVerificationlink(url) {
    
    $.ajax({
        url: url,
        type: "GET",
        dataType: "text",
        success: function (result) {
            console.log("result:"+result);
            switch (result) {
                case "ok":
                    var msg = "A new link to verify your email has been sent.<br>";
                    msg+="Please check your email and click that link now.<br>";
                    msg+="If you don't see the email please check your spam/junk folder.<br>";
                    msg+="The link will expire so please use it soon.<br>";
                    bootbox.alert(msg);
                    break;
                default:
                    console.log("unrecognized result:",result);
                    bootbox.alert("an error occured");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+" thrownError:"+thrownError+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:  // unauthorized
                    alertUnauthorized401(response);
                    break;
                case 403: // forbidden
                    alertForbidden403(response, "/");
                    break;
                default:
                    alertServerError500();
            }
        }
    });
}

function forgotPasswordClearEmail() {
    $('#real-email').val("");
}

function vEmailForgotEmail() {
    var email = $('#real-email').val();
    
    var pattern = /^(.*)+[@]+[^@]/;  // one @ in middle of string
    if(email.length===0) {
        $('#sendFP').prop("disabled", true);
    }
   else  if(pattern.test(email)) {
        $('#sendFP').prop("disabled", false);
    }
    else {
         $('#sendFP').prop("disabled", true);
    }
}

function requestForgotPassword() {
    var data = {
        email : $('#real-email').val(),
    };
    $.ajax({
        url: "/api/user-security/sendFPC",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "text",
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            var msg="Please check your email for instructions for the next step.<br/><br/>";
            msg+="Your password reset request will expire soon <br/>so please proceed quickly.<br/>"
            bootbox.alert(msg,function() {
            	window.location.replace("/auth");
            });
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            bootbox("An error occured.  Please try again.", function() {
                window.location.replace("/auth/forgot-password");
            });
        }
    });
}

function fnAnswer() {
    if($('#sqa1').val().length>0 && $('#sqa2').val().length>0)
        $('#sendAnswers').prop("disabled", false);
     else
         $('#sendAnswers').prop("disabled", true);
}

function fnSendAnswers() {
    
    var data = {
        e : $('#e').text(),
        w : $('#w').text(),
        c : $('#c').text(),
        u : $('#u').text(),
        sqa1: $('#sqa1').val(),
        sqa2: $('#sqa2').val(),
    };

    $.ajax({
        url: "/api/user-security/sendAnswers",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "text",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "expired":
                    bootbox.alert("Your password request has expired. Please try again.", function() {
                        window.location.replace("/auth/forgot-password");
                    });
                    break;
                case "bad answers":
                    bootbox.alert("Your security answers don't match what we have on file.<br>Please try again.  Too many failed attempts will lock your account.");
                    break;
                case "system locked":
                case "account locked":
                    bootbox.alert("We are unable to process your request.<br>Please try again later. "+result);
                    break;
                case "bad system locked":
                case "bad account locked":
                    bootbox.alert("Your security answers don't match what we have on file.<br>Please try again. "+resultsubstring(4));
                    break;
                case "already used":
                    bootbox.alert("Can't find that password reset request.<br> Perhaps you already used it.  Please request a new password reset.");
                    break;             
                case "not latest":
                    bootbox.alert("It looks like that is not the most recent<br>password reset request.<br>Please request a new password reset or check your email<br>for a newer message containing the latest request.");
                    break;
                case "expired":
                    bootbox.alert("The password reset request has expired.<br>Please request a new password reset.");
                    break;
                case "validation exception":
                    bootbox.alert("An error occured and was reported to support.");
                    break;
                default:
                    console.log("new password:"+result);
                    var msg="Your temporary password is:<div class='blackText center'>"+result+"</div><br>";
                    msg+="Please login using this temporary password at which time you<br> will be forced to change it.<br>";
                    msg+="Please remember this temporary password as you will <br>need to use it twice before the process is finished.";
                    bootbox.alert(msg);
            }
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus);
            switch(xhr.status) {
                case 403:
                    bootbox.alert("Your session expired, please refresh this page and ry again");
                    break;
                default:
                    bootbox.confirm({
                        message: "An error occured and was reported to support.",
                        buttons: {
                            confirm: {
                                label: '<i class="fa fa-check"></i> Send New Code'
                            },
                            cancel: {
                                label: '<i class="fa fa-times"></i> Try the Code Again'
                            }
                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                            if(result)
                                window.location.replace("/auth/forgot-password");
                            else
                                console.log("cancelled, ok fine sit here idle");
                        }
                    });
            }
        }
    });
}

function fnChangePasswordSubmit() {
    var data = {
        currentPassword : $('#currentPassword').val(),
        newPassword : $('#newPassword').val()
    };
    $.ajax({
        url: "/api/user-security/changeTemporaryPassword",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "text",
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("ok");
                    bootbox.alert("You changed your password. Please login again using the new password.", function() {
                        window.location.replace("/auth/forgot-password");
                    });
                    break;
                default:
                    bootbox.alert("Change password failed. Please try again. "+result);
                    fnChangePasswordInit();
            }
            // success finally
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alertUnauthorized401(response);
                    break;
                case 403:
                    alertForbidden403(response, "/");
                    break;
                default:
                    alertServerError500();
            }
            // error finally
        }
    });
}

var rememberNewPassword;
var rememberConfirmPassword;
function fnChangePasswordInit() {
    $('#rcurrentpassword').text("");
    $('#rnewpassword').text("");
    $('#rconfirmpassword').text("");
    $('#rcurrentpassword').val("");
    $('#newPassword').val("");
    $('#confirmPassword').val("");
    $('#changePasswordMessage').text("");
    rememberNewPassword="";
    rememberConfirmPassword="";
}

function vcurpasscp() {
    if($('#currentPassword').val().length<4) {
        $('#rcurrentpassword').text("bad").addClass('redText').removeClass('greenText');
    }
    else {
        $('#rcurrentpassword').text("ok").addClass('greenText').removeClass('redText');
        isChangePasswordComplete();
    }   
}
function vpasscp() {
    $('#changePasswordMessage').text("");
    
    var newPassword = $('#newPassword').val();
    var confirmPassword = $('#confirmPassword').val();
    
    // should not happen
    if(newPassword==rememberNewPassword && confirmPassword==rememberConfirmPassword) {
        console.log("huh?");
        return;
    }
    
    if(confirmPassword != rememberConfirmPassword) {
        rememberConfirmPassword=confirmPassword;
        if(confirmPassword != newPassword) {
            $('#rconfirmpassword').text("New and Confirm passwords don't match").addClass('redText').removeClass('greenText');          
            return;
        }
        $('#rconfirmpassword').text("ok").addClass('greenText').removeClass('redText');
        isChangePasswordComplete();
        return;
    }
    
    if(newPassword===rememberNewPassword) {
        console.log("double huh?");
        return;
    }
    rememberNewPassword=newPassword;
    
    var data = {
        password : newPassword
    };
    
    $.ajax({
        url: "/api/login/getPasswordStrength",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("ok:"+result.score+",feedback:"+result.feedback+",sug:"+result.suggestions);
            if(result.score===3 || result.score===4) {
                $('#changePasswordMessage').text("");
                $('#rnewpassword').text("ok").addClass('greenText').removeClass('redText');
                if(confirmPassword!=newPassword)
                    $('#rconfirmpassword').text("");
                isChangePasswordComplete();
                return;
            }
         
            var pMessage='Weak Password';
            if(result.feedback.length>0) {
                pMessage+='\n'+result.feedback;
            }
            if(result.suggestions.length>0) {
                pMessage+="\n"+result.suggestions;
            }
            $('#changePasswordMessage').text(pMessage);
            $('#rnewpassword').text("weak").addClass('redText').removeClass('greenText');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("xhr.status:"+xhr.status+", thrownError:"+thrownError);
            $('#rnewpassword').text("error").addClass('redText').removeClass('greenText');
        }
    });
}

function isChangePasswordComplete() {
    if($('#rcurrentpassword').text() == 'ok' && $('#rnewpassword').text() == 'ok' && $('#rconfirmpassword').text() == 'ok') {
        $('#btnChangePassword').prop("disabled", false);
    }
    else {
        $('#btnChangePassword').prop("disabled", true);       
    }
}
function alertServerError500() {
    bootbox.alert("An error has occured.  Support has been notified");
}
function alertForbidden403(response, redirect) {
    if(redirect!== undefined && redirect!==null && redirect!=="") {
        bootbox.alert("Your session expired.  Please try again.", function() {
            window.location.replace(redirect);
        });
    }
    else
        bootbox.alert("Your session expired.  Please refresh this page and try again");
}
function alertUnauthorized401(response) {
    if(response=="not auth") {
        bootbox.alert("You are not signed in.  Please sign in and try again.", function() {
            window.location.replace("/");
        });
    }
    else if(response=="system locked") {
        bootbox.alert("The login/registration system is currently locked.", function() {
            window.location.replace("/");
        });
    }
    else if(response=="account locked") {
        bootbox.alert("Your account is currently locked.", function() {
            window.location.replace("/");
        });
    }
    else if(response=="already verified") {
        bootbox.alert("You have already verified that email.", function() {
            window.location.replace("/");
        });
    }
    else if(response=="not reauth") {
        bootbox.alert("You need to reauthenticate.  Please refresh this page and try again.");
    }
    else if(response=="already logged in") {
        bootbox.alert("You are already logged in.", function() {
            window.location.replace("/");
        });
    }
    else {
        bootbox.alert("An error occured.");
    }
}

function ajaxExample() {
    var data = {
        password : $('#newPassword0').val(),
    };
    $.ajax({
        url: "/api/login/blah",
        type: "POST",
        data: JSON.stringify(data),
        //dataType: "text",
        //dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("ok");
                    break;
                default:
                    
            }
            // success finally
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alertUnauthorized401(response);
                    break;
                case 403:
                    alertForbidden403(response, "");
                    break;
                default:
                    alertServerError500();
            }
            // error finally
        }
    });
}

