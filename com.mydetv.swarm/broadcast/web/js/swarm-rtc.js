//https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
//https://stackoverflow.com/questions/27768320/html5-video-double-click-to-go-full-screen

var myRTCPeerConnectionsMap;
var pplMap;
// this is our media stream that we connect to videoElement and send to downstreams
var remoteMediaStream; // = new MediaStream();
var remoteVideo;
var remoteVideoSender;
// and if this is remote we need a local MediaStream
var localMediaStream;
var localVideo;
var localVideoSender;

$(document).ready(function() {
    console.log("document ready");
    remoteVideo = document.getElementById('remoteVideo');
    
    setupP2P();
    //setupVideoElementListeners();
    console.log("getting datafile");
    getDataFile(function() {
        console.log("got datafile "+datafileDownloadTime);
        initWebsocket();
    });
    
    console.log("creating new myRTCPeerConnectionsMap and pplMap");
    myRTCPeerConnectionsMap = new Map();
    pplMap = new Map();
    $('#testblue').addClass('fa fa-spinner fa-spin');    
});

function setupVideoElementListeners(name, videoElement) {

    videoElement.addEventListener("ended", function() {
        console.log(name+" ended");
        if(name=='remote') {
            $('#remoteVideo').addClass('d-none');
            showRemoteDancer();
        }
        else if(name=='local') {
            $('#localVideo').addClass('d-none');
            showLocalDancer();
        }
        });
    videoElement.addEventListener("error", function() { console.log(name+" error");} );
    
    // verified firefox and chrome friendly
    videoElement.addEventListener("loadeddata", function() { console.log(name+" loadeddata");} );
    videoElement.addEventListener("canplay", 	function() { console.log(name+" canplay");} );
    videoElement.addEventListener("loadstart", 	function() { console.log(name+" loadstart");} );
    videoElement.addEventListener("play", 		function() { console.log(name+" play");});
    
    // chrome only 
    videoElement.addEventListener("emptied", 	function() {
        console.log(name+" emptied");
        if(name==='local')
            showLocalDancer();
        });
    // way too chatty
    //videoElement.addEventListener("progress", 	function() { console.log(name+" progress");} );
      
    // firefox only
    videoElement.addEventListener("pause", 		function() { console.log(name+" pause");} );
    // unverified   
    
    videoElement.addEventListener("stalled", 	function() { console.log(name+" stalled");} );
    videoElement.addEventListener("suspended", 	function() { console.log(name+" suspended");} );
    videoElement.addEventListener("waiting", 	function() { console.log(name+" waiting");} );
}

window.addEventListener('unload', function(event) {
  console.log('window unload, undefining myRTCPeerConnectionsMap');
  myRTCPeerConnectionsMap=undefined;
});

function setupRTCConnection(config) {
    
    var cid = config.cid;
    var connectionType = config.connectionType;
    var role = config.role;
      
    var dataChannel;
    var dtStart = null;
    var dtEnd;
    var btLengthGoal = 0;
    var btLength=0;
    var time = new Date();
    var personMediaStream;  // media stream of person
    var personVideo; // video of person
    var personUid; // user id of person
    console.log("[" + time.toLocaleTimeString() + "] " + "**** setupRTCConnection "+connectionType+" role:"+role+" "+cid);
    
    var myRTCPeerConnectionConfig;
    //http://olegh.ftp.sh/public-stun.txt
    //https://gist.github.com/zziuni/3741933
    
    var stuns = config.stuns;
    var iceServers=[];
    $.each(stuns, function() {
        var url="stun:"+this.ip+":"+this.port;
        console.log("url:"+url);
        iceServers.push({urls:url});
    });
    //console.log("iceServers:"+iceServers);
    //iceServers:{urls: stun:turn0.mydetv.com:3478}
    myRTCPeerConnectionConfig = {
        iceServers: iceServers
    };
    myRTCPeerConnectionConfigOG = {
        iceServers: [
            { urls: "stun:stun.l.google.com:19302" },
            { urls: "stun:stun.ekiga.net"}
            ]
    };
    myRTCPeerConnectionConfigNG = {
        iceServers: [
            { urls: "stun:"+"stun.ekiga.net" }
            ]
    };
    
    var myPeerConnection = new RTCPeerConnection(myRTCPeerConnectionConfig);
    myRTCPeerConnectionsMap.set(cid, myPeerConnection);
    getStats();
    
    myPeerConnection.onnegotiationneeded        = negotiationNeededEventHandler;
    myPeerConnection.onconnectionstatechange    = connectionStateChangeHandler;
    myPeerConnection.onicecandidate             = iceCandidateEventHandler;
    myPeerConnection.oniceconnectionstatechange = iceConnectionStateChangeEventHandler;
    myPeerConnection.onicegatheringstatechange  = iceGatheringStateChangeEventHandler;
    
    
    myPeerConnection.onsignalingstatechange     = signalingStateChangeEventHandler;    
    myPeerConnection.ondatachannel              = dataChannelEventHandler;
    myPeerConnection.ontrack                    = trackEventHandler;
    myPeerConnection.onclose = function () {
        console.log("pc closed "+cid);
    };
    myPeerConnection.onopen = function () {
        console.log("pc opened "+cid);     
    };
               
    // **** 
    if(role === 'sender' && connectionType==='bw') {
        console.log("[" + time.toLocaleTimeString() + "] " + "sender creates an offer and data channel (bandwidth)");
        dataChannel = myPeerConnection.createDataChannel("sendChannel");
        dataChannel.onopen = (e) => { dataChannelOnopenHandler0(e); };
        dataChannel.onclose = (e) => { dataChannelOncloseHandler(e); };
        dataChannel.onmessage = (e) => {dataChannelMessageHandler(e); };
        dataChannel.onerror = (e) => {dataChannelErrorHandler(e); };
        dataChannel.onbufferedamountlow = (e) => {dataChannelBAL(e); };
    }   
    else if(role === 'sender' && connectionType==='video') {
        console.log("[" + time.toLocaleTimeString() + "] " + "sender adds remoteMediaStream's tracks");
        remoteMediaStream.getTracks().forEach(track => myPeerConnection.addTrack(track, remoteMediaStream));
        //myPeerConnection.addStream(remoteMediaStream);
    }  
    else if(role === 'sender' && connectionType==='u2u') {
        console.log("[" + time.toLocaleTimeString() + "] " + "sender adds localMediaStream's tracks");
        localMediaStream.getTracks().forEach(track => myPeerConnection.addTrack(track, localMediaStream));
        //myPeerConnection.addStream(localMediaStream);
    }
    else if(role === 'sender' && connectionType==='jtier0') {
        console.log("[" + time.toLocaleTimeString() + "] " + "sender adds stream for jtier0");
        localVideo  = document.querySelector('video#localVideo');
        setupVideoElementListeners("local", localVideo);
        //showLocalSpinner();
        myPeerConnection.addStream(localMediaStream);
    }
    else if(role === 'receiver' && connectionType==='janus') { 
        console.log("[" + time.toLocaleTimeString() + "] " + "setup janus");
        remoteVideo  = document.querySelector('video#remoteVideo');
        setupVideoElementListeners("remote", remoteVideo);
        showRemoteSpinner();
    }
    else if(role==='receiver' && connectionType==='u2u') {
        personUid = config.personUid;
    }
    else {
        console.log("[" + time.toLocaleTimeString() + "] " + "we created the PC but did nothing else "+role+" "+connectionType);
    }
    
    function createOffer() {
        time = new Date();
        if(myPeerConnection.signalingState == "have-local-offer") {
            console.log("[" + time.toLocaleTimeString() + "] " + "createOffer skipping, in state have-local-offer");
            return;
        }
        myPeerConnection.createOffer().then(function(offer) {
            console.log("[" + time.toLocaleTimeString() + "] " + "createOfferSuccess");
            sendOffer(offer);
            return(myPeerConnection.setLocalDescription(offer));
        })
        .then(function() {
            //sendOffer();
        })
        .catch(function(reason) {
            time = new Date();
            console.log("[" + time.toLocaleTimeString() + "] " + "createOfferFailure "+reason);
        });       
    }
    
    function createOfferRestart() {
        time = new Date();
        if(myPeerConnection.signalingState == "have-local-offer") {
            console.log("[" + time.toLocaleTimeString() + "] " + "createOffer skipping, in state have-local-offer");
            return;
        }
        myPeerConnection.createOffer({iceRestart:true}).then(function(offer) {
            console.log("[" + time.toLocaleTimeString() + "] " + "createOfferSuccess");
            sendOffer(offer);
            return(myPeerConnection.setLocalDescription(offer));
        })
        .then(function() {
            //sendOffer();
        })
        .catch(function(reason) {
            time = new Date();
            console.log("[" + time.toLocaleTimeString() + "] " + "createOfferFailure "+reason);
        });       
    }
    
    function onOffer(error, offerSdp) {
        if (error)
            return console.error('Error generating the offer');
        console.info('Invoking SDP offer callback function ' + location.host);
        var message = {
            'mydetv':mydetv,
            verb : 'offer',
            connectionType: connectionType,
            cid: cid,
            sdpOffer : offerSdp
        };
        sendMessage(message);
    }
    
    // ***********************************************************
    // ********** myPeerConnection event handlers  ***************
    // ***********************************************************
        
    function negotiationNeededEventHandler(event) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** *** *** Negotiation needed, will create offer:"+event);
        createOffer();
        }
        
    function connectionStateChangeHandler() {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "connectionStateChangeHandler "+myPeerConnection.connectionState+":"+cid);
    }
    
    function onIceCandidateDM(candidate) {
        console.log('Local candidate' + JSON.stringify(candidate));
        myMessage = {
            'mydetv':mydetv,
            verb : 'iceCandidate',
            connectionType: connectionType,
            role: role,
            cid: cid,
            candidate: candidate
            };
      sendMessage(myMessage);
    }
  
    function iceCandidateEventHandler(event) {
        time = new Date();
        if(! event.candidate) {           
            console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidateEventHandler done"+":"+cid);
            myPeerConnection.addIceCandidate(event.candidate);
            myMessage = {
                'mydetv':mydetv,
                verb : 'iceCandidatesDone',
                connectionType: connectionType,
                role: role,
                cid: cid,
                sdp: myPeerConnection.localDescription
            };
            sendMessage(myMessage);
            return;
        }
        else {
            console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidateEventHandler candidate:",event.candidate);
                myMessage = {
                    'mydetv':mydetv,
                    verb : 'iceCandidate',
                    connectionType: connectionType,
                    role: role,
                    cid: cid,
                    candidate: event.candidate
                };
                sendMessage(myMessage);
        }
    }

    // new, checking, connected, completed, failed, disconnected, closed
    // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceConnectionState
    function iceConnectionStateChangeEventHandler() {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** ICE connection state changed to: "+myPeerConnection.iceConnectionState+":"+cid);

        switch(myPeerConnection.iceConnectionState) {
            case "closed":
                break;
            case "failed":
                myMessage = {
                    'mydetv':mydetv,
                    verb : 'iceFailed',
                    connectionType: connectionType,
                    role: role,
                    cid: cid,
                    sdp: myPeerConnection.localDescription
                }; 
                sendMessage(myMessage);
                break;
            case "disconnected":
                break;
            default:
                break;
        }
    }
    
    function iceGatheringStateChangeEventHandler() {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** ICE gathering state changed to: " + myPeerConnection.iceGatheringState+":"+cid);  
    }
        
    function signalingStateChangeEventHandler() {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** WebRTC signaling state changed to: " +myPeerConnection.signalingState+":"+cid);
    }
 
    function trackEventHandler(event) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** trackEventHandler "+event.track.kind+":"+cid);
        var remoteVideo;
        if(connectionType==='video') {
            //getStats();
        }
        
        if(event.track.kind==="video") {       
            console.log("[" + time.toLocaleTimeString() + "] " + "*** trackEventHandler adding video "+connectionType);
            if(connectionType==='video' || connectionType==='janus') {
                remoteVideo  = document.querySelector('video#remoteVideo');
                remoteVideo.srcObject = event.streams[0];
                remoteMediaStream = event.streams[0];
                showRemoteVideo();
                makeVideoControls("remoteVideo");
            }
            else if(connectionType==='u2u' ) {             
                videoElementID='video#personVideo-'+personUid;
                console.log("videoElementID:"+videoElementID);
                imgElementId='#personImg-'+personUid;
                console.log("imgElementId:"+imgElementId);
                
                personVideo = document.querySelector(videoElementID);
                personVideo.srcObject = event.streams[0];
                personVideoStream = event.streams[0];
                
                height = $('#personImg-'+personUid).height();
                width =  $('#personImg-'+personUid).width();
                console.log("height:"+height+",width:"+width);
                
                //$('#person-'+personUid).css({'width':'99%'});
                
                $(videoElementID).css({'height':height+'px'});
                $(videoElementID).css({'width':width+'px'});
                $(videoElementID).removeClass('d-none');
                $(imgElementId).addClass('d-none');
            }
            else if(connectionType==='jtier0') {
                showLocalVideo();
            }
            else {
                console.log("[" + time.toLocaleTimeString() + "] " + "*** trackEventHandler didn't recognize "+connectionType);
                return;
            }
            
            myMessage = {
                'mydetv':mydetv,                  
                verb : 'videoConnected',
                connectionType: connectionType,
                role:role,
                cid: cid
            };
            console.log("[" + time.toLocaleTimeString() + "] " + "videoConnected");
            sendMessage(myMessage);
            //showRemoteVideo();
        }
        else {
            console.log("[" + time.toLocaleTimeString() + "] " + "*** trackEventHandler ignoring "+event.track.kind);
        }
    }

    function dataChannelEventHandler(e) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelEventHandler "+JSON.stringify(e));

        // not implemented yet in the browser, try again later
        //var sctp = myPeerConnection.sctp;
        //console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelEventHandler sctp"+JSON.stringify(sctp));
        dataChannel = e.channel;
        console.log("["+time.toLocaleTimeString()+"] "+"dataChannelEventHandler "+dataChannel.id+" protocol "+dataChannel.readyState+":"+cid);

        dataChannel.onopen = (e) => { dataChannelOnopenHandler1(e); };
        dataChannel.onclose = (e) => { dataChannelOncloseHandler(e); };
        dataChannel.onmessage = (e) => {dataChannelMessageHandler(e); };
        dataChannel.onerror = (e) => {dataChannelErrorHandler(e); };
        dataChannel.onbufferedamountlow = (e) => {dataChannelBAL(e); };
        
        dataChannel.onopen = dataChannelOnopenHandler2;
        dataChannel.onclose = dataChannelOncloseHandler;
        dataChannel.onmessage = dataChannelMessageHandler;
        dataChannel.onerror = dataChannelErrorHandler;
        dataChannel.onbufferedamountlow = dataChannelBAL;
        
        console.log("["+time.toLocaleTimeString()+"] "+"dataChannelEventHandler event handlers setup done");                 
    }

   function dataChannelErrorHandler(event) {
        time = new Date();
        dtEnd = time;
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelErrorHandler "+JSON.stringify(event));       
    }
   function dataChannelBAL(event) {
        time = new Date();
        dtEnd = time;
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelBAL "+JSON.stringify(event));       
    }
    
    // ***********************************************************
    // ********** datachannel event handlers  ***************
    // ***********************************************************
    function dataChannelOnopenHandler0(event) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler0 event "+JSON.stringify(event));
        var desc = myPeerConnection.localDescription.toJSON();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler localDescription "+JSON.stringify(desc));
    
        if(role === "sender") {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler sending bandwidthTestSenderReady "+sessionStorage.getItem(datafileStorageName).length);
           
            myMessage = {
                'mydetv':mydetv,
                verb : 'bandwidthTestSenderReady',
                size: sessionStorage.getItem(datafileStorageName).length,
                cid: cid
            };
            sendMessage(myMessage); 
        }
    }
    function dataChannelOnopenHandler1(event) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler1 event "+JSON.stringify(event));
        var desc = myPeerConnection.localDescription.toJSON();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler localDescription "+JSON.stringify(desc));
    
        if(role === "sender") {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler sending bandwidthTestSenderReady "+sessionStorage.getItem(datafileStorageName).length);
           
            myMessage = {
                'mydetv':mydetv,
                verb : 'bandwidthTestSenderReady',
                size: sessionStorage.getItem(datafileStorageName).length,
                cid: cid
            };
            sendMessage(myMessage); 
        }
    }
        function dataChannelOnopenHandler2(event) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler2 event "+JSON.stringify(event));
        var desc = myPeerConnection.localDescription.toJSON();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler localDescription "+JSON.stringify(desc));
    
        if(role === "sender") {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOnopenHandler sending bandwidthTestSenderReady "+sessionStorage.getItem(datafileStorageName).length);
           
            myMessage = {
                'mydetv':mydetv,
                verb : 'bandwidthTestSenderReady',
                size: sessionStorage.getItem(datafileStorageName).length,
                cid: cid
            };
            sendMessage(myMessage); 
        }
    }

    function dataChannelOncloseHandler(event) {
        time = new Date();
        dtEnd = time;
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelOncloseHandler "+JSON.stringify(event));
        myMessage = {
            'mydetv':mydetv,
            verb : 'dataChannelClosed',
            cid: cid
        };
        //sendMessage(myMessage); 
    }

    function dataChannelMessageHandler(e) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelMessageHandler dtStart "+dtStart+" "+e.data.length);

        btLength += e.data.length;
        if(btLengthGoal === 0) {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelMessageHandler we don't have a btLengthGoal yet");
            return;
        }
        console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelMessageHandler goal "+btLengthGoal+" current "+btLength);
        updateProgressbarBw(btLength);
        if(btLength>btLengthGoal) {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelMessageHandler odd goal "+btLengthGoal+" is less than current "+btLength);
        }
        if(btLength>=btLengthGoal) {
            console.log("[" + time.toLocaleTimeString() + "] " + "dataChannelMessageHandler goal achieved "+time);
            myMessage = {
                'mydetv':mydetv,                  
                verb : 'bandwidthTestFinished',
                cid: cid,
                dtStart: dtStart,
                dtEnd: time
                };
            sendMessage(myMessage);
            progressbarFinished++;
        }      
    }
    
    // ***********************************************************
    // ********** mydetvMessage custom event handler  ************
    // ***********************************************************
       
    myPeerConnection.addEventListener("mydetvMessage", function(e) {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "*** mydetvMessage:"+JSON.stringify(e.detail));
        var command = e.detail.message.command;
        console.log("[" + time.toLocaleTimeString() + "] " + "*** mydetvMessage command:"+command+":"+cid);
        switch(command) {
            case 'bandwidthTestPrepare':
                btLengthGoal = e.detail.message.args;
                dtStart = time;
                //console.log("[" + time.toLocaleTimeString() + "] " + "dataChannel:"+dataChannel.readyState+" negotiated:"+dataChannel.negotiated);
                myMessage = {
                    'mydetv':mydetv,
                    verb : 'bandwidthTestStart',
                    cid: cid
                };
                sendMessage(myMessage);
                break;
            case 'bandwidthTestStart':              
                var chunkSize = 1024*8;
                var numChunks = Math.ceil(sessionStorage.getItem(datafileStorageName).length / chunkSize);
                for(var i = 0, o = 0; i < numChunks; ++i, o += chunkSize) {
                    dataChannel.send(sessionStorage.getItem(datafileStorageName).substr(o, chunkSize));
                 }
                time = new Date();
                console.log("[" + time.toLocaleTimeString() + "] " + "*** mydetvMessage bandwidth test sent");
                break;
            case 'offer':
                receiveOffer(e.detail.message);
                break;
            case 'answer':
                receiveAnswer(e.detail.message);
                break;
            case 'startVideo':
                startVideo(e.detail.message);
                break;
            case 'reconnectVideo':
                reconnectVideo(e.detail.message);
                break;
            case 'iceCandidate':
                iceCandidate(e.detail.message);
                break;
            case 'prepareRemoveRTCConnection':
                prepareRemoveRTCConnection();
                break;
            case 'offerRestart':
                createOfferRestart();
                break;
            default:
                console.error("[" + time.toLocaleTimeString() + "] " + "*** mydetvMessage command not recognized:"+command);
        }    
    });
    
// ******************************************************************************************************************
// These come from signaling server via mydetvCustom events
// ******************************************************************************************************************
    function startVideo() {  
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "startVideo "+cid);
        remoteVideo.srcObject = remoteMediaStream;
        console.log("[" + time.toLocaleTimeString() + "] " + "startVideo done "+cid);
    }
    
    function reconnectVideo() {  
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "reconnectVideo "+cid);
        remoteMediaStream = videoElement.srcObject;
        console.log("[" + time.toLocaleTimeString() + "] " + "restarting");
    }

    function receiveOffer(message) {  
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "recieveOffer "+cid);
        sdp = message.sdp;
        var desc = new RTCSessionDescription(message.sdp);
        
        myPeerConnection.setRemoteDescription(desc)
            .then(function() {
                console.log("[" + time.toLocaleTimeString() + "] " + "setRemoteDescription success, now creating answer ");
                return(myPeerConnection.createAnswer());
            })
            .then(function(answer) {
                console.log("[" + time.toLocaleTimeString() + "] " + "created answer, setting localDescription");
                myPeerConnection.setLocalDescription(answer).then(function() {
                    console.log("[" + time.toLocaleTimeString() + "] " + "set localDescription done");
                    })
                .catch(function(error) {
                    time = new Date();
                    console.error("[" + time.toLocaleTimeString() + "] " + "set localDescription "+error);  
                });
            
            return answer;
            })
            .then(function(answer) {
                 console.log("[" + time.toLocaleTimeString() + "] " + "set localDescription done");
                 sendAnswer(answer);
            })
            .catch(function(error) {
                time = new Date();
                console.error("[" + time.toLocaleTimeString() + "] " + "receiveOffer error "+error);  
            });
    }
    
    function sendOffer(offer) {
        myMessage = {
            'mydetv':mydetv,                  
            verb : 'offer',
            cid: cid,
            connectionType: connectionType,
            sdp: myPeerConnection.localDescription,
            offer: offer
            };
        sendMessage(myMessage);
    }
    
    function receiveAnswer(message) {
        time = new Date();
        
        if(connectionType==='jtier0') {
           
        }
        var desc = new RTCSessionDescription(message.sdp);
        console.log("[" + time.toLocaleTimeString() + "] " + "receiveAnswer setRemoteDescription "+connectionType);
        myPeerConnection.setRemoteDescription(desc).then(function() {
            console.log("[" + time.toLocaleTimeString() + "] " + "answer: setRemoteDescription done "+connectionType);
        })
        .catch(function(error) {
            time = new Date();
            console.error("[" + time.toLocaleTimeString() + "] " + "setRemoteDescription "+connectionType+" error:"+error);  
        });
    }
    
    function sendAnswer(answer) {
        time = new Date();
        //console.log("[" + time.toLocaleTimeString() + "] " + "sendAnswer "+JSON.stringify(answer));
        myMessage = {
            'mydetv':mydetv,                  
            verb : 'answer',
            cid: cid,
            connectionType: connectionType,
            sdp: myPeerConnection.localDescription,
            answer: answer
            };
        sendMessage(myMessage);
    }
    
    function iceCandidate(message) {
        
        time = new Date();
        //console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidate type:"+myPeerConnection.remoteDescription.type+" "+cid);
        myIceCandidate = message.iceCandidate;
        //console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidate myIceCandidate:"+JSON.stringify(myIceCandidate));
        rtcIceCandidate = new RTCIceCandidate(myIceCandidate);    
        //console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidate rtcIceCandidate: "+JSON.stringify(rtcIceCandidate));
        myPeerConnection.addIceCandidate(rtcIceCandidate)
            .then(function() {
                console.log("ice candidate accepted");
            })
            .catch(function(err) {
                console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidate error "+err.name+":"+err.message);
            });
            
        //console.log("[" + time.toLocaleTimeString() + "] " + "iceCandidate done "+cid); 
    }

    function prepareRemoveRTCConnection() {
        console.log("prepareRemoveRTCConnection role:"+role+",connectionType:"+connectionType+",cid:"+cid);
        if(role==='receiver' && connectionType==='janus') {
            console.log("removing pc receiver janus");
            myPeerConnection.getSenders().forEach(function(sender) {
                console.log("removing sender "+sender);
                myPeerConnection.removeTrack(sender);
                var old_element  = document.querySelector('video#remoteVideo');
                var new_element = old_element.cloneNode(true);
                old_element.parentNode.replaceChild(new_element, old_element);
                remoteMediaStream=null;
                remoteVideo=null;
            });
            showRemoteDancer();
        }
        else if(connectionType==='video') {
            console.log("removing pc video");
            if(role==='sender') {
                console.log("removing pc video sender "+cid);
                myPeerConnection.close();
            }
            else {
                console.log("removing pc video receiver "+cid);
                myPeerConnection.close();
            }
        }
    //var personMediaStream;  // media stream of person
    //var personVideo; // video of person
    //var personUid; // user id of person
        else if(connectionType==='u2u') {         
            if(role==='sender') {
                console.log("removing pc u2u sender "+cid);
                myPeerConnection.getSenders().forEach(function(sender) {
                    console.log("removing sender "+sender);
                    myPeerConnection.removeTrack(sender);
                });
                myPeerConnection.close();
            }
            else {
                console.log("removing pc u2u receiver "+cid);              
                personVideo.srcObject = null;
                personVideoStream = null;
                $(videoElementID).addClass('d-none');
                $(imgElementId).removeClass('d-none');
                myPeerConnection.close();
            }
        }     
    }
    

    function getStats() {
        console.log("getting stats");
        var countRes=0;
        myPeerConnection.getStats(null).then(function(res) {
              res.forEach(function(report) {
                //console.log("report: "+report.key);
                //alert(Object.keys(report));
                if(report.type !== null && report.type !== undefined && report.type==='transport') {
                    console.log("recv:"+report.bytesReceived);
                }
                Object.keys(report).forEach(function(k) {
                    console.log(countRes+" "+k+":"+report[k]);
                });
                countRes++;
              });

        });

        setTimeout(function () {
            getStats();
        }, 100000000);
    }
    
    function myGetStats(callback) {
        var baselineReport, currentReport;
        var i=0;
        for(let s of myPeerConnection.getSenders()){
            console.log("sender "+i);
            i++;
        }
        if(i===0) {
            console.log("no sender");
            return;
        }
        /*
        var sender = myPeerConnection.getSenders()[0];
        
        sender.getStats().then(function (report) {
            baselineReport = report;
        })
        .then(function() {
            return new Promise(function(resolve) {
                setTimeout(resolve, aBit); // ... wait a bit
            });
        })
        .then(function() {
            return sender.getStats();
        })
        .then(function (report) {
            currentReport = report;
            processStats();
        })
        .catch(function (error) {
          console.log(error.toString());
        });
        */
    }
    
    function processStats() {
        // compare the elements from the current report with the baseline
        for (let now of currentReport.values()) {
            if (now.type != "outbound-rtp")
                continue;
    
            // get the corresponding stats from the baseline report
            let base = baselineReport.get(now.id);
    
            if (base) {
                remoteNow = currentReport[now.remoteId];
                remoteBase = baselineReport[base.remoteId];
    
                var packetsSent = now.packetsSent - base.packetsSent;
                var packetsReceived = remoteNow.packetsReceived - remoteBase.packetsReceived;
    
                // if intervalFractionLoss is > 0.3, we've probably found the culprit
                var intervalFractionLoss = (packetsSent - packetsReceived) / packetsSent;
            }
        }
    }
}

    // ***********************************************************
    // ********** end of setupRTCConnection  ***************
    // ***********************************************************

// the mydetvCustom message hooks in as an event
function mydetvCustom(message) {
    var cid = message.cid;
    var myPeerConnection = myRTCPeerConnectionsMap.get(cid);
	if(myPeerConnection===undefined) {
		console.log("could not find pc for mydetvCustom "+cid);
		return;
	}
    time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "mydetvCustom "+JSON.stringify(message));
    var mydetvEvent = new CustomEvent("mydetvMessage", {
        detail: {
            message: message
        }
    });
    console.log("dispatch");
    myPeerConnection.dispatchEvent(mydetvEvent); 
}

async function removeRTCConnection(message) {
    var cid = message.cid;
    var myPeerConnection = myRTCPeerConnectionsMap.get(cid);
    console.log("closing "+cid);
    var em = {
      verb: 'mydetvCustom',
      cid: cid,
      command: 'prepareRemoveRTCConnection'
    };
    mydetvCustom(em);
    await sleep(15000);
    myPeerConnection.close();
    await sleep(2000);
    myRTCPeerConnectionsMap.delete(cid);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function makeVideoControls(vid) {
    var video = document.getElementById(vid);
    video.controls = false;
    var rv = "<div id='controls'>";
    rv += "<button id='"+vid+"' title='play' onclick='togglePlayPause("+vid+")'>Play</button>";
    rv += "</div>";
}

function togglePlayPause(vid) {
   var playpause = document.getElementById(vid);
   if (video.paused || video.ended) {
      playpause.title = "pause";
      playpause.innerHTML = "pause";
      video.play();
   }
   else {
      playpause.title = "play";
      playpause.innerHTML = "play";
      video.pause();
   }
}

function makeFullScreen(divObj) {
  console.log("DOUBLE SCREEN!");
  if (!document.fullscreenElement && // alternative standard method
    !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
    if (divObj.requestFullscreen) {
      divObj.requestFullscreen();
    } else if (divObj.msRequestFullscreen) {
      divObj.msRequestFullscreen();
    } else if (divObj.mozRequestFullScreen) {
      divObj.mozRequestFullScreen();
    } else if (divObj.webkitRequestFullscreen) {
      divObj.webkitRequestFullscreen();
    } else {
      console.log("Fullscreen API is not supported");
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}

function connectRemoteStream(message) {
    var cid = message.cid;
    console.log("********* *************** *************");
    console.log("connect our remote stream to "+cid);
    console.log("********* *************** *************");
    var myPeerConnection = myRTCPeerConnectionsMap.get(cid);
    
    remoteMediaStream.getTracks().forEach(track => myPeerConnection.addTrack(track, remoteMediaStream));  
}

function hideVideoElements() {
    if(localVideo !== undefined && localVideo!==null)
        $('#localVideo').addClass('d-none');
    if(remoteVideo!== undefined && remoteVideo!== null)
        $('#remoteVideo').addClass('d-none');
}

