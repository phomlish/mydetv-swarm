var mydetv;

//window.onload = function() {
    //console.log("swarm.js window.onload");
    var hostname = window.location.hostname;
    //console.log("hostname:"+hostname);
    var cookies = decodeURIComponent(document.cookie);
    //console.log("cookies:"+cookies);
    var ca = cookies.split(';');
    for(var i = 0; i < ca.length; i++) {
        //console.log("ca:"+ca[i]);
        var cookieParts = ca[i].split('=');
        var cname=cookieParts[0].trim();
        var cvalue=cookieParts[1];
        //console.log("name:"+cookieParts[0]+",value:"+cvalue);
        if(cname=="mydetv") {
            //console.log("cvalue:"+cvalue);
            var vParts = cvalue.split(":");
            
            if(vParts.length==2 && vParts[0] == hostname) {
                mydetv = vParts[1];
            }
        }
    }
    console.log("mydetv:"+mydetv);
    const tzid = Intl.DateTimeFormat().resolvedOptions().timeZone;
    document.cookie = "btz="+tzid+";path=/";
    $('[data-toggle="popover"]').popover(); 
//}

function toggleChevron() {
    //d-none
    if($('#mainNavbar').hasClass("collapse") === true) {
        console.log("showing navbar");
        $('#mainNavbar').removeClass("collapse");
        $('#mainContainer').addClass("navbarShown");
        $('#bootstrap-overrides').removeClass('mt-2').removeClass('pt-3');
        $('#bootstrap-overrides').addClass('mt-5').addClass('pt-5');
        $('#chevronSpan').removeClass("chevronSpanTop");
        $('#chevronSpan').addClass("chevronSpanOffset");
        $('#mychevronUp').removeClass("d-none");
        $('#mychevronDown').addClass("d-none");
    }
    else {
        console.log("hiding navbar");
        $('#mainNavbar').addClass("collapse");
        $('#mainContainer').removeClass("navbarShown");
        $('#bootstrap-overrides').removeClass('mt-5').removeClass('pt-5');
        $('#bootstrap-overrides').addClass('mt-2').addClass('pt-3');
        $('#chevronSpan').removeClass("chevronSpanOffset");
        $('#chevronSpan').addClass("chevronSpanTop");
        $('#mychevronUp').addClass("d-none");
        $('#mychevronDown').removeClass("d-none");
    }
}

function urlencodeFormData(fd){
    var s = '';
    function encode(s) {
        return encodeURIComponent(s).replace(/%20/g,'+');
    }
    for(var p in fd.entries()) {
        if(typeof p[1]=='string') {
            s += (s?'&':'') + encode(p[0])+'='+encode(p[1]);
        }
    }
    return s;
}

function logout() {
    //console.log("logout");
    $.ajax({
        url: "/api/login/logout",
        type: "GET",
        success: function (result) {
            //console.log("result:"+result);
            window.location.replace("/");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //console.log("xhr.status:"+xhr.status+" thrownError:"+thrownError);
            window.location.replace("/");
        }
    });
    //console.log("logout done");
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}







