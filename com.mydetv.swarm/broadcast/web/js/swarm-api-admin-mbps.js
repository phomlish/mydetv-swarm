
function getMbpsGridData() {
	var url="/api/mbps";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            drawMbpsGrid(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function sendText(item, column) {
    console.log("sending txt "+column+" to server",item);
    var data = {};
    data["item"] = item;
    data["column"] = column;
    $.ajax({
        url: "/api/mbps/text",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("updated");
                    break;
                default:
                    
            }
            // success finally
            //alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}