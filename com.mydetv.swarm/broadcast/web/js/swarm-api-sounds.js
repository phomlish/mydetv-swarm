function getSoundsGridData() {
	var url="/api/sounds/getSounds";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            proccessSoundData(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function getProfileSoundsGridData() {
	var url="/api/sounds/getProfileSounds";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
        	console.log("result:",result);
        	proccessProfileSoundData(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function addProfileSound(id) {
	var url="/api/sounds/addProfileSound?id="+id;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            finishAddProfileSound(id);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function removeProfileSound(id) {
	var url="/api/sounds/removeProfileSound?id="+id;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
        	finishRemoveProfileSound(id);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
            return false;
        }
    });
}

function updownProfileSound(id, ud) {
	var url="/api/sounds/updownProfileSound?bpId="+id+'&ud='+ud;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
        	finishUdProfileSound();
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
            return false;
        }
    });
}

function apiPlaySound(id) {
	
	console.log("id",id);
	var url="/api/sounds/playRemoteSound?id="+id;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
        	playingRemoteSound(id);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
            return false;
        }
    });
}





