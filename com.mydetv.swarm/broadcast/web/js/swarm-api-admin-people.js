$(document).ready(function() {
	console.log("ok");    	
	//getPeople();
});

function getPeople() {
	var url="/api/admin/people";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            drawPeople(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function roleChange(ce, role, wuid) {
    console.log("roleChange role:"+role+",wuid:"+wuid+",checked:"+ce.checked);
    if(role===0 && ce.checked===true)
        uncheckAllExceptAdmin();
    else if(ce.checked===true)
        uncheckIfAdminChecked(ce, role);
    if(role===2)
        uncheckIfPeopleChecked(ce,role);
    if(role===3)
        uncheckChat(ce,role);
}
function uncheckAllExceptAdmin() {  
    $('#tblRoles').find('.cbrole').each(function() {
        console.log("cbrole:"+this.id+" "+this.checked);
        if(this.id!=='role-0')
            this.checked=false;
    });
}
function uncheckIfAdminChecked(ce, role) {  
    var admin=$('#role-0');
    console.log("admin:",admin);
    if(admin.prop('checked')) {
        console.log("test");
       ce.checked=false;
    }
}
function uncheckIfPeopleChecked(ce, role) {  
    var people=$('#role-3');
    console.log("people:",people);
    if(people.prop('checked')) {
        console.log("test");
        ce.checked=false;
    }
}
function uncheckChat(ce, role) {  
    var chat=$('#role-2');
    console.log("chat:",chat);
    if(chat.prop('checked')) {
        console.log("test chat");
        chat.prop('checked', false);
    }
}
function submitRoles() {

    var data={};
    data.wuid=$('#wuid').text();
    $('#tblRoles').find('.cbrole').each(function() {
        console.log("cbrole:"+this.id+" "+this.checked);
        data[this.id]=this.checked;
    });
    console.log("data:",data);
    
    $.ajax({
        url: "/api/admin/people/roles",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("ok");
                    bootbox.alert("roles successfully submitted");
                    break;
                default:
                    
            }
            // success finally
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alertUnauthorized401(response);
                    break;
                case 403:
                    alertForbidden403(response, "");
                    break;
                default:
                    alertServerError500();
            }
            // error finally
        }
    });
}

function apBanned(ce, wuid) {
    //console.log("apBanned",wuid);
    if(ce.checked) {
        console.log("banned checked "+wuid);
        bootbox.confirm({
            message: "Are you sure you want to ban this person?<br>They will be immediately logged out and will not be able to login again.",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Yes'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> No'
                }
            },
            callback: function (result) {
                if(result)
                    $.get("/api/admin/people/ban/"+wuid, function() {
                        console.log("success");
                        $('#dtBanned').text("banned");
                    })
                    .fail(function(error) {
                        console.log("error ",error);
                        bootbox.alert("error "+error.statusText+" "+error.responseText);
                        ce.checked=false;
                    });
                else
                    ce.checked=false;
            }
        });
    }
    else {
        console.log("banned unchecked "+wuid);
        bootbox.confirm({
            message: "Are you sure you want to un-ban this person?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Yes'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> No'
                }
            },
            callback: function (result) {
                if(result)
                    $.get("/api/admin/people/unban/"+wuid, function() {
                        console.log("success");
                        $('#dtBanned').text("");
                    })
                    .fail(function(error) {
                        console.log("error ",error);
                        bootbox.alert("error "+error.statusText+" "+error.responseText);
                        ce.checked=false;
                    });
                else
                    ce.checked=true;
            }
        });
    }
}

function apLocked(ce, wuid) {
    console.log("apBanned",wuid);
    if(ce.checked) {
        console.log("unchecked "+wuid);
        bootbox.alert("Can't lock people");
    }
    else {
        console.log("checked "+wuid);
        bootbox.confirm({
            message: "Are you sure you want to unlock this person?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Yes'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> No'
                }
            },
            callback: function (result) {
                if(result)
                    $.get("/api/admin/people/unlock/"+wuid, function() {
                        console.log("success");
                    })
                    .fail(function(error) {
                        console.log("error ",error);
                        bootbox.alert("error "+error.statusText+" "+error.responseText);
                        ce.checked=false;
                    });
                else
                    ce.checked=false;
            }
        });

    }
}

function apMuted(ce, wuid) {
    console.log("apMuted",wuid);
    if(ce.checked) {
        console.log("checked "+wuid);
        bootbox.confirm({
            message: "Are you sure you want to mute this person?<br>They won't be able to share video or chat.",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Yes'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> No'
                }
            },
            callback: function (result) {
                if(result)
                    $.get("/api/admin/people/mute/"+wuid, function() {
                        console.log("success");
                    })
                    .fail(function(error) {
                        console.log("error ",error);
                        bootbox.alert("error "+error.statusText+" "+error.responseText);
                        ce.checked=false;
                    });
                else
                    ce.checked=false;
            }
        });
    }
    else {
        console.log("unchecked "+wuid);
        bootbox.confirm({
            message: "Are you sure you want to un-ban this person?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-check"></i> Yes'
                },
                cancel: {
                    label: '<i class="fa fa-times"></i> No'
                }
            },
            callback: function (result) {
                if(result)
                    $.get("/api/admin/people/unmute/"+wuid, function() {
                        console.log("success");
                    })
                    .fail(function(error) {
                        console.log("error ",error);
                        bootbox.alert("error "+error.statusText+" "+error.responseText);
                        ce.checked=false;
                    });
                else
                    ce.checked=true;
            }
        });
    }
}