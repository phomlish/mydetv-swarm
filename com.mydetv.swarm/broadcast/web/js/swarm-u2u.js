// this is wha i use to select the video
//profileVideo = document.querySelector('video#profileVideo');

// these had the verb u2u
function u2uMessage(message) {
    switch(message.action) {
    case 'startCamera':
        startCamera();
        break;
    case "requestVideo":
        requestVideo(message);
        break;
    case "failed":
        bootbox.alert("failed: "+message.message);
        break;
    default:
        console.log("what are they saying "+message.action);
    }
}

// messages
function privateVideo(otherPerson) {
    console.log("starting private video with "+otherPerson);
    console.log("lv:"+localVideo+",lms:"+localMediaStream);
    var message = {
        action: 'initialize',
        person: otherPerson
    };
    sendU2U(message);  
}

function sendLocalVideoStarted() {
    var message = {
        action: 'sendLocalVideoStarted',
        person: otherPerson
    };
    sendU2U(message);  
}

function startCamera(oMessage) {
    showLocalSpinner();
    profileVideo = document.querySelector('video#profileVideo');
    console.log("got profileVideo");
    var constraints = {
        audio: true,
        video: {
            width: { min: 320, ideal: 320, max: 640 },
            height: { min: 240, ideal: 240, max: 480  }
            }
        };

    navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream1) {
        console.log("got localmediaStream");
        localMediaStream=mediaStream1;
        profileVideo.srcObject = mediaStream1;
        profileVideo.volume = 0;
        profileVideo.muted = 1;
        profileVideo.onloadedmetadata = function() {
            console.log("playing local video");
            profileVideo.volume = 0;
            profileVideo.muted = 1;
            showProfileVideo();          
            sendU2U(oMessage);
            
        };
        mediaStream1.oninactive = function() {
            console.log('Stream inactive');
        };
    })
    .catch(function(err) { console.error(err.name + ": " + err.message); });
}
//NotFoundError: The object can not be found here.

function requestVideo(message) {
    console.log(message.from+" is requesting a private video");
    var oMessage;
    bootbox.confirm(
        {
        message: message.from+" is requesting video chat. Accept?",
        buttons: {
            confirm: {
                label: 'Accept',
                className: 'btn-success'
            },
            cancel: {
                label: 'Reject',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
        if(result) {
            console.log("accept");
            oMessage = {
                action : 'accept',
                cid: message.cid
            };

            startCamera(oMessage);

        }
        else {
            oMessage={
                action : 'reject',
                cid: message.cid
            };
            sendU2U(oMessage);
        }
        
        }
    });
}
function sendU2U(message) {  
    message.mydetv = mydetv;
    message.verb = 'u2u';
    sendMessage(message); 
    
}
function showProfileVideo() {
    $('#profilePicture').addClass('d-none');
    $('video#profileVideo').removeClass('d-none'); 
}

function videoDialog(pv) {
    var muted=0;
    var volume=50;
    
    console.log("pv:",pv);
    myVideo = document.querySelector('video#'+pv);
    
    // video.muted = false;
    //video.volume = volumeBar.value;
    var dc="<div class='center'>";
    dc+="<div class='btnSlide' id='slide-"+pv+"'></div>";
    
    dc+="<button class='btnVc' id='btnMute-"+pv+"'>";
    if(muted===1) dc+="unmute";
    else dc+="mute";
    dc+="</button>";
    
    dc+="<button class='btnVc' id='btnStop-"+pv+"'>stop</button>";
    dc+="<button class='btnVc' id='btnFullscreen-"+pv+"'>full screen</button>";
    dc+="</div>";
        
    if ($('#dlgVideoControls').length === 0) {     
        $(document.body).append('<div id="dlgVideoControls">'+dc+'</div>');
    } else {
        $('#dlgVideoControls').html(dc);
    }
    $( "#dlgVideoControls" ).dialog({
        autoOpen: false,
        show: "blind",
        hide: "explode",
        draggable: true,
        title: "video controls",
        buttons: {
            OK: function() { //ok
                    $( this ).dialog( "close" );
                },
            Cancel: function() { //cancel
                    $( this ).dialog( "close" );
                }
            }
        
    });
    $("#dlgVideoControls").dialog("open");
    $("#slide-"+pv).slider({
        max: 100,
        min: 0,
        step: 5,
        value: volume,
        change: function(event, ui) {
            console.log("new:"+ui.value);
            myVideo.volume = ui.value;
        }
             
    });
    
    $("#btnMute-"+pv).button()
        .click(function() {
            console.log("muted:"+muted);
            if(muted===0) {
                muted=1;
                myVideo.muted = false;
                $("#btnMute-"+pv).html("unmute");
            }
            else {
                muted=0;
                myVideo.muted = true;
                $("#btnMute-"+pv).html("mute");
            }
        });
        
    $("#btnStop-"+pv).button()
        .click(function() {
            console.log("close");
            var message = {
                action: 'stop',
                person: pv
            };
            sendU2U(message); 
                     
            $('#dlgVideoControls').dialog( "close" );
        });

    $("#btnFullscreen-"+pv).button()
        .click(function() {
            myVideo.webkitEnterFullScreen();
        });
}












