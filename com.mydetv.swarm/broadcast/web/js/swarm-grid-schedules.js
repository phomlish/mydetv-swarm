
var grid;
var dataView;
var data = [];
var schannel;
var jSchedules;

var columns = [
	{id: "pkey", name: "pkey", field: "pkey", sortable:true, maxWidth: 50}
	,{id: "pn", name: "pn", field: "pn", sortable:true, minWidth: 400}
	,{id: "title", name: "title", field: "title", sortable:true, minWidth: 150}
	,{id: "catMain", name: "catMain", field: "catMainName", sortable:true, maxWidth: 50}
	,{id: "catSub", name: "catSub", field: "catSubName", sortable:true, maxWidth: 50}
	,{id: "length", name: "length", field: "length", sortable:true}
	,{id: "scheduled", name: "scheduled", field: "scheduled", sortable:true, editable:true, formatter: Slick.Formatters.Checkmark, editor:scheduleEditor}
	,{id: "viewvideo", name: "viewvideo", field: "viewvideo", formatter: function (args2) {
		var pkey = data[args2].pkey;
		var pn = data[args2].pn;
		var rv="";
		rv+="<a data-fancybox='preview-video' data-width='640' ";
		rv+=" href='/api/video/getConverted?video="+pn+"'>";
		rv+=" <img src='/s/img/Video-Icon.png' alt='"+pn+"'height='16' width='16' />";
		rv+="</a>";
		return rv;
	}
	}
	,{id: "donotuse", name: "donotuse", field: "donotuse", sortable:true, editable:true, formatter: Slick.Formatters.Checkmark, editor:donotuseEditor}
	];
var options = {
		editable: true,
		autoEdit:true
};

function doOnClick(pn) {
	console.log("this:",this);
	//console.log("pn:",pn);
}

$(document).ready(function() {

	schannel = $("#schannel").val();
	console.log("schannel:",schannel);
	getSchedules(schannel)
});

function drawSchedules(result) {
	jSchedules=result;
	var j=0;
	$.each(jSchedules, function(key, jSchedule) {
		var jo = jSchedules[key];
		//console.log("jo:",jo.pkey);
		jSchedule.id="id_"+jo.pkey;
		/*
		jSchedule.title=jSchedule.ov.title;
		jSchedule.pn = jSchedule.ov.pn;
		jSchedule.catMain=jSchedule.ov.vcmName;
		jSchedule.catSub = jSchedule.ov.vcsName;
		jSchedule.length=jSchedule.ov.length;
		jSchedule.doNotUse=jSchedule.ov.doNotUse;
		*/
		data[j] = jSchedule;
		j++;
	});
	
	dataView = new Slick.Data.DataView({ inlineFilters: true });

	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.endUpdate();
	dataView.refresh();

	grid = new Slick.Grid("#myGrid", dataView, columns, options);
	grid.autosizeColumns();

	grid.onSort.subscribe(function(e, args) {
		console.log("sorting:"+args.sortCol.field);
		var comparer = function(a, b) {
			return (a[args.sortCol.field] > b[args.sortCol.field]) ? 1 : -1;
		}

		dataView.sort(comparer, args.sortAsc);
		dataView.refresh();
	});

	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.render();
	});
	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.render();
	});
	/*
    grid.onClick.subscribe(function(e, args) {
        var item = grid.getDataItem(args.row);

        console.log("args:",args);
        console.log("looking:",args.grid.getItems);
        //var item = args.grid.getData()[0];
        console.log("item:",item);
    });
	 */
};

function scheduleEditor(args) {
	var $select;
	var defaultValue;
	var scope = this;

	this.init = function () {
		$select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
		$select.appendTo(args.container);
		$select.focus();
	};

	this.destroy = function () {
		$select.remove();
	};

	this.focus = function () {
		$select.focus();
	};

	this.loadValue = function (item) {
		defaultValue = !!item[args.column.field];
		if (defaultValue) {
			$select.prop('checked', true);
		} else {
			$select.prop('checked', false);
		}
	};

	this.preClick = function () {
		$select.prop('checked', !$select.prop('checked'));
	};

	this.serializeValue = function () {
		return $select.prop('checked');
	};

	this.applyValue = function (item, state) {   
		console.log("change "+state,item);
		sendScheduledChange(schannel, item);
		item[args.column.field] = state;
	};

	this.isValueChanged = function () {
		return (this.serializeValue() !== defaultValue);
	};

	this.validate = function () {
		return {
			valid: true,
			msg: null
		};
	};

	this.init();
}

function donotuseEditor(args) {
	var $select;
	var defaultValue;
	var scope = this;

	this.init = function () {
		$select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
		$select.appendTo(args.container);
		$select.focus();
	};

	this.destroy = function () {
		$select.remove();
	};

	this.focus = function () {
		$select.focus();
	};

	this.loadValue = function (item) {
		defaultValue = !!item[args.column.field];
		if (defaultValue) {
			$select.prop('checked', true);
		} else {
			$select.prop('checked', false);
		}
	};

	this.preClick = function () {
		$select.prop('checked', !$select.prop('checked'));
	};

	this.serializeValue = function () {
		return $select.prop('checked');
	};

	this.applyValue = function (item, state) {   
		console.log("change "+state,item);
		item[args.column.field] = state;
		sendDoNotUse(item);
	};

	this.isValueChanged = function () {
		return (this.serializeValue() !== defaultValue);
	};

	this.validate = function () {
		return {
			valid: true,
			msg: null
		};
	};

	this.init();
}

