

function onChangeLocalVideo() {
	if($('#changeLocalVideo').is(":checked")) {
		startLocalVideo();
	}
	else {
		stopLocalVideo();
	}	
}

function startLocalVideo() {	
	console.log("startLocalVideo");
    showLocalSpinner();
    localVideo = document.querySelector('video#localVideo');
    var constraintsNU = { 
    	audio: true, 
    	video: {
    		width: {min: 320, ideal: 1920, max: 1920},
   		    height: {min: 240, ideal: 1080, max: 1080}
    	}
    };
    var constraints = { 
    	audio: true, 
    	video: true   		
    };
    console.log("go");
    navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream1) {
        console.log("getting mediaStream");
        showLocalSpinner();
        localMediaStream=mediaStream1;
        localVideo.srcObject = mediaStream1;
        localVideo.onloadedmetadata = function() {
            console.log("playing local video");
            showLocalVideo();
            localVideoStarted(mediaStream1);
            
        };
        mediaStream1.oninactive = function() {
            console.log('Stream inactive');
            localVideoStopped();
        };
    })
    .catch(function(err) {
    	showLocalDancer();
    	bootbox.alert("could not start your video, "+err.name + ": " + err.message,err);
    	$('#changeLocalVideo').prop("checked", false);
    });
}

function stopLocalVideo() {
	$('#sourceLive').prop("disabled", true);
	
    let tracks = localMediaStream.getTracks();
	tracks.forEach(function(track) {
		track.stop();
	});
	showLocalDancer();  
	sendLocalVideoStopped();
}

function localVideoStarted(mediaStream) {
	$('#sourceLive').prop("disabled", false);
	
	var media = {};
	media.audio = [];
	media.video = [];
    $.each(mediaStream.getAudioTracks(), function(index, value) {
    	var element = {};
    	element.track = value;
    	element.settings = value.getSettings();
    	media.audio.push(element);
    });
    $.each(mediaStream.getVideoTracks(), function(index, value) {
    	var element = {};
    	element.track = value;
    	element.settings = value.getSettings();
    	media.video.push(element);
    });

    console.log("media",media);
    
    var message={};
    message.mydetv = mydetv;
    message.verb = 'localVideoStarted';
    message.channelId = $('#channelId').text();
    message.media = media;
    sendMessage(message);
}
function sendLocalVideoStopped() {
    var message={};
    message.mydetv = mydetv;
    message.verb = 'localVideoStopped';
    message.channelId = $('#channelId').text();
    sendMessage(message);
}




