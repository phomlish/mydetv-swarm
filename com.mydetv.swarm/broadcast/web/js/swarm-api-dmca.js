function dmcaSchedule(channelId, pkey, osId) {
	
	console.log("dmcaSchedule "+channelId+" "+pkey+" "+osId);
	var url = "/dmca?c="+channelId+"&p="+pkey+"&o="+osId;
	window.location.replace(url);
}

function showDMCA_ODescHelp() {
	bootbox.alert($('#oDescriptionHelp').html());
}
function showDMCA_TDescHelp() {
	bootbox.alert($('#tDescriptionHelp').html());
}
function submitDmca() {
	var errors=0;
	var data={};
	var cpname = $('#cpname').val();
	if(cpname=="") {
		$('#cpname').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#cpname').removeClass('dmcaRedTint');
		data.cpname = cpname;
	}
	var cptitle = $('#cptitle').val();
	if(cptitle=="") {
		$('#cptitle').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#cptitle').removeClass('dmcaRedTint');
		data.cptitle = cptitle;
	}
	var cname = $('#cname').val();
	if(cname=="") {
		$('#cname').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#cname').removeClass('dmcaRedTint');
		data.cname = cname;
	}
	var odesc = $('#odesc').val();
	if(odesc=="") {
		$('#odesc').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#odesc').removeClass('dmcaRedTint');
		data.odesc = odesc;
	}
	var titem = $('#titem').val();
	if(titem=="") {
		$('#titem').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#titem').removeClass('dmcaRedTint');
		data.titem = titem;
	}
	var address = $('#address').val();
	if(address=="") {
		$('#address').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#address').removeClass('dmcaRedTint');
		data.address = address;
	}
	var city = $('#city').val();
	if(city=="") {
		$('#city').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#city').removeClass('dmcaRedTint');
		data.city = city;
	}
	var state = $('#state').val();
	if(state=="") {
		$('#state').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#state').removeClass('dmcaRedTint');
		data.state = state;
	}
	var zipcode = $('#zipcode').val();
	if(zipcode=="") {
		$('#zipcode').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#zipcode').removeClass('dmcaRedTint');
		data.zipcode = zipcode;
	}
	var country = $('#country').val();
	if(cpname=="") {
		$('#country').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#country').removeClass('dmcaRedTint');
		data.country = country;
	}
	var phone = $('#phone').val();
	if(phone=="") {
		$('#phone').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#phone').removeClass('dmcaRedTint');
		data.phone = phone;
	}
	var email = $('#email').val();
	if(email=="") {
		$('#email').addClass('dmcaRedTint');
		errors++;
	}
	else {
		$('#email').removeClass('dmcaRedTint');
		data.email = email;
	}
	
	console.log("data:",data);
	if(errors>0) {
		bootbox.alert("Errors in your submission have been highlighted in red.  Please resolve and try again.");
		return;
	}
	
	$.ajax({
        url: "/api/dmca/submit",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "text",
        success: function (result) {
        	bootbox.alert("Your request will be processed forthwith");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            bootbox.alert("An error occured submitting your request.  We will resolve this error forthwith");
        }
    });
	
}