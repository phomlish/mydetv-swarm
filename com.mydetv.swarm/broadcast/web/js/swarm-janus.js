var opaqueId = "streamingtest-"+mydetv;
var streaming = null;
var janus = null;
var spinner = null;

function initJanus() {
    var time = new Date();
    Janus.init({
        debug: true, dependencies: Janus.useDefaultDependencies(), callback: function() {
            console.log("[" + time.toLocaleTimeString() + "] " + "janus initialized");
            server = "https://10.11.1.96:8089/janus";
            janus = new Janus({
                //	
                server: server,
                success: function() {
                    console.log("[" + time.toLocaleTimeString() + "] " + "janus onInitSuccess0 ");
                    onInitSuccess();
                },
                error: function(error) {
                    console.log("[" + time.toLocaleTimeString() + "] " + "janus error ");
                    //Janus.error(error);
                    bootbox.alert(error, function() {
                        //window.location.reload();
                    });
                },
                destroyed: function() {
                    console.log("[" + time.toLocaleTimeString() + "] " + "janus destroyed ");
                    //window.location.reload();
                }
            });
        }
    });   
}

function onInitSuccess() {
    var time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "janus onInitSuccess, attaching...");
    janus.attach({
		plugin: "janus.plugin.streaming",
		opaqueId: opaqueId,
        success: function(pluginHandle) {
            console.log("[" + time.toLocaleTimeString() + "] " + "janus attach success ");
            streaming = pluginHandle;
            Janus.log("Plugin attached! (" + streaming.getPlugin() + ", id=" + streaming.getId() + ")");
            
            var body = { "request": "watch", id: 12 };
            streaming.send({"message": body});
            

    
        },
        error: function(error) {
            Janus.error("  -- Error attaching plugin... ", error);
			bootbox.alert("Error attaching plugin... " + error);
		},
        onmessage: function(msg, jsep) {
            console.log("[" + time.toLocaleTimeString() + "] " + "janus onmessage ");
            Janus.debug(" ::: Got a message :::");
			Janus.debug(msg);
            
            var result = msg.result;
            if(result !== null && result !== undefined) {
                if(result.status !== undefined && result.status !== null) {
                    var status = result.status;
                    if(status === 'stopped')
                        console.log("[" + time.toLocaleTimeString() + "] " + "stopped ");
                }
                else if(msg.streaming === "event") {
                    // Is simulcast in place?
                    var substream = result.substream;
                    var temporal = result.temporal;
                    if((substream !== null && substream !== undefined) || (temporal !== null && temporal !== undefined)) {
                        if(!simulcastStarted) {
                            simulcastStarted = true;
                            //addSimulcastButtons();
                        }
                        // We just received notice that there's been a switch, update the buttons
                        //updateSimulcastButtons(substream, temporal);
                    }
                }
                else if(msg.error !== undefined && msg.error !== null) {
                    bootbox.alert(msg.error);
                    //stopStream();
                    return;
                }
                else {
                    console.error("[" + time.toLocaleTimeString() + "] " + "unrecognized "+msg);                                          
                }
            }
            else if(msg.error !== undefined && msg.error !== null) {
                bootbox.alert(msg.error);
                //stopStream();
                return;
            }
            else {
                console.error("[" + time.toLocaleTimeString() + "] " + "unrecognized "+msg);                    
            }
            
            if(jsep !== undefined && jsep !== null) {
                Janus.debug("Handling SDP as well...");
                Janus.debug(jsep);
                // Answer
                streaming.createAnswer(
                    {
                        jsep: jsep,
                        media: { audioSend: false, videoSend: false },	// We want recvonly audio/video
                        success: function(jsep) {
                            Janus.debug("Got SDP!");
                            Janus.debug(jsep);
                            var body = { "request": "start" };
                            streaming.send({"message": body, "jsep": jsep});
                            //$('#watch').html("Stop").removeAttr('disabled').click(stopStream);
                        },
                        error: function(error) {
                            Janus.error("WebRTC error:", error);
                            bootbox.alert("WebRTC error... " + JSON.stringify(error));
                        }
                    });
            }
            
        },
        onremotestream: function(stream) {
            console.log("[" + time.toLocaleTimeString() + "] " + "janus onremotestream ");
            Janus.debug(" ::: Got a remote stream :::");
            Janus.debug(stream);
            
            if($('#remotevideo').length > 0) {
                // Been here already: let's see if anything changed
                var videoTracks = stream.getVideoTracks();
                if(videoTracks && videoTracks.length > 0 && !videoTracks[0].muted) {
                    if($("#remotevideo").get(0).videoWidth)
                        $('#remotevideo').show();
                }
                return;
            }
            $('#stream').append('<video class="rounded centered hide" id="remotevideo" width=320 height=240 autoplay/>');
            $("#remotevideo").bind("playing", function () {
                if(this.videoWidth)
                    $('#remotevideo').removeClass('hide').show();
                if(spinner !== null && spinner !== undefined)
                    spinner.stop();
                spinner = null;
                var videoTracks = stream.getVideoTracks();
                if(videoTracks === null || videoTracks === undefined || videoTracks.length === 0)
                    return;
            });
            
            Janus.attachMediaStream($('#video').get(0), stream);
            
            var videoTracks = stream.getVideoTracks();
            
            
            if(videoTracks && videoTracks.length &&
                (Janus.webRTCAdapter.browserDetails.browser === "chrome"
                || Janus.webRTCAdapter.browserDetails.browser === "firefox"
                || Janus.webRTCAdapter.browserDetails.browser === "safari")) {
                        bitrateTimer = setInterval(function() {
                            // Display updated bitrate, if supported
                            var bitrate = streaming.getBitrate();
                            Janus.debug("Current bitrate is " + streaming.getBitrate());
                            // Check if the resolution changed too
                            var width = $("#remotevideo").get(0).videoWidth;
                            var height = $("#remotevideo").get(0).videoHeight;
                        }, 1000);
                    }
                     
            
        },
        oncleanup: function() {
            console.log("[" + time.toLocaleTimeString() + "] " + "janus oncleanup ");
            Janus.log(" ::: Got a cleanup notification :::");
        }
        
    });
    
}

