var initType;
var page;
// sounds
var sdata;
var soundsTable;
var stLoaded;
var stInit;
// profile sounds
var pdata;
var soundsProfileTable;
var stpLoaded;
var stpInit;
var reloadProfileData;

var editDisabled=false;
var spinner;

$(document).ready(function() {
	spinner=$('#spinner2Div');
	initVariables();
	var pathname = window.location.pathname;
	console.log("pathname:",pathname);
	if(pathname=="/sounds") {	
		initType=0;
		console.log("set initType "+initType);
		page='sounds';
	}
	else if(pathname=="/broadcast") {
		initType=1;
		console.log("set initType "+initType);
		page="broadcast";
	} 
	else {
		console.log("didn't recognize page "+pathname);
		return;
	}
	init();
});

function initVariables() {
	stLoaded=false;
	stInit=false;
	stpLoaded=false;
	stpInit=false;
	reloadProfileData=false;
}


function init() {
	console.log("init initType "+initType);
	switch(initType) {
	
	// sound page load
	case 0:
		if(!stpLoaded) {
			console.log("disable edit")
			disableEdit();
			getProfileSoundsGridData();
		}
		else if(!stLoaded)
			getSoundsGridData();
		else if(!stpInit)
			drawProfileSoundGrid();
		else if(!stInit)
			drawSoundGrid();
		else {
			console.log("enable edit")
			enableEdit();
		}
		
		break;
		
	// broadcast page load
	case 1:
		if(!stpLoaded)
			getProfileSoundsGridData();
		else if(!stLoaded)
			getSoundsGridData();
		else if(! stpInit){
			drawProfileSoundGridForBroadcastPage();
		}
		break;
		
	// just reload profile
	case 2:
		if(!reloadProfileData) {
			console.log("reload just profile data");
			reloadProfileData=true;
			soundsProfileTable.destroy();
			stpLoaded=false;
			stpInit=false;		
			console.log("disable edit")
			disableEdit();
			getProfileSoundsGridData();
		}
		else if(!stpInit) {
			console.log("reload profile table");
			drawProfileSoundGrid();
		}
		else {
			console.log("done reload profile");
			reloadProfileData=false;
			console.log("enable edit")
			enableEdit();
		}
		break;
	// reload both tables
	case 99:
		soundsProfileTable.destroy();
		soundsTable.destroy();
		stLoaded=false;
		stpLoaded=false;
		initType=0;
		init();
		break;
	default:
		console.log("didn't recognize initType "+initType);
	}
}

function proccessSoundData(result) {
	sdata = [];
	jSounds=result.jSounds;
	var j=0;
	$.each(jSounds, function(key, jSound) {		
		jSound.pn=jSound.dn+"/"+jSound.fn;
		
		var div = "<div id='localSoundDiv-"+jSound.id+"' class='soundDiv'>";
		div+="<audio id='localSound-"+jSound.id+"' preload='none'";
		div+=" src='/api/sounds/getSound?id="+jSound.id+"' type='audio/mp3'/>";
		div+="</audio></div>";	
		$(document.body).append(div);
		
		var psCtls="";
		psCtls+="<i class='fa fa-play sound-ctl' id='localSoundImg-"+jSound.id+"' aria-hidden='true' onclick='playStopLocalSound("+jSound.id+");'></i>";
	    jSound.playLocal = psCtls;
	    
	    result=doesSoundExistInProfile(jSound.id);
	    var fapm;
	    if(result) {
	    	//console.log("found ",jSound);
	    	fapm="fa-minus";
	    }
	    else {
	    	fapm="fa-plus";
	    }
	    jSound.ad = "<i class='fa "+fapm+" sound-ctl' id='addRemoveSoundImg-"+jSound.id+"' aria-hidden='true' onclick='addRemoveProfileSound("+jSound.id+");'></i>";

		//console.log("jSound",jSound);
		sdata[j] = jSound;
		j++;
	});
	console.log("done loading sounds sdata",sdata);
	stLoaded=true;
	init();
}

function proccessProfileSoundData(result) {
	var jpSounds=result.jProfileSounds;
	pdata = [];
	var j=0;
	var len = Object.keys(jpSounds).length;
	$.each(jpSounds, function(key, bps) {
		//console.log("bps",bps);
		//console.log("bpId "+bps.bpId);
		var os = bps.bpData.os;
		var seq = bps.bpData.seq;
		//console.log("os."+os.id);

		bps.id=key;
		var ud = "";
		if(seq!=0)
			ud+="<i class='fas fa-caret-up' onclick='moveUpDown("+key+",0);'></i>";
		if(seq!=(len-1))
		    ud+="<i class='fas fa-caret-down' onclick='moveUpDown("+key+",1);'></i>";
		bps.ud=ud;
		os.pn=os.dn+"/"+os.fn;
		var psSrc = "/api/sounds/getSound?id="+os.id;
		var psCtls="<i class='fa fa-play sound-ctl' id='localProfileSoundImg-"+os.id+"' aria-hidden='true' onclick='playStopLocalSound("+os.id+");'></i>";
	    bps.playLocal = psCtls;
	    
	    var pr="<i class='fa fa-play sound-ctl-remote' id='remoteProfileSoundImg-"+os.id+"'";
	    pr+="aria-hidden='true' onclick='playStopRemoteSound("+os.id+");'></i>";
	    bps.playRemote=pr;
	    
	    bps.ad = "<i class='fa fa-minus sound-ctl' id='removeSoundImg-"+os.id+"' aria-hidden='true' onclick='doRemoveProfileSound("+os.id+");'></i>";
		//console.log("bps",bps);
		pdata[j] = bps;
		j++;
	});
	console.log("done loading pdata",pdata);
	stpLoaded=true;
	init();
}
function drawSoundGrid() {
	//console.log("drawSoundGrid",result);

	var columns = [
		{"data":"id"},
		{"data":"dn"},
		{"data":"fn"},
		{"data":"pn"},
		{"data": "playLocal"},
		{"data":"ad"}
	]
	
	var coldefs = [
        {
            "targets": [ 0,1,2 ],
            "visible": false,
            "searchable": false,
            "orderable":false,
        },
        {
            "targets": [ 4,5 ],
            "orderable":false,
            "searchable": false,
        },
	]
	
	var options = {
			editable: true,
			autoEdit:true,
			enableFiltering: false,
			asyncEditorLoading: true,
			autoHeight: false,
	};
		
	soundsTable = $('#soundsTable').DataTable({
		"data": sdata,
        "columns": columns,
        "columnDefs":coldefs,
        "order": [[3, 'asc']],
        "lengthMenu": [[20, 200, -1], [10, 200, "All"]],
	});
	
	soundsTable.on('page', function () {
		console.log("event page soundTable");
		
	});
	soundsTable.on('draw-dt', function () {
		console.log("event draw soundTable");		
	});	
	console.log("soundsTable",soundsTable);
	stInit=true;
	init();	
}

function drawProfileSoundGrid() {
	
	var columns = [
		{"data":"id"},
		{"data":"bpData.os.dn"},
		{"data":"bpData.os.fn"},
		{"data":"bpData.os.pn"},
		{"data": "playLocal"},
		{"data":"playRemote"},
		{"data":"ad"},
		{"data":"ud"},
		{"data":"bpData.seq"},
	]
	
	var coldefs = [
        {
            "targets": [ 0,1,2,5 ],
            "visible": false,
            "searchable": false,
            "orderable":false,
        }];
	
	if(page=='sounds') {
        coldefs.push ({
            "targets": [ 3,4,6 ],
            "orderable":false,
            "searchable": false,
        });
        
        coldefs.push ({
            "targets": [ 7],
            "orderable":false,
            "searchable": false,
            "visible":true
        });
        coldefs.push ({
            "targets": [ 8 ],
            "orderable":false,
            "searchable": false,
            "visible":false
        });
	}
    else if(page=='broadcast') {
        coldefs.push ({
            "targets": [ 3,4,5,6 ],
            "orderable":false,
            "searchable": false,
        });
        
        coldefs.push ({
            "targets": [ 7],
            "orderable":false,
            "searchable": false,
            "visible":true
        });
        coldefs.push ({
            "targets": [ 8 ],
            "orderable":false,
            "searchable": false,
            "visible":false
        });
	}
	
	soundsProfileTable = $('#profileSoundsTable').DataTable({
		"data": pdata,
        "columns": columns,
        "columnDefs":coldefs,
        "order": [[8, 'asc']],        
        "searching":	false,
        "info": false,
        "scrollCollapse": true,
		"scrollY":        "400px",
        "paging":         false
	});
	
	soundsProfileTable.on('draw', function () {
		console.log("event draw soundsProfileTable");
	});
	
	console.log("soundsProfileTable",soundsProfileTable);
	stpInit=true;
	init();
}

function drawProfileSoundGridForBroadcastPage() {
	
	var columns = [
		{"data":"id"},
		{"data":"bpData.os.dn"},
		{"data":"bpData.os.fn"},
		{"data":"bpData.os.pn"},
		{"data": "playLocal"},
		{"data":"playRemote"},
		{"data":"ad"},
		{"data":"ud"},
		{"data":"bpData.seq"},
	]
	
	var coldefs = [
        {
            "targets": [ 0,1,2,4,6,7 ],
            "visible": false,
            "searchable": false,
            "orderable":false,
        }];
	
	
        coldefs.push ({
            "targets": [ 3,4,5,6 ],
            "orderable":false,
            "searchable": false,
        });
        
        coldefs.push ({
            "targets": [ 7],
            "orderable":false,
            "searchable": false,
            "visible":true
        });
        coldefs.push ({
            "targets": [ 8 ],
            "orderable":false,
            "searchable": false,
            "visible":false
        });
	
	
	soundsProfileTable = $('#profileSoundsTable').DataTable({
		"data": pdata,
        "columns": columns,
        "columnDefs":coldefs,
        "order": [[8, 'asc']],        
        "searching":	false,
        "info": false,
        "scrollCollapse": true,
		"scrollY":        "400px",
        "paging":         false
	});
	
	soundsProfileTable.on('draw', function () {
		console.log("event draw soundsProfileTable");
	});
	
	console.log("soundsProfileTable",soundsProfileTable);
	stpInit=true;
	init();
}
function playStopLocalSound(id) {
	console.log("play/stop sound",id);
	var myAudio = $('#localSound-'+id);
	var myLocalSoundImage = $('#localSoundImg-'+id);
	console.log("myLocalSoundImage",myLocalSoundImage);
	var myLocalProfileSoundImage = $('#localProfileSoundImg-'+id);	
	console.log("myLocalProfileSoundImage",myLocalProfileSoundImage);
	
	if(! myAudio.hasClass('lsnr-added')) {
		addEventLsnr(id, myAudio, myLocalSoundImage, myLocalProfileSoundImage);
	}
	
	// which image should we use to check play/stop class?
	var useImage;
	if(myLocalSoundImage.length==0)
		useImage=myLocalProfileSoundImage;
	else
		useImage=myLocalSoundImage;
	console.log("useImage",useImage);
	
	if(useImage.hasClass("fa-play")) {
		console.log("play sound");
		myAudio.get(0).play();
		console.log("playing sound");
	}
	else {
		console.log("stop sound");
		audioStopped(id, myLocalSoundImage, myLocalProfileSoundImage);
		myAudio.get(0).pause();
		myAudio.currentTime=0;
	}	
}

function addEventLsnr(id, myAudio, myLocalSoundImage, myLocalProfileSoundImage) {
	myAudio.addClass("lsnr-added");
	myAudio.on("play", function() { 
		audioPlaying(id, myLocalSoundImage, myLocalProfileSoundImage); 
		});
	myAudio.on("ended", function() { 
		audioStopped(id, myLocalSoundImage, myLocalProfileSoundImage); 
		});
}
function audioPlaying(id, myLocalSoundImage, myLocalProfileSoundImage) {
	console.log("playing");
	if(myLocalSoundImage.length!==0) {
		myLocalSoundImage.removeClass("fa-play");
		myLocalSoundImage.addClass("fa-stop");
	}
	if(myLocalProfileSoundImage.length!==0) {
		myLocalProfileSoundImage.removeClass("fa-play");
		myLocalProfileSoundImage.addClass("fa-stop");
	}
	
}
function audioStopped(id, myLocalSoundImage, myLocalProfileSoundImage) {
	console.log("ended");
	if(myLocalSoundImage.length!==0) {
		myLocalSoundImage.removeClass("fa-stop");
		myLocalSoundImage.addClass("fa-play");
	}
	if(myLocalProfileSoundImage.length!==0) {
		myLocalProfileSoundImage.removeClass("fa-stop");
		myLocalProfileSoundImage.addClass("fa-play");
	}
}

function addRemoveProfileSound(id) {
	if(disableEdit()) return;
	console.log("addRemoveProfileSound "+id);
	psound = findSoundInProfile(id);
	if(psound==null) {
		console.log("profile sound not found, maybe we are adding");
	}
	else {
		console.log("profile sound found, maybe we are removing:",psound);
	}
	
	
	var myImage = $('#addRemoveSoundImg-'+id);
	//console.log("img",myImage);
	if(myImage.hasClass("fa-plus")) {
		if(psound!=null) {
			alert("can' add that, it's already in your favorites");
			enableEdit();
			return;
		}
		console.log("add",id);
		doAddProfileSound(id);
	}
	else {
		if(psound==null) {
			alert("can't remove that it is not in your favorites");
			enableEdit();
			return;
		}
		console.log("remove psound.id",psound.id);
		doRemoveProfileSound(id);
	}
}

// call the sound api to add the profile sound
function doAddProfileSound(id) {
	console.log("doAddProfileSound",id);
	addProfileSound(id);
}
// call the api to remove the profile sound
function doRemoveProfileSound(id) {
	console.log("doRemoveProfileSound",id);
	removeProfileSound(id);	
}
function finishAddProfileSound(id) {
	var myImage = $('#addRemoveSoundImg-'+id);
	myImage.removeClass("fa-plus");
	myImage.addClass("fa-minus");
	initType=0;
	console.log("set initType "+initType);
	stpLoaded=false;
	initVariables();
	soundsProfileTable.destroy();
	soundsTable.destroy();
	init();
}
function finishRemoveProfileSound(id) {
	var myImage = $('#addRemoveSoundImg-'+id);
	// change soundsTable ad from minus to plus
	myImage.removeClass("fa-minus");
	myImage.addClass("fa-plus");
	// remove soundsProfileable row
	initType=2;
	console.log("set initType "+initType);
	stpLoaded=false;
	init();
}
function moveUpDown(id, dir) {
	console.log("moveUpDown",id,dir);
	if(disableEdit()) return;
	updownProfileSound(id,dir);
}
function finishUdProfileSound() {
	console.log("finishUdProfileSound");
	initType=2;
	console.log("set initType "+initType);
	stpLoaded=false;
	init();
}

function doesSoundExistInProfile(sid) {
	//console.log("does "+sid+" exist?");	
	//console.log("spd",spd);
	var result=false;
	$.each(pdata, function(key, psound) {
		var pid = psound.bpData.os.id;
		//console.log("compare",sid,pid);
		if(pid==sid) {
			//console.log("matched",sid,pid);
			result=true;
			return result;
		}
	});
	return result;
}

function findSoundInProfile(sid) {
	console.log("find "+sid+" in profile");
	var spd = soundsProfileTable.data();
	console.log("spd",spd);
	var result=null;
	$.each(spd, function(key, sound) {
		var pid = sound.bpData.os.id;
		//console.log("compare",sid,pid);
		if(pid==sid) {
			console.log("found it:",sid,pid,sound);
			result=sound;
			return result;
		}
	});
	return result;
}

function doneDrawSoundsTable() {
	console.log('done draw doneDrawSoundsTable',soundsTable);
}
function doneDrawSoundsProfileTable() {
	console.log('done draw doneDrawSoundsProfileTable',soundsProfileTable);
}

function playingRemoteSound(id) {
	console.log("playing");
}

function playStopRemoteSound(id) {
	apiPlaySound(id);
}
function disableEdit() {
	if(editDisabled) {
		console.log("error, editing is already disabled");
		return true;
	}
	editDisabled=true;
	spinner.addClass("fa fa-spinner fa-spin");
	return false;
}
function enableEdit() {
	if(!editDisabled) {
		console.log("error, editing is already enabled");
	}
	editDisabled=false;
	spinner.removeClass("fa fa-spinner fa-spin");
}







