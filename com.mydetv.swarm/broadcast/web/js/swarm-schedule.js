function initSchedule(targetChannel, targetDate) {
    var url = "/api/schedule/"+targetChannel+"/"+targetDate+"/0";
    getSchedule(url);
}
function doSchedule() {
    var schannel = $('#schannel').find(":selected").val();
    var sdate = $('#sdate').find(":selected").val();
    var url = "/api/schedule/"+schannel+"/"+sdate+"/";
    if ($('#showCommercials').is(":checked"))
        url+=1;
    else
        url+=0;
    getSchedule(url);
}

function plusDay() {
    var schannel = $('#schannel').find(":selected").val();
    var sdate = $('#sdate').find(":selected").val();
    var url = "/api/schedule/"+schannel+"/"+sdate+"/";
    if ($('#showCommercials').is(":checked"))
        url+=1;
    else
        url+=0;
    url+="/plus";
    getSchedule(url);
}
function minusDay() {
    var schannel = $('#schannel').find(":selected").val();
    var sdate = $('#sdate').find(":selected").val();
    var url = "/api/schedule/"+schannel+"/"+sdate+"/";
    if ($('#showCommercials').is(":checked"))
        url+=1;
    else
        url+=0;
    url+="/minus";
    getSchedule(url);
}

function getSchedule(url) {
    console.log("go "+url);
    $.ajax({  
        url: url,
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            //console.log(response);
            $('#schedDiv').replaceWith("<div class='container col-lg-12' id='schedDiv'>"+response+"</div>");
            },
        error: function () {
            console.log("error");
        }
    });
}


