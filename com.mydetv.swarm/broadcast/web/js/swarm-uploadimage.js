
var myCroppie;
var origFilename;

function checkImage() {

	console.log("check image");

    var files = $('#fileElem');
    if(files===undefined)
        return;
    if(files[0] === undefined) {
        console.log("no file");
        return;
    }
    //var file = files[0];
    var file = document.getElementById('fileElem').files[0];
    
    var supportedFormats = ['image/jpg','image/jpeg','image/gif','image/png'];
    var errors=0;
    //
    
    //var file = files[0];
    console.log("size:"+file.size+",type:"+file.type);
    if (file.size > 1024*1024*5) {
        $('#uploadWarningSize').addClass('redText');
        errors++;
    }
    if (0 > supportedFormats.indexOf(file.type)) {
        $('#uploadWarningType').addClass('redText');
        errors++;
    }
    if(errors>0) {
        $('#uploadImageButton').prop( "disabled", true);
        return;
    }
    console.log("image "+file.name+" seems fine");
    $('#uploadImageButton').prop( "disabled", false);
    getImageFromUserFilesystem(file);
}

function getImageFromUserFilesystem(file) {
    
    var img=new Image();
    img.id='imageBlob';
    img.file=file;
    img.src = URL.createObjectURL(file);
    img.subcat=subcat;
    img.maincat=maincat;
    console.log("check1");
      img.onload = function() {
        cropImage(img, img.file); 
      };

    console.log("set");
}

function cropImage(img, file) {
    origFilename=file.name;
    console.log("open dialog for "+origFilename);  
    var opt = {
        autoOpen: false,
        title: 'Edit Image',
        width: 350,
        height: 400,
        dialogClass: "no-close",
        buttons: [
            {
                text: "Accept",
                click: function() {
                    acceptImage();
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Rotate Left",
                click: function() {
                    rotateImage(90);
                }
            },
            {
                text: "Rotate Right",
                click: function() {
                    rotateImage(-90);
                }
            }
        ]
    };
    var imageDialog = $("#imageDialog").dialog(opt);
    imageDialog.dialog("open");
    
    console.log("crop");
    
    var croppieDiv=$('#croppieDiv');
    if(!croppieDiv.data('croppie')) {
        myCroppie = croppieDiv.croppie({
            viewport: { width: 192, height: 192 },
            boundary: { width: 200, height: 200 },
            exif:true,
            enableOrientation: true
        });
    }
    
    myCroppie.croppie('bind', {
        url: img.src
    });
}
function rotateImage(amount) {
    myCroppie.croppie('rotate', amount);
}
var finalFileUrl;
function acceptImage() {
    console.log("acceptImage");
    var data = myCroppie.get();
        myCroppie.croppie('result', {
            type: 'blob',
            size: 'viewport',
            format: 'png'
            
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            console.log("croppie result ",resp);
            finalFileUrl = window.URL.createObjectURL(resp);
            sendSubcatFile(resp);
        });
}

var imageData;
var myFile;
var xhr;
function sendSubcatFile(file) {
	var url="/api/admin/videocats/uploadVCSI";
	console.log("sending file to "+url);
    myFile=file;
    var reader = new FileReader();
    
    xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');  
    xhr.addEventListener("load", addSubcatImage, false);
    xhr.addEventListener("error", uploadFailed, false);
    xhr.addEventListener("abort", uploadCanceled, false);
    console.log("sendFile");
    reader.onload = function(evt) {
        imageData = evt.target.result;
        var formData = new FormData();
        formData.append("origFilename", origFilename);
        formData.append("vcm", maincat);
        formData.append("imageData", file);
        console.log("check4 "+imageData.length);
        xhr.send(formData);
    };
    
    reader.readAsBinaryString(file); 
}


function addSubcatImage(e) {
	console.log("xhr",xhr.responseText);
	var osci = $.parseJSON(xhr.responseText);
	var tr = "<tr>";	
	tr+="<td>"+osci.id+"</td>";
	tr+="<td>"+osci.filename+"</td>";
	tr+="<td><img width='50px' src='"+osci.fpn+"'></td>";
	tr+="<td><input type='radio' name='selectedImage' value='"+osci.id+"'>";
	//tr+="<td><input type='radio' name='selectedImage' id='"+osci.id+"value='"+osci.id+"'>";
	tr+="</tr>";
	$('#tblSubcatImages').append(tr);
}

function uploadFailed(evt) {
  alert("There was an error attempting to upload the file.");
}

function uploadCanceled(evt) {
  alert("The upload has been canceled by the user or the browser dropped the connection.");
}

function editProfileImage(obj) {
    console.log("editProfileImage:",obj);
    var id=$(obj).attr('id');
    console.log("id:",id);
    var array = id.split('-');
    var type = array[0];
    var wid=array[2];
    var i = array[1];
    console.log("id:"+id+",i:"+i+",wid:"+wid);
    if(type=='dpi')
        deleteProfileImage(i,wid);
    else if(type=='spi')
        selectProfileImage(i,wid);
    else
        console.log("didn't recognize type:"+type);
}


