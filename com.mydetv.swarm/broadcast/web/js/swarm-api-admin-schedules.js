

function getSchedules(channelId) {
	console.log("get schedules "+channelId);
	var url="/api/schedules";
	var data = {};
	data["channelId"] = 0+channelId;
	$.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            drawSchedules(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });		
}

function doSchedule() {
    var channelId = $("#schannel").val();
    console.log("changing channel to "+channelId);
    getSchedules(channelId);
    //var newurl = $(location).attr('protocol')+"//"+$(location).attr('host')+$(location).attr('pathname')+"?channel="+channelId;
    //console.log("newurl:"+newurl);
    //window.location.replace(newurl);
}

function fctChangeConvertType(ce, role, wuid) {
    
    var selected = $('#ctypes option:selected').text();
    console.log("fctChangeConvertType "+selected);
    
    var url = $(location).attr('href');
    console.log("url:"+url);
    
    console.log("host:"+$(location).attr('host'));       
    console.log("hostname:"+$(location).attr('hostname'));    
    console.log("pathname:"+$(location).attr('pathname'));        
    console.log("href:"+$(location).attr('href'));        
    console.log("port:"+$(location).attr('port'));        
    console.log("protocol:"+$(location).attr('protocol'));
    console.log("search:"+$(location).attr('search'));
    var newurl = $(location).attr('protocol')+"//"+$(location).attr('host')+$(location).attr('pathname')+"?convertType="+selected;
    console.log("newurl:"+newurl);
    window.location.replace(newurl);

}

function sendScheduledChange(schannel, item) {
    console.log("sendScheduledChange",item);
    console.log("schannel:",schannel);
    var data = {};
    data["channel"] =  schannel;
    data["item"] = item;
    console.log("data",data);
    
    $.ajax({
        url: "/api/schedules/changeChannelScheduled",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized , refresh this page "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden , refresh this page "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect , refresh this page "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error, refresh this page");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}
