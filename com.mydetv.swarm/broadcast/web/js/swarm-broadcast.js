function wsBroadcaster(parsedMessage) {
    var broadcaster = parsedMessage.broadcaster;
    var args = parsedMessage.args;
    switch(broadcaster) {
    	// lights
        case 'changeLight':
            changeLight(args);
            break;
        case 'updateLights':
            updateLights(args);
            break;
        case 'removeLights':
            removeLights();
            break;
            
        // shared
        case 'error':
            handlePresenterError(parsedMessage.error);
            break;
        
        default:
            console.log("didn't recognize command |"+broadcaster+"|");
    }
}

function onChannelActivate() {
    var channelId = $('#channelId').text();
    var checked = false;
    if ($('#channelActivate').is(":checked")) checked=true;
    console.log("channel:"+channelId+", "+checked);
    
    if(checked) {
        var message = {
            broadcaster : 'activateChannel',
            channelId: channelId
        };
        sendBroadcaster(message);
    }
    else {
        bootbox.confirm("Are you sure? This could make viewers very unhappy.  Did you warn them?", function(result){
            if(result) {
                console.log("really tearing down the channel now");
                var message = {
                    broadcaster : 'inactivateChannel',
                    channelId: channelId
                };
            sendBroadcaster(message);
            }
        });
    }
}

function onRemoteSourceLive() {
    var message={};
    message.broadcaster = 'goRemoteSourceLive';
    message.channelId = $('#channelId').text();
    sendBroadcaster(message);
}

function onRemoteSid() {
    var message={};
    message.broadcaster = 'goRemoteSid';
    message.channelId = $('#channelId').text();
    sendBroadcaster(message);
}
function onStudioSid() {
    var message = {
    	broadcaster : 'goStudioSid',
        channelId: $('#channelId').text()
    };
    sendBroadcaster(message);
}
function onStudioLive() {
    var message = {
        broadcaster : 'goStudioLive',
        channelId: $('#channelId').text()
    };
    sendBroadcaster(message);
}

function onRecording() {
    var channelId = $('#channelId').text();
    var message = {
        channelId: channelId
    };
    
    if ($('#recording').is(":checked")) {      
        message.broadcaster = "goStartRecording";      
    }
    else {
        message.broadcaster = "goStopRecording";    
    }
    sendBroadcaster(message);
}

// shared helpers
function sendBroadcaster(message) {
    message.mydetv = mydetv;
    message.verb = 'broadcaster';
    sendMessage(message);
}

function handlePresenterError(error) {
    console.log("handling broadcasterError:"+error);
    switch(error) {
        case 'errorLiveOn':
            $('#sid').prop('checked', true);
            $('#live').prop('checked', false);
            bootbox.alert("Error, can't make the stream live");
            break;
        case 'errorLiveOff':
            $('#sid').prop('checked', false);
            $('#live').prop('checked', true);
            
            break;
        default:
        	bootbox.alert(error+", please return home and return to reset your conrols");
    }
}
/*
// not migrated:
function startLocalLive(channelId) {
	console.log("startLocalLive");
    showLocalSpinner();
    localVideo = document.querySelector('video#localVideo');
    var constraints = { audio: true, video:true};

    navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream1) {
        console.log("getting mediaStream");
        showLocalSpinner();
        localMediaStream=mediaStream1;
        localVideo.srcObject = mediaStream1;
        localVideo.onloadedmetadata = function() {
            console.log("playing local video");
            showLocalVideo();
            //sendStartLive(channelId);
        };
        mediaStream1.oninactive = function() {
            console.log('Stream inactive');
            //showLocalSpinner();
        };
    })
    .catch(function(err) { console.log(err.name + ": " + err.message); });
}

function stopLocalLive(channelId) {
    showLocalSpinner();
    let tracks = localMediaStream.getTracks();

    tracks.forEach(function(track) {
        track.stop();
    });

    localVideo.srcObject = null;
    var message = {
    	broadcaster : 'stopLive',
	    channelId: channelId
    };
    sendBroadcaster(message);
}
*/



function sendStartRemoteLive(channelId) {
    var message = {
        broadcaster : 'startRemoteLive',
        channelId: channelId
    };
    sendBroadcaster(message);
}
function sendStopRemoteLive(channelId) {
    var message = {
        broadcaster : 'stopRemoteLive',
        channelId: channelId
    };
    sendBroadcaster(message);
}

function removeLights() {
    $('#lights').addClass('d-none');
}
function updateLights(content) {
	console.log("updateLights")
	$('#lights').removeClass('d-none');
	$('#lights').html(content);
	
}
function changeLight(args) {
    var light="#light-"+args.light;
    var control=args.control;
    var lightId = $(light);
    console.log("|"+args.light+"| "+control+"|",lightId);
    if(control==="off") {
        lightId.css('opacity','0.1');
        lightId.css('animation','');
        lightId.css('animation-iteration-count','');
    }
    else if(control==="on") {
        lightId.css('opacity','1.0');
        lightId.css('animation','');
        lightId.css('animation-iteration-count','');
    }
    else if(control==="flash") {
        lightId.css('opacity','1.0');
        lightId.css('animation','blink 1s');
        lightId.css('animation-iteration-count','infinite');
    }
    else if(control==="double") {
        lightId.css('opacity','1.0');
        lightId.css('animation','blink .5s');
        lightId.css('animation-iteration-count','infinite');
    }
}
function onProfileSilence() {
    var channelId = $('#channelId').text();
    var message = {
        broadcaster : 'silence',
        channelId: channelId
    };
    sendBroadcaster(message);
}
function onProfilePlay() {
    var channelId = $('#channelId').text();
    var message = {
        broadcaster : 'play',
        channelId: channelId
    };
    sendBroadcaster(message); 
}

