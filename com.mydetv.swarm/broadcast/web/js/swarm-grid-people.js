
var grid;
var dataView;
var data = [];
var columns = [
	{id: "wuid", name: "wuid", field: "wuid", sortable:true, width: 20}
	,{id: "username", name:"username", field:"username", sortable:true, width: 40}
	,{id: "dtAdded", name: "dtAdded", field: "dtAdded", sortable:true, width: 20}
	,{id:"emails", name:"emails", field:"strEmails", width: 60}
	,{id:"roles",name:"roles",field:"strRoles", width: 60}

	];
var options = {};

function drawPeople(result) {
	
	var j=0;
	$.each(result, function(key, person) {
		person.id="id_"+person.wuid;
		console.log("person",person);      	
		data[j] = person;
		j++;
	});
	

	dataView = new Slick.Data.DataView({ inlineFilters: true });

	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.endUpdate();
	dataView.refresh();

	grid = new Slick.Grid("#myGrid", dataView, columns, options);
	grid.autosizeColumns();
	grid.onClick.subscribe(function(e,args) {
		//console.log("e:",e,args);
		var item = grid.getDataItem(args.row);
		console.log("item:",item.wuid);
		window.open("/admin/person?wuid="+item.wuid);
	});
	grid.onSort.subscribe(function(e, args) {
		console.log("sorting:"+args.sortCol.field);
		// args.multiColumnSort indicates whether or not this is a multi-column sort.
		// If it is, args.sortCols will have an array of {sortCol:..., sortAsc:...} objects.
		// If not, the sort column and direction will be in args.sortCol & args.sortAsc.

		// We'll use a simple comparer function here.
		var comparer = function(a, b) {
			return (a[args.sortCol.field] > b[args.sortCol.field]) ? 1 : -1;
		}

		// Delegate the sorting to DataView.
		// This will fire the change events and update the grid.
		dataView.sort(comparer, args.sortAsc);
		dataView.refresh();
	});

	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.render();
	});
	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.render();
	});
};



