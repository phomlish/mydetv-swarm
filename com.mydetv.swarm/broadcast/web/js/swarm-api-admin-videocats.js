var fileElem;
$(document).ready(function() {
	fileElem = $('#fileElem');
	changeMaincat();
});

function getSubcats(maincat) {
	var url="/api/admin/videocats/getsubcat?maincat="+maincat;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            drawMaincatDefaultImage(result);
            drawSubcats(result);
            drawSubcatImages(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function changeMaincat() {
	maincat = $("#maincat").val();
	console.log("maincat:",maincat);
	getSubcats(maincat);	
}

function drawMaincatDefaultImage(result) {
	console.log("vcmi",result.vcmi);
	if(result.vcmi.filename!='none') {
		var img = "<image src='/ext/i/vsc/"+result.vcmi.filename+"' width='50px'>";
		$("#maincatDefaultImage").html(img);
	}
	else
		$("#maincatDefaultImage").html("");
	
}
function drawSubcats(result) {
	
	var rv = "<table class='table-bordered'>";
	rv+="<thead>";
	rv+="<tr>";
	rv+="<th>id</th><th>name</th><th>description</th><th>image</th><th>set image</th>";
	rv+="</tr>";
	rv+="</thead>";
	
	rv+="<tbody>";	
	$.each(result.jsubcats, function(index, osc) {
		console.log("osc",osc);
		
		rv+="<tr>";
		rv+="<td>"+osc.id+"</td>";
		rv+="<td>"+osc.name+"</td>";
		rv+="<td><input type='text' id='vscDesc-"+osc.id+"' onchange=changeSubcatDesc("+osc.id+") value='"+osc.description+"'></td>";
		
		rv+="<td id='divVscImage-"+osc.id+"'>";
		if(osc.image!=0) {
			rv+="<image src='/ext/i/vsc/"+osc.filename+"' width='50px'>";
		}
		rv+="</td>";
		rv+="<td>";
		rv+="<input type='button' class='btn' onclick='setVscDefaultImageVCS("+osc.id+");'>";
		rv+="</td>";
		rv+="<tr>";
	});
	rv+="</tbody>";
	
	rv += '</table>';
	$("#divSubcat").html(rv);
}
function drawSubcatImages(result) {
	
	var rv = "<table id='tblSubcatImages' class='table-bordered'>";
	rv+="<thead>";
	rv+="<tr>";
	rv+="<th>id</th><th>name</th><th>image</th><th>selected</th>";
	rv+="</tr>";
	rv+="</thead>";
	
	rv+="<tbody>";
	rv+="<tr><td>none</td><td></td><td></td>";
	rv+="<td><input type='radio' name='selectedImage' value='0' checked>";
	rv+="</tr>";
	
	$.each(result.jvscis, function(index, osci) {
		console.log("osci",osci);		
		rv+="<tr>";
		rv+="<td>"+osci.id+"</td>";
		rv+="<td>"+osci.filename+"</td>";
		rv+="<td><img width='50px' src='"+osci.fpn+"'></td>";
		rv+="<td><input type='radio' name='selectedImage' value='"+osci.id+"'>";
		rv+="<tr>";
	});
	rv+="</tbody>";
	
	rv += '</table>';
	$("#divSubcatImages").html(rv);
}

function changeSubcatDesc(vscId) {
	var desc = $('#vscDesc-'+vscId).val();
	console.log("changeSubcatDesc "+vscId+" "+desc);

	var data = {};
    data["vscId"] = vscId;
    data["desc"] = desc;
    $.ajax({
        url: "/api/admin/videocats/changeSubcatDesc",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("ok");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

var subcat;
var maincat;
function uploadVCSI() {

	maincat = $("#maincat option:selected").val();
	console.log("uploadVCSI "+maincat);
	fileElem.click();
}

function addNewVCS() {
	var newVscName = $('#newVscName').val();
	var newVscDesc = $('#newVscDesc').val();
	maincat = $("#maincat option:selected").val();
	
	if(newVscName=="" || newVscDesc=="") {
		alert("you must supply a name and description");
		return;
	}
	
    console.log("addNewVCS "+maincat+" "+newVscName+" "+newVscDesc);
    var data = {};
    data["name"] = newVscName;
    data["desc"] = newVscDesc;
    data["vcm"] = maincat;
    $.ajax({
        url: "/api/admin/videocats/addNewVCS",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

function setVscDefaultImageVCM() {
	var vcsi = $('input[name=selectedImage]:checked').val();
	console.log("set default main"+maincat,i);
	
	var data = {};
    data["vcm"] = maincat;
    data["vcsi"] = vcsi;
    $.ajax({
        url: "/api/admin/videocats/selectNewDefaultImageVCM",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result.filename);
            var img;
            if(result.filename=='none') 
            	img="";
            else
            	img = "<image src='/ext/i/vsc/"+result.filename+"' width='50px'>";
            $('#maincatDefaultImage').html(img);
            
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}
function setVscDefaultImageVCS(vscId) {
	var vcsi = $('input[name=selectedImage]:checked').val();
	console.log("set default sub "+vscId,i);

	var data = {};
    data["vcs"] = vscId;
    data["vcsi"] = vcsi;
    $.ajax({
        url: "/api/admin/videocats/selectNewDefaultImageVCS",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result.filename);
            var img;
            if(result.filename=='none') 
            	img="";
            else
            	img = "<image src='/ext/i/vsc/"+result.filename+"' width='50px'>";
            var target = "#divVscImage-"+vscId;
            $(target).html(img);
            
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}






