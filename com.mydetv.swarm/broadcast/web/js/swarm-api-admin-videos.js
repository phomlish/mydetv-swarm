function getVideosGridData() {
	var url="/api/videos";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            drawVideosGrid(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function fctChangeConvertType(ce, role, wuid) {
    
    var selected = $('#ctypes option:selected').text();
    console.log("fctChangeConvertType "+selected);
    
    var url = $(location).attr('href');
    console.log("url:"+url);
    
    console.log("host:"+$(location).attr('host'));       
    console.log("hostname:"+$(location).attr('hostname'));    
    console.log("pathname:"+$(location).attr('pathname'));        
    console.log("href:"+$(location).attr('href'));        
    console.log("port:"+$(location).attr('port'));        
    console.log("protocol:"+$(location).attr('protocol'));
    console.log("search:"+$(location).attr('search'));
    var newurl = $(location).attr('protocol')+"//"+$(location).attr('host')+$(location).attr('pathname')+"?convertType="+selected;
    console.log("newurl:"+newurl);
    window.location.replace(newurl);

}

function sendCategory(item) {
    console.log("sending category to server",item);
    
    $.ajax({
        url: "/api/video/category",
        type: "POST",
        data: JSON.stringify(item),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("video updated");
                    break;
                default:
                    
            }
            // success finally
            //alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

function sendDoNotUse(item) {
    console.log("sending donotuse to server",item);
    
    $.ajax({
        url: "/api/video/donotuse",
        type: "POST",
        data: JSON.stringify(item),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("updated");
                    break;
                default:
                    
            }
            // success finally
            //alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

function sendTitle(item) {
    console.log("sending title to server",item);
    
    $.ajax({
        url: "/api/video/title",
        type: "POST",
        data: JSON.stringify(item),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("updated");
                    break;
                default:
                    
            }
            // success finally
            //alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

$(document).ready(function() {
  $(".fancybox").fancybox({
    afterShow: function() {
      // After the show-slide-animation has ended - play the vide in the current slide
      this.content.find('video').trigger('play');
      
      // Attach the ended callback to trigger the fancybox.next() once the video has ended.
      this.content.find('video').on('ended', function() {
        $.fancybox.next();
      });
    }
  });
});