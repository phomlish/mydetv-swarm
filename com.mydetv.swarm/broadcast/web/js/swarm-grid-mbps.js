
var grid;
var dataView;
var data = [];
var catSel = {};
var columns;
var jMbps;
var jvcs;
var jvcm;
var searchStringCN = "";
var searchStringPN = "";

$(document).ready(function() {
	searchStringPN=$('#pnSearch').val();
	searchStringCN=$('#cnSearch').val();

	$("#pnSearch").keyup(function (e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		searchStringPN = this.value;
		updateFilter();
	});
	$("#cnSearch").keyup(function (e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		searchStringCN = this.value;
		updateFilter();
	});
	getMbpsGridData();
});

function drawMbpsGrid(result) {
	jMbps=result.jMbps;
	jvcs=result.jvcs;
	jvcm=result.jvcm;
	//console.log("jMbps",jMbps);

	var columns = [
		{id: "mbp", name: "mbp", field: "idMbp", sortable:true, maxWidth:50}
		,{id: "pkey", name: "pkey", field: "idVideo", sortable:true, maxWidth:50}
		,{id: "dtAdded", name: "dtAdded", field: "dtAdded", sortable:true, minWidth:140}
		,{id: "fn", name: "fn", field: "fn", sortable:true, minWidth:150}
		,{id: "fnVideo", name: "fnVideo", field: "fnVideo", sortable:true, minWidth:150}

		,{id: "details", name: "details", field: "details", sortable:true, editable:true, editor: textEditor,  minWidth:250}
		,{id: "notes", name: "notes", field: "notes", sortable:true, editable:true, editor: textEditor}

		,{id: "dvd", name: "dvd", field: "dvd", sortable:true, maxWidth:50}
		,{id: "converted", name: "converted", field: "converted", sortable:true, maxWidth:50}
		,{id: "edited", name: "edited", field: "edited", sortable:true, maxWidth:50}
		,{id: "showdate", name: "showdate", field: "showdate", sortable:true, maxWidth:100, editable:true, editor: textEditor}
		,{id: "showset", name: "showset", field: "showset", sortable:true, maxWidth:50, editable:true, editor: textEditor}
		,{id: "viewvideo", name: "viewvideo", field: "viewvideo", formatter: function (args2) {

			var cpn = data[args2].cpn;
			if(cpn===undefined||cpn===null || cpn.length<1)
				return "";
			var rv="";
			if(cpn.length>0) {
				//console.log("asking for ",data[args2]);
				rv+="<a data-fancybox='preview-video' data-width='640' ";
				rv+=" href='/api/video/getConverted?video="+cpn+"'>";
				rv+=" <img src='/s/img/Video-Icon.png' alt='"+cpn+"'height='16' width='16' />";
				rv+="</a>";
			}
			return rv;
		}
		}
		,{id: "fn2", name: "fn2", field: "fn2", sortable:true, minWidth:150}
		,{id: "fnOrig", name: "fnOrig", field: "fnOrig", sortable:true, minWidth:150}
		];

	var options = {
			editable: true,
			autoEdit:true,
			enableFiltering: true,
			asyncEditorLoading: true,
			autoHeight: false
	};

	var j=0;
	$.each(jMbps, function(key, jMbp) {
		jMbp.id="id_"+jMbp.idVideo;
		//console.log("jMbp",jMbp);      	
		data[j] = jMbp;
		j++;
	});
	console.log("done loading data");

	dataView = new Slick.Data.DataView({ inlineFilters: true });
	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.setFilterArgs({
		searchStringPN: searchStringPN,
		searchStringCN, searchStringCN
	});
	dataView.setFilter(myFilter);
	dataView.endUpdate();
	dataView.refresh();

	grid = new Slick.Grid("#myGrid", dataView, columns, options);
	grid.autosizeColumns();
	grid.init();
	grid.onSort.subscribe(function(e, args) {
		console.log("sorting:"+args.sortCol.field);
		var comparer = function(a, b) {
			return (a[args.sortCol.field] > b[args.sortCol.field]) ? 1 : -1;
		}
		dataView.sort(comparer, args.sortAsc);
		dataView.refresh();
	});

	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.invalidate(); 
		grid.render();
	});
	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.invalidate(); 
		grid.render();
	});
}

function textEditor(args) {
	var $input;
	var defaultValue;
	var scope = this;

	this.init = function () {
		$input = $("<INPUT type=text class='editor-text' />")
		.appendTo(args.container)
		.on("keydown.nav", function (e) {
			if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
				e.stopImmediatePropagation();
			}
		})
		.focus()
		.select();
	};

	this.destroy = function () {
		$input.remove();
	};

	this.focus = function () {
		$input.focus();
	};

	this.getValue = function () {
		return $input.val();
	};

	this.setValue = function (val) {
		$input.val(val);
	};

	this.loadValue = function (item) {
		defaultValue = item[args.column.field] || "";
		$input.val(defaultValue);
		$input[0].defaultValue = defaultValue;
		$input.select();
	};

	this.serializeValue = function () {
		return $input.val();
	};

	this.applyValue = function (item, state) {
		item[args.column.field] = state;
		console.log("new title:",item);
		console.log("args:",args);
		sendText(item, args.column.name);
	};

	this.isValueChanged = function () {
		return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
		if (args.column.validator) {
			var validationResults = args.column.validator($input.val());
			if (!validationResults.valid) {
				return validationResults;
			}
		}

		return {
			valid: true,
			msg: null
		};
	};

	this.init();
}

//****
function doNotUseEditor(args) {
	var $select;
	var defaultValue;
	var scope = this;

	this.init = function () {
		$select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
		$select.appendTo(args.container);
		$select.focus();
	};

	this.destroy = function () {
		$select.remove();
	};

	this.focus = function () {
		$select.focus();
	};

	this.loadValue = function (item) {
		defaultValue = !!item[args.column.field];
		if (defaultValue) {
			$select.prop('checked', true);
		} else {
			$select.prop('checked', false);
		}
	};

	this.preClick = function () {
		$select.prop('checked', !$select.prop('checked'));
	};

	this.serializeValue = function () {
		return $select.prop('checked');
	};

	this.applyValue = function (item, state) {
		item[args.column.field] = state;
		sendDoNotUse(item);
	};

	this.isValueChanged = function () {
		return (this.serializeValue() !== defaultValue);
	};

	this.validate = function () {
		return {
			valid: true,
			msg: null
		};
	};

	this.init();
}
// ****
function titleEditor(args) {
	var $input;
	var defaultValue;
	var scope = this;

	this.init = function () {
		$input = $("<INPUT type=text class='editor-text' />")
		.appendTo(args.container)
		.on("keydown.nav", function (e) {
			if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
				e.stopImmediatePropagation();
			}
		})
		.focus()
		.select();
	};

	this.destroy = function () {
		$input.remove();
	};

	this.focus = function () {
		$input.focus();
	};

	this.getValue = function () {
		return $input.val();
	};

	this.setValue = function (val) {
		$input.val(val);
	};

	this.loadValue = function (item) {
		defaultValue = item[args.column.field] || "";
		$input.val(defaultValue);
		$input[0].defaultValue = defaultValue;
		$input.select();
	};

	this.serializeValue = function () {
		return $input.val();
	};

	this.applyValue = function (item, state) {
		item[args.column.field] = state;
		console.log("new title:",item);
		sendTitle(item);
	};

	this.isValueChanged = function () {
		return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
		if (args.column.validator) {
			var validationResults = args.column.validator($input.val());
			if (!validationResults.valid) {
				return validationResults;
			}
		}

		return {
			valid: true,
			msg: null
		};
	};

	this.init();
}


function myFilter(item, args) {
	//console.log("item args",item,args);
	if(args.searchStringCN == "" && args.searchStringPN == "") {
		return true;
	}
	if(args.searchStringPN != "" && item["cpn"].indexOf(args.searchStringPN) == -1) {
		return false;
	}
	if(args.searchStringCN != "" && item["vcsName"].indexOf(args.searchStringCN) == -1) {
		return false;
	}
	return true;
}

//wire up the search textbox to apply the filter to the model


function updateFilter() {
	dataView.setFilterArgs({
		searchStringPN: searchStringPN,
		searchStringCN:searchStringCN
	});
	dataView.refresh();
	grid.invalidate(); 
	grid.render();
}

function SelectEditorCatMain(args) {
	SelectEditor(this, args, "catMain");
}
function SelectEditorCatSub(args) {
	SelectEditor(this, args, "catSub");
}

//**** NEW ****
function SelectEditor(caller, args, seType) {
	console.log("SelectEditor "+seType,args);
	var $input;
	var defaultValue;
	var calendarOpen = false;

	caller.keyCaptureList = [ Slick.keyCode.UP, Slick.keyCode.DOWN, Slick.keyCode.ENTER ];

	caller.init = function () {
		//console.log("init",args);
		$input = $('<select></select>');
		$input.width(args.container.clientWidth + 3);
		if(seType=="catSub")
			PopulateSelectCatSub(args.item, $input[0], args.column.dataSource, true);
		$input.appendTo(args.container);
		$input.focus().select();

		$input.select2({
			placeholder: '-',
			allowClear: true
		});
	};

	caller.destroy = function () {
		console.log("destroy");
		$input.select2('destroy');
		$input.remove();
	};

	caller.show = function () {
	};

	caller.hide = function () {
		$input.select2('results_hide');
		console.log("hide");
	};

	caller.position = function (position) {
	};

	caller.focus = function () {
		$input.select2('input_focus');
	};

	caller.loadValue = function (item) {
		//console.log("loadValue args",args);
		//console.log("loadValue item",item);
		defaultValue = item.catId;
		//console.log("loadValue defaultValue:",defaultValue);
		$input.val(defaultValue);
		$input[0].defaultValue = defaultValue;
		$input.trigger("change.select2");
	};

	caller.serializeValue = function () {
		console.log("serialize");
		return $input.val();
	};

	caller.applyValue = function (item, state) {
		item.catId = state;
		sendCategory(item);
	};

	caller.isValueChanged = function () {
		return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	caller.validate = function () {
		return {
			valid: true,
			msg: null
		};
	};

	caller.init();
}

function SelectFormatterCatSub(row, cell, value, columnDef, dataContext) {
	var pkey = dataContext.pkey;
	var jVideo = jMbps[pkey];
	return jVideo.vcsName;
}

function PopulateSelectCatSub(item, select, dataSource, addBlank) {
	console.log("PopulateSelectCatSub:",item);

	$.each(jvcs, function(index, value) {

		if(value.catMain==item.vcmId) {
			console.log("value",value);
			var selected="";
			if(item.catSub==value.idvideo_category) {
				selected=" selected='selected' "; 	
				console.log("selected "+value.name)
			}
			var newOption = new Option(value.name, index, false, selected);
			select.appendChild(newOption);
		}   
	});
};

function SelectFormatterViewVideo(row, cell, value, columnDef, dataContext) {
	var pn2=dataContext.pn2;
	var rv=""
		rv+="<a data-fancybox='preview-video' data-width='640' ";
	rv+=" href='/api/video/getConverted?video="+pn2+"'>";
	rv+=" <img src='/s/img/Video-Icon.png' alt='"+pn2+"'height='16' width='16' />";
	rv+="</a>";
	return rv;

}

function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
	return value ? "<img src='../images/tick.png'>" : "<img src='../images/delete.png'>";
}



