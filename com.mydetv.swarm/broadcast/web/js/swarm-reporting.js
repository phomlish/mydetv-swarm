function badVideo(channel, pkey) {
	
    bootbox.prompt({
        title: "Describe how this video is bad?  Choppy video? Bad sound? Incomplete?",
        inputType: 'textarea',
        callback: function (result) {
            //console.log("result:",result);
            if(result==null) 
            	return;
            var data = {};
            data.channel = channel;
            data.pkey=pkey;
            data.reason=result;
            console.log(data);
            $.ajax({  
                url: "/api/video/bad/",
                type: 'POST',
                contentType: "application/json",
                //contentType: "text/plain",
                data: JSON.stringify(data),
                success: function() {
                    bootbox.alert("thanks");

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log("ajaxError "+xhr.status + " " +thrownError);
                    bootbox.alert("ops, an error occured!");
                }
            });
        }
    });
    
}

function editVideoTitle(channel, pkey) {
	var title=$('#videoTitleText').html();
	var ofn = $('#ofn').text();
	console.log("channel:"+channel+",pkey:"+pkey+",title |"+title+"|",ofn);
	var input = $("<input id='newTitle' name='newTitle' type='text' class='fieldname' size='40' value='"+title+"' />");
	$("body").append(input);
	var newTitle = $('#newTitle').prop('outerHTML');
	var message = "<div>"+input+"</div>";
	
	console.log("newTitle:",newTitle);
	
    bootbox.dialog({
        title: "New Title "+ofn,
        inputType: 'text',
        message: input,
        show: true,
        onEscape: function() {
          return true;
        },
        buttons: [
            {
              label: "Save",
              className: "btn btn-primary pull-left",
              callback: function() {

            	var changedTitle = $('#newTitle').val();
            	console.log("changedTitle:",changedTitle);
                var item={};
                item.pkey=pkey;
                item.title=changedTitle;
                sendNewTitle(item);
              }
            },
            {
              label: "Close",
              className: "btn btn-default pull-left",
              callback: function() {
                console.log("just do something on close");
              }
            }
          ]
          
    });
}

function sendNewTitle(item) {
	$.ajax({
        url: "/api/video/title",
        type: "POST",
        data: JSON.stringify(item),
        contentType: "application/json",
        success: function (result) {
            console.log("result:",result);
            switch (result) {
                case "ok":
                    console.log("updated");
                    alert("ok");
                    break;
                default:
                    
            }
            // success finally
            //alert("got it");
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+mydetvUrl);
                    break;
                case 403:
                    alert("forbidden "+mydetvUrl);
                    break;
                case 0:
                  alert("unable to connect to "+mydetvUrl);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });
}

function reportPerson(personId) {
	var form = $('');
    bootbox.dialog({
        title: "Why are you reporting this person?<br><form><textarea rows='6' name='reason' id='reason' /></textarea</form>",
        message: "Please be detailed. We will use our extensive logging combined with your details to determine appopriate action up to and including banning their account.",
        buttons: {
            cancel: {
                label: "cancel",
                className: 'btn-danger',
                callback: function(){
                    console.log("cancel");
                }
            },
            ok: {
                label: "submit",
                className: 'btn-info',
                callback: function(){
                    console.log("submit "+personId);
                    var reason = $('#reason').val();
                    if(reason===undefined||reason==null||reason=="") {
                    	bootbox.alert("You must supply a reason, please try again");
                    	return;
                    }
                    
                    var message = {
                            'mydetv':mydetv,
                            verb : 'reportPerson',
                            personId: personId,
                            reason: reason
                        };
                    sendMessage(message);
                }
            }
        }
    });
}



