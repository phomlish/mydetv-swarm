var ws;
var videoElement;

var webRtcPeer;
var isPresenter = false;
var isViewer = false;
var webrtcImage;
var datafileDownloadTime=0;
var datafileDownloadFile="random-chars-1024.txt";
var datafileDownloadUrl="/s/random/"+datafileDownloadFile;
var datafileStorageName="random-1024";
var progressbarTotal;
var progressbarFinished;
var progressbarCurrent;
var emojiElt;

function setupP2P() {
    setupEmoji();
}

function setupEmoji() {
	disableStopButton();

    emojione.imagePathPNG = '/ext/EmojiOne_4.0_128x128_png/';
    emojiElt = $("#chatMessage").emojioneArea({
        autocomplete   : false,
        useInternalCDN: false,
        imageType: 'png',
        inline: true,
        events: {
            keyup: function (editor, event) {
                //console.log('event:keypress', event.which); //work
                if(event.which == 13){
                    //console.log('event:keypress2', event.which); //work
                    event.preventDefault(); // 
                    sendChat();
                }
            }
        }
    });
  $("#message_box").scrollTop($('#message_box')[0].scrollHeight);    
}

function setupEmoji0() {
	disableStopButton();

    // setup emojis on the message text entry box
    $('#message').emojiPicker({
      height: '300px',
      width:  '450px',
      recentCount: 0
    });
}

// don't need all these callbacks, but a nice reference to have
function getDataFile(callback) {
    
    // get a file of random characters to do our bandwidth tests
    console.log("b4 datafile download");
    var dtStart;
    $.ajax({  
        url: datafileDownloadUrl,  
        dataType: "text",   
        beforeSend: function() {
            dtStart = new Date().getTime();
            console.log("[" + dtStart + "] " + "dtStart");
        },
        success: function(data) {
            var dtEnd = new Date().getTime();
            datafileDownloadTime = dtEnd - dtStart;
            sessionStorage.setItem(datafileStorageName, data);
            console.log("[" + dtEnd + "] " + "success download");
        },
        error: function() {
            console.log("ajaxError");
        },
        complete: function(){
            callback();
            } 
    });
}

window.onbeforeunload = function() {
    var time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "onbeforeunload");
    if(ws !== undefined)
    	ws.close();
}

function initWebsocket() {
    var time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "location:"+location.host);
    ws = new WebSocket('wss://' + location.host + '/ws');
    wsListeners();
}

// ********* WEBSOCKET **********
function wsListeners() {
    
    ws.onclose = function() {
        var time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "websocket has closed");
        $('#userMessage').replaceWith("<div class='blink' id='userMessage' style='color:red'>Lost Backend Connection<br>Please go Home</div>");
        showRemoteDancer();
        showLocalDancer();
        if(myRTCPeerConnectionsMap !== null) {
            console.log("closing RTCPeerConnections");
            myRTCPeerConnectionsMap.forEach(function(value, key, map) {
                console.log("closing RTCPeerConnection"+key);
                value.close();
            });
        }
        myRTCPeerConnectionsMap=undefined;
        
        if(localMediaStream!==undefined && localMediaStream!==null) {
            let tracks = localMediaStream.getTracks();
            if(tracks!==null) {
                tracks.forEach(function(track) {
                    track.stop();
                });
            }
        }
    };
    
    // When the connection is open, send init
    ws.onopen = function () {
        var channel = getParameterByName('channel');
        var time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "ws.onopen channel:"+channel);
        console.log("[" + time.toLocaleTimeString() + "] " + "init "+mydetv);
        DetectRTC.load(function() {
            //console.log("[" + time.toLocaleTimeString() + "] " + "DetectRTC:"+JSON.stringify(DetectRTC));
            response = {
                'verb':'init',
                'mydetv':mydetv,
                channel: channel,
                DetectRTC:DetectRTC,
                pathname:window.location.pathname,
                datafileDownloadTime:datafileDownloadTime
                };
            var myJSON = JSON.stringify(response); 
            ws.send(myJSON); // Send the message 'init' to the server
        });
        

    };
    
    // Log errors
    ws.onerror = function (error) {
        var time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "websocket error:"+error);
    };
    
    ws.onmessage = function(message) {
        var parsedMessage = JSON.parse(message.data);
        var time = new Date();
        if(parsedMessage.verb != "ping" && parsedMessage.verb != "chatMessage") {
            console.log("[" + time.toLocaleTimeString() + "] " + "Received message:"+message.data);
        }
        
        switch (parsedMessage.verb) {
        case 'ping':
            var myMessage = {
                'mydetv':mydetv,
                verb : 'pong',
            };
            sendMessage(myMessage);
            break;
        case 'setupRTCConnection':
            setupRTCConnection(parsedMessage);
            break;
        case 'removeRTCConnection':
            removeRTCConnection(parsedMessage);
            break;
        case 'userMessage': 
            userMessage(parsedMessage);
            break;
        case 'stopCommunication':
            dispose();
            break;
        case 'chatMessage':
            chatMessage(parsedMessage);
            break;
        case 'chatMessages':
            chatMessages(parsedMessage);
            break;
        case 'personInfo':
            msgPersonInfo(parsedMessage);
            break;
        case 'chatBlock':
            msgChatBlock(parsedMessage);
            break;
        case 'people':
            people(parsedMessage);
            break;
        case 'mydetvCustom':
            mydetvCustom(parsedMessage);
            break;

        case 'connectRemoteStream':
            connectRemoteStream(parsedMessage);
            break;
        case 'initProgressbarBw':
            initProgressbarBw(parsedMessage);
            break;
        case 'hideProgressbarBw':
            hideProgressbarBw();
            break;
        case 'initProgressbarTurn':
            initProgressbarTurn(parsedMessage);
            break;
        case 'updateProgressbarTurn':
            updateProgressbarTurn(parsedMessage);
            break;
        case 'hideProgressbarTurn':
            hideProgressbarTurn();
            break;
        case "u2u":
            u2uMessage(parsedMessage);
            break;
        case "newVideoTitle":
        	newVideoTitle(parsedMessage);
            break;
            
        case 'msgBroadcaster':
            wsBroadcaster(parsedMessage);
            break;
            
        // Changing html elements
        case 'UpdateElementProperty':
        	var ename = '#'+parsedMessage.ename;
        	$(ename).prop(parsedMessage.pname, parsedMessage.pvalue);
        	break;
        case 'UpdateELementAddClass':
        	var ename = '#'+parsedMessage.ename;
        	$(ename).addClass(parsedMessage.classname)
        	break;
        case 'UpdateELementRemoveClass':
        	var ename = '#'+parsedMessage.ename;
        	$(ename).removeClass(parsedMessage.classname)
        	break;        	
        case 'UpdateELementAddRemoveClass':
        	var ename = '#'+parsedMessage.ename;
        	$(ename).addClass(parsedMessage.aclassname)
        	$(ename).removeClass(parsedMessage.rclassname)
        	break;
        default:
            console.error('Unrecognized message', parsedMessage);
        }
    };
}

function presenter() {
    // this tells swarm they can stop sending us the kmedia feed
    myMessage = {
        'mydetv':mydetv,
        verb : 'presenter',
    };
    sendMessage(myMessage);
    
    isPresenter=true;
    time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "presenter");
    
    var options = {
		localVideo : videoElement,
		onicecandidate : onIceCandidate
	};
	webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options,
        function(error) {
			if (error) {
				return console.error(error);
			}
			webRtcPeer.generateOffer(onOfferPresenter);
			});
}
function onOfferPresenter(error, offerSdp) {
	if (error)
		return console.error('Error generating the offer');
	console.info('Invoking SDP offer callback function ' + location.host);
	var message = {
		id : 'presenter',
		sdpOffer : offerSdp
	};
	sendMessage(message);
}

function viewer() {
    myMessage = {
        'mydetv':mydetv,
        verb : 'viewer',
    };
    sendMessage(myMessage);
    
    isViewer=true;
    time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] " + "viewer");
}

function recieverGetReadyForBandwidthTest(parsedMessage) {
    hc = parsedMessage.hashcode;
    
}
function stop() {
	var message = {
		verb : 'stop'
	};
	sendMessage(message);
	dispose();
}

function dispose() {
	if (webRtcPeer) {
		webRtcPeer.dispose();
		webRtcPeer = null;
	}
	disableStopButton();
}

function disableStopButton() {
	enableButton('#presenter', 'presenter()');
	enableButton('#viewer', 'viewer()');
	disableButton('#stop');
}

function enableStopButton() {
	disableButton('#presenter');
	disableButton('#viewer');
	enableButton('#stop', 'stop()');
}

function disableButton(id) {
	$(id).attr('disabled', true);
	$(id).removeAttr('onclick');
}

function enableButton(id, functionName) {
	$(id).attr('disabled', false);
	$(id).attr('onclick', functionName);
}

function sendMessage(message) {
	var jsonMessage = JSON.stringify(message);
    
    if(message.verb != "pong") {
        time = new Date();
        console.log("[" + time.toLocaleTimeString() + "] " + "sending message:"+jsonMessage);
    }
	ws.send(jsonMessage);
}

/**
 * Lightbox utility (to display media pipeline image in a modal dialog)
 */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
	//event.preventDefault();
	$(this).ekkoLightbox();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function userMessage(message) {
    //console.log("statusMessage:"+message.statusMessage);
    $('#userMessagesOld').prepend("<li>"+message.userMessage+"</li>");
    $('#userMessage').html(message.userMessage);

}

function userStatusAlert() {
    var message = "messages:<br>\n";
    $('#userMessagesOld li').each(function(index) {
        //console.log("adding "+$(this).text());
        message+=$(this).text();
        message+="<br>\n";
    });
    bootbox.alert(message);
}

function newVideoTitle(message) {
	var title = message.title;
	$('#videoTitle').html(title);
}
function people(message) {
    var action = message.action;
    var person;
    switch(action) {
        case 'replace':
            $('#people').replaceWith(message.people);
            break;
        case 'newperson':
            $('#people').append(message.person);
            break;
        case 'removeperson':
            person = '#person-'+message.person;
            console.log("removing "+person);
            $(person).remove();
            break;
        default:
            console.log("didn't recognize people action "+action);
    }    
}

function showChatHelp() {
    bootbox.alert($('#chatHelp').html());
}

function sendChat() {
    console.log("chatting");

    var chatText = emojiElt.data("emojioneArea").getText();
    emojiElt.data("emojioneArea").setText('');
    
    if(chatText === undefined || chatText.length < 1) {
        console.log("empty chat?");
        return;
    }
    var people = getPeopleSelected();
    console.log("people",people);
    console.log("people",JSON.stringify(people));
    console.log("sending message "+chatText);
    var message = {
		verb : 'sendChat',
        mydetv: mydetv,
		theMessage : chatText,
        people: people
	};
	sendMessage(message);
}

// sometimes I really hate UI.  what are scrollHeight, clientHeight and scrollTop equivilets in jQuery>
function chatMessage(parsedMessage) {
    var message = parsedMessage.message;
    var message_box = document.getElementById("message_box");
    //var message_box = $('#message_box');
    $(".message_box").append($(message));
    
    console.log("scrollHeight:"+message_box.scrollHeight+",clientHeight:"+message_box.clientHeight+", scrollTop:"+message_box.scrollTop);
    var isScrolled = (message_box.scrollTop + message_box.clientHeight+31) <= message_box.scrollHeight;

    if(! isScrolled) {
        console.log("scrolling to bottom");
        $("#message_box").scrollTop($('#message_box')[0].scrollHeight);    
    }
}
function chatMessages(parsedMessage) {
    var message = parsedMessage.message;
    var message_box = document.getElementById("message_box");
    $(".message_box").html(message);
    $("#message_box").scrollTop($('#message_box')[0].scrollHeight);    
}
function privateChat(personId) {
    console.log("privateChat "+personId);
    var him=$('#person-'+personId);
    if(him.hasClass('personSelected')) {
        him.removeClass('personSelected');
    }
    else {
        him.addClass('personSelected');
    }
    if(getPeopleSelected().length>0) {
        $('#send-btn').text("Private");
    }
    else {
        $('#send-btn').text("Send");
    }
}
function getPeopleSelected() {
    var people=[];
    $('#people').children('.personDiv').each(function(index) {
        if($(this).hasClass('personSelected'))
            people.push($(this).attr('id'));
        console.log( index + ": ",$( this ).attr('id'));
    });
    console.log("selected:"+people.length);
    return people;
}
function personInfo(personId) {
    console.log("personInfo "+personId);
    var message = {
		verb : 'personInfo',
        mydetv: mydetv,
		personId : personId
	};
	sendMessage(message);  
}
function msgPersonInfo(response) {
    console.log("msgPersonInfo:",response);
    
    var personId = response.personId;
    if(personId==="") {
        bootbox.alert("Person Info: not found");
        return;
    }
    var msg = "<center>Person Info for "+response.username+"</center><br>";
    //msg+=personId+"<br>";
    var link = response.link;
    if(link !==undefined && link!==null && link!=="")
        msg+="homepage: <a href="+link+" target=\"_blank\">"+link+"</a><br>";
    if(response.bday!=="")
        msg+="birthday: "+response.bday+"<br>";
    msg+="added: "+response.added+"<br>";
    msg+="timezone: "+response.tz+"<br>";
    msg+="roles: "+response.roles+"<br>";
    bootbox.alert(msg);
}

function blockPerson(personId) {
    console.log("blockPerson "+personId);
    var message = {
		verb : 'chatBlock',
        mydetv: mydetv,
		personId : personId
	};
	sendMessage(message);  
}
// buggy firefox or javascript- could not target my span to overlay an thumbs down
function msgChatBlock(response) {
    console.log("msgChatBlock:",response);
    var result = response.result;
    if(result!=='blocked' && result!=='unblocked') {
        console.log("error: "+result);
        return;
    }
    var personId = response.personId;
    
    var him=$('#person-'+personId);
    if(result=="blocked") {
        him.addClass('personBlocked');
    }
    else {
        him.removeClass('personBlocked');
    }
    
    /*
    var strBlocked = 'blocked-'+personId;
    var blockedElement = document.getElementById(strBlocked);

    console.log("blockedElement before:",blockedElement);
    if(result=="blocked") {
        blockedElement.innerHTML = "<span class='fas fa-thumbs-down'></span>";
    }
    else {
        blockedElement.innerHTML = "";
    }
    console.log("blockedElement after:",blockedElement);
    return;

    console.log("personMute before:",$('#'+strBlocked));
    if(result=="blocked") {
        $(strBlocked).replaceWith("<span id='#blocked-"+personId+"' class='fas fa-thumbs-down'></span>");
    }
    else {
        $(strBlocked).replaceWith("<span id='#blocked-"+personId+"'></span>");
    }
    console.log("personMute after:",$('#'+strBlocked));
    */
}
/* UI */
// bandwidth progress bar
function initProgressbarBw(parsedMessage) {
    progressbarTotal=parsedMessage.total;
    progressbarFinished=parsedMessage.finished;
    progressbarCurrent = 0;
    $('progress-bar-bw').attr('aria-valuenow', 0).css('width',0+'%');
    $('#progressRowBw').removeClass('d-none');
}
function updateProgressbarBw(progress) {
    var progressbarNew = Math.floor(((progressbarFinished/progressbarTotal)+(progress/1048576/progressbarTotal))*100);
    console.log("progress bw:"+progress+",pb old:"+progressbarCurrent+",new:"+progressbarNew);
    if(progressbarCurrent != progressbarNew) {
        $('#progress-bar-bw').attr('aria-valuenow', progressbarNew).css('width',progressbarNew+'%');
        progressbarCurrent=progressbarNew;
    }
}
function hideProgressbarBw() {
    $('#progressRowBw').addClass('d-none');
     $('progress-bar-bw').attr('aria-valuenow', 0).css('width',0+'%');
}
// turn progress bar
function initProgressbarTurn(parsedMessage) {
    progressbarTotal=parsedMessage.total;
    progressbarFinished=parsedMessage.finished;
    $('#progressRowTurn').removeClass('d-none');
    $('progress-bar-bw').attr('aria-valuenow', 0).css('width',0+'%');
}
function updateProgressbarTurn(parsedMessage) {
    var current = parsedMessage.current;
    var progressbarNew = Math.floor((current/progressbarTotal)*100);
    console.log("progress turn progressbarTotal:"+progressbarTotal+",current:"+current+",new:"+progressbarNew);
    $('#progress-bar-bw').attr('aria-valuenow', progressbarNew).css('width',progressbarNew+'%');
}
function hideProgressbarTurn() {
    $('#progressRowTurn').addClass('d-none');
    $('progress-bar-bw').attr('aria-valuenow', 0).css('width',0+'%');
}

function mydetvReady() {
    $("#profileAlias").on('change', function() {
        var nickname=$('#profileAlias').val();
        var data = { mydetv: mydetv,nickname: nickname};

        console.log("data:"+JSON.stringify(data));
        $.ajax({  
            url: "/api/up/nickname/",
            method: 'PUT',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: function() {
                console.log("yes");
                }
        });
    });
}

function showLocalVideo() {
    console.log("show local video maybe");
    localVideo.play();
    
    $('#localVideoDancer').addClass('d-none');
    $('#localVideoSpinner').addClass('d-none');
    $('video#localVideo').removeClass('d-none');   
}
function showLocalSpinner() {
    console.log("show local spinner");
    $('#localVideoDancer').addClass('d-none');
    $('#localVideoSpinner').removeClass('d-none');
    $('video#localVideo').addClass('d-none');  
}
function showLocalDancer() {
    console.log("show local dancer");
    $('#localVideoDancer').removeClass('d-none');
    $('#localVideoSpinner').addClass('d-none');
    $('video#localVideo').addClass('d-none');
}
async function showRemoteVideo() {
    console.log("show remote video maybe");
    try {
    	await remoteVideo.play();
    } catch(err) {
    	console.log("err ",err);
    	bootbox.confirm({
    		message: "Your browser has blocked the video.",
    	    buttons: {
    	        confirm: { label: 'Play', className: 'btn-success'},
    	        cancel: { label: 'Ignore', className: 'btn-danger'}
    	    },
    	    callback: function (result) { if(result) showRemoteVideo(); }
    	});
    }
    
    $('#remoteVideoDancer').addClass('d-none');
    $('#remoteVideoSpinner').addClass('d-none');
    $('video#remoteVideo').removeClass('d-none');   
}
function showRemoteSpinner() {
    console.log("show remote spinner");
    $('#remoteVideoDancer').addClass('d-none');
    $('#remoteVideoSpinner').removeClass('d-none');
    $('video#remoteVideo').addClass('d-none');  
}
function showRemoteDancer() {
    console.log("show remote dancer");
    $('#remoteVideoDancer').removeClass('d-none');
    $('#remoteVideoSpinner').addClass('d-none');
    $('video#remoteVideo').addClass('d-none');
}


