
/* stats */
$(document).ready(function() {
    //doStats();
    
    $('.doStatus').click(function() {
        doStats();
    });
    $('.doUsers').click(function() {
        doUsers();
    });
    $('.doFC').click(function() {
        doFC($(this));
    });
    $('.doChannelStats').click(function() {
        doChannelStats($(this));
    });
    $('.doChannelNodes').click(function() {
        doChannelNodes($(this));
    });
});
function goStatus() {
    window.location.replace("/admin/status");
}
function goUsers() {
    window.location.replace("/admin/status?users");
}
function goFC() {
	window.location.replace("/admin/status?fc");
}
function goChannel(channel) {
    window.location.replace("/admin/status?channel="+channel);
}
function goNodes(channel) {
    window.location.replace("/admin/status?nodes="+channel);
}
function doStats() {
    $.ajax({  
        url: "/api/stats",
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            //console.log(response);
            $('#status').replaceWith("<div class='status' id='status'><pre>"+JSON.stringify(response, null, 2)+"</pre></div>");
            }
    });
}

function doUsers() {
    $.ajax({  
        url: "/api/stats/users",
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            //console.log(response);
            $('#status').replaceWith("<div class='status' id='status'><pre>"+JSON.stringify(response, null, 2)+"</pre></div>");
            },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log("ajaxError "+xhr.status + " " +thrownError);
        }
    });
}

function doChannelStats(channel) {
    console.log("doChannelStats "+channel);
    $.ajax({  
        url: "/api/stats/channel/"+channel,
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            console.log("l:"+JSON.stringify(response));
            $('#status').replaceWith("<div class='status' id='status'><pre>"+JSON.stringify(response, null, 2)+"</pre></div>");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log("ajaxError "+xhr.status + " " +thrownError);
        }
    });
}

var nodes;
var edges;
var groups;
var mynodes

function doChannelNodes(channel) {
    console.log("doChannelNodes");
    url = "/api/stats/channelNodes/"+channel;

    console.log("getting "+url);
    $.ajax({  
        url: url,
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
        	$('#status').replaceWith("<div class='status' id='status'><div id='mynetwork'></div></div>");
        	console.log("myEdges:",response.edges);
            console.log("myNodes:",response.nodes);
            console.log("myGroups:",response.groups);
            mynodes = response.nodes;
            getLegend(response.groups);
            
            nodes = new vis.DataSet(response.nodes);
            edges = new vis.DataSet(response.edges);
            console.log("nodes:",nodes);
            
            var data = {
                nodes: nodes,
                edges: edges
            };
            
            var options = {
                	groups: addGroups(response.groups),
                	layout: {
                		improvedLayout:true
                	}
                };
            
            //console.log(response);
            
            var container = document.getElementById('mynetwork');
            //console.log("data:"+JSON.stringify(data.nodes[0]));
            //console.log("data:"+JSON.stringify(data));
            
            var network = new vis.Network(container, data, options);
            $(network).ready(function() {
            	
                network.on("click", function(e) { 
                    //Zoom only on single node clicks, zoom out otherwise
                    if (e.nodes.length !== 1) {
                      network.fit();
                      return;
                    }
                    var nodeId = e.nodes[0];
                    //Find out what group the node belongs to
                    var group = getGroup(nodeId);
                    //TODO: How do you want to handle ungrouped nodes?
                    if (group === undefined) return;
                    var groupNodes = getGroupNodes(group);
                    network.fit({
                      nodes: groupNodes
                    });
                });
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log("ajaxError "+xhr.status + " " +thrownError);
        }
    });
}

function getLegend(myGroups) {
    var mynetwork = document.getElementById('mynetwork');
    var x = - mynetwork.clientWidth / 2 + 50;
    console.log("mynetwork.clientWidth",mynetwork.clientWidth);
    var y = - mynetwork.clientHeight / 2 + 50;
    var step = 70;
    var lid = 10000;
	$.each(myGroups, function (index, value) {  
		mynodes.push({
			id: index+10000, 
			x: x, 
			y: y+(step*index), 
			label: value.name, 
			group: value.name, 
			value: 1, fixed: true, physics:false
		});	    
	});
}

function addGroups(myGroups) {
    
    var groups = {};
    $.each(myGroups, function (index, value) {  	
    	//console.log("group "+index,value);
    	//console.log("name:"+value.name);
    	var d = {};
    	d["shape"]=value.shape;
    	d["color"]=value.color;
    	var name = value.name;
    	groups[name] = d;
    });
    console.log("groups:",groups);
    
    return groups;
}

var getGroup = function getGroup(nodeId) {
  var nodesHandler = network.nodesHandler;
  var innerNodes = nodesHandler.body.nodes;
  //Lazily assume ids match indices
  var node = innerNodes[nodeId];
  return node.options.group;
};

var getGroupNodes = function getGroupNodes(group) {
  // http://elijahmanor.com/reducing-filter-and-map-down-to-reduce/
  var filtered = nodes.reduce(function(output, node) {
    if (node.group === group) {
      output.push(node.id);
    }
    return output;
  }, []);
  return filtered;
};

