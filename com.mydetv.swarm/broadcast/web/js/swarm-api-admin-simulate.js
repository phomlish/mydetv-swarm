$(document).ready(function() {
	//console.log("loaded");
	$("#simulateCGBW").prop("checked", false);	
});

function adminSimulateShowAllUsers() {
	var url="/api/admin/simulate/showAllPeople";
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            simulateShowAllUsers(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function simulateShowAllUsers(result) {
	console.log("people",result);
	
	var rv="<div id='simulate'>";
	rv+="<table width='90%' border='1'>";
	// header
	rv+="<tr>";
	rv+="<th>username</th>";
	rv+="<th>channel</th>";
	rv+="<th>FC ice failed</th>";
	rv+="<th>FC slow</th>";
	rv+="<th>edit</th>";
	rv+="</tr>";
	
	$.each(result, function(i, item) {
	    console.log("item",item);
	    rv+="<tr>";
		rv+="<td><a target='_blank' href=https://dev.swarm.mydetv.com:8991/admin/person?wuid="
		    +item.wuid+">"
	        +item.username
	        +"</a></td>";
		
	    rv+="<td>"+item.channel+"</td>";
	    
	    rv+="<td>"+item.iceFailed+"</td>";
	    rv+="<td>"+item.slow+"</td>";
	    
		rv+="<td><button class='btn btn-primary' onclick='simualteEditFC("+item.wuid+");'>edit</button></td>";
	
	    rv+="</tr>";
	});
	
	rv+="</table>";
	rv+="</div>"; // simulate
	$("#simulate").replaceWith(rv);
}

function doFC_edit(smydetv) {
	
	var rv="<div id='simulate'>";
	rv+="<div><button class='btn btn-primary' onclick='adminSimulateShowAllUsers();'>Show All Users</button></div>\n";
	rv+=smydetv;
	console.log("edit",smydetv);
	adminGetUserFC(smydetv);
	rv+="</div>"; // simulate
	
	$("#simulate").replaceWith(rv);
}

function adminGetUserFC(smydetv) {
	var url="/api/admin/simulate/getUserFC/"+smydetv;
	$.ajax({
        url: url,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
        	showUserFC(result);
        },
        error: function (xhr, textStatus, thrownError) {
            var response = xhr.responseText;
            console.log("xhr.status:"+xhr.status+",thrownError:"+thrownError+",textStatus:"+textStatus+",responseText:\n"+response);
            switch(xhr.status) {
                case 401:
                    alert("unauthorized "+url);
                    break;
                case 403:
                    alert("forbidden "+url);
                    break;
                case 0:
                  alert("unable to connect to "+url);
                  break;
                default:
                    console.log("unknown error");
                    alert("error "+xhr.status+" "+xhr.responseText);
            }
        }
    });	
}

function showUserFC(user) {
	console.log("edit",user);
	
	var rv="<div id='simulate'>";
	rv+="<div><button class='btn btn-primary' onclick='adminSimulateShowAllUsers();'>Show All Users</button></div>\n";
	rv+="Faulty Connections for "+user.username+" ("+user.wuid+")<br>";

	// show
	rv+="<table border='1'>";
	rv+="<tr>";
	rv+="<th>connection type</th>";
	rv+="</tr>";
	rv+="</table>";
	
	// add
	rv+="<table border='1'>";
	rv+="<tr>";
	rv+="<th>connection type</th>";
	rv+="</tr>";
	
	rv+="<hr>";
	
	rv+="<tr><td>add</td></tr>";
	rv+="</table>";
	rv+="</div>"; // simulate	
	$("#simulate").replaceWith(rv);
}


