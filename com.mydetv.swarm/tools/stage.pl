#!/usr/bin/perl

use strict;
use File::chdir;

my $project;
my $server_name;

foreach (@ARGV) {
    print "$_\n";
    my $arg = $_;
    if($arg eq 'broadcast' or $arg eq 'maint') {
	$project=$arg;
    }
    else {
	$server_name=$arg;
    }
}

if(length($server_name) < 1) {
    print "you forgot to pass server, I'll use a6\n";
    $server_name="a6";
    }
if(length($project) < 1) {
    print "you forgot to pass project, I'll use broadcast\n";
    $project="broadcast";
    }


$CWD =  $project;

######## version related code ###############################################################
my $sc;

my $filename = 'target/classes/version.txt';    
my $curVersion = "";
if (open(my $fh, '<:encoding(UTF-8)', $filename)) {
  while (my $row = <$fh>) {
    chomp $row;
    print "$row\n";
    if($row =~ /version/) {
	$curVersion=substr($row, 8);
	}
  }
} else {
  warn "Could not open file '$filename' $!";
  exit;
}

print "curVersion:$curVersion\n";

my $install_dir = "distributions/$project";
print "packaging in $install_dir\n";

$sc = "rm -rf $install_dir";
print "sc:$sc\n";
system $sc;

$sc = "mkdir -p $install_dir/bin";
print "sc:$sc\n";
system $sc;

$sc = "cp -a bin $install_dir/";
print "sc:$sc\n";
system $sc;

$sc = "cp target/$project.jar $install_dir/$project.jar";
print "sc:$sc\n";
system $sc;

$sc = "cp -a target/lib $install_dir";
print "sc:$sc\n";
system $sc;

$sc = "cp -a target/classes/version.txt $install_dir";
print "sc:$sc\n";
system $sc;

$sc = "cp -a target/classes/META-INF/maven/dependencies.properties $install_dir";
print "sc:$sc\n";
system $sc;

if($project eq 'broadcast') {
    $sc = "cp -a web $install_dir";
    print "sc:$sc\n";
    system $sc;
}

chdir 'distributions';

$sc = "zip --quiet -r $project-".$curVersion.".zip $project/";
print "sc:$sc\n";
system $sc;

$sc = "scp -P 2222 $project-".$curVersion.".zip swarm\@$server_name:";
print "sc:$sc\n";
system $sc;

if($project eq 'broadcast') {
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/broadcast/bin/stopBroadcast.sh";
    print "sc:$sc\n";
    system $sc;
}

if($project eq 'maint') {
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/maint/bin/stopMaint.sh";
    print "sc:$sc\n";
    system $sc;
}

$sc = "ssh -p 2222 swarm\@a6 unzip -q -o $project-".$curVersion.".zip";
print "sc:$sc\n";
system $sc;

if($project eq 'broadcast') {
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/broadcast/bin/startBroadcast.sh";
    print "sc:$sc\n";
    system $sc;
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/broadcast/bin/statusBroadcast.sh";
    print "sc:$sc\n";
    system $sc;
}

if($project eq 'maint') {
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/maint/bin/startMaint.sh";
    print "sc:$sc\n";
    system $sc;
    $sc = "ssh -p 2222 swarm\@a6 /usr2/mydetv/maint/bin/statusMaint.sh";
    print "sc:$sc\n";
    system $sc;
}

