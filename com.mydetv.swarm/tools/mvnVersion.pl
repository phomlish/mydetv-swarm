#!/usr/bin/perl

use strict;
use File::chdir;

my $project;
my $server_name;

my $version = $ARGV[0];
if($version eq "") {
    print "you must supply a version!\n";
    exit;
}

print "version:$version\n";
my $sc = "mvn versions:set -DnewVersion=$version";
system $sc;
$sc = "mvn clean package";
system $sc;

$sc = "echo $version > version.txt";
system $sc;

$sc = "cd broadcast/web";
system $sc;
$sc = "npm install";
system $sc;

$sc = "git add -A";
system $sc;
$sc = "git commit -F version.txt --edit";
system $sc;
$sc = "git push";
system $sc;
$sc = "git push gitlab";
system $sc;
