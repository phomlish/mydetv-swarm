package maint;

import org.apache.commons.configuration2.XMLConfiguration;

public class Config {

	private String configDir;
	private XMLConfiguration xml;
	private String ffmpeg;
	private String ffprobe;
	private String tmpPath;
	private String videosDirectory;
	private String outPath;
	private Integer category;
	
	// getters/setters
	public String getConfigDir() {
		return configDir;
	}
	public void setConfigDir(String configDir) {
		this.configDir = configDir;
	}
	public XMLConfiguration getXml() {
		return xml;
	}
	public void setXml(XMLConfiguration xml) {
		this.xml = xml;
	}
	public String getVideosDirectory() {
		return videosDirectory;
	}
	public void setVideosDirectory(String inPath) {
		this.videosDirectory = inPath;
	}
	public String getOutPath() {
		return outPath;
	}
	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}
	public String getFfmpeg() {
		return ffmpeg;
	}
	public void setFfmpeg(String ffmpeg) {
		this.ffmpeg = ffmpeg;
	}
	public String getTmpPath() {
		return tmpPath;
	}
	public void setTmpPath(String tmpPath) {
		this.tmpPath = tmpPath;
	}
	public String getFfprobe() {
		return ffprobe;
	}
	public void setFfprobe(String ffprobe) {
		this.ffprobe = ffprobe;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
}
