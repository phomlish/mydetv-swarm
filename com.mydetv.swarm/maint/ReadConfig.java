package maint;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.BasicConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.io.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ReadConfig {

	private static final Logger log = LoggerFactory.getLogger(ReadConfig.class);
	
	public static Config CreateConfig(String configDir) throws IOException, Exception, ConfigurationException {

		Config config = new Config();

		log.info("      _______________________________________");
		log.info("      _______________________________________");
		log.info("     /\\ .      .      .        .         .   \\");
		log.info("    |  \\           My Delaware TV            \\");
		log.info("     \\/ | * .    .      Maint      .     .    .|");
		log.info("         | . *                              . * |");
		log.info("       /______________________________________/__");
		log.info("      / \\ .    .   .      *      .  .        .   `\\");
		log.info("      \\_/   *    .     .      .        .  *     . /");
		log.info("       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");

		//XMLConfiguration xmlConfig = null;
		try {
			config.setConfigDir(configDir);

			File initialFile = new File(configDir+"/maint.xml");
			InputStream inputStream = new FileInputStream(initialFile);
			XMLConfiguration xml = new BasicConfigurationBuilder<>(XMLConfiguration.class).configure(new Parameters().xml()).getConfiguration();
			FileHandler fh = new FileHandler(xml);
			fh.load(inputStream);
			config.setXml(xml);
			
			config.setFfmpeg(xml.getString("ffmpeg", "ffmpeg"));
			config.setFfprobe(xml.getString("ffprobe", "ffprobe"));
			config.setTmpPath(xml.getString("tmpPath"));
			if(config.getTmpPath()==null) 
				throw new ConfigurationException("need tmpPath");
			config.setVideosDirectory(xml.getString("inPath"));
			if(config.getVideosDirectory()==null) 
				throw new ConfigurationException("need inPath");
			config.setOutPath(xml.getString("outPath"));
			if(config.getOutPath()==null) 
				throw new ConfigurationException("need outPath");
			if(config.getVideosDirectory().equals(config.getOutPath()))
				throw new ConfigurationException("inPath and outPath can't be the same");
			config.setCategory(xml.getInteger("category", -1));
		} catch (ConfigurationException cex) {
			log.error("Error opening configuration file: " + cex.toString());
			System.exit(1);
		}
		return config;		
	}
	
}
