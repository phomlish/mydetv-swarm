package videosConvert;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Blackboard;
import maint.PrivateBlackboard;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;

public class VideosHelpers {

	@Inject private Config config;
	@Inject private Blackboard blackboard;
	private static final Logger log = LoggerFactory.getLogger(VideosHelpers.class);
	
	public OneVideoConverted findVideoConverted(OneVideo ov, PrivateBlackboard videosBlackboard) {
		
		for(OneVideoConverted ovc : videosBlackboard.getVideosConverted().values()) {
			if(ovc.getDtInactive()!=null)
				continue;
			if(ovc.getPkey()==null || ! ovc.getPkey().equals(ov.getPkey()))
				continue;
			if(ovc.getConvertedType().equals(videosBlackboard.getConvertedType()))
				return ovc;
		}
		
		return null;
	}
	
	public String getCatMainDir(PrivateBlackboard bb, OneVideo ov) {
		VideoCategorySub ovcs = bb.getVideoCategoriesSub().get(ov.getCatSub());
		VideoCategoryMain ovcm = bb.getVideoCategoriesMain().get(ovcs.getCatMain());
		return ovcm.getName();
	}
	
	public float getLength(String fpn) {

		Float length = (float) 0;
		try {
			// ffprobe for the duration
			ProcessBuilder builder = new ProcessBuilder(config.getFfprobe(),
					"-v","error","-show_entries","format=duration","-of","default=noprint_wrappers=1",
					fpn);
			builder.redirectErrorStream(true);
			//builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";

			//duration=45.085000
			while((line=readerOutput.readLine()) != null) {
				log.trace("    "+line);
				if(line.contains("duration")) {
					String duration = line.substring(9);
					length = Float.valueOf(duration);
					log.debug("d:"+duration+",length:"+length);
					return length;
				}				
			}			
			process.waitFor();
		} catch(Exception e) {
			log.error("Exception finding length(duration):"+e);
		}
		return length;
	}

	public void setVideoTitle(OneVideo ov, Path path) {
		String title = "";
		try {
			// ffprobe for the other details
			ProcessBuilder builder = new ProcessBuilder(config.getFfprobe(),"-i",path.toString(),"-hide_banner");
			builder.redirectErrorStream(true);
			//builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";

			/*
			    title           : 34-01-05_She_Wronged_Him_Right - http://www.archive.org/details/SheWrongedHimRight
			    encoder         : Lavf57.71.100
			  Duration: 00:06:25.12, start: 0.000000, bitrate: 586 kb/s
			    Stream #0:0: Video: vp9 (Profile 0), yuv420p(tv, progressive), 640x480, SAR 73:80 DAR 73:60, 29.97 fps, 29.97 tbr, 1k tbn, 1k tbc (default)
			    Stream #0:1: Audio: opus, 48000 Hz, stereo, fltp (default)
			 */
			while((line=readerOutput.readLine()) != null) {
				log.trace("    "+line);
				if(line.toLowerCase().contains("title")) {
					title = line.substring(line.indexOf(":")+1).trim();
					ov.setTitle(title);
					return;
				}
			}
		} catch(Exception e) {
			log.error("Exception getting title:"+e);
		}
	}
	
	public String makeThumbnail(String fullDirName, String fn, String fnNoSuffix, Float duration, File tmpDir) {
		try {
			String tnFilename = fnNoSuffix+".gif";
			String tnFullpath = fullDirName+"/"+tnFilename;
			
			// we'll create a float between 12 and 13 for the duration calculation
			// maybe this will help the schedules page from looping all thumbnail animations exactly he same
			int plusTSeconds = blackboard.getSecureRandom().nextInt(10);
			float fps = (12 + (plusTSeconds/10))/duration;
			log.info("fps:"+fps);
			//float fps = 12/duration;
			
			String cmd1 = config.getFfmpeg()+" -y -i";
			String cmd2 = "-vf fps="+fps;
			ArrayList<String> pieces = new ArrayList<String>();
			pieces.addAll(Arrays.asList(cmd1.split(" ")));
			pieces.add(fullDirName+"/"+fn);
			pieces.addAll(Arrays.asList(cmd2.split(" ")));
			pieces.add(tnFullpath);

			log.info("pieces:"+pieces.toString());
			ProcessBuilder builder = new ProcessBuilder(pieces);
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";

			while((line=readerOutput.readLine()) != null) {
				log.trace("    "+line);
			}

			File tn = new File(tnFullpath);
			if(! tn.exists()) {
				log.error("no tn? "+tnFullpath);
				return "";
			}
			
			return tnFilename;
		} catch(Exception e) {
			log.error("Exception getting title:"+e);
			return "";
		}
	}
	
	// http://blog.pkh.me/p/21-high-quality-gif-with-ffmpeg.html
	// https://superuser.com/questions/556029/how-do-i-convert-a-video-to-gif-using-ffmpeg-with-reasonable-quality
	String fpsscale = "fps=10,scale=160";
	String version = "v2";
	File tmpDir;
	public String makeVideoThumbnail(PrivateBlackboard bb, OneVideo ov, File tmpDir) {
		try {
			this.tmpDir=tmpDir;
			
			String catMainDir = getCatMainDir(bb, ov);
			String videoPn = config.getVideosDirectory()+"/"+catMainDir+"/"+ov.getSubdir();
			String videoFpn = videoPn+"/"+ov.getFn();
			String tnFilename = ov.getFn()+"."+version+".gif";
			
			File file = new File(videoFpn);
			if(!file.exists()) {
				log.error("video does not exist "+videoFpn);
				return "";
			}
			
			String videoInputFilenameForThumbnail;
			log.info(ov.getFn()+" "+ov.getLength());
			
			// videos shorter than 120 seconds: skip 1/10 of video and stop at 90% of video length
			if(ov.getLength()<120) {
				videoInputFilenameForThumbnail=ov.getFn()+".mpeg";				
				Integer skipSeconds = (int) (ov.getLength()/10);
				Integer duration = skipSeconds*8;
				createVideoPiece(skipSeconds, duration, videoFpn, videoInputFilenameForThumbnail);
			}
			// videos longer than 120 seconds can be split into 12 pieces
			else {
			
				//int cntPieces = (int) (ov.getLength()/60/5);
				int cntPieces = 12;
				int oneTenthOfVideo = (int) (ov.getLength()/cntPieces);
				log.info(ov.getLength()+" oneTenthOfVideo:"+oneTenthOfVideo);
				String pieces ="";
				for(int i=1; i<(cntPieces-1);i++) {
					Integer skipSeconds = i*oneTenthOfVideo;
					log.info("get piece "+i+" at "+skipSeconds);
					String pieceName = ov.getFn()+"-"+i+".mpeg";
					if(pieces.length()!=0) 
						pieces+="|";
					pieces+=pieceName;
					createVideoPiece(skipSeconds, 4, videoFpn, pieceName);
				}
				
				videoInputFilenameForThumbnail = ov.getFn()+".mpeg";
				combineVideoPieces(pieces, videoInputFilenameForThumbnail);			
			}
			
			String palleteName = ov.getFn()+".png";
			createThumbnail(videoInputFilenameForThumbnail, palleteName, tnFilename);
			
			File tn = new File(tmpDir+"/"+tnFilename);
			if(! tn.exists()) {
				log.error("no tn? "+tnFilename);
				return "";
			}
			
			return tnFilename;
		} catch(Exception e) {
			log.error("Exception getting title:"+e);
			return "";
		}
	}
	
	/**
	 * 
	 * @param skipSeconds
	 * @param duration
	 * @param videoFpn
	 * @param tnFilename
	 * @return
	 */
	private String createVideoPiece(Integer skipSeconds, Integer duration, 
		String videoFpn, String tnFilename) {
		try {
			ArrayList<String> pieces = new ArrayList<String>();
			pieces.add(config.getFfmpeg());
			pieces.add("-y");
			pieces.add("-ss");
			pieces.add(skipSeconds.toString());
			pieces.add("-t");
			pieces.add(duration.toString());
			pieces.add("-i");
			pieces.add(videoFpn);
			pieces.add("-an");
			pieces.add(tnFilename);
	
			log.info("pieces:"+pieces.toString());
			ProcessBuilder builder = new ProcessBuilder(pieces);
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";
	
			while((line=readerOutput.readLine()) != null) {
				log.info("    "+line);
			}
			return tnFilename;
		} catch(Exception e) {
			log.error("Exception generating thumbnail:"+e);
			return "";
		}

	}
	
	/**
	 * 
	 * @param combinedVideos
	 * @param palleteName
	 * @param tnFilename
	 * @return
	 */
	private String createThumbnail(String videoFn, String palleteName, String tnFilename) {
		try {
			ArrayList<String> pieces = new ArrayList<String>();
			pieces.add(config.getFfmpeg());
			pieces.add("-y");
			pieces.add("-i");
			pieces.add(videoFn);
			pieces.add("-vf");
			pieces.add(fpsscale+":-1:flags=lanczos,palettegen");
			pieces.add(palleteName);
	
			log.info("pieces:"+pieces.toString());
			ProcessBuilder builder = new ProcessBuilder(pieces);
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";
	
			while((line=readerOutput.readLine()) != null) {
				log.info("    "+line);
			}
			
			//ffmpeg -ss 30 -t 3 -i input.flv -i palette.png -filter_complex \
			//"fps=10,scale=320:-1:flags=lanczos[x];[x][1:v]paletteuse" output.gif
			
			pieces = new ArrayList<String>();
			pieces.add(config.getFfmpeg());
			pieces.add("-y");
			pieces.add("-i");
			pieces.add(videoFn);
			pieces.add("-i");
			pieces.add(palleteName);
			pieces.add("-filter_complex");
			pieces.add(fpsscale+":-1:flags=lanczos[x];[x][1:v]paletteuse");
			pieces.add(tnFilename);
	
			log.info("pieces:"+pieces.toString());
			builder = new ProcessBuilder(pieces);
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			process = builder.start();			
			readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			line="";
	
			while((line=readerOutput.readLine()) != null) {
				log.info("    "+line);
			}
			return tnFilename;
		} catch(Exception e) {
			log.error("Exception generating thumbnail:"+e);
			return "";
		}
	}
	
	/**
	 * 
	 * @param vpieces
	 * @param combinedVideos
	 * @return
	 */
	// ffmpeg -i "concat:image1.ts|image2.ts|image3.ts" -pix_fmt rgb24 output.gif
	private String combineVideoPieces(String vpieces, String combinedVideos) {
		try {
			ArrayList<String> pieces = new ArrayList<String>();
			pieces.add(config.getFfmpeg());
			pieces.add("-y");
			pieces.add("-i");
			pieces.add("concat:"+vpieces);
			pieces.add("-c");
			pieces.add("copy");
			pieces.add(combinedVideos);
	
			log.info("pieces:"+pieces.toString());
			ProcessBuilder builder = new ProcessBuilder(pieces);
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			Process process = builder.start();			
			BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="";
	
			while((line=readerOutput.readLine()) != null) {
				log.info("    "+line);
			}
			return combinedVideos;
		} catch(Exception e) {
			log.error("Exception generating thumbnail:"+e);
			return "";
		}
	}
}
