package videosConvert;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import maint.Blackboard;
import maint.Ffmpeg;
import maint.MaintException;
import maint.PrivateBlackboard;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;
import shared.data.VideoCategoryMain;
import shared.db.DbLog;
import shared.db.DbVideoConverted;
import shared.helpers.FixPermissions;

public class DoConvert {
	
	private static final Logger log = LoggerFactory.getLogger(DoConvert.class);
	//@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private DbVideoConverted dbVideoConverted;
	@Inject private VideosHelpers helpers;
	@Inject private Ffmpeg ffmpeg;
	@Inject private DbLog dbLog;
	private boolean changed;
	
	public boolean convertVideos(PrivateBlackboard bb) {

		log.info("convert videos for "+bb.getConvertedType());
		bb.setTmpPath(config.getTmpDir()+"/convert/"+bb.getConvertedType());
		
		log.info("db table videosConverted has "+bb.getVideosConverted().size()+" entries");
		
		log.info("db table videosConverted has "+bb.getVideosConverted().size()+" entries");
		Integer cntConverted=0;
		for(OneVideo ov : bb.getVideos().values()) {
			OneVideoConverted ovc = helpers.findVideoConverted(ov, bb);
			if(ovc!=null && ovc.getDtInactive()!=null)
				cntConverted++;
		}
		log.info("found "+cntConverted+" converted videos out of "+bb.getVideos().size());
		
		int count=0;
		log.info("processing category "+config.getCategory());
		for(OneVideo ov : bb.getVideos().values()) {
			
			if(ov.getDtInactive()!=null)
				continue;
			if(ov.isDoNotUse())
				continue;
			
			count++;
			VideoCategorySub ovs = bb.getVideoCategoriesSub().get(ov.getCatSub());
								
			// if the config file has a category defined, only process those
			if(! config.getCategory().equals(0) && ! config.getCategory().equals(ovs.getIdvideo_category())) {
				log.debug("skipping "+ov.getPkey()+", not doing category "+ov.getCatSub());
				continue;
			}
			
			// only process things we have not done yet and not inactivated.  To redo them, set dtInactive
			OneVideoConverted ovc = helpers.findVideoConverted(ov, bb);
			if(ovc!=null && ovc.getDtInactive()==null) 
				continue;

			//if(! ov.getPkey().equals(1073))
			//	continue;
			log.info("converting "+ov.getPkey()+" "+ov.getTitle()+" "+ov.getSubdir());
			convertOV(bb, ov);
			}
		
		log.info("done converting ("+count+" out of "+bb.getVideos().size()+")");
		return changed;
	}
	
	public void convertOV(PrivateBlackboard bb, OneVideo ov) {
		log.info("convert "+ov.getPkey()+" "+bb.getConvertedType());
		// start with a clean directory
		File tmpDir = new File(bb.getTmpPath());
		tmpDir.mkdirs();
		try {
			FileUtils.cleanDirectory(tmpDir);
		} catch (IOException e1) {
			log.warn("Error cleaning the directory");
		}
		 
		if(! bb.getVideoCategoriesSub().containsKey(ov.getCatSub())) {
			log.error("can't find that category "+ov.getCatSub());
			return;
		}
		VideoCategorySub ovs = bb.getVideoCategoriesSub().get(ov.getCatSub());
		VideoCategoryMain vcm = bb.getVideoCategoriesMain().get(ovs.getCatMain());
		
		String newFilename = ov.getFn()+"."+bb.getConvertedType()+".webm";		
		String tmpFp=bb.getTmpPath()+"/"+newFilename;
		String oldDir = config.getVideosDirectory()+"/";
		String newDir =config.getVideosConvertedDirectory()+"/";
		log.info("using tmpFp:"+tmpFp);
		
		if(vcm.getName()!=null && ! vcm.getName().equals("")) {
			oldDir+=vcm.getName()+"/";
			newDir+=vcm.getName()+"/";
		}
		if(ov.getSubdir()!=null && ! ov.getSubdir().equals("")) {
			oldDir+=ov.getSubdir()+"/";
			newDir+=ov.getSubdir()+"/";
		}
		
		log.info("making dir "+newDir);
		File fileNp = new File(newDir);
		fileNp.mkdirs();
				
		String oldFp=oldDir+ov.getFn();
		File oldFile = new File(oldFp);
		if(!oldFile.exists()) {
			log.error("file does not exist "+oldFp);
			return;
		}
		String newFp = newDir+"/"+newFilename;
		String logFp = newDir+"/"+ov.getFn()+"-"+bb.getConvertedType()+".log";
		log.info("logging to "+logFp);
		ch.qos.logback.classic.Logger flog = createLog4JAppender(logFp);
		
		log.info("converting "+oldFp+" to "+newFp);
		flog.info("converting "+oldFp+" to "+newFp);
	
		ProcessBuilder builder;
		Process process=null;
		BufferedReader readerOutput;
		File  f;
		String line;
		String uuid="none";
		File convertedFile = null;
		OneVideoConverted ovc = new OneVideoConverted();
		Boolean success = false;
		try {
			
			uuid = UUID.randomUUID().toString();
		
			ovc.setMachineName(config.getMachineName());
			ovc.setFn(newFilename);
			ovc.setOfn(ov.getFn());
			ovc.setPkey(ov.getPkey());
			ovc.setConvertedType(bb.getConvertedType());
			
			// *************** first pass ***************
			log.info("start first pass "+uuid+" "+tmpFp);
			flog.info("start first pass "+uuid+" "+tmpFp);
			switch(bb.getConvertedType()) {
			case "vp8-opus":			
				ffmpeg.convert_Webm_Vp8_Opus_Pass1(uuid, oldFp, tmpFp, tmpDir, flog);
				break;
			case "vp8-vorbis":
				ffmpeg.convert_Webm_Vp8_Vorbis_Pass1(uuid, oldFp, tmpFp, tmpDir, flog);			
				break;
			case "vp9-opus":
				ffmpeg.convert_Webm_Vp9_Opus_Pass1(uuid, oldFp, tmpFp, tmpDir, flog);
				break;
			}
			log.info("finish first pass "+uuid+" "+tmpFp);
			// check if the log file grew
			f = new File(tmpDir+"/"+uuid+"-0.log");
			if(f.length()<10000)		
				throw new MaintException("Error creating ffmpeg log for first pass "+ov.getPkey()+" "+tmpDir+"/"+uuid+"-0.log");
			
			f = null;
			
			// *************** second pass ***************
			log.info("start second pass "+uuid+" "+tmpFp);
			flog.info("start second pass "+uuid+" "+tmpFp);
			switch(bb.getConvertedType()) {
			case "vp8-opus":			
				ffmpeg.convert_Webm_Vp8_Opus_Pass2(uuid, oldFp, tmpFp, tmpDir, flog);
				break;
			case "vp8-vorbis":
				ffmpeg.convert_Webm_Vp8_Vorbis_Pass2(uuid, oldFp, tmpFp, tmpDir, flog);			
				break;
			case "vp9-opus":
				ffmpeg.convert_Webm_Vp9_Opus_Pass2(uuid, oldFp, tmpFp, tmpDir, flog);			
				break;
			}
			log.info("finish second pass "+uuid+" "+tmpFp);
			flog.info("finished pass2");
			
			// check if the converted file grew
			convertedFile = new File(tmpFp);
			if(convertedFile.length()<10000)
				throw new MaintException("Error creating ffmpeg video file after second pass "+ov.getPkey()+" "+tmpFp);
			
			// probe our video and get some juicy details		
			ovc.setFilesize(convertedFile.length());
			ovc.setLength(helpers.getLength(tmpFp));
			
			// ffprobe for the other details
			builder = new ProcessBuilder(config.getFfprobe(),"-i",tmpFp,"-hide_banner");
			builder.redirectErrorStream(true);
			builder.directory(tmpDir);
			process = builder.start();			
			readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			line="";
			
			/*
		    title           : 34-01-05_She_Wronged_Him_Right - http://www.archive.org/details/SheWrongedHimRight
		    encoder         : Lavf57.71.100
		  Duration: 00:06:25.12, start: 0.000000, bitrate: 586 kb/s
		    Stream #0:0: Video: vp9 (Profile 0), yuv420p(tv, progressive), 640x480, SAR 73:80 DAR 73:60, 29.97 fps, 29.97 tbr, 1k tbn, 1k tbc (default)
		    Stream #0:1: Audio: opus, 48000 Hz, stereo, fltp (default)
			*/
			while((line=readerOutput.readLine()) != null) {
				log.info("    "+line);
				if(line.contains("title")) {
					//String title = line.substring(line.indexOf(":")+1);
					//ovc.setTitle(title);
				}				
				else if (line.contains("bitrate: ")) {
					//Duration: 00:03:19.75, start: 0.000000, bitrate: 841 kb/s
					String bitrate = line.substring(line.indexOf("bitrate: "));
					bitrate = bitrate.substring(9);
					String[] parts = bitrate.split(" ");
					if(parts.length!=2) 
						continue;
					
					bitrate = parts[0];
					String b = parts[1].toLowerCase();
					if(!b.equals("kb/s")) {
						log.error("need to write code to convert "+b+" to kb/s");
						continue;
					}
					
					Integer br = Integer.decode(bitrate);
					ovc.setBitrate(br);				
				}
				else if(line.contains("Stream #0:0")) {
					String stream = line.substring(4);
					ovc.setStream0(stream);
				}
				else if(line.contains("Stream #0:1")) {
					String stream = line.substring(4);
					ovc.setStream1(stream);
				}				
			}
						
			log.info("successfully converted, now moving from "+tmpFp+" to "+newFp);
			flog.info("successfully converted, now moving from "+tmpFp+" to "+newFp);
			success=true;
		} catch(MaintException | IOException e) {
			ovc.setFailedReason(e.getMessage());
			log.error("failed "+e);
			flog.error("failed "+e);
			dbLog.info("failed convert:"+e);
		}
		
		if(success) {
			synchronized(blackboard.getMaintLock()) {
				try {
					Files.copy(Paths.get(tmpFp), Paths.get(newFp), StandardCopyOption.REPLACE_EXISTING);
					FixPermissions.FixFilePermissions(newFp);
				} catch (IOException e) {
					log.error("Error copying "+e);
				}
				
				File newFile = new File(newFp);
				if(newFile.length() != ovc.getFilesize()) {
					log.error("Invalid filesize after copy +"+ov.getPkey()+" "+newFp);
					return;
				}
				
				// we copied it above, so now we delete it
				convertedFile.delete();
				
				// let's delete the UUID log file
				File logFile = new File(tmpDir+"/"+uuid+".log");
				logFile.delete();
			
				log.info("completely finished "+newFp);
				flog.info("completely finished "+newFp);
					
				Integer idVideoConverted = dbVideoConverted.insertVideoConverted(ovc);
				ovc.setVideoConvertedId(idVideoConverted);
				bb.getVideosConverted().put(ovc.getVideoConvertedId(), ovc);
				flog.detachAndStopAllAppenders();
			}
			changed=true;
		}
	}

	private ch.qos.logback.classic.Logger createLog4JAppender(String fn) {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		PatternLayoutEncoder ple = new PatternLayoutEncoder();
		ple.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
        ple.setContext(lc);
        ple.start();
        ch.qos.logback.core.FileAppender<ILoggingEvent> fileAppender = new ch.qos.logback.core.FileAppender<ILoggingEvent>();
        fileAppender.setFile(fn);
        fileAppender.setEncoder(ple);
        fileAppender.setContext(lc);
        fileAppender.start();

        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("FileLogger");
        logger.addAppender(fileAppender);
        logger.setLevel(ch.qos.logback.classic.Level.DEBUG);
        logger.setAdditive(false); /* set to true if root should log too */

        return logger;
	}
	
}
