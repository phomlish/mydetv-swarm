package videosConvert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Helpers;
import maint.PrivateBlackboard;
import maint.RefreshPrivateBlackboard;
import maint.UpdateBroadcast;

public class ProcessVideos implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ProcessVideos.class);
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private DoConvert doConvert;
	@Inject private Helpers helpers;
	@Inject private UpdateBroadcast updateBroadcast;
	
	PrivateBlackboard bb = new PrivateBlackboard();
	
	@Override
	public void run() {
		log.info("starting the videos thread for "+bb.getConvertedType());
				
		while(true) {
			refreshBlackboard.refreshBlackboard(bb);
			boolean changed = doConvert.convertVideos(bb);
			if(changed) 
				updateBroadcast.updateBroadcastBlackboard();
			
			else try {
				long nextHour = helpers.getMillisTillNextHour();
				log.info("no "+bb.getConvertedType()+" videos to convert, sleeping "+nextHour);
				Thread.sleep(nextHour);
				//Thread.sleep(1*60*1000);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}			
		}	
	}

	public void setConvertedType(String convertedType) {
		bb.setConvertedType(convertedType);
	}
	public PrivateBlackboard getVideosBlackboard() {
		return bb;
	}

}
