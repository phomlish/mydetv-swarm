package Checkers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.PrivateBlackboard;
import shared.data.webuser.OneWebUser;
import shared.data.webuser.OneWebUserEmail;
import shared.data.webuser.OneWebUserImage;
import shared.data.webuser.OneWebUserRole;
import shared.data.webuser.OneWebUserUsername;
import shared.db.DbConnection;
import shared.db.DbWebUser;
import shared.db.DbWebUsers;

public class VerifyWebusers {

	private static final Logger log = LoggerFactory.getLogger(VerifyWebusers.class);
	@Inject private DbConnection dbConnection;
	@Inject private DbWebUsers dbWebUsers;
	@Inject private DbWebUser dbWebUser;
	
	PrivateBlackboard bb;
	
	public void verify(PrivateBlackboard bb) {
		this.bb=bb;

		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			TreeMap<Integer, OneWebUser> webUsers = dbWebUsers.getWebUsers(conn);
			TreeMap<Integer, OneWebUserEmail> emails = dbWebUsers.getEmails(conn);
			TreeMap<Integer, OneWebUserRole> roles = dbWebUsers.getRoles(conn);
			TreeMap<Integer, OneWebUserUsername> usernames = dbWebUsers.getUsernames(conn);
			TreeMap<Integer, OneWebUserImage> images = dbWebUsers.getWebUserImages(conn);

			// init the web users
			for(OneWebUser owu : webUsers.values()) {							
				owu.setEmails(new TreeMap<Integer, OneWebUserEmail>());
				owu.setRoles(new TreeMap<Integer, OneWebUserRole>());
				owu.setImages(new TreeMap<Integer, OneWebUserImage>());
			}

			// do some checks
			checkDangling(webUsers, emails, roles, usernames, images);
			checkUnused(webUsers, emails, roles, usernames, images);
			checkUnvalidated(conn, webUsers);
			
			// fill in the slickgrid strings
			for(OneWebUser owu : webUsers.values()) {
				String rv="";
				for(OneWebUserEmail oe : owu.getEmails().values()) {
					if(rv.length()!=0) rv+=",";
					rv+=oe.getEmail();
				}
				owu.setStrEmails(rv);
				
				rv="";
				for(OneWebUserRole oe:owu.getRoles().values()) {
					if(rv.length()!=0) rv+=",";
					rv+=oe.getName();
				}
				owu.setStrRoles(rv);				
			}
			
			// all done
			return;

		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);
		return;
	}
	
	/**
	 * check emails, roles, usernames, images that do not match a webuser
	 * @param webUsers
	 * @param emails
	 * @param roles
	 * @param usernames
	 * @param images
	 */
	private void checkDangling(
			TreeMap<Integer, OneWebUser> webUsers,
			TreeMap<Integer, OneWebUserEmail> emails,
			TreeMap<Integer, OneWebUserRole> roles,
			TreeMap<Integer, OneWebUserUsername> usernames,
			TreeMap<Integer, OneWebUserImage> images) {
		for(OneWebUserEmail oe : emails.values()) {
			if(! webUsers.containsKey(oe.getIdWebUser())) {
				log.error("dangling email:"+oe.getEmail());
			}
			else {
				webUsers.get(oe.getIdWebUser()).getEmails().put(oe.getIdEmail(), oe);
				oe.setUsed(true);
			}
		}
		for(OneWebUserRole oe : roles.values()) {
			if(! webUsers.containsKey(oe.getWebUserId())) {
				log.error("dangling role:"+oe.getWebUserRoleType());
			}
			else {
				webUsers.get(oe.getWebUserId()).getRoles().put(oe.getIdWebUserRole(), oe);
				oe.setUsed(true);
			}
		}
		for(OneWebUserUsername oe : usernames.values()) {
			if(! webUsers.containsKey(oe.getIdWebUser())) {
				log.error("dangling username:"+oe.getUsername());
			}
			else {
				oe.setUsed(true);
			}
		}
		for(OneWebUserImage oe : images.values()) {
			if(! webUsers.containsKey(oe.getIdWebUser())) {
				log.error("dangling image:"+oe.getOrigFilename());
			}
			else {
				webUsers.get(oe.getIdWebUser()).getImages().put(oe.getIdWebUserImage(), oe);
				oe.setUsed(true);
			}
		}
	}

	private void checkUnused(
			TreeMap<Integer, OneWebUser> webUsers,
			TreeMap<Integer, OneWebUserEmail> emails,
			TreeMap<Integer, OneWebUserRole> roles,
			TreeMap<Integer, OneWebUserUsername> usernames,
			TreeMap<Integer, OneWebUserImage> images) {
		for(OneWebUserEmail oe : emails.values()) {
			if(!oe.getUsed())
				log.error("unused email:"+oe.getEmail());
		}
		for(OneWebUserRole oe : roles.values()) {
			if(!oe.getUsed())
				log.error("unused role:"+oe.getWebUserRoleType());
		}
		for(OneWebUserUsername oe : usernames.values()) {
			if(!oe.getUsed())
				log.error("unused username:"+oe.getUsername());
		}
		for(OneWebUserImage oe : images.values()) {
			if(!oe.getUsed())
				log.error("unused image:"+oe.getOrigFilename());
		}
	}
	
	/**
	 * users that have not validated at least one email in 4 hours will be deleted<br>
	 * 
	 * @param webUsers
	 * @param emails
	 * @param roles
	 * @param usernames
	 * @param images
	 */
	private void checkUnvalidated(
			Connection conn,
			TreeMap<Integer, OneWebUser> webUsers) {

		LocalDateTime old = LocalDateTime.now().minusHours(4);
		for(OneWebUser ow : webUsers.values()) {
			Boolean verified=false;
			for(OneWebUserEmail oe : ow.getEmails().values()) {
				if(oe.getDtVerified()!=null)
					verified=true;
				else if(ow.getDtAdded().isAfter(old))
					verified=true;
			}
			if(!verified) {
				log.error("unverified "+ow.getUsername());
				deleteWebuser(conn, ow);
			}
			else
				log.info("verified "+ow.getUsername());
		}
	}

	private void deleteWebuser(Connection conn, OneWebUser wu) {
		log.info("deleting "+wu.getUsername());
		
		for(OneWebUserEmail oe : wu.getEmails().values()) {
			dbWebUser.deleteEmail(conn, oe);
		}
		for(OneWebUserRole oe:wu.getRoles().values()) {
			dbWebUser.deleteRole(conn, oe.getIdWebUserRole());
		}
		for(OneWebUserImage oe:wu.getImages().values()) {
			dbWebUser.deleteWebUserImage(conn, oe);
		}
		dbWebUser.deleteWebUser(conn, wu.getWuid());
	}
}
