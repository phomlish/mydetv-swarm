package Checkers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Helpers;
import maint.PrivateBlackboard;
import maint.RefreshPrivateBlackboard;

public class CheckersThread implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(CheckersThread.class);
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private Helpers helpers;
	
	PrivateBlackboard bb = new PrivateBlackboard();
	
	@Override
	public void run() {
		log.info("starting the videos thread for "+bb.getConvertedType());
				
		while(true) {
			refreshBlackboard.refreshBlackboard(bb);
			try {
				long nextHour = helpers.getMillisTillNextHour();
				log.info("no "+bb.getConvertedType()+" videos to convert, sleeping "+nextHour);
				Thread.sleep(nextHour);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}			
		}	
	}
	
}
