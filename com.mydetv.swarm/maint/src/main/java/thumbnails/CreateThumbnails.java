package thumbnails;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import com.google.inject.Inject;

import maint.Helpers;
import maint.PrivateBlackboard;
import maint.RefreshPrivateBlackboard;
import maint.UpdateBroadcast;
import shared.data.Config;
import shared.data.OneVideo;
import shared.db.DbCommand;
import shared.helpers.FixPermissions;
import videosConvert.VideosHelpers;

public class CreateThumbnails implements Runnable {

	@Inject private Config config;
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private Helpers helpers;
	@Inject private VideosHelpers videoHelpers;
	@Inject private UpdateBroadcast updateBroadcast;
	@Inject private DbCommand dbCommand;
	private static final Logger log = LoggerFactory.getLogger(CreateThumbnails.class);
	
	PrivateBlackboard bb = new PrivateBlackboard();
	String tmpPathVideos;
	boolean changed;
	
	@Override
	public void run() {
		
		log.info("starting CreateThumbnails thread");
		tmpPathVideos = config.getTmpDir()+"/videos";
		
		while(true) {
			refreshBlackboard.refreshBlackboard(bb);
			changed=false;
			createThumbnails();
			if(changed) 
				updateBroadcast.updateBroadcastBlackboard();
			
			else try {
				long nextHour = helpers.getMillisTillNextHour();
				log.info("no thumbnails to create, sleeping "+nextHour);
				Thread.sleep(nextHour);
				//Thread.sleep(1*60*1000);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}			
		}	
		
	}
	
	
	public void createThumbnails() {
		Integer cnt=0;
		for(OneVideo ov : bb.getVideos().values()) {
			if(ov.isDoNotUse())
				continue;
			if(ov.getCatSub()==20) 
				continue;
			if(ov.getThumbnail()!=null) {
				String catMainDir = videoHelpers.getCatMainDir(bb, ov);
				String fpn = config.getVideosDirectory()+"/"+catMainDir+"/"+ov.getSubdir()+"/"+ov.getThumbnail();
				log.info("tn:"+fpn);
				File file = new File(fpn);
				if(file.exists())
					continue;
			}

			cnt++;
			createThumbnail(ov);	
		}
		log.info("found "+cnt+" to create");
	}
	
	public void createThumbnail(OneVideo ov) {
		
		File tmpDir = new File(tmpPathVideos);
		
		tmpDir.mkdirs();
		try {
			FileUtils.cleanDirectory(tmpDir);
		} catch (IOException e1) {
			log.error("Error cleaning the directory");
		}
		
		String catMainDir = videoHelpers.getCatMainDir(bb, ov);

		String tn = videoHelpers.makeVideoThumbnail(bb, ov, tmpDir);
		if(tn.length()==0) {
			log.error("error creating "+ov.getFn());
			String dbc = "UPDATE videos set thumbnail='error' where pkey="+ov.getPkey();
			log.info("dbc:"+dbc);
			dbCommand.doCommand(dbc);
			return;
		}
		
		log.info("tn:"+tn);
		
		String fpn = config.getVideosDirectory()+"/"+catMainDir+"/"+ov.getSubdir()+"/"+tn;
		
		File trgFile = new File(fpn);
		File srcFile = new File(tmpPathVideos+"/"+tn);
		try {
			Files.copy(srcFile, trgFile);
		} catch (IOException e) {
			log.error("error copying "+e);
			return;
		}
		FixPermissions.FixFilePermissions(fpn);
		

		String dbc = "UPDATE videos set thumbnail='"+tn.replaceAll("'", "''")+"' where pkey="+ov.getPkey();
		log.info("dbc:"+dbc);
		dbCommand.doCommand(dbc);
		ov.setThumbnail(tn);	
		changed=true;
		
		System.exit(0);
	}

}
