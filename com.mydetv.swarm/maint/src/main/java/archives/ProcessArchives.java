package archives;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Helpers;
import maint.MaintException;
import maint.UpdateBroadcast;
import shared.data.Config;
import shared.data.OneArchive;
import shared.data.OneVideo;
import shared.db.DbArchive;
import shared.db.DbVideo;
import videosConvert.VideosHelpers;

public class ProcessArchives implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ProcessArchives.class);	
	@Inject private Config config;
	@Inject private DbArchive dbArchive;
	@Inject private DbVideo dbVideo;
	@Inject private VideosHelpers videoHelpers;
	@Inject private Helpers helpers;
	@Inject private UpdateBroadcast updateBroadcast;
	
	String tmpPathArchives;
	String tmpPathThisArchive;
	TreeMap<Integer, OneArchive> archives;
	boolean changed;
	
	@Override
	public void run() {
		log.info("starting the archives thread");
		tmpPathArchives = config.getTmpDir()+"/archives";
		while(true) {
			//refreshBlackboard.refreshBlackboard(bb);  don'[t need this, we get archives from db
			archives = new TreeMap<Integer, OneArchive>();
			processArchives();
			if(changed) 
				updateBroadcast.updateBroadcastBlackboard();
			else try {
				long nextHour = helpers.getMillisTillNextHour();
				log.info("nothing to do, sleeping "+nextHour);
				Thread.sleep(nextHour);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}			
		}
	}

	public int processArchives() {
		
		log.info("Process archives destination "+config.getVideosDirectory());
		archives=dbArchive.get();
		log.info("db table archives has "+archives.size()+" entries");
		
		Integer cnt=0;
		changed=false;
		for(OneArchive oa : archives.values()) {
			if(oa.getDtReceived()==null && oa.getErrorReason()==null) {
				try {
					processOneArchive(oa);
				} catch (MaintException e) {
					log.error("failed "+e);
					oa.setErrorReason(e.getMessage());
					dbArchive.archiveUpdate(oa);
				}
				cnt++;
			}
		}
		log.info("processed "+cnt+" archives");
		return cnt;
	}

	private void processOneArchive(OneArchive oa) throws MaintException {
		// start with a clean directory
		File tmpDir = new File(tmpPathArchives);
		tmpDir.mkdirs();
		try {
			FileUtils.cleanDirectory(tmpDir);
		} catch (IOException e1) {
			log.error("Error cleaning the directory");
		}
		
		log.info("processing "+oa.getSiteUrl());

		List<String> dest = getFileAndDirFromUrl(oa.getSiteUrl());
		String dn = dest.get(0);
		String fn = dest.get(1);
		//always make a subdir!
		if(dn.equals(""))
			dn=fn;
		log.info("dn:"+dn);
		log.info("fn:"+fn);
		
		tmpPathThisArchive = tmpPathArchives+"/"+oa.getSite()+"/"+dn;
		log.info("tmpPathThisArchive:"+tmpPathThisArchive);
		log.info("downloading "+fn+" to "+tmpPathThisArchive);
		
		String fnNoSuffix = getFilenameNoSuffix(fn);
		String videoFpn = tmpPathThisArchive+"/"+fn;
		oa.setVideoFn(fn);
		oa.setVideoPath(dn);
		oa.setFnNoSuffix(fnNoSuffix);	
		
		File directory = new File(tmpPathThisArchive);
		directory.mkdirs();

		long filesize = downloadFile(oa.getSiteUrl(), videoFpn);
		if(filesize<1) {
			throw new MaintException("Error downloading "+videoFpn);
		}
		oa.setFilesize(filesize);
		
		getWebpage(oa);
		
		float length = videoHelpers.getLength(videoFpn);
		if(oa.getThumbnail()==null) {
			log.info("no thumbnail found from the webpage, making our own");
			String tn = videoHelpers.makeThumbnail(tmpPathThisArchive, fn, fnNoSuffix, length, tmpDir);
			if(tn!=null && ! tn.equals(""))
				oa.setThumbnail(tn);
		}
		
		File trgDir = new File(config.getVideosDirectory()+"/archive.org/"+dn);
		File srcDir = new File(tmpPathThisArchive);
		log.info("copy "+srcDir.toString()+" to "+trgDir.toString());
		try {
			FileUtils.copyDirectory(srcDir, trgDir);
		} catch (IOException e) {
			log.error("error copying "+e);
			return;
		}
		
		Integer rv = dbArchive.archiveUpdate(oa);
		if(rv!=1) {
			log.error("Error updating rv="+rv+" for "+oa.getSiteUrl());
			return;
		}
		OneVideo ov = new OneVideo();
		ov.setFn(oa.getVideoFn());
		ov.setSubdir(oa.getVideoPath());
		ov.setCatSub(15); // hard coded archive.org uncategorized
		
		ov.setTitle(oa.getDetails());
		if(oa.getDetails()==null || oa.getDetails().equals("")) {
			if(oa.getPretitle()!=null && ! oa.getPretitle().equals(""))
				ov.setTitle(oa.getPretitle());
		}
		else
			ov.setTitle(oa.getDetails());
		ov.setThumbnail(oa.getThumbnail());
		ov.setFilesize(oa.getFilesize());
		
		ov.setLength(length);
		//Afternoon_Cartoon_Commercial_Breaks_-_Early_1980-cOBgoVpW9VM.jpg
		Integer pkey = dbVideo.insertVideo(ov);
		if(pkey<1) {
			log.error("Error inserting video for "+oa.getSiteUrl());
			return;
		}
		changed=true;	
	}
	
	/**
	 * Get the original webpage<br>
	 *  * see if there is a matching jpg/gif/png for the filename<br>
	 *  * if there is a meta file (xml/json) matching the filename try to get the title from that<br>
	 * @param oa
	 */
	private void getWebpage(OneArchive oa) {
		String newUrl;
		// get web page
		String pageFn = tmpPathThisArchive+"/"+oa.getFnNoSuffix()+".html";
		newUrl = oa.getSiteUrl().replaceAll(oa.getVideoFn(), "");
		String contents;

		try {
			//FileUtils.copyURLToFile(new URL(newUrl), new File(pageFn), 10000, 10000);
			downloadFile(newUrl, pageFn);
			log.info("got webpage "+pageFn);
			contents = new String(Files.readAllBytes(Paths.get(pageFn)), StandardCharsets.UTF_8);

			ArrayList<String> lines=null;
			String[] suffixes = {"gif", "jpg", "png"};
			for(String suffix : suffixes) {
				lines = findLines(contents, oa.getFnNoSuffix(), suffix);
				if(lines.size()>0)
					break;
			}

			if(lines!=null && lines.size()>0) {
				log.info("found an image "+lines.get(0));
				getThumbnail(oa, lines.get(0));
			}
			else 
				log.info("no image found from the webpage matching the filename");
			
			// try to get the title from meta file
			boolean found=false;
			lines = findLines(contents, oa.getFnNoSuffix(), "xml");
			if(lines.size()>0) {
				log.info("found xml "+lines.get(0));
				found=true;			
			}
			if(!found) {
				lines = findLines(contents, oa.getFnNoSuffix(), "json");
				if(lines.size()>0) {
					log.info("found json "+lines.get(0));
					getTitleFromMetaJson(oa, lines.get(0));
					found=true;
				}
			}
			if(!found) {
				lines = findLines(contents, "meta.xml");
				if(lines.size()>0) {
					log.info("found xml "+lines.get(0));
					getTitleFromMetaXml(oa, lines.get(0));
					found=true;
				}
			}
			
		} catch (IOException e1) {
			log.error("no can read "+pageFn);
		}
	}

	private void getTitleFromMetaJson(OneArchive oa, String line) {
		log.info("getting title from json");
		String fn = getFilenameFromHref(line);
		String newUrl = oa.getSiteUrl().replaceAll(oa.getVideoFn(), fn);
		String newFpn = tmpPathThisArchive+"/"+fn;
		log.info("try to get "+newUrl+" to "+newFpn);
		downloadFile(newUrl, newFpn);
	
		try {
			String contents = new String(Files.readAllBytes(Paths.get(newFpn)), StandardCharsets.UTF_8);
			String title = getTitleFromJson(contents);
			log.info("title:"+title);
			if(title.length()>0) 
				oa.setDetails(title);
		} catch (IOException e) {
			log.error("no can read");
		}
		
	}
	private void getTitleFromMetaXml(OneArchive oa, String line) {
		log.info("getting title from xml");
		String fn = getFilenameFromHref(line);
		String newUrl = oa.getSiteUrl().replaceAll(oa.getVideoFn(), fn);
		String newFpn = tmpPathThisArchive+"/"+fn;
		log.info("try to get "+newUrl+" to "+newFpn);
		downloadFile(newUrl, newFpn);
		
		try {
			String contents = new String(Files.readAllBytes(Paths.get(newFpn)), StandardCharsets.UTF_8);
			String title = getTitleFromXml(contents);
			log.info("title:"+title);
			if(title.length()>0) 
				oa.setDetails(title);
		} catch (IOException e) {
			log.error("no can read");
		}
		
	}

	private void getThumbnail(OneArchive oa, String line) {
		String tn=getFilenameFromHref(line);
		
		String newUrl = oa.getSiteUrl().replaceAll(oa.getVideoFn(), tn);
		String newFpn = tmpPathThisArchive+"/"+tn;
		log.info("try to get "+newUrl+" to "+newFpn);

			//FileUtils.copyURLToFile(new URL(newUrl), new File(newFpn), 10000, 10000);
			downloadFile(newUrl, newFpn);
		oa.setThumbnail(tn);
	}

	//https://archive.org/download/1931_Cartoon_Within_a_Cartoon_1/MakingEmMove.ogv
	private List<String> getFileAndDirFromUrl(String url) {
		List<String> rv = new ArrayList<String>();
		try {
			String[] parts = url.split("/");
			rv.add(parts[parts.length-2]);
			rv.add(parts[parts.length-1]);
		}  catch(Exception ex) {
			log.error("exception reading site "+ex);
		}
		return rv;
	}

	private String getFilenameNoSuffix(String fn) {
		String rv="";
		String[] pieces = fn.split("\\.");
		
		if(pieces.length>0) {
			for(int i=0; i<(pieces.length-1); i++) {
				if(rv.length()>0)
					rv+=".";
				rv+=pieces[i];
			}
			return rv;
		}
		return rv;
	}
	
	private ArrayList<String> findLines(String contents, String lookfor1, String lookfor2) {
		ArrayList<String> rv = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new StringReader(contents));
			String line = reader.readLine();
			while(line != null){
				if(line.contains(lookfor1) && line.contains(lookfor2)) {
					rv.add(line);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			log.error("no can read");		
		}	
		return rv;
	}
	private ArrayList<String> findLines(String contents, String lookfor1) {
		ArrayList<String> rv = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new StringReader(contents));
			String line = reader.readLine();
			while(line != null){
				if(line.contains(lookfor1)) {
					rv.add(line);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			log.error("no can read");		
		}	
		return rv;
	}
	
	private String getFilenameFromHref(String line) {
		String rv="";
		String patternString = "(href=\")(.*)(\">)";
		Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(line);
		if(matcher.find()) {
		    log.info(matcher.group(2));
		    rv=matcher.group(2);
		}
		if(rv.length()<1) {
			log.info("huh");
		}
		return rv;
	}

	// "title": "Afternoon Cartoon Commercial Breaks - Early 1980",
	private String getTitleFromJson(String contents) {
		String rv="";
		String patternString = "(\"title\":)(.*?\")(.*?)(\")";
		Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(contents);
		if(matcher.find()) {
		    log.info(matcher.group(3));
		    rv=matcher.group(3);
		}
		if(rv.length()<1) {
			log.info("huh");
		}
		return rv;
	}
	// <title>
	// The Mayberry Band Season 3 Episode 8 Of The Andy Griffith Show
	// </title>
	private String getTitleFromXml(String contents) {
		String rv="";
		String patternString = "(<title>)(.*?)(</title>)";
		Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(contents);
		if(matcher.find()) {
		    log.info(matcher.group(2));
		    rv=matcher.group(2);
		}
		if(rv.length()<1) {
			log.info("no title found");
		}
		return rv;
	}

	private static final int BUFFER_SIZE = 4096;
	private long downloadFile(String strUrl, String fpn) {

		log.info("downloading "+strUrl+" to "+fpn);
		try {
			
			URL url = new URL(strUrl.replaceAll(" ","%20"));
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setDoInput(true);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.connect();
			
			int responseCode = httpConn.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = httpConn.getInputStream();
				FileOutputStream outputStream = new FileOutputStream(fpn);
	            int bytesRead = -1;
	            byte[] buffer = new byte[BUFFER_SIZE];
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	                outputStream.write(buffer, 0, bytesRead);
	            }
	 
	            outputStream.close();
	            inputStream.close();
			}
			httpConn.disconnect();
		} catch (IOException e) {
			log.error("Error downloading "+strUrl+" to "+fpn,e);
			return 0;
		}

		File file = new File(fpn);
		if(!file.exists()) {
			log.error("File does not exist "+fpn);
			return 0;
		}
		if(file.length()<1) {
			log.error("File size is zero "+fpn);
			return 0;
		}
		return file.length();
	}
}
