package maint;

import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import archives.ProcessArchives;
import schedule.ProcessSchedule;
import shared.data.Config;
import shared.data.MaintConfig;
import shared.runonce.RunOnce;
import shared.setups.SetupEncryption;
import shared.setups.SwarmConfigException;
import thumbnails.CreateThumbnails;
import videosConvert.ProcessVideos;
import videosWalk.VideosWalk;

public class MaintImpl implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(MaintImpl.class);
	@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private Fixers fixers;
	@Inject private ProcessArchives processArchives;
	@Inject Provider<ProcessVideos> processVideosProvider;
	@Inject private ProcessSchedule processSchedule;
	@Inject private VideosWalk videosWalk;
	@Inject private CreateThumbnails createThumbnails;
	@Inject private Watchdog watchdog;
	@Inject private LogMailer logMailer;
	@Inject private JobScheduler jobScheduler;
	@Inject private GeoIp geoIp;
	
	public void setInjector(Injector mainInjector) {
		blackboard.setMainInjector(mainInjector);
	}

	@Override
	public void run() {
		
		log.info("firing up maint");
		//getEnv();
		
		// setup our configs and encryption
		try {
			blackboard.setSslContextFactory(SetupEncryption.SetupHttpsKeystore(config));
			SetupEncryption.SetupHttpsTruststore(config);
		} catch (SwarmConfigException e) {
			log.error("Error setting up configs"+e);
		}
		
		log.info("running  maint on machine "+config.getMachineName());
		
		Integer cnt = RunOnce.countRunning("maint.Main");
		if(cnt>1) {
			log.error("Already running, can't run twice ("+cnt+")");
			System.exit(0);
		}
		
		blackboard.setConvertedTypes(Arrays.asList("vp8-opus", "vp8-vorbis", "vp9-opus"));
		
		// use these if you need to fix something caused by code / db changes
		//fixers.fixAll();
		//System.exit(0);
		
		// fix archive.org so it does all it's work in tmp and then move things after it inserts into db
		//    so videosWalk thread doesn't grab things by accident before there are added

		MaintConfig mc = config.getMaint();
		
		//blackboard.setConvertedTypes(Arrays.asList("vp9-opus"));
		for(String convertedType : blackboard.getConvertedTypes()) {
			ProcessVideos processVideos = processVideosProvider.get();
			processVideos.setConvertedType(convertedType);
			Thread processVidoesThread = new Thread(processVideos);			
			processVidoesThread.setName("pv-"+convertedType);
			blackboard.getVideoThreads().add(processVidoesThread);
			if(mc.doProcessVideos()) 
				processVidoesThread.start();
		}		
		
		// get the geoip monthly
		Thread jobSchedulerThread = new Thread(jobScheduler);
		jobSchedulerThread.setName("jobScheduler");
		blackboard.setJobScheduler(jobSchedulerThread);
		if(mc.doGeoip()) 
			jobSchedulerThread.start();
		
		Thread processArchivesThread = new Thread(processArchives);
		processArchivesThread.setName("processArchives");
		if(mc.doArchives())
			processArchivesThread.start();
		blackboard.setArchivesThread(processArchivesThread);
				
		Thread processSchedThread = new Thread(processSchedule);
		processSchedThread.setName("processSchedule");
		if(mc.doSchedule())
			processSchedThread.start();
		blackboard.setScheduleThread(processSchedThread);
		
		Thread videosWalkThread = new Thread(videosWalk);
		videosWalkThread.setName("videosWalk");
		if(mc.doVideoWalk())
			videosWalkThread.start();
		blackboard.setVideosWalkThread(videosWalkThread);
		
		Thread logMailerThread = new Thread(logMailer);
		logMailerThread.setName("logMailer");
		if(mc.doLogMailer())
			logMailerThread.start();	
		blackboard.setLogWalkerThread(logMailerThread);
		
		Thread createThumbnailsThread = new Thread(createThumbnails);
		createThumbnailsThread.setName("thumbnails");
		if(mc.doThumbnails())
			createThumbnailsThread.start();
		blackboard.setCreateThumbnailsThread(createThumbnailsThread);
		
		/*Thread watchdogThread = new Thread(watchdog);
		watchdogThread.setName("watchdog");
		if(!debug)
			watchdogThread.start();
		blackboard.setWatchdogThread(watchdogThread);*/
		
		log.info("finished starting threads");
	
		//updateBroadcast.updateVideos();

	}

	private void getEnv() {
		
		Properties props = System.getProperties();
		for(Entry<Object, Object> entry : props.entrySet()) {
			log.info("prop:"+entry.getKey()+" "+entry.getValue());
		}
	}

}
