package maint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;
import shared.db.DbVideoConverted;
import videosConvert.VideosHelpers;

public class Watchdog implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(Watchdog.class);
	@Inject private Blackboard blackboard;
	@Inject private Config config;
	@Inject private DbVideoConverted dbVideoConverted;
	@Inject private VideosHelpers helpers;
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("started the watchdog thread");
		
	}
	
	
}
