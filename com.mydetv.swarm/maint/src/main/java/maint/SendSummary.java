package maint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;
import shared.db.DbConnection;
import shared.db.DbLog;
import shared.mail.SwarmMail;

public class SendSummary {

	private static final Logger log = LoggerFactory.getLogger(SendSummary.class);
	@Inject private DbLog dbLog;
	@Inject private DbConnection dbConnection;
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	
	public void sendLog(PrivateBlackboard bb) {
		
		String yl = dbLog.getYesterdaysLogs();
		log.debug(yl);
		
		String body="";
		body += "<html>\n";
		body+=SwarmMail.GetEmailHeader();
		body+="<body style='background-color:#562356; text-align: center; color: orange;'>\n";
		body+="<img src='https://"
			+config.getHostname()+":"+config.getWebPort()+"/s/people/buffalo.png'><br>\n";

		body+=getVideosSummary(bb);	
		body+=yl;
		body+="<br>\n";
		
		body+="</body>\n";
		body+="</html>\n";
		
		SwarmMail.SendEMail(config, "phomlish@homlish.net", "My Delaware TV log summary", body, "");
	}
	
	// crazy inefficient but it runs just once a day
	public String getVideosSummary(PrivateBlackboard bb) {
		Connection conn = null;
		String rv="<div style=\"text-align: left;\">";
		
		String cmd = "";
		PreparedStatement s;
		ResultSet rs;
		
		try {
			Integer cntVideos=0;
			conn = dbConnection.getConnection();
			cmd="select count(*) cnt from videos where active=1 and donotuse=false";
			s = conn.prepareStatement(cmd);
			rs = s.executeQuery();
			while (rs.next()) {
				cntVideos=rs.getInt("cnt");
				rv+="we have "+cntVideos+" videos (active and ! donotuse)<br>\n";
			}
			rs.close();
			s.close();
			
			for(String convertedType : blackboard.getConvertedTypes()) {
				int cntFailed=0;
				int cntGood=0;
				cmd="select pkey from videos where active=1 and donotuse=false";
				s = conn.prepareStatement(cmd);
				rs = s.executeQuery();
				while (rs.next()) {
					Integer pkey = rs.getInt("pkey");
					//log.info("pkey:"+pkey);
					cmd = "select failedReason from videoConverted ";
					cmd+=" where  dtInactive is null and convertedType='"+convertedType+"'";
					cmd+="and videoId ="+pkey;
					PreparedStatement s2 = conn.prepareStatement(cmd);
					ResultSet rs2 = s2.executeQuery();
					while (rs2.next()) {
						String failedReason = rs2.getString("failedReason");
						if(failedReason==null || failedReason.equals(""))
							cntGood++;
						else {
							cntFailed++;
						}
					}
					rs2.close();
					s2.close();
				}
				rs.close();
				s.close();			
				
				rv+=convertedType+" good "+cntGood+", failed "+cntFailed+" not done "+(cntVideos-cntGood-cntFailed)+"<br>\n";
				//rv+=failed;
				
			}
			
		} catch (SQLException e) {
			log.error("no can '"+cmd+"'"+" "+e);
		}
		finally {
			dbConnection.tryClose(conn);
		}
		rv+="</div>";
		log.info("rv:"+rv);
		return rv;
	}
}
