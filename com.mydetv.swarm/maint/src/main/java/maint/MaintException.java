package maint;

public class MaintException extends Exception {
	public MaintException(String errorMessage, Throwable ex) {
		super(errorMessage, ex);
	}
	public MaintException(String errorMessage) {
		super(errorMessage);
	}
}
