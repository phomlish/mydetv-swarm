package maint;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import shared.data.Config;
import shared.setups.ReadConfigJson;


public class Main {
	
	static {System.setProperty("user.timezone", "UTC");}
	static {System.setProperty("logback.configurationFile", "logback-maint.xml");}
	private static Logger log = LoggerFactory.getLogger(Main.class);
	
	@Inject private static MaintImpl maintImpl;
	
	public static void main(String[] mainArgs)
	{
	try {
		if(mainArgs.length<1) {
			log.error("you must provide the config directory on the command line...");
			throw new ConfigurationException();
		}
		
		//Config config = ReadConfig.GetConfig(mainArgs[0]);
		Config config = ReadConfigJson.GetConfig(mainArgs[0]);
		
		Injector mainInjector = Guice.createInjector(new Bindings(config));
		maintImpl = mainInjector.getInstance(MaintImpl.class);
		maintImpl.setInjector(mainInjector);
		maintImpl.run();;
		
	} catch (Exception e) {
		log.warn("Check previous ERROR/WARNING logs as well!!!!", e);
	}
	}

}
