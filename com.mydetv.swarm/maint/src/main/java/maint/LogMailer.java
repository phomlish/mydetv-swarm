package maint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class LogMailer implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(LogMailer.class);
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private SendSummary sendSummary;
	@Inject private Helpers helpers;
	
	private PrivateBlackboard bb = new PrivateBlackboard();
	
	@Override
	public void run() {
		log.info("starting the log mailer");
		while(true) {
			try {
				refreshBlackboard.refreshBlackboard(bb);
				sendSummary.sendLog(bb);
				long tomorrow = helpers.getMillisTillTomorrowEastern();
				log.info("sleeping till tomorrow "+tomorrow);
				Thread.sleep(tomorrow);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}
		}		
	}
}
