package maint;

import com.google.inject.Binder;
import com.google.inject.Module;

import archives.ProcessArchives;
import schedule.SchedHelpers;
import shared.data.Config;
import shared.db.DbArchive;
import shared.db.DbConnection;
import shared.db.DbVideo;
import shared.db.DbVideoCategoryMain;
import shared.db.DbVideoCategorySub;
import shared.db.DbVideoConverted;
import shared.setups.SetupEncryption;
import thumbnails.CreateThumbnails;
import videosConvert.DoConvert;
import videosConvert.VideosHelpers;
import videosWalk.PopulateConverted;
import videosWalk.PopulateVideos;
import videosWalk.Verify;
import videosWalk.VideosWalk;

public class Bindings implements Module {
	
	Config config;
	public Bindings(Config config) {
		this.config = config;
	}
	
	public void configure(Binder binder) {
		
	binder.bind(Config.class).toInstance(config);
	binder.bind(Blackboard.class).asEagerSingleton();
	
	binder.bind(SetupEncryption.class).asEagerSingleton();
	
	binder.bind(DbConnection.class).asEagerSingleton();
	binder.bind(DbVideoCategorySub.class).asEagerSingleton();
	binder.bind(DbVideo.class).asEagerSingleton();
	binder.bind(DbVideoConverted.class).asEagerSingleton();
	binder.bind(DbArchive.class).asEagerSingleton();
	binder.bind(DbVideoCategoryMain.class).asEagerSingleton();
	
	// maint impl
	binder.bind(RefreshPrivateBlackboard.class).asEagerSingleton();
	binder.bind(Fixers.class).asEagerSingleton();
	binder.bind(Ffmpeg.class).asEagerSingleton();
	binder.bind(Watchdog.class).asEagerSingleton();
	binder.bind(LogMailer.class).asEagerSingleton();
	binder.bind(JobScheduler.class).asEagerSingleton();
	
	// videos
	//binder.bind(ProcessVideos.class).asEagerSingleton();
	binder.bind(Verify.class).asEagerSingleton();
	binder.bind(PopulateVideos.class).asEagerSingleton();
	binder.bind(PopulateConverted.class).asEagerSingleton();
	binder.bind(DoConvert.class).asEagerSingleton();
	binder.bind(VideosHelpers.class).asEagerSingleton();
	
	// videosWalk
	binder.bind(VideosWalk.class).asEagerSingleton();
	// schedule
	binder.bind(SchedHelpers.class).asEagerSingleton();
	
	// archives
	binder.bind(ProcessArchives.class).asEagerSingleton();
	
	// CreateThumbnails
	binder.bind(CreateThumbnails.class).asEagerSingleton();
	
	}

}
