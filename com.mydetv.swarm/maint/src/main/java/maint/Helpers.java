package maint;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class Helpers {
	
	@Inject private Blackboard blackboard;
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Helpers.class);

	public long getMillisTillNextHour() {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime later = now.plusHours(1).truncatedTo(ChronoUnit.HOURS);
		long diff = ChronoUnit.MILLIS.between(now, later)+1000;
		// add an extra 0-29 minutes
		int plusSome = blackboard.getSecureRandom().nextInt(30) * 1000 * 60;
		//log.info("now:"+now+",next hour:+"+later+",diff:"+diff);
		return diff+plusSome;
	}
	
	public long getMillisTillTomorrowEastern() {
		ZoneId eastern = ZoneId.of("US/Eastern");
		ZonedDateTime nowEastern = ZonedDateTime.now(eastern);
		ZonedDateTime laterEastern = nowEastern.plusDays(1).truncatedTo(ChronoUnit.DAYS);
		long diff = ChronoUnit.MILLIS.between(nowEastern, laterEastern);		
		//log.info("nowEastern:"+nowEastern+",laterEastern:+"+laterEastern+",diff:"+diff);
		return diff;
	}
	

}
