package maint;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.Properties;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/*
 * cronSchedule looks like 

*   *     *     *   *    *   *     command to be executed
-   -     -     -   -    -   -
|   |     |     |   |    |   +-- year
|   |     |     |   |    +----- day of week (1 - 7) (Sunday=1)
|   |     |     |   +------- month (1 - 12)
|   |     |     +--------- day of month (1 - 31)
|   |     +----------- hour (0 - 23)
|   +------------- min (0 - 59)
+------------- seconds (0 - 59) 
 */

public class JobScheduler implements Runnable  {

	private static final Logger log = LoggerFactory.getLogger(JobScheduler.class);
	@Inject private Blackboard blackboard;
	Scheduler sched;
	JobKey geoipJobKey;
	
	@Override
	public void run() {
		log.info("------- Initializing ----------------------");
		
		try {
			Properties prop = new Properties();
			prop.put("org.quartz.threadPool.threadCount", "2");
			
			SchedulerFactory sf = new StdSchedulerFactory(prop);
			
			sched = sf.getScheduler();
			
			sched.setJobFactory(blackboard.getMainInjector().getInstance(GuiceJobFactory.class));
			
			geoipJobKey = JobKey.jobKey("geoip", "maint");
			JobDetail geoipJob = newJob(GeoIp.class).withIdentity(geoipJobKey).build();
			CronTrigger geoipTrigger = newTrigger().withIdentity("fileCheckTrigger", "jukebox").
				withSchedule(cronSchedule("00 17 00 ? * 4#1")).build();
			Date ft = sched.scheduleJob(geoipJob, geoipTrigger);			
			log.info(geoipJob.getKey()+" will run at: " +ft+" and repeat based on expression: "+geoipTrigger.getCronExpression());
						
		} catch (SchedulerException e1) {
			log.error("e:"+e1);
		}
	}
	
}
