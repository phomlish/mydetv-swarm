package maint;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;
import shared.data.OneChannelSchedule;
import shared.data.OneMbp;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.webuser.OneWebUserImage;
import shared.data.OneVideoConverted;
import shared.db.DbChannelSchedule;
import shared.db.DbCommand;
import shared.db.DbConnection;
import shared.db.DbSchedule;
import shared.db.DbWebUsers;
import shared.helpers.FilenameHelpers;
import shared.helpers.FixPermissions;
import videosConvert.VideosHelpers;

public class Fixers {
	
	private static final Logger log = LoggerFactory.getLogger(Fixers.class);
	@Inject private Config config;
	@Inject private VideosHelpers helpers;
	@Inject private DbConnection dbConnection;
	@Inject private DbCommand dbCommand;
	@Inject private DbSchedule dbSchedule;
	@Inject private DbChannelSchedule dbChannelSchedule;
	@Inject private DbWebUsers dbWebUsers;
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private SendSummary sendSummary;
	
	private PrivateBlackboard bb = new PrivateBlackboard();
	
	public void fixAll() {
		refreshBlackboard.refreshBlackboard(bb);
		//dumpPeopleImages();
		//checkLength();
		//fixVC();
		//summary();
		//filetest();
		//fixMbpShow();
		//fixMbpsMatchFromVideos();
		//fixMbpsMatchToVideos();
		//fixOldConverted();
		//fixVideoId();
		//fixConvertedTooMany();
		editSchedule();
	}
	
	/**
	 * delete 2 hours before and afer now
	 */
	private void editSchedule() {
		log.info("editing schedule");
	
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime earlier = now.minusHours(2);
		LocalDateTime later = now.plusHours(2);
		log.info("earlier :"+earlier);
		log.info("later   :"+later);
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			//HashMap<Integer, OneChannelSchedule> scheds = dbChannelSchedule.getAll(1);
			
			TreeMap<Integer, OneSchedule> scheds = dbSchedule.getSchedulesAfter(1, earlier);
			log.info("got "+scheds.size()+" schedules");
			OneSchedule last = null;
			OneSchedule first = null;
			for(OneSchedule os : scheds.values()) {
				if(first==null) {
					first=os;
					continue;
				}
				if(os.getDt().isAfter(later)) {
					last=os;
					break;
				}
				log.info("deleting "+os.log());	
				//dbSchedule.removeSchedule(os);
			}
			
			log.info("first :"+first.log());
			log.info("last  :"+last.log());
			
			log.info("done");
		} catch (SQLException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);	
		
	}
	private void dumpPeopleImages() {
		log.info("dumping images "+config.getInstance());
		
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			TreeMap<Integer, OneWebUserImage> images = dbWebUsers.getWebUserImages(conn);
			for(OneWebUserImage image : images.values()) {
				log.info("image "+image.getIdWebUserImage());
				
				String nfn = UUID.randomUUID().toString()+".png";
				
				Path path = Paths.get(config.getExternalWebDirectory(),"people",image.getIdWebUser().toString());
				if(! Files.exists(path)) {
					Files.createDirectories(path);
					FixPermissions.FixFilePermissions(path.toString());
				}
				path = Paths.get(path.toString(), nfn);
				log.info("fpn "+path.toString());
				Files.write(path, image.getOrigFile());
				FixPermissions.FixFilePermissions(path.toString());
				String dbc = "UPDATE webUserImage set newFn='"+nfn+"'";
				dbc +=" where idWebUserImage ="+image.getIdWebUserImage();
				log.info("dbc:"+dbc);
				dbCommand.doCommand(conn, dbc);
			}
			
		} catch (SQLException | IOException e) {
			log.error("failed e:"+e);
		}
		dbConnection.tryClose(conn);		
	}
	
	private void checkLength() {
		String fpn = "/private/nfs/a6/usr6/mydetv/webm/archive.org/Beverly_Hillbillies_Ep08_Jethro_Goes_To_School/BH08_Jethro_Goes_To_School.ogv.vp8-opus.webm";
		log.info("checking "+fpn);
		float length = helpers.getLength(fpn);
		log.info("length:"+length);
	}
	
	/*
	private void fixVC() {

		for(VideoCategorySub vcs : bb.getVideoCategoriesSub().values()) {
			String uuid = UUID.randomUUID().toString();
			String fpn = "/private/nfs/a6/usr2/mydetv/externalWeb/i/vsc/"+uuid+".png";
			log.info("fpn:"+fpn);
			try {
				//if(vcs.getOrigFile()!=null) {
				//FileUtils.writeByteArrayToFile(new File(fpn), vcs.getOrigFile());
				String dbc = "UPDATE videoCategorySub set imageUUID='"+uuid+"' where idvideo_category="+vcs.getIdvideo_category();
				dbCommand.doCommand(dbc);
				//}
			} catch (IOException e) {
				log.error("no create "+e);
			}			
		}
		
	}
	*/
	
	public void summary() {
		log.info("\n"+sendSummary.getVideosSummary(bb));		
	}
	private void filetest() {
		String dn = config.getVideosConvertedDirectory()+"/test";
		String fn = dn+"/test.me";
		log.info("create "+dn+" "+fn);
		File dir = new File(dn);
		
		if(dir.exists()) {
			dir.delete();
		}
		dir.mkdirs();
		
		File f = new File(fn);
		if(f.exists())
			f.delete();
		try {
			f.createNewFile();

		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		FixPermissions.FixFilePermissions(dn);
		FixPermissions.FixFilePermissions(fn);
	}
	
	//20071112-1.C.wmv
	private void fixMbpShow() {
		for(OneMbp mbp : bb.getMbps().values()) {
		
			if(mbp.getShowset()!=null) 
				continue;
			String fn = mbp.getFn();
			String strShowdate;
			if(fn.length()< 8)
				continue;
			
			
		}
	}

	private void fixMbpsMatchFromVideos() {
		int cnt=0;
		int nf=0;
		for(OneVideo ov : bb.getVideos().values()) {
			VideoCategorySub ovcs = bb.getVideoCategoriesSub().get((ov.getCatSub()));
			VideoCategoryMain ovcm = bb.getVideoCategoriesMain().get(ovcs.getCatMain());
			if(! ovcm.getName().equals("mbp"))
				continue;
			if(ov.getDtInactive()!=null)
				continue;
			if(ov.isDoNotUse())
				continue;
			//log.info("looking at "+ov.getFn());
			OneMbp mbp = findVideoMbp(ov.getFn());
			if(mbp==null) {
				//log.info("not found "+ov.log());
				String what;
				String values = ov.getPkey()+",\""+ov.getFn()+"\",\""+ov.getFn()+"\",UTC_TIMESTAMP()";
				if(ov.getTitle()!=null && ! ov.getTitle().equals("")) {
					what = "(idVideo, fn, fnVideo, dt_added, details)";
					values+=",\""+ov.getTitle()+"\"";
				}
				else
					what = "(idVideo, fn, fnVideo, dt_added)";
				
				if(ov.getTitle()!=null && ! ov.getTitle().equals("")) {
					String dbc = "INSERT into mbp "+what+" values ("+values+");";
					log.info("dbc:"+dbc);
					//dbCommand.doCommand(dbc);
				}
				nf++;
			}
			else {
				//log.info("found "+mbp.getIdVideo());
			}
			
			cnt++;
		}
		log.info("found "+cnt+" vides in mbp category, nf:"+nf);
	}
	// that's a wrap, videos that are missing are missing but are on a0 or as dvd unconverted
	private void fixMbpsMatchToVideos() {
		int cnt=0;
		int nf=0;
		String lc = "";
		for(OneMbp mbp : bb.getMbps().values()) {
			//log.info("looking at "+mbp.getFn()+" "+mbp.getFn2()+" "+mbp.getFnOrig());
			OneVideo ov = findMbpVideo(mbp.getFnOrig());
			if(ov==null) 
				ov=findMbpVideo(mbp.getFn2());
			if(ov==null)
				ov=findMbpVideo(mbp.getFn());
			if(ov!=null) {
				String dbc = "UPDATE mbp set idVideo="+ov.getPkey()+",fnVideo=\""+ov.getFn()+"\" where idMbp="+mbp.getIdMbp();
				dbCommand.doCommand(dbc);
			}
			else {
				nf++;
				log.info("not found "+mbp.getFn()+" "+mbp.getFn2()+" "+mbp.getFnOrig());
				lc+="locate "+mbp.getFn()+"\n";
			}
			if(mbp.getShowdate()==null) {
				
			}
			cnt++;
		}
		log.info("looked at "+cnt+" mbps, nf:"+nf);
		log.info("lc:\n"+lc);
	}
	
	private OneMbp findVideoMbp(String fn) {
		OneMbp rv=null;
		for(OneMbp mbp : bb.getMbps().values()) {
			if(mbp.getFn().equals(fn)) {
				if(rv!=null) {
				}
				rv=mbp;
			}
		}
		if(rv==null) {
			for(OneMbp mbp : bb.getMbps().values()) {
				if(mbp.getFn2()==null)
					continue;
				if(mbp.getFn2().equals(fn)) {
					if(rv!=null) {
					}
					rv=mbp;
				}
			}
		}
		if(rv==null) {
			for(OneMbp mbp : bb.getMbps().values()) {
				if(mbp.getFnOrig()==null)
					continue;
				if(mbp.getFnOrig().equals(fn)) {
					if(rv!=null) {
					}
					rv=mbp;
				}
			}
		}
		
		return rv;
	}
	private OneVideo findMbpVideo(String fn) {
		OneVideo rv=null;
		for(OneVideo ov : bb.getVideos().values()) {
			if(ov.getFn().equals(fn)) {
				if(rv!=null) 
					log.info("dup "+ov.getPkey()+" "+rv.getPkey());
				rv=ov;
			}
		}
		return rv;
	}
	private void fixOldConverted() {
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			if(ovc.getFn().contains("PillsburyCakeMix1956Ad1"))
				log.info("PillsburyCakeMix1956Ad1");
			if(ovc.getDtInactive()!=null) 
				continue;
			if(ovc.getFailedReason()!=null)
				continue;
			if(! ovc.getConvertedType().equals("vp8-opus"))
				continue;
			if(ovc.getFn().contains("vp8-opus"))
				continue;
			
			OneVideo ov = bb.getVideos().get(ovc.getPkey());
			
			String catSubdir = helpers.getCatMainDir(bb, ov);
			String opn = config.getVideosConvertedDirectory()+"/"+catSubdir+"/"+ov.getSubdir()+"/"+ovc.getFn();			
			log.info("considering opn "+opn);
			String nfn = FilenameHelpers.StripSuffix(ovc.getFn()) +".vp8-opus.webm";
			String npn = config.getVideosConvertedDirectory()+"/"+catSubdir+"/"+ov.getSubdir()+"/"+nfn;
			log.info("considering npn "+npn);
			File newFile = new File(npn);
			if(newFile.exists() && doesConvertedExist(nfn)) {
				log.error("can't move it, already exists");
			}
			else {
				String cmd = "update videoConverted set fn=\""+nfn+"\" where videoConvertedId="+ovc.getVideoConvertedId();
				log.info("cmd:"+cmd);
				dbCommand.doCommand(cmd);
				
				File oldFile = new File(opn);
				log.info("moving "+opn+" to "+npn);
				oldFile.renameTo(newFile);
				log.info("done");
			}
			
		}
	}
	
	private boolean doesConvertedExist(String nfn) {
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			if(ovc.getFn().equals(nfn))
				return true;
		}
		return false;
	}
	/*
	private void fixVideoId() {
		for(OneVideo ov : bb.getVideos().values()) {
			if(ov.getConvertedId()==0 || ov.getConvertedId()==-1)
				continue;
			if(!bb.getVideosConverted().containsKey(ov.getConvertedId())) {
				log.error("missing "+ov.log());
				continue;
			}
			OneVideoConverted ovc = bb.getVideosConverted().get(ov.getConvertedId());
			if(! ovc.getPkey().equals(ov.getPkey())) {
				String cmd = "UPDATE videoConverted set videoId="+ov.getPkey()+" where videoConvertedId="+ov.getConvertedId();
				log.info("cmd:"+cmd);
				dbCommand.doCommand(cmd);
			}
		}
	}
	*/
	/**
	 * there should only be one VideoConverted which is active by ConvertedType
	 */
	private void fixConvertedTooMany() {
		int cntVideos=0;
		for(OneVideo ov : bb.getVideos().values()) {
			cntVideos++;
			TreeMap<LocalDateTime, OneVideoConverted> converted = new TreeMap<LocalDateTime, OneVideoConverted>();
			for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
				if(ovc.getPkey().equals(ov.getPkey()) && ovc.getConvertedType().equals("vp8-opus"))
					converted.put(ovc.getDtConverted(), ovc);
			}
			
			if(converted.size()>1) {
				log.error("too mamy for "+ov.getPkey());
				int i=0;
				for(OneVideoConverted ovc : converted.values()) {
					log.info("ovc:"+ovc.getDtConverted()+" "+ovc.getConvertedType());
					if(i<(converted.size()-1)) {
						log.info("delete:"+ovc.getDtConverted()+" "+ovc.getConvertedType());
						String cmd = "UPDATE videoConverted set dtInactive='2018-11-17 8:27' where videoConvertedId="+ovc.getVideoConvertedId();
						log.info("cmd:"+cmd);
						dbCommand.doCommand(cmd);
					}
					i++;
				}

			}
		
		}
		log.info("cnt:"+cntVideos);
	}
	
	
	
/*
	private void removeMissingConvertedFromSchedules() {
		
	}
	*/
	/*
	public void fixMbp() {
		HashMap<Integer, OneMbp> mbps = dbMbp.get();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		String dbc = "";
		Connection conn=null;
		try {
			conn = dbConnection.getConnection();

			for(OneMbp mbp : mbps.values()) {
				//DateTime dt = mbp.getShowdate();
				LocalDate old = LocalDate.of(1971, 01, 01);
				if(dt != null && dt.isAfter(old.toEpochDay())) {
									
					dbc = "update mbp set sd='"+dt.toString(dtf)+"' where pkey="+mbp.getPkey();
					log.info("dbc:"+dbc);
					java.sql.Statement stmt = conn.createStatement();
					stmt.execute(dbc);		
				}
			}
		} catch(Exception e) {
			log.error("Error:"+e+"\n"+dbc);
		}
		dbConnection.tryClose(conn);
	}
	*/
	/*
	private void fixSchedules() {
		for(OneVideo ov : blackboard.getVideos().values()) {
			Integer sc = 1;
			if(sc == 2) 
				continue;
			if(sc != 3 && sc!=4 && sc!=5)
				continue;
			
			//	continue;
			//if(ov.getSchedule()==null)
			//	continue;
			//if(! ov.getSchedule().equals(1))
			//	continue;
			
			//if(ov.getSchedule() != null && ov.getSchedule().equals(1)) {
				log.info("need to set schedule:"+ov.getPkey());
				OneChannelSchedule ocs = new OneChannelSchedule();
				ocs.setChannelsId(4);
				ocs.setPkey(ov.getPkey());
				dbChannelSchedule.insert(ocs);			
			//}
		}
	}
	*/
	
}
