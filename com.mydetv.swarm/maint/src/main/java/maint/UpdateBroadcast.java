package maint;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;

public class UpdateBroadcast {

	private static final Logger log = LoggerFactory.getLogger(UpdateBroadcast.class);
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	
	/**
	 * calls broadcast /api/admin/maint<br>
	 * broadcast will then update it's blackboard's video/schedule/etc hash/tree maps
	 */
	public void updateBroadcastBlackboard() {
		String url = "https://"+config.getHostname()+":"+config.getWebPort()+"/api/admin/maint";
		HttpClient httpClient = new HttpClient(blackboard.getSslContextFactory());
		httpClient.setFollowRedirects(false);
		try {
			log.info("updating "+url);
			httpClient.start();
			ContentResponse response = httpClient.GET(url+"/"+config.getApiKey());
			log.info("response:"+response.getContentAsString());
			
		} catch (Exception e) {
			log.error("no joy updating broadcast "+e);
		} finally {
			try {
				httpClient.stop();
			} catch (Exception e) {
				log.error("Error shutting down our client :"+e);
			}
		}
	}
	

}
