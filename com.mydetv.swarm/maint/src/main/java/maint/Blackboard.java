package maint;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.google.inject.Injector;

public class Blackboard {
	
	private Injector mainInjector;
	private SecureRandom secureRandom = new SecureRandom();
	private SslContextFactory sslContextFactory;
	
	private Thread archivesThread;
	private List<Thread> videoThreads = new ArrayList<Thread>();
	private Thread scheduleThread;
	private Thread watchdogThread;
	private Thread videosWalkThread;
	private Thread logMailerThread;
	private Thread CreateThumbnailsThread;
	private Thread jobScheduler;
	private Object maintLock = new Object();
	
	private List<String> convertedTypes = new ArrayList<String>();

	// getters/setters
	public Injector getMainInjector() {
		return mainInjector;
	}
	public void setMainInjector(Injector mainInjector) {
		this.mainInjector = mainInjector;
	}
	public Thread getArchivesThread() {
		return archivesThread;
	}
	public void setArchivesThread(Thread archivesThread) {
		this.archivesThread = archivesThread;
	}
	public Object getMaintLock() {
		return maintLock;
	}
	public void setMaintLock(Object maintLock) {
		this.maintLock = maintLock;
	}
	public Thread getScheduleThread() {
		return scheduleThread;
	}
	public void setScheduleThread(Thread scheduleThread) {
		this.scheduleThread = scheduleThread;
	}
	public Thread getWatchdogThread() {
		return watchdogThread;
	}
	public void setWatchdogThread(Thread watchdogThread) {
		this.watchdogThread = watchdogThread;
	}
	public List<String> getConvertedTypes() {
		return convertedTypes;
	}
	public void setConvertedTypes(List<String> convertedTypes) {
		this.convertedTypes = convertedTypes;
	}
	public List<Thread> getVideoThreads() {
		return videoThreads;
	}
	public void setVideoThreads(List<Thread> videoThreads) {
		this.videoThreads = videoThreads;
	}
	public Thread getVideosWalkThread() {
		return videosWalkThread;
	}
	public void setVideosWalkThread(Thread videosWalkThread) {
		this.videosWalkThread = videosWalkThread;
	}
	public SecureRandom getSecureRandom() {
		return secureRandom;
	}
	public void setSecureRandom(SecureRandom secureRandom) {
		this.secureRandom = secureRandom;
	}
	public Thread getLogWalkerThread() {
		return logMailerThread;
	}
	public void setLogWalkerThread(Thread logMailerThread) {
		this.logMailerThread = logMailerThread;
	}
	public Thread getCreateThumbnailsThread() {
		return CreateThumbnailsThread;
	}
	public void setCreateThumbnailsThread(Thread createThumbnailsThread) {
		CreateThumbnailsThread = createThumbnailsThread;
	}
	public SslContextFactory getSslContextFactory() {
		return sslContextFactory;
	}
	public void setSslContextFactory(SslContextFactory sslContextFactory) {
		this.sslContextFactory = sslContextFactory;
	}
	public Thread getJobScheduler() {
		return jobScheduler;
	}
	public void setJobScheduler(Thread jobScheduler) {
		this.jobScheduler = jobScheduler;
	}
}
