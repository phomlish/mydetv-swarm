package maint;

import java.io.File;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import shared.data.Config;
import shared.ffmpeg.ProcessBuilders;

//https://developer.mozilla.org/en-US/docs/Web/HTML/Supported_media_formats
//originally used vp8/opus
//vp8/vorbis - good for all
//vp9/opus good for chrome/firefox/opera
//keep reading this to figure out how to send 3 streams in one
//https://webrtchacks.com/chrome-vp9-svc/

public class Ffmpeg {

	@Inject private Config config;
	private static final Logger log = LoggerFactory.getLogger(Ffmpeg.class);
	
	
	public void convert_Webm_Vp8_Vorbis_Pass1(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {
		/*
ffmpeg -i 20060501B.C.wmv \
    -c:v libvpx -crf 10 -vf scale=640x480 -b:v 512k \
    -c:a libvorbis -b:a 128k -q 10  \
    -pass 1 \
    -passlogfile abcd \
    -y test-vp8-vorbis.webm  
		 */
		
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx");
		pieces.add("-crf");
		pieces.add("10");
		pieces.add("-b:v");
		pieces.add("512k");
		pieces.add("-c:a");		// audio
		pieces.add("libvorbis");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-q");
		pieces.add("10");
		pieces.add("-pass");	// pass
		pieces.add("1");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}
	public void convert_Webm_Vp8_Vorbis_Pass2(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx");
		pieces.add("-crf");
		pieces.add("10");
		pieces.add("-b:v");
		pieces.add("512k");
		pieces.add("-c:a");		// audio
		pieces.add("libvorbis");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-q");
		pieces.add("10");
		pieces.add("-pass");	// pass
		pieces.add("2");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);	
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}
	
	public void convert_Webm_Vp9_Opus_Pass1(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {

		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx-vp9");
		pieces.add("-b:v");
		pieces.add("512k");
		
		pieces.add("-c:a");		// audio
		pieces.add("libopus");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-vbr");
		pieces.add("on");
		pieces.add("-compression_level");
		pieces.add("10");
		
		pieces.add("-pass");	// pass
		pieces.add("1");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}
	public void convert_Webm_Vp9_Opus_Pass2(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx-vp9");
		pieces.add("-b:v");
		pieces.add("512k");
		
		pieces.add("-c:a");		// audio
		pieces.add("libopus");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-vbr");
		pieces.add("on");
		pieces.add("-compression_level");
		pieces.add("10");
		
		pieces.add("-pass");	// pass
		pieces.add("2");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);	
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}

	public void convert_Webm_Vp8_Opus_Pass1(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {
		
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx");
		pieces.add("-b:v");
		pieces.add("512k");
		
		pieces.add("-c:a");		// audio
		pieces.add("libopus");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-vbr");
		pieces.add("on");
		pieces.add("-compression_level");
		pieces.add("10");
		
		pieces.add("-pass");	// pass
		pieces.add("1");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}
	public void convert_Webm_Vp8_Opus_Pass2(String uuid, String oldFp, String tmpFp, File tmpDir, Logger plog) {
		ArrayList<String> pieces = new ArrayList<String>();
		pieces.add(config.getFfmpeg());
		pieces.add("-i");
		pieces.add(oldFp);
		pieces.add("-c:v");		// video
		pieces.add("libvpx");
		pieces.add("-b:v");
		pieces.add("512k");
		
		pieces.add("-c:a");		// audio
		pieces.add("libopus");
		pieces.add("-b:a");
		pieces.add("128k");
		pieces.add("-vbr");
		pieces.add("on");
		pieces.add("-compression_level");
		pieces.add("10");
		
		pieces.add("-pass");	// pass
		pieces.add("2");
		pieces.add("-passlogfile");
		pieces.add(uuid);
		pieces.add("-y");
		pieces.add(tmpFp);
		
		log.info("pieces:"+String.join(" ", pieces));
		ProcessBuilder pb = new ProcessBuilder(pieces);	
		pb.directory(tmpDir);
		ProcessBuilders.runPB(pb, plog);
	}
}
