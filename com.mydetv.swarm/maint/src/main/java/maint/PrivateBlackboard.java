package maint;

import java.util.HashMap;
import java.util.TreeMap;

import shared.data.OneMbp;
import shared.data.OneMydetvChannel;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;
import shared.data.VideoType;

public class PrivateBlackboard {

	private HashMap<Integer, VideoCategoryMain> videoCategoriesMain;
	private HashMap<Integer, VideoCategorySub> videoCategoriesSub;
	private HashMap<Integer, OneVideo> videos;
	private HashMap<Integer, OneVideoConverted> videosConverted;
	private HashMap<Integer, OneMbp> mbps;
	private TreeMap<Integer, OneMydetvChannel> channels;
	// for scheduling
	private TreeMap<Integer, VideoType> videoTypes;
	//private HashMap<Integer, OneVideo> channelVideos;
	private OneMydetvChannel currentMydetvChannel;
	private Integer fallback;
	// locking/threads
	private Object maintLock = new Object();
	
	// used for converting
	private String convertedType;
	private String tmpPath;
	
	// getters/setters
	public HashMap<Integer, VideoCategorySub> getVideoCategoriesSub() {
		return videoCategoriesSub;
	}
	public void setVideoCategoriesSub(HashMap<Integer, VideoCategorySub> videoCatagories) {
		this.videoCategoriesSub = videoCatagories;
	}
	public HashMap<Integer, OneVideo> getVideos() {
		return videos;
	}
	public void setVideos(HashMap<Integer, OneVideo> videos) {
		this.videos = videos;
	}
	public HashMap<Integer, OneVideoConverted> getVideosConverted() {
		return videosConverted;
	}
	public void setVideosConverted(HashMap<Integer, OneVideoConverted> videosConverted) {
		this.videosConverted = videosConverted;
	}
	public Object getMaintLock() {
		return maintLock;
	}
	public void setMaintLock(Object maintLock) {
		this.maintLock = maintLock;
	}
	public String getConvertedType() {
		return convertedType;
	}
	public void setConvertedType(String convertedType) {
		this.convertedType = convertedType;
	}
	public TreeMap<Integer, OneMydetvChannel> getChannels() {
		return channels;
	}
	public void setChannels(TreeMap<Integer, OneMydetvChannel> channels) {
		this.channels = channels;
	}
	public String getTmpPath() {
		return tmpPath;
	}
	public void setTmpPath(String tmpPath) {
		this.tmpPath = tmpPath;
	}
	public HashMap<Integer, VideoCategoryMain> getVideoCategoriesMain() {
		return videoCategoriesMain;
	}
	public void setVideoCategoriesMain(HashMap<Integer, VideoCategoryMain> videoCategoriesMain) {
		this.videoCategoriesMain = videoCategoriesMain;
	}
	public HashMap<Integer, OneMbp> getMbps() {
		return mbps;
	}
	public void setMbps(HashMap<Integer, OneMbp> mbps) {
		this.mbps = mbps;
	}
	public TreeMap<Integer, VideoType> getVideoTypes() {
		return videoTypes;
	}
	public void setVideoTypes(TreeMap<Integer, VideoType> videoTypes) {
		this.videoTypes = videoTypes;
	}
	public OneMydetvChannel getCurrentMydetvChannel() {
		return currentMydetvChannel;
	}
	public void setCurrentMydetvChannel(OneMydetvChannel currentMydetvChannel) {
		this.currentMydetvChannel = currentMydetvChannel;
	}
	public Integer getFallback() {
		return fallback;
	}
	public void setFallback(Integer fallback) {
		this.fallback = fallback;
	}
	
}
