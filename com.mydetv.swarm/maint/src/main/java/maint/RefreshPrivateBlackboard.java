package maint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.db.DbChannels;
import shared.db.DbMbp;
import shared.db.DbSchedule;
import shared.db.DbVideo;
import shared.db.DbVideoCategorySub;
import shared.db.DbVideoCategoryMain;
import shared.db.DbVideoConverted;

public class RefreshPrivateBlackboard {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(RefreshPrivateBlackboard.class);
	@Inject private DbChannels dbChannels;
	@Inject private DbVideoCategorySub dbSubcat;
	@Inject private DbVideoCategoryMain dbMaincat;
	@Inject private DbVideo dbVideo;
	@Inject private DbVideoConverted dbVideoConverted;
	@Inject private DbSchedule dbSchedule;
	@Inject private DbMbp dbMbp;
	
	public void refreshBlackboard(PrivateBlackboard bb) {
		synchronized(bb.getMaintLock()) {
			refreshBlackboardSync(bb);
		}
	}
	public void refreshBlackboardSync(PrivateBlackboard bb) {
		
		bb.setVideoCategoriesMain(dbMaincat.getMainCategories());
		bb.setVideoCategoriesSub(dbSubcat.getCategories());
		bb.setVideos(dbVideo.getVideos());
		bb.setVideosConverted(dbVideoConverted.getVideosConvertedAll());
		bb.setMbps(dbMbp.get());
		bb.setChannels(dbChannels.getChannels());
		bb.setVideoTypes(dbSchedule.getVideoTypes());
		//LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
		//for(OneMydetvChannel channel : bb.getChannels().values()) {
			//TreeMap<Integer, OneSchedule> schedules = dbSchedule.getSchedulesAll(channel.getChannelId());
			//TreeMap<Integer, OneSchedule> trimSchedules = new TreeMap<Integer, OneSchedule>();
			//for(OneSchedule os : schedules.values())
			//	if(os.getDt().isAfter(yesterday))
			//		trimSchedules.put(os.getIdschedule(), os);
			//channel.setSchedules(trimSchedules);
			//channel.setChannelSchedules(dbChannelSchedule.getAll(channel.getChannelId()));
		//}
		
	}
	
}
