package maint;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import shared.data.Config;

public class GeoIp implements Job {

	@Inject private Config config;
	private static final Logger log = LoggerFactory.getLogger(GeoIp.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		getNewDatabases();
		
	}

	/*
	 https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz
	 https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz
	 https://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz
	 
	 This product includes GeoLite2 data created by MaxMind, available from
<a href="https://www.maxmind.com">https://www.maxmind.com</a>.

The GeoLite2 Country and City databases are updated on the first Tuesday of each month. 
The GeoLite2 ASN database is updated every Tuesday.
	 */
	public void getNewDatabases() {
		getNewDatabase("https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz");
		getNewDatabase("https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz");
		getNewDatabase("https://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz");
	}
	public void getNewDatabase(String strUrl) {
		
		String fpnTar;

	    Path fpath;
		try {
			fpnTar = config.getConfigDir()+"/geoip/geoid.tar.gz";
			fpath = Paths.get(fpnTar);
			URL url = new URL(strUrl);
			InputStream in = url.openStream();
			Files.copy(in, fpath, StandardCopyOption.REPLACE_EXISTING);
			in.close();

			File infile = new File(fpnTar);
			File outdir = new File(config.getConfigDir()+"/geoip/");
			try {
				uncompressTarGZ(infile, outdir);
			} catch (IOException e) {
				log.error("Exception unzipping "+e);
			}           
			in.close();
			
			
		} catch (IOException e) {
			log.error("Exception getting new databases "+e);
		}
		
	}
	
	
	private static void uncompressTarGZ(File tarFile, File dest) throws IOException {
	    dest.mkdir();
	    TarArchiveInputStream tarIn = null;

	    tarIn = new TarArchiveInputStream(
	                new GzipCompressorInputStream(
	                    new BufferedInputStream(
	                        new FileInputStream(
	                            tarFile
	                        )
	                    )
	                )
	            );

	    TarArchiveEntry tarEntry = tarIn.getNextTarEntry();
	    // tarIn is a TarArchiveInputStream
	    while (tarEntry != null) {// create a file with the same name as the tarEntry
	        File destPath = new File(dest, tarEntry.getName());
	        System.out.println("working: " + destPath.getCanonicalPath());
	        if (tarEntry.isDirectory()) {
	            destPath.mkdirs();
	        } else {
	            destPath.createNewFile();
	            //byte [] btoRead = new byte[(int)tarEntry.getSize()];
	            byte [] btoRead = new byte[1024];
	            //FileInputStream fin 
	            //  = new FileInputStream(destPath.getCanonicalPath());
	            BufferedOutputStream bout = 
	                new BufferedOutputStream(new FileOutputStream(destPath));
	            int len = 0;

	            while((len = tarIn.read(btoRead)) != -1)
	            {
	                bout.write(btoRead,0,len);
	            }

	            bout.close();
	            btoRead = null;

	        }
	        tarEntry = tarIn.getNextTarEntry();
	    }
	    tarIn.close();
	} 
}
