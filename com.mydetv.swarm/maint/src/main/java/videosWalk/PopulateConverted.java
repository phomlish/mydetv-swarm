package videosWalk;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.Normalizer;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Blackboard;
import maint.PrivateBlackboard;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;
import shared.db.DbCommand;
import shared.db.DbLog;
import shared.helpers.FixPermissions;

public class PopulateConverted {
	
	@Inject private Config config;
	@Inject private Blackboard blackboard;
	@Inject private DbCommand dbCommand;
	@Inject private DbLog dbLog;
	private static final Logger log = LoggerFactory.getLogger(PopulateConverted.class);
	
	private ArrayList<String> trackedSuffixes = new ArrayList<String>();
	private ArrayList<String> ignoredSuffix = new ArrayList<String>();
	private ArrayList<String> ignoredFiles = new ArrayList<String>();
	private PrivateBlackboard bb = new PrivateBlackboard();
	Integer countSeen = 0;
	Integer countNew = 0;
	boolean changed;
	
	public boolean populateConvertedVideos(PrivateBlackboard bb) {

		log.info("**** populateConverted");
		changed=false;
		
		this.bb=bb;
		populateTrackedSuffixes();
		ignoredSuffix = new ArrayList<String>();

		for(VideoCategoryMain vcm : bb.getVideoCategoriesMain().values())
			populateMaincat(vcm);
		
		log.info("found "+countSeen);
		log.info("ignored suffixes:"+ignoredSuffix.size());
		for(String suffix : ignoredSuffix) 
			log.info("    "+suffix);
		log.info("ignored files:"+ignoredFiles.size());
		for(String file : ignoredFiles) {
			String suffix = getSuffix(file);
			if(suffix.equals("log"))
				continue;
			log.info("    "+file);
		}
		int cntNotFound=0;
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			if(ovc.getDtInactive()!=null)
				continue;
			if(ovc.getFailedReason()!=null)
				continue;
			if(! ovc.getFound()) {
				log.error("    missing :"+ovc.getVideoConvertedId()+" "+ovc.getPkey()+" "+ovc.getFn());
				String dbc="update videoConverted set dtInactive=now() where videoConvertedId="+ovc.getVideoConvertedId();
				dbCommand.doCommand(dbc);
				cntNotFound++;
			}
		}
		log.info("not found "+cntNotFound);
		log.info("**** populateConverted done");
		return changed;
	}
	
	private void populateMaincat(VideoCategoryMain vcm) {		
		walkSubcatDirectory(vcm);
		log.info("populateConverted subcat done "+vcm.getName());
	}
	
	private int subcatPathNameCount;
	private int thisCount;
	private int thisMatchedCount;
	private void walkSubcatDirectory(VideoCategoryMain vcm) {

		Path maincatPath = Paths.get(config.getVideosConvertedDirectory()+"/"+vcm.getName());
		subcatPathNameCount = maincatPath.getNameCount();
		log.info("walking converted "+maincatPath.toString());
		thisCount=0;
		thisMatchedCount=0;
		try {
			Files.walkFileTree(maincatPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					dealFile(vcm, file, attrs);
					thisCount++;
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					log.error("visitFileFailed:");
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					//log.info("postVisitDirectory:"+dir.getFileName());
					return FileVisitResult.CONTINUE;
				}
			});
			log.info("thisCount "+thisCount);
			log.info("thisMatchedCount "+thisMatchedCount);
		} catch (IOException e) {
			log.error("Exception walking "+e);
		}
	}
	Integer cntDealFile=0;
	private void dealFile(VideoCategoryMain vcm, Path path, BasicFileAttributes attrs) {
		//log.info("dealing "+path.getParent().getNameCount()+" "+path.toString());
		countSeen++;

		String filename = path.getFileName().toString();
		filename = Normalizer.normalize(filename, Normalizer.Form.NFD);
		String suffix = getSuffix(filename);
		if(filename.startsWith(".")) {
			//log.info("ignoring dot file: "+filename);
		}
		else if(! isTrackedSuffix(suffix)) {			
			//log.info("ignored "+filename+" "+suffix);
			ignoredFiles.add(path.toString());
		}

		else {
			String sd = "";
			for(int i=subcatPathNameCount; i<path.getNameCount()-1; i++) {
				if(sd.length() != 0)
					sd+="/";
				sd+=Normalizer.normalize(path.getName(i).toString(), Normalizer.Form.NFD);
			}
			String fn = path.getFileName().toString();
			fn = Normalizer.normalize(fn, Normalizer.Form.NFD);
			log.debug("cntDealFile:"+cntDealFile+" "+fn);
			cntDealFile++;
			OneVideoConverted ovc=null;
			synchronized(blackboard.getMaintLock()) {
				ovc = findConvertedVideoInDatabase(vcm, sd, fn);
			}
			if(ovc==null) {
				log.info("not found "+"/"+sd+"/"+fn+" "+path.toString());
				dbLog.info("not found "+"/"+sd+"/"+fn+" "+path.toString());
				File file = new File(path.toString());
				file.delete();
			}
			else {
				//log.info("found:"+sd+"/"+fn+" "+ov.log());
			}
		}		
	}
		
	/**
	 * from the path make sure we have this in the bb.videosConverted<br>
	 * @param vcm
	 * @param sd
	 * @param fn
	 * @return
	 */
	private OneVideoConverted findConvertedVideoInDatabase(VideoCategoryMain vcm, String sd, String fn) {
		String pn = config.getVideosConvertedDirectory()+"/"+vcm.getName()+"/"+sd+"/"+fn;

		//if(fn.contains("KristenWilliams"))  {
		//	log.info("findConvertedVideo "+pn);
		//	foundTarget=true;
		//}
		ArrayList<OneVideoConverted> foundConvertedVideos = new ArrayList<OneVideoConverted>();;

		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {			
			
			//if(foundTarget && ovc.getFn().contains("KristenWilliams"))
			//	log.info(ovc.getFn());

			if(ovc.getDtInactive()!=null)
				continue;
			if(ovc.getFailedReason()!=null)
				continue;			
			if(! ovc.getFn().equals(fn)) 
				continue;
			
			OneVideo ov = bb.getVideos().get(ovc.getPkey());
			if(ov.getDtInactive()!=null)
				continue;
			// don't do this, maybe we want to use them in the future
			//if(ov.isDoNotUse())
			//	continue;
			if(! ov.getSubdir().equals(sd))
				continue;
			
			if(! ov.getFn().equals(ovc.getOfn()))
				continue;
			
			VideoCategorySub vcs = bb.getVideoCategoriesSub().get(ov.getCatSub());
			VideoCategoryMain tvcm = bb.getVideoCategoriesMain().get(vcs.getCatMain());
			if(! tvcm.equals(vcm)) 
				continue;
			
			foundConvertedVideos.add(ovc);
			//log.info("foundConvertedVideos:"+foundConvertedVideos.size());
		}
				
		if(foundConvertedVideos.size()==0) {
			log.info("could not find "+pn);
			return null;
		}
		else if(foundConvertedVideos.size()==1) {
		
			OneVideoConverted ovc = foundConvertedVideos.get(0);	
			OneVideo ov = bb.getVideos().get(ovc.getPkey());
			String nfn = ov.getFn()+"."+ovc.getConvertedType()+".webm";
			String nfpn = config.getVideosConvertedDirectory()+"/"+vcm.getName()+"/"+ov.getSubdir()+"/"+nfn;
			
			//log.info("nfn:"+nfn+","+nfpn);
			if(! nfn.equals(fn)) {
			
				log.error("\n"+ov.log()+"\n"+pn+"\n"+nfpn);
				File oldfile = new File(pn);
				File newfile = new File(nfpn);
				if(newfile.exists()) {
					log.error("huh");
					oldfile.delete();
					return null;
				}
				oldfile.renameTo(newfile);
				if(! newfile.exists()) {
					log.error("double huh");
				}		
			
				String dbc = "update videoConverted set fn=\""+nfn+"\" where videoConvertedId="+ovc.getVideoConvertedId();
				dbCommand.doCommand(dbc);
				changed=true;
			}
			FixPermissions.FixFilePermissions(nfpn);
			if(ovc.getFound()) {
				log.info("huh");
			}
			ovc.setFound(true);
			return ovc;
		}
		else {
			log.info("huh");
		}

		String logdups="";
		for(OneVideoConverted ovc : foundConvertedVideos) 
			logdups+="\n"+ovc.log();
		log.error("found too many matching converted videos for "+pn+logdups);
		return null;
	}
	
	private void populateTrackedSuffixes() {
		trackedSuffixes.add("webm");
	}
	
	private boolean isTrackedSuffix(String suffix) {
		//log.info("huh "+fn);
		if(suffix == null)
			return false;
		for(Integer i=0; i<trackedSuffixes.size(); i++) {
			//log.info("comparing to "+trackedSuffixes.get(i));
			if(trackedSuffixes.get(i).compareToIgnoreCase(suffix)==0)
				return true;				
		}
		if(! ignoredSuffix.contains(suffix))
			ignoredSuffix.add(suffix);
				
		return false;
	}
	
	private String getSuffix(String fn) {
		String suffix = "";
		String[] pieces = fn.split("\\.");
		if(pieces.length<2) 
			return null;
		suffix = pieces[pieces.length-1];
		return suffix;
	}
	
	public ArrayList<String> getIgnoredSuffix() {
		return ignoredSuffix;
	}

	public void setIgnoredSuffix(ArrayList<String> ignoredSuffix) {
		this.ignoredSuffix = ignoredSuffix;
	}

	public ArrayList<String> getIgnoredFiles() {
		return ignoredFiles;
	}

	public void setIgnoredFiles(ArrayList<String> ignoredFiles) {
		this.ignoredFiles = ignoredFiles;
	}

}
