package videosWalk;

import java.io.File;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.PrivateBlackboard;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.data.OneVideoConverted;
import shared.db.DbCommand;
import shared.db.DbLog;
import shared.helpers.FilenameHelpers;
import videosConvert.VideosHelpers;

public class Verify {

	@Inject private Config config;
	@Inject private DbLog dbLog;
	@Inject private DbCommand dbCommand;
	@Inject private VideosHelpers helpers;
	private static final Logger log = LoggerFactory.getLogger(Verify.class);
	
	public void checkDbVideosExistOnFilesystem(PrivateBlackboard bb) {
		
		log.info("*** checkDbVideosExistOnFilesystem "+bb.getVideos().size()+" videos from the database exist on filesystem");
		for(OneVideo ov : bb.getVideos().values()) {
			if(ov.getDtInactive()!=null)
				continue;
			VideoCategorySub ovc = bb.getVideoCategoriesSub().get(ov.getCatSub());
			VideoCategoryMain vcm = bb.getVideoCategoriesMain().get(ovc.getCatMain());
			
			String fpn = config.getVideosDirectory()+"/"+vcm.getName()+"/"+ov.getSubdir()+"/"+ov.getFn();
			File file = new File(fpn);
			if(!file.exists()) {
				log.error("missing "+fpn+" "+ov.log());
				String cmd = "UPDATE videos set active=0 where pkey="+ov.getPkey();
				log.info("cmd:"+cmd);
				dbCommand.doCommand(cmd);
				dbLog.info("missing set inactive: "+ov.log());
			}
		}
		log.info("**** checkDbVideosExistOnFilesystem done");
	}
	
	public void checkDbVideoConvertedExistOnFilesystem(PrivateBlackboard bb) {
		
		log.info("**** checkDbVideoConvertedExistOnFilesystem "+bb.getVideos().size()+" videos from the database exist on filesystem");
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			if(ovc.getDtInactive()!=null)
				continue;
			if(ovc.getFailedReason()!=null)
				continue;
			if(!bb.getVideos().containsKey(ovc.getPkey())) {
				log.error("Meldown can/t find videoConverted in videos "+ovc.getVideoConvertedId());
				continue;
			}
			OneVideo ov = bb.getVideos().get(ovc.getPkey());
			String fpn = config.getVideosConvertedDirectory()+"/"+helpers.getCatMainDir(bb, ov)+"/"+ov.getSubdir()+"/"+ovc.getFn();
			File file = new File(fpn);
			
			// TEMPORARY!!!
			
			if(!file.exists()) {
				String ofn1 = FilenameHelpers.StripSuffix(ov.getFn())+"."+ovc.getConvertedType()+".webm";
				String ofn2 = ov.getFn()+"."+ovc.getConvertedType()+".webm";
				String nfpn = config.getVideosConvertedDirectory()+"/"+helpers.getCatMainDir(bb, ov)+"/"+ov.getSubdir()+"/"+ofn1;
				log.error("missing\n"+fpn+"\n"+nfpn);
				File nf = new File(nfpn);
				if(nf.exists()) {
					log.info("yes1");
					String dbc = "UPDATE videoConverted set fn=\""+ofn1+"\" where videoConvertedId="+ovc.getVideoConvertedId();
					log.info("dbc:"+dbc);
					dbCommand.doCommand(dbc);
				}
				else {
					log.info("no1");
					nfpn = config.getVideosConvertedDirectory()+"/"+helpers.getCatMainDir(bb, ov)+"/"+ov.getSubdir()+"/"+ofn2;
					log.error("missing\n"+fpn+"\n"+nfpn);
					nf = new File(nfpn);
					if(nf.exists()) {
						log.info("yes2");
						String dbc = "UPDATE videoConverted set fn=\""+ofn2+"\" where videoConvertedId="+ovc.getVideoConvertedId();
						log.info("dbc:"+dbc);
						dbCommand.doCommand(dbc);
					}
					else {
						log.info("no2");
						log.error("missing \n"+fpn+"\n"+ovc.getVideoConvertedId()+" "+ov.log());
						String cmd = "UPDATE videoConverted set dtInactive=now() where videoConvertedId="+ovc.getVideoConvertedId();
						log.info("cmd:"+cmd);
						dbCommand.doCommand(cmd);
						dbLog.info("missing set inactive: "+ov.log());
					}
					
				}
				log.info("");
			}
			
			/*
			if(!file.exists()) {
				log.error("missing \n"+fpn+"\n"+ovc.getVideoConvertedId()+" "+ov.log());
				String cmd = "UPDATE videosConverted set dtActive=now() where videoConvertedId="+ovc.getVideoConvertedId();
				log.info("cmd:"+cmd);
				dbCommand.doCommand(cmd);
				dbLog.info("missing set inactive: "+ov.log());
			}
			*/
			
		}
		log.info("**** checkDbVideoConvertedExistOnFilesystem done");
	}
	
	public void checkForVideoDups(PrivateBlackboard bb) {
		log.info("**** check db for dups");
		//for(OneVideo ov : bb.getVideos().values()) 
		//	ov.setFound(false);
		
		HashMap<Integer, OneVideo> videos = new HashMap<Integer, OneVideo>();
		videos.putAll(bb.getVideos());
		for(OneVideo ov : videos.values()) {
			TreeMap<Integer, OneVideo> instances = findVideoInstances(bb, ov);
			
			if(instances.size()>1) {
				log.error("too many videos "+instances.size()+" for "+ov.log());
				OneVideo oldest = null;
				for(OneVideo tov : instances.values()) {
					log.info("    looking at "+tov.log());
					if(oldest==null)
						oldest=tov;
					else if(tov.getDtAdded().isBefore(oldest.getDtAdded()))
						oldest=tov;
				}
				log.info("        keeping "+oldest.log());
				for(OneVideo tov : instances.values()) {
					if(tov==oldest)
						continue;
					log.info("        removing "+tov.log());
					bb.getVideos().remove(tov.getPkey());
					//String cmd = "UPDATE videos set active=0 where pkey="+tov.getPkey();
					dbLog.info("videoDup "+ov.log());
					//dbCommand.doCommand(cmd);
				}				
			}
		}
		
		log.info("**** checkForDups done");
	}

	private TreeMap<Integer, OneVideo> findVideoInstances(PrivateBlackboard bb, OneVideo ov) {
		TreeMap<Integer, OneVideo> instances = new TreeMap<Integer, OneVideo>();
		
		for(OneVideo tov : bb.getVideos().values()) {
			if(!bb.getVideoCategoriesSub().containsKey(tov.getCatSub())) {
				log.error("tov that cat des not exist");
				continue;
			}
			if(!bb.getVideoCategoriesSub().containsKey(ov.getCatSub())) {
				log.error("ov that cat does not exist");
				continue;
			}
			Integer tovMain = bb.getVideoCategoriesSub().get(tov.getCatSub()).getCatMain();
			Integer ovMain = bb.getVideoCategoriesSub().get(ov.getCatSub()).getCatMain();
			if(!tovMain.equals(ovMain))
				continue;
			if(! tov.getSubdir().equals(ov.getSubdir()))
				continue;
			if(! tov.getFn().equals(ov.getFn()))
				continue;
			instances.put(tov.getPkey(), tov);			
		}
		return instances;
	}
	
	
	public void checkDbConvertedHaveDbVideo(PrivateBlackboard bb) {
		log.info("**** checkConvertedHaveVideo");
		
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			//log.info("ovc:"+ovc.getPkey());
			if(ovc.getDtInactive()!=null)
				continue;
			if(!bb.getVideos().containsKey(ovc.getPkey())) {
				log.error("Can't find converted video for "+ovc.getPkey()+" "+ovc.getVideoConvertedId());
				String cmd= "update videoConverted set dtInactive=now() where videoConvertedId="+ovc.getVideoConvertedId();
				//cmd= "delete from videoConverted where videoConvertedId="+ovc.getVideoConvertedId();
				log.info("cmd:"+cmd);
				dbCommand.doCommand(cmd);
				dbLog.info("missing converted "+cmd);
				continue;
			}
		}
		log.info("**** done checkConvertedHaveVideo");		
	}
	
	public void checkConvertedDups(PrivateBlackboard bb) {
		log.info("**** checkConvertedDups");
		for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
			if(ovc.getDtInactive()!=null || ovc.getFailedReason()!=null) 
				continue;

			TreeMap<Integer, OneVideoConverted> instances = findConvertedInstances(bb, ovc);
			if(instances.size()>1) {
				log.error("too many converted instances:");
				OneVideoConverted oldest = null;
				for(OneVideoConverted tovc : instances.values()) {
					if(oldest==null)
						oldest=tovc;
					else if(oldest.getDtConverted().isAfter(tovc.getDtConverted()))
						oldest=tovc;
				}
				for(OneVideoConverted tovc : instances.values()) {
					if(tovc!=oldest) {
						log.info("removing "+tovc.getPkey()+" "+tovc.getVideoConvertedId()+" "+tovc.getFn());
						tovc.setDtInactive(LocalDateTime.now());
						String cmd= "update videoConverted set dtInactive=now() where videoConvertedId="+tovc.getVideoConvertedId();
						//cmd= "delete from videoConverted where videoConvertedId="+tovc.getVideoConvertedId();
						
						log.info("cmd:"+cmd);
						dbCommand.doCommand(cmd);
						dbLog.info("dup converted "+cmd);
					}
				}
				//dbLog.info("too many instances, never implemented the fix "+ovc.getVideoConvertedId());
			}				
		}			
		log.info("**** done checkConvertedDups");		
	}
	
	private TreeMap<Integer, OneVideoConverted> findConvertedInstances(PrivateBlackboard bb, OneVideoConverted ovc) {
		
		TreeMap<Integer, OneVideoConverted> instances = new TreeMap<Integer, OneVideoConverted>();
		for(OneVideoConverted tovc : bb.getVideosConverted().values()) {
			if(tovc.getDtInactive()!=null || tovc.getFailedReason()!=null) 
				continue;
			if(! tovc.getPkey().equals(ovc.getPkey()))
				continue;
			if(! ovc.getConvertedType().equals(tovc.getConvertedType()))
				continue;
			instances.put(tovc.getVideoConvertedId(), tovc);
		}
		
		return instances;
	}
	
	
	
	
}
