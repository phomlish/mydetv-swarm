package videosWalk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Helpers;
import maint.PrivateBlackboard;
import maint.RefreshPrivateBlackboard;
import maint.UpdateBroadcast;

public class VideosWalk implements Runnable {

	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private PopulateVideos populateVideos;
	@Inject private PopulateConverted populateConvered;
	@Inject private Helpers helpers;
	@Inject private Verify verify;
	@Inject private UpdateBroadcast updateBroadcast;
	private static final Logger log = LoggerFactory.getLogger(VideosWalk.class);
	
	private PrivateBlackboard bb = new PrivateBlackboard();
	
	@Override
	public void run() {
		log.info("starting the videos walk thread");
		
		while(true) {
			boolean changed = false;
			boolean tchanged;
			
			refreshBlackboard.refreshBlackboard(bb);
			
			// process videos
			verify.checkForVideoDups(bb);					
			verify.checkDbVideosExistOnFilesystem(bb);
			tchanged = populateVideos.populateVideos(bb);
			if(!tchanged)
				changed=tchanged;
			
			// process converted
			verify.checkConvertedDups(bb);					
			verify.checkDbConvertedHaveDbVideo(bb);
			verify.checkDbVideoConvertedExistOnFilesystem(bb);
			// this is dangerous but does cleanup the directory.  
			// it happens where we set things inactive or donotuse with the web interface 
			// recommended to only run manually and brake where it deletes the file
			tchanged = populateConvered.populateConvertedVideos(bb);
			if(!tchanged)
				changed=tchanged;
			
			log.info("done one videosWalk loop");
			if(changed) 
				updateBroadcast.updateBroadcastBlackboard();
				
			else try {
				long nextHour = helpers.getMillisTillNextHour();
				Thread.sleep(nextHour);
				//Thread.sleep(1*60*1000);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}
		}		
	}
}
