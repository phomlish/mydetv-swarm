package videosWalk;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.PrivateBlackboard;
import shared.data.Config;
import shared.data.OneVideo;
import shared.data.VideoCategoryMain;
import shared.data.VideoCategorySub;
import shared.db.DbLog;
import shared.db.DbVideo;
import videosConvert.VideosHelpers;

public class PopulateVideos {
	
	@Inject private Config config;
	@Inject private VideosHelpers helpers;
	@Inject private DbVideo dbVideo;
	@Inject private DbLog dbLog;
	private static final Logger log = LoggerFactory.getLogger(PopulateVideos.class);
	
	private ArrayList<String> trackedSuffixes = new ArrayList<String>();
	private ArrayList<String> ignoredSuffix = new ArrayList<String>();
	private ArrayList<String> ignoredFiles = new ArrayList<String>();
	private PrivateBlackboard bb = new PrivateBlackboard();
	Integer countSeen = 0;
	Integer countNew = 0;
	boolean changed;
	
	public boolean populateVideos(PrivateBlackboard bb) {

		log.info("**** populateVideos");
		changed=false;
		
		this.bb=bb;
		populateTrackedSuffixes();
		ignoredSuffix = new ArrayList<String>();

		for(VideoCategoryMain vcm : bb.getVideoCategoriesMain().values()) {
			if(vcm.getName().equals("archive.org"))
				continue;
			if(vcm.getName().equals("mbp"))
				continue;
			populateMaincat(vcm);
		}
		
		log.info("found "+countSeen);
		log.info("ignored suffixes:"+ignoredSuffix.size());
		for(String suffix : ignoredSuffix) 
			log.info("    "+suffix);
		log.info("ignored files:"+ignoredFiles.size());
		// show all
		//for(String file : ignoredFiles)
		//	log.info("    "+file);
		// show some
		for(String file : ignoredFiles)
			if(! isIgnoredSuffix(file))
				log.info("    "+file);
		
		int cntNotFound=0;
		for(OneVideo ov : bb.getVideos().values()) {
			
			VideoCategorySub vcs = bb.getVideoCategoriesSub().get(ov.getCatSub());
			VideoCategoryMain vcm = bb.getVideoCategoriesMain().get(vcs.getCatMain());
			if(vcm.getName().equals("archive.org"))
				continue;
			if(vcm.getName().equals("mbp"))
				continue;
			
			if(! ov.isFound()) {
				log.error("found in db but not on fs :"
					+config.getVideosDirectory()+"/"+helpers.getCatMainDir(bb, ov)+"/"+ov.getFn()
					+ov.log());
				cntNotFound++;
			}
		}
		log.info("not found "+cntNotFound);
		log.info("**** populateVideos done");;
		return changed;
	}
	
	private void populateMaincat(VideoCategoryMain vcm) {		
		walkMaincatDirectory(vcm);
		log.info("populate maincat done "+vcm.getName());
	}
	
	private int subcatPathNameCount;
	private int thisCount;
	private int thisMatchedCount;
	private void walkMaincatDirectory(VideoCategoryMain vcm) {
		Path maincatPath = Paths.get(config.getVideosDirectory()+"/"+vcm.getName());
		subcatPathNameCount = maincatPath.getNameCount();
		log.info("walking maincat "+maincatPath.toString());
		thisCount=0;
		thisMatchedCount=0;
		try {
			Files.walkFileTree(maincatPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					dealFile(vcm, file, attrs);
					thisCount++;
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					log.error("visitFileFailed:");
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					//log.info("postVisitDirectory:"+dir.getFileName());
					return FileVisitResult.CONTINUE;
				}
			});
			log.info("done "+maincatPath.toString()+" cnt:"+thisCount+"matched: "+thisMatchedCount);
		} catch (IOException e) {
			log.error("Exception walking "+e);
		}
	}
	
	private void dealFile(VideoCategoryMain vcm, Path path, BasicFileAttributes attrs) {
		//log.info("dealing "+path.getParent().getNameCount()+" "+path.toString());
		countSeen++;
		String filename = path.getFileName().toString();
		filename = Normalizer.normalize(filename, Normalizer.Form.NFD);
		String suffix = getSuffix(filename);
		if(filename.startsWith(".")) {
			//log.info("ignoring dot file: "+filename);
		}
		else if(! isTrackedSuffix(suffix)) {			
			//log.info("ignored "+filename+" "+suffix);
			ignoredFiles.add(path.toString());
		}

		else {
			String sd = "";
			for(int i=subcatPathNameCount; i<path.getNameCount()-1; i++) {
				if(sd.length() != 0)
					sd+="/";
				sd+=Normalizer.normalize(path.getName(i).toString(), Normalizer.Form.NFD);
			}
			String fn = path.getFileName().toString();
			fn = Normalizer.normalize(fn, Normalizer.Form.NFD);

			OneVideo ov = findVideo(sd, fn);
			if(ov==null) {
				log.info("NEW: sd:"+sd+", fn:"+fn);
				
				ov = new OneVideo();
				ov.setFn(fn);
				ov.setSubdir(sd);
				ov.setCatSub(getVideoCategorySubUncategorized(vcm).getIdvideo_category());
				ov.setFilesize(attrs.size());
				ov.setDtAdded(LocalDateTime.now());
				ov.setLength(helpers.getLength(path.toString()));
				helpers.setVideoTitle(ov, path);
				log.info("    "+ov.getFn()+",size:"+attrs.size());
				
				Integer pkey = dbVideo.insertVideo(ov);
				if(pkey<1) {
					log.error("error inserting "+ov.getFn());
				}
				
				changed=true;
				dbLog.info("added new: "+ov.log());
				countNew++;
			}
			else {
				ov.setFound(true);
				thisMatchedCount++;
			}
		}		
	}
	
	private VideoCategorySub getVideoCategorySubUncategorized(VideoCategoryMain vcm) {
		for(VideoCategorySub vcs : bb.getVideoCategoriesSub().values()) {
			if(vcs.getCatMain().equals(vcm.getIdVCM()) && vcs.getName().equals("uncategorized"))
				return vcs;
		}		
		return null;
	}
		
	private Boolean isIgnoredSuffix(String fn) {
		String suffix = getSuffix(fn);
		for(String ignoredSuffix : ignoredSuffix) {
			if(ignoredSuffix.equals(suffix)) 
				return true;
		}
		return false;
	}
	
	// pretty inefficient but we don't have that many videos
	private OneVideo findVideo(String sd, String fn) {
		OneVideo foundVideo=null;
		for(OneVideo ov : bb.getVideos().values()) {
			if(ov.getSubdir().equals(sd) && ov.getFn().equals(fn)) {
				//log.info("found "+fn+" "+ov.getFn());	
				if(foundVideo!=null) {
					log.error("video found twice\n"+ov.log()+"\n"+foundVideo.log());
				}
				foundVideo=ov;
			}
		}
		return foundVideo;
	}
	
	private void populateTrackedSuffixes() {
		trackedSuffixes.add("mp4");
		trackedSuffixes.add("flv");
		trackedSuffixes.add("wmv");
		trackedSuffixes.add("mpg");
		trackedSuffixes.add("ogv");
		trackedSuffixes.add("mpeg");
		trackedSuffixes.add("mpeg4");
		trackedSuffixes.add("mov");
		trackedSuffixes.add("m4v");
		trackedSuffixes.add("avi");
		trackedSuffixes.add("webm");
		trackedSuffixes.add("ts");
	}
	
	private boolean isTrackedSuffix(String suffix) {
		//log.info("huh "+fn);
		if(suffix == null)
			return false;
		for(Integer i=0; i<trackedSuffixes.size(); i++) {
			//log.info("comparing to "+trackedSuffixes.get(i));
			if(trackedSuffixes.get(i).compareToIgnoreCase(suffix)==0)
				return true;				
		}
		if(! ignoredSuffix.contains(suffix))
			ignoredSuffix.add(suffix);
				
		return false;
	}
	
	private String getSuffix(String fn) {
		String suffix = "";
		String[] pieces = fn.split("\\.");
		if(pieces.length<2) 
			return null;
		suffix = pieces[pieces.length-1];
		return suffix;
	}

	public ArrayList<String> getIgnoredSuffix() {
		return ignoredSuffix;
	}

	public void setIgnoredSuffix(ArrayList<String> ignoredSuffix) {
		this.ignoredSuffix = ignoredSuffix;
	}

	public ArrayList<String> getIgnoredFiles() {
		return ignoredFiles;
	}

	public void setIgnoredFiles(ArrayList<String> ignoredFiles) {
		this.ignoredFiles = ignoredFiles;
	}

}
