package schedule;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Blackboard;
import maint.PrivateBlackboard;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoType;
import shared.db.DbSchedule;

public class SchedHelpers {

	private static final Logger log = LoggerFactory.getLogger(SchedHelpers.class);
	@Inject private Blackboard blackboard;
	@Inject private DbSchedule dbSchedule;
	
	PrivateBlackboard bb;
	HashMap<Integer, OneVideo> videos;
	
	/**
	 * 
	 * @param dtStart: the dt for when this we be scheduled
	 * @param diff: don't chose a video larger than this to ensure we hit our boundary
	 * @return
	 */
	public OneSchedule addSchedule(
			PrivateBlackboard bb, 
			HashMap<Integer, OneVideo> videos,
			LocalDateTime dtStart, 
			LocalDateTime dtEnd) {
		this.bb=bb;
		this.videos=videos;
		int diff = (int) dtStart.until(dtEnd, ChronoUnit.SECONDS);

		// **** boundaries **** 
		// on an hour boundary add our massive videos (a show)
		if(dtStart.getMinute()==0 
			&& dtStart.getSecond()==0 
			&& diff >= 3000)
			return addSchedule(bb.getVideoTypes().get(1), diff, dtStart);
		
		// on 1/2 hour boundary we can try to schedule a huge video
		if((dtStart.getMinute()==0 || dtStart.getMinute()==30) 
			&& dtStart.getSecond()==0 
			&& diff >= 1500)
			return addSchedule(bb.getVideoTypes().get(2), diff, dtStart);
		
		// if we are close to 1/2 hour boundary only schedule small videos
		LocalDateTime dt = adjustDtToBoundry(dtStart);
		if(! dt.equals(dtStart)) 
			return addSchedule(bb.getVideoTypes().get(5), diff, dtStart);
		
		
		// large
		if(diff>=480)
			return addSchedule(bb.getVideoTypes().get(3), diff, dtStart);
		// medium
		if(diff>120)
			return addSchedule(bb.getVideoTypes().get(4), diff, dtStart);
		// small
		return addSchedule(bb.getVideoTypes().get(5), diff, dtStart);
	}
	
	/**
	 * find and add a video to the schedule<br>
	 * @param type
	 * @param maxLength
	 * @param dtStart
	 * @return
	 */
	private OneSchedule addSchedule(VideoType vst, Integer maxLength, LocalDateTime dtStart) {
		log.info("adding a "+vst.getName()+" video for dtStart "+dtStart);
		bb.setFallback(0);
		OneVideo ov = getVideo(dtStart, vst, maxLength);
		if(ov==null)
			return null;
		
		OneSchedule os = new OneSchedule();
		os.setPkey(ov.getPkey());
		os.setDt(dtStart);
		os.setChannel(bb.getCurrentMydetvChannel().getChannelId());

		os = dbSchedule.insertSchedule(os);
		return os;
	}
	
	private OneVideo getVideo(LocalDateTime startDT, VideoType vst, Integer maxLength) {
		HashMap<Integer, Integer> eligible = new HashMap<Integer, Integer>();
		OneVideo rov = null;
		Integer cntEligible = 0;
		
		//Integer min = vst.getSizeMin();
		Integer max = vst.getSizeMax();
		Integer repeat;
		// for Zane we'll use the alt smaller gap
		if(bb.getCurrentMydetvChannel().getChannelId()==4)
			repeat = vst.getAltGap();
		else
			repeat=vst.getGap();
		if(max > maxLength) 
			max = maxLength;
		log.info("find a " + vst.getName() + " video with " + Integer.valueOf(vst.getSizeMin()/60) + " min and " + Integer.valueOf(vst.getSizeMax()/60) + " max duration, not played in " + repeat + " minutes");

		for(OneVideo ov : vst.getVideos()) {	
			// throttle certain videos.  don't put them in our list of eligible
			if(ov.getTitle()!=null && ov.getTitle().contains("Crest")) {  // 
				Integer r = blackboard.getSecureRandom().nextInt(3);
				if(r != 0) 
					continue;
			}
			if(ov.getTitle()!=null && ov.getTitle().contains("Continental")) {
				Integer r = blackboard.getSecureRandom().nextInt(1);
				if(r != 0) 
					continue;
			}
			if(ov.getTitle()!=null && ov.getTitle().contains("Cool Whip")) {
				Integer r = blackboard.getSecureRandom().nextInt(1);
				if(r != 0) 
					continue;
			}
			
			// skip things that are too large and will cause us to miss our boundary
			Integer length = (int) Math.floor(ov.getLength()+0.99999999);
			if(length>maxLength) 
				continue;
						
			// skip things that played too recently
			Integer playedGap;
			if(ov.getLastScheduled()==null)
				playedGap=Integer.MAX_VALUE;
			else 
				playedGap=(int) ChronoUnit.SECONDS.between(ov.getLastScheduled(), startDT);
			
			if(playedGap>repeat) {
				log.trace("    old enough");
				eligible.put(cntEligible, ov.getPkey());
				cntEligible++;
			}
		}
		if(cntEligible<1) {
			log.warn("none eligible for " + vst.getName() + ", falling back");
			return(fallback(startDT, vst, maxLength));
		}
		Integer selected = blackboard.getSecureRandom().nextInt(cntEligible);
		rov = videos.get(eligible.get(selected));
		int mins = (int) Math.floor(rov.getLength()/60);
		log.info("found " + cntEligible + " eleigible videos, chose " + rov.getPkey() + " with dur " + rov.getLength()+"("+mins+") "+rov.getTitle());
		return rov;
	}

	private OneVideo fallback(LocalDateTime startDT, VideoType vst, Integer maxDuration) {
		vst.incrementFallback();
		bb.setFallback(bb.getFallback()+1);
		log.info("***** falling back!!!");
		Integer smallerVst = vst.getId()+1;
		if(smallerVst>=bb.getVideoTypes().size()) {
			log.error("total meltdown, can't even find a small video for "+bb.getCurrentMydetvChannel().getName());
			return null;
		}
		return(getVideo(startDT, bb.getVideoTypes().get(smallerVst), maxDuration));
	}
		
	/**
	 * if we are close to an hour or 1/2 hour boundary return that boundary<br>
	 * the 24x7 sched player will add SID (station identification)<br>
	 * there is no such thing as a 'day' boundary since we support any timezone
	 */
	public LocalDateTime adjustDtToBoundry(LocalDateTime dt) {
		
		LocalDateTime dtTest;
		int minute;
		
		// check for an hour or 1/2 hour boundary
		minute = dt.getMinute();
		if(minute<30)
			dtTest = dt.withMinute(30).withSecond(0);
		else
			dtTest = dt.plusHours(1).withMinute(0).withSecond(0);
		
		int gapleft = (int) dt.until(dtTest, ChronoUnit.SECONDS);
		if(gapleft<60)
			return dtTest;
		
		return dt;
	}
	

}
