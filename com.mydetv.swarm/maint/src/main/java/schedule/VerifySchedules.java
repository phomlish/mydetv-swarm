package schedule;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.PrivateBlackboard;
import shared.data.OneChannelSchedule;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.OneVideoConverted;
import shared.db.DbCommand;
import shared.db.DbLog;

public class VerifySchedules {

	@Inject private DbLog dbLog;
	@Inject private DbCommand dbCommand;
	private static final Logger log = LoggerFactory.getLogger(VerifySchedules.class);

	PrivateBlackboard bb;
	// TAKE THIS OUT once all videos are converted to all blackboard types
	ArrayList<String> convertedTypes;
	
	// won't work as it used bb schedules
	boolean changed;
	public boolean checkSchedulesNeedRewrite(PrivateBlackboard bb) {
		this.bb=bb;
		convertedTypes = new ArrayList<String>();
		convertedTypes.add("vp8-opus");
		convertedTypes.add("vp8-vorbis");
		convertedTypes.add("vp9-opus");
				
		changed=false;
		for(OneMydetvChannel channel : bb.getChannels().values()) {
			checkChannel(channel);
		}
		return changed;
	}

	private void checkChannel(OneMydetvChannel channel) {
		log.info("**** checkChannel "+channel.getName());
		if(channel.getChannelType()!=1)
			return;
		
		OneSchedule prev=null;
		OneSchedule current=null;
		// check for gaps and overlaps
		long max=0;
		for(OneSchedule os : channel.getSchedules().values()) {
			prev=current;
			current=os;
			if(prev==null)
				continue;
			
			OneVideo ovPrev = bb.getVideos().get(prev.getPkey());
			log.debug("prev "+prev.getDt()+" "+ovPrev.getFn()+" "+ovPrev.getTitle());
			Long lengthNanos = (long) Math.floor(ovPrev.getLength()*1000000000);
			log.debug("length:"+ovPrev.getLength()+",nanos:"+lengthNanos);
			LocalDateTime prevEnd = prev.getDt().plusNanos(lengthNanos);
			log.debug("prevEnd:"+prevEnd);
			
			OneVideo ovCurrent = bb.getVideos().get(current.getPkey());
			log.debug("current "+current.getDt()+" "+ovCurrent.getFn()+" "+ovCurrent.getTitle());
			
			long diff = ChronoUnit.NANOS.between(prevEnd, current.getDt());
			long diff2 = diff/1000000;
			log.debug("diff:"+diff/1000000);
			if(diff2>max)
				max=diff2;
			if(diff2>10000) {
				//log.info("huge diff");
			}
			if(diff2<0) {
				String msg = "\n"+"prev "+prev.getDt()+" "+prev.getIdschedule()+" "+ovPrev.getFn()+" "+ovPrev.getTitle()
				+"\nlength:"+ovPrev.getLength()+",nanos:"+lengthNanos
				+"\nprevEnd:"+prevEnd
				+"\ncurrent "+current.getDt()+" "+current.getIdschedule()+" "+ovCurrent.getFn()+" "+ovCurrent.getTitle()
				+"\ndiff:"+diff/1000000;
				log.info(msg);;
				log.error("error");
			}
		}
		log.debug("max diff:"+max);
		
		// delete from channelSchedule if not converted
		for(OneChannelSchedule channelSchedule : channel.getChannelSchedules().values()) {
			//log.info("sched:"+channelSchedule.getPkey());
			OneVideo ov = bb.getVideos().get(channelSchedule.getPkey());		
			if(! isConverted(channelSchedule.getPkey())) {
				log.info("ov:"+ov.log());
				String lv = "not converted channelSchedule:"+channelSchedule.getIdchannelSchedule()+" "+channelSchedule.getPkey();
				String cmd = "delete from channelSchedule where idchannelSchedule="+channelSchedule.getIdchannelSchedule();
				log.info("lv:"+lv+",cmd:"+cmd);
				dbCommand.doCommand(cmd);
				dbLog.info(lv+" "+cmd);
				changed=true;
			}
		}
		
		// delete from schedule if not converted
		for(OneSchedule os : channel.getSchedules().values()) {
			if(! isConverted(os.getPkey())) {
				String lv = "not converted schedule:"+os.getPkey()+" "+os.getPkey();
				String cmd = "delete from schedule where idschedule="+os.getIdschedule();
				log.info("lv:"+lv+",cmd:"+cmd);
				dbCommand.doCommand(cmd);
				dbLog.info(lv+" "+cmd);
				changed=true;
			}
		}
		
		log.info("**** checkChannel done");
	}
	
	private boolean isConverted(Integer pkey) {
		
		if(!bb.getVideos().containsKey(pkey)) {
			//log.info("missing "+channelSchedule.getPkey());
			return false;
		}
		OneVideo ov = bb.getVideos().get(pkey);
		return isVideoConverted(ov);
	}
	
	private boolean isVideoConverted(OneVideo ov) {

		int need = convertedTypes.size();
		int cnt=0;
		for(String convertedType : convertedTypes) {
			for(OneVideoConverted ovc : bb.getVideosConverted().values()) {
				if(ovc.getPkey().equals(ov.getPkey()) && ovc.getConvertedType().equals(convertedType)) {
					cnt++;
					break;
				}
			}
		}
		if(cnt==need)
			return true;
		return false;
	}

}
