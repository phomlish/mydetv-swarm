package schedule;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import maint.Helpers;
import maint.PrivateBlackboard;
import maint.RefreshPrivateBlackboard;
import maint.UpdateBroadcast;
import shared.data.OneChannelSchedule;
import shared.data.OneMydetvChannel;
import shared.data.OneSchedule;
import shared.data.OneVideo;
import shared.data.VideoType;
import shared.db.DbChannelSchedule;
import shared.db.DbCommand;
import shared.db.DbConnection;
import shared.helpers.ScheduleHelpers;

public class ProcessSchedule implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ProcessSchedule.class);
	@Inject private UpdateBroadcast updateBroadcast;
	@Inject private RefreshPrivateBlackboard refreshBlackboard;
	@Inject private DbChannelSchedule dbChannelSchedule;
	@Inject private DbConnection dbConnection;
	@Inject private DbCommand dbCommand;
	@Inject private SchedHelpers schedHelpers;
	@Inject private Helpers helpers;
	
	PrivateBlackboard bb = new PrivateBlackboard();
	boolean changed;
	
	OneMydetvChannel channel;
	HashMap<Integer, OneChannelSchedule>  channelSchedules;
	TreeMap<String, OneSchedule> schedules;
	HashMap<Integer, OneVideo> videos;
	Integer cntSid;
	Connection conn;
	LocalDateTime now;
	LocalDateTime midnightToday;
	// we use 15 days so displaying our schedule is easier when we account for timezone of the user
	LocalDateTime plusFifteenDays;
	DateTimeFormatter treeDtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	
	@Override
	public void run() {
		log.info("starting the schedule thread");

		while(true) {
			changed=false;

			refreshBlackboard.refreshBlackboard(bb);
			
			//verifySchedules.checkSchedules(bb);
			
			for(OneMydetvChannel mydetvChannel : bb.getChannels().values()) {
				channel = mydetvChannel;
				
				if(! channel.getChannelType().equals(1))
					continue;
				for(VideoType vt : bb.getVideoTypes().values())
					vt.setFallback(0);
				try {					
					conn = dbConnection.getConnection();
					makeSchedule();
					log.info("All done making schedules for "+mydetvChannel.getName());
				} catch (SQLException e) {
					log.error("failed e:"+e);
				} catch (Exception e) {
					log.error("failed e:"+e);
				}
				dbConnection.tryClose(conn);
				log.info("fallbacks:");
				for(VideoType vt : bb.getVideoTypes().values()) {
					log.info(vt.getName()+" "+vt.getSizeMin()+":"+vt.getSizeMax()+"\t"+vt.getFallback());
				}
			}
			if(changed) 
				updateBroadcast.updateBroadcastBlackboard();
			
			else try {
				long nextHour = helpers.getMillisTillNextHour();
				log.info("done, sleeping");
				Thread.sleep(nextHour);
				//Thread.sleep(1*60*1000);
			} catch (InterruptedException e) {
				log.error("Thread interupted");
			}		
		}		
	}
	
	public void makeSchedule() throws SQLException {
		now = LocalDateTime.now();
		cntSid=0;
		midnightToday = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0));
		plusFifteenDays = midnightToday.plusDays(15);
		
		log.info("\n\nmaking schedules for "+channel.getChannelId()+" "+channel.getName()+"\n");
		log.info("now:            "+now);
		log.info("midnightToday:  "+midnightToday);
		log.info("plusFifteenDays:"+plusFifteenDays+"\n");
		bb.setCurrentMydetvChannel(channel);
		
		// get what this channel will schedule
		getChannelSchedules();
		getVideos();
		getSchedules();
		setLastScheduled();
		addVideosToVideoTypes();

		log.info("\n\nwe loaded our schedules and set videos dtLastScheduled\n"
			+"\nscheduling until "+plusFifteenDays+"\n\n");
		
		OneSchedule prev=null;
		//OneSchedule last = null;
		
		// start with now and fill in ANY gaps
		TreeMap<String, OneSchedule> fSchedules =new TreeMap<String, OneSchedule>(schedules);
		for(OneSchedule os : fSchedules.values()) {
			// we don't fill gaps before now
			if(os.getDtEnd().isBefore(now))
				continue;

			if(prev!=null)
				fillGap(prev, os);
			
			//last=prev;
			prev=os;
		}

		// do the rest until 15 days later
		fillGap(prev,null);
		
		log.info("added " + cntSid + " station identifications");
	}
	
	private void getChannelSchedules() {
		channelSchedules = dbChannelSchedule.getAll(channel.getChannelId());
		log.info("got "+channelSchedules.size()+" channelSchedules");
		
	}
	/**
	 * get videos this channel can use<br>
	 * also reset last scheduled for those videos
	 */
	private void getVideos() {
		videos = new HashMap<Integer, OneVideo>();
		for(OneChannelSchedule ocs : channelSchedules.values()) {
			if(! bb.getVideos().containsKey(ocs.getPkey())) {
				log.error("could not find video "+ocs.getPkey()+" for ocs "+ocs.getIdchannelSchedule());
				continue;
			}
			OneVideo ov = bb.getVideos().get(ocs.getPkey());
			videos.put(ov.getPkey(), ov);
			ov.setLastScheduled(null);
		}
		log.info("got "+videos.size()+" videos");
	}

	private void setLastScheduled() {
		int cnt=0;
		for(OneSchedule os : schedules.values()) {
			if(os.getDt().isAfter(now))
				break;
			if(!bb.getVideos().containsKey(os.getPkey())) {
				log.error("could not find video "+os.getPkey()+" for os "+os.getIdschedule());
				continue;
			}
			OneVideo ov = bb.getVideos().get(os.getPkey());
			ov.setLastScheduled(os.getDt());
			//log.info("added"+logSched(os));
			cnt++;
		}
		log.info("set "+cnt+" last scheduled");
	}
	private void getSchedules() throws SQLException {
		
		schedules = new TreeMap<String, OneSchedule>();
		String dbc = "SELECT * from schedule";
		dbc+=" where mydetvChannel="+channel.getChannelId();
		dbc +=" and dt>='"+midnightToday.minusDays(3)+"'";
		log.info("dbc:"+dbc);
		ResultSet rs = dbCommand.doCommandRS(conn, dbc);
		while(rs.next()) {
			OneSchedule os = new OneSchedule();
			os.setIdschedule(rs.getInt("idschedule"));
			os.setPkey(rs.getInt("pkey"));
			os.setChannel(channel.getChannelId());
			os.setDt(rs.getTimestamp("dt").toLocalDateTime());
			ScheduleHelpers.SetEndDt(os, bb.getVideos().get(os.getPkey()));
			//log.info("os"+logSched(os));
			schedules.put(os.getDt().format(treeDtf), os);
		}
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		for(OneSchedule os : schedules.values()) {
			OneVideo ov = videos.get(os.getPkey());
			int seconds = (int) Math.ceil(ov.getLength());
			log.info("os "+os.getIdschedule()+" "
			+os.getDt().format(dtf)+" "
			+os.getDtEnd().format(dtf)+"    "
			+getPrettyTime(seconds));
		}
		
		log.info("pulled "+schedules.size()+" schedules since 3 days ago");
	}
	
	public void addVideosToVideoTypes() {
		for(VideoType vst : bb.getVideoTypes().values())
			vst.setVideos(new ArrayList<OneVideo>());	

		for(OneVideo ov : videos.values()) {
			setVideoType(ov);
			ov.getVideoType().getVideos().add(ov);
		}
		
		log.info(channel.getName()+" has these potential videos:");
		for(VideoType vst : bb.getVideoTypes().values())
			log.info(vst.getId()+" "+vst.getName()
				+" ("+vst.getSizeMin()+" to "+vst.getSizeMax()+") "
				+" ("+getPrettyTime(vst.getSizeMin())+" to "+getPrettyTime(vst.getSizeMax())+") "
				+vst.getVideos().size());
	}
	
	private void setVideoType(OneVideo ov) {
		int length = (int) Math.floor(ov.getLength());
		Boolean found=false;
		for(VideoType vst : bb.getVideoTypes().values()) {
			if(length>=vst.getSizeMin() && length<vst.getSizeMax()) {
				found=true;
				ov.setVideoType(vst);
				break;
			}
		}
		if(!found)
			ov.setVideoType(bb.getVideoTypes().get(6));
	}
	
	private void fillGap(OneSchedule prev, OneSchedule next) {
		if(next!=null && next.getIdschedule()>=150350)
			log.info("here");
		LocalDateTime dtStart;  
		LocalDateTime dtEnd;
		String msg;
		// handle the case where we have no recent schedules to work with
		if(prev==null) {
			dtStart = now.withNano(0).plusSeconds(2);
			// if we have no next then fill in until plus 15 days
			if(next==null) {
				dtEnd = plusFifteenDays;
				msg="1 prev==null next==null";
			}
			// else fill in to next
			else {
				dtEnd = next.getDt();
				msg="2 prev==null next!=null";
			}
		}
		// if we have no next fill in to end of 2 weeks
		else if(next==null) {
			dtEnd = plusFifteenDays;
			if(prev.getDtEnd().plusMinutes(1).isAfter(plusFifteenDays)) {
				log.info("seems we have enough videos\nprev"+logSched(prev));
				return;
			}
			if(prev.getDtEnd().isBefore(now)) {
				dtStart = now.withNano(0).plusSeconds(2);
				msg="3 next==null, prev is before now";
			}
			else {
				dtStart = prev.getDtEnd();
				msg="4 next==null, prev is after now";
			}
				
		}
		// we have a prev and next, fill the gap
		else {
			dtStart = prev.getDtEnd();
			dtEnd = next.getDt();
			msg="5 next!=null, prev!=null";
		}		

		if(prev!=null) msg+="\nprev"+logSched(prev);
		if(next!=null) msg+="\nnext"+logSched(next);
		log.info(msg+"\n"+dtStart+" "+dtEnd);

		// if we are close to dtEnd, skip
		if(dtStart.until(dtEnd, ChronoUnit.SECONDS)<2) {
			log.info("tiny gap skip "+dtStart.until(dtEnd, ChronoUnit.SECONDS));
			return;
		}
		
		LocalDateTime dt;
		
		dt= schedHelpers.adjustDtToBoundry(dtStart);
		if(! dtStart.equals(dt)) {
			log.info("adj dt of dtStart:"+dt);
			log.info("close to boundary, skip forward");
			dtStart=dt;
		}
	
		dt= schedHelpers.adjustDtToBoundry(dtEnd);
		if(! dtEnd.equals(dt)) {
			log.info("adj dt of dtEnd:"+dt);
			log.info("close to boundary, skip forward");
			dtEnd=dt;
		}
		
		if(dtStart.isEqual(dtEnd) || dtStart.isAfter(dtEnd)) {
			log.info("After adjusting for bundaryies we will ignore this fillGap");
			return;
		}
		boolean done=false;
		LocalDateTime thisDtEnd = dtEnd;
		while(!done) {
						
			thisDtEnd = adjustDtEnd(dtStart, dtEnd);
			log.info("\n\nadd schedule start "+dtStart+" dtEnd "+dtEnd+" thisDtEnd "+thisDtEnd+"\n");
			
			OneSchedule os = schedHelpers.addSchedule(bb, videos, dtStart, thisDtEnd);
			if(os==null) {
				log.info("stop");
				return;
			}
			fillOsDetails(os);		
			log.info("added os "+logSched(os));
			schedules.put(os.getDt().format(treeDtf), os);
			
			dtStart = os.getDtEnd();
			if(areWeDone(dtStart, dtEnd)) {
				done=true;
				break;
			}
			
			dtStart = schedHelpers.adjustDtToBoundry(dtStart);
			if(areWeDone(dtStart, dtEnd)) {
				done=true;
				break;
			}
		}
		
	}
	
	private boolean areWeDone(LocalDateTime dtStart, LocalDateTime dtEnd) {
		if(dtStart.isEqual(dtEnd)) {
			log.info("start is equal end, done "+dtStart+" "+dtEnd);
			return true;
		}
		else if(dtStart.isAfter(dtEnd)) {
			log.error("start is after end, done "+dtStart+" "+dtEnd);
			return true;
		}
		return false;
	}

	
	/**
	 * adjust the dtEnd<br>
	 * if dtStart minute==0 then we can schedule a video of more than an hour<br>
	 * otherwise, schedule at most a 1 hour video
	 * @param dt
	 * @param maxgap
	 */
	private LocalDateTime adjustDtEnd(LocalDateTime dtStart, LocalDateTime dtEnd) {
		LocalDateTime endThis;
		// we have at least an hour to work with
		if(dtStart.getMinute()==0) {
			endThis = dtStart.toLocalDate().atTime(LocalTime.MAX);

		}
		else {
			endThis = dtStart.plusHours(1).truncatedTo(ChronoUnit.HOURS);
		}
		
		return endThis;
		
	}
	
	private void fillOsDetails(OneSchedule os) {
		OneVideo ov = videos.get(os.getPkey());
		ScheduleHelpers.SetEndDt(os, ov);
		ov.setPrevLastScheduled(ov.getLastScheduled());
		ov.setLastScheduled(os.getDt());
	}

	public String logSched(OneSchedule sched) {
		Integer pkey = sched.getPkey();
		OneVideo ov = bb.getVideos().get(pkey);
		LocalDateTime startDt = sched.getDt();
		LocalDateTime endDt = sched.getDtEnd();
		//Integer millisUntilEnd = (int) ChronoUnit.MILLIS.between(now, endDt);
		//Float te = (float) (millisUntilEnd/1000);
		String rv = "\n";
		rv+="startDt "+startDt+"\n";
		rv+="endDt   "+endDt+"\n";
		//rv+="secondsUntilEnd "+te+"\n";
		rv+="ov:"+ov.log()+"\n";
		rv+="os:"+sched.log();		
		return rv;
	}
	private String getPrettyTime(Integer seconds) {
		String rv="";
		int hours = seconds/(60*60);
		rv+=hours+" hours ";
		seconds = seconds-(hours*60*60);
		int minutes = seconds/60;
		rv+=minutes+" minutes ";
		seconds = seconds-(minutes*60);
		rv+=seconds+" seconds";
		rv+="";
		return rv;
	}
}
