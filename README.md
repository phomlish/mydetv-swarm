# MYDETV Swarm

The [My Delaware TV](https://www.mydetv.com) Swarm suite of applications provides a  solution to deliver live and prerecorded peer to peer (P2P) broadcasting over the internet viewable to the end user's browser.

For a background and brief history see the project summary at docs/MydetvSwarmProjectSummary.odt

The following projects provide the solution.  See [PROJECTS.md](PROJECTS.md) for details.
- com.mydetv.java
    - shared 
    - broadcast
    - maint
    - schedule

## Developing

See docs/Develop Swarm.odt for instructions to get your development environment setup.

## Deployment

See docs/Install Swarm.odt for instructions to deploy the suite to Ununtu 16.04.

## Supported Browsers
- Chrome
- Firefox

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We have not figured out versioning yet.

## Authors

* **Paul Homlish** - *Initial work* - [website](https://www.homlish.net/~phomlish/pjhiii/pjhiii.html)

See also the list of [contributors](https://github.com/phomlish/mydetv-swarm/graphs/contributors) who participated in this project.

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Everyone who participated in [Mad Buffalo Productions](https://www.madbuffaloproductions.com/) live video P2P events over the last 12 years
* The folks over at [WebRTC.org](https://webrtc.org) for getting us started learning browser to browser video P2P
* [Meetecho-Janus](https://janus.conf.meetecho.com/docs/index.html) for creating the media gateway so we didn't have to write the C code to interface with GStreamer


