-- MySQL dump 10.13  Distrib 8.0.1-dmr, for macos10.12 (x86_64)
--
-- Host: mysql.homlish.net    Database: mydetv
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ScheduleLive`
--

DROP TABLE IF EXISTS `ScheduleLive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ScheduleLive` (
  `idschedule_live` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `dtStart` datetime DEFAULT NULL,
  `dtEnd` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `facebookGroupID` varchar(45) DEFAULT NULL,
  `facebookEventID` varchar(64) DEFAULT NULL COMMENT '-1 if not to be added to facebook, 0 is facebook event but failed to add/edit/update',
  `dtAdded` datetime DEFAULT NULL,
  `dtUpdated` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `subcat` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idschedule_live`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `archives`
--

DROP TABLE IF EXISTS `archives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archives` (
  `pkey` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(255) NOT NULL,
  `site_fpn` varchar(255) NOT NULL,
  `dt_added` datetime DEFAULT NULL,
  `dt_received` datetime DEFAULT NULL,
  `videos_fn` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `videos_path` varchar(255) NOT NULL,
  `filesize` int(10) unsigned NOT NULL,
  `details` varchar(255) NOT NULL,
  PRIMARY KEY (`pkey`),
  KEY `site_fpn` (`site_fpn`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=917 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channelSchedule`
--

DROP TABLE IF EXISTS `channelSchedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channelSchedule` (
  `idchannelSchedule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channelsId` int(10) unsigned NOT NULL,
  `pkey` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idchannelSchedule`)
) ENGINE=InnoDB AUTO_INCREMENT=3963 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channelWebUsers`
--

DROP TABLE IF EXISTS `channelWebUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channelWebUsers` (
  `channelWebUsersId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channelId` int(10) DEFAULT NULL,
  `webUserId` int(10) DEFAULT NULL,
  `dtEntered` timestamp(3) NULL DEFAULT NULL,
  `dtExited` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`channelWebUsersId`),
  UNIQUE KEY `channelEnterId_UNIQUE` (`channelWebUsersId`)
) ENGINE=InnoDB AUTO_INCREMENT=2062 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `channels_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `channelType` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not scheduled\n1: scheduled',
  `janusId` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `janusAudioPort` int(4) unsigned NOT NULL DEFAULT '0',
  `janusVideoPort` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channels_id`),
  UNIQUE KEY `channels_id_UNIQUE` (`channels_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatMessages`
--

DROP TABLE IF EXISTS `chatMessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatMessages` (
  `chatId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webuserId` int(10) unsigned DEFAULT NULL,
  `message` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtSent` timestamp(3) NULL DEFAULT NULL,
  `channelId` int(10) DEFAULT NULL,
  PRIMARY KEY (`chatId`),
  UNIQUE KEY `chatId_UNIQUE` (`chatId`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebookAttending`
--

DROP TABLE IF EXISTS `facebookAttending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookAttending` (
  `idfacebookAttending` int(11) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) DEFAULT NULL,
  `groupID` bigint(20) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `eventID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idfacebookAttending`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebookAuth`
--

DROP TABLE IF EXISTS `facebookAuth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookAuth` (
  `idfacebook_auth` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned DEFAULT NULL,
  `userName` varchar(128) DEFAULT NULL,
  `dtAquired` datetime DEFAULT NULL,
  `expires` int(10) unsigned DEFAULT NULL,
  `appID` varchar(45) DEFAULT NULL,
  `state` char(36) DEFAULT NULL,
  `code` varchar(256) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`idfacebook_auth`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebookPicture`
--

DROP TABLE IF EXISTS `facebookPicture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookPicture` (
  `idFacebookPicture` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` bigint(20) unsigned DEFAULT NULL,
  `dtCreated` datetime DEFAULT NULL,
  `link` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`idFacebookPicture`)
) ENGINE=InnoDB AUTO_INCREMENT=4967 DEFAULT CHARSET=latin1 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebookState`
--

DROP TABLE IF EXISTS `facebookState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookState` (
  `idfacebookState` int(11) NOT NULL AUTO_INCREMENT,
  `state` char(36) DEFAULT NULL,
  `dtReceived` datetime DEFAULT NULL,
  PRIMARY KEY (`idfacebookState`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebookUsers`
--

DROP TABLE IF EXISTS `facebookUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookUsers` (
  `idFacebookUsers` int(11) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `administrator` tinyint(4) DEFAULT NULL,
  `dtUpdated` datetime DEFAULT NULL,
  `dtFirstSeen` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `idPicture` int(10) unsigned DEFAULT NULL,
  `dtLastSeen` datetime DEFAULT NULL,
  PRIMARY KEY (`idFacebookUsers`)
) ENGINE=InnoDB AUTO_INCREMENT=786 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleCovers`
--

DROP TABLE IF EXISTS `googleCovers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleCovers` (
  `idgoogleCovers` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `google_id` decimal(21,0) NOT NULL,
  `url` varchar(255) NOT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `topImageOffset` smallint(5) unsigned DEFAULT NULL,
  `leftImageOffset` smallint(5) unsigned DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `modified_dt` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idgoogleCovers`),
  UNIQUE KEY `idgoogleCovers_UNIQUE` (`idgoogleCovers`),
  KEY `googleId` (`google_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleHangouts`
--

DROP TABLE IF EXISTS `googleHangouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleHangouts` (
  `idgoogleHangouts` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `googleHangoutId` varchar(255) DEFAULT NULL,
  `dtCreated` datetime DEFAULT NULL,
  `createdBy` decimal(21,0) unsigned NOT NULL,
  `dtEnded` datetime DEFAULT NULL,
  `dtPing` datetime DEFAULT NULL,
  PRIMARY KEY (`idgoogleHangouts`),
  UNIQUE KEY `idgoogleHangouts_UNIQUE` (`idgoogleHangouts`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf32 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleImages`
--

DROP TABLE IF EXISTS `googleImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleImages` (
  `idgoogleImages` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `google_id` decimal(21,0) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_dt` datetime NOT NULL,
  `modified_dt` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idgoogleImages`),
  UNIQUE KEY `idgoogleImages_UNIQUE` (`idgoogleImages`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf32 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleScope`
--

DROP TABLE IF EXISTS `googleScope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleScope` (
  `idgoogleScope` int(11) NOT NULL,
  `scope` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgoogleScope`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleType`
--

DROP TABLE IF EXISTS `googleType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleType` (
  `idgoogleType` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`idgoogleType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleUserTracking`
--

DROP TABLE IF EXISTS `googleUserTracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleUserTracking` (
  `idgoogleUserTracking` int(11) NOT NULL AUTO_INCREMENT,
  `google_id` decimal(21,0) NOT NULL,
  `hangoutId` int(11) DEFAULT NULL,
  `hangoutPersonInstanceId` varchar(255) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `scope` smallint(6) DEFAULT NULL,
  `details` varchar(2048) DEFAULT NULL,
  `dtEntered` datetime DEFAULT NULL,
  `dtLeft` datetime DEFAULT NULL,
  PRIMARY KEY (`idgoogleUserTracking`),
  UNIQUE KEY `idgoogleUserTracking_UNIQUE` (`idgoogleUserTracking`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleUserTrackingSummary`
--

DROP TABLE IF EXISTS `googleUserTrackingSummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleUserTrackingSummary` (
  `idgoogleUserTrackingSummary` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `dt` datetime NOT NULL,
  `idGoogleHangouts` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgoogleUserTrackingSummary`)
) ENGINE=InnoDB AUTO_INCREMENT=8060 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `googleUsers`
--

DROP TABLE IF EXISTS `googleUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googleUsers` (
  `google_id` decimal(21,0) NOT NULL,
  `displayName` varchar(60) NOT NULL,
  `email` varchar(60) DEFAULT NULL,
  `link` varchar(60) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `isAdmin` tinyint(1) DEFAULT '0',
  `created_dt` datetime NOT NULL,
  `modified_dt` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`google_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guests`
--

DROP TABLE IF EXISTS `guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guests` (
  `idguests` int(11) NOT NULL AUTO_INCREMENT,
  `id_schedule` int(11) DEFAULT NULL,
  `id_people` int(11) DEFAULT NULL,
  `responsibility` int(11) DEFAULT NULL,
  PRIMARY KEY (`idguests`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteUriBase64` varchar(5000) DEFAULT NULL,
  `dtAdded` datetime DEFAULT NULL,
  `dtRecieved` datetime DEFAULT NULL,
  `userAdded` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `siteUri` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userAdded_idx` (`userAdded`),
  KEY `fk_imagesCategory_idx` (`category`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imagesCategory`
--

DROP TABLE IF EXISTS `imagesCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagesCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mbp`
--

DROP TABLE IF EXISTS `mbp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mbp` (
  `pkey` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `dt_added` datetime NOT NULL,
  `dvd` char(1) DEFAULT NULL,
  `converted` char(1) DEFAULT NULL,
  `edited` char(1) DEFAULT NULL,
  `fn2` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `showdate` datetime DEFAULT NULL,
  `showset` int(4) DEFAULT NULL,
  `fn_orig` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`)
) ENGINE=MyISAM AUTO_INCREMENT=442 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plog`
--

DROP TABLE IF EXISTS `plog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plog` (
  `idplog` int(11) NOT NULL AUTO_INCREMENT,
  `dt` datetime DEFAULT NULL,
  `program` varchar(45) DEFAULT NULL,
  `function` varchar(255) DEFAULT NULL,
  `details` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`idplog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responsibility`
--

DROP TABLE IF EXISTS `responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsibility` (
  `idresponsbility` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`idresponsbility`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `idschedule` int(11) NOT NULL AUTO_INCREMENT,
  `mydetvChannel` int(11) NOT NULL DEFAULT '1',
  `dt` datetime NOT NULL,
  `pkey` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idschedule`,`dt`),
  KEY `schedule2video_idx` (`pkey`),
  CONSTRAINT `schedule2video` FOREIGN KEY (`pkey`) REFERENCES `videos` (`pkey`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11042 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scheduleTest`
--

DROP TABLE IF EXISTS `scheduleTest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduleTest` (
  `dt` datetime NOT NULL,
  `video` int(10) unsigned NOT NULL,
  `duration` float(10,2) DEFAULT NULL,
  `idschedule` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idschedule`,`dt`)
) ENGINE=MyISAM AUTO_INCREMENT=74428 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sounds`
--

DROP TABLE IF EXISTS `sounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sounds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) unsigned NOT NULL,
  `sd` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4998 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videoCategory`
--

DROP TABLE IF EXISTS `videoCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videoCategory` (
  `idvideo_category` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idvideo_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videoConverted`
--

DROP TABLE IF EXISTS `videoConverted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videoConverted` (
  `videoConvertedId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `machineName` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `fn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `length` float DEFAULT NULL COMMENT 'length in seconds',
  `filesize` bigint(20) unsigned NOT NULL COMMENT 'size on the filesystem',
  `bitrate` int(11) DEFAULT NULL,
  `dtConverted` timestamp(3) NULL DEFAULT NULL,
  `encoder` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `stream0` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `stream1` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`videoConvertedId`),
  UNIQUE KEY `videoConvertedId_UNIQUE` (`videoConvertedId`)
) ENGINE=InnoDB AUTO_INCREMENT=2123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `pkey` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int(8) NOT NULL DEFAULT '0',
  `dt_added` datetime NOT NULL,
  `length` float DEFAULT NULL COMMENT 'length in seconds',
  `filesize` bigint(20) unsigned NOT NULL COMMENT 'size on the filesystem',
  `convertedDt` datetime DEFAULT NULL,
  `convertedId` int(11) DEFAULT NULL,
  `convertedStatus` varchar(80) DEFAULT NULL,
  `notes` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pkey`)
) ENGINE=InnoDB AUTO_INCREMENT=4083 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webUser`
--

DROP TABLE IF EXISTS `webUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webUser` (
  `webUser_id` int(11) NOT NULL AUTO_INCREMENT,
  `webAuthProvider` tinyint(4) DEFAULT NULL COMMENT '1: google\n2: facebook\n3: twitter\n',
  `webAuthId` varchar(40) DEFAULT NULL COMMENT 'the id from the auth provider',
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `timezone` varchar(80) DEFAULT NULL,
  `is18` char(1) DEFAULT NULL COMMENT 'did the provider say they are 18?',
  `verified` char(1) DEFAULT NULL,
  `userAlias` varchar(60) DEFAULT NULL COMMENT 'set by user',
  `userTimezone` varchar(80) DEFAULT NULL,
  `is18Admin` char(1) DEFAULT NULL,
  `dtAdded` datetime DEFAULT NULL,
  `dtVerified` datetime DEFAULT NULL,
  `dtLastSeen` datetime DEFAULT NULL,
  `dtLocked` datetime DEFAULT NULL,
  `userLink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`webUser_id`),
  KEY `IDX_username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webUserPicture`
--

DROP TABLE IF EXISTS `webUserPicture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webUserPicture` (
  `webUserPicture_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webUserId` int(11) NOT NULL,
  `webUrl` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dtAdded` datetime DEFAULT NULL,
  PRIMARY KEY (`webUserPicture_id`),
  UNIQUE KEY `webUserPicture_id_UNIQUE` (`webUserPicture_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webUserRole`
--

DROP TABLE IF EXISTS `webUserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webUserRole` (
  `webUserRole_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webUserId` int(11) DEFAULT NULL,
  `webUserRoleType` tinyint(4) DEFAULT NULL,
  `details` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`webUserRole_id`),
  UNIQUE KEY `webUserRole_id_UNIQUE` (`webUserRole_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'mydetv'
--
/*!50003 DROP PROCEDURE IF EXISTS `channelSchedule_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channelSchedule_get`(
    IN in_channelsId INT(11)
    )
BEGIN
    SELECT idchannelSchedule,pkey from channelSchedule where channelsId=in_channelsId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channelSchedule_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channelSchedule_Insert`(
    IN in_channelsId INT(11),
    IN in_pkey INT(11),
    OUT out_insertedId INT(11)
    )
BEGIN
	DECLARE exit handler for sqlexception
		BEGIN    
			ROLLBACK;
		END;

START TRANSACTION;

	INSERT INTO channelSchedule (channelsId, pkey) values (in_channelsId, in_pkey);
    SET out_insertedId = LAST_INSERT_ID();
    
    SELECT out_insertedId;

	COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channels_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channels_get`()
BEGIN
	SELECT channels_id,name,active,channelType,janusId,janusAudioPort,janusVideoPort from channels;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channelWebUsers_EnterExit` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channelWebUsers_EnterExit`(
IN in_channelWebUsersId INT(10),
IN in_channelId INT(10),
IN in_webUserId INT(10),
IN in_dt TIMESTAMP(3)
, OUT out_ChannelWebUsersId INT(10)
)
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
    
  ROLLBACK;
END;

START TRANSACTION;
   if(in_channelWebUsersId = 0) THEN
        insert into channelWebUsers (channelId,webUserId,dtEntered) values (in_channelId,in_webUserId,in_dt);
      SET out_ChannelWebUsersId = LAST_INSERT_ID();
	ELSE
	update channelWebUsers  set dtExited=in_dt where channelWebUsersId=in_channelWebUsersId;	
    SET out_ChannelWebUsersId=in_channelWebUsersId;
    END IF;
    
	SELECT out_channelWebUsersId;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channelWebUsers_Reset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channelWebUsers_Reset`()
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
    
  ROLLBACK;
END;

START TRANSACTION;

	UPDATE channelWebUsers set dtExited=UTC_TIMESTAMP() where dtExited is null;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channel_updateActive` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channel_updateActive`(
    IN in_channelsId INT(11),
    IN in_channelActive INT(4),
    OUT out_updated INT(11)
    )
BEGIN
	DECLARE exit handler for sqlexception
		BEGIN    
			ROLLBACK;
		END;

SET @out_updated = 0;
START TRANSACTION;
	
	UPDATE channels set active=in_channelActive where channels_id=in_channelsId;
    SET @out_updated = ROW_COUNT() + @out_updated;	
    SELECT out_updated;

	COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `channel_updateName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `channel_updateName`(
    IN in_channelsId INT(11),
    IN in_channelName VARCHAR(80),
    OUT out_updated INT(11)
    )
BEGIN
	DECLARE exit handler for sqlexception
		BEGIN    
			ROLLBACK;
		END;

START TRANSACTION;
	
	UPDATE channels set name=in_channelName where channels_id=in_channelsId;
    SET out_updated = ROW_COUNT();
    SELECT out_updated;

	COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `chatMessages_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `chatMessages_get`(
	IN in_channelId INT(10),
	IN in_dtSent TIMESTAMP(0),
    IN in_limit INT(10)
)
BEGIN
	SELECT cm.message,cm.dtSent,cm.webuserId,wu.username,wu.userAlias 
		from chatMessages cm
		LEFT JOIN webUser wu ON cm.webUserId=wu.webUser_id
		where channelId=in_channelId and dtSent>in_dtSent
        order by dtSent desc
		limit in_limit;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `chatMessages_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `chatMessages_insert`(
IN in_channelId INT(10),
IN in_webUserId INT(10),
IN in_dtSent TIMESTAMP(3),
IN in_message VARCHAR(1024)
)
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
    
  ROLLBACK;
END;

START TRANSACTION;
	 insert into chatMessages (channelId,webUserId,dtSent,message) values (in_channelId,in_webUserId,in_dtSent,in_message);
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `schedules_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `schedules_get`(
IN in_mydetvChannel INT(11)
)
BEGIN
	SELECT s.idschedule,s.dt,s.pkey,v.length from schedule s
    LEFT JOIN videos v on v.pkey=s.pkey
    where s.mydetvChannel=in_mydetvChannel;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `schedules_getAfter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `schedules_getAfter`(
IN in_mydetvChannel INT(11),
IN in_dt DATETIME
)
BEGIN
	SELECT s.idschedule,s.dt,s.pkey,v.length from schedule s
    LEFT JOIN videos v on v.pkey=s.pkey
    where s.mydetvChannel=in_mydetvChannel
    and s.dt >= in_dt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `schedule_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `schedule_insert`(
    IN in_mydetvChannel INT(11),
    IN in_pkey INT(11),
    IN in_dt TIMESTAMP(3),
    OUT out_idschedule INT(11)
    )
BEGIN
  DECLARE exit handler for sqlexception
    BEGIN
      ROLLBACK;
    END;

START TRANSACTION;

	insert into schedule (mydetvChannel,pkey,dt) values(in_mydetvChannel,in_pkey,in_dt);
	SET out_idschedule = LAST_INSERT_ID();

    SELECT out_idschedule;

COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `videos_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `videos_get`(
    
    )
BEGIN
	SELECT pkey,fn,subdir,title,category,dt_added,length,filesize,convertedDt,convertedId,convertedStatus,notes,thumbnail from videos;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `webUserPicture_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `webUserPicture_insert`(
    IN in_webUserId INT(11),
    IN in_webUrl VARCHAR(255),
    OUT out_webUserPicture_id INT(11),
    OUT out_path VARCHAR(255)
)
BEGIN
DECLARE exit handler for sqlexception
  BEGIN
    
  ROLLBACK;
END;

START TRANSACTION;
select  webUserPicture_id,path 
	INTO out_webUserPicture_id,out_path 
    FROM webUserPicture
    WHERE webUserId=in_webUserId and webUrl=in_webUrl
    ORDER BY dtAdded desc
    LIMIT 1;

IF (out_webUserPicture_id is null or out_webUserPicture_id = '')
Then
	insert into webUserPicture (webUserId,webUrl,dtAdded) values(in_webUserId,in_webUrl,UTC_TIMESTAMP());
	SET out_webUserPicture_id = LAST_INSERT_ID();
END IF;

SELECT out_webUserPicture_id, out_path;
COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `webUserPicture_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `webUserPicture_update`(
    IN in_webUserPicture_id VARCHAR(40),
    IN in_path VARCHAR(255)
)
BEGIN
	update webUserPicture set path=in_path where webUserPicture_id=in_webUserPicture_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `webUserRole_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `webUserRole_get`(
    IN webUserId INT
    )
BEGIN
	SELECT webUserRoleType from webUserRole where webUserId=webUserId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `webUser_insertUpdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `webUser_insertUpdate`(
    IN in_webAuthId VARCHAR(40),
    IN in_webAuthProvider INT(4),
    IN in_username VARCHAR(255),
    IN in_email VARCHAR(255),
    IN in_link VARCHAR(255),
    IN in_timezone VARCHAR(80),
    IN in_is18 CHAR(1),
    IN in_verified CHAR(1),
    OUT wuid INT(11),
    OUT out_userAlias VARCHAR(60),
    OUT out_userTimezone VARCHAR(80),
    OUT out_userLink VARCHAR(255),
    OUT out_dtVerified TIMESTAMP,
    OUT out_is18Admin CHAR(1)
)
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
    
  ROLLBACK;
END;

START TRANSACTION;
select  webUser_id,userAlias,userTimezone,userLink,dtVerified,is18Admin INTO wuid,out_userAlias,out_userTimezone,out_userLink,out_dtVerified,out_is18Admin 
    from webUser 
    where webAuthId=in_webAuthId ; 

IF (wuid is not null)
Then
  update webUser 
    set 
		username		=in_username,
		email 				=in_email,
		link					=in_link,
        timezone			=in_timezone,
        verified			=in_verified,
        is18					=in_is18,
        dtLastSeen		=UTC_TIMESTAMP()
    where webUser_id	=wuid;
ELSE
  insert into webUser
	(webAuthId, webAuthProvider,username, email, profile,dtAdded,timezone,verified,is18) 
    values
    (in_webAuthId,in_webAuthProvider,in_username, in_email, in_profile,UTC_TIMESTAMP(),in_timezone,in_verified,in_is18);
    
  SET wuid = LAST_INSERT_ID();
END IF;


COMMIT;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `webUser_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`phomlish`@`%` PROCEDURE `webUser_Update`(
	IN in_webUser_id INT(4),
    IN in_alias VARCHAR(255),
    IN in_link VARCHAR(255),
    IN in_timezone VARCHAR(80),
    OUT wuid INT(11)
 )
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
END;

START TRANSACTION;
select  webUser_id  INTO wuid
    from webUser 
    where webUser_id=in_webUser_id ; 

IF (wuid is not null)
Then
  update webUser 
    set 
		userAlias			=in_alias,
		userLink			=in_link,
        userTimezone=in_timezone
    where webUser_id	=wuid;
ELSE
    SET wuid=0;
END IF;


COMMIT;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-06  2:49:34
