-- MySQL dump 10.13  Distrib 8.0.1-dmr, for macos10.12 (x86_64)
--
-- Host: mysql.homlish.net    Database: mydetv
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `videoCategory`
--

LOCK TABLES `videoCategory` WRITE;
/*!40000 ALTER TABLE `videoCategory` DISABLE KEYS */;
INSERT INTO `videoCategory` VALUES (1,'md','mbp','Michael Davis','MichaelDavis.jpg');
INSERT INTO `videoCategory` VALUES (2,'zane','mbp','Zane Campbell','ZaneCampbell.jpg');
INSERT INTO `videoCategory` VALUES (3,'bb','archive.org','Betty Boop','BettyBoop.jpg');
INSERT INTO `videoCategory` VALUES (4,'om','archive.org','Archive.Org Old Show / Movie','ArchiveOrg.png');
INSERT INTO `videoCategory` VALUES (5,'oc','archive.org','Archive.Org Old Commercial','ArchiveOrg.png');
INSERT INTO `videoCategory` VALUES (6,'nf','mbp','Nate Farrar','NateFarrar.jpg');
INSERT INTO `videoCategory` VALUES (7,'guest','mbp','Club Malawi Guest','mbp.png');
INSERT INTO `videoCategory` VALUES (8,'mp','mbp','MBP Party','mbp.png');
INSERT INTO `videoCategory` VALUES (9,'mbp clip','mbp','MBP short video clip','mbp.png');
INSERT INTO `videoCategory` VALUES (10,'christmas','archive.org','Chrismas Show',NULL);
INSERT INTO `videoCategory` VALUES (11,'donated','mbp','MBP Shared','mbp.png');
INSERT INTO `videoCategory` VALUES (12,'mbpr','mbp','MBP Remote','mbp.png');
INSERT INTO `videoCategory` VALUES (13,'sid','sid','Station Identification',NULL);
INSERT INTO `videoCategory` VALUES (14,'tp','tp','Test Pattern',NULL);
INSERT INTO `videoCategory` VALUES (15,'uncategorized','archive.org','uncategorized',NULL);
INSERT INTO `videoCategory` VALUES (16,'uncategorized','mbp','uncategorized',NULL);
INSERT INTO `videoCategory` VALUES (17,'uncategorized','clips','uncategorized',NULL);
/*!40000 ALTER TABLE `videoCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (0,'My First Channel',0,0,11,5018,5020);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-06  3:01:25
